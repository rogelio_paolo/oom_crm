<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountTm extends Model
{
    protected $table = 'accounts_tm';

    protected $fillable = [
        'contact_page_url',
        'ty_page_url',
        'ga_required',
        'ga_remark',
        'implementations',
        'implementations_remark',
        'web_login',
        'is_renew',
        'renew_date',
        'svc_blog_id',
        'svc_social_id',
        'svc_baidu_id',
        'svc_weibo_id',
        'svc_wechat_id'
    ];

    public $timestamps = false;

    public function blog() {
        return $this->hasOne('App\AccountServicesBlogContent', 'id', 'svc_blog_id');
    }

    public function social() {
        return $this->hasOne('App\AccountServicesSocialManagement', 'id', 'svc_social_id');
    }

    public function baidu() {
        return $this->hasOne('App\AccountServicesBaidu', 'id', 'svc_baidu_id');
    }

    public function weibo() {
        return $this->hasOne('App\AccountServicesWeibo', 'id', 'svc_weibo_id');
    }

    public function wechat() {
        return $this->hasOne('App\AccountServicesWeChat', 'id', 'svc_wechat_id');
    }

}
