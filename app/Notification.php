<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'notifiable_id');
    }

    // public function scopeFetchNotification($query) {

    //     $auth_user = Auth::user();

    //     return $query->
    // }
}
