<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'role',
        'activeflag'
    ];

    public $timestamps = false;

    public function syscode()
    {
        return $this->hasOne('App\System','systemcode','role');
    }
}
