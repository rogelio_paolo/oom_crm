<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class EmployeeTotal extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->labels(['Singapore','Philippines']);
        
        return $this->options([
            'chart' => [
                'options3d' => [
                    'enabled' => true,
                    'alpha' => 45,
                ]
            ],
            'plotOptions' => [
                'pie' => [
                    'innerSize' => 100,
                    'depth' => 45
                ]
            ],
            'title' => [
                'text' => 'Total Employees',
                'color' => '#fff',
                'fill' => '#fff'
            ],
            'subtitle' => [
                'text' => 'Per Branch',
                'color' => '#fff',
                'fill' => '#fff'
            ]
        ]);
    }
}
