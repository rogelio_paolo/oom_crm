<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class AccountPerStatus extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->labels(['Active','Not Renewing (Lost)']);
        
        return $this->options([
            'chart' => [
                'options3d' => [
                    'enabled' => true,
                    'alpha' => 45,
                ]
            ],
            'plotOptions' => [
                'pie' => [
                    'innerSize' => 100,
                    'depth' => 45
                ]
            ],
            'title' => [
                'text' => 'Total Accounts Created'
            ],
            'subtitle' => [
                'text' => 'Per Status'
            ]
        ]);
    }
}
