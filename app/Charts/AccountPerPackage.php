<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class AccountPerPackage extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->labels(['SEM','FB','SEO','Web Development','Baidu','Weibo','WeChat','Blog Content','Social Media Management','Post Paid SEM']);
        
        return $this->options([
            'chart' => [
                'options3d' => [
                    'enabled' => true,
                    'alpha' => 45,
                ]
            ],
            'plotOptions' => [
                'pie' => [
                    'innerSize' => 100,
                    'depth' => 45
                ]
            ],
            'title' => [
                'text' => 'Total Accounts Created'
            ],
            'subtitle' => [
                'text' => 'Per Package'
            ]
            
        ]);
    }
}
