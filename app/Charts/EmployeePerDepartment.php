<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class EmployeePerDepartment extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->labels(['Account Management',
                        'Business Management', 
                        'Content Management', 
                        'Development & Creative', 
                        'Human & Finance Management', 
                        'Senior Management',
                        'AI & Tech Team',
                        'Search Engine Marketing',
                        'Search Engine Optimization',
                    ]);
        
        return $this->options([
            'chart' => [
                'options3d' => [
                    'enabled' => true,
                    'alpha' => 45,
                ]
            ],
            'plotOptions' => [
                'pie' => [
                    'innerSize' => 100,
                    'depth' => 45
                ]
            ],
            'title' => [
                'text' => 'Total Employees',
                'color' => '#fff',
                'fill' => '#fff'
            ],
            'subtitle' => [
                'text' => 'Per Department',
                'color' => '#fff',
                'fill' => '#fff'
            ]
        ]);
    }
}
