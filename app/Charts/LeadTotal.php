<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class LeadTotal extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct($dates)
    {
        parent::__construct();

        $this->labels($dates);
        
    }
}
