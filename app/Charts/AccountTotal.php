<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class AccountTotal extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct($months)
    {
        parent::__construct();

        $this->labels($months);

    }
}
