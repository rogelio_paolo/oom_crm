<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class LeadPerStatus extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->labels(['Super Hot','Hot', 'Warm', 'Cold', 'Converted', 'Lost']);
        
        return $this->options([
            'chart' => [
                'options3d' => [
                    'enabled' => true,
                    'alpha' => 45,
                ]
            ],
            'plotOptions' => [
                'pie' => [
                    'innerSize' => 100,
                    'depth' => 45
                ]
            ],
            'title' => [
                'text' => 'Total Leads Created',
                'color' => '#fff',
                'fill' => '#fff'
            ],
            'subtitle' => [
                'text' => 'Per Status',
                'color' => '#fff',
                'fill' => '#fff'
            ]
        ]);
    }
}
