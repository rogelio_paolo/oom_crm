<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class AccountByHolder extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $busi = \App\Employee::select('f_name')->where('department',2)->get()->toArray();
        $this->labels(array_flatten($busi));
    }
}
