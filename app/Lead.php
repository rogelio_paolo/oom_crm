<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{

    protected $table = 'leads';

    protected $fillable = [
        'company',
        'f_name',
        'l_name',
        'designation',
        'email',
        'contact_number',
        'office_number',
        'street_address',
        'zip_code',
        'city',
        'website',
        'services',
        'other_services',
        'source',
        'source_other',
        'landing_page',
        'landing_other',
        'currency',
        'contract_value',
        'industry',
        'status',
        'is_convert_again',
        'lock_this_lead',
        'status_updated_by',
        'status_updated_at',
        'created_by',
        'delete_flag',
        'deleted_at'
    ];
   
    public function account()
    {
        return $this->hasOne('App\Account','lead_id','id');
    }

    public function leadStatus()
    {
        return $this->hasOne('App\System','systemcode','status');
    }

    public function leadServices()
    {
        return $this->hasOne('App\System','systemcode','services');
    }
    
    public function note()
    {
        return $this->hasMany('App\LeadNote','lead_id','id');
    }

    public function sales()
    {
        return $this->hasOne('App\User','userid','created_by');
    }

    public function landing()
    {
        return $this->hasOne('App\System','systemcode','landing_page');
    }

    public function source()
    {
        return $this->hasOne('App\System','systemcode','source');
    }

    public function source_leads()
    {
        return $this->hasOne('App\System','systemcode','source');
    }

    public function lead_industry()
    {
        return $this->hasOne('App\Industry', 'code', 'industry');
    }

    public function scopeFindName($query,$key)
    {
        $user = \Auth::user();
        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->where('leads.created_by',$user->userid)
                         ->where('f_name','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag', 1)
                          ->paginate($perPage);
        else
            return $query->where('f_name','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                        ->paginate($perPage);
    }

    public function scopeFindEmail($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->where('leads.created_by',$user->userid)
                         ->where('email','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
        else
            return $query->where('email','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
    }

    public function scopeFindContact($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->where('leads.created_by',$user->userid)
                         ->where('contact_number','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
        else
            return $query->where('contact_number','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
    }

    public function scopeFindCompany($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->where('leads.created_by',$user->userid)
                         ->where('company','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
        else
            return $query->where('company','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
    }

    public function scopeFindStatus($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->join('system','leads.status','system.systemcode')
                     ->where('leads.created_by',$user->userid)
                     ->where('system.systemdesc','like','%'.$key.'%')
                     ->whereNull('leads.delete_flag')
                     ->where('leads.active_flag',1)
                     ->paginate($perPage);
        else
            return $query->join('system','leads.status','system.systemcode')
                     ->where('system.systemdesc','like','%'.$key.'%')
                     ->whereNull('leads.delete_flag')
                     ->where('leads.active_flag',1)
                     ->paginate($perPage);
    }
    
    public function scopeFindDate($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            return $query->where('leads.created_by',$user->userid)
                         ->where('created_at','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
        else
            return $query->where('created_at','like','%'.$key.'%')
                         ->whereNull('delete_flag')
                         ->where('active_flag',1)
                         ->paginate($perPage);
    }

    public function scopeFindOwner($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        return $query->select('leads.*')
                     ->join('users','leads.created_by','users.userid')
                     ->join('employees','users.emp_id','employees.emp_id')
                     ->where('employees.f_name','like','%'.$key.'%')
                     ->orWhere('employees.l_name','like','%'.$key.'%')
                     ->where('leads.active_flag',1)
                     ->paginate($perPage);
    }

    public function scopeSort($query,$field,$order)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1)
            if ($field != 'status')
                return $query->where('created_by',$user->userid)
                             ->where('active_flag',1)
                             ->whereNull('delete_flag')
                             ->where('status', '!=', 'LS005')
                             ->orderBy($field,$order)
                             ->paginate($perPage);
            else
                return $query->join('system','leads.status','system.systemcode')
                            ->where('created_by',$user->userid)
                            ->whereNull('leads.delete_flag')
                            ->where('leads.active_flag',1)
                            ->where('leads.status', '!=', 'LS005')
                            ->orderBy('system.systemdesc',$order)
                            ->paginate($perPage);
        else
            if ($field == 'owner')
                return $query->select('leads.*')
                                ->join('users','leads.created_by','users.userid')
                                ->join('employees','users.emp_id','employees.emp_id')
                                ->whereNull('leads.delete_flag')
                                ->where('leads.active_flag',1)
                                ->where('leads.status', '!=', 'LS005')
                                ->orderBy('employees.f_name',$order)
                                ->paginate($perPage);
            elseif ($field != 'status')
                return $query
                ->whereNull('delete_flag')
                ->where('status', '!=', 'LS005')
                ->where('active_flag',1)
                ->orderBy($field,$order)->paginate($perPage);
            else
                return $query->join('system','leads.status','system.systemcode')
                            ->whereNull('leads.delete_flag')
                            ->where('leads.active_flag',1)
                            ->where('leads.status', '!=', 'LS005')
                            ->orderBy('system.systemdesc',$order)
                            ->paginate($perPage);
    }
}
