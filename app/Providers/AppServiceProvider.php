<?php

namespace App\Providers;

use Auth;
use View;
use Hash;
use Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // \URL::forceScheme('https');

        Schema::defaultStringLength(191);

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });

        View::composer('*', function($view){
            $currentUser = Auth::user();
			// $currentLang = Session::has('locale') ? Session::get('locale') : Session::put('locale','en');
            // \App::setLocale($currentLang);
            if($currentUser != null)
            {
                $view->with([
                    'user' => $currentUser
                ]);
            }
        });
        
        $this->app['validator']->extend('min_array_size', function($attribute, $value, $parameters) {
            $data = $value;

            if(!is_array($data)){
                return true;
            }
            return count($data) >= $parameters[0];
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
