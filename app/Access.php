<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $table = 'module_access';

    protected $fillable = [
        'userid',
        'module',
        'access'
    ];

    public function ModType()
    {
        return $this->hasOne('App\System','systemcode','module');
    }
}
