<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\PaginatePerPage;
use App\FilterSelection;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = [
        'opportunity_info_id',
        'prospect_id',
        'contract_id',
        'nature',
        'services_signedup',
        'status',
        'tm_id',
        'acc_assigned',
        'acc_assistant',
        'ppc_id',
        'fb_id',
        'seo_id',
        'web_id',
        'baidu_id',
        'weibo_id',
        'wechat_id',
        'blog_id',
        'social_id',
        'postpaid_sem_id',
        'created_id',
        'updated_at',
        'updated_by',
        'active_flag',
        'acc_team',
        'dev_team'
    ];

    public static function paginatePerPage()
    {
        $user = Auth::user();
        $pagination = PaginationPerPage::where('table_name','accounts')->where('userid', $user->userid)->first();
        $perPage = !empty($pagination->per_page) ? $pagination->per_page : '10';
        return $perPage;
    }
    
    function getFilterSetting()
    {
        $user = Auth::user();
        $initFilter = FilterSelection::where('table_name', 'account')->where('user_id', $user->userid)->first();
        return !empty($initFilter) ? $initFilter : '';
    }

    public function contract()
    {
        return $this->hasOne('App\Contract', 'id', 'contract_id');
    }

    public function prospect()
    {
        return $this->hasOne('App\Prospect','id','prospect_id');
    }

    public function convert()
    {
        return $this->hasOne('App\User','userid','created_id');
    }

    public function holder()
    {
        return $this->hasOne('App\User','emp_id','acc_assigned');
    }

    public function asst_holder()
    {
        return $this->hasOne('App\User','emp_id','acc_assistant');
    }

    public function package()
    {
        return $this->hasOne('App\System','systemcode','nature');
    }

    public function accountStatus()
    {
        return $this->hasOne('App\System','systemcode','status');
    }

    public function sem()
    {
        return $this->hasOne('App\AccountPpc','id','ppc_id');
    }

    public function fb()
    {
        return $this->hasOne('App\AccountFb','id','fb_id');
    }

    public function seo()
    {
        return $this->hasOne('App\AccountSeo','id','seo_id');
    }

    public function web()
    {
        return $this->hasOne('App\AccountWeb','id','web_id');
    }

    public function baidu()
    {
        return $this->hasOne('App\AccountBaidu','id','baidu_id');
    }

    public function weibo()
    {
        return $this->hasOne('App\AccountWeibo','id','weibo_id');
    }

    public function wechat()
    {
        return $this->hasOne('App\AccountWeChat','id','wechat_id');
    }
    
    public function blog()
    {
        return $this->hasOne('App\AccountBlogContent','id','blog_id');
    }

    public function social()
    {
        return $this->hasOne('App\AccountSocialManagement','id','social_id');
    }

    public function postpaid()
    {
        return $this->hasOne('App\AccountPostpaidSEM','id','postpaid_sem_id');
    }

    public function additional()
    {
        return $this->hasOne('App\AccountTm','id','tm_id');
    }

    public function editor()
    {
        return $this->hasOne('App\User','userid','updated_by');
    }
    
    public function scopeSort($query,$field,$order)
    {
        $user = auth()->user();
        $filter = $this->getFilterSetting();
        $teamcode = $user->userInfo->team->team_code;
        $like = $teamcode == 'dev' ? "%004%" : ['%001%','%002%','%003%'];
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
        

        if ($user->role == 1 || (in_array($teamcode, ['acc', 'sp', 'dev','con']) !== false && in_array($user->emp_id, $managers) !== false ))
        {   
            if($teamcode == 'con')
            {
                if ($field == 'status')
                return $query->select('accounts.*')->join('system','accounts.status','system.systemcode')
                            ->where('accounts.active_flag',1)
                            ->whereNull('accounts.delete_flag')
                            ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                            })
                            ->orderBy('system.systemdesc',$order)
                            ->paginate(self::paginatePerPage());
            elseif ($field == 'company')
                return $query->select('accounts.*')
                            ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                            ->leftjoin('leads','prospects.lead_id','leads.id')
                            ->where('accounts.active_flag',1)
                            ->whereNull('accounts.delete_flag')
                            ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                            })
                            ->orderBy('leads.company',$order)
                            ->paginate(self::paginatePerPage());
            elseif ($field == 'holder')
                return $query->select('accounts.*')
                            ->leftjoin('users','accounts.acc_assigned','users.emp_id')
                            ->leftjoin('employees','users.emp_id','employees.emp_id')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                            })
                            ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                            ->paginate(self::paginatePerPage());
            elseif ($field == 'sales')
                return $query->select('accounts.*')
                            ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                            ->leftjoin('users','prospects.created_by','users.userid')
                            ->leftjoin('employees','users.emp_id','employees.emp_id')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                            })
                            ->orderBy('employees.f_name',$order)
                            ->paginate(self::paginatePerPage());
            elseif ($field == 'created_at')
                return $query->select('accounts.*')
                            ->where('accounts.nature', 'like', '%003%')
                            ->orWhere('accounts.nature', 'like', '%008%')
                            ->orWhere('accounts.nature', 'like', '%009%')
                            ->whereNull('delete_flag')->where('accounts.active_flag',1)
                            ->orderBy('created_at',$order)->paginate(self::paginatePerPage());
            
            elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                            })
                             ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                             })
                             ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                             })
                             ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                             })
                             ->paginate(self::paginatePerPage());
        }

            if ($field == 'company')
            {
                return $query->select('accounts.*')
                             ->leftjoin('prospects', 'accounts.prospect_id', 'prospects.id')
                             ->leftjoin('leads','prospects.lead_id', 'leads.id')
                             ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                             })
                             ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                             })
                             ->whereNull('accounts.deleted_at')
                             ->where('accounts.active_flag', 1)
                             ->orderBy('leads.company', $order)
                             ->paginate(self::paginatePerPage());
            }
            else if($field == 'holder')
            {
                return $query->select('accounts.*')
                            ->leftjoin('users', 'accounts.acc_assigned', 'users.emp_id')
                            ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                            ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                             })
                             ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                             })
                            ->whereNull('accounts.deleted_at')
                            ->where('accounts.active_flag', 1)
                            ->orderBy(\DB::raw('case when accounts.acc_assigned is null then 1 else 0 end, employees.f_name'), $order)
                            ->paginate(self::paginatePerPage());
            }
            else if($field == 'sales')
            {
                return $query->select('accounts.*')
                            ->leftjoin('prospects', 'accounts.prospect_id', 'prospects.id')
                            ->leftjoin('users', 'prospects.created_by', 'users.userid')
                            ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                            ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                             })
                             ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                             })
                            ->whereNull('accounts.deleted_at')
                            ->where('accounts.active_flag', 1)
                            ->orderBy('employees.f_name', $order)
                            ->paginate(self::paginatePerPage());
            }
            else if($field == 'status')
            {
                return $query->select('accounts.*')
                            ->leftjoin('system', 'accounts.status', 'system.systemcode')
                            ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                             })
                             ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                             })
                            ->whereNull('accounts.deleted_at')
                            ->where('accounts.active_flag', 1)
                            ->orderBy('system.systemdesc', $order)
                            ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
            {
                return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                             ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                             })
                             ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                             })
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                             })
                             ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                             })
                             ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                             })
                             ->paginate(self::paginatePerPage());
            }
            else
            {
                return $query->whereNull('delete_flag')
                            ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                $query->where('accounts.created_id', $filter->person_filter);
                            })
                            ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                $query->where('accounts.acc_assigned', $filter->person_filter);
                            })
                            ->where('accounts.active_flag',1)
                            ->orderBy('created_at',$order)
                            ->paginate(self::paginatePerPage());
            }
        }
        elseif ($user->role == 4  && ($teamcode == 'acc' || $teamcode == 'sp' || $teamcode == 'con'))
        {
            $emp_id = $user->emp_id;
            $user_id = $user->userid;    

            if($teamcode == 'acc')
            {
                if ($field == 'status')
                    return $query->select('accounts.*')
                                 ->leftjoin('system','accounts.status','system.systemcode')
                                 ->whereNull('accounts.delete_flag')
                                 ->where('accounts.active_flag',1)
                                 ->where(function($query) use ($emp_id, $user_id) {
                                    $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.created_id', $user_id);
                                 })
                                 ->orderBy('system.systemdesc',$order)
                                 ->paginate(self::paginatePerPage());
                elseif ($field == 'company')
                    return $query->select('accounts.*')
                                ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                                ->leftjoin('leads','prospects.lead_id','leads.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id, $user_id) {
                                    $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.created_id', $user_id);
                                 })
                                ->orderBy('leads.company',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'holder')
                    return $query->select('accounts.*')
                                 ->leftjoin('users','accounts.acc_assigned','users.emp_id')
                                 ->leftjoin('employees','users.emp_id','employees.emp_id')
                                 ->whereNull('accounts.delete_flag')->where('accounts.active_flag',1)
                                 ->where(function($query) use ($emp_id, $user_id) {
                                    $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.created_id', $user_id);
                                 })
                                 ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                                 ->paginate(self::paginatePerPage());
                elseif ($field == 'sales')
                    return $query->select('accounts.*')->join('prospects','accounts.prospect_id','prospects.id')
                                ->join('users','prospects.created_by','users.userid')
                                ->join('employees','users.emp_id','employees.emp_id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id, $user_id) {
                                    $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.created_id', $user_id);
                                 })
                                ->orderBy('employees.f_name',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                    return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                                    ->whereNull('accounts.delete_flag')
                                    ->where('accounts.active_flag',1)
                                    ->where(function($query) use ($emp_id, $user_id) {
                                        $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                        $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                        $query->orWhere('accounts.created_id', $user_id);
                                    })
                                    ->when($field == 'contract_number', function($query) use ($order) {
                                    $query->orderBy('contracts.contract_number', $order);
                                    })
                                    ->when($field == 'day_90', function($query) use ($order) {
                                    $query->orderBy('contracts.day_90', $order);
                                    })
                                    ->when($field == 'day_120', function($query) use ($order) {
                                    $query->orderBy('contracts.day_120', $order);
                                    })
                                    ->paginate(self::paginatePerPage());
                elseif ($field == 'created_at')
                    return $query->select('accounts.*')
                                ->whereNull('delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id, $user_id) {
                                    $query->orWhere('accounts.acc_assigned','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.acc_assistant','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.created_id', $user_id);
                                })
                                ->orderBy('created_at',$order)->paginate(self::paginatePerPage());

                
            }
            else if($teamcode == 'con')
            {
                if ($field == 'status')
                    return $query->select('accounts.*')->join('system','accounts.status','system.systemcode')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('system.systemdesc',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'company')
                    return $query->select('accounts.*')
                                ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                                ->leftjoin('leads','prospects.lead_id','leads.id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('leads.company',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'holder')
                    return $query->select('accounts.*')->leftjoin('users','accounts.acc_assigned','users.emp_id')
                                ->leftjoin('employees','users.emp_id','employees.emp_id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'sales')
                    return $query->select('accounts.*')
                                ->join('prospects','accounts.prospect_id','prospects.id')
                                ->join('users','prospects.created_by','users.userid')
                                ->join('employees','users.emp_id','employees.emp_id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('employees.f_name',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'created_at')
                    return $query->select('accounts.*')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('created_at',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                    return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                                ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                                })
                                ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                                })
                                ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                                })
                                ->paginate(self::paginatePerPage());
            }
            else
            {
                if ($field == 'status')
                    return $query->select('accounts.*')->join('system','accounts.status','system.systemcode')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                                ->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                               ->orderBy('system.systemdesc',$order)
                               ->paginate(self::paginatePerPage());
                elseif ($field == 'company')
                    return $query->select('accounts.*')
                                ->leftjoin('prospects','accounts.prospect_id','prospects.id')->leftjoin('leads','prospects.lead_id','leads.id')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                                ->whereNull('accounts.delete_flag')->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('leads.company',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'holder')
                    return $query->select('accounts.*')->leftjoin('users','accounts.acc_assigned','users.emp_id')->leftjoin('employees','users.emp_id','employees.emp_id')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                                ->whereNull('accounts.delete_flag')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                               ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'sales')
                    return $query->select('accounts.*')->join('prospects','accounts.prospect_id','prospects.id')->join('users','prospects.created_by','users.userid')
                                ->join('employees','users.emp_id','employees.emp_id')->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                                ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                                ->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')->whereNull('accounts.delete_flag')->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('employees.f_name',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'created_at')
                    return $query->select('accounts.*')->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                                ->whereNull('delete_flag')->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->orderBy('created_at',$order)
                                ->paginate(self::paginatePerPage());
                elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                    return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                                ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                                ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                                ->whereNull('accounts.delete_flag')
                                ->where('accounts.active_flag',1)
                                ->where(function($query) use ($emp_id) {
                                    $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                                    $query->orWhere('accounts_webdev.dev_team','like','%'.$emp_id.'%');
                                    $query->orWhere('accounts.active_flag', 1);
                                })
                                ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                                })
                                ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                                })
                                ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                                })
                                ->paginate(self::paginatePerPage());
            }
        }
        else if($user->role == 2 && in_array($user->emp_id, $managers))
        {
            if ($field == 'status')
            {
                return $query->select('accounts.*')
                            ->join('system','accounts.status','system.systemcode')
                            ->where('nature','like',$like)
                            ->whereNull('accounts.delete_flag')->where('accounts.active_flag',1)
                            ->orderBy('system.systemdesc',$order)
                            ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'company')
            {
                return $query->select('accounts.*')
                            ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                            ->leftjoin('leads','prospects.lead_id','leads.id')
                            ->where('nature','like',$like)->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)->orderBy('leads.company',$order)
                            ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'holder')
            {
                return $query->select('accounts.*')
                            ->leftjoin('users','accounts.acc_assigned','users.emp_id')
                            ->leftjoin('employees','users.emp_id','employees.emp_id')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where('nature','like',$like)
                            ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                            ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'sales')
            {
                return $query->select('accounts.*')
                             ->join('prospects','accounts.prospect_id','prospects.id')
                             ->join('users','prospects.created_by','users.userid')
                             ->join('employees','users.emp_id','employees.emp_id')
                             ->where('nature','like',$like)
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->orderBy('employees.f_name',$order)
                             ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'created_at')
            {
                return $query->select('accounts.*')
                             ->where('nature','like',$like)
                             ->whereNull('delete_flag')
                             ->where('accounts.active_flag',1)
                             ->orderBy('created_at',$order)
                             ->paginate(self::paginatePerPage());
            }      
            elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                            ->where('nature','like',$like)
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                            })
                            ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                            })
                            ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                            })
                            ->paginate(self::paginatePerPage());
        }
        else
        {
            $field = $field == 'leads.company' ? 'leads.company' : ($field == 'created_id' ? 'employees.f_name' : ($field == 'status' ? 'system.systemdesc' : 'accounts.created_at') );

            return $query->select('accounts.*')
                         ->join('system','accounts.status','system.systemcode')
                         ->join('prospects','accounts.prospect_id','prospects.id')
                         ->join('users','accounts.created_id','users.userid')
                         ->join('employees','users.emp_id','employees.emp_id')
                         ->whereNull('accounts.delete_flag')
                         ->where('accounts.active_flag',1)
                         ->where('accounts.created_id',$user->userid)
                         ->orderBy($field,$order)
                         ->paginate(self::paginatePerPage());

            if ($field == 'company')
            {
                return $query->select('accounts.*')
                             ->join('prospects','accounts.prospect_id','prospects.id')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->where('accounts.created_id',$user->userid)
                             ->orderBy('leads.company',$order)
                             ->paginate(self::paginatePerPage());
            }
            else if($field == 'holder')
            {
                return $query->select('accounts.*')
                            ->join('prospects','accounts.prospect_id','prospects.id')
                            ->leftjoin('users','accounts.acc_assigned','users.emp_id')
                            ->leftjoin('employees','users.emp_id','employees.emp_id')
                            ->whereNull('delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where('accounts.created_id',$user->userid)
                            ->orderBy(\DB::raw('case when acc_assigned is null then 1 else 0 end, acc_assigned'),$order)
                            ->paginate(self::paginatePerPage());
            }
            else if($field == 'sales')
            {
                return $query->select('accounts.*')
                             ->join('prospects','accounts.prospect_id','prospects.id')
                             ->join('users','prospects.created_by','users.userid')
                             ->join('employees','users.emp_id','employees.emp_id')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->where('accounts.created_id',$user->userid)
                             ->orderBy('employees.f_name',$order)
                             ->paginate(self::paginatePerPage());
            }  
            else if($field == 'status')
            {
                return $query->select('accounts.*')
                            ->join('prospects','accounts.prospect_id','prospects.id')
                            ->join('system','accounts.status','system.systemcode')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where('accounts.created_id',$user->userid)
                            ->orderBy('system.systemdesc',$order)
                            ->paginate(self::paginatePerPage());
            }
            elseif ($field == 'contract_number' || $field == 'day_90' || $field == 'day_120')
                return $query->join('contracts', 'accounts.contract_id', 'contracts.id')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where('accounts.created_id', $user->userid)
                            ->when($field == 'contract_number', function($query) use ($order) {
                                $query->orderBy('contracts.contract_number', $order);
                            })
                            ->when($field == 'day_90', function($query) use ($order) {
                                $query->orderBy('contracts.day_90', $order);
                            })
                            ->when($field == 'day_120', function($query) use ($order) {
                                $query->orderBy('contracts.day_120', $order);
                            })
                            ->paginate(self::paginatePerPage());
            else
            {
                return $query->select('accounts.*')
                             ->join('prospects','accounts.prospect_id','prospects.id')
                             ->whereNull('delete_flag')
                             ->where('accounts.active_flag',1)
                             ->where('accounts.created_by',$user->userid)
                             ->orderBy('accounts.created_at',$order)
                             ->paginate(self::paginatePerPage());
            }   
        }
    }
    
    public function scopeFindCompany($query,$company)
    {   
        $user = auth()->user();
        $filter = $this->getFilterSetting();
        $teamcode = $user->userInfo->team->team_code;
        $like = $teamcode == 'dev' ? "%004%" : ['%001%','%002%','%003%'];
        $emp_id = $user->emp_id;
        $user_id = $user->userid;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        if($user->role == 1 || (in_array($teamcode, ['acc', 'sp', 'dev', 'con']) !== false && in_array($user->emp_id, $managers) !== false ))
        {
            if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                             ->leftjoin('prospects','accounts.prospect_id','=','prospects.id')
                             ->leftjoin('leads','prospects.lead_id','=','leads.id')
                             ->where('leads.company','like','%'.$company.'%')
                             ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                             })
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->orderBy('leads.company','asc')
                             ->paginate(self::paginatePerPage());
            }

            return $query->select('accounts.*')
                         ->leftjoin('prospects', 'accounts.prospect_id', '=', 'prospects.id')
                         ->leftjoin('leads', 'prospects.lead_id', '=', 'leads.id')
                         ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                            $query->where('accounts.created_id', $filter->person_filter);
                         })
                         ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                            $query->where('accounts.acc_assigned', $filter->person_filter);
                         })
                         ->where('leads.company', 'like', '%'.$company.'%')
                         ->where('accounts.active_flag', 1)
                         ->orderBy('leads.company', 'asc')
                         ->paginate(self::paginatePerPage());
        }
        else if($user->role == 4 && ($teamcode == 'acc' || $teamcode == 'sp' || $teamcode == 'con'))
        {
            if($teamcode == 'acc')
            {
                return $query->select('accounts.*')
                             ->leftjoin('prospects','accounts.prospect_id', '=', 'prospects.id')
                             ->leftjoin('leads', 'prospects.lead_id', '=', 'leads.id')
                             ->where('leads.company','like','%'.$company.'%')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag', 1)
                             ->where(function($query) use ($user_id, $emp_id) {
                                $query->orWhere('accounts.acc_assigned', $emp_id);
                                $query->orWhere('accounts.acc_assistant', $emp_id);
                                $query->orWhere('accounts.created_id', $user_id);
                             })
                             ->orderBy('leads.company', 'asc')
                             ->paginate(self::paginatePerPage());
            }
            else if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                            ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                            ->leftjoin('leads','prospects.lead_id','leads.id')
                            ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                            ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                            ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                            ->where('leads.company','like','%'.$company.'%')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where(function($query) use ($emp_id) {
                                $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts.active_flag', 1);
                            })
                            ->orderBy('leads.company','asc')
                            ->paginate(self::paginatePerPage());
            }
            else
            {
                return $query->select('accounts.*')
                    ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                    ->leftjoin('leads','prospects.lead_id','leads.id')
                    ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                    ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                    ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                    ->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                    ->where('leads.company', 'like', '%'.$company.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where(function($query) use ($emp_id) {
                        $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                        $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                        $query->orWhere('accounts.active_flag', 1);
                    })
                    ->orderBy('leads.company','asc')
                    ->paginate(self::paginatePerPage());
            }
        }
        else if($user->role == 2 && in_array($user->emp_id, $managers))
        {
            return $query->select('accounts.*')
                        ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                        ->leftjoin('leads','prospects.lead_id','leads.id')
                        ->where('accounts.nature','like', $like)
                        ->where('leads.company', 'like', '%'.$company.'%')
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag',1)
                        ->orderBy('leads.company', 'asc')
                        ->paginate(self::paginatePerPage());
        }
        else
        {
            return $query->select('accounts.*')
                    ->leftjoin('prospects','accounts.prospect_id','prospects.id')
                    ->leftjoin('leads','prospects.lead_id','leads.id')
                    ->where('leads.company', 'like', '%'.$company.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where('accounts.created_id',$user->userid)
                    ->orderBy('leads.company','asc')
                    ->paginate(self::paginatePerPage());
        }
    }

    public function scopeFindContractNumber($query,$contractNumber)
    {   
        $user = auth()->user();
        $filter = $this->getFilterSetting();
        $teamcode = $user->userInfo->team->team_code;
        $like = $teamcode == 'dev' ? "%004%" : ['%001%','%002%','%003%'];
        $emp_id = $user->emp_id;
        $user_id = $user->userid;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        if($user->role == 1 || (in_array($teamcode, ['acc', 'sp', 'dev', 'con']) !== false && in_array($user->emp_id, $managers) !== false ))
        {
            if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                             ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                             ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                             ->leftjoin('leads','prospects.lead_id','=','leads.id')
                             ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                             ->where('accounts.active_flag',1)
                             ->where(function($query) {
                                $query->orWhere('accounts.nature', 'like', '%003%');
                                $query->orWhere('accounts.nature', 'like', '%008%');
                                $query->orWhere('accounts.nature', 'like', '%009%');
                                $query->orWhere('accounts.active_flag', 1);
                             })
                             ->whereNull('accounts.delete_flag')
                             ->orderBy('leads.company','asc')
                             ->paginate(self::paginatePerPage());
            }

            return $query->select('accounts.*')
                        ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                        ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                        ->leftjoin('leads','prospects.lead_id','=','leads.id')
                         ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                            $query->where('accounts.created_id', $filter->person_filter);
                         })
                         ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                            $query->where('accounts.acc_assigned', $filter->person_filter);
                         })
                         ->when($filter->type_filter == 'contract_number', function($query) use ($filter) {
                            $query->where('accounts.contract_id', $filter->person_filter);
                         })
                         ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                         ->where('accounts.active_flag', 1) 
                         ->orderBy('leads.company', 'asc')
                         ->paginate(self::paginatePerPage());
        }
        else if($user->role == 4 && ($teamcode == 'acc' || $teamcode == 'sp' || $teamcode == 'con'))
        {
            if($teamcode == 'acc')
            {
                return $query->select('accounts.*')
                            ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                            ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                            ->leftjoin('leads','prospects.lead_id','=','leads.id')
                            ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag', 1)
                             ->where(function($query) use ($user_id, $emp_id) {
                                $query->orWhere('accounts.acc_assigned', $emp_id);
                                $query->orWhere('accounts.acc_assistant', $emp_id);
                                $query->orWhere('accounts.created_id', $user_id);
                                $query->orWhere('accounts.active_flag', 1);
                             })
                             ->orderBy('leads.company', 'asc')
                             ->paginate(self::paginatePerPage());
            }
            else if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                            ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                            ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                            ->leftjoin('leads','prospects.lead_id','=','leads.id')
                            ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                            ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                            ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                            ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->where(function($query) use ($emp_id) {
                                $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                                $query->orWhere('accounts.active_flag', 1);
                            })
                            ->orderBy('leads.company','asc')
                            ->paginate(self::paginatePerPage());
            }
            else
            {
                return $query->select('accounts.*')
                    ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                    ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                    ->leftjoin('leads','prospects.lead_id','=','leads.id')
                    ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                    ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                    ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                    ->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                    ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where(function($query) use ($emp_id) {
                        $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.seo_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id);
                        $query->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id);
                        $query->orWhere('accounts.active_flag', 1);
                    })
                    ->orderBy('leads.company','asc')
                    ->paginate(self::paginatePerPage());
            }
        }
        else if($user->role == 2 && in_array($user->emp_id, $managers))
        {
            return $query->select('accounts.*')
                        ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                        ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                        ->leftjoin('leads','prospects.lead_id','=','leads.id')
                        ->where('accounts.nature','like', $like)
                        ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag',1)
                        ->orderBy('leads.company', 'asc')
                        ->paginate(self::paginatePerPage());
        }
        else
        {
            return $query->select('accounts.*')
                    ->leftjoin('contracts', 'accounts.contract_id', 'contracts.id')
                    ->leftjoin('prospects','contracts.prospect_id','=','prospects.id')
                    ->leftjoin('leads','prospects.lead_id','=','leads.id')
                    ->where('contracts.contract_number','like','%'.$contractNumber.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where('accounts.created_id',$user->userid)
                    ->orderBy('leads.company','asc')
                    ->paginate(self::paginatePerPage());
        }
    }
    
    public function scopeFindHolder($query,$holder)
    {
        $user = auth()->user();
        $filter = $this->getFilterSetting();
        $teamcode = $user->userInfo->team->team_code;
        $like = $teamcode == 'dev' ? "%004%" : ['%001%','%002%','%003%'];
        $emp_id = $user->emp_id;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
        
        if($user->role == 1 || (in_array($teamcode, ['acc', 'sp', 'dev', 'con']) !== false && in_array($user->emp_id, $managers) !== false ))
        {
            if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                             ->leftjoin('users', 'accounts.created_id', 'users.userid')
                             ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                             ->where('employees.f_name','like','%'.$holder.'%')
                             ->orWhere('employees.l_name','like','%'.$holder.'%')
                             ->where('accounts.nature', 'like', '%003%')
                             ->orWhere('accounts.nature', 'like', '%008%')
                             ->orWhere('accounts.nature', 'like', '%009%')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->orderBy('employees.f_name','asc')
                             ->paginate(self::paginatePerPage());
            }

            return $query->select('accounts.*')
                         ->leftjoin('users', 'accounts.created_id', 'users.userid')
                         ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                         ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                            $query->where('accounts.created_id', $filter->person_filter);
                         })
                         ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                            $query->where('accounts.acc_assigned', $filter->person_filter);
                         })
                         ->where('employees.f_name', 'like', '%'.$holder.'%')
                         ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                         ->where('accounts.active_flag', 1)
                         ->orderBy('employees.f_name','asc')
                         ->paginate(self::paginatePerPage());
        }
        else if($user->role == 4 && ($teamcode == 'acc' || $teamcode == 'sp' || $teamcode == 'con'))
        {
            if($teamcode == 'acc')
            {
                return $query->select('accounts.*')
                            ->leftjoin('users', 'accounts.created_id', 'users.userid')
                            ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                            ->where('employees.f_name', 'like', '%'.$holder.'%')
                            ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                             ->where('accounts.acc_assigned', $emp_id)
                             ->orWhere('accounts.acc_assistant', $emp_id)
                             ->where('accounts.created_id', $user->userid)
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag', 1)
                             ->orderBy('employees.f_name','asc')
                             ->paginate(self::paginatePerPage());
            }
            else if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                            ->leftjoin('users', 'accounts.created_id', 'users.userid')
                            ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                            ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                            ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                            ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                            ->where('employees.f_name', 'like', '%'.$holder.'%')
                            ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                            ->where('accounts_seo.seo_content_team','like','%'.$emp_id.'%')
                            ->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%')
                            ->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->orderBy('employees.f_name','asc')
                            ->paginate(self::paginatePerPage());
            }
            else
            {
                return $query
                    ->leftjoin('users', 'accounts.created_id', 'users.userid')
                    ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                    ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                    ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                    ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                    ->leftjoin('accounts_webdev','accounts.web_id','accounts_webdev.id')
                    ->where('employees.f_name', 'like', '%'.$holder.'%')
                    ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                    ->where('accounts_ppc.sem_strategist_assigned', $emp_id)
                    ->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id)
                    ->orWhere('accounts_fb.fb_strategist_assigned', $emp_id)
                    ->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.seo_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id)
                    ->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id)
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->orderBy('employees.f_name','asc')
                    ->paginate(self::paginatePerPage());
            }
        }
        else if($user->role == 2 && in_array($user->emp_id, $managers))
        {
            return $query->select('accounts.*')
                        ->leftjoin('users', 'accounts.created_id', 'users.userid')
                        ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                        ->where('accounts.nature','like', $like)
                        ->where('employees.f_name', 'like', '%'.$holder.'%')
                        ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag',1)
                        ->orderBy('employees.f_name','asc')
                        ->paginate(self::paginatePerPage());
        }
        else
        {
            return $query->select('accounts.*')
                    ->leftjoin('users', 'accounts.created_id', 'users.userid')
                    ->leftjoin('employees', 'users.emp_id', 'employees.emp_id')
                    ->where('employees.f_name', 'like', '%'.$holder.'%')
                    ->orWhere('employees.l_name', 'like', '%'.$holder.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where('accounts.created_id',$user->userid)
                    ->orderBy('employees.f_name','asc')
                    ->paginate(self::paginatePerPage());
        }

    }
    
    public function scopeFindStatus($query,$status)
    {
        $user = auth()->user();

        $teamcode = auth()->user()->userInfo->team->team_code;
        $like = $teamcode == 'dev' ? "%004%" : ['%001%','%002%','%003%'];
        $emp_id = auth()->user()->emp_id;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        if($user->role == 1 || (in_array($teamcode, ['acc', 'sp', 'dev', 'con']) !== false && in_array($user->emp_id, $managers) !== false ))
        {
            if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                            ->join('system','accounts.status','=','system.systemcode')
                            ->where('system.systemdesc','like','%'.$status.'%')
                             ->where('accounts.nature', 'like', '%003%')
                             ->orWhere('accounts.nature', 'like', '%008%')
                             ->orWhere('accounts.nature', 'like', '%009%')
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag',1)
                             ->orderBy('system.systemdesc','asc')
                             ->paginate(self::paginatePerPage());
            }

            return $query->select('accounts.*')
                         ->join('system','accounts.status','=','system.systemcode')
                         ->where('system.systemdesc','like','%'.$status.'%')
                         ->where('accounts.active_flag', 1)
                         ->orderBy('system.systemdesc','asc')
                         ->paginate(self::paginatePerPage());
        }
        else if($user->role == 4 && ($teamcode == 'acc' || $teamcode == 'sp' || $teamcode == 'con'))
        {
            if($teamcode == 'acc')
            {
                return $query->select('accounts.*')
                            ->join('system','accounts.status','=','system.systemcode')
                            ->where('system.systemdesc','like','%'.$status.'%')
                             ->where('accounts.acc_assigned', $emp_id)
                             ->orWhere('accounts.acc_assistant', $emp_id)
                             ->where('accounts.created_id', $user->userid)
                             ->whereNull('accounts.delete_flag')
                             ->where('accounts.active_flag', 1)
                             ->orderBy('system.systemdesc','asc')
                             ->paginate(self::paginatePerPage());
            }
            else if($teamcode == 'con')
            {
                return $query->select('accounts.*')
                            ->join('system','accounts.status','=','system.systemcode')
                            ->join('accounts_seo','accounts.seo_id','accounts_seo.id')
                            ->join('accounts_blog','accounts.blog_id','accounts_blog.id')
                            ->join('accounts_social','accounts.social_id','accounts_social.id')
                            ->where('system.systemdesc','like','%'.$status.'%')
                            ->where('accounts_seo.seo_content_team','like','%'.$emp_id.'%')
                            ->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%')
                            ->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%')
                            ->whereNull('accounts.delete_flag')
                            ->where('accounts.active_flag',1)
                            ->orderBy('system.systemdesc','asc')
                            ->paginate(self::paginatePerPage());
            }
            else
            {
                return $query->select('accounts.*')
                    ->join('system','accounts.status','=','system.systemcode')
                    ->join('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                    ->join('accounts_seo','accounts.seo_id','accounts_seo.id')
                    ->join('accounts_fb','accounts.fb_id','accounts_fb.id')
                    ->join('accounts_webdev','accounts.web_id','accounts_webdev.id')
                    ->where('system.systemdesc','like','%'.$status.'%')
                    ->where('accounts_ppc.sem_strategist_assigned', $emp_id)
                    ->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id)
                    ->orWhere('accounts_fb.fb_strategist_assigned', $emp_id)
                    ->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.seo_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.asst_seo_strategist_assigned', $emp_id)
                    ->orWhere('accounts_seo.seo_link_builder_assigned', $emp_id)
                    ->orWhere('accounts_seo.asst_seo_link_builder_assigned', $emp_id)
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->orderBy('system.systemdesc','asc')
                    ->paginate(self::paginatePerPage());
            }
        }
        else if($user->role == 2 && in_array($user->emp_id, $managers))
        {
            return $query->select('accounts.*')
                        ->join('system','accounts.status','=','system.systemcode')
                        ->where('accounts.nature','like', $like)
                        ->where('system.systemdesc','like','%'.$status.'%')
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag',1)
                        ->orderBy('system.systemdesc','asc')
                        ->paginate(self::paginatePerPage());
        }
        else
        {
            return $query->select('accounts.*')
                    ->join('system','accounts.status','=','system.systemcode')
                    ->where('system.systemdesc','like','%'.$status.'%')
                    ->whereNull('accounts.delete_flag')
                    ->where('accounts.active_flag',1)
                    ->where('accounts.created_id',$user->userid)
                    ->orderBy('system.systemdesc','asc')
                    ->paginate(self::paginatePerPage());
        }
   
    }

    public function scopeListAccounts($query, $team)
    {
        $emp_id = Auth::user()->UserInfo->emp_id;

        $pagination = PaginationPerPage::where('table_name','accounts')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($pagination->per_page) ? $pagination->per_page : '10';

        switch($team) {
            case 'seo' :   
                $package = 'accounts_seo.seo_strategist_assigned';
                $package2 = 'accounts_seo.asst_strategist_assigned';
                $table   = 'accounts_seo';
                $original = 'accounts.seo_id';
                $foreign = 'accounts_seo.id';
                break;
            case 'web' :
                $package = 'accounts_webdev.dev_team';
                $table   = 'accounts_webdev';
                $original = 'accounts.web_id';
                $foreign = 'accounts_webdev.id';
                break;
        }

        if ($team == 'seo')
        {
            return $query->select('accounts.*')
                        ->join($table,$original,$foreign)
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag', 1)
                        ->where(function($query) use ($package, $package2, $emp_id) {
                            $query->orWhere($package, $emp_id);
                            $query->orWhere($package2, $emp_id);
                        })
                        ->orderBy('accounts.updated_at', 'desc')
                        ->paginate(self::paginatePerPage());

        } 
        else if($team == 'web')
        {
            return $query->select('accounts.*')
                        ->join($table,$original,$foreign)
                        ->whereNull('accounts.delete_flag')
                        ->where('accounts.active_flag', 1)
                        ->where($package, $emp_id)
                        ->orderBy('accounts.updated_at', 'desc')
                        ->paginate(self::paginatePerPage());
        }

        return $query->select('accounts.*')
                     ->leftjoin('accounts_ppc','accounts.ppc_id','accounts_ppc.id')
                     ->leftjoin('accounts_fb','accounts.fb_id','accounts_fb.id')
                     ->whereNull('accounts.delete_flag')
                     ->where('accounts.active_flag', 1)
                     ->where(function($query) use ($emp_id){
                        $query->orWhere('accounts_ppc.sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_ppc.asst_sem_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts_fb.asst_fb_strategist_assigned', $emp_id);
                        $query->orWhere('accounts.active_flag', 1);
                     })
                    ->orderBy('accounts.updated_at', 'desc')
                    ->paginate(self::paginatePerPage());

    }

    public function scopeListAccountsForACCmembers($query)
    {   
        $user = Auth::user();
        $emp_id = $user->userInfo->emp_id;
        $user_id = $user->userid;

        return $query->where('active_flag', 1)
                    ->whereNull('deleted_at')
                    ->where(function($query) use ($user_id, $emp_id) {
                        $query->orWhere('acc_assigned', $emp_id);
                        $query->orWhere('acc_assistant', $emp_id);
                        $query->orWhere('created_id', $user_id);
                    })
                    ->orderBy('updated_at','desc')
                    ->paginate(self::paginatePerPage());
    }

    public function scopeListAccountsForContent($query)
    {   
        $emp_id = Auth::user()->userInfo->emp_id;

        return $query->select('accounts.*')
                    ->leftjoin('accounts_seo','accounts.seo_id','accounts_seo.id')
                    ->leftjoin('accounts_blog','accounts.blog_id','accounts_blog.id')
                    ->leftjoin('accounts_social','accounts.social_id','accounts_social.id')
                    ->whereNull('deleted_at')
                    ->where('active_flag', 1)
                    ->where(function($query) use ($emp_id) {
                        $query->orWhere('accounts_seo.seo_content_team','like','%'.$emp_id.'%');
                        $query->orWhere('accounts_blog.blog_content_team','like','%'.$emp_id.'%');
                        $query->orWhere('accounts_social.social_content_team','like','%'.$emp_id.'%');
                        $query->orWhere('accounts.active_flag', 1);
                    })
                    ->orderBy('updated_at','desc')
                    ->paginate(self::paginatePerPage());
    }

    public function scopeSalesLeads($query,$owner)
    {
        return $query->select('accounts.*')
                     ->join('prospects','accounts.prospect_id','prospects.id')
                     ->join('leads','prospects.lead_id','leads.id')
                     ->where('accounts.created_id',$owner)
                     ->where('accounts.active_flag', 1)
                     ->whereNull('accounts.delete_flag')
                     ->orderBy('accounts.updated_at','desc')
                     ->paginate(self::paginatePerPage());
    }

    public function scopeFinanceView($query)
    {
        

        return $query->select('accounts.*')
                     ->where('accounts.active_flag', 1)
                     ->orderBy('accounts.updated_at','desc')
                     ->paginate(self::paginatePerPage());
    }

    public function scopeAccList($query)
    {
        return $query->select('accounts.id', 'contracts.company', 'contracts.contract_number')
                     ->join('contracts', 'accounts.contract_id', 'contracts.id')
                     ->where('accounts.active_flag', 1)
                     ->orderBy('accounts.updated_at', 'desc')
                     ->get();
    }
}
