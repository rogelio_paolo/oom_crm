<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Lead;
use App\Prospect;
use App\Employee;

class LeadConvertedProspectNotification extends Notification
{
    use Queueable;

    public $prospect;
    public $lead;
    public $employee;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Prospect $prospect, Lead $lead, Employee $employee)
    {
        $this->prospect = $prospect;
        $this->lead = $lead;
        $this->employee = $employee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'prospect'  => $this->prospect,
            'lead'      => $this->lead,
            'employee'  => $this->employee
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'prospect'  => $this->prospect,
                'lead'      => $this->lead,
                'employee'  => $this->employee
            ]
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
