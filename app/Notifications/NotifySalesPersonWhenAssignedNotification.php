<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Account;
use App\Employee;
use App\Contract;


class NotifySalesPersonWhenAssignedNotification extends Notification
{
    use Queueable;

    public $account;
    public $employee;
    public $contract;
    public $members;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Employee $employee, Account $account, Contract $contract, $members, $nature)
    {
        $this->account = $account;
        $this->employee = $employee;
        $this->contract = $contract;
        $this->members = $members;
        $this->nature = $nature;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'employee' => $this->employee,
            'account'  => $this->account,
            'contract' => $this->contract,
            'members'  => $this->members,
            'nature'   => $this->nature
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'employee' => $this->employee,
                'account'  => $this->account,
                'contract' => $this->contract,
                'members'  => $this->members,
                'nature'   => $this->nature
            ]
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
