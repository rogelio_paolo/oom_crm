<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Account;
use App\Prospect;
use App\Employee;
use App\Lead;
use App\Team;

class ProspectConvertedAccountNotification extends Notification
{
    use Queueable;

    public $account;
    public $prospect;
    public $lead;
    public $employee;
    public $team;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Account $account, Prospect $prospect, Lead $lead, Employee $employee, Team $team)
    {
        $this->account = $account;
        $this->prospect = $prospect;
        $this->lead = $lead;
        $this->employee = $employee;
        $this->team = $team;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'account'   => $this->account,
            'prospect'  => $this->prospect,
            'lead'      => $this->lead,
            'employee'  => $this->employee,
            'team'      => $this->team
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'account'   => $this->account,
                'prospect'  => $this->prospect,
                'lead'      => $this->lead,
                'employee'  => $this->employee,
                'team'      => $this->team
            ]
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
