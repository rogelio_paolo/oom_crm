<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Prospect;
use App\Account;
use App\Reminder;
use App\Lead;

class AccountReminderNotification extends Notification
{
    use Queueable;

    public $account;
    public $prospect;
    public $lead;
    public $reminder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Account $account, Prospect $prospect, Lead $lead, Reminder $reminder)
    {
        $this->account = $account;
        $this->prospect = $prospect;
        $this->lead = $lead;
        $this->reminder = $reminder;
    }

    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'account' => $this->account,
            'prospect' => $this->prospect,
            'lead'  => $this->lead,
            'reminder' => $this->reminder
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'account' => $this->account,
                'prospect'    => $this->prospect,
                'lead'  => $this->lead,
                'reminder' => $this->reminder
            ]
        ];
    }
}
