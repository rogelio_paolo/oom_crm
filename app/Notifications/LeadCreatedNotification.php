<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Lead;
use App\Employee;

class LeadCreatedNotification extends Notification
{
    use Queueable;

    public $lead;
    public $employee;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lead $lead, Employee $employee)
    {
        $this->lead = $lead;
        $this->employee = $employee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     * 
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'lead' => $this->lead,
            'employee' => $this->employee
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'lead' => $this->lead,
                'employee' => $this->employee
            ]
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
