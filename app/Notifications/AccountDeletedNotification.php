<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Prospect;
use App\Account;
use App\Employee;
use App\Lead;

class AccountDeletedNotification extends Notification
{
    use Queueable;

    public $account;
    public $prospect;
    public $employee;
    public $lead;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Account $account, Prospect $prospect, Lead $lead, Employee $employee)
    {
        $this->account = $account;
        $this->prospect = $prospect;
        $this->lead = $lead;
        $this->employee = $employee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     * 
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'account' => $this->account,
            'prospect'    => $this->prospect,
            'lead'  => $this->lead,
            'employee'    => $this->employee
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'account' => $this->account,
                'prospect'    => $this->prospect,
                'lead'    => $this->lead,
                'employee'    => $this->employee
            ]
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
