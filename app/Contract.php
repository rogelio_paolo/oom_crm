<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contracts';

    protected $fillable = [
        'prospect_id',
        'account_id',
        'contract_number',
        'is_active',
        'currency',
        'contract_value',
        'company',
        'f_name',
        'l_name',
        'designation',
        'email',
        'contact_number',
        'profit',
        'day_90',
        'day_120',
        'created_by',
        'created_at',
        'updated_at',
        'delete_flag',
        'deleted_at',
        'contract_type'
    ];

    protected $dates = ['day_90', 'day_120'];

    public function prospect()
    {
        return $this->hasMany('App\Prospect', 'id', 'prospect_id');
    }

    public function account()
    {
        return $this->hasOne('App\Account', 'id', 'account_id');
    }

    public function contractType()
    {
        return $this->hasOne('App\System', 'systemcode', 'contract_type');
    }

    public function scopeContractNumber($query)
    {
        return $query->select('contracts.id', 'contracts.contract_number')
                     ->orderBy('contracts.created_at', 'desc')
                     ->get();
    }
}
