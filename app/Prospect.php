<?php

namespace App;

use App\PaginationPerPage;

use Illuminate\Database\Eloquent\Model;


class Prospect extends Model
{
    protected $table = 'prospects';

    protected $fillable = [
        'lead_id',
        'status',
        'lock_this_prospect',
        'created_by',
        'delete_flag',
        'deleted_at',
        'updated_by',
        'ex_client_flag',
        'active_flag'
    ];

    public function opportunity() {
        return $this->belongsTo('App\Opportunity', 'prospect_id', 'id');
    }

    public function contract() {
        return $this->hasMany('App\Contract', 'prospect_id', 'id');
    }

    public function lead()
    {
        return $this->hasOne('App\Lead','id','lead_id');
    }

    public function account()
    {
        return $this->hasOne('App\Account','prospect_id','id');
    }

    public function landing()
    {
        return $this->hasOne('App\System','systemcode','landing_page');
    }

    public function prospectStatus()
    {
        return $this->hasOne('App\System','systemcode','status');
    }

    public function sales()
    {
        return $this->hasOne('App\User','userid','created_by');
    }

    public function source_leads()
    {
        return $this->hasOne('App\System','systemcode','source');
    }

    public function scopeFindName($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1 || in_array($user->UserInfo->team->team_code,['acc','busi'] ) ) 
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                         ->join('leads','prospects.lead_id','leads.id')
                         ->where('prospects.created_by',$user->userid)
                         ->where('leads.f_name','like','%'.$key.'%')
                         ->where('prospects.active_flag',1)
                         ->whereNull('prospects.delete_flag')
                         ->paginate($perPage);
        else
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                         ->join('leads','prospects.lead_id','leads.id')
                         ->where('leads.f_name','like','%'.$key.'%')
                         ->where('prospects.active_flag',1)
                         ->whereNull('prospects.delete_flag')
                         ->paginate($perPage);
    }

    public function scopeFindCompany($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1 || in_array($user->UserInfo->team->team_code,['acc','busi'] ) ) 
           
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                         ->join('leads','prospects.lead_id','leads.id')
                         ->where('prospects.created_by',$user->userid)
                         ->where('leads.company','like','%'.$key.'%')
                         ->where('prospects.active_flag',1)
                         ->whereNull('prospects.delete_flag')
                         ->paginate($perPage);
        else 
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                         ->join('leads','prospects.lead_id','leads.id')
                         ->where('leads.company','like','%'.$key.'%')
                         ->where('prospects.active_flag',1)
                         ->whereNull('prospects.delete_flag')
                         ->paginate($perPage);
        
    }

    public function scopeFindStatus($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if ($user->role != 1 || in_array($user->UserInfo->team->team_code,['acc','busi'] ) ) 
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                        ->join('system','prospects.status','system.systemcode')
                        ->where('prospects.created_by',$user->userid)
                        ->where('system.systemdesc','like','%'.$key.'%')
                        ->where('prospects.active_flag',1)
                        ->whereNull('prospects.delete_flag')
                        ->paginate($perPage);
        else
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                     ->join('system','prospects.status','system.systemcode')
                     ->where('system.systemdesc','like','%'.$key.'%')
                     ->where('prospects.active_flag',1)
                     ->whereNull('prospects.delete_flag')
                     ->paginate($perPage);
    }
    
    public function scopeFindOwner($query,$key)
    {
        $user = \Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        if ($user->role != 1 || in_array($user->UserInfo->team->team_code,['acc','busi'] ) ) 
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                        ->join('users','prospects.created_by','users.userid')
                        ->join('employees','users.emp_id','employees.emp_id')
                        ->where('prospects.created_by',$user->userid)
                        ->where('prospects.active_flag',1)
                        ->where('employees.f_name','like','%'.$key.'%')
                        ->orWhere('employees.l_name','like','%'.$key.'%')
                        ->paginate($perPage);
        else
            return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                        ->join('users','prospects.created_by','users.userid')
                        ->join('employees','users.emp_id','employees.emp_id')
                        ->where('prospects.active_flag',1)
                         ->where('employees.f_name','like','%'.$key.'%')
                        ->orWhere('employees.l_name','like','%'.$key.'%')
                        ->paginate($perPage);
    }

    public function scopeSort($query,$field,$order)
    {
        $user = auth()->user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $teamcode = $user->userInfo->team->team_code;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        if ($user->role == 1 || ($teamcode == 'acc' && in_array($user->emp_id, $managers)) )
        {   
            if ($field == 'company')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->where('prospects.active_flag',1)
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('leads.company',$order)
                             ->paginate($perPage);
            else if($field == 'created_by')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                            ->join('users','prospects.created_by','users.userid')
                            ->join('employees','users.emp_id','employees.emp_id')
                            ->join('leads','prospects.lead_id','leads.id')
                            ->where('prospects.active_flag',1)
                            ->whereNull('prospects.delete_flag')
                            ->where('prospects.status','!=','LS005')
                            ->orderBy('employees.f_name',$order)
                            ->paginate($perPage);
            else if($field == 'status')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->join('system','prospects.status','system.systemcode')
                             ->where('prospects.active_flag',1)
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('system.systemdesc',$order)
                             ->paginate($perPage);
            else if($field == 'ex_client_flag')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                            ->join('leads','prospects.lead_id','leads.id')
                            ->where('prospects.active_flag',1)
                            ->whereNull('prospects.delete_flag')
                            ->where('prospects.status','!=','LS005')
                            ->orderBy('ex_client_flag',$order)
                            ->paginate($perPage);
            else if($field == 'lock_this_prospect')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                        ->join('leads','prospects.lead_id','leads.id')
                        ->where('prospects.active_flag',1)
                        ->whereNull('prospects.delete_flag')
                        ->where('prospects.status','!=','LS005')
                        ->orderBy('prospects.lock_this_prospect', $order)
                        ->paginate($perPage);
            else
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->where('prospects.active_flag',1)
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('prospects.updated_at',$order)
                             ->paginate($perPage);
        } else if ($teamcode == 'acc' || $teamcode == 'busi')
        {
            if ($field == 'company')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->where('prospects.active_flag',1)
                             ->where('prospects.created_by',$user->userid)
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('leads.company',$order)
                             ->paginate($perPage);
            else if($field == 'status')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->join('system','prospects.status','system.systemcode')
                             ->where('prospects.created_by',$user->userid)
                             ->where('prospects.active_flag',1)
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('system.systemdesc',$order)
                             ->paginate($perPage);
            else if($field == 'ex_client_flag')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                            ->join('leads','prospects.lead_id','leads.id')
                            ->whereNull('prospects.delete_flag')
                            ->where('prospects.active_flag',1)
                            ->where('prospects.status','!=','LS005')
                            ->orderBy('ex_client_flag',$order)
                            ->paginate($perPage);
            else if($field == 'lock_this_prospect')
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                    ->join('leads','prospects.lead_id','leads.id')
                    ->where('prospects.active_flag',1)
                    ->whereNull('prospects.delete_flag')
                    ->where('prospects.status','!=','LS005')
                    ->orderBy('prospects.lock_this_prospect',$order)
                    ->paginate($perPage);
            else
                return $query->select('leads.company','leads.f_name','leads.l_name','leads.services','leads.other_services','prospects.*')
                             ->join('leads','prospects.lead_id','leads.id')
                             ->whereNull('prospects.delete_flag')
                             ->where('prospects.created_by',$user->userid)
                             ->where('prospects.active_flag',1)
                             ->where('prospects.status','!=','LS005')
                             ->orderBy('prospects.updated_at',$order)
                             ->paginate($perPage);
        }

    //     $user = \Auth::user();
        
    //     if ($user->role != 1)
    //         if ($field != 'status')
    //             return $query->where('created_by',$user->userid)->whereNull('delete_flag')->orderBy($field,$order)->paginate(5);
    //         else
    //             return $query->join('system','prospects.status','system.systemcode')
    //                         ->join('leads','prospects.lead_id','leads.id')
    //                         ->where('created_by',$user->userid)
    //                         ->whereNull('delete_flag')
    //                         ->orderBy('system.systemdesc',$order)
    //                         ->paginate(5);
    //     else
    //         if ($field == 'created_by')
    //             return $query->join('users','prospects.created_by','users.userid')
    //                             ->join('employees','users.emp_id','employees.emp_id')
    //                             ->join('leads','prospects.lead_id','leads.id')
    //                             ->whereNull('delete_flag')
    //                             ->orderBy('employees.f_name',$order)
    //                             ->orderBy('employees.l_name',$order)
    //                             ->paginate(5);
    //         elseif ($field != 'status')
    //             return $query
    //             ->whereNull('delete_flag')
    //             ->orderBy($field,$order)->paginate(5);
    //         else
    //             return $query->join('system','prospects.status','system.systemcode')
    //                         ->whereNull('delete_flag')
    //                         ->orderBy('system.systemdesc',$order)
    //                         ->paginate(5);
    }

    // public function scopeProspectAccountCompare($query,$key)
    // {
    //     return $query->select('accounts.prospect_id')
    //                  ->join('accounts','prospects.id','accounts.prospect_id')
    //                  ->where('accounts.prospect_id','like','%'.$key.'%')
    //                  ->get();
    // }
}
