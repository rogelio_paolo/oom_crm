<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModifyAccountsColumn extends Model
{
    protected $table = 'modify_accounts_column';

    protected $fillable = [
        'userid',
        'company_name',
        'date_created',
        'contract_number',
        'day_90',
        'day_120',
        'nature',
        'account_holder',
        'sp_assigned',
        'busi_manager',
        'am_fee',
        'asst_account_holder',
        'campaign_start',
        'campaign_end',
        'budget'
    ];

    public $timestamps = false;

    public function userInfo()
    {
        return $this->hasOne('App/User', 'userid', 'userid');
    }
}
