<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $table = 'reminders';

    protected $fillable = [
        'reminderid',
        'name',
        'date',
        'receivers',
        'remark',
        'createdid',
        'accountid'
    ];

    public function reminderDetails() {
        return $this->hasOne('App\System', 'systemcode', 'time');
    }

    public function reminderOtherDetails() {
        return $this->hasOne('App\System', 'systemcode', 'time_other');
    }
}
