<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    protected $table = 'audittrails';

    public function user()
    {
        return $this->hasOne('App\User','userid','userid');
    }

    public function sysmodule()
    {
        return $this->hasOne('App\System','systemcode','module')->where('system.systemtype','systemmodule');
    }

    public function sysaction()
    {
        return $this->hasOne('App\System','systemcode','action');
    }
}
