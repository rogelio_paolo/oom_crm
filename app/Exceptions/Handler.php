<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException as AuthException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof \Illuminate\Session\TokenMismatchException) {    
            return 
                    redirect()
                    ->route('user.login')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Unauthorized user, please login first.',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
       
        }
        
        // if($this->isHttpException($e))
        // {
        //     switch ($e->getStatusCode())
        //         {
        //         case '401':
        //             $action = redirect()->route('error.render','401');
        //         break;

        //         case '403':
		// 			$action = redirect()->route('error.render','403');
		// 		break;

        //         case '404':
		// 			$action = redirect()->route('error.render','404');
        //         break;

		// 		case '405':
		// 			$action = redirect()->route('error.render','405');
		// 		break;

        //         case '500':
        //             $action = redirect()->route('error.render','500');
        //         break;

        //         case '503':
        //             $action = redirect()->route('error.render','503');
        //         break;

        //         default:
		// 			$action = $this->renderHttpException($e);
        //         break;
        //     }
        //     return $action;
        // }

        return parent::render($request, $e);
    }

    public function unauthenticated($request, AuthException $exception)
    {
        $route = \Request::route()->getName();
        
        if ($route == 'account.edit.assign' || $route == 'account.view'){
            \Session::put('redirectURL',request()->url());
            return 
                redirect()
                ->route('user.login');
        }

        return 
                redirect()
                ->route('user.login')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Unauthorized user, please login first',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }
}
