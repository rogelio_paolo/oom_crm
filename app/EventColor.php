<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventColor extends Model
{
    protected $table = 'event_colors';

    public $timestamps = false;

    protected $fillable = [
        'hex_value', 'name'
    ];
}
