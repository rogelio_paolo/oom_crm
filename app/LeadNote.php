<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadNote extends Model
{
    protected $table = 'lead_notes';

    protected $fillable = [
      'lead_id',
      'prospect_id',
      'note',
      'userid'
    ];

    public function user()
    {
        return $this->hasOne('App\User','userid','userid');
    }

    public function lead()
    {
        return $this->hasOne('App\Lead','id','lead_id');
    }

    public function prospect()
    {
        return $this->hasOne('App\Prospect','lead_id','lead_id');
    }

    public function performance()
    {
        return $this->belongsTo('App\SalesPerformance', 'id', 'lead_id');
    }
}
