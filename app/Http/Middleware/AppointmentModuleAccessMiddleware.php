<?php

namespace App\Http\Middleware;

use Closure;

class AppointmentModuleAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        
        if (in_array($user->role, [1,2]) || in_array($user->userInfo->team->team_code, ['busi','acc']))
            return $next($request);

        return
            redirect()
            ->route('user.dashboard')
            ->with([
                'alert'   => 'error',
                'message' => 'You are not allowed to access appointments module',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
    }
}
