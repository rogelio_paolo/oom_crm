<?php

namespace App\Http\Middleware;

use Closure;

class AccountsFileViewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);

        $response->headers->set('Last-Modified', new DateTime("now"));
        $response->headers->set('Expires', new DateTime("tomorrow"));
        
        return $response;
    }
}
