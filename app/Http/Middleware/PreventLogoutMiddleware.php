<?php

namespace App\Http\Middleware;

use Closure;

class PreventLogoutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);

        return $response->header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0")
                        ->header("Cache-Control: post-check=0, pre-check=0", false)
                        ->header("Pragma: no-cache");
    }
}
