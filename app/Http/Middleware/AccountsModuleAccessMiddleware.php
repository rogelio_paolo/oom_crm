<?php

namespace App\Http\Middleware;

use Closure;

class AccountsModuleAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        //if ($user->role != 3)
            return $next($request);

        // return
        //     redirect()
        //     ->route('user.dashboard')
        //     ->with([
        //         'alert'   => 'error',
        //         'message' => 'You are not allowed to access accounts module',
        //         'button'  => 'OK',
        //         'confirm' => 'true'
        //     ]);
    }
}
