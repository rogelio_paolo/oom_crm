<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Notification;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function fetch()
    {   
            // $notification = Auth::user()->unreadNotifications()->where('type','not like','%AccountReminder%')
            //                             ->whereNull('is_cleared')
            //                             ->where('notifiable_id', Auth::user()->id)
            //                             ->get();
            $auth = Auth::user();
            
            $notifications = \DB::select('select 
                                        a.*,
                                        b.notifs
                                    from 
                                        notifications a,
                                        (select 
                                        count(case when read_at is null then 1 else 0 end) as notifs
                                        from
                                        notifications
                                        where 
                                        notifiable_id = "'.$auth->id.'" 
                                        and 
                                        read_at is null 
                                        and 
                                        type not like "%AccountReminder%"
                                        and 
                                        is_cleared is null
                                        ) b
                                    where 
                                        notifiable_id = "'.$auth->id.'"
                                        and 
                                        is_cleared is null
                                        and 
                                        type not like "%AccountReminder%"
                                    ');
            
            return $notifications;

    }

    public function fetchReminders()
    {
        $auth = Auth::user();
        $reminders = \DB::select('select 
                                        a.*,
                                        b.notifs
                                    from 
                                        notifications a,
                                        (select 
                                        count(case when read_at is null then 1 else 0 end) as notifs
                                        from
                                        notifications
                                        where 
                                        notifiable_id = "'.$auth->id.'" 
                                        and 
                                        read_at is null 
                                        and 
                                        type like "%AccountReminder%"
                                        and 
                                        is_cleared is null
                                        ) b
                                    where 
                                        notifiable_id = "'.$auth->id.'"
                                        and 
                                        is_cleared is null
                                        and
                                        type like "%AccountReminder%"
                                    ');
            
        return $reminders;
    }

    public function read(Request $request)
    {
        $notif = Notification::find($request->id);
        $notif->read_at = $notif->read_at != null ? $notif->read_at : Carbon::now();
        $notif->save();

        return 0;
    }

    public function fetchAll()
    {
        $notification = Auth::user()->Notifications()->where('type','not like','%AccountReminder%')->get();

        return $notification;
    }

    public function list()
    {
        $notifications = Notification::where('notifiable_id',Auth::user()->id)
                                     ->where('type','not like','%AccountReminder%')
                                     ->orderBy('created_at','desc')->get();
        return view('notification.list')->with(compact('notifications'));
    }

    public function clear()
    {
        $notifications = Notification::where('type','not like','%AccountReminder%')
                                     ->where('notifiable_id', Auth::user()->id)
                                     ->update(['is_cleared' => 1 ]);
        return redirect()->back();
    }

    public function reminderClear()
    {
        $reminders = Auth::user()->unreadNotifications()->where('type','like','%AccountReminder%')
                                                        ->update(['is_cleared' => 1 ]);
        return redirect()->back();
    }

}
