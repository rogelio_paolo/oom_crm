<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Appointment;
use App\Lead;
use App\System;
use App\Manager;
use App\User;
use App\EventColor;

use App\Notifications\AppointmentUpdatedNotification;
use App\Notifications\AppointmentCreatedNotification;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class AppointmentManagementController extends Controller
{   
    public function list()
    {
        $user = Auth::user();

        $user = Auth::user();
        $creator = strtoupper(substr($user->userInfo->f_name, 0, 3));
     
        $team = $user->userInfo->team->team_code;
        $managers_list = Manager::select('emp_id')->get();
        $emails = User::emailRecipientName();
        $event_colors = EventColor::get();

        $managers = array_flatten($managers_list->toArray());

        if($user->role == 1 || ($team == 'acc' && in_array($managers, $user->emp_id)) )
            $companies = Lead::whereNotNull('company')->whereNull('delete_flag')->get(['id','company']);
        else 
            $companies = Lead::whereNotNull('company')->whereNull('delete_flag')->where('created_by', $user->userid)->get(['id','company']);
        
        $statuses = System::where('systemtype', 'appointment_status')->where('activeflag', 1)->get();

        $appointmentsModel = Appointment::listAppointments();
                                        
        $appointments = $this->tableAppointments($appointmentsModel);

        // For Quick Add Leads Module
        $lead_services = System::where('systemtype','service')->where('activeflag',1)->get();
        $lead_sources = System::where('systemtype','leadsource')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $lead_landingpages = System::where('systemtype','landingpage')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $lead_statuses = System::where('systemtype','leadstatus')->where('activeflag',1)->get();

        app(__('global.Audit'))->logAuditTrail("MD011","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);

        return view('appointment.list', compact('appointments', 'user', 'companies', 'statuses', 'team', 'managers', 'emails', 'lead_services', 'lead_sources', 'lead_landingpages', 'lead_statuses', 'event_colors'));
    }

    public function store(Request $request)
    {   
        $input = $request->except('_token');
        $user = Auth::user();
        // $creator = strtoupper(substr($user->userInfo->f_name, 0, 3));
        $firstName = strtoupper($user->userInfo->f_name);
        
        $team = $user->userInfo->team->team_code;

        $managers_list = Manager::select('emp_id')->get();
        $managers = array_flatten($managers_list->toArray());

        $lead = Lead::find($input['lead_id']);
        
        /* ----- GOOGLE CALENDAR SCRIPTS ----- */
        $client = (new AccountReminderController)->getClient(); // Call google calendar client from another controller Reminder
        $service = new \Google_Service_Calendar($client); // Create Calendar Object with parameter client function.

        // Google Calendar Creation on Left Panel Web UI
        $calendar = new \Google_Service_Calendar_Calendar();
        $calendar->setSummary($firstName . ': ' . $lead->company);
        $calendar->setTimeZone('Asia/Kuala_Lumpur');
        $createdCalendar = $service->calendars->insert($calendar);

        // Google Calendar List Entry Creation on Left Panel Web UI
        $calendarListEntry = new \Google_Service_Calendar_CalendarListEntry();
        $calendarListEntry->setId($createdCalendar->getId());
        $calendarListEntry->setSelected("true");
        $createdCalendarListEntry = $service->calendarList->insert($calendarListEntry); // Insert the calendar to API

         // Google Calendar List Access Control Rule
         $rule = new \Google_Service_Calendar_AclRule();
         $scope = new \Google_Service_Calendar_AclRuleScope();

         // Get all the selected guests
        if(isset($input['guest']))
        {
            $arrayGuestEmails = $input['guest'];

            foreach($arrayGuestEmails as $row)
            {
                $guests[] = $row;
            }
        }
        // Push array of own email
        $guests[] = $user->email;
        $guests[] = 'iwbappts@gmail.com';

        // List event attendees
        for($i = 0; $i < count($guests); $i++)
        {
            $scope->setType("user");
            $scope->setValue($guests[$i]);
            $rule->setScope($scope);
            $rule->setRole('owner');
            $createdRule = $service->acl->insert($createdCalendarListEntry->getId(), $rule); // Activate Access Control Rule
        }

        $event = new \Google_Service_Calendar_Event(); // Create Calendar Appointment Object

        // Set reminder name and remarks amd color
        $event->setSummary($firstName . ': ' . $lead->company);
        $event->setDescription(
            (isset($lead->f_name) || isset($lead->l_name) ? 'Contact Person: ' . $lead->f_name.' '.$lead->l_name . '<br/>' : '') . (isset($input['description']) ? 'Description: ' . nl2br($input['description']) : '')
        );

        // Create Calendar Appointment Date/Time Object
        $format = 'Y-m-d\TH:i:sP';

        $start_date = new \Google_Service_Calendar_EventDateTime();
        $start = new \DateTime($input['date_start']);
        $start_date->setDateTime($start->format($format)); // Appointment Date Start

        $end_date = new \Google_Service_Calendar_EventDateTime();
        $end = new \DateTime($input['date_end']);
        $end_date->setDateTime($end->format($format)); // Appointment Date End

        $creator = new \Google_Service_Calendar_EventCreator();
        $creator->setDisplayName($user->userInfo->f_name . ' ' . $user->userInfo->l_name);
        $creator->setEmail('info.oomcrm@gmail.com');
        $event->setCreator($creator);

        $event->setStart($start_date);
        $event->setEnd($end_date);

        // $event->getColorId(array_pluck(EventColor::get(), 'name', 'index_value'));
        $event->setColorId($input['color']);

         // List event attendees
         $attendee = new \Google_Service_Calendar_EventAttendee();
         $attendee->setEmail('info.oomcrm@gmail.com');
         $attendee->setResponseStatus('accepted');
         $attendees[] = $attendee;
         $event->setAttendees($attendees);

         // Create event organizer object
        $organizer = new \Google_Service_Calendar_EventOrganizer();  // Create appointment receiver
        $organizer->setEmail('info.oomcrm@gmail.com');
        $event->setOrganizer($organizer);

        $optParams = array('sendUpdates' => 'all');
        $results = $service->events->insert($createdCalendarListEntry->getId(), $event);

        $guestEmails = implode('|', $guests);
        
        /* ----- END OF GOOGLE CALENDAR SCRIPTS ----- */

        $appointment = Appointment::create([
            'google_calendar_id'    => $results->getId(),
            'google_calendarlist_id'=> $createdCalendarListEntry->getId(),
            'color'                 => $input['color'],
            'lead_id'               => $input['lead_id'],
            'company'               => $lead->company,
            'services'              => $lead->services,
            'title'                 => $firstName . ': ' . $lead->company,
            'client_name'           => $this->getClientName($lead->contact_number, $lead->f_name, $lead->l_name),
            'guest'                 => $guestEmails,
            'description'           => $input['description'],
            'date_start'            => $input['date_start'],
            'date_end'              => $input['date_end'],
            'created_by'            => $user->userid,
        ]);

        $appointment_model = $this->singleDataAppointment($appointment);

        $receivers = $this->receivers($appointment_model);
        
        foreach($receivers as $row)
        {
            $row->notify(new AppointmentCreatedNotification($appointment_model, $user->userInfo));
        }

        app(__('global.Audit'))->logAuditTrail("MD011","AT002","Created Appointment ". $appointment->company, null, null, request()->ip(), $user->userid, $user->role);

        return response()->json([
            'appointment'   => $appointment_model,
            'user'          => $user,
            'team'          => $team,
            'managers'      => $managers
        ], 200);
    }

    public function edit($id)
    {
        $appointment = Appointment::findOrFail($id);
        $status = System::where('systemtype', 'appointment_status')->where('activeflag', 1)->get();

        if($appointment->guest != null)
        {
            foreach(explode('|', $appointment->guest) as $guest)
            {
                $user = User::where('email', '=', $guest)->first();
                if($user)
                    $email[] = $user->userInfo->f_name.' '.$user->userinfo->l_name;
                else
                    $email[] = $guest;
            }
        }
        else
        {
            $email[] = 'None';
        }

        $appointmentModel = [
            'id'                    => $appointment->id,
            'google_calendar_id'    => $appointment->google_calendar_id,
            'google_calendarlist_id'=> $appointment->google_calendarlist_id,
            'company'               => $appointment->company,
            'title'                 => $appointment->title,
            'client_name'           => $appointment->client_name,
            'guest'                 => implode(', ', $email),
            'description'           => $appointment->description,
            'date_start'            => $appointment->date_start,
            'date_end'              => $appointment->date_end,
            'status'                => $appointment->status,
            'color'                 => $appointment->color
        ];

        return response()->json([
            'appointment' => $appointmentModel,
            'status'      => $status
        ], 200);
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $appointment = Appointment::find($input['id']);
        $user = Auth::user();
        $team = $user->userInfo->team->team_code;
        $managers_list = Manager::select('emp_id')->get();
        $managers = array_flatten($managers_list->toArray());

        $lead = Lead::find($appointment->lead_id);

        $this->updateGoogleCalendar($user, $lead->company, $input['client_name'], $input['description'], $input['date_start'], $input['date_end'], $input['status'], $appointment->guest, $appointment->google_calendar_id, $appointment->google_calendarlist_id, $input['color']);
       
        foreach($input as $k => $v)
        {
            $appointment->$k = $v;
        }

        $appointment->update();

        $appointment_model = $this->singleDataAppointment($appointment);

        $receivers = $this->receivers($appointment_model);
        
        foreach($receivers as $row)
        {
            $row->notify(new AppointmentUpdatedNotification($appointment_model, $user->userInfo));
        }

        app(__('global.Audit'))->logAuditTrail("MD011","AT005","Updated Appointment ". $appointment->company, null, null, request()->ip(), $user->userid, $user->role);
        
    
        return response()->json([
            'appointment' => $appointment_model,
            'user' => $user,
            'team' => $team,
            'managers' => $managers
        ], 200);
    }

    public function pending()
    {
        $user = Auth::user();
        $appointmentModel = Appointment::listPendingAppointments();
        $appointment = $this->tableAppointments($appointmentModel);
        return response()->json(['appointment' => $appointment, 'login_count_today' => $user->login_count_today, 'current_login_at' => $user->current_login_at], 200);
    }

    public function updatePending(Request $request) 
    {
        $input = $request->except('_token');
        $user = Auth::user();

        $appointment = Appointment::find($input['id']);
        $appointment->status = $input['status'];
        $appointment->save();

        return response()->json(['appointment' => $appointment], 200);
    }

    public function quickAddLead(Request $request) 
    {
        $input = $request->except('_token');
        $user = Auth::user();

        foreach($input as $k => $v)
        {
           $arr[$k] = $v;
        }

        $arr['created_by'] = $user->userid;

        $lead = Lead::create($arr);

        return response()->json(['lead' => $lead], 200);
    }

     /* ----- CUSTOM HELPER FUNCTIONS ----- */
     function updateGoogleCalendar($user, $company, $client_name, $description, $date_start, $date_end, $ap_status, $guest, $calendarId, $calendarListId, $color)
     {
       $firstName = strtoupper($user->userInfo->f_name);
       /* ----- GOOGLE CALENDAR SCRIPTS----- */
       $client = (new AccountReminderController)->getClient(); // Call google calendar client from another controller Reminder
       $service = new \Google_Service_Calendar($client); // Create Calendar Object with parameter client function.
       $event = new \Google_Service_Calendar_Event(); // Create Calendar Appointment Object

        //  // Google Calendar Creation on Left Panel Web UI
        $calendar = $service->calendars->get($calendarListId);

        // Google Calendar List Entry Creation on Left Panel Web UI
        $calendarListEntry = $service->calendarList->get($calendarListId);

        // Get all the selected guests
        if(!empty($guest))
        {
            $arrayGuestEmails = explode('|', $guest);
            foreach($arrayGuestEmails as $row)
            {
                $guests[] = $row;
            }
        }         
         
        for($i = 0; $i < count($guests); $i++)
        {
            ${'rule' . $i} = $service->acl->get($calendarListId, 'user:'. $guests[$i]);
            $updatedRule[] = ${'rule'. $i};
        }
        
        $event->setSummary($firstName . ': ' . $company);
        $event->setDescription(
            (isset($client_name) ? 'Contact Person: ' . $client_name . '<br/>' : '') . (isset($description) ? 'Description: ' . nl2br($description) : ''));
        
        $event->setColorId($color);

       // Create Calendar Appointment Date/Time Object
       $format = 'Y-m-d\TH:i:sP';

        $start_date = new \Google_Service_Calendar_EventDateTime();
        $start = new \DateTime($date_start);
        $start_date->setDateTime($start->format($format)); // Appointment Date Start
        
        $end_date = new \Google_Service_Calendar_EventDateTime();
        $end = new \DateTime($date_end);
        $end_date->setDateTime($end->format($format)); // Appointment Date End

        $event->setStart($start_date);
        $event->setEnd($end_date);
        $event->setColorId($color);

        // List event attendees
        $attendee = new \Google_Service_Calendar_EventAttendee();  // Create appointment receiver
        $attendee->setEmail('info.oomcrm@gmail.com');
        $attendee->setResponseStatus('accepted');
        $attendees[] = $attendee;
        $event->setAttendees($attendees);

        // Create event organizer object
        $organizer = new \Google_Service_Calendar_EventOrganizer();  // Create appointment receiver
        $organizer->setEmail('info.oomcrm@gmail.com');
        $event->setOrganizer($organizer);

        $results = $service->events->update($calendarListEntry->getId(), $calendarId, $event);
        /* END OF GOOGLE CALENDAR SCRIPTS */
     }


    function receivers($appointment)
     {
        $administrators = User::where('role', 1)->get();

         foreach($administrators as $admin)
         {
             $receivers[] = $admin;
         }

         $appt = Appointment::find($appointment['id']);
 
         foreach(explode('|', $appt->guest) as $guest)
         {
             $guestEmail = User::where('email', '=', $guest)->first();
             $receivers[] = $guestEmail;
         }
         
        foreach($receivers as $row)
         {
            //  if ($row['email'] == Auth::user()->email)
            //      continue;

             if($row == '')
                 continue;
 
             $notifiable[] = $row;
         }
         return array_unique($notifiable);
     }

    function getGuestNames($guestEmails)
    {
        if($guestEmails != null)
        {
            $guests = explode('|', $guestEmails);

                foreach($guests as $guest)
                {
                    if (in_array($guest, ['info.oomcrm@gmail.com', 'iwbappts@gmail.com']))
                        continue;

                    $name = User::where('email', '=', $guest)->orderBy('role')->first();
                    
                    if($name)
                        $email[] = $name->userInfo->f_name;
                    else
                        $email[] = $guest;      
                }

            return implode(', ', $email);
        }

        return 'None';
    }

    function getClientName($mobile, $fName, $lName = null)
    {
        $clientName = $fName.' '.$lName;

        return !empty($fName) || !empty($lName) ? ''. $mobile .' '. $clientName : $mobile ;
    }

    function singleDataAppointment($appointment)
    {
        $appointment = [
            'id'                        =>  $appointment->id,
            'scheduled_date'            =>  $this->getScheduledDate($appointment->date_start, $appointment->date_end),
            'client'                    =>  $this->checkDataIfEmpty($appointment->client_name),
            'company'                   =>  $this->checkDataIfEmpty($appointment->company),
            'title'                     =>  $this->shortData($appointment->title),
            'description'               =>  $this->shortData($appointment->description),
            'services'                  =>  $this->getServices($appointment->services),
            'guest'                     =>  $this->getGuestNames($appointment->guest),
            'google_calendar_id'        =>  $appointment->google_calendar_id,
            'google_calendarlist_id'    =>  $appointment->google_calendarlist_id,
            'lead_id'                   =>  $appointment->lead_id,
            'created_by'                =>  $appointment->creator->userInfo->f_name . ' ' . $appointment->creator->userInfo->l_name,
            'status'                    =>  $appointment->status != null ? $appointment->apStatus->systemdesc : 'Not Specified',
            'created_at'                =>  $appointment->created_at,
            'owner'                     =>  Auth::user()->userid,
            'color'                     =>  $appointment->eventColor->hex_value
        ]; 

        return $appointment;
    }

    function tableAppointments($model)
     {
         $appointment = [];
 
         if(count($model) > 0)
         {
             foreach($model as $row)
             {
                 $appointment[] = [
                     'id'                        =>  $row->id,
                     'scheduled_date'            =>  $this->getScheduledDate($row->date_start, $row->date_end),
                     'client'                    =>  $this->checkDataIfEmpty($row->client_name),
                     'company'                   =>  $this->checkDataIfEmpty($row->company),
                     'title'                     =>  $this->shortData($row->title),
                     'description'               =>  $this->shortData($row->description),
                     'guest'                     =>  $this->getGuestNames($row->guest),
                     'services'                  =>  $this->getServices($row->services),
                     'google_calendar_id'        =>  $row->google_calendar_id,
                     'google_calendarlist_id'    =>  $row->google_calendarlist_id,
                     'lead_id'                   =>  $row->lead_id,
                     'created_by'                =>  $row->creator->userInfo->f_name,
                     'status'                    =>  $row->status != null ? $row->apStatus->systemdesc : 'Not Specified',
                     'created_at'                =>  Carbon::parse($row->created_at)->format('d M Y'),
                     'owner'                     =>  Auth::user()->userid,
                     'color'                     =>  $row->eventColor->hex_value,
                     'full_title'                =>  $row->title,
                     'full_description'          =>  $row->description
                 ];
             } 
         }
         return $appointment;
     }

     function shortData($data) 
     {
        $str = $data != null ? (strlen($data) > 20 ? nl2br(substr(ucfirst($data), 0, 20)) . '...' : nl2br(ucfirst($data))) : 'Not Specified';
        return $str;
     }

     function checkDataIfEmpty($data)
     {
         return $data != null ? ucfirst($data) : 'Not Specified';
     }
 
     function getScheduledDate($startDate, $endDate)
     {
         $start = new \DateTime($startDate);
         $startFormat = $start->format('Y-M-D');

         $end = new \DateTime($endDate);
         $endFormat = $end->format('Y-M-D');


         return $startFormat == $endFormat ? Carbon::parse($startDate)->format('M. d, Y (h:i A') . ' - ' . Carbon::parse($endDate)->format('h:i A)') : Carbon::parse($startDate)->format('M. d h:i A') . ' - ' . Carbon::parse($endDate)->format('M. d, Y h:i A');
     }

     function getServices($services) 
     {
        if($services != null)
        {
            $services_array = explode('|', $services);
            foreach($services_array as $row)
            {
                $svc[] = strtok(System::where('systemtype', 'service')->where('systemcode', $row)->first()->systemdesc, ' ');
            }
            return $service = implode(', ', $svc);
        }
        return $service = 'Not Specified';
     }

}
