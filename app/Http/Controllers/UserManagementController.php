<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function show($userid)
    {
        $user = Auth::user();
        $name = $user->UserInfo->f_name.' '.$user->UserInfo->m_name.' '.$user->UserInfo->l_name;
        $adrs = $user->UserInfo->present_home.' '.$user->UserInfo->present_street.' '.
                $user->UserInfo->present_city.' '.$user->UserInfo->present_region.' '.
                $user->UserInfo->present_country;
        $email = $user->UserInfo->email;
        $bday = $user->UserInfo->bday;
        $work = $user->UserInfo->emp_designation->designation;
        $department = $user->UserInfo->emp_department->department;

        return view('user.show')->with(compact('user','name','adrs','email','bday','work','department'));
    }
    public function create(Request $request)
    {
        $input = $request->except('_token');

        $rules = [
            'emp_id'    => 'required|max:5|unique:users',
            'username'  => 'required|min:8|max:50|unique:users',
            'email'     => 'required|email|max:50|unique:users',
            'role'      => 'required|numeric'
        ];

        $messages = [
            'required' => 'This field is required.',
            'alphanum' => 'Please input letters and numbers only.',
            'min'      => 'This field must be atleast :min characters',
            'max'      => 'This field must be no more than :max characters',
            'numeric'  => 'Please select a role'
        ];

        $validator = Validator::make($input, $rules, $messages);


 
        if ($validator->fails()) {
            if(isset(request()->api_request))
            {
                return response()->json($validator,200);
            }
            
            return redirect()->back()
                        // ->with(array(
                        //     'system_message' => 'Errors found! Please double check it before proceeding',
                        //     'notification_type' => 'warn'))
                        ->with('error_type','userAdd')
                        ->withErrors($validator)
                        ->withInput();
        }

    
        $password = $this->randomString(7).'0oM';
        $padded = date('Ymd');
        $recentUser = User::where('userid','like','%'.$padded.'%')->orderby('created_at','desc')->first();
        $recentUserID = $recentUser == null ? 1 : (int)substr($recentUser->userid,8) + 1;
        $userid = $padded.sprintf('%02d',$recentUserID);

        $created_user = User::create([
            'emp_id'    => $input['emp_id'],
            'userid'    => $userid,
            'email'     => $input['email'],
            'username'  => $input['username'],
            'password'  => bcrypt($password),
            'role'      => $input['role'],
            'dept_code' => $input['dept_code']
        ]);

        $emailDetails = ['name' =>  $input['username'], 'username' => $input['username'], 'email' => $input['email'], 'password' => $password];

        \Mail::send('layouts.emails.userregistration',$emailDetails,function($message) use ($emailDetails){
           $message->to($emailDetails['email'])->subject
              ('CRM System - Login Credentials');
           $message->from('noreply@oom.com.sg','System Auto Message');
        });

        if(isset(request()->api_request))
        {
            return response()->json($created_user,200);
        }

        return redirect()->back();

    }

    public function postFunc(Request $request)
    {
        if ($request != null)
        {
            $password = $this->randomString(7).'0oM';
            $padded = date('Ymd');
            $recentUser = User::where('userid','like','%'.$padded.'%')->orderby('created_at','desc')->first();
            $recentUserID = $recentUser == null ? 1 : (int)substr($recentUser->userid,8) + 1;
            $userid = $padded.sprintf('%02d',$recentUserID);
    
            User::create([
                'emp_id'    => $request['emp_id'],
                'userid'    => $userid,
                'email'     => $request['email'],
                'username'  => $request['username'],
                'password'  => bcrypt($password),
                'role'      => $request['role'],
                'dept_code' => $request['dept_code']
            ]);

            $emailDetails = ['name' => $request['username'],'username' => $request['username'], 'email' => $request['email'], 'password' => $password];

            \Mail::send('layouts.emails.userregistration',$emailDetails,function($message) use ($emailDetails){
                $message->to($emailDetails['email'])->subject
                    ('CRM System - Login Credentials');
                $message->from('noreply@oom.com.sg','System Auto Message');
            });

            return response()->json(['message' => 'Gumagana']);
        }
        return response()->json(['message' => 'Hindi nagana']);
    }

    public function edit($userid)
    {
        $user = User::where('userid',$userid)->first();

        return response()->json($user);
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $user = User::where('userid',$input['userid'])->first();
        $uname = $user->username != $input['username'] ? 'required|min:8|max:50|unique:users' : '';
        $email = $user->email != $input['email'] ? 'unique:users' : '';

        $rules = [
            'firstname' => 'required|max:100|alpha_num',
            'lastname'  => 'required|max:100|alpha_num',
            'username'  => $uname,
            'email'     => 'required|email|max:50|'.$email
        ];

        $messages = [
            'required' => 'This field is required.',
            'alpha_num' => 'Please input letters and numbers only.',
            'min'      => 'This field must be atleast :min characters',
            'max'      => 'This field must be no more than :max characters'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        // ->with(array(
                        //     'system_message' => 'Errors found! Please double check it before proceeding',
                        //     'notification_type' => 'warn'))
                        ->with('error_type','userUpdate')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user->firstname = $input['firstname'];
        $user->lastname = $input['lastname'];
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->save();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $user = User::where('userid',$request->userid)->first();
        $user->delete_flag = 1;
        $user->deleted_at = date('Y-m-d H:i:s');
        $user->save();

        return redirect()->back();
    }

    public function search($user)
    {

        if ($user == 'null')
        {
            $users = User::whereNull('delete_flag')->paginate(5);
            $result = $this->tblUser($users);
            return response()->json(['result' => $result,'pagination' => $users],200);
        }

        // $byFname = User::where('firstname','like',$user.'%')->whereNull('delete_flag')->paginate(5);
        // $byLname = User::where('lastname','like',$user.'%')->whereNull('delete_flag')->paginate(5);
        $byUname = User::where('username','like','%'.$user.'%')->whereNull('delete_flag')->paginate(5);

        // if (!empty($byFname->toArray()['data']))
        //     $users = $byFname;
        // else if (!empty($byLname->toArray()['data']))
        //     $users = $byLname;
        // else if(!empty($byUname->toArray()['data']))
            $users = $byUname;
        // else
        //     $users = "";

        $result = empty($users) ? '' : $this->tblUser($users);

        return response()->json(['result' => $result,'pagination' => $users],200);
    }

    public function sort($field,$order)
    {
        $users = User::Sort($field,$order);

        foreach($users as $row)
        {
            $result[] = [
                'userid' => $row->userid,
                'name' => $row->firstname.' '.$row->lastname,
                'username' => $row->username,
                'role' => $field != 'role' ? $row->userRole->syscode->systemdesc : $row->systemdesc,
                'date' => date_format($row->created_at,'F d,Y')
            ];
        }

        return response()->json(['result' => $result,'pagination' => $users],200);
    }

    function refresh()
    {
        $users = User::whereNull('delete_flag')->paginate(5);
        $result = $this->tblUser($users);

        return response()->json(['result' => $result,'pagination' => $users],200);
    }

    function tblUser($users)
    {
        if(count($users) > 0)
            foreach($users as $row)
            {
                $result[] = [
                    'userid' => $row->userid,
                    'name' => $row->firstname.' '.$row->lastname,
                    'username' => $row->username,
                    'role' => $row->userRole->syscode->systemdesc,
                    'date' => date_format($row->created_at,'F d,Y')
                ];
            }
        else
            $result[] = [];

        return $result;
    }

    function randomString($length = 5)
    {
    		$str = "";
    		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    		$max = count($characters) - 1;

        for ($i = 0; $i < $length; $i++) {
    			$rand = mt_rand(0, $max);
    			$str .= $characters[$rand];
    		}

    		return $str;
  	}
}
