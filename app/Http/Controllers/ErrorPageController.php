<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorPageController extends Controller
{
    public function render($name)
    {
        return view('error.'.$name)->with(compact('url'));
    }
}
