<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use App\User;
use App\Lead;
use Validator;
use App\System;
use App\Country;
use App\Account;
use App\Prospect;
use App\Manager;
use App\Employee;
use App\LeadNote;
use App\AccountTm;
use App\AccountFb;
use App\AccountPpc;
use App\AccountSeo;
use App\AccountWeb;
use App\Notifications\LeadCreatedNotification;
use App\Notifications\LeadUpdatedNotification;
use App\Notifications\LeadDeletedNotification;
use App\Notifications\LeadConvertedNotification;
use App\Notifications\LeadConvertedProspectNotification;
use App\Notifications\LeadNoteCreatedNotification;
use App\Notifications\LeadNoteUpdatedNotification;
use App\Notifications\LeadNoteDeletedNotification;
use App\Notifications\AccountNoteCreatedNotification;
use App\Notifications\AccountNoteUpdatedNotification;
use App\Notifications\AccountNoteDeletedNotification;
use App\Notifications\ProspectNoteCreatedNotification;
Use App\Notifications\ProspectNoteUpdatedNotification;
use App\Notifications\ProspectNoteDeletedNotification;

use App\AccountBlogContent;
use App\AccountSocialManagement;
use App\AccountBaidu;
use App\AccountWeibo;
use App\AccountWeChat;
use App\Industry;
use App\Opportunity;
use App\OpportunityInfo;

use App\PaginationPerPage;
use App\LeadProspectLockedPeriod;

use Carbon\Carbon;
use Excel;
use File;
use DB;


class LeadManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        $manager_list = Manager::all();
        foreach($manager_list as $row)
        {
            $emp = Employee::where('emp_id',$row->emp_id)->first();

            if($row->dept_id == 2)
                continue;

            $managers[$emp['userInfo']['userid']] = [
                'email' => $emp['userInfo']['email'],
                'team'  => $emp['team']['team_code'],
                'name'  => $emp['f_name']. ' ' . $emp['l_name']
            ];
        }

        $administrators = User::where('role', 1)
                                ->where('id','!=',Auth::user()->id)
                                ->get();

        foreach($administrators as $admin)
        {
            $managers[$admin->userid] = [
                'email' => $admin->email,
                'team'  => $admin->UserInfo->team->team_code,
                'name'  => $admin->UserInfo->f_name.' '.$admin->UserInfo->l_name
            ];
        }

        $finance = User::where('role',3)
                        ->where('id', '!=', Auth::user()->id)
                        ->get();

        foreach($finance as $row)
        {
            $emp = Employee::where('emp_id',$row->emp_id)->first();

            $managers[$emp->userInfo->userid] = [
                'email' => $emp->userInfo->email,
                'team'  => $emp->team->team_code,
                'name'  => $emp->f_name.' '.$emp->l_name
            ];
        }


        $userlogged = Auth::user();
        $user = $userlogged;
        
        $lockedin = LeadProspectLockedPeriod::where('userid',$user->userid)->first();
      
        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $role = $user->role;

        $teamcode = !empty($user->UserInfo->team->team_code) ? $user->UserInfo->team->team_code : 'member';
        app(__('global.Audit'))->logAuditTrail("MD002","AT010","Accessed module",null,null,request()->ip(),$user->userid,$user->role);

        if($userlogged->role == 1)
            $data = Lead::whereNull('deleted_at')->where('active_flag',1)->where('status', '!=', 'LS005')->orderBy('updated_at','desc')->paginate($perPage);
        else
            $data = Lead::whereNull('deleted_at')->where('active_flag',1)->where('status', '!=', 'LS005')->where('created_by',$userlogged->userid)->orderBy('updated_at','desc')->paginate($perPage);

        $leads = $this->tblLead($data);

        if (request()->ajax())
        {
            return response()->json(['pagination' => $data, 'leads' => $leads, 'user' => $user, 'role' => $role, 'teamcode' => $teamcode ],200);
        }
        
        return view('lead.list')->with(compact('data','user','items','lockedin'));

    }

    public function view($id)
    {
        $user = Auth::user();
        $lead = Lead::find($id);
        $notes = LeadNote::where('lead_id',$id)->paginate(5);
        $commentors = LeadNote::where('lead_id',$id)->select('userid')->distinct()->get();

        if (empty($lead))
        {
            app(__('global.Audit'))->logAuditTrail("MD003","AE001","Lead not found",null,null,request()->ip(),$user->userid,$user->role);

            return
                    redirect()
                    ->route('lead.list')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Lead not found or no access to it',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
        }

        if(request()->ajax())
        {
            foreach($notes as $row)
            {
                $leadnotes[] = [
                    'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                    'date' => date_format($row->created_at,'Y-m-d g:i A'),
                    'id'   => $row->id,
                    'note' => $row->note,
                    'created_user' => $row->userid
                ];
            }

            return
                response()
                ->json([
                    'pagination' => $notes,
                    'notes'      => $leadnotes,
                    'user'       => Auth::user()->userid 
                ],200);
        }

        app(__('global.Audit'))->logAuditTrail("MD002","AT001","View lead ".$lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('lead.view')->with(compact('lead','user','notes','commentors'));
    }

    public function create()
    {
        $user = Auth::user();
        $services = System::where('systemtype','service')->where('activeflag',1)->get();
        $sources = System::where('systemtype','leadsource')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $landingpages = System::where('systemtype','landingpage')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $statuses = System::where('systemtype','leadstatus')->where('activeflag',1)->get();
        $industries =  Industry::orderBy('title', 'asc')->get(); // Industries Fetch List

        app(__('global.Audit'))->logAuditTrail("MD002","AT002","Create a lead",null,null,request()->ip(),$user->userid,$user->role);

        return view('lead.create')->with(compact('user','statuses','services','sources','landingpages', 'industries'));
    }

    public function save(Request $request)
    {
        $input = $request->except('_token');

        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = implode('|', $emailInput);
        }

        foreach($input as $k => $v)
        {
            // $lead_info[$k] = is_array($v) ? implode('|',$v) : $v;
            $lead_info[$k] = substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v;

            if ($k == 'zip_code' || $k == 'contact_number' || $k == 'office_number' )
                $rules[$k] = !empty($v) ? 'required|numeric' : '';
            else if (in_array($k,['f_name','l_name','street_address','zip_code','city','website','designation','other_services','services','source','landing_page','currency','contract_value']))
                continue;
            else if ( ($input['source'] != 'SL999' && $k == 'source_other') ||
                      ($input['landing_page'] != 'SL999' && $k == 'landing_other') || $k == 'service_other')
                $rules[$k] = '';
            else if ($k == 'email')
                $rules[$k] = 'required|unique:leads';
            else
                $rules[$k] = 'required';
        }

        // dd($input);
        $messages = [
            'required' => 'This field is required.',
            'numeric' => 'This field must be numeric only.',
            'email' => 'This field must be a valid email address',
            'unique' => 'The email address already exist'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
                    }

        $lead = Lead::create($lead_info);

        $admin = User::where('role',1)
                    ->where('id','!=',Auth::user()->id)     
                    ->get();
                    
        foreach($admin as $row)
        {
            $row->notify(new LeadCreatedNotification($lead,$lead->sales->UserInfo));

        }

        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD002","AT003","Created lead ".$lead_info['company'],null,null,request()->ip(),$user->userid,$user->role);

        return redirect()->route('lead.list')->with([
            'alert'   => 'success',
            'message' => 'A new lead has been created',
            'button'  => 'OK',
            'confirm' => 'true'    
        ]);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $lead = Lead::find($id);
        $statuses = System::where('systemtype','leadstatus')->where('activeflag',1)->get();
        $notes = LeadNote::where('lead_id',$id)->paginate(5);
        $services = System::where('systemtype','service')->where('activeflag',1)->get();
        $sources = System::where('systemtype','leadsource')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $landingpages = System::where('systemtype','landingpage ')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $commentors = LeadNote::where('lead_id',$id)->select('userid')->distinct()->get();
        $lockedin = LeadProspectLockedPeriod::where('userid',$user->userid)->first();
        $industries = Industry::orderBy('title', 'asc')->get(); // Industries Fetch List
        $salesPersons = Employee::salesPersonName();

        if (empty($lead))
        {
            app(__('global.Audit'))->logAuditTrail("MD003","AE001","Lead not found",null,null,request()->ip(),$user->userid,$user->role);

            return
                    redirect()
                    ->route('lead.list')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Lead not found or no access to it',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
        }
            

        if(request()->ajax())
        {
            foreach($notes as $row)
            {
                $leadnotes[] = [
                    'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                    'date' => date_format($row->created_at,'Y-m-d g:i A'),
                    'id'   => $row->id,
                    'note' => $row->note,
                    'created_user' => $row->userid
                ];
            }

            return
                response()
                ->json([
                    'pagination' => $notes,
                    'notes'      => $leadnotes,
                    'user'       => Auth::user()->userid 
                ],200);
        }

        app(__('global.Audit'))->logAuditTrail("MD002","AT004","Edit lead ".$lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('lead.edit')->with(compact('user','lead','statuses','services','sources','landingpages','notes','commentors','lockedin', 'industries', 'salesPersons'));
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');

        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = implode('|', $emailInput);
        }

        $lead = Lead::find($input['id']);
        
        foreach($input as $k => $v)
        {
            foreach($lead->toArray() as $i => $j)
            {
                if (!in_array($i,['id','created_at','updated_at','delete_flag','deleted_at','lock_this_lead']))
                    if ($i == $k)
                        $lead->$i = substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v;
            }

            if (in_array($k,['zip_code','contact_number','office_number']))
                $rules[$k] = isset($input[$k]) ? 'numeric' : '';
            else if (in_array($k,['f_name','l_name','designation','street_address','zip_code','city','website','other_services','services','source','landing_page','currency', 'contract_value']))
                continue;
            else if ( ($input['source'] != 'SL999' && $k == 'source_other') ||
                      ($input['landing_page'] != 'SL999' && $k == 'landing_other') || $k == 'service_other')
                $rules[$k] = '';
            else if ($k == 'email')
                $rules[$k] = $v != $lead->email ? 'required|unique:leads' : 'required';
            else
                $rules[$k] = 'required';
        }
        
        $messages = [
            'required' => 'This field is required.',
            'numeric' => 'This field must be numeric only.',
            'email' => 'This field must be a valid email address',
            'unique' => 'The email address already exist'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        // Eloquent LeadProspectLockedPeriod by User ID
        $lockedin = LeadProspectLockedPeriod::where('userid',Auth::user()->userid)->first();

        // New input of has locked-in period
        $new_locked = $input['lock_this_lead'];

        // Old input of has locked-in period
        $old_locked = $lead->lock_this_lead;

        // If locked-in period is not empty
        if(!empty($lockedin))
        {
            // If old and new input is not match
            if($old_locked != $new_locked)
            {
                // Decrease by 1 if input is Yes and Increase if it's set to No
                $new_locked != 1 ? $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock + 1 : $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock - 1;

                // Exceeding limit warning: 10 times only
                if($lockedin->leadprospect_to_unlock == -1 || $lockedin->leadprospect_to_unlock == 11)
                    return redirect()->back()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Warning: 10 times per one person. You are exceeding!',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);

                // Update the eloquent LeadProspectLockedPeriod
                $lead->lock_this_lead = $new_locked;
                $lockedin->update();
            }
        }
        else
        {
            // Update the eloquent prospect that has locked in period
            $lead->lock_this_lead = $new_locked;
            
            // If locked in period is empty, create new row
            $new_locked != 1 ?  LeadProspectLockedPeriod::create(['userid' => Auth::user()->userid]) : LeadProspectLockedPeriod::create(['userid' => Auth::user()->userid, 'leadprospect_to_unlock' => 9]);
        }

        // Update the eloquent Lead
        $lead->update();

        $admin = User::where('role',1)
                    ->where('id','!=',Auth::user()->id)     
                    ->get();

        foreach($admin as $row)
        {
            $row->notify(new LeadUpdatedNotification($lead,$lead->sales->UserInfo));

        }

        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD002","AT005","Updated lead ".$lead->company,null,null,request()->ip(),$user->userid,$user->role);
        
        return 
                redirect()
                ->back()
                ->with([
                    'alert'    => 'success',
                    'message'  => 'Lead '.$lead->f_name.' '.$lead->l_name.' is now updated',
                    'button'   => 'OK',
                    'confirm'  => 'true',
                    'stamp'    => time()
                ]);
    }

    public function delete(Request $request)
    {
        $input = $request->except('_token');

        $lead = Lead::where('id',$input['leadid'])->first();
        $del_lead = $lead->company;
        $lead->delete_flag = 1;
        $lead->deleted_at = date('Y-m-d H:i:s');
        $lead->save();

        $admin = User::where('role',1)->where('id','!=', Auth::user()->id)->get();
       
        foreach($admin as $row)
        {
            $row->notify(new LeadDeletedNotification($lead,$lead->sales->UserInfo));
        }

        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD002","AT006","Deleted lead ".$del_lead,null,null,request()->ip(),$user->userid,$user->role);

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'lead '.$del_lead.' has been deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getProspectStep1($id)
    {
        $user = Auth::user();
        $lead = Lead::find($id);

        $statuses = System::where('systemtype','leadstatus')->where('activeflag',1)->get();
        $services = System::where('systemtype','service')->where('activeflag',1)->get();
        $sources = System::where('systemtype','leadsource')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $landingpages = System::where('systemtype','landingpage ')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $industries =  Industry::orderBy("title", 'asc')->get(); // Industries fetch list

        if (empty($lead))
            return 
                redirect()
                ->route('lead.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Lead id '.$id.' does not exist. Please check the list first before converting it to prospect',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        app(__('global.Audit'))->logAuditTrail("MD004","AT012","Converting lead ".$lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('prospect.create')->with(compact('user','lead','statuses','services','sources','landingpages', 'industries'));
    }

    public function convertProspectStep1(Request $request)
    {
        $input = $request->except('_token');
        $lead = Lead::find($input['lead_id']);
        $user = Auth::user();

        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = implode('|', $emailInput);
        }

        foreach($input as $k => $v)
        {
            if($k == 'email')
                $rules[$k] = 'required';
            else if(in_array($k, ['company', 'industry']))
                $rules[$k] = 'required';
            else if($k == 'office_number')
                $rules[$k] = isset($input['office_number']) ? 'numeric' : '';
            else if($k == 'contact_number')
                $rules[$k] = isset($input['contact_number']) ? 'numeric' : '';
        }

        $messages = [
            'required' => 'This field is required.',
            'numeric' => 'This field must be numeric only.',
            'email' => 'This field must be a valid email address',
            'unique' => 'The email address already exist'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        // Get the old status
        $oldstatus = [
            'status' => $lead->status
        ];

        // Loop the input for setting and getting the data
        foreach($input as $field => $value)
        {
            if(in_array($field, ['lead_id','status', 'create_opportunity']))
                continue;
            
            $lead->$field =  $field != 'contract_value' ? (substr($value,5,1) == ',' ? implode('|',explode(',',$value)) : $value) : $value;
        }


        // If locked-in period is set to 'Yes' execute this!
        if($lead->lock_this_lead == 1)
        {
            $lockedin = LeadProspectLockedPeriod::where('userid', Auth::user()->userid)->first();
            $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock + 1;
            $lockedin->update();
        }

        // Set again the locked-in period when converting
        $lead->lock_this_lead = null;

        // Set the status to Closed
        $lead->status = 'LS005';

        // Update the lad
        $lead->save();
        
        $newstatus = [
            'status' => $lead->status
        ];
        
        // New data array for first-time client
        $dataForNewLead = [
            'lead_id'       => $input['lead_id'],
            'status'        => $input['status'],
            'created_by'    => Auth::user()->userid,
        ];

        // Existing data array for ex-client
        $dataForExistingLead = [
            'lead_id'       => $input['lead_id'],
            'status'        => $input['status'],
            'created_by'    => Auth::user()->userid,
            'ex_client_flag'=> 1,
            'active_flag'   => 1
        ];

        if(empty($lead->is_convert_again))
            // Create prospect if it's new client
            $prospectNew = Prospect::create($dataForNewLead);
        else
            // Update prospect if it's ex-client
            Prospect::where('lead_id',$lead->id)->update($dataForExistingLead);
            

        if (isset($input['create_opportunity']))
        {
            Opportunity::create([
                'prospect_id' => $prospectNew->id,
                'company'     => $prospectNew->lead->company,
            ])->info()->create([
                'status'            => $input['status'],
                'currency'          => $prospectNew->lead->currency,
                'contract_value'    => $input['contract_value'],
                'client_name'       => !empty($input['f_name']) || !empty($input['l_name']) ? $input['f_name'].' '.$input['l_name'] : '',
                'office_number'     => $input['office_number'],
                'mobile_number'     => $input['contact_number'],
                'email'             => $input['email'],
                'designation'       => $input['designation'],
                'address'           => $input['street_address'],
                'postal_code'       => $input['zip_code'],
                'city'              => $input['city'],
                'industry'          => $input['industry'],
                'website_url'       => $input['website'],
                'other_services'    => $input['other_services'],
                'source'            => $input['source'],
                'other_source'      => $input['source_other'],
                'landing_page'      => $input['landing_page'],
                'other_landing'     => $input['landing_other'],
                'created_by'        => $user->userid
            ]);
        }


        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD004","AT013","Converted lead ".$lead->company,$oldstatus,$newstatus,request()->ip(),$user->userid,$user->role);

        // Get the prospect's lead_id
        $prospect = Prospect::where('lead_id',$lead->id)->first();

        // $manager_list = Manager::all();

        // foreach($manager_list as $row)
        // {
        //     $emp = Employee::where('emp_id',$row->emp_id)->where('emp_id', '!=', Auth::user()->UserInfo->emp_id)->first();
            
        //     if($row->dept_id == 2)
        //         continue;

        //     if($emp['userInfo']['email'] == '')
        //         continue;

        //     $managers[$emp['userInfo']['userid']] = [
        //         'email' => $emp['userInfo']['email'],
        //         'team'  => $emp['team']['team_code'],
        //         'name'  => $emp['f_name']. ' ' . $emp['l_name']
        //     ];
        // }

        $administrators = User::where('role', 1)
                                ->where('id','!=',Auth::user()->id)
                                ->get();

        foreach($administrators as $admin)
        {
            $managers[$admin->userid] = [
                'email' => $admin->email,
                'team'  => $admin->UserInfo->team->team_code,
                'name'  => $admin->UserInfo->f_name.' '.$admin->UserInfo->l_name
            ];
        }

        $finance = User::where('role',3)
                        ->where('id', '!=', Auth::user()->id)
                        ->get();

        foreach($finance as $row)
        {
            $emp = Employee::where('emp_id',$row->emp_id)->first();

            $managers[$emp->userInfo->userid] = [
                'email' => $emp->userInfo->email,
                'team'  => $emp->team->team_code,
                'name'  => $emp->f_name.' '.$emp->l_name
            ];
        }

        $emailDetails = [
            // 'name' => $v['name'], 
            // 'email' => $v['email'], 
            // 'team' => $v['team'], 
            'lead' => $lead->company,
            'client'=> !empty($lead->l_name) ? $lead->f_name.' '.$lead->l_name : $lead->f_name,
            'prospectid' => $prospect->id,
            'creator' => !empty($lead->sales->UserInfo->f_name) ? $lead->sales->UserInfo->f_name . ' ' . $lead->sales->UserInfo->l_name : Auth::user()->UserInfo->f_name . ' ' . Auth::user()->UserInfo->l_name,
            'convert' => Auth::user()->UserInfo->f_name . ' ' . Auth::user()->UserInfo->l_name
        ];

        \Mail::send('layouts.emails.leadconvertprospect',$emailDetails,function($message) use ($emailDetails, $managers){
            $mails = array_pluck($managers, 'email');
            $message->bcc($mails)->subject
               ('CRM System - Converted Lead to Prospect '.$emailDetails['lead']);
            $message->from('noreply@oom.com.sg','CRM Admin');
            });

        // foreach($managers as $k => $v)
        // {
           
        // }

        foreach($managers as $row)
        {
            $thisuser = User::where('email',$row['email'])->first();
            $thisuser->notify(new LeadConvertedProspectNotification($prospect, $lead, $lead->sales->UserInfo));
        }

        return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'success',
                    'message' => 'Lead is now converted to prospect',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function search($lead)
    {
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        if ($lead == 'null')
        {
            $leads = Lead::whereNull('delete_flag')->where('active_flag',1)->where('status', '!=', 'LS005')->paginate($perPage);
            $result = $this->tblLead($leads);
            return response()->json(['result' => $result,'pagination' => $leads, 'user' => $user],200);
        }

        $byName = Lead::FindName($lead);
        $byEmail = Lead::FindEmail($lead);
        $byCno = Lead::FindContact($lead);
        $byComp = Lead::FindCompany($lead);
        $byStat = Lead::FindStatus($lead);
        $byDate = Lead::FindDate($lead);
        
        if($user->role == 1)
            $byOwner = Lead::FindOwner($lead);

        if (!empty($byName->toArray()['data']))
            $leads = $byName;
        else if (!empty($byEmail->toArray()['data']))
            $leads = $byEmail;
        else if(!empty($byCno->toArray()['data']))
            $leads = $byCno;
        else if(!empty($byComp->toArray()['data']))
            $leads = $byComp;
        else if(!empty($byStat->toArray()['data']))
            $leads = $byStat;
        else if(!empty($byDate->toArray()['data']))
            $leads = $byDate;
        else if($user->role ==1)
            if(!empty($byOwner->toArray()['data']))
            $leads = $byOwner;
        else
            $leads = "";

        $result = empty($leads) ? '' : $this->tblLead($leads);
        $pagination = empty($leads) ? ' ' : $leads;
        return response()->json(['result' => $result, 'pagination' => $pagination, 'user' => $user],200);
    }

    function refresh()
    {   
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','leads')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        if($user->role = 1)
            $leads = Lead::whereNull('delete_flag')->where('active_flag',1)->where('status', '!=', 'LS005')->paginate($perPage);
        else
            $leads = Lead::whereNull('delete_flag')->where('created_by',$user->userid)->where('status', '!=', 'LS005')->where('active_flag',1)->paginate($perPage);

        $result = $this->tblLead($leads);

        return response()->json(['result' => $result,'pagination' => $leads],200);
    }

    function tblLead($leads)
    {
        $isAdmin = Auth::user()->role == 1 ? 'yes' : 'no';
        if(count($leads) > 0)
            foreach($leads as $row)
            {
                $result[] = [
                    'isAdmin' => $isAdmin,
                    'leadid' => $row->id,
                    'name' => !empty($row->f_name) ? $row->f_name.' '.$row->l_name : 'Not specified',
                    'cno' => $row->contact_number,
                    'comp' => $row->company,
                    'leadstatus' => $row->leadStatus->systemdesc,
                    'date' => date('d M Y', strtotime($row->updated_at)),
                    'owner' => !empty($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None',
                    'locked' => $row->lock_this_lead == 1 ? 'Yes' : 'No',
                    'user'  => Auth::user()->userid,
                    'role'  => Auth::user()->role,
                    'teamcode' => Auth::user()->userInfo->team->teamcode,
                ];
            }
        else
            $result = "";

        return $result;
    }

    public function sort($field,$order)
    {
        $user = Auth::user();

        $leads = Lead::Sort($field,$order);

        $result = $this->tblLead($leads);

        return response()->json([
          'result'     => $result,
          'pagination' => $leads,
          'field'      => $field,
          'order'      => $order,
          'user'       => $user
        ],200);
    }

    public function createNote(Request $request)
    {
        $user = Auth::user();
        $input = $request->except('_token');

        $note = preg_replace("/\r\n|\r|\n/",'<br/>',$input['addnote']);

        $rules = [
            'addnote' => 'required'
        ];

        $messages = [
            'required' => 'Lead note cannot be blank',
            'regex' => 'Lead note may only contain alphanumeric characters'
        ];

        $validator = Validator::make($input, $rules, $messages);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => $validator->errors()->first(),
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        $leadnote = LeadNote::create([
            'lead_id' => $input['leadid'],
            'note' => $note,
            'userid' => $user->userid
        ]);

        
        if($leadnote->lead->status != 'LS005')
        {
            $lead = Lead::find($input['leadid']);
            $lead->updated_at = Carbon::now();
            $lead->update();

            $receivers = $this->receivers();
            foreach($receivers as $row)
            {
                if(!empty($leadnote->user->UserInfo))
                    $row->notify(new LeadNoteCreatedNotification($leadnote->lead,$leadnote->user->UserInfo));
                else
                    $row->notify(new LeadNoteCreatedNotification($leadnote->lead,Auth::user()->UserInfo));
            }

            app(__('global.Audit'))->logAuditTrail("MD008","AT002","Created a note in lead ".$leadnote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        }
        else if($leadnote->prospect->status != 'LS005' && $leadnote->lead->status == 'LS005')
        {
            $prospect = Prospect::find($input['prospectid']);
            $prospect->updated_at = Carbon::now();
            $prospect->updated_by = $user->userid;
            $prospect->update();

            $receivers = app(__('global.Prospect'))->receivers();
            foreach($receivers as $row)
            {
                $users = User::find($row['id']);
                if(!empty($leadnote->user->UserInfo))
                    $users->notify(new ProspectNoteCreatedNotification($leadnote->lead,$leadnote->prospect,$leadnote->user->UserInfo));
                else
                    $users->notify(new ProspectNoteCreatedNotification($leadnote->lead,$leadnote->prospect,Auth::user()->UserInfo));
            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT002","Created a note in prospect ".$leadnote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        } 
        else
        {
            $account = Account::find($input['acc_id']);
            $account->updated_at = Carbon::now();
            $account->updated_by = $user->userid;
            $account->update();

            $receivers = app(__('global.Account'))->receivers($leadnote->prospect->account);
            foreach($receivers as $row)
            {
                if(!empty($leadnote->user->UserInfo))
                    $row->notify(new AccountNoteCreatedNotification($leadnote->prospect,$leadnote->prospect->account,$leadnote->lead,$leadnote->user->UserInfo));
                else
                    $row->notify(new AccountNoteCreatedNotification($leadnote->prospect,$leadnote->prospect->account,$leadnote->lead,Auth::user()->UserInfo));
            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT002","Created a note in account ".$leadnote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        }        


        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'You have added a note.',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function updateNote(Request $request)
    {
        $input = $request->except('_token');
        $user = Auth::user();
        $updateNote = str_replace("\n",'<br>',$input['note']);
        $leadNote = LeadNote::find($input['id']);
        $leadNote->note = $updateNote;
        $leadNote->update();

        if($leadNote->lead->status != 'LS005')
        {
            $lead = Lead::where('id', $leadNote->lead_id)->first();
            $lead->updated_at = Carbon::now();
            $lead->update();

            $receivers = $this->receivers();
            foreach($receivers as $row)
            {
                if(!empty($leadNote->user->UserInfo))
                    $row->notify(new LeadNoteUpdatedNotification($leadNote->lead ,$leadNote->user->UserInfo));
                else
                    $row->notify(new LeadNoteUpdatedNotification($leadNote->lead, $user->userInfo));
            }

            app(__('global.Audit'))->logAuditTrail("MD008","AT005","Updated a note in lead ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
            
        }
        else if($leadNote->prospect->status != 'LS005' && $leadNote->lead->status == 'LS005')
        {
            $prospect = Prospect::where('lead_id', $leadNote->prospect->lead_id)->first();
            $prospect->updated_at = Carbon::now();
            $prospect->updated_by = $user->userid;
            $prospect->update();

            $receivers = app(__('global.Prospect'))->receivers();
            foreach($receivers as $row)
            {
                $users = User::find($row['id']);
                if(!empty($leadNote->user->UserInfo))
                    $users->notify(new ProspectNoteUpdatedNotification($leadNote->lead,$leadNote->prospect, $leadNote->user->UserInfo));
                else
                    $users->notify(new ProspectNoteUpdatedNotification($leadNote->lead,$leadNote->prospect, $user->UserInfo));
            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT005","Updated a note in prospect ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        }
        else
        {
            $account = Account::where('id', $input['acc_id'])->first();
            $account->updated_at = Carbon::now();
            $account->updated_by = $user->userid;
            $account->update();

            $receivers = app(__('global.Account'))->receivers($leadNote->prospect->account);
            foreach($receivers as $row)
            {
                if(!empty($leadNote->user->UserInfo))
                    $row->notify(new AccountNoteUpdatedNotification($leadNote->prospect,$leadNote->prospect->account,$leadNote->lead,$leadNote->user->UserInfo));
                else
                    $row->notify(new AccountNoteUpdatedNotification($leadNote->prospect,$leadNote->prospect->account,$leadNote->lead,Auth::user()->UserInfo));
            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT005","Updated a note in account ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        }      

        return response()->json($request->note, 200);
        // return response()->json($request->note,200);  
    }

    public function deleteNote(Request $request)
    {   
        $input = $request->except('_token');
        $user = Auth::user();
        $leadNote = LeadNote::find($input['note_id']);

        if($leadNote->lead->status != 'LS005')
        {
            $lead = Lead::find($input['lead_id']);
            $lead->updated_at = Carbon::now();
            $lead->update();

            $receivers = $this->receivers();
            foreach($receivers as $row)
            {
                if(!empty($leadNote->user->UserInfo))
                    $row->notify(new LeadNoteDeletedNotification($leadNote->lead, $leadNote->user->UserInfo));
                else
                    $row->notify(new LeadNoteDeletedNotification($leadNote->lead, $user->UserInfo));
            }

            app(__('global.Audit'))->logAuditTrail("MD008","AT006","Deleted a note in lead ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        }
        else if ($leadNote->prospect->status != 'LS005' && $leadNote->lead->status == 'LS005')
        {
            $prospect = Prospect::find($input['prospect_id']);
            $prospect->updated_at = Carbon::now();
            $prospect->updated_by = $user->userid;
            $prospect->update();

            $receivers = app(__('global.Prospect'))->receivers();
            foreach($receivers as $row)
            {
                $users = User::find($row['id']);
                if(!empty($leadNote->user->UserInfo))
                    $users->notify(new ProspectNoteDeletedNotification($leadNote->lead,$leadNote->prospect,$leadNote->user->UserInfo));
                else
                    $users->notify(new ProspectNoteDeletedNotification($leadNote->lead,$leadNote->prospect,Auth::user()->UserInfo));

            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT006","Deleted a note in prospect ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role); 
        }
        else
        {
            $account = Account::find($input['account_id']);
            $account->updated_at = Carbon::now();
            $account->updated_by = $user->userid;
            $account->update();

            $receivers = app(__('global.Account'))->receivers($leadNote->prospect->account);
            foreach($receivers as $row)
            {
                if(!empty($leadNote->user->UserInfo))
                    $row->notify(new AccountNoteDeletedNotification($leadNote->prospect,$leadNote->prospect->account,$leadNote->lead,$leadNote->user->UserInfo));
                else
                    $row->notify(new AccountNoteDeletedNotification($leadNote->prospect,$leadNote->prospect->account,$leadNote->lead,Auth::user()->UserInfo));
            }
            app(__('global.Audit'))->logAuditTrail("MD008","AT006","Deleted a note in account ".$leadNote->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        } 

        $leadNote->delete();
        
        
    }

    public function filterNotes($id,$by,$userid,$from,$to)
    {
        $notes = $by == 'date' ? 
                    LeadNote::where('lead_id',$id)->whereBetween('created_at',[$from,$to.' 23:59:59'])->paginate(5) 
                    :
                    LeadNote::where('lead_id',$id)->where('userid',$userid)->paginate(5);

        foreach($notes as $row)
        {
            $leadnotes[] = [
                'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                'date' => date_format($row->created_at,'Y-m-d g:i A'),
                'id'   => $row->id,
                'note' => $row->note,
                'created_user' => $row->userid
            ];
        }
        
        return
            response()
            ->json([
                'pagination' => $notes,
                'notes'      => !isset($leadnotes) ? [] : $leadnotes,
                'user'       => Auth::user()->userid 
            ],200);
    }

    function receivers()
    {
        $administrators = User::where('role',1)
                            ->where('id','!=',Auth::user()->id)     
                            ->get();
        foreach($administrators as $admin)
        {

            $receivers[] = $admin;
        }

        // dd($receivers);

        return $receivers;
    }

    function forgetAccountSession()
    {
      Session::forget('account_additional');
      Session::forget('account_sem');
      Session::forget('account_fb');
      Session::forget('account_seo');
      Session::forget('account_web');
      Session::forget('account_baidu');
      Session::forget('account_weibo');
      Session::forget('account_wechat');
      Session::forget('account_blog');
      Session::forget('account_social');
      Session::forget('account_postpaid');
    }
}
