<?php

namespace App\Http\Controllers;

use App\Role;
use App\System;
use App\Manager;
use App\Employee;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create(Request $request)
    {
        $padded = 'RL';
        $role = Role::where('role','like','%'.$padded.'%')->orderby('role','desc')->first();
        $newRole = (int)substr($role->role,4) + 1;
        $roleCode = $padded.sprintf('%03d',$newRole);
        
        $this->validate($request, [
            'role' => 'required'
        ]);
        
        Role::create(array(
            'role'       => $roleCode,
            'activeflag' => 1
        ));

        System::create(array(
            'systemcode' => $roleCode,
            'systemtype' => 'role',
            'systemdesc' => $request->role,
            'activeflag' => 1
        ));

        return redirect()->back();
    }

    public function edit($roleid)
    {
        $thisrole = Role::find($roleid);
        $roleid = $thisrole->id;
        $role = $thisrole->syscode->systemdesc;
        $isActive = $thisrole->activeflag;

        return response()->json(['role' => $role,'roleid' => $roleid, 'isActive' => $isActive],200);
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');

        $role = Role::find($input['roleid']);
        $role->activeflag = isset($input['role-active']) ? 1 : 0;
        $role->save();

        $system = System::where('systemcode',$role->role)->first();
        $system->systemdesc = $input['role'];
        $system->activeflag = isset($input['role-active']) ? 1 : 0;
        $system->save();

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Role '.$role->syscode->systemdesc.' has been updated',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function addManager(Request $request)
    {
        $input = $request->except('_token');
        foreach($input['managers'] as $row)
        {
            $employee = Employee::where('emp_id',$row)->first();
            Manager::create([
                'emp_id' => $row,
                'dept_id' => $employee->dept_code
            ]);
        }

        return
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Manager list has been updated',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function deleteManager(Request $request)
    {
        $input = $request->except('_token');
        $employee = Employee::where('emp_id',$input['emp_id'])->first();
        $manager = Manager::where('emp_id',$input['emp_id'])->first();
        $manager->delete();

        return
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => $employee->f_name.' '.$employee->l_name.' has been removed from the manager list',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }
}
