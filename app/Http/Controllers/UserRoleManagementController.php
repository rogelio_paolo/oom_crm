<?php

namespace App\Http\Controllers;

use Auth;
use App\Role;
use App\User;
use App\Access;
use App\System;
use App\Manager;
use Illuminate\Http\Request;

class UserRoleManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function list()
    {
        $roles = Role::all();
        $user = Auth::user();
        $managers = Manager::all();
        $headofdepts = User::where('role',2)->whereNotIn('emp_id',$managers->pluck('emp_id')->toArray())->get();

        return view('roles.list')->with(compact('roles','user','managers','headofdepts'));
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');

        $module = [
            'MD002' => !isset($input['accounts-module']) || $input['role'] == 'RL003' ? '' : $input['accounts-module'],
            'MD003' => !isset($input['employees-module']) || $input['role'] == 'RL004' ? '' : $input['employees-module']
        ];

        $user = User::where('emp_id',$input['emp_id'])->first();
        $access = Access::where('userid',$user->userid)->first();
        $role = System::where('systemcode',$input['role'])->first();
        
        $oldval = [
            'role' => $user->userRole->syscode->systemdesc
        ];

        $newval = [
            'role' => $role->systemdesc
        ];

        $user->role = $role->role->id;
        $user->save();

        foreach($module as $n => $row)
        {
            if($access == null)
            {
                Access::create([
                    'userid' => $user->userid,
                    'module' => $n,
                    'access' => json_encode($row)
                ]);
            }
            else
            {
                $userAccess = Access::where('module',$n)->where('userid',$user->userid)->get();

                foreach($userAccess->toArray() as $k => $v)
                {
                    if($v['module'] != $n)
                    {
                        Access::where('module',$n)->where('userid',$user->userid)->delete();
                    }
                    else
                    {
                        $userAccess = Access::where('module',$n)->where('userid',$user->userid)->first();
                        $userAccess->access = json_encode($row);
                        $userAccess->update();
                    }
                }
            }

        }
        
        app(__('global.Audit'))->logAuditTrail("MD004","AT005","Updated role of employee ".$user->UserInfo->f_name.' '.$user->UserInfo->l_name,json_encode($oldval),json_encode($newval),request()->ip(),Auth::user()->userid,Auth::user()->role);

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Employee '.$user->userInfo->f_name.' '.$user->userInfo->l_name.' role has been updated',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getRole($empid)
    {
        $user = User::where('emp_id',$empid)->first();
        $role = $user->UserRole->role;
        $roles = System::where('systemtype','role')->get();
        $access_modules = Access::where('userid',$user->userid)->get();

        if (!empty($access_modules->toArray()))
            foreach($access_modules as $index => $row)
            {
                $access[] = [
                    'module'      => $row->module,
                    'module_name' => strtolower(str_replace(' Module','',$row->ModType->systemdesc)),
                    'access'      => json_decode($row->access)
                ];
            }
        else
            $access = '';

        return response()->json(['roles' => $roles,'role' => $role, 'access' => $access],200);
    }

    public function userRole()
    {
        $role = Auth::user()->role;

        return response()->json($role,200);
    }
}
