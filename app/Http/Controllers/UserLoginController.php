<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\User;
use Validator;
use Illuminate\Http\Request;

class UserLoginController extends Controller
{
    protected function authenticated(Request $request, User $user)
    {
        return redirect()->intended();
    }

    function index()
    {
        if(Auth::user())
        {
            return redirect()->route('user.dashboard');
        }

        return view('userlogin');
    }

    function login(Request $request)
    {
        $input = $request->all();
        $login = $request->login;
        $login_val = empty(strpos($request->login,'@')) ? 'username' : 'email';
        $password = $request->password;
        $user = $login_val == 'email' ? User::whereNull('deleted_at')->where('email',$login)->first() : User::whereNull('deleted_at')->where('username',$login)->first();
        
        if (empty($user))
            return redirect()->back()
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Username or email does not exist',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
                        
        $rules = [
            'login'    => 'required|min:5|max:50|exists:users,'.$login_val,
            'password' => 'required|alpha_dash|min:5|max:20|old_password:'.$user->password
        ];

        $messages = [
            'required'     => 'This field is required.',
            'alphanum'     => 'Please input letters and numbers only.',
            'min'          => 'Field must be atleast :min characters',
            'max'          => 'Field must be no more than :max characters',
            'exists'       => 'Username or email does not exist',
            'old_password' => 'Password is incorrect'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {

            app(__('global.Audit'))->logAuditTrail("MD003","AE003","Login error",null,null,request()->ip(),$user->userid,$user->role);

            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        $credentials = array(
  	        'login' => $input['login'],
  	        'password' => $input['password'],
        );

        if (strpos($password,'0oM'))
        {
            return redirect()->route('user.firstlogin',$user->username);
        }
        else
        {

            if (Auth::attempt([$login_val => $login, 'password' => $password, 'deleted_at' => null]))
            {
                app(__('global.Audit'))->logAuditTrail("MD001","AT008","Logged in the CRM",null,null,\Request::ip(),$user->userid,$user->role);
                \Session::put('userlogged',$user);

                if( \Session::has('redirectURL'))
                {
                    return \Redirect::to(\Session::get('redirectURL'));
                }

                return redirect()->intended(route('user.dashboard'));
            }
            
        }
    }

    public function getInitialLogin($username)
    {
        $user = User::where('username',$username)->first();
        $password = $user->password;

        app(__('global.Audit'))->logAuditTrail("MD006","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);

        return view('firstlogin')->with(compact('password','username'));
    }

    public function postInitialLogin(Request $request,$username)
    {
        $user = User::where('username',$username)->first();
        $user->password = bcrypt($request->newpass);
        $user->save();

        app(__('global.Audit'))->logAuditTrail("MD006","AT004","Changed Default Password",null,null,request()->ip(),$user->userid,$user->role);

        if (Auth::attempt(['username' => $username, 'password' => $request->newpass]))
        {
            return redirect()->intended(route('user.dashboard'));
        }
    }

    public function getResetPassword()
    {
        return view('resetpassword');
    }

    public function postResetPassword(Request $request)
    {
        $input = $request->except('_token');

        $messages = [
            'required' => 'Cannot proceed with an empty email address.',
            'exists'    => 'Email address is not recognized in the system',
            'email'    => 'Email address should be valid'
        ];

        $rules = [
            'email' => 'required|email|exists:users,email'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
            return
                redirect()
                ->back()
                ->withErrors($validator)
                ->with([
                    'alert'   => 'error',
                    'message' => $validator->errors()->first(),
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = User::where('email',$input['email'])->first();

        app(__('global.Audit'))->logAuditTrail("MD007","AT010","Reset Password",null,null,request()->ip(),$user->userid,$user->role);
        
        $emailDetails = [
            'name'     => $user->UserInfo->f_name.' '.$user->UserInfo->l_name,
            'username' => $user->username,
            'userid'   => $user->userid,
            'email'    => $input['email']
        ];

        $this->email($emailDetails);

        return
            redirect()
            ->back()
            ->with([
                'alert'   => 'success',
                'message' => 'An email has been sent to '.$input['email'].', containing a link to reset your password. Thank you!',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
            
    }

    public function getResetPasswordChange($token,$userid)
    {
        $user = User::where('userid',$userid)->first();
        app(__('global.Audit'))->logAuditTrail("MD007","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);
        return view('resetpasswordchange')->with(compact('userid'));
    }

    public function postResetPasswordChange(Request $request)
    {
        $input = $request->except('_token');

        $rules = [
            'password'          => 'required|max:20|alpha_dash',
            'confirm_password'  => 'required|max:20|alpha_dash|same:password'
        ];

        $messages = [
            'required'   => 'This field is required',
            'max'        => 'password must contain not more than 20 characters',
            'alpha_dash' => 'password must be alpha-numeric characters, dashes and underscores only',
            'same'       => 'new password must be the same with confirm password'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
            return
                redirect()
                ->back()
                ->withErrors($validator)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Some of the fields has an error. Please check',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = User::where('userid',$input['userid'])->first();
        $user->password = bcrypt($input['password']);
        $user->save();
        
        app(__('global.Audit'))->logAuditTrail("MD007","AT005","Reset Password",null,null,request()->ip(),$user->userid,$user->role);

        // $emailDetails = [
        //     'name' => $user->UserInfo->f_name.' '.$user->UserInfo->l_name,
        //     'username' => $user->username,
        //     'email' => $input['email']
        // ];

        // $this->email($emailDetails);

        return
            redirect()
            ->route('user.login')
            ->with([
                'alert'   => 'success',
                'message' => 'Your password is successful changed. Please login now. Thank you!',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
    }

    function email($emailDetails)
    {
        \Mail::send('layouts.emails.resetpassword',$emailDetails,function($message) use ($emailDetails){
            $message->to($emailDetails['email'])->subject
               ('CRM System - Reset Password');
            $message->from('noreply@oom.com.sg','CRM Admin');
            });
    }
    
    public function checkAuth()
    {
        $user = Auth::user();
        if($user == null)
        {
            return redirect('/')->with('message','Session Expired. Please login again.');
        }
        else
        {
            return response()->json([],200);
        }
    }
}
