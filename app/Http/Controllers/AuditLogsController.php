<?php

namespace App\Http\Controllers;

use App\System;
use App\AuditTrail;
use Illuminate\Http\Request;

class AuditLogsController extends Controller
{
    public function list()
    {
        $auditlogs = AuditTrail::orderby('created_at','desc')->paginate(10);

        if (request()->ajax())
        {
            $logs = $this->tblAudit($auditlogs);

            return response()->json(['logs' => $logs,'pagination' => $auditlogs],200);
        }

        return view('audit.list')->with(compact('auditlogs'));
    }

    public function show($id)
    {
        $audit = AuditTrail::find($id);

        if(!empty($audit->oldval))
        {
            $oldval = json_decode(json_decode($audit->oldval),true);
            $newval = json_decode(json_decode($audit->newval),true);
            
            $changes = array();
            foreach($oldval as $k => $v)
            {
                $oldvalue = $this->systemValue($oldval[$k]);
                $newvalue = $this->systemValue($newval[$k]);
                $changes[$k] = [$oldvalue,$newvalue];
            }
        }
        else
        {
            $changes = 0;
        }

        $browser = !empty($audit->browser) ? json_decode($audit->browser,true) : '';

        return view('audit.show')->with(compact('audit','changes','browser'));
    }

    function tblAudit($logs)
    {
        if(count($logs) > 0)
            foreach($logs as $row)
            {
                $result[] = [
                    'id' => $row->id,
                    'ip' => $row->ip,
                    'module' => $row->sysmodule->systemdesc,
                    'action' => $row->sysaction->systemdesc,
                    'message' => str_limit($row->log,20),
                    'employee' => $row->user->UserInfo->f_name.' '.$row->user->UserInfo->l_name,
                    'date' => date_format($row->created_at,'F d, Y')
                ];
            }
        else
            $result[] = [];

        return $result;
    }

    function systemValue($systemcode)
    {
        $system = System::where('systemcode',$systemcode)->first();

        if(!empty($system))
            return $system->systemdesc;
        else
            return $systemcode;
    }

}
