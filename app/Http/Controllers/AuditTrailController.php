<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use Illuminate\Http\Request;

class AuditTrailController extends Controller
{
    
    public function logAuditTrail($module, $action, $log, $oldval = null, $newval = null, $ip, $userid, $role)
    {
        $audit = New AuditTrail;
        $audit->module = $module;
        $audit->action = $action;
        $audit->log = $log;
        
        if($oldval || $newval)
        {
            $audit->oldval = json_encode($oldval);
            $audit->newval = json_encode($newval);
        }

        $audit->ip = $ip;
        $audit->userid = $userid;
        
        if($role != 1){
			$checkBrowser = app(__('global.Audit'))->getBrowser();
			$audit->browser = json_encode($checkBrowser);
		}else{
			$audit->browser = null;
        }
        
        $audit->save();
    }

    public function getBrowser(){		
		$u_agent = $_SERVER['HTTP_USER_AGENT']; 
	    $bname = 'Unknown';
	    $platform = 'Unknown';
	    $version= "";
	
	    //First get the platform?
	    if (preg_match('/linux/i', $u_agent)) {
	        if(preg_match('/Android/i', $u_agent)){
	        $platform = 'Android';
	        }else{
	        $platform = 'linux';
	        }
	    }
	    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	        if(preg_match('/iPad/i', $u_agent)){
	        $platform = 'iPad';
	        }else{
	        $platform = 'mac';
	        }
	    }
	    elseif (preg_match('/windows|win32/i', $u_agent)) {
	        $platform = 'windows';
	    }
	
	    // Next get the name of the useragent yes seperately and for good reason
	    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
	    { 
	        $bname = 'Internet Explorer'; 
	        $ub = "MSIE"; 
	    } 
	    elseif(preg_match('/Trident/i',$u_agent)) 
	    { // this condition is for IE11
	        $bname = 'Internet Explorer'; 
	        $ub = "rv"; 
	    } 
	    elseif(preg_match('/Firefox/i',$u_agent)) 
	    { 
	        $bname = 'Mozilla Firefox'; 
	        $ub = "Firefox"; 
	    } 
	    elseif(preg_match('/SamsungBrowser/i',$u_agent)) 
	    { 
	        $bname = 'SamsungBrowser'; 
	        $ub = "SamsungBrowser"; 
	    } 
	    elseif(preg_match('/Chrome/i',$u_agent)) 
	    { 
	        $bname = 'Google Chrome'; 
	        $ub = "Chrome"; 
	    } 
	    elseif(preg_match('/Safari/i',$u_agent)) 
	    { 
	        $bname = 'Apple Safari'; 
	        $ub = "Safari"; 
	    } 
	    elseif(preg_match('/Opera/i',$u_agent)) 
	    { 
	        $bname = 'Opera'; 
	        $ub = "Opera"; 
	    } 
	    elseif(preg_match('/Netscape/i',$u_agent)) 
	    { 
	        $bname = 'Netscape'; 
	        $ub = "Netscape"; 
	    } 
	    
	    // finally get the correct version number
	    // Added "|:"
	    $known = array('Version', $ub, 'other');
	    $pattern = '#(?<browser>' . join('|', $known) .
	     ')[/|: ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    if (!preg_match_all($pattern, $u_agent, $matches)) {
	        // we have no matching number just continue
	    }
	
	    // see how many we have
	    $i = count($matches['browser']);
	    if ($i != 1) {
	        //we will have two since we are not using 'other' argument yet
	        //see if version is before or after the name
	        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
	            $version= $matches['version'][0];
	        }
	        else {
	            $version= $matches['version'][1];
	        }
	    }
	    else {
	        $version= $matches['version'][0];
	    }
	
	    // check if we have a number
	    if ($version==null || $version=="") {$version="?";}
	
	    return array(
	        'userAgent' => $u_agent,
	        'name'      => $bname,
	        'version'   => $version,
	        'platform'  => $platform,
	        'pattern'    => $pattern
	    );
	}
}
