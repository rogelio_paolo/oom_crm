<?php

namespace App\Http\Controllers;

use Auth;

use App\Contract;
use App\Account;
use App\Prospect;
use App\System;
use App\Manager;
use App\User;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Notifications\ContractDeletedNotification;

class AccountContractController extends Controller
{
    public function getContracts($id) 
    {
        $contracts = Contract::where('prospect_id', $id)->whereNull('delete_flag')->get();  // Get the active_contracts on contract model
        
         // Invoke the helper function of contracts
        $results = $this->tblContract($contracts);

        // Return the data AJAX request
        return response()->json([
            'contracts' => $results
        ], 200);
    }

    public function getSummary($prospect_id)
    {
        $active_contracts = Contract::where('prospect_id', $prospect_id)->whereNull('delete_flag')->where('is_active', 1)->orderBy('created_at', 'desc')->get();
        $past_contracts = Contract::where('prospect_id', $prospect_id)->whereNull('delete_flag')->where('is_active', 0)->orderBy('created_at', 'desc')->get();
        $prospect = Prospect::find($prospect_id);
        $user = Auth::user();
        $ref_emp_id = $user->userInfo->emp_id;
        $teamcode = $user->userInfo->team->team_code;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
        $first_contract = Contract::where('prospect_id', '=', $prospect_id)->where('is_active', 1)->orderBy('id', 'asc')->first();

        return view('contract.summary', compact('active_contracts', 'past_contracts', 'prospect', 'user', 'teamcode', 'ref_emp_id', 'managers'));
    }

    public function deleteContract(Request $request)
    {
        $input = $request->except('_token');
        $user = Auth::user();
        $contract = Contract::find($input['contractid']);

        if (empty($contract))
        {
            app(__('global.Audit'))->logAuditTrail("MD012","AE006","Contract Delete Error",null,null,request()->ip(),$user->userid,$user->role);

            return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Contract does not exist or already deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        }

        $number = $contract->contract_number;
        $contract->deleted_at = Carbon::now();
        $contract->delete_flag = 1;
        $contract->update();

        app(__('global.Audit'))->logAuditTrail("MD012","AT006","Deleted Contract # ". $number,null,null,request()->ip(),$user->userid,$user->role);

        $receivers = $this->receivers();

        foreach($receivers as $row)
        {
            $row->notify(new ContractDeletedNotification($contract, $user->userInfo));
        }

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Contract # '.$number.' has been deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

    }

    function receivers()
     {
         $administrators = User::where('role', 1)->get();

         foreach($administrators as $admin)
         {
             $receivers[] = $admin;
         }
         
         foreach($receivers as $row)
         {
            //  if ($row['email'] == Auth::user()->email)
            //      continue;

             if($row == '')
                 continue;
 
             $notifiable[] = $row;
         }

         return array_unique($notifiable);
     }

    // Helper function for empty row
    function isEmpty($value)
    {
        !empty($value) ? $value : $value = 'N/A';
        return $value;
    }

    // Helper function for Contracts table
    function tblContract($contracts)
    {
        if(count($contracts) > 0)
        {
            foreach($contracts as $contract)
            {
                $array_contracts[] = [
                    'id'                => $contract->id,
                    'prospect_id'       => $contract->prospect_id,
                    'account_id'        => $contract->account_id,
                    'contract_number'   => $contract->contract_number,
                    'contract_value'    => !empty($contract->contract_value) ? $contract->currency . ' ' . $contract->contract_value : 'N/A',
                    'is_active'         => $contract->is_active == 1 ? 'Active' : 'Past',
                    'company'           => $this->isEmpty($contract->company),
                    'full_name'         => $this->isEmpty($contract->f_name),
                    'designation'       => $this->isEmpty($contract->designation),
                    'contact_number'    => $this->isEmpty($contract->contact_number),
                    'email'             => $this->isEmpty($contract->email),
                    'created_by'        => $this->isEmpty($contract->created_by),
                    'created_at'        => date('d M Y', strtotime($contract->created_at)),
                    'contract_type'     => System::where('systemtype', 'contract_type')->where('systemcode', $contract->contract_type)->first()->systemdesc,
                ];
            }
        }
        else
        {
            $array_contracts = '';
        }

        return $array_contracts;
    }
}
