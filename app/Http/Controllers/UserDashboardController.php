<?php

namespace App\Http\Controllers;

use Excel;
use Auth;
use Session;
use Validator;

use App\Lead;
use App\Role;
use App\User;
use App\Account;
use App\System;
use App\Manager;
use App\Department;
use App\Employee;

use Illuminate\Http\Request;

use App\Charts\AccountTotal;
use App\Charts\AccountPerStatus;
use App\Charts\AccountPerPackage;
use App\Charts\LeadTotal;
use App\Charts\LeadPerStatus;
use App\Charts\EmployeeTotal;
use App\Charts\EmployeePerStatus;
use App\Charts\EmployeePerDepartment;

class UserDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    { 
        $user = Auth::user();
        //$this->filterAccountTotalByDate('2017-12-09','2018-02-14')
        $totalAccounts = $this->getTotalAccountsChart($user);
        $countAccounts = array_sum((array)$totalAccounts->datasets[0]->values);
        $accountsPerStatus = $this->getAccountStatusesChart($user);
        $accountsPerPackage = $this->getAccountPackagesChart($user);

        $totalLeads = $this->getTotalLeadsChart($user);
        $countLeads = array_sum((array)$totalLeads->datasets[0]->values);
        $leadsPerStatus = $this->getLeadStatusesChart($user);

        $totalEmployees = $this->getTotalEmployeesChart($user);
        $countEmployees = array_sum((array)$totalEmployees->datasets[0]->values);
        $employeesPerStatus = $this->getEmployeeStatusesChart($user);
        $employeesPerDepartment = $this->getEmployeeDepartmentsChart($user);
        
        $holders = Employee::AccountHolder();
        $sales = Employee::Sales();

        if (request()->ajax())
            return 
                response()
                ->json([
                    'chart' => $totalAccounts,
                    'count' => $countAccounts,
                    'date' => date('F d, Y'),
                    'chartPerStatus' => $accountsPerStatus,
                    'chartPerPackage' => $accountsPerPackage,
                    'leadchart' => $totalLeads,
                    'leadcount' => $countLeads,
                    'date' => date('F d, Y'),
                    'leadchartPerStatus' => $leadsPerStatus,
                ],200);

        return 
            view('dashboard')
            ->with(compact(
                'user',
                'countAccounts',
                'totalAccounts',
                'accountsPerStatus',
                'accountsPerPackage',
                'countLeads',
                'totalLeads',
                'leadsPerStatus',
                'countEmployees',
                'totalEmployees',
                'employeesPerStatus',
                'employeesPerDepartment',
                'holders',
                'sales'
            ));
    }

    public function getTotalAccountsChart($user)
    {
        $team = $user->userInfo->team->team_code;
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
 
        $first = Account::whereNull('deleted_at')->first();   
        $last = Account::whereNull('deleted_at')->orderby('created_at','desc')->first();
        
        if (!empty($first))
        {
            $interval = (intval(substr($last->created_at,5,2)) + (12 * (substr($last->created_at,0,4) - substr($first->created_at,0,4)) )) - intval(substr($first->created_at,5,2));
            for ($i=0; $i<=$interval; $i++)
            { 
                //echo date("Y-m-d", strtotime($from."+".$i." month")).'<br>';
                $month = date("Y-m", strtotime(substr($first->created_at,0,7)."+".$i." month"));

                if ($user->role == 1 || ($team == 'acc' && $user->role == 2))
                    $count[$month] = count(Account::whereNull('delete_flag')->where('created_at','like','%'.$month.'%')->get());
                else if($user->role == 2 && $team == 'sp' && in_array($user->userInfo->emp_id,$managers))
                    $count[$month] = count(Account::whereNull('delete_flag')->where('created_at','like','%'.$month.'%')->where('nature','like',['%001%','%002%','%003%'])->get());
                else if($user->role == 2 && $team == 'dev' && in_array($user->userInfo->emp_id,$managers))
                    $count[$month] = count(Account::whereNull('delete_flag')->where('created_at','like','%'.$month.'%')->where('nature','like','%NP004%')->get());
                else if($user->role == 4 && ($user->UserInfo->team->team_code != 'busi'))
                    $count[$month] = count(Account::whereNull('delete_flag')->where('created_at','like','%'.$month.'%')->where($team.'_team','like','%'.$user->emp_id.'%')->get());
                else
                    $count[$month] = count(Account::whereNull('delete_flag')->where('created_id',$user->userid)->where('created_at','like','%'.$month.'%')->get());
            }
        }
        else
        {
            $count[] = [];
        }

        foreach($count as $n => $row)
        {
            
            $dates[] = date('M y',strtotime($n));
        }
        
        $months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        $chart = new AccountTotal($dates);
        $chart->dataset(
            'Total Accounts', 
            'column', 
            array_values($count)
        )->options(['color' => '#00c1ae']);

        return $chart;
    }

    public function getAccountStatusesChart($user)
    {
        $team = $user->userInfo->team->team_code;
        $statuses = System::where('systemtype','accountstatus')->where('activeflag','!=',0)->get();
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        foreach($statuses as $row)
        {
            if ($user->role == 1 || ($team == 'acc' && $user->role == 2))
                $status[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('status',$row->systemcode)->get());
            else if($user->role == 2 && $team == 'sp' && in_array($user->userInfo->emp_id,$managers))
                $status[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('status',$row->systemcode)->where('nature','like',['%001%','%002%','%003%'])->get());
            else if($user->role == 2 && $team == 'dev' && in_array($user->userInfo->emp_id,$managers))
                $status[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('status',$row->systemcode)->where('nature','like','%NP004%')->get());
            else if($user->role == 4  && ($user->UserInfo->team->team_code != 'busi'))
                $status[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('status',$row->systemcode)->where($team.'_team','like','%'.$user->emp_id.'%')->get());
            else
                $status[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('status',$row->systemcode)->where('created_id',$user->userid)->get());
        }
        
        $chart = new AccountPerStatus;
        $chart->dataset('No of Accounts per status','pie', [$status['Active'],$status['Not Renewing (Lost)']])->options(['color' => ['#5bbc64','#fb5e64'] ]);

        return $chart;
    }

    public function getAccountPackagesChart($user)
    {
        $team = $user->userInfo->team->team_code;
        $packages = System::where('systemtype','nature')->where('activeflag','!=',0)->get();
        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        foreach($packages as $row)
        {
            if ($user->role == 1 || $team == 'acc')
                $package[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('nature','like','%'.$row->systemcode.'%')->get());
            else
                $package[$row->systemdesc] = count(Account::whereNull('delete_flag')->where('nature','like','%'.$row->systemcode.'%')->where('acc_assigned',$user->userid)->get());
        }
        
        $chart = new AccountPerPackage;
        $chart->dataset('No of Accounts per package','pie', [$package['SEM'],$package['FB'],$package['SEO'],$package['Web Development'],$package['Baidu'],$package['Weibo'],$package['WeChat'],$package['Blog Content'],$package['Social Media Management'],$package['Post Paid SEM']])->options(['color' => ['#c983be','#3594d8','#977acf','#ff8641'] ]);

        return $chart;
    }

    public function getTotalLeadsChart($user)
    {
        $first = Lead::first();   
        $last = Lead::orderby('created_at','desc')->first();
        
        if (!empty($first))
        {
            $interval = (intval(substr($last->created_at,5,2)) + (12 * (substr($last->created_at,0,4) - substr($first->created_at,0,4)) )) - intval(substr($first->created_at,5,2));

            for ($i = 0; $i <= $interval; $i++)
            {
                $month = date("Y-m", strtotime(substr($first->created_at,0,7)."+".$i." month"));

                if ($user->role == 1)
                    $count[$month] = count(Lead::where('created_at','like','%'.$month.'%')->get());
                else
                    $count[$month] = count(Lead::where('created_by',$user->userid)->where('created_at','like','%'.$month.'%')->get());
            }
        }
        else
        {
            $count[] = [];    
        }

        foreach($count as $n => $row)
        {
            $dates[] = date('M y',strtotime($n));
        }
        
        $chart = new LeadTotal($dates);
        $chart->dataset(
            'Total Leads', 
            'column', 
            array_values($count)
        )->options(['color' => '#00c1ae']);

        return $chart;
    }

    public function getLeadStatusesChart($user)
    {
        $statuses = System::where('systemtype','leadstatus')->get();

        foreach($statuses as $row)
        {
            if ($user->role == 1)
                $status[$row->systemdesc] = count(Lead::where('status',$row->systemcode)->get());
            else
                $status[$row->systemdesc] = count(Lead::where('status',$row->systemcode)->where('created_by',$user->userid)->get());
        }
        
        $chart = new LeadPerStatus;
        $chart->dataset(
            'No of Leads',
            'pie', 
            [$status['Super Hot'],$status['Hot'], $status['Warm'], $status['Cold'], $status['Converted'], $status['Lost'] ]
        )->options(['color' => ['#fb5e64','#ff8641','#5bbc64','#3594d8','#977acf','#c983be'] ]);

        return $chart;
    }

    public function getTotalEmployeesChart($user)
    {
        $branches = ['sg','ph'];

        foreach($branches as $row)
        {
            $branch[$row] = count(Employee::where('branch',$row)->get());
        }

        $chart = new EmployeeTotal;
        $chart->dataset(
            'Total Employees', 
            'pie', 
            [$branch['sg'], $branch['ph']]
        )->options(['color' => '#80173d','#cb2827']);

        return $chart;
    }

    public function getEmployeeStatusesChart($user)
    {
        $statuses = System::where('systemtype','employmentstatus')->where('activeflag','!=',0)->get();

        foreach($statuses as $row)
        {
            $status[$row->systemdesc] = count(Employee::where('status',$row->systemcode)->get());
        }
        
        $chart = new EmployeePerStatus;
        $chart->dataset(
            'No of Employees',
            'pie', 
            [$status['Probation'],$status['Permanent'], $status['Resigned'], $status['Part-Time'], $status['Intern'], $status['Terminated'] ]
        )->options(['color' => ['#ff8641','#00c1ae','#3594d8','#c983be','#977acf'] ]);

        return $chart;
    }

    public function getEmployeeDepartmentsChart($user)
    {
        $departments = System::where('systemtype', 'department')->where('activeflag', '1')->get();

        foreach($departments as $row)
        {
            $department[$row->systemcode] = count(Employee::where('dept_code',$row->systemcode)->get());
        }

        $chart = new EmployeePerDepartment;
        $chart->dataset(
            'No of Employees',
            'pie', 
            [$department['DP001'],$department['DP002'], $department['DP003'], $department['DP004'], $department['DP005'],$department['DP007'],$department['DP008'],$department['DP009'],$department['DP010'] ]
        )->options(['color' => ['#3594d8','#fb5e64','#5bbc64','#00c1ae','#977acf', '#2c3e50', '#c0392b', '#f1c40f', '#16a085'] ]);

        return $chart;
    }

    public function filterAccountTotal($by,$userid,$from,$to)
    {
        
        $accounts = $by == 'date' ?
                    Account::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->orderby('created_at','asc')->get()
                    : Account::whereNull('delete_flag')->where('acc_assigned',$userid)->get();

        if (empty($accounts->toArray()))
            return response()->json([],200);

        $from = $by == 'date' ? $from : $accounts->first()->created_at;
        $to = $by == 'date' ? $to : $accounts->last()->created_at;

        $datefrom = substr($from,5,2);
        $dateto = substr($to,5,2);
        
        $count_accounts = count($accounts) - 1;

        $interval = (intval(substr($to,5,2)) + (12 * (substr($to,0,4) - substr($from,0,4)) )) - intval(substr($from,5,2));
        for ($i=0; $i<=$interval; $i++) { 
            //echo date("Y-m-d", strtotime($from."+".$i." month")).'<br>';
            $date = date("Y-m", strtotime($from."+".$i." month"));
            $date_from = $i == 0 ? date("Y-m-d", strtotime($from."+".$i." month")) : $date.'-01';
            $date_to = $i == $interval ? $to : $date.'-31';
            $countAccounts = $by == 'date' ?
                             count(Account::whereNull('delete_flag')->whereBetween('created_at',[$date_from,$date_to.' 23:59:59'])->get())
                             :
                             count(Account::whereNull('delete_flag')->whereBetween('created_at',[$date_from,$date_to.' 23:59:59'])->where('acc_assigned',$userid)->get());

            if ($countAccounts > 0)
                $count[date("Y-m", strtotime($from."+".$i." month"))] = $countAccounts;
            else
                $count[date("Y-m", strtotime($from."+".$i." month"))] = 0;
        }
        // foreach($accounts as $n => $row)
        // {
        //     $date = substr($row->created_at,0,7);
        //     $date_from = $n == 0 ? $row->created_at : $date.'-01 00:00:00';
        //     $date_to = $n == $count_accounts ? $row->created_at : $date.'-31 00:00:00';
        //     $countAccounts = count(Account::whereBetween('created_at',[$date_from,$date_to])->get());
            
        //     if ($date == $row->created_at->format('Y-m'))
        //     $count[$date] = $countAccounts;
        //     else
        //     $count[substr($accounts[$n]->created_at,0,5).$r] = 0;
            
        // }
        
        foreach($count as $n => $row)
        {
            $dates[] = date('M y',strtotime($n));
        }
        
        $chart = new AccountTotal($dates);
        $chart->dataset(
            'Total Accounts as of '.date('Y'), 
            'column', 
            array_values($count)
        )->options(['color' => '#00c1ae']);

        $AccountsPerStatus = $this->getAccountStatusesByDate($by,$userid,$from,$to);
        $AccountsPerPackage = $this->getAccountPackagesByDate($by,$userid,$from,$to);

        $countAccounts = array_sum((array)$chart->datasets[0]->values);
        
        return response()->json([
            'chart' => $chart, 
            'datefrom' => date('F d, Y',strtotime($from)), 
            'dateto' => date('F d, Y',strtotime($to)),
            'chartPerStatus' => $AccountsPerStatus,
            'chartPerPackage' => $AccountsPerPackage,
            'count' => $countAccounts 
        ],200);
    }

    public function getAccountStatusesByDate($by,$userid,$from,$to)
    {
        $statuses = System::where('systemtype','accountstatus')->where('activeflag','!=',0)->get();

        foreach($statuses as $row)
        {
            $count = $by == 'date' ? count(Account::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->where('status',$row->systemcode)->get()) : count(Account::whereNull('delete_flag')->where('acc_assigned',$userid)->where('status',$row->systemcode)->get()) ;
            $status[$row->systemdesc] = [ $row->systemdesc, $count ];
        }
        
        $chart = new AccountPerStatus;
        $chart->dataset('No of Accounts per status','pie', [$status['Active'],$status['Not Renewing (Lost)']])->options(['color' => ['#5bbc64','#fb5e64'] ]);

        return $chart;
    }

    public function getAccountPackagesByDate($by,$userid,$from,$to)
    {
        $packages = System::where('systemtype','nature')->where('activeflag','!=',0)->get();
        
        foreach($packages as $row)
        {
            $count = $by == 'date' ? count(Account::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->where('nature','like','%'.$row->systemcode.'%')->get()) : count(Account::whereNull('delete_flag')->where('acc_assigned',$userid)->where('nature','like','%'.$row->systemcode.'%')->get()) ;
            $package[$row->systemdesc] = [ $row->systemdesc, $count ];
        }
        
        $chart = new AccountPerPackage;
        $chart->dataset('No of Accounts per package','pie', [$package['SEM'],$package['FB'],$package['SEO'],$package['Web Development'],$package['Baidu'],$package['Weibo'],$package['WeChat'],$package['Blog Content'],$package['Social Media Management'],$package['Post Paid SEM']])
                ->options(['color' => ['#5bbc64','#fb5e64'] ]);

        return $chart;
    }

    public function excelExportAccounts($by,$empid,$from,$to)
    {
        $accounts = $by == 'date' ?
                    Account::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->orderby('created_at','asc')->get()
                    : 
                    ($by == 'all' ? Account::whereNull('delete_flag')->get() : Account::where('acc_assigned',$empid)->get())
                    ;       

        if(count($accounts) > 0)
        {
            foreach($accounts as $key => $row)
            {
                $nature = explode('|',$row->nature);
                $np = array();
                foreach($nature as $k => $v)
                {
                    $package = System::where('systemcode',$v)->first();
                    $np[] = mb_strimwidth($package->systemdesc,0,7,'');
                }
                                        
                $nature = implode(' , ', $np);

                $holder = empty($row->acc_assigned) ? 'None' : $row->holder->UserInfo->f_name.' '.$row->holder->UserInfo->l_name; 
                
                $exportAccounts[] = [
                    'id'         => $row->id,
                    'created_id' => $row->created_id,
                    'company'    => $row->lead->company,
                    'holder'     => $holder,
                    'sales'      => $row->lead->sales->UserInfo->f_name.' '.$row->lead->sales->UserInfo->l_name,
                    'package'    => $nature,
                    'status'     => $row->accountStatus->systemdesc,
                    'date'       => date('F d, Y', strtotime($row->created_at))
                ];

            }

        }
        else
        {
            $result = '';
        }
        
        if ($by == 'holder')
            $sales = Employee::where('emp_id',$empid)->first();
        
        $excelfilename = $by == 'date' ? 'accounts-report-'.$from.'-to-'.$to : ($by == 'holder' ? strtolower('accounts-report-'.$sales->f_name.'-'.$sales->l_name) : 'accounts-report-all');

        return Excel::create($excelfilename, function($excel) use ($exportAccounts) {

            $excel->sheet('accounts', function($sheet) use ($exportAccounts)
            {
                $sheet->loadView('layouts.excel.accounts.export',['accounts' => $exportAccounts]);
                $sheet->setOrientation('landscape');
            });

        })->download('xlsx');
    }

    public function filterLeadTotal($by,$userid,$from,$to)
    {
        $leads = $by == 'date' ?
                    Lead::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->orderby('created_at','asc')->get()
                    : Lead::whereNull('delete_flag')->where('created_by',$userid)->get();

        if (empty($leads->toArray()))
            return response()->json([],200);

        $from = $by == 'date' ? $from : $leads->first()->created_at;
        $to = $by == 'date' ? $to : $leads->last()->created_at;

        $datefrom = substr($from,5,2);
        $dateto = substr($to,5,2);
        
        $count_leads = count($leads) - 1;

        $interval = (intval(substr($to,5,2)) + (12 * (substr($to,0,4) - substr($from,0,4)) )) - intval(substr($from,5,2));
        for ($i=0; $i<=$interval; $i++) { 
            //echo date("Y-m-d", strtotime($from."+".$i." month")).'<br>';
            $date = date("Y-m", strtotime($from."+".$i." month"));
            $date_from = $i == 0 ? date("Y-m-d", strtotime($from."+".$i." month")) : $date.'-01';
            $date_to = $i == $interval ? $to : $date.'-31';
            $countLeads = $by == 'date' ?
                             count(Lead::whereNull('delete_flag')->whereBetween('created_at',[$date_from,$date_to.' 23:59:59'])->get())
                             :
                             count(Lead::whereNull('delete_flag')->whereBetween('created_at',[$date_from,$date_to.' 23:59:59'])->where('created_by',$userid)->get());

            if ($countLeads > 0)
                $count[date("Y-m", strtotime($from."+".$i." month"))] = $countLeads;
            else
                $count[date("Y-m", strtotime($from."+".$i." month"))] = 0;
        }
        
        foreach($count as $n => $row)
        {
            $dates[] = date('M y',strtotime($n));
        }
        
        $chart = new LeadTotal($dates);
        $chart->dataset(
            'Total Leads as of '.date('Y'), 
            'column', 
            array_values($count)
        )->options(['color' => '#00c1ae']);

        $LeadsPerStatus = $this->getLeadStatusesByDate($by,$userid,$from,$to);

        $sales = User::where('userid',$userid)->first();

        $countLeads = array_sum((array)$chart->datasets[0]->values);

        return response()->json([
            'leadchart' => $chart,
            'filterby' => $by, 
            'datefrom' => date('F d, Y',strtotime($from)), 
            'dateto' => date('F d, Y',strtotime($to)),
            'sales' => empty($sales) ? '' : $sales->UserInfo->f_name.' '.$sales->UserInfo->l_name,
            'datenow' => date('F d, Y'),
            'leadchartPerStatus' => $LeadsPerStatus,
            'count' => $countLeads 
        ],200);
    }

    public function getLeadStatusesByDate($by,$userid,$from,$to)
    {
        $statuses = System::where('systemtype','leadstatus')->get();

        foreach($statuses as $row)
        {
            $count = $by == 'date' ? count(Lead::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->where('status',$row->systemcode)->get()) : count(Lead::whereNull('delete_flag')->where('created_by',$userid)->where('status',$row->systemcode)->get()) ;
            $status[$row->systemdesc] = [ $row->systemdesc, $count ];
        }
        
        $chart = new LeadPerStatus;
        $chart->dataset(
            'No of Leads',
            'pie', 
            [$status['Super Hot'],$status['Hot'], $status['Warm'], $status['Cold'], $status['Converted'], $status['Lost'] ]
        )->options(['color' => ['#fb5e64','#ff8641','#5bbc64','#3594d8','#977acf','#c983be'] ]);
        
        return $chart;
    }

    public function excelExportLeads($by,$userid,$from,$to)
    {
        $leads = $by == 'date' ?
                    Lead::whereNull('delete_flag')->whereBetween('created_at',[$from,$to.' 23:59:59'])->orderby('created_at','asc')->get()
                    : 
                    ($by == 'all' ? Lead::whereNull('delete_flag')->get() : Lead::where('created_by',$userid)->get())
                    ;
                    
        if(count($leads) > 0)
        {
            foreach($leads as $key => $row)
            {
                $exportLeads[] = [
                    'id'         => $row->id,
                    'created_by' => $row->created_by,
                    'name'       => $row->f_name.' '.$row->l_name, 
                    'company'    => $row->company,
                    'contact'    => empty($row->contact_number) ? 'Not Specified' : $row->contact_number,
                    'sales'      => $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name,
                    'status'     => $row->leadStatus->systemdesc,
                    'date'       => date('F d, Y', strtotime($row->created_at))
                ];

            }

        }
        else
        {
            $result = '';
        }
        
        if ($by == 'sales')
            $sales = User::where('userid',$userid)->first();
        
        $excelfilename = $by == 'date' ? 'leads-report-'.$from.'-to-'.$to : ($by == 'sales' ? strtolower('leads-report-'.$sales->UserInfo->f_name.'-'.$sales->UserInfo->l_name) : 'leads-report-all');

        return Excel::create($excelfilename, function($excel) use ($exportLeads) {

            $excel->sheet('leads', function($sheet) use ($exportLeads)
            {
                $sheet->loadView('layouts.excel.leads.export',['leads' => $exportLeads]);
                $sheet->setOrientation('landscape');
            });

        })->download('xlsx');
    }

    public function getChangePassword()
    {
        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD006","AT010","Accessed module",null,null,request()->ip(),$user->userid,$user->role);
        return view('changepassword')->with(compact('user'));
    }

    public function postChangePassword(Request $request)
    {
        $userid = Auth::user()->userid;
        $oldpass = Auth::user()->password;
        $input = $request->except('_token');
        $user = User::where('userid',$userid)->first();

        $rules = [
            'oldpass'          => 'required|max:20|alpha_dash|old_password:'.$oldpass,
            'newpass'          => 'required|max:20|alpha_dash',
            'confirm_newpass'  => 'required|max:20|alpha_dash|same:newpass'
        ];

        $messages = [
            'required'     => 'This field is required.',
            'alpha_dash'   => 'Please input alpha-numeric characters, dashes and underscores only',
            'min'          => 'This field must be atleast :min characters',
            'max'          => 'This field must be no more than :max characters',
            'old_password' => 'Old password is incorrect',
            'same'         => 'New Password must be the same with Confirm Password'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return 
                redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Some fields has an error, kindly check.',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        }

        $user->password = bcrypt($input['newpass']);
        $user->save();

        app(__('global.Audit'))->logAuditTrail("MD006","AT005","Changed password",null,null,request()->ip(),$user->userid,$user->role);

        Auth::logout();
        Session::flush();
        return 
            redirect()
            ->route('user.login')
            ->with([
                'alert'   => 'success',
                'message' => 'Password has been changed successfully, please login again.',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
    }

    public function logout()
    {
        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD001","AT009","Logged out from the CRM",null,null,request()->ip(),$user->userid,$user->role);
        Auth::logout();
        Session::flush();
        return redirect()->route('user.login');
    }
    
}
