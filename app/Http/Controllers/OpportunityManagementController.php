<?php

namespace App\Http\Controllers;

use App\Opportunity;
use App\OpportunityInfo;
use App\System;
use App\SalesPerformance;
use App\PaginationPerPage;
use App\Industry;
use App\Prospect;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Session;
use Validator;

class OpportunityManagementController extends Controller
{
    public function list($sort = false, $order = false)
    {
        
        $user = Auth::user();
        $field = $sort ? $sort : 'created_at';
        $fieldOrder = $order ? $order : 'desc';

        Session::forget('opportunity');
        Session::forget('direct');

        $opportunities = Opportunity::list($field, $fieldOrder);

        $results = $this->tableOpportunities($opportunities->unique()->values());

        if(request()->ajax())
        {
            return response()->json(['opportunities' => $results, 'role' => $user->role, 'user' => $user->userid], 200);
        }

        app(__('global.Audit'))
            ->logAuditTrail("MD014","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);

        return view('opportunity.list', compact('results', 'user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $all = $request->except('_token');
        $user = Auth::user();
        $input = isset($all['opportunity']) ? $all['opportunity'] : $all['opportunityForm'];
        $rules = array();

        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = implode('|', $emailInput);
        }

        foreach($input as $k => $v)
        {
            if ($k == '_token')
                continue;

            if (in_array($k, ['contract_type','currency','contract_value', 'client_name',
                'mobile_number', 'source', 'status', 'services']))
                $rules[$k] = 'required';
            else if($k == 'email')
                $rules['email'] = 'required';
        }

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            if(request()->ajax()) {
                return response()->json(['errors' => $validator->errors()], 403);
            }
            return redirect()->back()->withErrors($validator)->withInput();
        }

        foreach($input as $k => $v)
        {
            if($k == 'services' || $k =='_token')
                continue;

            $inputArray[$k] = $v;
        }


        $opportunity_services = array();
        if(isset($input['services']))
        {
            foreach($input['services'] as $service)
            {
                $array_services[] = isset($service['systemcode']) ? $service['systemcode'] : $service;
            }
            $opportunity_services = implode('|', $array_services);
        }


        $inputValues = array_merge($inputArray, ['created_by' => $user->userid, 'services' => $opportunity_services]);

        $opportunity_info = Opportunity::where('id', '=', $input['opportunity_id'])->first();

        // If opportunity found, create info
        if(!empty($opportunity_info))
        {
            $opportunities = OpportunityInfo::create($inputValues);
        }
        else
        {  // Create new if 
            $prospect = Prospect::where('id', $input['prospect_id'])->first();

            $opportunities = Opportunity::create([
                'prospect_id' => $prospect->id,
                'company'     => $prospect->lead->company,
            ])->info()->create($inputValues);
        }

        $results = [
            'id'             => $opportunities->id,
            'client_name'    => $opportunities->client_name_desc,
            'contract_type'  => $opportunities->contract_type_desc,
            'contract_value' => $opportunities->contract_value_desc,
            'services'       => $opportunities->services_desc,
            'status'         => $opportunities->status_desc,
            'mobile_number'  => $opportunities->mobile_number_desc,
            'created_at'     => $opportunities->created_at_desc,
            'created_by'     => $opportunities->created_by_desc,
        ];

        app(__('global.Audit'))->logAuditTrail("MD014","AT002","Created Opportunity " . $opportunities->company,null,null,request()->ip(),$user->userid,$user->role);

        if(request()->ajax())
            return response()->json(['opportunity' => $results, 'message' => 'Successfully created the opportunity'], 200);

        return redirect()->back();

      
    }

    public function refresh()
    {
        $opportunities = Opportunity::list('created_at', 'desc');

        $results = $this->tableOpportunities($opportunities->unique()->values());

        return response()->json(['opportunities' => $results], 200);
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $opportunityInfo = OpportunityInfo::where('id', '=', $input['opportunityID'])->first() ;

        if (!empty($opportunityInfo)) {
            $opportunity = Opportunity::where('id', '=', $opportunityInfo->opportunity_id)->first();

            try {
                if (!empty($opportunity))
                {
                    if ($opportunity->info->count() == 1) {
                        return response()->json(['message' => 'You cannot delete last opportunity information.'], 422);
                    }

                    $company = $opportunity->company;
                    $clientName = $opportunityInfo->client_name;
                    $opportunityInfo->delete_flag = 1;
                    $opportunityInfo->deleted_at = Carbon::now();
                    $opportunityInfo->save();

                    app(__('global.Audit'))->logAuditTrail("MD014","AT006","Deleted Opportunity " . $company . ' with Client name ' . $clientName, null, null,request()->ip(),$user->userid,$user->role);

                    return response()->json(['message' => 'Successfully deleted the opportunity information!'], 200);
                }

            }
            catch(Exception $e)
            {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }

        return response()->json(['message' => 'Opportunity ID not found make sure you input correct ID!'], 422);

    }

    public function directSessionConvert(Request $request)
    {
        $all = isset($request->all()['info']) ? $request->all()['info'] : $request->except('_token');

        Session::forget('opportunity');

        if(!isset($request->all()['info'])) {
            foreach (explode('|', $all['services']) as $svc)
            {
                $array_service = System::where('systemcode', $svc)->where('systemtype', 'nature')->first();
                $service[] = $array_service->systemdesc;
            }

            $systemdesc = implode(', ', $service);

            $industry = isset($all['industry']) ? Industry::where('code', $all['industry'])->first() : 'Others';
            $website_url = !empty(isset($all['website_url'])) ? $all['website_url'] : 'Not Specified';

            $all['industry'] = $industry->title;
            $all['services'] = $systemdesc;
            $all['website_url'] = $website_url;
        }

        Session::put(['direct' => $all]);

        return response()->json('',204);
    }

    public function storeSession(Request $request)
    {
        $input = $request->all();
        $rules = array();

        foreach($input as $k => $v)
        {
            if (in_array($k, ['services', 'contract_type', 'contract_value', 'status']) !== 1)
               continue;
            else
                $rules[$k] = 'required';
        }

        $validator = Validator::make($input, $rules);
        if($validator->fails())
        {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();                    
        }

        Session::forget('direct');
        Session::put(['opportunity' => $input]);

        return response()->json(['message' => 'Successfully redirected to prospect conversion'], 200);
    }

    public function search($string)
    {
        $user = Auth::user();

        if(empty($string))
            return false;

        $byCompany = Opportunity::findCompany($string);
        $byClientName = Opportunity::findClientName($string);

        $results = '';

        if (!empty($byCompany->toArray()))
            $opportunities = $byCompany;
        else if (!empty($byClientName->toArray()))
            $opportunities = $byClientName;
        else
            $opportunities = '';

        if(!empty($opportunities))
        {
            $results = $this->tableOpportunities($opportunities);
        }

        return response()->json(['opportunities' => $results, 'user' => $user], 202);
    }
    
    public function searchString($data)
    {
        if(empty($data))
            return false;

        $results = array();

        $byCompany = Opportunity::findCompany($data);
        $byClientName = Opportunity::findClientName($data);

        if (!empty($byCompany->toArray()))
            $opportunities = $byCompany;
        else if (!empty($byClientName->toArray()))
            $opportunities = $byClientName;
        else
            $opportunities = '';

        if(!empty($opportunities))
        {
            foreach($opportunities as $opportunity)
            {
                foreach($opportunity->info as $info)
                {
                    $results[] = [
                        'id'                            => $info->id,
                        'company'                       => $info->opportunity->company,
                        'opportunity_id'                => $info->opportunity_id,
                        'prospect_id'                   => $info->opportunity->prospect_id,
                        'status'                        => $info->status,
                        'contract_type'                 => $info->contract_type,
                        'currency'                      => $info->currency,
                        'contract_value'                => $info->contract_value,
                        'client_name'                   => $info->client_name,
                        'office_number'                 => $info->office_number,
                        'mobile_number'                 => $info->mobile_number,
                        'email'                         => $info->email,
                        'designation'                   => $info->designation,
                        'address'                       => $info->address,
                        'postal_code'                   => $info->postal_code,
                        'city'                          => $info->city,
                        'industry'                      => $info->industry,
                        'website_url'                   => $info->website_url,
                        'services'                      => $info->services,
                        'other_services'                => $info->other_services,
                        'source'                        => $info->source,
                        'other_source'                  => $info->other_source,
                        'landing_page'                  => $info->landing_page,
                        'other_landing'                 => $info->other_landing,
                        'created_at'                    => $info->created_at,
                        'updated_at'                    => $info->updated_at,
                        'created_by'                    => $info->created_by,
                        'updated_by'                    => $info->updated_by
                    ];
                }
            }

            return response()->json(['opportunities' => $results], 200);
        }

        return response()->json(['message' => 'Opportunity not found!'], 200);
    }

    public function getProspect($prospectId) 
    {
        $infos = array();
        $user = Auth::user();

        $opportunities = Opportunity::with('info')->where('prospect_id', $prospectId)->get();
        $results = $this->tableOpportunities($opportunities);
        

        return response()->json(['opportunities' => $results], 200);
    }

     // SYSTEM TYPES
     public function getOptionsData()
     {
         $services = System::where('systemtype', 'nature')->where('activeflag', 1)->get();
         $contract_types = System::where('systemtype', 'contract_type')->where('activeflag', 1)->get();
         $lead_statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();
         $lead_sources = System::where('systemtype', 'leadsource')->where('activeflag', 1)->get();
         $industries = Industry::orderBy('code', 'asc')->get();
         $landings = System::where('systemtype', 'landingpage')->where('activeflag', 1)->get();
 
 
         return response()->json([
             'services'          => $services,
             'contract_types'    => $contract_types,
             'lead_statuses'     => $lead_statuses,
             'lead_sources'      => $lead_sources,
             'industries'        => $industries,
             'landings'          => $landings,
         ], 200);
     }

     public function getOpportunityInfo($id)
     {
         if (empty($id))
         {
             return response()->json(['message' => 'Invalid Opportunity ID'], 403);
         }

         $opportunityInfo = OpportunityInfo::where('id', '=', $id)->first();
         return response()->json(['opportunity' => $opportunityInfo], 200);
     }

     /* HELPER FUNCTIONS */
     function checkIfEmpty($data)
     {
         return !empty($data) ? $data : 'Not Specified';
     }

     function tableOpportunityInfos($infos)
     {
        $data = array();
        
        if(!empty($infos))
        {
            foreach($infos as $row)
            {
                $data[] = [
                    'id'                            => $row->id,
                    'company'                       => $row->company,
                    'opportunity_id'                => $row->opportunity_id,
                    'prospect_id'                   => $row->opportunity->prospect_id,
                    'status'                        => $row->status_desc,
                    'contract_type'                 => $row->contract_type_desc,
                    'contract_value'                => $row->contract_value_desc,
                    'client_name'                   => $row->client_name_desc,
                    'office_number'                 => $row->office_number_desc,
                    'mobile_number'                 => $row->mobile_number_desc,
                    'email'                         => $row->email_desc,
                    'designation'                   => $row->designation_desc,
                    'address'                       => $row->address_desc,
                    'postal_code'                   => $row->postal_code_desc,
                    'city'                          => $row->city_desc,
                    'industry'                      => $row->industry_desc,
                    'website_url'                   => $row->website_url_desc,
                    'services'                      => $row->services_desc,
                    'other_services'                => $row->other_services_desc,
                    'source'                        => $row->source_desc,
                    'other_source'                  => $row->other_source_desc,
                    'landing_page'                  => $row->landing_page_desc,
                    'other_landing'                 => $row->other_landing_desc,
                    'created_at'                    => $row->created_at_desc,
                    'updated_at'                    => $row->updated_at_desc,
                    'created_by'                    => $row->created_by_desc,
                    'updated_by'                    => $row->updated_by_desc
                ];
            }
        }

        return $data;
    }

    function tableOpportunities($opportunities)
    {
        $data = array();

        if(!empty($opportunities))
        {
            foreach($opportunities as $row)
            {
                $data[] = [
                    'id' => $row->id,
                    'company' => $row->company,
                    'client_name' => $row->info[0]->client_name_desc,
                    'contract_type' => $row->info[0]->contract_type_desc,
                    'contract_value' => $row->info[0]->contract_value_desc,
                    'status' => $row->info[0]->status_desc,
                    'mobile_number' => $row->info[0]->mobile_number_desc,
                    'created_at' => $row->info[0]->created_at_desc,
                    'created_by' => $row->info[0]->created_by_desc,
                    'infos' => $this->tableOpportunityInfos($row->info)
                ];
            }
        }

        return $data;
    }

    function getPaginationPerPage()
    {
        $user = Auth::user();
        $items = PaginationPerPage::where('table_name','opportunity')->where('userid', '=', $user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : 10;
        return $perPage;
    }


   

}
