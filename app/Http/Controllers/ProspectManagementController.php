<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Log;
use Mail;

use App\Prospect;
use App\Lead;
use App\Employee;
use App\Manager;
use App\System;
use App\LeadNote;
use App\User;
use App\Country;
use App\Account;
use App\AccountTm;
use App\AccountFb;
use App\AccountPpc;
use App\AccountSeo;
use App\AccountWeb;
use App\AccountBaidu;
use App\AccountWeibo;
use App\AccountWeChat;
use App\AccountBlogContent;
use App\AccountSocialManagement;
use App\AccountPostpaidSEM;
use App\PaginationPerPage;
use App\LeadProspectLockedPeriod;
use App\Contract;
use App\Industry;
use App\Opportunity;
use App\OpportunityInfo;
use App\OpportunityClosed;

use App\Jobs\SendConvertAccountEmail;

use App\Notifications\ProspectUpdatedNotification;
use App\Notifications\ProspectDeletedNotification;
use App\Notifications\ProspectConvertedAccountNotification;

use Carbon\Carbon;

class ProspectManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        $data      = $this->tblProspects();
        $user      = $data['user'];
        $items     = $data['perpage'];
        $prospects = $data['prospects'];
        $result    = $data['result'];
        $managers  = $data['managers'];
        $teamcode  = $data['teamcode'];

        $services = System::where('systemtype', 'nature')->where('activeflag', 1)->get();
        $contract_types = System::where('systemtype', 'contract_type')->where('activeflag', 1)->get();
        $lead_statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();
        $lead_sources = System::where('systemtype', 'leadsource')->where('activeflag', 1)->get();
        $industries = Industry::orderBy('code', 'asc')->get();
        $landings = System::where('systemtype', 'landingpage')->where('activeflag', 1)->get();
        $lockedin = LeadProspectLockedPeriod::where('userid',Auth::user()->userid)->first();

        Session::forget('opportunity');
        Session::forget('direct');

        app(__('global.Audit'))->logAuditTrail("MD009","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);
        if (request()->ajax())
        {
            return response()->json(['result' => $data['result'], 'pagination' => $data['prospects'], 'managers' => $data['managers'], 'teamcode' => $data['teamcode'], 'user' => $data['user'], 'role' => Auth::user()->role ],200);
        }

        $this->forgetAccountSession();
        return view('prospect.list')->with(compact('prospects','result','managers','teamcode','user','items','lockedin','services', 'contract_types',
            'lead_statuses', 'lead_sources', 'industries', 'landings'));
    }

    public function view($id)
    {
        $user = Auth::user();
        $prospect = Prospect::find($id);
        $notes = LeadNote::where('lead_id',$prospect->lead_id)->paginate(5);
        $commentors = LeadNote::where('lead_id',$id)->select('userid')->distinct()->get();

        if (empty($prospect))
        {
            app(__('global.Audit'))->logAuditTrail("MD009","AE005","Prospect not found",null,null,request()->ip(),$user->userid,$user->role);

            return
                    redirect()
                    ->route('lead.list')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Prospect not found or no access to it',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
        }

        if(request()->ajax())
        {
            foreach($notes as $row)
            {
                $leadnotes[] = [
                    'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                    'date' => date_format($row->created_at,'Y-m-d g:i A'),
                    'id'   => $row->id,
                    'note' => $row->note,
                    'created_user' => $row->userid
                ];
            }

            return
                response()
                ->json([
                    'pagination' => $notes,
                    'notes'      => $leadnotes,
                    'user'       => Auth::user()->userid 
                ],200);
        }

        app(__('global.Audit'))->logAuditTrail("MD009","AT001","View prospect ".$prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('prospect.view')->with(compact('prospect','user','notes','commentors'));
    }
    
    public function edit($id)
    {
        $user = Auth::user();
        $prospect = Prospect::find($id);
        $statuses = System::where('systemtype','leadstatus')->where('activeflag',1)->get();
        $notes = LeadNote::where('lead_id',$prospect->lead_id)->paginate(5);
        $services = System::where('systemtype','service')->where('activeflag',1)->get();
        $sources = System::where('systemtype','leadsource')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $landingpages = System::where('systemtype','landingpage ')->where('activeflag',1)->orderBy('systemcode','asc')->get();
        $commentors = LeadNote::where('lead_id',$id)->select('userid')->distinct()->get();
        $lockedin = LeadProspectLockedPeriod::where('userid',$user->userid)->first();
        $industries = Industry::orderBy("title", "asc")->get();  // Industries fetch list
        $salesPersons = Employee::salesPersonName();

        if (empty($prospect))
        {
            app(__('global.Audit'))->logAuditTrail("MD009","AE005","Prospect not found",null,null,request()->ip(),$user->userid,$user->role);

            return
                    redirect()
                    ->route('lead.list')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Prospect not found or no access to it',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
        }

        if(request()->ajax())
        {
            foreach($notes as $row)
            {
                $leadnotes[] = [
                    'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                    'date' => date_format($row->created_at,'Y-m-d g:i A'),
                    'id'   => $row->id,
                    'note' => $row->note,
                    'created_user' => $row->userid
                ];
            }

            return
                response()
                ->json([
                    'pagination' => $notes,
                    'notes'      => $leadnotes,
                    'user'       => Auth::user()->userid 
                ],200);
        }

        app(__('global.Audit'))->logAuditTrail("MD009","AT005","Edit prospect ".$prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('prospect.edit')->with(compact('user','prospect','statuses','services','sources','landingpages','notes','commentors','lockedin', 'industries', 'salesPersons'));
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $prospect = Prospect::find($input['id']);


        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = implode('|', $emailInput);
        }

        $lead = Lead::find($prospect->lead_id);

        foreach($input as $k => $v)
        {
            foreach($prospect->toArray() as $i => $j)
            {
                if (!in_array($i,['id','created_at','updated_at','delete_flag','deleted_at','lock_this_prospect']))
                    if ($i == $k)
                        $prospect->$i = substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v;
            }

            if (in_array($k,['zip_code','contact_number','office_number']))
                $rules[$k] = isset($input[$k]) ? 'numeric' : '';
            else if (in_array($k,['f_name','l_name','designation','street_address','zip_code','city','website','services','other_services','currency', 'contract_value']))
                continue;
            else if ( ($input['source'] != 'SL999' && $k == 'source_other') ||
                      ($input['landing_page'] != 'SL999' && $k == 'landing_other') || $k == 'service_other')
                $rules[$k] = '';
            else if ($k == 'email')
                $rules[$k] = $v != $prospect->lead->email ? 'required|unique:leads' : 'required';
            else
                $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'This field is required.',
            'numeric' => 'This field must be numeric only.',
            'email' => 'This field must be a valid email address',
            'unique' => 'This field is already exist'
        ];


        $validator = Validator::make($input, $rules, $messages);


        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        // Eloquent LeadProspectLockedPeriod by User ID
        $lockedin = LeadProspectLockedPeriod::where('userid',Auth::user()->userid)->first();

        // New input of has locked-in period
        $new_locked = $input['lock_this_prospect'];

        // Old input of has locked-in period
        $old_locked = $prospect->lock_this_prospect;

        if(!empty($lockedin))
        {
            // If old and new input is not match
            if($old_locked != $new_locked)
            {
                // Decrease by 1 if input is Yes and Increase if it's set to No
                $new_locked != 1 ? $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock + 1 : $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock - 1;

                // Exceeding limit warning: 10 times only
                if($lockedin->leadprospect_to_unlock == -1 || $lockedin->leadprospect_to_unlock == 11)
                    return redirect()->back()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Warning: 10 times per one person. You are exceeding!',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);

                // Update the eloquent LeadProspectLockedPeriod
                $prospect->lock_this_prospect = $new_locked;
                $lockedin->update();
            }
        }
        else
        {
            // Update the eloquent prospect that has locked in period
            $prospect->lock_this_prospect = $new_locked;
            // If locked in period is empty, create new row
            $new_locked != 1 ?  LeadProspectLockedPeriod::create(['userid' => Auth::user()->userid]) : LeadProspectLockedPeriod::create(['userid' => Auth::user()->userid, 'leadprospect_to_unlock' => 9]);
        }

        foreach($input as $k => $v)
        {
            if(in_array($k, ['id','lock_this_prospect','status']))
                continue;

            $prospect->lead->$k =substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v;
        }



        // Update the eloquent Lead and Prospect
        $prospect->updated_by = Auth::user()->userid;
        $prospect->updated_at = Carbon::now();
        $prospect->lead->update();
        $prospect->update();
        
        
        
        $admin = User::where('role',1)
                    ->where('id','!=',Auth::user()->id)     
                    ->get();
        foreach($admin as $row)
        {
            $row->notify(new ProspectUpdatedNotification($prospect->lead,$prospect,$prospect->sales->UserInfo));
        }

        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD009","AT005","Updated prospect ".$prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        
        return 
                redirect()
                ->back()
                ->with([
                    'alert'    => 'success',
                    'message'  => 'Prospect ' .$prospect->lead->company.' of ' .$prospect->lead->f_name.' '.$prospect->lead->l_name.' is now updated',
                    'button'   => 'OK',
                    'confirm'  => 'true',
                    'stamp'    => time()
                ]);
    }

    public function delete(Request $request)
    {
        $input = $request->except('_token');

        $prospect = Prospect::where('id',$input['prospectid'])->first();
        $del_prospect = $prospect->lead->company;
        $prospect->delete_flag = 1;
        $prospect->deleted_at = date('Y-m-d H:i:s');
        $prospect->active_flag = null;
        $prospect->save();

        $admin = User::where('role',1)
                    ->where('id','!=',Auth::user()->id)     
                    ->get();

        foreach($admin as $row)
        {
            $row->notify(new ProspectDeletedNotification($prospect->lead,$prospect,$prospect->sales->UserInfo));
        }
        
        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD009","AT006","Deleted prospect ".$del_prospect,null,null,request()->ip(),$user->userid,$user->role);

        

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect '.$del_prospect.' has been deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep1($id)
    {
        $user = Auth::user();
        $prospect = Prospect::find($id);
        $duration = System::where('systemtype', 'campaignduration')->orderBy('systemcode', 'asc')->get();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode','desc')->get();
        $active_contract = Contract::where('prospect_id', $id)->where('is_active', 1)->whereNull('delete_flag')->first();
        $contract_type = System::where('systemtype', 'contract_type')->orderBy('systemcode', 'asc')->get();

        $opportunity = array();
        $opportunity_services = array();

        if(Session::has('opportunity'))
        {
            $opportunity = Session::get('opportunity')['opportunity'];
            foreach($opportunity['services'] as $service) 
            {
                $array_services[] = $service['systemcode'];
            }

            $opportunity_services = $array_services;
        }
        else if (Session::has('direct')) {
            $direct_opportunity = Session::get('direct');
            foreach (explode(', ', $direct_opportunity['services']) as $service_desc)
            {
                $service = System::where('systemdesc', $service_desc)->where('systemtype', 'nature')->first();
                $array_services[] = !empty($service) ? $service->systemcode : 'NP001';
            }

            $opportunity_services = $array_services;
        }

        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        
        app(__('global.Audit'))->logAuditTrail("MD009","AT012","Converting prospect ".$prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

        $nature = System::where('systemtype','nature')->get();

        return view('account.create.firststep')->with(compact('user','prospect','nature','duration','countries','otherduration','active_contract', 'contract_type', 'opportunity', 'opportunity_services', 'direct_opportunity'));
    }

    public function convertStep1($prospect, Request $request)
    {
        $input = $request->except('_token');

        $emailInput = json_decode($input['email']);
        if (isset($emailInput))
        {
            $input['email'] = $emailInput;
        }   
           

        foreach($input as $k => $v)
        {
            // $tm[$k] = is_array($v) ? implode('|',$v) : $v;
            $tm[$k] = is_array($v) ? implode('|',$v) : ($k != 'contract_value' ? (substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v) : $v);

            if (in_array($k, ['f_name', 'nature', 'contract_value', 'contract_type'])) 
                $rules[$k] = 'required';
            else if($k == 'email')
                $rules[$k] = 'required';

            if (strpos($k, 'url'))
                $rules[$k] = isset($input[$k]) ? 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/' : '';
        }

        // dd($input);

        $messages = [
            'required'  => 'This field is required.',
            'numeric'   => 'This field must be numeric only.',
            'mimes'     => 'Historical data must be doc,docx,pdf,xls,xlsx files only'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        
        if (count($emailInput) == 0)
        {
           $tm['email'] = null;
        }
        else
        {
            $splitEmail = implode('|', $emailInput);
            $tm['email'] = $splitEmail;
        }

        Session::put(['account_additional' => $tm]);



        $selectedNature = in_array('NP001',explode('|',$tm['nature'])) ? 'step2' : 
                            (in_array('NP002',explode('|',$tm['nature'])) ? 'step3' : 
                            (in_array('NP003',explode('|',$tm['nature'])) ? 'step4' : 
                            (in_array('NP004',explode('|',$tm['nature'])) ? 'step5' : 
                            (in_array('NP005',explode('|',$tm['nature'])) ? 'step6' : 
                            (in_array('NP006',explode('|',$tm['nature'])) ? 'step7' : 
                            (in_array('NP007',explode('|',$tm['nature'])) ? 'step8' : 
                            (in_array('NP008',explode('|',$tm['nature'])) ? 'step9' :
							(in_array('NP009',explode('|',$tm['nature'])) ? 'step10' : 'step11' ))))))));

        $nextstep = $selectedNature;

        return redirect()->route('prospect.convert.'.$nextstep, $prospect);
    }

    public function getStep2($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP001',Session::get('account_additional')['nature']) )
            return
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'SEM Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        
        $user = Auth::user();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $listings = System::where('systemtype','accountlisting')->where('systemcode', '!=', 'LA003')->orderBy('systemcode','asc')->get();
        $payment = System::where('systemtype','paymentmethod')->orderBy('systemcode','desc')->get();
        $channels = System::where('systemtype','mediachannel')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.secondstep')->with(compact('user','prospect','countries','payment','channels','duration','otherduration','listings'));
    }

    public function convertStep2($prospect,Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'historical_data' && is_array($v))
            {
                foreach($v as $file)
                {
                    $sem[$k][] = $file->getClientOriginalName(); 
                }
                
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            
            $sem[$k] = is_array($v) ? implode('|',$v) : $v;
            
            if ($input['duration'] != 'CD999' && $k == 'duration_other')
                continue;

            // if ($k == 'has_historical_data')
            //     continue;

            if (strpos($k,'datefrom'))
                $rules[$k] = $input['campaign_date_set'] == '1' ? 'required|date|before:campaign_dateto' : '';
            else if (strpos($k,'dateto'))
                $rules[$k] = $input['campaign_date_set'] == '1' ?'required|date|after:campaign_datefrom' : '';
            // else if (strpos($k, 'media_channel.*'))
            //     $rules[$k] = 'required|distinct';
            else if($k == 'has_historical_data')
                $rules['historical_data'] = $input['has_historical_data'] == 1 ? 'required' : '';
            else if($k == 'ad_scheduling_remark')
                $rules[$k] = $sem['ad_scheduling'] == '1' ? 'required' : '';
//            else if ($k == 'historical_data')
//                $rules[$k] = 'mimes:pdf,doc,docx|max:10000';
            else
                $rules[$k] = 'required' ;
        }

        // dd($rules);

        $messages = [
            'required' => 'This field is required.',
            'mimes' => 'Historical data must be doc,docx,pdf,xls,xlsx files only',
        ];
        
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }        

        if($sem['has_historical_data'] == '1')
        {
            
            foreach($request->file('historical_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }

            $sem['historical_data'] = implode('|',$hashed);
            $sem['historical_data_fname'] = implode('|',$original);
        }

        Session::put(['account_sem' => $sem]);

        

       

        $selectedNature = in_array('NP002',explode('|',Session::get('account_additional')['nature'])) ? 'step3' : 
                            (in_array('NP003',explode('|',Session::get('account_additional')['nature'])) ? 'step4' : 
                            (in_array('NP004',explode('|',Session::get('account_additional')['nature'])) ? 'step5' :  
                            (in_array('NP005',explode('|',Session::get('account_additional')['nature'])) ? 'step6' :
                            (in_array('NP006',explode('|',Session::get('account_additional')['nature'])) ? 'step7' : 
                            (in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                            (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                            (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                            (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ))))))));

        $nextstep = $selectedNature;

        if (!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return 
                redirect()
                ->route('contract.summary', $prospect)
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect is now converted to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep3($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP002',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Facebook Advertisement Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $nature = System::where('systemtype','nature')->get();
        $webtype = System::where('systemtype','projecttype')->get();
        $listings = System::where('systemtype','accountlisting')->where('systemcode', '!=', 'LA002')->orderBy('systemcode','asc')->get();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $payment = System::where('systemtype','paymentmethod')->orderBy('systemcode','desc')->get();
        $channels = System::where('systemtype','mediachannel')->get();
        $fb_channels = System::where('systemtype','fbmediachannel')->get();
        $duration = System::where('systemtype','campaignduration')->orderBy('systemcode','asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode','desc')->get();

        return view('account.create.thirdstep')->with(compact('user','prospect','nature','webtype','countries','payment','channels','fb_channels','duration','otherduration','listings'));
    }

    public function convertStep3($prospect,Request $request)
    {
        $input = $request->except('_token');
        // dd($input);
        foreach($input as $k => $v)
        {
            if($k == 'fb_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $fb[$k][] = $file->getClientOriginalName(); 
                }
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }
            
            $fb[$k] = is_array($v) ? implode('|',$v) : (substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if ($input['fb_duration'] != 'CD999' && $k == 'fb_duration_other')
                continue;

            // if ($k == 'has_fb_hisdata')
            // continue;

            if (strpos($k,'dt_from'))
                $rules[$k] =  $input['fb_campaign_date_set'] == '1' ? 'required|date|before:fb_campaign_dt_to' : ''; 
            else if (strpos($k,'dt_to'))
                $rules[$k] =  $input['fb_campaign_date_set'] == '1' ? 'required|date|after:fb_campaign_dt_from' : '';
            else if($k == 'has_fb_hisdata')
                $rules['fb_hist_data'] = $input['has_fb_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required' ;
        }
        
        // dd($input);

        $messages = [
            'required' => 'This field is required.'
        ];
        
        // dd($rules);

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        if($fb['has_fb_hisdata'] == '1')
        {
            foreach($request->file('fb_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }

            $fb['fb_hist_data'] = implode('|',$hashed);
            $fb['fb_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_fb' => $fb]);

        $selectedNature = in_array('NP003',explode('|',Session::get('account_additional')['nature'])) ? 'step4' : 
                            (in_array('NP004',explode('|',Session::get('account_additional')['nature'])) ? 'step5' :  
                            (in_array('NP005',explode('|',Session::get('account_additional')['nature'])) ? 'step6' :
                            (in_array('NP006',explode('|',Session::get('account_additional')['nature'])) ? 'step7' : 
                            (in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                            (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                            (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                            (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' )))))));

        $nextstep = $selectedNature;

        if (!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return 
                redirect()
                ->route('contract.summary', $prospect)
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect is now converted to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep4($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP003',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'SEO Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $keywordsno = System::where('systemtype','keywordsno')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.fourthstep')->with(compact('user','prospect','countries','duration','keywordsno','otherduration'));
    }

    public function convertStep4($prospect,Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'seo_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $seo[$k][] = $file->getClientOriginalName(); 
                }
                
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }
            
            $seo[$k] = is_array($v) ? implode('|',$v) : (substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if ($input['seo_duration'] != 'CD999' && $k == 'seo_duration_other')
                continue;
            
            if ($input['keyword_themes'] != 'NK999' && $k == 'keyword_themes_other')
                continue;
            
            if ($k == $input['seo_total_fee'])
                $rules[$k] = 'required|numeric';

            // if (in_array($k,['has_seo_hisdata','has_keyword_maintenance']))
            //     continue;

            if ($k == 'has_seo_hisdata')
                $rules['seo_hist_data'] = $input['has_seo_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required' ;
        }

        //dd($seo);
        
        $messages = [
            'mimes' => 'Historical data must be doc,docx,pdf,xls,xlsx files only',
            'required' => 'field is required',
            'date' => 'field should be in date format yyyy-mm-dd',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        //dd($seo);

        if($seo['has_seo_hisdata'] == '1')
        {
            foreach($request->file('seo_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }

            $seo['seo_hist_data'] = implode('|',$hashed);
            $seo['seo_hist_data_fname'] = implode('|',$original);
        }
        
        Session::put(['account_seo' => $seo]);

        //dd(Session::get('account_additional'),Session::get('account_sem'),Session::get('account_fb'),Session::get('account_seo'));

        $selectedNature = in_array('NP004',explode('|',Session::get('account_additional')['nature'])) ? 'step5' :  
                            (in_array('NP005',explode('|',Session::get('account_additional')['nature'])) ? 'step6' :
                            (in_array('NP006',explode('|',Session::get('account_additional')['nature'])) ? 'step7' : 
                            (in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                            (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                            (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                            (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ))))));

        $nextstep = $selectedNature;

        if (!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return 
                redirect()
                ->route('contract.summary', $prospect)
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect is now converted to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep5($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP004',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Web Development Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $nature = System::where('systemtype','nature')->get();
        $webtype = System::where('systemtype','projecttype')->get();
        $cmstype = System::where('systemtype','cmstype')->get();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $payment = System::where('systemtype','paymentmethod')->orderBy('systemcode','desc')->get();
        $channels = System::where('systemtype','mediachannel')->get();
        $fb_channels = System::where('systemtype','fbmediachannel')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();

        return view('account.create.fifthstep')->with(compact('user','prospect','nature','webtype','cmstype','countries','payment','channels','fb_channels','duration'));
    }

    public function convertStep5($prospect,Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'web_hist_data' && is_array($v))
            {
                foreach($v as $file)
                {
                    $web[$k][] = $file->getClientOriginalName(); 
                }
                
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }
            
            $web[$k] = is_array($v) ? implode('|',$v) : (substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if ($k == 'has_web_hisdata')
                $rules['web_hist_data'] = $input['has_web_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required' ;
        }

        $messages = [
            'required' => 'This field is required.',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        if($web['has_web_hisdata'] == '1')
        {
            foreach($request->file('web_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }

            $web['web_hist_data'] = implode('|',$hashed);
            $web['web_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_web' => $web]);

        $nextstep = in_array('NP005',explode('|',Session::get('account_additional')['nature'])) ? 'step6' :
                    (in_array('NP006',explode('|',Session::get('account_additional')['nature'])) ? 'step7' : 
                    (in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                    (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                    (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                    (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' )))));

        if (!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return 
                redirect()
                ->route('contract.summary', $prospect)
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect is now converted to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep6($id, Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP005',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Baidu Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $baidu_channels = System::where('systemtype','baidumediachannel')->orderBy('systemcode', 'asc')->get();
        $countries = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $duration = System::where('systemtype', 'campaignduration')->orderBy('systemcode', 'asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.sixthstep')->with(compact('user','prospect','baidu_channels','countries','duration','otherduration'));
    }

    public function convertStep6($prospect, Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'baidu_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $baidu[$k][] = $file->getClientOriginalName();
                }
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            $baidu[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if($input['baidu_duration'] != 'CD999' && $k == 'baidu_duration_other')
                continue;
            
            if ($k == 'baidu_account')
                $rules[$k] = $input['has_baidu_account'] == '1' ? 'required' : '';
            else if ($k == 'baidu_campaign_datefrom')
                $rules[$k] = $input['has_baidu_campaign_dateset'] == '1' ? 'required|date|before:baidu_campaign_dateto' : '';
            else if ($k == 'baidu_campaign_dateto')
                $rules[$k] = $input['has_baidu_campaign_dateset'] == '1' ? 'required|date|after:baidu_campaign_datefrom' : '';
            else if ($k == 'has_baidu_hisdata')
                $rules['baidu_hist_data'] = $input['has_baidu_hisdata'] == '1' ? 'required' : '';
            else if($k == 'baidu_ad_scheduling_remark')
                $rules[$k] = $baidu['baidu_ad_scheduling'] == '1' ? 'required' : '';
            else
                $rules[$k] = 'required';

        }

        $messages = [
            'required' => 'This field is required.'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput()
                             ->with([
                                 'alert'    => 'error',
                                 'message'  => 'Some of the fields has an error, kindly check.',
                                 'button'   => 'OK',
                                 'confirm'  => 'true'
                             ]);
        }

        if($baidu['has_baidu_hisdata'] == '1')
        {
            foreach($request->file('baidu_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }
            
            $baidu['baidu_hist_data'] = implode('|',$hashed);
            $baidu['baidu_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_baidu' => $baidu]);

        $selectedNature = in_array('NP006',explode('|',Session::get('account_additional')['nature'])) ? 'step7' : 
                            (in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                            (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                            (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                            (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ))));
        
        $nextstep = $selectedNature;

        if(!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return
            redirect()->route('contract.summary', $prospect)
                      ->with([
                          'alert'   => 'success',
                          'message' => 'Prospect is now converted to account',
                          'button'  => 'OK',
                          'confirm' => 'true'
                      ]);
        
    }

    public function getStep7($id, Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP006',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Weibo Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $duration = System::where('systemtype', 'campaignduration')->orderBy('systemcode', 'asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.seventhstep')->with(compact('user','prospect','duration','otherduration'));
    }

    public function convertStep7($prospect, Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'weibo_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $weibo[$k][] = $file->getClientOriginalName();
                }
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            $weibo[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if($input['weibo_duration'] != 'CD999' && $k == 'weibo_duration_other')
                continue;

            if(in_array($k, ['weibo_budget','weibo_monthly_budget','weibo_daily_budget','weibo_campaign']))
                continue;
            
            if ($k == 'weibo_account')
                $rules[$k] = $input['has_weibo_account'] == '1' ? 'required' : '';
            else if ($k == 'weibo_campaign_datefrom')
                $rules[$k] = $input['has_weibo_campaign_dateset'] == '1' ? 'required|date|before:weibo_campaign_dateto' : '';
            else if ($k == 'weibo_campaign_dateto')
                $rules[$k] = $input['has_weibo_campaign_dateset'] == '1' ? 'required|date|after:weibo_campaign_datefrom' : '';
            else if ($k == 'has_weibo_hisdata')
                $rules['weibo_hist_data'] = $input['has_weibo_hisdata'] == '1' ? 'required' : '';
            else if($k == 'weibo_ad_scheduling_remark')
                $rules[$k] = $weibo['weibo_ad_scheduling'] == '1' ? 'required' : '';
            else
                $rules[$k] = 'required';

        }

        $messages = [
            'required' => 'This field is required.'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput()
                             ->with([
                                 'alert'    => 'error',
                                 'message'  => 'Some of the fields has an error, kindly check.',
                                 'button'   => 'OK',
                                 'confirm'  => 'true'
                             ]);
        }

        if($weibo['has_weibo_hisdata'] == '1')
        {
            foreach($request->file('weibo_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }
            
            $weibo['weibo_hist_data'] = implode('|',$hashed);
            $weibo['weibo_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_weibo' => $weibo]);

        $selectedNature = in_array('NP007',explode('|',Session::get('account_additional')['nature'])) ? 'step8' :
                            (in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                            (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                            (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' )));
        
        $nextstep = $selectedNature;

        if(!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return
            redirect()->route('contract.summary', $prospect)
                      ->with([
                          'alert'   => 'success',
                          'message' => 'Prospect is now converted to account',
                          'button'  => 'OK',
                          'confirm' => 'true'
                      ]);
        
    }

    public function getStep8($id, Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP007',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'WeChat Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $duration = System::where('systemtype', 'campaignduration')->orderBy('systemcode', 'asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();
        $advertising = System::where('systemtype', 'wechatadvertising')->orderBy('systemcode', 'asc')->get();

        return view('account.create.eightstep')->with(compact('user','prospect','countries','duration','otherduration','advertising'));
    }

    public function convertStep8($prospect,Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'wechat_hist_data' && is_array($v))
            {
                foreach($v as $file)
                {
                    $wechat[$k][] = $file->getClientOriginalName(); 
                }
                
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            $wechat[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);
            
            if ($k == in_array($k, ['wechat_age_from','wechat_age_to','wechat_marital','wechat_gender','wechat_handphone','wechat_education']))
                continue;

            if ($k == in_array($k, ['wechat_type','has_wechat_menu_setup']))
                $rules[$k] = $input['has_wechat_type'] == '1' ? 'required' : '';
            else if ($k == in_array($k, ['wechat_advertising_type','wechat_location']))
                $rules[$k] = $input['has_wechat_advertising'] == '1' ? 'required' : '';
            else if ($k == 'has_wechat_hisdata')
                $rules['wechat_hist_data'] = $input['has_wechat_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'This field is required.',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        if($wechat['has_wechat_hisdata'] == '1')
        {
            foreach($request->file('wechat_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }

            $wechat['wechat_hist_data'] = implode('|',$hashed);
            $wechat['wechat_hist_data_fname'] = implode('|',$original);
        }
        
        $wechat['wechat_advertising_type'] = !isset($wechat['wechat_advertising_type']) ? null : $wechat['wechat_advertising_type'];
        // dd($wechat);
        Session::put(['account_wechat' => $wechat]);

        $nextstep = in_array('NP008',explode('|',Session::get('account_additional')['nature'])) ? 'step9' : 
                    (in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                    (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ));

        if (!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return 
                redirect()->route('contract.summary', $prospect)
                ->with([
                    'alert'   => 'success',
                    'message' => 'Prospect is now converted to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function getStep9($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP008',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Blog Content Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $nature = System::where('systemtype','nature')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.ninthstep')->with(compact('user','prospect','nature','duration','otherduration'));
    }

    public function convertStep9($prospect, Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'blog_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $blog[$k][] = $file->getClientOriginalName();
                }
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            $blog[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);
                
            if ($k == 'blog_monthly_post')
                $rules[$k] = isset($input['blog_monthly_post']) ? 'numeric' : '';
            else if ($k == 'blog_total_post')
                $rules[$k] = isset($input['blog_total_post']) ? 'numeric' : 'required';
            else if($k == 'has_blog_hisdata')
                $rules['blog_hist_data'] = $input['has_blog_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'This field is required.'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput()
                             ->with([
                                 'alert'    => 'error',
                                 'message'  => 'Some of the fields has an error, kindly check.',
                                 'button'   => 'OK',
                                 'confirm'  => 'true'
                             ]);
        }

        if($blog['has_blog_hisdata'] == '1')
        {
            foreach($request->file('blog_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }
            
            $blog['blog_hist_data'] = implode('|',$hashed);
            $blog['blog_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_blog' => $blog]);

        $selectedNature = in_array('NP009',explode('|',Session::get('account_additional')['nature'])) ? 'step10' :
                          (in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ) ;
        
        $nextstep = $selectedNature;

        if(!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return
            redirect()->route('contract.summary', $prospect)
                      ->with([
                          'alert'   => 'success',
                          'message' => 'Prospect is now converted to account',
                          'button'  => 'OK',
                          'confirm' => 'true'
                      ]);
    }

    public function getStep10($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP009',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Social Media Management Package is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $nature = System::where('systemtype','nature')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.tenthstep')->with(compact('user','prospect','nature','duration','otherduration'));
    }

    public function convertStep10($prospect, Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            if($k == 'social_histdata' && is_array($v))
            {
                foreach($v as $file)
                {
                    $social[$k][] = $file->getClientOriginalName();
                }
                $rules[$k.'.*'] = 'mimes:pdf,doc,docx,xls,xlsx|max:10000';
                continue;
            }

            $social[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if($input['social_duration'] != 'CD999' && $k == 'social_duration_other')
                continue;
                
            if ($k == 'social_monthly_post')
                $rules[$k] = 'required|numeric';
            else if ($k == 'social_media_budget')
                $rules[$k] = $input['has_social_media_budget'] == '1' ? 'required:numeric' : '';
            else if($k == 'has_social_hisdata')
                $rules['social_hist_data'] = $input['has_social_hisdata'] == 1 ? 'required' : '';
            else
                $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'This field is required.'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput()
                             ->with([
                                 'alert'    => 'error',
                                 'message'  => 'Some of the fields has an error, kindly check.',
                                 'button'   => 'OK',
                                 'confirm'  => 'true'
                             ]);
        }

        if($social['has_social_hisdata'] == '1')
        {
            foreach($request->file('social_hist_data') as $file)
            {
                $origfname = $file->getClientOriginalName();
                $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                $file->move(storage_path() . DIRECTORY_SEPARATOR . 'temp', $fname);
                $hashed[] = $fname;
                $original[] = $origfname;
            }
            
            $social['social_hist_data'] = implode('|',$hashed);
            $social['social_hist_data_fname'] = implode('|',$original);
        }

        Session::put(['account_social' => $social]);

        $selectedNature = in_array('NP010',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ;
        
        $nextstep = $selectedNature;

        if(!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return
            redirect()->route('contract.summary', $prospect)
                      ->with([
                          'alert'   => 'success',
                          'message' => 'Prospect is now converted to account',
                          'button'  => 'OK',
                          'confirm' => 'true'
                      ]);
    }

    public function getStep11($id,Request $request)
    {
        $prospect = Prospect::find($id);
        if (empty($prospect))
            return 
                redirect()
                ->route('prospect.list')
                ->with([
                    'alert'   => 'error',
                    'message' => 'Prospect ID '.$id.' does not exist. Please check the list first before converting it to account',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        if (empty(Session::get('account_additional')) || Session::has('account_additional') && strpos('NP010',Session::get('account_additional')['nature']) )
            return 
                redirect()
                ->route('prospect.convert.step1',$id)
                ->with([
                    'alert'   => 'error',
                    'message' => 'Post Paid SEM Service is not selected in prospect conversion or is not a valid session',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);

        $user = Auth::user();
        $nature = System::where('systemtype','nature')->get();
        $duration = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();

        return view('account.create.eleventhstep')->with(compact('user','prospect','nature','duration','otherduration'));
    }

    public function convertStep11($prospect, Request $request)
    {
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            $postpaid[$k] = is_array($v) ? implode('|',$v) : ( substr($v,5,1) == ',' ? implode('|',explode(',',$v)) : $v);

            if($input['postpaid_duration'] != 'CD999' && $k == 'postpaid_duration_other')
                continue;
                
            if ($k == 'postpaid_campaign_datefrom')
                $rules[$k] = $input['postpaid_campaign_date_set'] == '1' ? 'required|date|before:postpaid_campaign_dateto' : '';
            else if ($k == 'postpaid_campaign_dateto')
                $rules[$k] = $input['postpaid_campaign_date_set'] == '1' ? 'required|date|after:postpaid_campaign_datefrom' : '';
            else if($k == 'postpaid_am_fee')
                $rules[$k] = 'required|numeric';
            else
                $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'This field is required.'
        ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
        {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput()
                             ->with([
                                 'alert'    => 'error',
                                 'message'  => 'Some of the fields has an error, kindly check.',
                                 'button'   => 'OK',
                                 'confirm'  => 'true'
                             ]);
        }


        Session::put(['account_postpaid' => $postpaid]);

        $selectedNature = in_array('NP011',explode('|',Session::get('account_additional')['nature'])) ? 'step11' : '' ;
        
        $nextstep = $selectedNature;

        if(!empty($nextstep))
            return redirect()->route('prospect.convert.'.$nextstep,$prospect);
        else
            $this->convertProspect($prospect);

        return
            redirect()->route('contract.summary', $prospect)
                      ->with([
                          'alert'   => 'success',
                          'message' => 'Prospect is now converted to account',
                          'button'  => 'OK',
                          'confirm' => 'true'
                      ]);
    }

    function convertProspect($prospect_id)
    {
        $additional = Session::get('account_additional');
        $sem = Session::get('account_sem');
        $fb = Session::get('account_fb');
        $seo = Session::get('account_seo');
        $web = Session::get('account_web');
        $baidu = Session::get('account_baidu');
        $weibo = Session::get('account_weibo');
        $wechat = Session::get('account_wechat');
        $blog = Session::get('account_blog');
        $social = Session::get('account_social');
        $postpaid = Session::get('account_postpaid');
        $opportunity = Session::get("opportunity")['opportunity'];
        $direct_opportunity = Session::get('direct');

        $tm_id = AccountTm::max('id');
        if (!empty($sem))
        {
            foreach($sem as $k => $v)
            {
                if ($k == 'has_historical_data')
                    continue;
                    

                $account_sem[$k] = $v;
            }

            AccountPpc::create($sem);
            $ppc_id = AccountPpc::max('id');
            
            if (isset($sem['historical_data']))
            {
                foreach(explode('|',$sem['historical_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/sem/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($fb))
        {
            foreach($fb as $k => $v)
            {
                if ($k == 'has_fb_hisdata')
                    continue;

                $account_fb[$k] = $v;
            }

            AccountFb::create($fb);
            $fb_id = AccountFb::max('id');

            if (isset($fb['fb_hist_data']))
            {
                foreach(explode('|',$fb['fb_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/fb/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($seo))
        {
            foreach($seo as $k => $v)
            {
                if ($k == 'has_seo_hisdata')
                    continue;

                if ($k == 'has_keyword_maintenance')
                {
                    $account_seo['keyword_maintenance'] = $v == 0 ? 0 : '';
                }

                $account_seo[$k] =  $v;
            }

            AccountSeo::create($seo);
            $seo_id = AccountSeo::max('id');

            if (isset($seo['seo_hist_data']))
            {
                foreach(explode('|',$seo['seo_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/seo/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($web))
        {
            foreach($web as $k => $v)
            {
                if($k == 'has_web_hisdata')
                    continue;

                $account_web[$k] = $v;
            }

            AccountWeb::create($web);
            $web_id = AccountWeb::max('id');

            if (isset($web['web_hist_data']))
            {
                foreach(explode('|',$web['web_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/web/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
           
        }

        if (!empty($baidu))
        {
            foreach($baidu as $k => $v)
            {
                if ($k == 'has_baidu_hisdata')
                    continue;

                $account_baidu[$k] =  $v;
            }

            AccountBaidu::create($baidu);
            $baidu_id = AccountBaidu::max('id');

            if (isset($baidu['baidu_hist_data']))
            {
                foreach(explode('|',$baidu['baidu_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/baidu/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($weibo))
        {
            foreach($weibo as $k => $v)
            {
                if ($k == 'has_weibo_hisdata')
                    continue;

                $account_weibo[$k] = $v;
            }

            AccountWeibo::create($weibo);
            $weibo_id = AccountWeibo::max('id');

            if (isset($weibo['weibo_hist_data']))
            {
                foreach(explode('|',$weibo['weibo_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/weibo/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($wechat))
        {
            foreach($wechat as $k => $v)
            {
                if ($k == 'has_wechat_hisdata')
                    continue;

                $account_wechat[$k] = $v;
            }

            AccountWeChat::create($wechat);
            $wechat_id = AccountWeChat::max('id');

            if (isset($wechat['wechat_hist_data']))
            {
                foreach(explode('|',$wechat['wechat_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/wechat/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($blog))
        {
            foreach($blog as $k => $v)
            {
                if ($k == 'has_blog_hisdata')
                    continue;

                $account_blog[$k] = $v;
            }

            AccountBlogContent::create($blog);
            $blog_id = AccountBlogContent::max('id');

            if (isset($blog['blog_hist_data']))
            {
                foreach(explode('|',$blog['blog_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/blog/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($social))
        {
            foreach($social as $k => $v)
            {
                if ($k == 'has_social_hisdata')
                    continue;

                $account_social[$k] = $v;
            }

            AccountSocialManagement::create($social);
            $social_id = AccountSocialManagement::max('id');

            if (isset($social['social_hist_data']))
            {
                foreach(explode('|',$social['social_hist_data']) as $row)
                {                           
                    \File::copy(storage_path().'/temp'.DIRECTORY_SEPARATOR.$row,storage_path().'/social/historicaldata'.DIRECTORY_SEPARATOR.$row);
                }
            }
        }

        if (!empty($postpaid))
        {
            foreach($postpaid as $k => $v)
            {
                $account_postpaid[$k] = $v;
            }

            AccountPostpaidSEM::create($postpaid);
            $postpaid_id = AccountPostpaidSEM::max('id');
        }
        
        /* Update the account to inactive */
        $account = Account::where('prospect_id', $prospect_id)->where('active_flag', 1)->whereNull('delete_flag')->where('status', 'AS001')->first();

        if(!empty($account))
        {
            $account->active_flag = null;
            $account->update();
        }

         // Find the prospect id on prospect model
        $prospect = Prospect::find($prospect_id);

        $account_contract = Contract::where('prospect_id', '=', $prospect_id)->where('is_active',1)->whereNull('delete_flag')->count();
        $first_contract = Contract::where('prospect_id', '=', $prospect_id)->where('is_active', 1)->whereNull('delete_flag')->orderBy('id', 'asc')->first();

        if($account_contract == 0)
        {
            $day90OnFirstContract = Carbon::now()->addDays(90);
            $day120OnFirstContract = Carbon::now()->addDays(120);
        } else 
        {
            $day90OnFirstContract = $first_contract->day_90;
            $day120OnFirstContract = $first_contract->day_120;
        }
       
        $account_created = Account::create([
            'opportunity_info_id' => isset($opportunity) ? $opportunity['opportunity_info'] : null,
            'prospect_id'         => $prospect_id,
            'contract_id'         => Contract::max('id') + 1,
            'nature'              => $additional['nature'],
            'tm_id'               => $tm_id + 1,
            'ppc_id'              => isset($ppc_id) ? $ppc_id : 0,
            'fb_id'               => isset($fb_id) ? $fb_id : 0,
            'seo_id'              => isset($seo_id) ? $seo_id : 0,
            'web_id'              => isset($web_id) ? $web_id : 0,
            'baidu_id'            => isset($baidu_id) ? $baidu_id : 0,
            'weibo_id'            => isset($weibo_id) ? $weibo_id : 0,
            'wechat_id'           => isset($wechat_id) ? $wechat_id : 0,
            'blog_id'             => isset($blog_id) ? $blog_id : 0,
            'social_id'           => isset($social_id) ? $social_id : 0,
            'postpaid_sem_id'     => isset($postpaid_id) ? $postpaid_id : 0,
            'created_id'          => Auth::user()->userid,
            'active_flag'         => 1
          ]);
  
        
          $account_additional_created = AccountTm::create([
              'contact_page_url'          => $additional['contact_page_url'],
              'ty_page_url'               => $additional['ty_page_url'],
              'ga_required'               => $additional['ga_required'],
              'ga_remark'                 => $additional['ga_remark'],
              'implementations'           => $additional['implementations'],
              'implementations_remark'    => $additional['implementations_remark'],
              'web_login'                 => $additional['web_login']
          ]);

        /*---------------- Contract number ------------------*/
        $numberOfZeros = 3; // Number of leading zeros on contract number
        $contracts = Contract::max('contract_number'); // Select the max value of contract number on contract model
        $contract = explode('-', $contracts); // Explode the max value of contract_number by using '-'

        // If the contract number year and month is the same as today's year and month
        if($contract[0] == date('ym')) 
            $counter = $contract[1] + 1; // Increment the max value by 1
        else
            $counter = 1; // Back to the count of 1

        $contract_number = date('ym') . "-" . str_pad($counter, $numberOfZeros, '0', STR_PAD_LEFT); // Setting of all data to generate contract number
        /*-----------------End of Contract Number Creation-----------*/

        // Create new data for contract model
        Contract::create([
            'prospect_id'       => $prospect_id,
            'account_id'        => Account::max('id'),
            'contract_number'   => $contract_number,
            'currency'          => $additional['currency'],
            'contract_value'    => $additional['contract_value'],
            'company'           => $prospect->lead->company,
            'f_name'            => $additional['f_name'],
            'l_name'            => $additional['l_name'],
            'designation'       => $additional['designation'],
            'contact_number'    => $additional['contact_number'],
            'email'             => $additional['email'],
            'created_by'        => Auth::user()->userid,
            'contract_type'     => $additional['contract_type'],
            'day_90'            => $day90OnFirstContract,
            'day_120'           => $day120OnFirstContract
        ]);

        if(isset($opportunity) || isset($direct_opportunity)) {
            $info = OpportunityInfo::where("id", "=", (!empty($opportunity) ? $opportunity['opportunity_info'] : $direct_opportunity['id']))->first();
            $info->status = 'LS005';
            $info->save();
        }

        // Get the old value of status 'prospect'
        $oldval = [
            'status' => $prospect->status
        ];

        // If prospect is ex-client
        if($prospect->lead->is_convert_again == 1)
            $prospect->ex_client_flag = 1;

        // Set the status to 'Closed'
        $prospect->status = 'LS005';
        
        // If locked-in period is set to 'Yes' execute this!
        if($prospect->lock_this_prospect == 1)
        {
            $lockedin = LeadProspectLockedPeriod::where('userid', Auth::user()->userid)->first();

            if(!empty($lockedin))
            {
                $lockedin->leadprospect_to_unlock = $lockedin->leadprospect_to_unlock + 1;
                $lockedin->update();
            }
        }

        // Set again the locked-in period when converting
        $prospect->lock_this_prospect = 0;
        
        // Update the prospect
        $prospect->update();

        // Get the new value of status 'prospect'
        $newval = [
            'status' => $prospect->status
        ];

        $user = Auth::user();
        app(__('global.Audit'))->logAuditTrail("MD002","AT013","Converted prospect ".$prospect->lead->company,$oldval,$newval,request()->ip(),$user->userid,$user->role);

        $account = Account::where('prospect_id',$prospect_id)->where('active_flag', 1)->first();
        
        $managers_list = Manager::all();
        foreach($managers_list as $row)
        {
            if ($row->team->team_code == 'dev' && !in_array('NP004',explode('|',$additional['nature'])))
                continue;
            
            if ( $row->team->team_code == 'sp' && (!in_array('NP001',explode('|',$additional['nature'])) && !in_array('NP002',explode('|',$additional['nature'])) && !in_array('NP003',explode('|',$additional['nature']))) )
                continue;
            
            $emp = Employee::where('emp_id', $row->emp_id)->first();
    
            if ($row['dept_id'] == 2)
                continue;

            if($emp['userInfo']['email'] == '')
                continue;

            $managers[$emp['userInfo']['userid']] = [
                'email' => $emp['userInfo']['email'],
                'team'  => $emp['team']['team_code'],
                'name'  => $emp['f_name'].' '.$emp['l_name']
            ];
        }

        $finance = User::where('role',3)
                        ->where('id', '!=', Auth::user()->id)
                        ->get();

        foreach($finance as $row)
        {
            $emp = Employee::where('emp_id',$row->emp_id)->first();

            $managers[$emp->userInfo->userid] = [
                'email' => $emp->userInfo->email,
                'team'  => $emp->team->team_code,
                'name'  => $emp->f_name.' '.$emp->l_name
            ];
        }

        $administrators = User::where('role', 1)
                            ->where('id','!=', Auth::user()->id)   
                            ->get();

        foreach($administrators as $admin)
        {
            $managers[$admin->userid] = [
                'email' => $admin->email,
                'team' => $admin->UserInfo->team->team_code,
                'name' => $admin->UserInfo->f_name.' '.$admin->UserInfo->l_name
            ];
        }
        
        foreach(explode('|',$additional['nature']) as $row)
        {
            $package = System::where('systemcode',$row)->first();
            $nature[] = $package->systemdesc;
        }

        $prospect = Prospect::find($prospect_id);
        
        $emailDetails = [ 
                            'prospect' => $prospect->lead->company, 
                            'nature' => implode(', ',$nature), 
                            'account' => $account,
                            'contract' => $account->contract->contract_number,
                            'creator' => !empty($prospect->sales->UserInfo->f_name) ? $prospect->sales->UserInfo->f_name . ' ' . $prospect->sales->UserInfo->l_name : Auth::user()->UserInfo->f_name . ' ' . Auth::user()->UserInfo->l_name,
                            'convert' => Auth::user()->UserInfo->f_name . ' ' . Auth::user()->UserInfo->l_name
                        ];
        \Mail::send('layouts.emails.prospectconvertaccount',$emailDetails,function($message) use ($emailDetails, $managers, $sem, $fb, $seo, $web, $baidu, $weibo, $wechat, $blog, $social){
            $mails = array_pluck($managers, 'email');
            $message->bcc($mails)->subject('CRM System - #' . $emailDetails['contract'] . ' New Contract - ' . $emailDetails['prospect']);
            $message->from('noreply@oom.com.sg','CRM Admin');

            if (isset($sem['historical_data']))
            {
                foreach(explode('|',$sem['historical_data']) as $file)
                {
                    foreach(explode('|', $sem['historical_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'sem - ' . $name]);
                    }                    
                }
            }

            if (isset($fb['fb_hist_data']))
            {
                foreach(explode('|',$fb['fb_hist_data']) as $file)
                {
                    foreach(explode('|', $fb['fb_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'fb - ' . $name]);
                    }                    
                }
            }

            if (isset($seo['seo_hist_data']))
            {
                foreach(explode('|',$seo['seo_hist_data']) as $file)
                {
                    foreach(explode('|', $seo['seo_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'seo - ' . $name]);
                    }                    
                }
            }

            if (isset($web['web_hist_data']))
            {
                foreach(explode('|',$web['web_hist_data']) as $file)
                {
                    foreach(explode('|', $web['web_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'web - ' . $name]);
                    }                    
                }
            }

            if (isset($baidu['baidu_hist_data']))
            {
                foreach(explode('|',$baidu['baidu_hist_data']) as $file)
                {
                    foreach(explode('|', $baidu['baidu_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'baidu - ' . $name]);
                    }                    
                }
            }

            if (isset($weibo['weibo_hist_data']))
            {
                foreach(explode('|',$weibo['weibo_hist_data']) as $file)
                {
                    foreach(explode('|', $weibo['weibo_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'weibo - ' . $name]);
                    }                    
                }
            }

            if (isset($wechat['wechat_hist_data']))
            {
                foreach(explode('|',$wechat['wechat_hist_data']) as $file)
                {
                    foreach(explode('|', $wechat['wechat_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'wechat - ' . $name]);
                    }                    
                }
            }

            if (isset($blog['blog_hist_data']))
            {
                foreach(explode('|',$blog['blog_hist_data']) as $file)
                {
                    foreach(explode('|', $blog['blog_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'blog - ' . $name]);
                    }                    
                }
            }

            if (isset($social['social_hist_data']))
            {
                foreach(explode('|',$social['social_hist_data']) as $file)
                {
                    foreach(explode('|', $social['social_hist_data_fname']) as $name)
                    {
                        $message->attach(storage_path().'/temp'.DIRECTORY_SEPARATOR.$file, ['as' => 'social - ' . $name]);
                    }                    
                }
            }

            
        });

        foreach($managers as $row)
        {   
            $thisuser = User::where('email',$row['email'])->first();
            $thisuser->notify(new ProspectConvertedAccountNotification($account,$prospect,$prospect->lead,$prospect->sales->UserInfo,$prospect->sales->UserInfo->team));
        } 

        $this->forgetAccountSession();
      }

    

    public function search($prospect)
    {
        $access = Auth::user()->userAccess->toArray();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        
        if($prospect == null)
        {
            if ($user->role != 1)
                $prospects = Prospect::whereNull('delete_flag')->where('active_flag',1)->where('status','!=','LS005')->where('created_id', Auth::user()->userid)->orderBy('updated_at','desc')->paginate($perPage);
            else
                $prospects = Prospect::whereNull('delete_flag')->where('active_flag',1)->where('status','!=','LS005')->where('updated_at','desc')->paginate($perPage);

            $result = $this->tblProspects($prospects);
            return response()->json(['result' => $result,'pagination' => $prospects],200);
        }

        $byCompany = Prospect::FindCompany($prospect);
        $byName  = Prospect::FindName($prospect);
        $byOwner = Prospect::FindOwner($prospect);
        $byStatus  = Prospect::FindStatus($prospect);

        if (!empty($byCompany->toArray()['data']))
            $prospects = $byCompany;
        else if (!empty($byName->toArray()['data']))
            $prospects = $byName;
        else if(!empty($byStatus->toArray()['data']))
            $prospects = $byStatus;
        else if(!empty($byOwner->toArray()['data']))
            $prospects = $byOwner;
        else
            $prospects = "";
        
        if (!empty($prospects))
        {
            foreach($prospects as $key => $row)
            {
                if(!empty($row->lead->services))
                {
                    $services = explode('|',$row->lead->services);
                    $np = array();
                    foreach($services as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
    
                    $services = implode(', ',$np);
                }
                else
                {
                    $services = 'Not Specified';
                }

                $result[] = [
                    'id'         => $row->id,
                    'contract'   => count(Contract::where('prospect_id', $row->id)->get()),
                    'company'    => $row->lead->company,
                    'name'       => !empty($row->lead->f_name) || !empty($row->lead->l_name) ? $row->lead->f_name.' '.$row->lead->l_name : 'Not Specified',
                    'services'   => $services,
                    'status'     => $row->prospectStatus->systemdesc,
                    'created_by' => isset($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None',
                    'created_date'=> date('d M Y', strtotime($row->updated_at)),
                    'ex_client' => !empty($row->ex_client_flag) ? $row->ex_client_flag == 1 ? 'Yes' : 'No' : 'No',
                    'locked'    => $row->lock_this_prospect == 1 ? 'Yes' : 'No',
                    'opportunity_exist' => Opportunity::where('prospect_id', '=', $row->id)->exists()
                ];
            }
        }
        else
        {
            $result = '';
            $prospects = '';
        }
        
        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;

        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        return response()->json([
            'perpage'    => $items,
            'result'     => $result,
            'pagination' => $prospects,
            'access'     => $access,
            'role'       => $role,
            'teamcode'   => $teamcode,
            'managers'   => $managers,
            'user'       => Auth::user(),
        ],200);
    }

    public function refresh()
    {
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;

        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        if($user->role == 1 || in_array($user->UserInfo->emp_id, $managers))
            $prospects = Prospect::whereNull('delete_flag')->where('active_flag',1)->where('status','!=','LS005')->orderBy('updated_at','desc')->paginate($perPage);
        else
            $prospects = Prospect::whereNull('delete_flag')->where('active_flag',1)->where('status','!=','LS005')->orderBy('updated_at','desc')->where('created_by',$user->userid)->paginate($perPage);

        if (!empty($prospects))
        {
            foreach($prospects as $key => $row)
            {
                if(!empty($row->lead->services))
                {
                    $services = explode('|',$row->lead->services);
                    $np = array();
                    foreach($services as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
    
                    $services = implode(', ',$np);
                }
                else
                {
                    $services = 'Not Specified';
                }
               

                $result[] = [
                    'id'         => $row->id,
                    'contract'   => count(Contract::where('prospect_id', $row->id)->get()),
                    'company'    => $row->lead->company,
                    'name'       => !empty($row->lead->f_name) || !empty($row->lead->l_name) ? $row->lead->f_name.' '.$row->lead->l_name : 'Not Specified',
                    'services'   => $services,
                    'status'     => $row->prospectStatus->systemdesc,
                    'created_by' => isset($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None',
                    'created_date'=> date('d M Y', strtotime($row->updated_at)),
                    'ex_client' => !empty($row->ex_client_flag) ? $row->ex_client_flag == 1 ? 'Yes' : 'No' : 'No',
                    'locked'    => $row->lock_this_prospect == 1 ? 'Yes' : 'No',
                    'opportunity_exist' => Opportunity::where('prospect_id', '=', $row->id)->exists()

                ];

            }
            
        }
        else
        {
            $result = '';
            $prospects = '';
        }

        return response()->json([
            'perpage'    => $items,
            'result'     => isset($result) ? $result : '',
            'pagination' => $prospects,
            'access'     => $access,
            'role'       => $role,
            'teamcode'   => $teamcode,
            'managers'   => $managers,
            'user'       => Auth::user(),
        ],200);
    }

    public function sort($field, $order)
    {
        $prospects = Prospect::Sort($field, $order);

        if(!empty($prospects))
        {
            foreach($prospects as $key => $row)
            {
                if(!empty($row->lead->services))
                {
                    $services = explode('|',$row->lead->services);
                    $np = array();
                    foreach($services as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
                    
                    $services = implode(', ',$np);
                }
                else
                {
                    $services = 'Not Specified';
                }
               

                $result[] = [
                    'id'         => $row->id,
                    'contract'   => count(Contract::where('prospect_id', $row->id)->get()),
                    'company'    => $row->lead->company,
                    'name'       => !empty($row->lead->f_name) || !empty($row->lead->l_name) ? $row->lead->f_name.' '.$row->lead->l_name : 'Not Specified',
                    'services'   => $services,
                    'status'     => $row->prospectStatus->systemdesc,
                    'created_by' => isset($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None',
                    'created_date'=> date('d M Y', strtotime($row->updated_at)),
                    'ex_client'  => !empty($row->ex_client_flag) ? $row->ex_client_flag == 1 ? 'Yes' : 'No' : 'No',
                    'locked'     => $row->lock_this_prospect == 1 ? 'Yes' : 'No',
                    'opportunity_exist' => Opportunity::where('prospect_id', '=', $row->id)->exists()
                ];
            }
        }
        else
        {
            $result = '';
            $prospects = '';
        }

        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;

        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
        
        return response()->json([
          'result'     => isset($result) ? $result : '',
          'pagination' => $prospects,
          'field'      => $field,
          'order'      => $order,
          'access'     => $access,
          'role'       => $role,
          'teamcode'   => $teamcode,
          'managers'   => $managers,
          'user'       => Auth::user(),
        ], 200);
    }

    function tblProspects()
    {
        
        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $userlogged = Auth::user();

        $items = PaginationPerPage::where('table_name','prospects')->where('userid',$userlogged->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $user = $userlogged;

        $manager_list = Manager::select('emp_id')->get();
        
        $managers = array_flatten($manager_list->toArray());


        if($userlogged->role == 1 || ($teamcode == 'acc' && in_array($userlogged->userInfo->emp_id,$managers)) )
            $prospects = Prospect::whereNull('deleted_at')->where('active_flag',1)->where('status','!=','LS005')->orderBy('updated_at','desc')->paginate($perPage);
        else
            $prospects = Prospect::whereNull('deleted_at')->where('active_flag',1)->where('status','!=','LS005')->orderBy('updated_at','desc')->where('created_by',Auth::user()->userid)->paginate($perPage);

        if(count($prospects) > 0)
        {   
            foreach($prospects as $key => $row)
            {
                if(!empty($row->lead->services))
                {
                    $services = explode('|',$row->lead->services);
                    $np = array();
                    foreach($services as $k => $v)
                    {
                        $service = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($service->systemdesc,0,100,'');
                    }

                    $services = implode(', ', $np);
                }

                $result[] = [
                    'id'                => $row->id,
                    'contract'          => count(Contract::where('prospect_id', $row->id)->get()),
                    'company'           => $row->lead->company,
                    'name'              => !empty($row->lead->f_name) || !empty($row->lead->l_name) ? $row->lead->f_name.' '.$row->lead->l_name : 'Not Specified',
                    'services'          => !empty($services) ? $services : 'None',
                    'status'            => $row->prospectStatus->systemdesc,
                    'created_by'        => isset($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None',
                    'created_date'      => date('d M Y', strtotime($row->updated_at)),
                    'ex_client'         => !empty($row->ex_client_flag) ? $row->ex_client_flag == 1 ? 'Yes' : 'No' : 'No',
                    'locked'            => $row->lock_this_prospect == 1 ? 'Yes' : 'No',
                    'opportunity_exist' => Opportunity::where('prospect_id', '=', $row->id)->exists()
                ];
            }
        }
        else
        {
            $result = '';
            $prospects = '';
        }
        
        
        $data = [
            'perpage'  => $items,
            'user'     => $user,
            'managers' => $managers,
            'teamcode' => $teamcode,
            'prospects' => $prospects,
            'result'   => $result
        ];

        return $data;
    }

    function receivers()
    {
        $manager_list = Manager::all();
        foreach($manager_list as $row)
        {
            $emp = Employee::where('emp_id',$row['emp_id'])->first();

            if($row['dept_id'] == 2)
                continue;

            $managers[] = [
                'id'    => $emp['userInfo']['id'],
                'email' => $emp['userInfo']['email'],
                'team'  => $emp['team']['team_code'],
                'name'  => $emp['f_name'] . ' ' . $emp['l_name']
            ];
        }

        $administrators = User::where('role', 1)
                                ->get();

        foreach($administrators as $admin)
        {
            $managers[$admin->userid] = [
                'id'    => $admin->id,
                'email' => $admin->email,
                'team'  => $admin->UserInfo->team->team_code,
                'name'  => $admin->UserInfo->f_name.' '.$admin->UserInfo->l_name
            ];
        }


        foreach($managers as $k => $v)
        {
            if ($v['id'] == Auth::user()->id)
                continue;

            $notifiable[] = $v;
        }    

        return $notifiable;

    }

    function forgetAccountSession()
    {
      Session::forget('account_additional');
      Session::forget('account_sem');
      Session::forget('account_fb');
      Session::forget('account_seo');
      Session::forget('account_web');
      Session::forget('account_baidu');
      Session::forget('account_weibo');
      Session::forget('account_wechat');
      Session::forget('account_blog');
      Session::forget('account_social');
      Session::forget('account_postpaid');
    }

    

}
