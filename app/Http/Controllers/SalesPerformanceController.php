<?php

namespace App\Http\Controllers;

use Auth;
use Excel;
use App\PaginationPerPage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\SalesPerformanceExport;

use App\Contract;
use App\SalesPerformance;
use App\System;
use App\Employee;
use App\User;
use App\SalesRemark;
use App\Appointment;
use App\Lead;
use App\Prospect;
use App\FilterSelection;
use App\Account;
use App\LeadNote;


class SalesPerformanceController extends Controller
{
    public function setPaginationPerPage($tblName, $item)
    {
        $user = Auth::user();
        $pagination = PaginationPerPage::where('table_name', $tblName)->where('userid',$user->userid)->first();

        if(!empty($pagination))
        {
            $pagination->per_page = $item;
            $pagination->update();
        }
        else
        {
            DB::table('pagination_per_page')->insert(['table_name' => $tblName, 'per_page' => $item, 'userid' => $user->userid ]);
        }


    }

    public function setFilterSelection($tblName, $item)
    {
        $user = Auth::user();
        $filter = FilterSelection::where('table_name', $tblName)->where('user_id', $user->userid)->first();

        $filter = FilterSelection::updateOrCreate(
            ['table_name' => $tblName, 'user_id' => $user->userid],['person_filter' => $item]
        );
    }


    public function list()
    {
        $loggedUser = Auth::user();

        $salesPerson = array();
        $salesPerson2 = array();
        $salesPersonName = Employee::salesPersonName();
        $items = $this->getPaginationPerPage();
        $filter = $this->getFilterSelection('performance');
        $statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();


        if ($loggedUser->role == 1)
        {
            if($filter == 'all')
                $performances = SalesPerformance::orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
            else
                $performances = SalesPerformance::where('created_by', $filter)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());

        }
        else
        {
            $performances = SalesPerformance::where('created_by', $loggedUser->userid)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
        }


        $salesPerformance = $this->tableSalesPerformance($performances);

        app(__('global.Audit'))->logAuditTrail("MD013","AT010","Accessed Module",null,null,request()->ip(),$loggedUser->userid,$loggedUser->role);

        if(request()->ajax())
        {
            return response()->json(['performance' => $salesPerformance, 'pagination' => $performances, 'role' => $loggedUser->role, 'statuses' => $statuses], 200);
        }

        return view('performance.list', compact('salesPerformance', 'performances', 'loggedUser', 'options', 'items', 'loggedUser','statuses', 'salesPersonName', 'filter'));
    }

    public function addNote(Request $request)
    {
        $input = $request->except('_token');
        $user = Auth::user();

        $newNote = LeadNote::create([
            'lead_id'    => $input['sales_id'],
            'note'     => $input['remarks'],
            'userid'  => $user->userid,
        ]);

        $performance = SalesPerformance::where('id', '=', $input['sales_id'])->first();
        $created_by = $user->userInfo->full_name;
        $created_at = Carbon::parse()->format('d M Y h:i:s A');

        app(__('global.Audit'))->logAuditTrail("MD013","AT002","Create new remark on " . $performance->company ,null ,null, request()->ip(),$user->userid,$user->role);

        return response()->json(['note' => $newNote, 'performance' => $performance, 'created_by' => $created_by, 'created_at' => $created_at], 200);
    }

    public function fetchNotes($id)
    {
        $user = Auth::user();
        $performance = SalesPerformance::where('id', '=', $id)->first();
        $initial_data_notes = LeadNote::where('lead_id', '=', $id)->orderBy('created_at', 'desc')->get();
        $notes = [];

        if(!empty($initial_data_notes))
        {
            foreach($initial_data_notes as $row)
            {
                $notes[] = [
                    'id'         => $row->id,
                    'lead_id'    => $row->lead_id,
                    'created_by' => !empty($row->userid) ? $row->user->userInfo->full_name : null,
                    'note'       => !empty($row->note) ? nl2br($row->note) : null,
                    'created_at' => Carbon::parse($row->created_at)->format('d M Y h:i:s A'),
                    'updated_at' => Carbon::parse($row->updated_at)->format('d M Y h:i:s A')
                ];
            }
        }

        return response()->json(['performance' => $performance, 'notes' => $notes], 200);
    }

    public function updateStatus(Request $request, $id)
    {
        $user = Auth::user();
        $input = $request->except('_token');
        $initial = SalesPerformance::findorFail($id);

        if($initial->category == 'Prospect')
        {
            $update = \DB::select('UPDATE 
                                        prospects p 
                                    JOIN leads l 
                                        ON p.`lead_id` = l.`id` 
                                    SET 
                                        p.`status` = "'.$input['status'].'",
                                        p.`status_updated_by` = "'.$user->userid.'",
                                        p.`status_updated_at` = "'.Carbon::now().'",
                                        p.`updated_at` = "'.Carbon::now().'"
                                    WHERE 
                                        l.`id` = "'.$id.'"');
        }
        else
        {
            $update =  \DB::select('UPDATE 
                                        leads 
                                    SET 
                                        `status` = "' .$input['status'].'",
                                        `status_updated_by` = "'.$user->userid.'",
                                        `status_updated_at` = "'.Carbon::now().'",
                                        `updated_at` =  "'.Carbon::now().'"
                                    WHERE 
                                        `id` = "'.$id.'"');
        }

        $performance = SalesPerformance::with('getStatus', 'getStatusUpdatedBy')->findOrFail($id);

        $oldval['status'] = $initial->getStatus->systemdesc;
        $newval['status'] = $performance->getStatus->systemdesc;

        $oldvalue = isset($oldval) ? json_encode($oldval) : null;
        $newvalue = isset($newval) ? json_encode($newval) : null;

        app(__('global.Audit'))->logAuditTrail("MD013","AT005","Update Status of " . $performance->company ,$oldvalue, $newvalue, request()->ip(),$user->userid,$user->role);

        return response()->json(['performance' => $performance, 'status' => $performance->getStatus->systemdesc, 'role' => $user->role], 200);
    }

    public function refresh()
    {
        $loggedUser = Auth::user();
        $filter = $this->getFilterSelection('performance');
        $statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();

        if ($loggedUser->role == 1)
        {
            if ($filter == 'all')
                $performance = SalesPerformance::orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
            else
                $performance = SalesPerformance::where('created_by', $filter)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
        } else {
            $performance = SalesPerformance::where('created_by', $loggedUser->userid)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
        }

        $salesPerformance = $this->tableSalesPerformance($performance);

        return response()->json(['performance' => $salesPerformance, 'pagination' => $performance, 'role' => $loggedUser->role, 'statuses' => $statuses]);
    }

    public function search($data)
    {
        $user = Auth::user();
        $filter = $this->getFilterSelection('performance');
        $statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();

        if($data == null)
        {
            if ($user->role == 1)
            {
                if ($filter == 'all')
                    $performances = SalesPerformance::orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
                else
                    $performances = SalesPerformance::where('created_by', $filter)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
            }
            else
            {
                $performances = SalesPerformance::where('created_by', $user->user_id)->orderBy('created_at', 'desc')->paginate($this->getPaginationPerPage());
            }

            $salesPerformance = $this->tableSalesPerformance($performances);
            return response()->json(['performance' => $salesPerformance,'pagination' => $performances, 'role' => $user->role, 'statuses' => $statuses], 200);
        }

        $byCompany = SalesPerformance::FindCompany($data);
        $byName = SalesPerformance::FindName($data);
        $byContact = SalesPerformance::FindContact($data);
        $byContractValue = SalesPerformance::FindContractValue($data);

        if (!empty($byCompany->toArray()['data']))
            $performances = $byCompany;
        else if (!empty($byName->toArray()['data']))
            $performances = $byName;
        else if (!empty($byContact->toArray()['data']))
            $performances = $byContact;
        else if (!empty($byContractValue->toArray()['data']))
            $performances = $byContractValue;
        else
            $performances = "";

        $salesPerformance = $this->tableSalesPerformance($performances);

        return response()->json(['performance' => $salesPerformance, 'pagination' => $performances, 'key' => $data, 'role' => $user->role, 'statuses' => $statuses], 200);
    }

    public function sort($field, $order)
    {
        $user = Auth::user();
        $statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();
        $performances = SalesPerformance::sort($field, $order);
        $salesPerformance = $this->tableSalesPerformance($performances);
        return response()->json(['performance'=> $salesPerformance, 'pagination' => $performances, 'field' => $field, 'order' => $order, 'role' => $user->role, 'statuses' => $statuses], 200);
    }

    function getContractValues($salesPerson, $opportunityStart, $opportunityEnd)
    {
        $value = array();

        $sales_achieved = Contract::whereIn('contract_type', ['CT001', 'CT003'])
            ->whereNotNull('contract_value')
            ->whereDate('created_at', '>=', $opportunityStart->toDateString())
            ->whereDate('created_at', '<=', $opportunityEnd->toDateString())
            ->where('created_by', '=', $salesPerson)
            ->whereNull('delete_flag')
            ->orWhere(function($query) use ($salesPerson, $opportunityStart, $opportunityEnd) {
                $query->whereIn('contract_type', ['CT002', 'CT004']);
                $query->whereNotNull('contract_value');
                $query->whereDate('created_at', '>=', $opportunityStart->toDateString());
                $query->whereDate('created_at', '<=', $opportunityEnd->toDateString());
                $query->where('created_by', '=', $salesPerson);
                $query->whereNull('delete_flag');
                $query->where('day_90', '>=', Carbon::now()->toDateTimeString());
            })
            ->get();

        foreach($sales_achieved as $row)
        {
            $value[] = floatval(preg_replace('/[^\d\.\-]/', '', $row->contract_value));
        }

        return $value;
    }

    public function getSalesReport($salesPerson, $opportunityStart, $opportunityEnd, $appointmentStart, $appointmentEnd, $appointmentCheck)
    {
        $user = Auth::user(); // variable for current user logged in
        $role = $user->role; // variable for role of current user logged in
        $statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get(); // get all the lead status where mark as active
        $employee = User::where('userid', $salesPerson)->first(); // get the sales person employee information
        $salesTarget = !empty($employee->userInfo->sales_target) ? floatval(preg_replace('/[^\d\.\-]/', '', $employee->userInfo->sales_target)) : floatval(preg_replace('/[^\d\.\-]/', '', 0));

        // get the start and end date of opportunity to get the total Target Achieved
        $opportunityStartDateString = \Carbon\Carbon::parse($opportunityStart);
        $opportunityEndDateString = \Carbon\Carbon::parse($opportunityEnd);

        $checkTargetAchieved = $this->getContractValues($salesPerson, $opportunityStartDateString, $opportunityEndDateString);
        $initialTargetAchieved = floatval(preg_replace('/[^\d\.\-]/', '', number_format(array_sum($checkTargetAchieved), 2))); // TARGET ACHIEVED!
        $holidays = ['2019-04-19', '2019-05-01', '2019-05-19', '2019-06-05', '2019-08-09', '2019-08-11', '2019-10-27', '2019-12-25'];
        $startDate = \Carbon\Carbon::parse($opportunityStart);
        $endDate = \Carbon\Carbon::parse($opportunityEnd);

        // Check if target achieved meet the sales target
        if($salesTarget > $initialTargetAchieved)
        {
            if (in_array($endDate->addDay()->toDateString(), $holidays))
            {
                $opportunityEndDateString->addDays(2);
            }
            elseif ($endDate->addDay()->isWeekend())
            {
                $opportunityEndDateString->next(Carbon::MONDAY);
            }
            else
            {
                $opportunityEndDateString->addDay();
            }
        }

        $appointmentStartDateString =  \Carbon\Carbon::parse($appointmentStart);
        $appointmentEndDateString =  \Carbon\Carbon::parse($appointmentEnd);
        $lastWeekAppointmentStartDateString = \Carbon\Carbon::parse($appointmentStartDateString)->subWeek();
        $lastWeekAppointmentEndDateString = \Carbon\Carbon::parse($appointmentEndDateString)->subweek();

        /* Code block to get Sales Target Achieved of every salesperson */
        $value = array();

        if($salesPerson != 'all')
        {
            $value = $this->getContractValues($salesPerson, $opportunityStartDateString, $opportunityEndDateString);
        }

        $salesTarget = !empty($employee->userInfo->sales_target) ? number_format(floatval(preg_replace('/[^\d\.\-]/', '', $employee->userInfo->sales_target)), 2) : number_format(floatval(preg_replace('/[^\d\.\-]/', '', 0)), 2);
        $targetAchieved = number_format(array_sum($value), 2); // TARGET ACHIEVED!
        $targetShortFall = floatval(preg_replace('/[^\d\.\-]/', '', $salesTarget)) - floatval(preg_replace('/[^\d\.\-]/', '', $targetAchieved));
        /* End of code block Sales Target Achieved of every salesperson */

        $currentAppointmentsModel = Appointment::listCurrentAppointmentReport($salesPerson, $appointmentStartDateString, $appointmentEndDateString);
        $lastWeekAppointmentsModel = Appointment::listLastWeekAppointmentReport($salesPerson, $lastWeekAppointmentStartDateString, $lastWeekAppointmentEndDateString);
        $currentPostponedAppointments = Appointment::listCurrentPostponedAppointments($salesPerson, $appointmentStartDateString, $appointmentEndDateString);
        $lastWeekPostponedAppointments = Appointment::listLastWeekPostponedAppointments($salesPerson, $lastWeekAppointmentStartDateString, $lastWeekAppointmentEndDateString);

        $currentAppointments = (new AppointmentManagementController)->tableAppointments($currentAppointmentsModel);
        $lastWeekAppointments = (new AppointmentManagementController)->tableAppointments($lastWeekAppointmentsModel);

        $generator = $user->userInfo->f_name . ' ' . $user->userInfo->l_name;
        $exportSales = array();

        foreach($statuses as $status)
        {
            ${preg_replace("/\s+/", '_', strtolower($status->systemdesc)) . '_arr'} = array();

            ${preg_replace("/\s+/", '_', strtolower($status->systemdesc))} = $salesPerson == 'all' ? ($user->role == 1 ? SalesPerformance::where('status', '=', $status->systemcode)->orderBy('status')->get() : SalesPerformance::where('created_by', $salesPerson)->where('status', '=', $status->systemcode)->orderBy('status')->get())
                : SalesPerformance::where('created_by', $salesPerson)->where('status', '=', $status->systemcode)->orderBy('status')->get();

            ${'export_' . preg_replace("/\s+/", '_', strtolower($status->systemdesc))} = $this->tableSalesPerformance(${preg_replace("/\s+/", '_', strtolower($status->systemdesc))});


            foreach(${preg_replace("/\s+/", '_', strtolower($status->systemdesc))} as $row)
            {
                ${preg_replace("/\s+/", '_', strtolower($status->systemdesc)) . '_arr'}[]  =  floatval(preg_replace('/[^\d\.\-]/', '',  $row['contract_value']));
            }

            ${'totalContractValue' . preg_replace("/\s+/", '', $status->systemdesc)} = ${preg_replace("/\s+/", '_', strtolower($status->systemdesc)) . '_arr'};
            ${'prettyFormatTotalContractValue' . preg_replace("/\s+/", '', $status->systemdesc)} = number_format(array_sum(${'totalContractValue' . preg_replace("/\s+/", '', $status->systemdesc)}), 2);
        }

        // dd($appointmentCheck, $currentAppointments, $lastWeekAppointments, $currentPostponedAppointments, $lastWeekPostponedAppointments);

        $name = $salesPerson == 'all' ? ($user->role == 1 ? 'All' : 'All ' . strtoupper(User::where('userid', $user->userid)->first()->userInfo->f_name) . ' ' . strtoupper(User::where('userid', $user->userid)->first()->userInfo->l_name . ' account'))
            : strtoupper(User::where('userid', $salesPerson)->first()->userInfo->f_name) . ' ' . strtoupper(User::where('userid', $salesPerson)->first()->userInfo->l_name);

        $excelName = 'sales_report_' . preg_replace("/\s+/", '_', $name) . '_(' . Carbon::parse()->format('Y-m-d H:i:s') . ')';

        app(__('global.Audit'))->logAuditTrail("MD013","AT014","Download " . $name . ' sales', null, null, request()->ip(),$user->userid,$user->role);

        return Excel::create($excelName, function($excel) use ($appointmentCheck, $currentAppointments, $lastWeekAppointments, $currentPostponedAppointments, $lastWeekPostponedAppointments,
            $appointmentStartDateString, $appointmentEndDateString, $lastWeekAppointmentStartDateString, $lastWeekAppointmentEndDateString, $export_super_hot, $export_hot, $export_warm, $export_cold,
            $export_lost, $generator, $name, $role, $statuses, $salesTarget, $targetAchieved, $opportunityStartDateString, $opportunityEndDateString,
            $targetShortFall, $prettyFormatTotalContractValueSuperHot,  $prettyFormatTotalContractValueHot, $prettyFormatTotalContractValueWarm, $prettyFormatTotalContractValueCold, $prettyFormatTotalContractValueLost) {

            $excel->sheet('Appointments', function($sheet) use ($appointmentCheck, $currentAppointments, $lastWeekAppointments, $currentPostponedAppointments, $lastWeekPostponedAppointments,
                $generator, $name, $role, $statuses, $appointmentStartDateString, $appointmentEndDateString, $lastWeekAppointmentStartDateString, $lastWeekAppointmentEndDateString)
            {
                $sheet->loadView('layouts.excel.sales_report.export_appointment',[
                    'appointment_check' => $appointmentCheck,
                    'current_appointments' => $currentAppointments,
                    'last_week_appointments' => $lastWeekAppointments,
                    'current_postponed_appointments' => $currentPostponedAppointments,
                    'last_week_postponed_appointments' => $lastWeekPostponedAppointments,
                    'generator' => $generator,
                    'salesPerson' => $name,
                    'role' => $role,
                    'statuses' => $statuses,
                    'current_from' => $appointmentStartDateString->format('d F Y'),
                    'current_to' => $appointmentEndDateString->format('d F Y'),
                    'last_week_from' => $lastWeekAppointmentStartDateString->format('d F Y'),
                    'last_week_to' => $lastWeekAppointmentEndDateString->format('d F Y'),
                ]);

                $sheet->setOrientation('landscape');
                $sheet->setStyle([
                    'font' => [
                        'name' => 'Arial',
                        'size' => 11
                    ]
                ]);
            });

            $excel->sheet('Opportunities', function($sheet) use ($export_super_hot, $export_hot, $export_warm, $export_cold,
                $export_lost, $generator, $name, $role, $statuses, $salesTarget, $targetAchieved, $opportunityStartDateString, $opportunityEndDateString, $targetShortFall,
                $prettyFormatTotalContractValueSuperHot,  $prettyFormatTotalContractValueHot, $prettyFormatTotalContractValueWarm, $prettyFormatTotalContractValueCold, $prettyFormatTotalContractValueLost)
            {
                $sheet->loadView('layouts.excel.sales_report.export_opportunities',[
                    'super_hot' => $export_super_hot,
                    'hot' => $export_hot,
                    'warm' => $export_warm,
                    'cold' => $export_cold,
                    'lost' => $export_lost,
                    'generator' => $generator,
                    'sales' => $name,
                    'role' => $role,
                    'statuses' => $statuses,
                    'sales_target' => $salesTarget,
                    'target_achieved' => $targetAchieved,
                    'from' => $opportunityStartDateString->format('d M Y'),
                    'to' => $opportunityEndDateString->format('d M Y'),
                    'target_shortfall' => number_format($targetShortFall, 2),
                    'total_super_hot' => $prettyFormatTotalContractValueSuperHot,
                    'total_hot' => $prettyFormatTotalContractValueHot,
                    'total_warm' =>$prettyFormatTotalContractValueWarm,
                    'total_cold' => $prettyFormatTotalContractValueCold,
                    'total_lost' => $prettyFormatTotalContractValueLost,
                ]);

                $sheet->setOrientation('landscape');
                $sheet->setStyle([
                    'font' => [
                        'name' => 'Arial',
                        'size' => 11
                    ]
                ]);
            });

        })->download('xlsx');
    }

    public function getLeadInformation($id)
    {
        $performance = SalesPerformance::findOrFail($id);
        $services = array();


        if($performance->category == 'Lead')
        {
            $result = Lead::where('id', $id)->firstOrFail();
            $source = !empty($result->source) ? System::where('systemcode', $result->source)->first()->systemdesc : 'Not Specified';
            $landing = !empty($result->landing_page) ? System::where('systemcode', $result->landing_page)->first()->systemdesc : 'Not Specified';


            if(!empty($result->services))
            {
                foreach(explode('|', $result->services) as $row)
                {
                    $array_services = System::where('systemcode', $row)->first();
                    $service[] = mb_strimwidth($array_services->systemdesc, 0, 30, '');

                }
                $services = implode(', ', $service);
            }

        }
        else
        {
            $result = Prospect::where('lead_id', $id)->firstOrFail();
            $source = !empty($result->lead->source) ? System::where('systemcode', $result->lead->source)->first()->systemdesc : 'Not Specified';
            $landing = !empty($result->lead->landing_page) ? System::where('systemcode', $result->lead->landing_page)->first()->systemdesc : 'Not Specified';

            if(!empty($result->lead->services))
            {
                foreach(explode('|', $result->lead->services) as $row)
                {
                    $array_services = System::where('systemcode', $row)->first();
                    $service[] = mb_strimwidth($array_services->systemdesc, 0, 30, '');

                }
                $services = implode(', ', $service);
            }
        }

        $results = [
            'id'            => $result->id,
            'status'        => $performance->category == 'Lead' ? $result->leadStatus->systemdesc : $result->prospectStatus->systemdesc,
            'company'       => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->company) : $this->check_if_empty_lead($result->lead->company),
            'contract_value'=> $performance->category == 'Lead' ? (!empty($result->contract_value) ? '$ ' . $result->contract_value : 'TBA') : (!empty($result->lead->contract_value) ? '$ ' . $result->lead->contract_value : 'TBA'),
            'client_name'   => $performance->category == 'Lead' ? (!empty($result->f_name) ? $result->f_name.' '.$result->l_name : 'Not Specified') : (!empty($result->lead->f_name) ? $result->lead->f_name.' '.$result->lead->l_name : 'Not Specified'),
            'office_number' => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->office_number) : $this->check_if_empty_lead($result->lead->office_number),
            'contact_number'=> $performance->category == 'Lead' ? $this->check_if_empty_lead($result->contact_number) : $this->check_if_empty_lead($result->lead->contact_number),
            'email'         => $performance->category == 'Lead' ? $this->getEmailAddress($result->email) : $this->getEmailAddress($result->lead->email),
            'designation'   => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->designation) : $this->check_if_empty_lead($result->lead->designation),
            'street_address'=> $performance->category == 'Lead' ? $this->check_if_empty_lead($result->street_address) : $this->check_if_empty_lead($result->lead->street_address),
            'zip_code'      => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->zip_code) : $this->check_if_empty_lead($result->lead->zip_code),
            'city'          => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->city) : $this->check_if_empty_lead($result->lead->city),
            'website'       => $performance->category == 'Lead' ? $this->check_if_empty_lead(wordwrap($result->website, 20, "<br>", true)) : $this->check_if_empty_lead(wordwrap($result->lead->website, 20, "<br>", true)),
            'services'      => $this->check_if_empty_lead($services),
            'other_services'=> $performance->category == 'Lead' ? $this->check_if_empty_lead($result->other_services) : $this->check_if_empty_lead($result->lead->other_services),
            'source'        => $this->check_if_empty_lead($source),
            'source_other'  => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->source_other) : $this->check_if_empty_lead($result->lead->source_other),
            'landing_page'  => $this->check_if_empty_lead($landing),
            'landing_other' => $performance->category == 'Lead' ? $this->check_if_empty_lead($result->landing_other) : $this->check_if_empty_lead($result->lead->landing_other),
            'created_by'    => $result->sales->userInfo->f_name.' '.$result->sales->userInfo->l_name,
            'created_at'    => Carbon::parse($result->created_at)->format('d M Y g:i:s A')
        ];

        return response()->json(['result' => $results, 'category' => $performance->category], 200);
    }

    /* HELPER FUNCTIONS */
    function getPaginationPerPage()
    {
        $user = Auth::user();
        $items = PaginationPerPage::where('table_name','performance')->where('userid', '=', $user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : 10;
        return $perPage;
    }

    function getFilterSelection($tblName)
    {
        $user = Auth::user();
        $filter = FilterSelection::where('table_name', $tblName)->where('user_id', '=', $user->userid)->first();
        return !empty($filter) ? $filter->person_filter : 'all';
    }

    function check_if_empty($data)
    {
        return !empty($data) ? $data : 'TBA';
    }

    function check_if_empty_lead($data)
    {
        return !empty($data) ? $data : 'Not Specified';
    }

    function getEmailAddress($data)
    {
        if (!empty($data))
            return implode(', ', explode('|', $data));

        return 'Not Specified';
    }

    function fetchLatestRemark($leadID)
    {
        $note = LeadNote::where('lead_id', '=', $leadID)->orderBy('created_at','desc')->first();

        if(!empty($note))
        {
            return [
                'remark_date' => $note->created_at->format('d M Y'),
                'remark' => $note->note
            ];
        }

        return false;
    }

    function tableSalesPerformance($performances)
    {
        $salesPerformance = array();

        if(!empty($performances))
        {
            foreach($performances as $row)
            {
                if(!empty($row->services))
                {
                    $array_services = explode('|', $row->services);
                    $sv = array();
                    foreach($array_services as $row2)
                    {
                        $service_desc = System::where('systemcode', $row2)->first();
                        $sv[] = $service_desc->systemdesc;
                    }
                    $services = implode(', ', $sv);
                }
                else
                {
                    $services = '';
                }

                $status = $row->status ? System::where('systemcode', $row->status)->first()->systemdesc : '';
                $salesPerformance[] = [
                    'id' => $row->id,
                    'company' => $this->check_if_empty($row->company),
                    'client_name' => $this->check_if_empty($row->client_name),
                    'contract_value' => !empty($row->contract_value) ? '$ ' . $row->contract_value : 'TBA',
                    'contact_number' => $this->check_if_empty($row->contact_number),
                    'services' => $this->check_if_empty($services),
                    'landing_page' => $this->check_if_empty($row->landing_page),
                    'status' => $this->check_if_empty($status),
                    'created_at' => $this->check_if_empty($row->created_at),
                    'created_by' => !empty($row->created_by) ? $row->getUser->userInfo->f_name . ' ' . $row->getUser->userInfo->l_name : '',
                    'remarks' => $this->check_if_empty($this->fetchLatestRemark($row->id)['remark']),
                    'remarks_date' => $this->check_if_empty($this->fetchLatestRemark($row->id)['remark_date']),
                    'status_updated_by' => !empty($row->status_updated_by) ? $row->getStatusUpdatedBy->userInfo->f_name.' '.$row->getStatusUpdatedBy->userInfo->l_name : null,
                    'status_updated_at' => !empty($row->status_updated_at) ? Carbon::parse($row->status_updated_at)->format('d M Y g:i:A') : null,
                    'updated_at' => $row->updated_at,
                    'category' => $row->category

                ];
            }
        }


        return $salesPerformance;
    }
}
