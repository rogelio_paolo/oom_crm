<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Account;
use App\User;
use App\Manager;
use App\Reminder;
use Auth;
use App\Notifications\AccountReminderNotification;
use App\Notification;
use App\System;

class AccountReminderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create(Request $request)
    {  

        $input = $request->except('_token');
        
        $account = Account::find($input['reminderaccountid']);
       
        // Call function getClient() from this class.
        $client = $this->getClient();

        // Create Calendar Object with parameter client function.
        $service = new \Google_Service_Calendar($client);
        // Create Calendar Event Object
        $event = new \Google_Service_Calendar_Event();

        // Set reminder name and remarks
        $event->setSummary($input['name']);
        $event->setDescription(nl2br($input['remark']));

        // Create Calendar Event Date/Time Object
        $datetime = new \Google_Service_Calendar_EventDateTime();
        $datetime->setDateTime(date('c',strtotime($input['date'])));
        $event->setStart($datetime);
        $event->setEnd($datetime);

        // List event attendees
        $i = 0;
        foreach($input['receivers'] as $row)
        {
            $i++;
            ${'attendee' . $i} = new \Google_Service_Calendar_EventAttendee();
            ${'attendee' . $i}->setEmail($row);
            ${'attendee' . $i}->setResponseStatus('accepted');
            $attendees[] = ${'attendee' . $i};
        }
        $event->setAttendees($attendees);
        
        // Create event organizer object
        $organizer = new \Google_Service_Calendar_EventOrganizer();
        $organizer->setEmail(Auth::user()->email);
        $event->setOrganizer($organizer);

        // All the results will enter in insert method with email notification
        $optParams = array('sendNotifications' => true);
        $results = $service->events->insert('primary', $event, $optParams);
        
        $reminder = Reminder::create([
            'reminderid'        => $results->getId(),
            'name'              => $input['name'],
            'date'              => date('Y-m-d H:i:s',strtotime($input['date'])),
            'receivers'         => implode('|',$input['receivers']),
            'remark'            => preg_replace("/\r\n|\r|\n/", '<br/>', $input['remark']),
            'createdid'         => Auth::user()->userid,
            'accountid'         => $input['reminderaccountid']
        ]);   
        
        $email_reminder = Reminder::find($input['reminderaccountid']);

        $receivers = $this->receivers($input['receivers']);

        foreach($receivers as $row)
        {
            $emailDetails = [
                                'name' => $input['name'],
                                'date' => $input['date'], 
                                'receiver' => $row->email,
                                'remark' => nl2br($input['remark']),
                                'recipient' => $row->UserInfo->f_name . ' ' . $row->UserInfo->l_name,
                                'company' => $account->prospect->lead->company 
                            ];
            
            $this->email($emailDetails);

            $row->notify(new AccountReminderNotification($account, $account->prospect, $account->prospect->lead, $reminder));
        }
        
        return
            redirect()
            ->back()
            ->with([
                'alert'   => 'success',
                'message' => 'Successfully created a reminder',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
    }

    function email($emailDetails)
    {
        \Mail::send('layouts.emails.emailreminder', $emailDetails ,function($message) use ($emailDetails) {
            $message->to($emailDetails['receiver'])->subject
               ('CRM System - Reminder Notification - '. $emailDetails['name']);
            $message->from('noreply@oom.com.sg','CRM Admin');
            });
    }


    public function getReceivers($accountid)
    {
        $account = Account::find($accountid);

        if(!empty($account->web_id))
        {
            if(!empty($account->web->dev_team))
                foreach(explode('|',$account->web->dev_team) as $row)
                {
                    $user = User::where('emp_id',$row)->first();
                    $receivers[] = [
                        'empid' => $row,
                        'email' => $user->email,
                        'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
                    ];
                }
        }

        if(!empty($account->ppc_id))
        {
            if(!empty($account->sem->sem_team))
                foreach(explode('|',$account->sem->sem_team) as $row)
                {
                    $user = User::where('emp_id',$row)->first();
                    $receivers[] = [
                        'empid' => $row,
                        'email' => $user->email,
                        'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
                    ];
                }
        }

        if(!empty($account->fb_id))
        {
            if(!empty($account->fb->sem_team))
                foreach(explode('|',$account->fb->sem_team) as $row)
                {
                    $user = User::where('emp_id',$row)->first();
                    $receivers[] = [
                        'empid' => $row,
                        'email' => $user->email,
                        'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
                    ];
                }
        }

        if(!empty($account->seo_id))
        {
            if(!empty($account->seo->seo_team))
                foreach(explode('|',$account->seo->seo_team) as $row)
                {
                    $user = User::where('emp_id',$row)->first();
                    $receivers[] = [
                        'empid' => $row,
                        'email' => $user->email,
                        'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
                    ];
                }
        }

        // if(!empty($account->ppc_id) || !empty($account->fb_id) || !empty($account->seo_id) )
        // {
        //     if(!empty($account->sp_team))
        //         foreach(explode('|',$account->sp_team) as $row)
        //         {
        //             $user = User::where('emp_id',$row)->first();
        //             $receivers[] = [
        //                 'empid' => $row,
        //                 'email' => $user->email,
        //                 'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
        //             ];
        //         }
        // }

        if(!empty($account->acc_assigned))
            foreach(explode('|',$account->acc_assigned) as $row)
            {

                $user = User::where('emp_id',$row)->first();
                $receivers[] = [
                    'empid' => $row,
                    'email' => $user->email,
                    'name'  => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
                ];
            }

        $manager_list = Manager::all();

        foreach($manager_list as $row)
        {

            if ($row->team->team_code == 'dev' && !in_array('NP004',explode('|',$account->nature)))
                continue;
            
            if ( $row->team->team_code == 'sp' && (!in_array('NP001',explode('|',$account->nature)) && !in_array('NP002',explode('|',$account->nature)) && !in_array('NP003',explode('|',$account->nature))) )
                continue;
            
            $manager = User::where('emp_id',$row['emp_id'])->first();
         
            if ($row['dept_id'] == 2)
                continue;

            if($manager['emp_id'] == '')
                continue; 
            
            $receivers[] = [
                'empid' => $manager['emp_id'],
                'email' => $manager['email'],
                'name'  => $manager['userInfo']['f_name'].' '.$manager['userInfo']['l_name']
            ];
        }

        $administrators = User::where('role',1)->get();

        foreach($administrators as $admin)
        {
            if($admin['email'] == '')
                continue;

            $receivers[] = [
                'empid' => $admin->emp_id,
                'email' => $admin->email,
                'name' => $admin->UserInfo->f_name.' '.$admin->UserInfo->l_name
            ];
        }

        $remind_time = System::where('systemtype','remindertime')->orderBy('systemcode','asc')->get();

        $remind_time_other = System::where('systemtype', 'remindertimeother')->orderBy('systemcode','desc')->get();
        
        return response()->json([$receivers, $remind_time, $remind_time_other], 200);

    }

    public function list()
    {   
            $reminders = Notification::where('notifiable_id',Auth::user()->id)->where('type','like','%AccountReminder%')->orderBy('created_at','desc')->get();
        
            return view('reminder.list')->with(compact('reminders'));
        
    }

    public function show($id)
    {
        $reminder = Reminder::find($id);

        foreach(explode('|',$reminder->receivers) as $row)
        {
            $user = User::where('email',$row)->first();
            $receivers[] = [
                'name' => $user->UserInfo->f_name.' '.$user->UserInfo->l_name
            ];
        }

        $result = [
            'title'             => $reminder->name,
            'date'              => date('F d, Y h:i A',strtotime($reminder->date)),
            'receivers'         => $receivers,
            'remark'            => $reminder->remark,
        ];

        return response()->json($result,200);
    }

    function receivers($email)
    {
        foreach($email as $row)
        {
            $user = User::where('email',$row)->first();
            $receivers[] = $user;
        }

        return $receivers;
    }

    function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes(\Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig(storage_path("app/google-calendar/crm-google-calendar-04db200f9c57.json"));
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = storage_path("app/google-calendar/token.json");
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}
