<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Excel;
use File;

use App\Lead;
use App\Prospect;
use App\Account;
use App\System;
use App\User;
use App\Employee;
use App\PaginationPerPage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class LobbyManagementController extends Controller
{
    public function list()
    {
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','lobby')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        app(__('global.Audit'))->logAuditTrail("MD010","AT010","Accessed module",null,null,request()->ip(),$user->userid,$user->role);

        $data = DB::table('open_lobby_list')
                    ->orderBy('created','desc')
                    ->paginate($perPage);
                
        foreach($data as $row)
        { 
            $lobby[] = [
                'id'        => $row->id,
                'company'   => !empty($row->company) ? utf8_encode($row->company) : 'Not Specified',
                'name'      => !empty($row->name) ? utf8_encode($row->name) : 'Not Specified',
                'contact'   => !empty($row->contact) ? $row->contact : 'Not Specified',
                'email'     => !empty($row->email) ? implode(', ', explode('|', utf8_encode($row->email))) : 'Not Specified',
                'status'    => $row->status,
                'created'   => date('d M Y', strtotime($row->created)),
                'contract'  => $row->contract,
                'prospect'  => $row->prospect
            ]; 
        }
        
        if(!empty($lobby))
            $result = $this->tblLobby($lobby);
        else
            $result = '';
        
        if(request()->ajax())
            return response()->json(['pagination' => $data, 'lobby' => !empty($result) ? $result : ''], 200);


        return view('lobby.list')->with(compact('lobby','data','user','items'));

        Session::forget('existingEmail');
        Session::forget('insert');
    }

    public function filter($column, $value = null)
    {
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','lobby')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $con = mysqli_connect('localhost','gqbgrzsght','KWPy3r8GZY','gqbgrzsght');

        $field = array();

        if($value == null)
        {
            $filter = mysqli_query($con,"select distinct " . $column .  " from all_lobby_list ORDER BY " .$column. " DESC");

            while ($row = mysqli_fetch_row($filter)) { 
                $field[] = $row;
            };
        }
        else
        {
            $filter = mysqli_query($con,"select * from all_lobby_list where " .$column . " like " . "'%".$value."%' order by company desc");
            while ($row= mysqli_fetch_assoc($filter)) { 
                $result[] = $row;
            };
            

            $items = collect($result);
            $page = LengthAwarePaginator::resolveCurrentPage();
            $paginator = new LengthAwarePaginator(
                $items->forPage($page, $perPage), $items->count(), $perPage, $page, ['path'=> route('lobby.filter',[$column, $value])]
            );

            foreach($paginator as $row)
            {
                $field[] = [
                    'id'        => $row['id'],
                    'company'   => !empty($row['company']) ? utf8_encode($row['company']) : 'Not Specified',
                    'name'      => !empty($row['name']) ? utf8_encode($row['name']) : 'Not Specified',
                    'contact'   => !empty($row['contact']) ? $row['contact'] : 'Not Specified',
                    'email'     => !empty($row['email']) ? implode(', ', explode('|', utf8_encode($row->email))) : 'Not Specified',
                    'status'    => $row['status'],
                    'created'   => $row['created'],
                    'contract'  => $row['contract'],
                    'prospect'  => $row['prospect']
                ]; 
            }
           
        }

            mysqli_close($con);

            return response()->json(['result' => $field, 'user' => $user, 'pagination' => isset($paginator) ? $paginator : '', 200]);

        
        
    }

    public function refresh()
    {
        $items = PaginationPerPage::where('table_name','lobby')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $lobby = DB::table('open_lobby_list')
                ->orderBy('created','desc')
                ->paginate($perPage);

        $result = $this->tblLobby($lobby);
        
        return response()->json(['result' => $result, 'pagination' => $lobby]);
    }

    public function sort($field, $order)
    {
        $items = PaginationPerPage::where('table_name','lobby')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $data = DB::table('open_lobby_list')
                ->orderBy($field, $order)->paginate($perPage);

        foreach($data as $row)
        {
            
            $lobby[] = [
                'id'        => $row->id,
                'company'   => !empty($row->company) ? utf8_encode($row->company) : 'Not Specified',
                'name'      => !empty($row->name) ? utf8_encode($row->name) : 'Not Specified',
                'contact'   => !empty($row->contact) ? $row->contact : 'Not Specified',
                'email'     => !empty($row->email) ? implode(', ', explode('|', utf8_encode($row->email))) : 'Not Specified',
                'status'    => $row->status,
                'created'   => date('d M Y', strtotime($row->created)),
                'contract'  => $row->contract,
                'prospect'  => $row->prospect
            ]; 
        }

        if(!empty($lobby))
            $result = $this->tblLobby($lobby);
        else
            $result = '';

        return response()->json(['result' => !empty($result) ? $result : '', 'pagination' => $data, 'field' => $field, 'order' => $order]);
    }

    public function search($lobby)
    {
        $items = PaginationPerPage::where('table_name','lobby')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        if($lobby == null)
        {
            $lobby = DB::table('open_lobby_list')
                    ->orderBy('created','desc')
                    ->paginate($perPage);


            $result = $this->tblLobby($data);
            return response()->json(['result' => $data, 'pagination' => $lobby], 200);
        }

       $companyName = $this->searchKeyword('company', $lobby, $perPage);
       $clientName = $this->searchKeyword('name', $lobby, $perPage);
       $clientNumber = $this->searchKeyword('contact', $lobby, $perPage);
       $clientEmail = $this->searchKeyword('email', $lobby, $perPage);
        
        if(!empty($companyName))
            $lobby = $companyName;
        else if(!empty($clientName))
            $lobby = $clientName;
        else if(!empty($clientNumber))
            $lobby = $clientNumber;
        else if(!empty($clientEmail))
            $lobby = $clientEmail;
        else
            $lobby = "";

        $result = empty($lobby) ? '' : $this->tblLobby($lobby);
        $pagination = empty($lobby) ? '' : $lobby;

        return response()->json(['result' => $result, 'pagination' => $pagination], 200);
    }

    function searchKeyword($field, $value, $page)
    {
        $tableView = DB::table('all_lobby_list')
                        ->where($field,'like','%'.$value.'%')
                        ->orderBy('created','desc')
                        ->paginate($page);

        $tableData = array();

        foreach($tableView as $row)
        {
            $tableData[] = [
                'id'        => $row->id,
                'company'   => !empty($row->company) ? utf8_encode($row->company) : 'Not Specified',
                'name'      => !empty($row->name) ? utf8_encode($row->name) : 'Not Specified',
                'contact'   => !empty($row->contact) ? $row->contact : 'Not Specified',
                'email'     => !empty($row->email) ? implode(', ', explode('|', utf8_encode($row->email))) : 'Not Specified',
                'status'    => $row->status,
                'created'   => date('d M Y', strtotime($row->created)),
                'contract'  => $row->contract,
                'prospect'  => $row->prospect
            ];

            
        }

        return $tableData;
    }

    public function updateLeadProspectAccountToOpen(Request $request)
    {
        $user = Auth::user();
        $input = $request->except('_token');

        $checkOpen = isset($input['check-open']) ? $input['check-open'] : '';

        if (!empty($checkOpen))
        {
            $implodeCheck = implode('|', $checkOpen);

            $explodeCheck = explode('|', $implodeCheck);

            foreach($explodeCheck as $k => $v)
            {
                $lobby = DB::table('open_lobby_list')
                        ->where('id', $v)
                        ->first();

                if($lobby->category === 'Lead')
                    $lead = Lead::where('id', $v)
                                ->update(['status' => 'LS002', 'created_by' => $user->userid, 'active_flag' => 1]);
            
                if($lobby->category === 'Prospect')
                    $prospect = Prospect::where('id', $v)
                                        ->update(['status' => 'LS002', 'created_by' => $user->userid, 'active_flag' => 1]);
                
                if($lobby->category === 'Account')
                {
                    // Get the account id then change status to Closed
                    $account = Account::where('id', $v)->first();
                    $account->status = 'AS005';
                    $account->update();
                    
                    // Get the prospect id then change to inactive
                    $prospect = Prospect::where('id', $account->prospect->id)
                                        ->update(['active_flag' => null]);

                    // Get the lead id then change status to Hot and owner goes to sales who claim
                    $lead = Lead::where('id',$account->prospect->lead->id)
                                ->update(['status' => 'LS002', 'created_by' => $user->userid, 'is_convert_again' => 1]);
                }

            }

            return redirect()->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Successfully claim the lead/prospect/account. Go to Leads or Prospects module to check.',
                    'button'  => 'OK',
                    'confirm' => 'true',
                ]);
            
        }
        else
        {
            return redirect()->back()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Select at least one lead/prospect',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        }
        
    }

    public function getUpload()
    {
        return view('lobby.upload');

        Session::forget('existingEmail');
    }

    public function storeUpload(Request $request)
    {
        // validate the xls file
        $this->validate($request, array(
            'import'  => 'required'
        ), ['required' => 'You cannot do that']);


        // Condition for input request file named "import"
        if($request->hasFile('import'))
        {
            // Get the original name of file
            $extension = File::extension($request->import->getClientOriginalName());
            
            // Condition for file if the file is in correct extension
            if($extension == "xlsx" || $extension == "xls" || $extension == "xlsm")
            {
                // Get the original path of file on the temp
                $path = $request->import->getRealPath();

                // Call the package Excel for reading
                $sheets = Excel::load($path, function($reader){ 
                })->get();
                
                foreach ($sheets as $sheet) {
                    foreach ($sheet as $rowObj) {
                
                        $is_row_empty = true;
                        foreach ($rowObj as $cell) {
                            if($cell !== '' &&  $cell !== NULL) {
                                $is_row_empty = false; //detect not empty row
                                break;
                            }
                        }
                
                        if($is_row_empty ) continue; // skip empty row
                        $row = $rowObj->toArray();

                        $data[] = $row;
                    }

                    if(!empty($data)) // check if not empty data
                    {
                        foreach($data as $key => $value) 
                        {
                            $emailExist = Lead::where('email', $value['emailaddress'])->first(); // check if lead have matched their contact email address to existing leads, then we'll skipped these all to avoid duplicate data
                            if($emailExist)
                            {
                                $existingEmail[] = $value;
                                continue;
                            }

                            if(empty($value))
                                continue;

                            $saveToDb[] = $value;
                        }

                        if(isset($saveToDb)) // if there is data to save after checking of duplicate inputs then run these code
                        {
                            foreach($saveToDb as $row)
                            {
                                // Services field
                                if(!empty($row['services']))
                                {
                                    $selected = [];
                                    $services_array = explode(',',$row['services']);

                                    foreach(array_unique($services_array) as $v)
                                    {
                                        $service = System::where('systemtype','service')->where('systemdesc',$v)->first();
                                        $selected[] = $service->systemcode;
                                    }
                                    $services = implode('|',$selected);
                                }
                                else
                                {
                                    $services = '';
                                }

                                // Source field
                                $source = !empty($row['source']) ? System::where('systemtype','leadsource')->where('systemdesc',$row['source'])->first()->systemcode : '';

                                 // Landing Page field
                                $landing = !empty($row['landingpage']) ? System::where('systemtype','landingpage')->where('systemdesc',$row['landingpage'])->first()->systemcode : '';

                                // Status field
                                $status = !empty($row['status']) ? System::where('systemtype','leadstatus')->where('systemdesc',$row['status'])->first()->systemcode : '';

                                // Custom array data before saving to DB
                                $insert[] = [
                                    "company"       => $row['company'],
                                    "f_name"        => $row['firstname'],
                                    "l_name"        => $row['lastname'],
                                    "designation"   => $row['designation'],
                                    "email"         => preg_replace('/\s+/', '', $row['emailaddress']),
                                    "contact_number"=> !empty($row['contactnumber']) ? (is_numeric($row['contactnumber']) ? intval($row['contactnumber']) :null) : null,
                                    "office_number" => !empty($row['officenumber']) ? (is_numeric($row['officenumber']) ? intval($row['officenumber']) :null) : null,
                                    "street_address"=> $row['streetaddress'],
                                    "zip_code"      => !empty($row['zipcode']) ? (is_numeric($row['zipcode']) ? intval($row['zipcode']) :null) : null,
                                    "city"          => $row['city'],
                                    "website"       => $row['website'],
                                    "services"      => !empty($services) ? $services : null,
                                    "other_services"=> !empty($row['otherservices']) ? $row['otherservices'] : null,
                                    "source"        => $source,
                                    "source_other"  => !empty($row['othersource']) ? $row['othersource'] : null,
                                    "landing_page"  => $landing,
                                    "landing_other" => !empty($row['otherlanding']) ? $row['otherlanding'] : null,
                                    "status"        => !empty($status) ? $status : 'LS999',
                                    "created_by"    => !empty($status) ? ($status != 'LS999' ? Auth::user()->userid : null) : null,
                                    "updated_at"    => Carbon::now(),
                                    "active_flag"   => !empty($status) ? ($status != 'LS999' ? 1 : null) : null
                                ];
                            }
                        }

                        // Upload validation
                        if(!empty($insert) && empty($existingEmail))
                        {
                            $messages = [
                                'required'  => 'Please fill up the required fields.',
                                'numeric'   => 'Contact or Office Number must be number format',
                                'email'     => 'Make sure the email is a valid email'
                            ];

                            $validator = Validator::make($insert, [
                                '*.company'           => 'required',
                                '*.email'             => 'required|email',
                                '*.contact_number'    => isset($value['contactnumber']) ? 'numeric' : '',
                                '*.office_number'     => isset($value['officenumber']) ? 'numeric' : '',
                            ], $messages);

                            if ($validator->fails()) {
                                return redirect()->back()
                                            ->withErrors($validator)
                                            ->with([
                                                'alert'   => 'error',
                                                'message' => 'Some of the fields has an error',
                                                'button'  => 'OK',
                                                'confirm' => 'true'
                                            ]);
                                        }

                            // Save the data
                            $insertData = DB::table('leads')->insert($insert);
                            
                            // Session flash message
                            Session::flash('insert', $saveToDb);  

                            return redirect()->route('lobby.list')->with([
                                'alert'   => 'success',
                                'message' => 'Successfully imported the leads',
                                'button'  => 'OK',
                                'confirm' => 'true'   
                            ]);  

                            
                            
                        } 
                        else if(!empty($insert) && !empty($existingEmail))
                        {
                            $messages = [
                                'required'  => 'Please fill up the required fields.',
                                'numeric'   => 'Contact or Office Number must be number format',
                                'email'     => 'Make sure the email is a valid email'
                            ];

                            $validator = Validator::make($insert, [
                                '*.company'           => 'required',
                                '*.email'             => 'required|email',
                                '*.contact_number'    => isset($value['contactnumber']) ? 'numeric' : '',
                                '*.office_number'     => isset($value['officenumber']) ? 'numeric' : '',
                            ], $messages);

                            if ($validator->fails()) {
                                return redirect()->back()
                                            ->withErrors($validator)
                                            ->with([
                                                'alert'   => 'error',
                                                'message' => 'Some of the fields has an error',
                                                'button'  => 'OK',
                                                'confirm' => 'true'
                                            ]);
                                        }

                            
                            $insertData = DB::table('leads')->insert($insert);

                            Session::flash('insert', $saveToDb);  
                                                
                            Session::flash('existingEmail', $existingEmail);

                                    
                            return redirect()->route('lobby.list')->with([
                                'alert'   => 'success',
                                'message' => 'Successfully imported the leads but with errors!',
                                'button'  => 'OK',
                                'confirm' => 'true'    
                            ]);
                            
                            
                            
                        }
                        else
                        {
                            Session::flash('existingEmail', $existingEmail);

                            return redirect()->back()
                                            ->with([
                                                'alert'   => 'error',
                                                'message' => 'Error uploading file! Please check the error message and try again',
                                                'button'  => 'OK',
                                                'confirm' => 'true'
                                            ]);
                        }
                    }

                }
                
            }
            else
            {
                Session::flash('existingEmail', $existingEmail);

                return redirect()->back()
                                ->with([
                                    'alert'   => 'error',
                                    'message' => 'File is a .'.$extension.' file!! Please upload a valid xls/csv file..!',
                                    'button'  => 'OK',
                                    'confirm' => 'true'
                                ]);

            }
        }

        
    }

    public function downloadLead()
    {
        $file = public_path()."/file/import_multiple_leads - final.xlsm";
        $headers = array('Content-Type: application/vnd.ms-excel.sheet.macroEnabled.12',);
        return \Response::download($file, 'import_multiple_leads.xlsm', $headers);
    }

    function tblLobby ( $lobby )
    {
        if ( count ( $lobby ) > 0 )
            foreach ( $lobby as $row ) {
                $result[] = $row;
            }
        else
            $result = "";


        return $result;

    }


}
