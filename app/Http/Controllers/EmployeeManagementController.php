<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Validator;
use App\User;
use App\Role;
use App\Access;
use App\System;
use App\Employee;
use App\Designation;
use App\PaginationPerPage;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EmployeeManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function list()
    {
        $user = Auth::user();

        $items = PaginationPerPage::where('table_name','employees')->where('userid',$user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $data = Employee::orderBy('date_hired','desc')->whereNull('delete_flag')->paginate($perPage);
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;
        $roles = Role::all();

        foreach($data as $row)
        {
            $employees[] = [
                'emp_id' => $row->emp_id,
                'name' => $row->l_name.', '.$row->f_name.' '.$row->m_name,
                'designation' => $row->emp_designation->designation,
                'status' => $row->emp_status->systemdesc,
                'role' => !empty($row->UserInfo) ? $row->UserInfo->userRole->syscode->systemdesc : '',
                'date' => !empty($row->date_hired) ? date('d M Y', strtotime($row->date_hired)) : 'unspecified'
            ];
        }

        if (request()->ajax())
        {
            return response()->json(['pagination' => $data, 'employees' => $employees, 'access' => $access, 'role' => $role],200);
        }

        app(__('global.Audit'))->logAuditTrail("MD004","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);

        return view('employee.list')->with(compact('data','roles','user','items'));
    }

    public function create()
    {
        $user           = Auth::user();
        $civil_statuses = System::where('systemtype','civilstatus')->get();
        $designations   = Designation::orderBy('designation','asc')->get();
        $departments    = System::where('systemtype', 'department')->where('activeflag', 1)->orderBy('systemcode','asc')->get();
        $statuses       = System::where('systemtype','employmentstatus')->orderBy('systemdesc','asc')->get();
        $roles          = Role::where('activeflag',1)->get();

        app(__('global.Audit'))->logAuditTrail("MD004","AT002","Create Employee",null,null,request()->ip(),$user->userid,$user->role);

        return view('employee.create')->with(compact('user','civil_statuses','departments','designations','statuses','roles'));
    }

    public function view()
    {
        return view('employee.view');
    }

    public function save(Request $request)
    {   
        $input = $request->except('_token');

        $rules = [
            'username'  => 'required|min:8|max:50|unique:users',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->whereNull('deleted_at');
                }),
            ],
            'role' => 'required',
            'f_name' => 'required',
            'l_name' => 'required',
            'nationality' => 'required',
            'religion' => 'required',
            'highest_education' => 'required',
            'address' => 'required',
            'personal_email' => 'email|unique:employees',
            'bank_name' => $input['branch'] == 'sg' ? 'required' : '',
            'bank_acc_no' => $input['branch'] == 'sg' ? 'required' : '',
            'payee_name' => $input['branch'] == 'sg' ? 'required' : '',
            'bank_code' => $input['branch'] == 'sg' ? 'required' : '',
            'branch_code' => $input['branch'] == 'sg' ? 'required' : '',
            'emergency_contact_name' => 'required',
            'emergency_contact_number' => 'required',
            'emergency_contact_rel' => 'required',
            'date_hired' => 'required',
            'nric_id' => $input['branch'] == 'sg' ? 'required|min:7|max:10|unique:employees' : '',
        ];

        $messages = [
            'required' => 'This field is required.',
            'min'      => 'This field must be atleast :min characters',
            'max'      => 'This field must be no more than :max characters',
            'email'    => 'This field should be a valid email address'
        ];
        
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        foreach($input as $k => $v)
        {
            if(!in_array($k,['username','email','role']))
                $employee_info[$k] = $v;
            else
                $user[$k] = $v;
        }

        Employee::create($employee_info);

        $password = $this->randomString(7).'0oM';
        $padded = date('Ymd');
        $recentUser = User::where('userid','like','%'.$padded.'%')->orderby('created_at','desc')->first();
        $recentUserID = $recentUser == null ? 1 : (int)substr($recentUser->userid,8) + 1;
        $userid = $padded.sprintf('%02d',$recentUserID);

        $user_info = array_merge($user,['emp_id' => $input['emp_id'],'userid'=> $userid,'password'=> bcrypt($password)]);

        User::create($user_info);

        $module = [
            'MD002' => $input['role'] == 1 || $input['role'] == 2 ? '["C","R","U","D"]' : '',
            'MD003' => '["C","R","U","D"]'
        ];

        foreach($module as $n => $row)
        {
            Access::create([
                'userid' => $userid,
                'module' => $n,
                'access' => $row
            ]);
        }

        $emailDetails = ['name' => $input['f_name'],'username' => $input['username'], 'email' => $input['email'], 'password' => $password];

        \Mail::send('layouts.emails.userregistration',$emailDetails,function($message) use ($emailDetails){
           $message->to($emailDetails['email'])->subject
              ('CRM System - Login Credentials');
           $message->from('noreply@oom.com.sg','CRM - Employee Registration');
        });

        $user = Auth::user();
        $createdUser = User::where('userid',$userid)->first();

        app(__('global.Audit'))->logAuditTrail("MD004","AT010","Created employee ".$user->UserInfo->f_name.' '.$user->UserInfo->l_name,null,null,request()->ip(),$user->userid,$user->role);

        return 
                redirect()
                ->route('employee.list')
                ->with([
                    'alert'   => 'success',
                    'message' => 'New Employee is created',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function edit($emp_id)
    {
        $civil_statuses = System::where('systemtype','civilstatus')->get();
        $designations   = Designation::orderBy('designation','asc')->get();
        $departments    = System::where('systemtype', 'department')->where('activeflag', 1)->orderBy('systemcode','asc')->get();
        $statuses       = System::where('systemtype','employmentstatus')->get();
        $employee       = Employee::where('emp_id',$emp_id)->first();
        $roles          = Role::where('activeflag',1)->get();
        $emp_user       = User::where('emp_id',$emp_id)->first();
        $user           = Auth::user();

        app(__('global.Audit'))->logAuditTrail("MD004","AT004","Edit employee ".$employee->f_name.' '.$employee->l_name,null,null,request()->ip(),$user->userid,$user->role);

        return view('employee.edit')->with(compact('employee','user','emp_user','roles','civil_statuses','designations','departments','statuses'));
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $thisuser = User::where('emp_id',$input['emp_id'])->first();

        $rules = [
            'username'  => 'required|min:8|max:50|unique:users,emp_id,' . $thisuser->emp_id,
            'email' => 'required|email|unique:users,emp_id,' . $thisuser->emp_id,
            'role' => 'required',
            'f_name' => 'required',
            'l_name' => 'required',
            'nationality' => 'required',
            'religion' => 'required',
            'highest_education' => 'required',
            'address' => 'required',
            'personal_email' => 'email|unique:employees,emp_id,' . $thisuser->emp_id,
            'bank_name' => $input['branch'] == 'sg' ? 'required' : '',
            'bank_acc_no' => $input['branch'] == 'sg' ? 'required' : '',
            'payee_name' => $input['branch'] == 'sg' ? 'required' : '',
            'bank_code' => $input['branch'] == 'sg' ? 'required' : '',
            'branch_code' => $input['branch'] == 'sg' ? 'required' : '',
            'emergency_contact_name' => 'required',
            'emergency_contact_number' => 'required',
            'emergency_contact_rel' => 'required',
            'date_hired' => 'required',
            'nric_id' => $input['branch'] == 'sg' ? ($thisuser->userInfo->nric_id == $input['nric_id'] ? 'required|min:7|max:10' : 'required|min:7|max:10|unique:employees') : '',
        ];

        $messages = [
            'required' => 'This field is required.',
            'min'      => 'This field must be atleast :min characters',
            'max'      => 'This field must be no more than :max characters',
            'email'    => 'This field should be a valid email address'
        ];
        
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        $employee = Employee::where('emp_id',$input['emp_id'])->whereNull('delete_flag')->first();


        $user     = User::where('emp_id',$input['emp_id'])->first();

        foreach($input as $key => $row)
        {
            foreach($employee->toArray() as $k => $v)
            {
                if (!in_array($k,['id','created_at','updated_at','delete_flag','deleted_at']))
                {
                    if ($k == $key)
                    {
                        $employee->$k = $row;
                    }
                }

            }

            foreach($user->toArray() as $i => $j)
            {
                if (!in_array($i,['id','created_at','updated_at','delete_flag','deleted_at','password','userid']))
                {
                    if ($i == $key)
                    {
                        $user->$i = $row;
                    }
                }
            }
        }

        $employee->update();
        $user->update();

        app(__('global.Audit'))->logAuditTrail("MD004","AT005","Updated employee ".$employee->f_name.' '.$employee->l_name,null,null,request()->ip(),$user->userid,$user->role);

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Employee '.$employee->f_name.' '.$employee->l_name.' is now updated',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function delete(Request $request)
    {
        $input = $request->except('_token');

        $employee = Employee::where('emp_id',$input['empid'])->whereNull('delete_flag')->first();

        if (empty($employee))
        {
            app(__('global.Audit'))->logAuditTrail("MD004","AE004","Employee Deletion Error",null,null,request()->ip(),$user->userid,$user->role);

            return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Employee not found or already deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        }
        
        $del_employee = $employee->f_name.' '.$employee->l_name;
        $employee->delete_flag = 1;
        $employee->deleted_at = Carbon::now();
        $employee->update();

        $user = User::where('emp_id',$input['empid'])->first();
        $user->delete_flag = 1;
        $user->deleted_at = Carbon::now();
        $user->update();

        app(__('global.Audit'))->logAuditTrail("MD004","AT006","Deleted employee ".$del_employee,null,null,request()->ip(),$user->userid,$user->role);

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Employee '.$del_employee.' has been deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    public function search($employee)
    {
        $items = PaginationPerPage::where('table_name','employees')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;

        if ($employee == null)
        {
            $employees = Employee::whereNull('delete_flag')->paginate($perPage);
            $result = $this->tblEmployee($employees);
            return response()->json(['result' => $result,'pagination' => $employees],200);
        }

        $byEmpID = Employee::where('emp_id','like','%'.$employee.'%')->whereNull('delete_flag')->paginate($perPage);
        $byName = Employee::whereNull('delete_flag')->where('f_name','like','%'.$employee.'%')->orWhere('l_name','like','%'.$employee.'%')->paginate($perPage);
        $byDesignation = Employee::FindDesignation($employee);
        $byStatus = Employee::FindStatus($employee);
        $byRole = Employee::FindRole($employee);

        if (!empty($byEmpID->toArray()['data']))
            $employees = $byEmpID;
        else if (!empty($byName->toArray()['data']))
            $employees = $byName;
        else if(!empty($byDesignation->toArray()['data']))
            $employees = $byDesignation;
        else if(!empty($byStatus->toArray()['data']))
            $employees = $byStatus;
        else if(!empty($byRole->toArray()['data']))
            $employees = $byRole;
        else
            $employees = "";

        $result = empty($employees) ? '' : $this->tblEmployee($employees);

        return response()->json(['employees' => $result,'pagination' => $employees,'access' => $access, 'role' => $role],200);
    }

    function refresh()
    {
        $items = PaginationPerPage::where('table_name','employees')->whereNull('delete_flag')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;
        $employees = Employee::whereNull('delete_flag')->paginate($perPage);
        $result = $this->tblEmployee($users);

        return response()->json(['result' => $result,'pagination' => $users,'access' => $access, 'role' => $role],200);
    }

    function tblEmployee($employees)
    {
        if(count($employees) > 0)
            foreach($employees as $row)
            {
                $result[] = [
                    'emp_id' => $row->emp_id,
                    'name' => $row->l_name.', '.$row->f_name.' '.$row->m_name,
                    'designation' => $row->emp_designation->designation,
                    'status' => $row->emp_status->systemdesc,
                    'role' => !empty($row->UserInfo) ? $row->UserInfo->userRole->syscode->systemdesc : '',
                    'date' => !empty($row->date_hired) ? date('d M Y', strtotime($row->date_hired)) : 'unspecified'
                ];
            }
        else
            $result[] = [];

        return $result;
    }

    public function sort($field,$order)
    {
        $employees = Employee::Sort($field,$order);
        $result = $this->tblEmployee($employees);
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;

        return response()->json([
          'employees'  => $result,
          'pagination' => $employees,
          'field'      => $field,
          'order'      => $order,
          'access'     => $access,
          'role'       => $role
        ],200);
    }

    public function newEmployee($branch)
    {
        $lastEmp = Employee::where('branch',$branch)->whereNull('delete_flag')->orderby('emp_id','desc')->first();
        $padded = strtoupper($branch);
        $lastEmpID = $lastEmp == null ? 1 : (int)substr($lastEmp->emp_id,3) + 1;
        $newEmp = $padded.sprintf('%03d',$lastEmpID);

        return response()->json($newEmp,200);
    }

    public function designations($dept)
    {
        if(strpos($dept,'SG') !== false || strpos($dept,'PH') !== false)
        {
            $employee = Employee::where('emp_id',$dept)->whereNull('delete_flag')->first();
            $designations = Designation::whereIn('dept_code', [$employee->dept_code, 'DP011'])->get();
            return response()->json(['designations' => $designations, 'selected' => $employee->designation ],200);
        }
            
        $designations = Designation::whereIn('dept_code', [$dept, 'DP011'])->get();

        return response()->json($designations,200);
    }

    function randomString($length = 5)
    {
    		$str = "";
    		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    		$max = count($characters) - 1;

        for ($i = 0; $i < $length; $i++) {
    			$rand = mt_rand(0, $max);
    			$str .= $characters[$rand];
    		}

    		return $str;
  	}
}
