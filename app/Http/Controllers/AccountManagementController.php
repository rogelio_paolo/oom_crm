<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Validator;
use App\User;
use App\Team;
use App\Lead;
use App\System;
use App\Manager;
use App\Employee;
use App\Account;
use App\Contract;
use App\AccountPpc;
use App\AccountFb;
use App\AccountSeo;
use App\AccountTm;
use App\AccountWeb;
use App\AccountBaidu;
use App\AccountWeibo;
use App\AccountWeChat;
use App\AccountBlogContent;
use App\AccountSocialManagement;
use App\AccountPostpaidSEM;
use App\LeadNote;
use App\PaginationPerPage;
use App\ModifyAccountsColumn;
use App\FilterSelection;
use App\Industry;

use DB;
use App\Notifications\AccountUpdatedNotification;
use App\Notifications\AccountDeletedNotification;
use App\Notifications\AccountAssignedNotification;
use App\Notifications\AccountAssignedPerPackageNotification;
use App\Notifications\NotifySalesPersonWhenAssignedNotification;
use Dompdf\Dompdf;
use Illuminate\Http\Request;

use App\Http\Requests\MultipleUpdateAMHolderPostRequest;
use Session;
use Response;

class AccountManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function modifyAccountsColumn(Request $request)
    {
        $input = $request->except('_token');
        $user = Auth::user();
       
        foreach($input as $k => $v)
        {
            $columnValuesArray[$k] = $v;
        }

        $columnValues = array_merge($columnValuesArray, ['userid' => $user->userid]);

        $accountsColumnUser = ModifyAccountsColumn::updateOrCreate(['userid' => $user->userid], $columnValues);

        return response()->json(['accounts_column' => $accountsColumnUser], 200);
    }

    public function paginationPerPage($tblName, $item)
    {
        $user = Auth::user();
        $pagination = PaginationPerPage::where('table_name',$tblName)->where('userid',$user->userid)->first();

        if(!empty($pagination))
        {
            $pagination->per_page = $item;
            $pagination->update();
        }
        else
        {
            DB::table('pagination_per_page')->insert(['table_name' => $tblName, 'per_page' => $item, 'userid' => $user->userid ]);
        }
        
    }
    
    public function list()
    {
        Session::forget('direct');
        Session::forget('opportunity');

        $user = Auth::user();
        $data     = $this->tblAccounts();
        $user     = $data['user'];
        $accounts = $data['accounts'];
        $result   = $data['result'];
        $managers = $data['managers'];
        $teamcode = $data['teamcode'];
        $items    = $data['perpage'];
        $role     = $data['role'];
        $column   = $data['column'];
        $filter   = $data['filter'];
        $acc_team = Employee::accTeam();
        $acc_list = Account::accList();

        $services = System::where('systemtype', 'nature')->where('activeflag', 1)->get();
        $contract_types = System::where('systemtype', 'contract_type')->where('activeflag', 1)->get();
        $lead_statuses = System::where('systemtype', 'leadstatus')->where('activeflag', 1)->get();
        $lead_sources = System::where('systemtype', 'leadsource')->where('activeflag', 1)->get();
        $industries = Industry::orderBy('code', 'asc')->get();
        $landings = System::where('systemtype', 'landingpage')->where('activeflag', 1)->get();

        // $accounts = Account::whereNull('delete_flag')->whereNotNull('contract_id')->take(1)->get();
        // $emails = array();

        // foreach ($accounts as $k => $row)
        // {
        //     $collects[] = [
        //         'id'            => $row->id,
        //         'company'       => $row->contract->company,
        //         'contract_no'   => $row->contract->contract_number,
        //         'day_90'        => $row->contract->day_90->format('Y-m-d'),
        //         'day_120'       => $row->contract->day_120->format('Y-m-d'),
        //         'created'       => $row->contract->created_at->format('Y-m-d'),
        //         'sem'           => !empty($row->ppc_id)? $row->sem->sem_strategist_assigned : '',
        //         'sem_asst'      => $row->sem ? $row->sem->asst_sem_strategist_assigned : '',
        //         'seo'           => $row->seo ? $row->seo->seo_strategist_assigned : '',
        //         'sem_asst'      => $row->seo ? $row->seo->asst_seo_strategist_assigned : '',
        //         'seo_link'      => $row->seo ? $row->seo->seo_link_builder_assigned : '',
        //         'asst_seo_link' => $row->seo ? $row->seo->asst_seo_link_builder_assigned : '',
        //         'fb'            => $row->fb ? $row->fb->fb_strategist_assigned : '',
        //         'fb_asst'       => $row->fb ? $row->fb->asst_fb_strategist_assigned : '',
        //         'fb_link'       => $row->fb ? $row->fb->fb_link_builder_assigned : '',
        //         'asst_fb_link'  => $row->fb ? $row->fb->asst_fb_link_builder_assigned : '',
        //         'web'           => $row->web ? $row->web->dev_team : '',
        //         'acc'           => $row->acc_assigned ? $row->acc_assigned : '',
        //         'acc_asst'      => $row->acc_assistant ? $row->acc_assistant : '',
        //         'busi_manager'  => $row->created_id ? User::where('userid', $row->created_id)->first()->emp_id : ''
        //     ];
        // }

        // foreach($collects as $k => $v)
        // {
        //     $filtered_members[] = [
        //         $this->getEmail($v['sem']),
        //         $this->getEmail($v['sem_asst']),
        //         $this->getEmail($v['seo']),
        //         $this->getEmail($v['sem_asst']),
        //         $this->getEmail($v['seo_link']),
        //         $this->getEmail($v['asst_seo_link']),
        //         $this->getEmail($v['fb']),
        //         $this->getEmail($v['fb_asst']),
        //         $this->getEmail($v['fb_link']),
        //         $this->getEmail($v['asst_fb_link']),
        //         $this->getEmail($v['web']),
        //         $this->getEmail($v['acc']),
        //         $this->getEmail($v['acc_asst']),
        //         $this->getEmail($v['busi_manager'])
        //     ];
        // }

        // foreach($filtered_members as $k => $row)
        // {
        //     foreach($row as $key => $r)
        //     {
        //         if (empty($r)) 
        //         {
        //             unset($row[$key]);
        //         }            
        //     }

        //     $emails[] = array_flatten($row);
        // }

        // foreach($collects as $row)
        // {
        //     if (Carbon::now()->format('Y-m-d') == Carbon::parse($row['day_90'])->addDay()->format('Y-m-d'))
        //     {
        //         foreach($emails as $email)
        //         {
        //             foreach($email as $row2)
        //             {
        //                 $rowUser = User::where("email", $row2)->first();
        //                 $userData[] = $rowUser->userInfo->f_name . ' ' . $rowUser->userInfo->l_name;
        //             }
                    
        //             $member = implode(', ', $userData);
        
        //             $emailDetails = [
        //                 'company' => $row['company'],
        //                 'created' => Carbon::parse($row['created'])->format('d M Y'),
        //                 'day_90' => Carbon::parse($row['day_90'])->format('d M Y'),
        //                 'day_120' => Carbon::parse($row['day_120'])->format('d M Y'),
        //                 'contract_no' => $row['contract_no'],
        //                 'member'  => $member,
        //                 'accountId' => $row['id']
        //             ];

        //             \Mail::send('layouts.emails.notifyallmembers', $emailDetails, function($message) use ($emailDetails, $email) {
        //                 $message->to($email)->subject('CRM - ' . $emailDetails['company'] . ' with Contract # ' . $emailDetails['contract_no'] . ' reached 90 days');
        //                 $message->from('noreply@oom.com.sg','CRM Admin');
        //             });
        //         }
        //     }
        // }

        app(__('global.Audit'))->logAuditTrail("MD003","AT010","Accessed Module",null,null,request()->ip(),$user->userid,$user->role);

        if (request()->ajax())
        {
            return response()->json(['result' => $data['result'], 'pagination' => $data['accounts'], 'managers' => $data['managers'], 'teamcode' => $data['teamcode'], 'user' => $data['user'], 'role' => $data['role'], 'column' => $data['column'], 'acc_team' => $acc_team, 'acc_list' => $acc_list, 'filter' => $filter],200);
        }

        return view('account.list')->with(compact('accounts','result','managers','teamcode','user','items','role','column',
            'acc_team', 'acc_list', 'filter', 'services', 'contract_types', 'lead_statuses', 'lead_sources', 'industries', 'landings'));
    }

    function getEmail($row)
    {
        $user = User::where('emp_id', '=', $row)->first();
        return $row != '' ? $user->email : '';

    }

    public function edit($id)
    {   
        $user          = Auth::user();
        $managers_list = Manager::select('emp_id')->get();
        $managers      = array_flatten($managers_list->toArray());
        $account       = Account::find($id);
        $account_hist  = Account::where('prospect_id', $account->prospect->id)->whereNull('delete_flag')->orderBy('created_at','asc')->whereNull('active_flag')->get();
        
        // dd($contract);

        if (empty($account))
        {
            app(__('global.Audit'))->logAuditTrail("MD003","AE002","Account does not exist",null,null,request()->ip(),$user->userid,$user->role);

            return
                    redirect()
                    ->route('account.list')
                    ->with([
                        'alert'   => 'error',
                        'message' => 'Account not found or no access to it',
                        'button'  => 'OK',
                        'confirm' => 'true'
                    ]);
        }

        $statuses      = System::where('systemtype','accountstatus')->where('activeflag','!=',0)->get();
        $nature        = System::where('systemtype','nature')->get();
        $notes         = LeadNote::where('lead_id',$account->prospect->lead_id)->paginate(5);
        $countries     = \DB::table('countries')->select(\DB::raw('distinct country,code'))->orderBy('country','asc')->where('country','!=','')->get();
        $payment       = System::where('systemtype','paymentmethod')->orderBy('systemcode','desc')->get();
        $listings      = System::where('systemtype','accountlisting')->where('systemcode', '!=', 'LA003')->orderBy('systemcode','asc')->get();
        $listings_fb   = System::where('systemtype','accountlisting')->where('systemcode', '!=', 'LA002')->orderBy('systemcode','asc')->get();
        $channels      = System::where('systemtype','mediachannel')->get();
        $fbchannels    = System::where('systemtype','fbmediachannel')->get();
        $duration      = System::where('systemtype','campaignduration')->orderby('systemcode','asc')->get();
        $webtype       = System::where('systemtype','projecttype')->get();
        $cmstype       = System::where('systemtype','cmstype')->get();
        $keywordsno    = System::where('systemtype','keywordsno')->get();
        $otherduration = System::where('systemtype','otherduration')->orderBy('systemcode', 'desc')->get();
        $commentors    = LeadNote::where('lead_id',$id)->select('userid')->distinct()->get();
        $baidu_channels = System::where('systemtype','baidumediachannel')->orderBy('systemcode', 'asc')->get();
        $advertising = System::where('systemtype', 'wechatadvertising')->orderBy('systemcode', 'asc')->get();
        $contract_type = System::where('systemtype', 'contract_type')->orderBy('systemcode', 'asc')->get();
        $industries = Industry::orderBy("title", "asc")->get(); // Industries fetch list

        if(request()->ajax())
        {
            foreach($notes as $row)
            {
                $leadnotes[] = [
                    'name' => $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name,
                    'date' => date_format($row->created_at,'Y-m-d h:i A'),
                    'id'   => $row->id,
                    'note' => $row->note,
                    'created_user' => $row->userid
                ];
            }

            return
                response()
                ->json([
                    'pagination' => $notes,
                    'notes'      => $leadnotes,
                    'user'       => Auth::user()->userid,
                    'history'    => $account_hist 
                ],200);
        }
            
        app(__('global.Audit'))->logAuditTrail("MD003","AT004","Edit account ".$account->prospect->company,null,null,request()->ip(),$user->userid,$user->role);

        return 
            view('account.edit')
            ->with(compact(
                'user',
                'account',
                'statuses',
                'nature',
                'notes',
                'countries',
                'payment',
                'channels',
                'fbchannels',
                'duration',
                'webtype',
                'cmstype',
                'keywordsno',
                'otherduration',
                'commentors',
                'baidu_channels',
                'advertising',
                'account_hist',
                'managers',
                'contract',
                'listings',
                'listings_fb',
                'contract_type',
                'industries'
            ));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $input = $request->except('_token');
        // dd($input);
        $managers_list = Manager::select('emp_id')->get();
        $managers      = array_flatten($managers_list->toArray());

        if (!isset($input['nature']))
            return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Must select atleast 1 service',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        
        $package = implode('|',$input['nature']);

        $account = Account::find($input['accountid']);

        $account->nature = $package;

        $changes = 0;

        $created_id = Auth::user()->userid;

        $prospect = $input['account']['prospect'];
        $contract = $input['account']['contract'];
                    
        foreach($input['account'] as $key => $row)
        {
        
            if($input[$key.'_included'] != 'yes')
                continue;

            if ($key == 'prospect' && $key == 'contract')
                if ($account->acc_assigned != $user->emp_id || ($account->created_id != $user->userid && $user->userInfo->team->team_code != 'busi' || $user->userInfo->team->team_code == 'acc'))
                continue;

            foreach($input['account'][$key] as $i => $j)
            {
                $input_fields[$i] = $j;
                ${$key}[$i] = is_array($j) ? implode('|',$j) : $j;
                if (!empty($account->$key) && $account->$key->$i != ${$key}[$i])
                {
                    $oldval[$i] = $account->$key->$i;
                    $newval[$i] = $i;
                    $changes++;
                }
            }

        }

            
        if ($account->acc_assigned == $user->emp_id || ($user->userInfo->team->team_code == 'busi' || $user->userInfo->team->team_code == 'acc'))
        {
            if ((isset($account->contract->company) || $account->prospect->lead->company) != $contract['company'])
            {
                $changes++;
                $oldval['company'] = !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company;
                $newval['company'] = $contract['company'];
            }
            
            if ((isset($account->contract->f_name) || $account->prospect->lead->f_name) != trim(strtok($contract['client_name'],' ')) )
            {
                $changes++;
                $oldval['f_name'] = !empty($account->contract->f_name) ? $account->contract->f_name : $account->prospect->lead->f_name;
                $newval['f_name'] = trim(strtok($contract['client_name'],' '));
            }

            if ((isset($account->contract->l_name) || $account->prospect->lead->l_name) != trim(strstr($contract['client_name'],' ')) )
            {
                $changes++;
                $oldval['l_name'] = !empty($account->contract->l_name) ? $account->contract->l_name : $account->prospect->lead->l_name;
                $newval['l_name'] = trim(strstr($contract['client_name'],' '));
            }

            if ((isset($account->contract->contact_number) || $account->prospect->lead->contact_number) != $contract['client_number'])
            {
                $changes++;
                $oldval['contact_number'] = !empty($account->contract->contact_number) ? $account->contract->contact_number : $account->prospect->lead->contact_number;
                $newval['contact_number'] = $contract['client_number'];
            }

            if ((isset($account->contract->email) || $account->prospect->lead->email) != $contract['email'])
            {
                $changes++;
                $oldval['email'] = !empty($account->contract->email) ? $account->contract->email : $account->prospect->lead->email;
                $newval['email'] = $contract['email'];
            }

            if ($account->prospect->lead->website != $prospect['website'])
            {
                $changes++;
                $oldval['website'] = $account->prospect->lead->website;
                $newval['website'] = $prospect['website'];
            }

            if ($account->prospect->lead->industry != $prospect['industry'])
            {
                $changes++;
                $oldval['industry'] = $account->prospect->lead->industry;
                $newval['industry'] = $prospect['industry'];
            }
            
        }


        $rules = [
            'renew_date' => isset($additional['is_renew']) ? $additional['is_renew'] == '1' ? 'required|date' : '' : '',
            'contact_page_url' =>  isset($additional['contact_page_url']) ? 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/' : '',
            'ty_page_url' =>  isset($additional['ty_page_url']) ? 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/' : '',
            'historical_data.*' => isset($input['historical_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'fb_hist_data.*' => isset($input['fb_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'seo_hist_data.*' => isset($input['seo_hist_data']) ?'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'baidu_hist_data.*' => isset($input['baidu_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'weibo_hist_data.*' => isset($input['weibo_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'blog_hist_data.*' => isset($input['blog_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'social_hist_data.*' => isset($input['social_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
            'wechat_hist_data.*' => isset($input['wechat_hist_data']) ? 'mimes:pdf,doc,docx,xls,xlsx|max:10000' : '',
        ];

        if ($user->role == 1 || in_array($user->userInfo->team->team_code, ['hr']))
            $rules['contract_number'] =  isset($input['contract_number']) ? 'regex:/^(\d{4})(-+)(\d{3})$/' : '';

        if ($account->acc_assigned == $user->emp_id || ($user->userInfo->team->team_code == 'busi' || $user->userInfo->team->team_code == 'acc'))
        {
            $rules['company'] = empty($contract['company']) ? 'required' : '';
            $rules['client_email'] = empty($contract['email']) ? 'required|email' : '';
            $rules['contact_number'] = isset($contract['client_number']) ? 'numeric' : '';
        }

        if (!empty($account->sem) && $input['sem_included'] == 'yes')
        {
            foreach($sem as $i => $j)
            {
                if ($sem['duration'] != 'CD999' && $i == 'duration_other')
                    continue;

                if (strpos($i,'_historical_data') || $i == 'historical_data')
                    continue;

                if (strpos($i,'datefrom'))
                    $sem_rule = $input['campaign_date_set'] == '1' ? 'required|date|before:campaign_dateto' : '';
                else if (strpos($i,'dateto'))
                    $sem_rule = $input['campaign_date_set'] == '1' ? 'required|date|after:campaign_datefrom' : '';
                else if($i == 'has_historical_data')
                    $rules['historical_data'] = $input['has_historical_data'] == 1 ? 'required' : '';
                else if($i == 'ad_scheduling_remark')
                    $sem_rule = $sem['ad_scheduling'] == '1' ? 'required' : '';
                //else if ($i == 'am_fee')
                    //$sem_rule = 'required|numeric';
                else
                    $sem_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$sem_rule);
            }
        }

        if (!empty($account->fb) && $input['fb_included'] == 'yes')
        {
            foreach($fb as $i => $j)
            {
                if ($i == 'fb_included')
                    continue;
                
                if ($fb['fb_duration'] != 'CD999' && $i == 'fb_duration_other')
                    continue;

                if (strpos($i,'_historical_data') || $i == 'fb_hist_data')
                    continue;

                if (strpos($i,'dt_from'))
                    $fb_rule = $input['fb_campaign_date_set'] == 1 ? 'required|date|before:fb_campaign_dt_to' : '' ;
                else if (strpos($i,'dt_to'))
                    $fb_rule = $input['fb_campaign_date_set'] == 1 ? 'required|date|after:fb_campaign_dt_from' : '';
                else if($i == 'has_fb_hisdata')
                    $rules['fb_hist_data'] = $fb['has_fb_hisdata'] == 1 ? 'required' : '';
                else
                    $fb_rule = 'required' ;

                $rules = $this->array_push_assoc($rules,$i,$fb_rule);
            }
        }

        if (!empty($account->seo) && $input['seo_included'] == 'yes')
        {
            foreach($seo as $i => $j)
            {
                if ($i == 'keyword_maintenance')
                {
                    $rules['keyword_maintenance'] = $input['has_keyword_maintenance'] == '1' ? 'required' : '';
                    continue; 
                } 
                
                if ($seo['seo_duration'] != 'CD999' && $i == 'seo_duration_other')
                    continue;
                
                if ($seo['keyword_themes'] != 'NK999' && $i == 'keyword_themes_other')
                    continue;

                if ($i == 'seo_total_fee')
                    $rules[$i] = 'required|numeric';

                if ($i == 'has_seo_hisdata')
                    $rules['seo_hist_data'] = $seo['has_seo_hisdata'] == 1 ? 'required' : '';
                else
                    $rules[$i] = 'required' ;

            }
        }

        if (!empty($account->web) && $input['web_included'] == 'yes')
        {
            foreach($web as $i => $j)
            {
                $rules[$i] = 'required';
            }
        }

        if (!empty($account->baidu) && $input['baidu_included'] == 'yes')
        {
            foreach($baidu as $i => $j)
            {
                if ($baidu['baidu_duration'] != 'CD999' && $i == 'baidu_duration_other')
                    continue;
                if (strpos($i,'_baidu_hist_data') || $i == 'baidu_hist_data')
                    continue;
                
                if ($i == 'baidu_account')
                    $baidu_rule = $input['has_baidu_account'] == '1' ? 'required' : '';
                else if ($i == 'baidu_campaign_datefrom')
                    $baidu_rule = $input['has_baidu_campaign_dateset'] == '1' ? 'required|date|before:baidu_campaign_dateto' : '';
                else if ($i == 'baidu_campaign_dateto')
                    $baidu_rule = $input['has_baidu_campaign_dateset'] == '1' ? 'required|date|after:baidu_campaign_datefrom' : '';
                else if ($i == 'has_baidu_hisdata')
                    $rules['baidu_hist_data'] = $input['has_baidu_hisdata'] == '1' ? 'required' : '';
                else if($i == 'baidu_ad_scheduling_remark')
                    $baidu_rule = $baidu['has_baidu_ad_scheduling'] == '1' ? 'required' : '';
                else
                    $baidu_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$baidu_rule);
            }
        }

        if (!empty($account->weibo) && $input['weibo_included'] == 'yes')
        {
            foreach($weibo as $i => $j)
            {
                if ($weibo['weibo_duration'] != 'CD999' && $i == 'weibo_duration_other')
                    continue;

                if (strpos($i,'_weibo_hist_data') || $i == 'weibo_hist_data')
                    continue;

                if(in_array($i, ['weibo_campaign','weibo_duration','weibo_budget','weibo_monthly_budget','weibo_daily_budget']))
                    continue;
                
                if ($i == 'weibo_account')
                    $weibo_rule = $input['has_weibo_account'] == '1' ? 'required' : '';
                else if ($i == 'weibo_campaign_datefrom')
                    $weibo_rule = $input['has_weibo_campaign_dateset'] == '1' ? 'required|date|before:weibo_campaign_dateto' : '';
                else if ($i == 'weibo_campaign_dateto')
                    $weibo_rule = $input['has_weibo_campaign_dateset'] == '1' ? 'required|date|after:weibo_campaign_datefrom' : '';
                else if ($i == 'has_weibo_hisdata')
                    $rules['weibo_hist_data'] = $input['has_weibo_hisdata'] == '1' ? 'required' : '';
                else if($i == 'weibo_ad_scheduling_remark')
                    $weibo_rule = $weibo['has_weibo_ad_scheduling'] == '1' ? 'required' : '';
                else
                    $weibo_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$weibo_rule);
            }
        }

        if (!empty($account->wechat) && $input['wechat_included'] == 'yes')
        {
            foreach($wechat as $i => $j)
            {
                if ($i == in_array($i, ['wechat_age_from','wechat_age_to','wechat_marital','wechat_gender','wechat_handphone','wechat_education']))
                    continue;

                if ($i == in_array($i, ['wechat_type','has_wechat_menu_setup']))
                    $wechat_rule = $wechat['has_wechat_type'] == '1' ? 'required' : '';
                else if ($i == in_array($i, ['wechat_advertising_type','wechat_location']))
                    $wechat_rule = $input['has_wechat_advertising'] == 1 ? 'required' : '';
                else if ($i == 'has_wechat_hisdata')
                    $rules['wechat_hist_data'] = $input['has_wechat_hisdata'] == '1' ? 'required' : '';
                else
                    $wechat_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$wechat_rule);

            }
        }

        if (!empty($account->blog) && $input['blog_included'] == 'yes') 
        {
            foreach($blog as $i => $j)
            {
                if (strpos($i,'_blog_hist_data') || $i == 'blog_hist_data')
                    continue;

                if ($i == 'blog_monthly_post')
                    $blog_rule = isset($input['blog_monthly_post']) ? 'numeric' : '';
                else if ($i == 'blog_total_post')
                    $blog_rule = isset($input['blog_total_post']) ? 'numeric' : 'required';
                else if($i == 'has_blog_hisdata')
                    $rules['blog_hist_data'] = $input['has_blog_hisdata'] == 1 ? 'required' : '';
                else
                    $blog_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$blog_rule);
            }
        }

        if (!empty($account->social) && $input['social_included'] == 'yes')
        {
            foreach($social as $i => $j)
            {
                if ($social['social_duration'] != 'CD999' && $i == 'social_duration_other')
                    continue;
                if (strpos($i,'_social_hist_data') || $i == 'social_hist_data')
                    continue;

                if ($i == 'social_monthly_post')
                    $social_rule = 'required|numeric';
                else if ($i == 'social_media_budget')
                    $social_rule = $input['has_social_media_budget'] == '1' ? 'required:numeric' : '';
                else if($i == 'has_social_hisdata')
                    $rules['social_hist_data'] = $input['has_social_hisdata'] == 1 ? 'required' : '';
                else
                    $social_rule = 'required';

                $rules = $this->array_push_assoc($rules,$i,$social_rule);
            }
        }

        $input_fields['nature'] = $input['nature'];
        $input_fields['contract_number'] = $input['contract_number'];
        
        $messages = [
            'mimes' => 'Historical data must be doc,docx,pdf files only',
            'required' => 'field is required',
            'date' => 'field should be in date format yyyy-mm-dd',
        ];
        
        $validator = Validator::make($input_fields, $rules, $messages);

        if ($validator->fails()) {

            app(__('global.Audit'))->logAuditTrail("MD003","AE002","Account Update Error",null,null,request()->ip(),$user->userid,$user->role);

            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with([
                            'alert'   => 'error',
                            'message' => 'Some of the fields has an error, kindly check.',
                            'button'  => 'OK',
                            'confirm' => 'true'
                        ]);
        }

        foreach($input['account']['additional'] as $field => $value)
        {
            $account_tm = AccountTm::find($account->tm_id);
            $account_tm->$field = $value;
            $account_tm->update();
        }

        if (!empty($account->blog_id) && $input['blog_included'] == 'yes')
        {
            foreach($blog as $field => $value)
            {
                $service_blog = AccountBlogContent::find($account->blog_id);

                if($service_blog->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_blog->$field;
                    $newval[$field] = $value;
                }
                $service_blog->$field = $value;
                $service_blog->update();
            }

            if(isset($input['blog_hist_data']))
            {
                foreach($input['blog_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'blog/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $service_blog->blog_hist_data = !empty($service_blog->blog_hist_data) ? $service_blog->blog_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $service_blog->blog_hist_data_fname = !empty($service_blog->blog_hist_data_fname) ? $service_blog->blog_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $service_blog->update();

                $changes++;
                $oldval['blog_hist_data'] = explode('|',$service_blog->blog_hist_data_fname);
                $newval['blog_hist_data'] = explode('|',$service_blog->blog_hist_data_fname.'|'.implode('|',$original));
            }
        }
        elseif (empty($account->blog_id) && $input['blog_included'] == 'yes')
        {
             AccountBlogContent::create($blog);
             $blog_id = AccountBlogContent::max('id');
             $account->blog_id = $blog_id;
             
             if(isset($input['blog_hist_data']))
             {
                 $account_blog = AccountBlogContent::find($blog_id);
                 foreach($input['blog_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'blog/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_blog->blog_hist_data = implode('|',$hashed);
                 $account_blog->blog_hist_data_fname = implode('|',$original);
                 $account_blog->update();
             }

             $changes++;
        }
        elseif (!empty($account->blog_id) && $input['blog_included'] == 'no')
        {
            // Delete the account blog
            $blog_id = AccountBlogContent::find($account->blog_id);
            $blog_id->delete();

            // Empty the account blog_id & update the account nature dropdown
            $account->blog_id = 0;
            $account->nature = implode('|',$input['nature']);

            // Track changes
            $changes++;
            
        }

        if (!empty($account->social_id) && $input['social_included'] == 'yes')
        {
            foreach($social as $field => $value)
            {
                $service_social = AccountSocialManagement::find($account->social_id);

                if($service_social->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_social->$field;
                    $newval[$field] = $value;
                }
                $service_social->$field = $value;
                $service_social->update();
            }

            if(isset($input['social_hist_data']))
            {
                foreach($input['social_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'social/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $service_social->social_hist_data = !empty($service_social->social_hist_data) ? $service_social->social_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $service_social->social_hist_data_fname = !empty($service_social->social_hist_data_fname) ? $service_social->social_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $service_social->update();

                $changes++;
                $oldval['social_hist_data'] = explode('|',$service_social->social_hist_data_fname);
                $newval['social_hist_data'] = explode('|',$service_social->social_hist_data_fname.'|'.implode('|',$original));
            }
        }
        elseif (empty($account->social_id) && $input['social_included'] == 'yes')
        {
             AccountSocialManagement::create($social);
             $social_id = AccountSocialManagement::max('id');
             $account->social_id = $social_id;
             
             if(isset($input['social_hist_data']))
             {
                 $account_social = AccountSocialManagement::find($social_id);
                 foreach($input['social_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'social/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_social->social_hist_data = implode('|',$hashed);
                 $account_social->social_hist_data_fname = implode('|',$original);
                 $account_social->update();
             }

             $changes++;
        }
        elseif (!empty($account->social_id) && $input['social_included'] == 'no')
        {
             // Delete the account social
             $social_id = AccountSocialManagement::find($account->social_id);
             $social_id->delete();

             // Empty the account social_id & update the account nature dropdown
             $account->social_id = 0;
             $account->nature = implode('|',$input['nature']);

             // Track changes
             $changes++; 
        }

        if (!empty($account->baidu_id) && $input['baidu_included'] == 'yes')
        {
            foreach($baidu as $field => $value)
            {
                $service_baidu = AccountBaidu::find($account->baidu_id);

                if($service_baidu->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_baidu->$field;
                    $newval[$field] = $value;
                }
                $service_baidu->$field = $value;
                $service_baidu->update();
            }

            if(isset($input['baidu_hist_data']))
            {
                foreach($input['baidu_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'baidu/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $service_baidu->baidu_hist_data = !empty($service_baidu->baidu_hist_data) ? $service_baidu->baidu_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $service_baidu->baidu_hist_data_fname = !empty($service_baidu->baidu_hist_data_fname) ? $service_baidu->baidu_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $service_baidu->update();

                $changes++;
                $oldval['baidu_hist_data'] = explode('|',$service_baidu->baidu_hist_data_fname);
                $newval['baidu_hist_data'] = explode('|',$service_baidu->baidu_hist_data_fname.'|'.implode('|',$original));
            }
        }
        elseif (empty($account->baidu_id) && $input['baidu_included'] == 'yes')
        {
             AccountBaidu::create($baidu);
             $baidu_id = AccountBaidu::max('id');
             $account->baidu_id = $baidu_id;
             
             if(isset($input['baidu_hist_data']))
             {
                 $account_baidu = AccountBaidu::find($baidu_id);
                 foreach($input['baidu_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'baidu/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_baidu->baidu_hist_data = implode('|',$hashed);
                 $account_baidu->baidu_hist_data_fname = implode('|',$original);
                 $account_baidu->update();
             }

             $changes++;
         
             
        }
        elseif (!empty($account->baidu_id) && $input['baidu_included'] == 'no')
        {
           // Delete the account baidu
           $baidu_id = AccountBaidu::find($account->baidu_id);
           $baidu_id->delete();

           // Empty the account baidu_id & update the account nature dropdown
           $account->baidu_id = 0;
           $account->nature = implode('|',$input['nature']);

           // Track changes
           $changes++;
            
        }

        if (!empty($account->weibo_id) && $input['weibo_included'] == 'yes')
        {
            foreach($weibo as $field => $value)
            {
                $service_weibo = AccountWeibo::find($account->weibo_id);

                if($service_weibo->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_weibo->$field;
                    $newval[$field] = $value;
                }
                $service_weibo->$field = $value;
                $service_weibo->update();
            }

            if(isset($input['weibo_hist_data']))
            {
                foreach($input['weibo_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'weibo/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $service_weibo->weibo_hist_data = !empty($service_weibo->weibo_hist_data) ? $service_weibo->weibo_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $service_weibo->weibo_hist_data_fname = !empty($service_weibo->weibo_hist_data_fname) ? $service_weibo->weibo_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $service_weibo->update();

                $changes++;
                $oldval['weibo_hist_data'] = explode('|',$service_weibo->weibo_hist_data_fname);
                $newval['weibo_hist_data'] = explode('|',$service_weibo->weibo_hist_data_fname.'|'.implode('|',$original));
            }
        }
        elseif (empty($account->weibo_id) && $input['weibo_included'] == 'yes')
        {
             AccountWeibo::create($weibo);
             $weibo_id = AccountWeibo::max('id');
             $account->weibo_id = $weibo_id;
             
             if(isset($input['weibo_hist_data']))
             {
                 $account_weibo = AccountWeibo::find($weibo_id);
                 foreach($input['weibo_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'weibo/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_weibo->weibo_hist_data = implode('|',$hashed);
                 $account_weibo->weibo_hist_data_fname = implode('|',$original);
                 $account_weibo->update();
             }

             $changes++;
        }
        elseif (!empty($account->weibo_id) && $input['weibo_included'] == 'no')
        {
            // Delete the account weibo
            $weibo_id = AccountWeibo::find($account->weibo_id);
            $weibo_id->delete();

            // Empty the account weibo_id & update the account nature dropdown
            $account->weibo_id = 0;
            $account->nature = implode('|',$input['nature']);

            // Track changes
            $changes++;
        }

        if (!empty($account->wechat_id) && $input['wechat_included'] == 'yes')
        {
            foreach($wechat as $field => $value)
            {
                $service_wechat = AccountWeChat::find($account->wechat_id);

                if($service_wechat->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_wechat->$field;
                    $newval[$field] = $value;
                }
                $service_wechat->$field = $value;
                $service_wechat->update();
            }

            if(isset($input['wechat_hist_data']))
            {
                foreach($input['wechat_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'wechat/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $service_wechat->wechat_hist_data = !empty($service_wechat->wechat_hist_data) ? $service_wechat->wechat_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $service_wechat->wechat_hist_data_fname = !empty($service_wechat->wechat_hist_data_fname) ? $service_wechat->wechat_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $service_wechat->update();

                $changes++;
                $oldval['wechat_hist_data'] = explode('|',$service_wechat->wechat_hist_data_fname);
                $newval['wechat_hist_data'] = explode('|',$service_wechat->wechat_hist_data_fname.'|'.implode('|',$original));
            }

            $changes++;
        }
        elseif (empty($account->wechat_id) && $input['wechat_included'] == 'yes')
        {
             AccountWeChat::create($wechat);
             $wechat_id = AccountWeChat::max('id');
             $account->wechat_id = $wechat_id;
             
             if(isset($input['wechat_hist_data']))
             {
                 $account_wechat = AccountWeChat::find($wechat_id);
                 foreach($input['wechat_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'wechat/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_wechat->wechat_hist_data = implode('|',$hashed);
                 $account_wechat->wechat_hist_data_fname = implode('|',$original);
                 $account_wechat->update();
             }
        }
        elseif (!empty($account->wechat_id) && $input['wechat_included'] == 'no')
        {
            // Delete the account wechat
            $wechat_id = AccountWeChat::find($account->wechat_id);
            $wechat_id->delete();

            // Empty the account wechat_id & update the account nature dropdown
            $account->wechat_id = 0;
            $account->nature = implode('|',$input['nature']);

            // Track changes
            $changes++;    
        }

        if (!empty($account->postpaid_sem_id) && $input['postpaid_included'] == 'yes')
        {
            foreach($postpaid as $field => $value)
            {
                $service_postpaid = AccountPostpaidSEM::find($account->postpaid_sem_id);

                if($service_postpaid->$field != $value)
                {
                    $changes++;
                    $oldval[$field] = $service_postpaid->$field;
                    $newval[$field] = $value;
                }
                $service_postpaid->$field = $value;
                $service_postpaid->update();
            }

            $changes++;
        }
        elseif (empty($account->postpaid_sem_id) && $input['postpaid_included'] == 'yes')
        {
            AccountPostpaidSEM::create($postpaid);
            $postpaid_sem_id = AccountPostpaidSEM::max('id');
            $account->postpaid_sem_id = $postpaid_sem_id;

            $changes++;
        }
        elseif (!empty($account->postpaid_sem_id) && $input['postpaid_included'] == 'no')
        {
            // Delete the account postpaid sem
            $postpaid_sem_id = AccountPostpaidSEM::find($account->postpaid_sem_id);
            $postpaid_sem_id->delete();

            // Empty the account postpaid_sem_id & update the account nature dropdown
            $account->postpaid_sem_id = 0;
            $account->nature = implode('|',$input['nature']);

            // Track changes
            $changes++;    
        }
        

        if (!empty($account->ppc_id) && $input['sem_included'] == 'yes')
        {
            foreach($sem as $field => $value)
            {
                $account_ppc = AccountPpc::find($account->ppc_id);
                $account_ppc->$field = $value; // chinese
                $account_ppc->update();
            }

            if(isset($input['historical_data']))
            {
                foreach($input['historical_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'sem/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $account_ppc->historical_data = !empty($account_ppc->historical_data) ? $account_ppc->historical_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $account_ppc->historical_data_fname = !empty($account_ppc->historical_data_fname) ? $account_ppc->historical_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $account_ppc->update();

                $changes++;
                $oldval['sem_historical_data'] = explode('|',$account_ppc->historical_data_fname);
                $newval['sem_historical_data'] = explode('|',$account_ppc->historical_data_fname.'|'.implode('|',$original));
            }
        }
        elseif (empty($account->ppc_id) && $input['sem_included'] == 'yes')
        {
             AccountPpc::create($sem);
             $ppc_id = AccountPpc::max('id');
             $account->ppc_id = $ppc_id;
             
             if(isset($input['historical_data']))
             {
                 $account_ppc = AccountPpc::find($ppc_id);
                 foreach($input['historical_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'sem/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_ppc->ppc_hist_data = implode('|',$hashed);
                 $account_ppc->ppc_hist_data_fname = implode('|',$original);
                 $account_ppc->update();
             }

             $changes++;
        }
        elseif (!empty($account->ppc_id) && $input['sem_included'] == 'no')
        {
           // Delete the account ppc
           $ppc_id = AccountPpc::find($account->ppc_id);
           $ppc_id->delete();

           // Empty the account ppc_id & update the account nature dropdown
           $account->ppc_id = 0;
           $account->nature = implode('|',$input['nature']);

           // Track changes
           $changes++;
        }


        if (!empty($account->fb_id) && $input['fb_included'] == 'yes')
        {
            foreach($fb as $field => $value)
            {
                $account_fb = AccountFb::find($account->fb_id);
                $account_fb->$field = $value;
                $account_fb->update();
            }

            if(isset($input['fb_hist_data']))
            {
                $account_fb = AccountFb::find($account->fb_id);
                foreach($input['fb_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'fb/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $changes++;
                $account_fb->fb_hist_data = !empty($account_fb->fb_hist_data) ? $account_fb->fb_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $account_fb->fb_hist_data_fname = !empty($account_fb->fb_hist_data_fname) ? $account_fb->fb_hist_data_fname.'|'.implode('|',$original) : implode('|',$original) ;
                $account_fb->update();
            }
        }
        elseif (empty($account->fb_id) && $input['fb_included'] == 'yes')
        {
            AccountFb::create($fb);
            $fb_id = AccountFb::max('id');
            $account->fb_id = $fb_id;
            

            if(isset($input['fb_hist_data']))
            {
                $account_fb = AccountFb::find($fb_id);
                foreach($input['fb_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'fb/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $changes++;
                $account_fb->fb_hist_data = implode('|',$hashed);
                $account_fb->fb_hist_data_fname = implode('|',$original);
                $account_fb->update();
            }

            $changes++;
        }
        elseif (!empty($account->fb_id) && $input['fb_included'] == 'no')
        {
           // Delete the account fb
           $fb_id = AccountFb::find($account->fb_id);
           $fb_id->delete();

           // Empty the account fb_id & update the account nature dropdown
           $account->fb_id = 0;
           $account->nature = implode('|',$input['nature']);

           // Track changes
           $changes++;
            
        }


        if (!empty($account->seo_id) && $input['seo_included'] == 'yes')
        {

            foreach($seo as $field => $value)
            {
                $account_seo = AccountSeo::find($account->seo_id);
                $account_seo->$field = $value;
                $account_seo->update();
            }

            if(isset($input['seo_hist_data']))
            {
                $account_seo = AccountSeo::find($account->seo_id);
                foreach($input['seo_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'seo/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }
                
                $changes++;
                $account_seo->seo_hist_data = !empty($account_seo->seo_hist_data) ? $account_seo->seo_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed);
                $account_seo->seo_hist_data_fname = !empty($account_seo->seo_hist_data_fname) ? $account_seo->seo_hist_data_fname.'|'.implode('|',$original) : implode('|',$original);
                $account_seo->update();
            }
        }
        elseif (empty($account->seo_id) && $input['seo_included'] == 'yes')
        {
            AccountSeo::create($seo);
            $seo_id = AccountSeo::max('id');
            $account->seo_id = $seo_id;
            

            if(isset($input['seo_hist_data']))
            {
                $account_seo = AccountSeo::find($seo_id);
                foreach($input['seo_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'seo/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $changes++;
                $account_seo->seo_hist_data = implode('|',$hashed);
                $account_seo->seo_hist_data_fname = implode('|',$original);
                $account_seo->update();
            }
            
            $changes++;
        }
        elseif (!empty($account->seo_id) && $input['seo_included'] == 'no')
        {
             // Delete the account seo
             $seo_id = AccountSeo::find($account->seo_id);
             $seo_id->delete();

             // Empty the account seo_id & update the account nature dropdown
             $account->seo_id = 0;
             $account->nature = implode('|',$input['nature']);

             // Track changes
             $changes++;
        }
        
        if (!empty($account->web_id) && $input['web_included'] == 'yes')
        {
            foreach($web as $field => $value)
            {
                $account_web = AccountWeb::find($account->web_id);
                $account_web->$field = $value;
                $account_web->update();
            }

            if(isset($input['web_hist_data']))
            {
                $account_web = AccountWeb::find($account->web_id);
                foreach($input['web_hist_data'] as $file)
                {
                    $origfname = $file->getClientOriginalName();
                    $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path() . DIRECTORY_SEPARATOR . 'web/historicaldata', $fname);
                    $hashed[] = $fname;
                    $original[] = $origfname;
                }

                $changes++;
                $account_web->web_hist_data = !empty($account_web->web_hist_data) ? $account_web->web_hist_data.'|'.implode('|',$hashed) : implode('|',$hashed) ;
                $account_web->web_hist_data_fname = !empty($account_web->web_hist_data_fname) ? $account_web->web_hist_data_fname.'|'.implode('|',$original) : implode('|',$original) ;
                $account_web->update();
            }
        }
        elseif (empty($account->web_id) && $input['web_included'] == 'yes')
        {
             AccountWeb::create($web);
             $web_id = AccountWeb::max('id');
             $account->web_id = $web_id;
             
             if(isset($input['web_hist_data']))
             {
                 $account_web = AccountWeb::find($web_id);
                 foreach($input['web_hist_data'] as $file)
                 {
                     $origfname = $file->getClientOriginalName();
                     $fname = uniqid() . md5($origfname . str_random(6)) . '.' . $file->getClientOriginalExtension();
                     $file->move(storage_path() . DIRECTORY_SEPARATOR . 'web/historicaldata', $fname);
                     $hashed[] = $fname;
                     $original[] = $origfname;
                 }
 
                 $changes++;
                 $account_web->web_hist_data = implode('|',$hashed);
                 $account_web->web_hist_data_fname = implode('|',$original);
                 $account_web->update();
             }

             $changes++;
             
        }
        elseif (!empty($account->web_id) && $input['web_included'] == 'no')
        {
            // Delete the account web
             $web_id = AccountWeb::find($account->web_id);
             $web_id->delete();

             // Empty the account web_id & update the account nature dropdown
             $account->web_id = 0;
             $account->nature = implode('|',$input['nature']);

             // Track changes
             $changes++;
        }

        if($user->role == 1 || ($user->UserInfo->team->team_code == 'acc' && in_array($user->UserInfo->emp_id, $managers)) )
        {
            
            if($input['status'] != $account->status)
                $changes++;
            
            $account->status = $input['status'];
        }

         /*---------------- Contract number ------------------*/
         $numberOfZeros = 3; // Number of leading zeros on contract number
         $contracts = Contract::max('contract_number'); // Select the max value of contract number on contract model
         $contract_explode = explode('-', $contracts); // Explode the max value of contract_number by using '-'

         // If the contract number year and month is the same as today's year and month
         if($contract_explode[0] == date('ym')) 
             $counter = $contract_explode[1] + 1; // Increment the max value by 1
         else
             $counter = 1; // Back to the count of 1

         $contract_number = date('ym') . "-" . str_pad($counter, $numberOfZeros, '0', STR_PAD_LEFT); // Setting of all data to generate contract number

         $account_contract = Contract::where('prospect_id', '=', $account->prospect_id)->where('is_active',1)->whereNull('delete_flag')->count();
         $first_contract = Contract::where('prospect_id', '=', $account->prospect_id)->where('is_active', 1)->whereNull('delete_flag')->orderBy('id', 'asc')->first();

         if($account_contract == 0)
         {
             $day90OnFirstContract = Carbon::parse($account->created_at)->addDays(90);
             $day120OnFirstContract = Carbon::parse($account->created_at)->addDays(120);
         } 
         else 
         {
             $day90OnFirstContract = $first_contract->day_90;
             $day120OnFirstContract = $first_contract->day_120;
         }

        if ($user->role == 1 || $account->acc_assigned == $user->emp_id || ($user->userInfo->team->team_code == 'busi' || $user->userInfo->team->team_code == 'acc'))
        {
            if(empty($account->contract))
            {
                Contract::create([
                    'contract_number'   => $contract_number,
                    'contract_value'    => !isset($contract['contract_value']) ? null : $contract['contract_value'],
                    'prospect_id'       => $account->prospect_id,
                    'account_id'        => $account->id,
                    'company'           => $contract['company'],
                    'f_name'            => trim(strtok($contract['client_name'],' ')),
                    'l_name'            => trim(strstr($contract['client_name'],' ')),
                    'contact_number'    => $contract['client_number'],
                    'email'             => $contract['email'],
                    'created_by'        => Auth::user()->userid,
                    'day_90'            => $day90OnFirstContract,
                    'day_120'           => $day120OnFirstContract
                ]);

                $account->contract_id = Contract::max('id');
            
            }
            else
            {
    
                if ($user->role == 1 || in_array($user->userInfo->team->team_code, ['hr']))
                    $account->contract->contract_number = $input['contract_number'];
                    
                $account->contract->company = $contract['company'];
                $account->contract->contract_value = $contract['contract_value'];
                $account->contract->contract_type = $contract['contract_type'];
                $account->contract->f_name = trim(strtok($contract['client_name'],' '));
                $account->contract->l_name = trim(strstr($contract['client_name'],' '));
                $account->contract->contact_number = $contract['client_number'];
                $account->contract->email = $contract['email'];
                $account->prospect->lead->website = $prospect['website'];
                $account->prospect->lead->street_address = $prospect['street_address'];
                $account->prospect->lead->industry = $prospect['industry'];
                $account->contract->save();
                $account->prospect->lead->save();
            }
            $changes++;
        } 
        
            
        if ($changes == 0)
            return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'info',
                    'message' => 'No changes made',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        $account->updated_at = Carbon::now();
        $account->updated_by = Auth::user()->userid;
        $account->save();
        
        $oldvalue = isset($oldval) ? json_encode($oldval) : null;
        $newvalue = isset($oldval) ? json_encode($newval) : null;
        
        $receivers = $this->receivers($account);
        
        foreach($receivers as $row)
        {
            $row->notify(new AccountUpdatedNotification($account, $account->prospect, $account->prospect->lead, Auth::user()->UserInfo));
        }
        
        app(__('global.Audit'))->logAuditTrail("MD003","AT005","Updated Account ".$account->prospect->lead->company,$oldvalue,$newvalue,request()->ip(),$user->userid,$user->role);
        
        return 
            redirect()
            ->back()
            ->with([
                'alert'   => 'success',
                'message' => 'Account '.$account->prospect->lead->f_name.' '.$account->prospect->lead->l_name.' has been updated',
                'button'  => 'OK',
                'confirm' => 'true'
            ]);
    }

    function chineseToUnicode($str){
		//split word
		preg_match_all('/./u',$str,$matches);
		
		$c = "";
		foreach($matches[0] as $m){
			$c .= "&#".base_convert(bin2hex(iconv('UTF-8',"UCS-4",$m)),16,10);
		}
		return $c;
	}

    public function delete(Request $request)
    {
        $input = $request->except('_token');

        $account = Account::where('id',$input['accountid'])->first();

        if (empty($account))
        {
            app(__('global.Audit'))->logAuditTrail("MD003","AE002","Account Delete Error",null,null,request()->ip(),$user->userid,$user->role);

            return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'error',
                    'message' => 'Account does not exist or already deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
        }

        $del_account = $account->prospect->lead->company;
        $account->delete_flag = 1;
        $account->deleted_at = date('Y-m-d H:i:s');
        $account->active_flag = 0;
        $account->update();

        $receivers = $this->receivers($account);
        
        foreach($receivers as $row)
        {
            $row->notify(new AccountDeletedNotification($account,$account->prospect,$account->prospect->lead,Auth::user()->UserInfo));
        }

        app(__('global.Audit'))->logAuditTrail("MD003","AT006","Deleted Account ".$del_account,null,null,request()->ip(),Auth::user()->userid,Auth::user()->role);

        return 
                redirect()
                ->back()
                ->with([
                    'alert'   => 'success',
                    'message' => 'Account '.$del_account.' has been deleted',
                    'button'  => 'OK',
                    'confirm' => 'true'
                ]);
    }

    // public function deleteHistoricalData($accountid, $package, $doc)
    // {
    //     $user = Auth::user();
    //     $account = Account::find($accountid);

    //     $data = $package == 'sem' ? 'historical_data' : $package.'_hist_data';
    //     $data_fname = $package == 'sem' ? 'historical_data_fname' : $package.'_hist_data_fname';
    //     $hashed = explode('|',$account->$package->$data);
    //     $original = explode('|',$account->$package->$data_fname);
        
    //     foreach($hashed as $n => $row)
    //     {
    //         if($row == $doc)
    //         {
    //             $del_file = $original[$n];
    //             \File::delete(storage_path($package.'/historicaldata/'.$doc));
    //             unset($hashed[$n]);
    //             unset($original[$n]);
    //         }
    //     }
        
    //     $account->$package->$data = !empty($hashed) ? implode('|',$hashed) : null;
    //     $account->$package->$data_fname = implode('|',$original);
    //     $account->$package->update();

    //     app(__('global.Audit'))->logAuditTrail("MD003","AT006","Removed historical data of Account ".$account->prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);
        
    //     return 
    //             redirect()
    //             ->back()
    //             ->with([
    //                 'alert'   => 'success',
    //                 'message' => $del_file.' has been deleted',
    //                 'button'  => 'OK',
    //                 'confirm' => 'true'
    //             ]);
    // }

    public function assign(Request $request, $id)
    {
        $input = $request->except('_token');

        if (\Session::has('redirectURL'))
            \Session::forget('redirectURL');

        if (!Auth::user())
        {
            \Session::put('redirectURL','account/edit/assign/'.$id);
        }

        $user = Auth::user();

        $teamcode = $user->userInfo->team->team_code;

        $account = Account::find($id);

        $managers_list = Manager::select('emp_id')->get()->toArray();
        $managers = array_flatten($managers_list);
        $team = Team::where('team_code',$teamcode)->first();

        $strategists    = Employee::strategists();
        $link_builders  = Employee::linkBuilders();

        $sem_team       = Employee::semTeam();
        $seo_team       = Employee::seoTeam();
        $dev_team       = Employee::devTeam();
        $acc_team       = Employee::accTeam();
        $content_team   = Employee::contentTeam();

        $packages = explode('|',$account->nature);
        foreach($packages as $row)
        {
            $package = System::where('systemcode',$row)->first();
            $nature[] = $package->systemdesc;
        }

        // $team_name = $teamcode.'_team';

        app(__('global.Audit'))->logAuditTrail("MD003","AT007","Assign member in account ".$account->prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

        if (!empty($account->sem->sem_team) || !empty($account->web->dev_team) || !empty($account->busi_team) || !empty($account->acc_assigned))
            return view('account.assign')->with(compact('managers','account', 'nature', 'teamcode', 'user','strategists', 'link_builders','sem_team','seo_team','dev_team','acc_team','content_team'));
        
        return view('account.assign')->with(compact('managers','account', 'nature', 'teamcode', 'user','strategists', 'link_builders', 'sem_team','seo_team','dev_team','acc_team','content_team'));
    }

    public function assignMembers($accountID, $package, $packageID = null, $members = null, $other_members = null, $asst_members = null, $asst_other_members = null, $others = null)
    {
        $account = Account::find($accountID);
        $servicesArray = explode('|',$account->nature);
        $np = array();
        foreach($servicesArray as $k => $v)
        {
            $systemService= System::where('systemcode',$v)->first();
            $np[] = $systemService->systemdesc;
        }                 
        $services = implode(', ', $np);
        
        // $selected = explode('|', $members);

        $selected = explode('|', $members);
        $selected_other = isset($other_members) ? explode('|', $other_members) : '';
        $selected2 = isset($asst_members) ? explode('|', $asst_members) : '';
        $selected2_other = isset($asst_other_members) ? explode('|', $asst_other_members) : '';
        $add_others = isset($others) ? explode('|', $others) : '';
    
        switch($package) {
            case 'sem':
                $accountPackage = AccountPpc::find($packageID);
                $accountPackage->sem_strategist_assigned = $members;
                $accountPackage->asst_sem_strategist_assigned = $other_members;
                $accountPackage->update();
                $main = !empty($accountPackage->sem_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->sem_strategist_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->sem_strategist_assigned)->first()->l_name : '';
                $asst = !empty($accountPackage->asst_sem_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->asst_sem_strategist_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->asst_sem_strategist_assigned)->first()->l_name : '';
                break;
            case 'fb':
                $accountPackage = AccountFb::find($packageID);
                $accountPackage->fb_strategist_assigned = $members;
                $accountPackage->asst_fb_strategist_assigned = $other_members;
                $accountPackage->update();
                $main = !empty($accountPackage->fb_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->fb_strategist_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->fb_strategist_assigned)->first()->l_name : '';
                $asst = !empty($accountPackage->asst_fb_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->asst_fb_strategist_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->asst_fb_strategist_assigned)->first()->l_name : '';
                break;
            case 'seo':
                $accountPackage = AccountSeo::find($packageID);
                $accountPackage->seo_strategist_assigned = $members;
                $accountPackage->asst_seo_strategist_assigned = $other_members;
                $accountPackage->seo_link_builder_assigned = $asst_members;
                $accountPackage->asst_seo_link_builder_assigned = $asst_other_members;
                $accountPackage->seo_content_team = $others;
                $accountPackage->update();
                $main = !empty($accountPackage->seo_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->seo_strategist_assigned)->first()->f_name. ' ' . Employee::where('emp_id', '=', $accountPackage->seo_strategist_assigned)->first()->l_name : '';
                $asst = !empty($accountPackage->asst_seo_strategist_assigned) ? Employee::where('emp_id', '=', $accountPackage->asst_seo_strategist_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->asst_seo_strategist_assigned)->first()->l_name : '';
                $main_link = !empty($accountPackage->seo_link_builder_assigned) ? Employee::where('emp_id', '=', $accountPackage->seo_link_builder_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->seo_link_builder_assigned)->first()->l_name : '';
                $asst_link = !empty($accountPackage->asst_seo_link_builder_assigned) ? Employee::where('emp_id', '=', $accountPackage->asst_seo_link_builder_assigned)->first()->f_name . ' ' . Employee::where('emp_id', '=', $accountPackage->asst_seo_link_builder_assigned)->first()->l_name : '';

                $array = explode('|', $accountPackage->seo_content_team);
                foreach($array as $row)
                {
                    if($row == '0')
                        continue;

                    $names_array[] = Employee::where('emp_id', $row)->first()->f_name . ' ' . Employee::where('emp_id', $row)->first()->l_name;
                }
                $other = !empty($names_array) ? implode(', ', $names_array) : '';
                break;
            case 'web':
                $accountPackage = AccountWeb::find($packageID);
                $accountPackage->dev_team = $members;
                $accountPackage->dev_content_team = $other_members;
                $accountPackage->update();
               
                $array = explode('|', $accountPackage->dev_team);
                foreach($array as $row)
                {
                    if($row == '0')
                        continue;

                    $names_array[] = Employee::where('emp_id', $row)->first()->f_name . ' ' . Employee::where('emp_id', $row)->first()->l_name;
                }
                
                $array = explode('|', $accountPackage->dev_content_team);
                foreach($array as $row)
                {
                    if($row == '0')
                        continue;

                    $names_array2[] = Employee::where('emp_id', $row)->first()->f_name . ' ' . Employee::where('emp_id', $row)->first()->l_name;
                }

                $main = !empty($names_array) ? implode(', ', $names_array) : '';
                $asst = !empty($names_array2) ?implode(', ', $names_array2) : '';
                break;
            case 'blog' :
                $accountPackage = AccountBlogContent::find($packageID);
                $accountPackage->blog_content_team = $members;
                $accountPackage->update();

                $array = explode('|', $accountPackage->blog_content_team);
                foreach($array as $row)
                {
                    $names_array[] = Employee::where('emp_id', $row)->first()->f_name . ' ' . Employee::where('emp_id', $row)->first()->l_name;
                }

                $main = !empty($names_array) ? implode(', ', $names_array) : '';
                break;
            case 'social' :
                $accountPackage = AccountSocialManagement::find($packageID);
                $accountPackage->social_content_team = $members;
                $accountPackage->update();

                $array = explode('|', $accountPackage->social_content_team);
                foreach($array as $row)
                {
                    $names_array[] = Employee::where('emp_id', $row)->first()->f_name . ' ' . Employee::where('emp_id', $row)->first()->l_name;
                }
                $main = !empty($names_array) ? implode(', ', $names_array) : '';

                break;
            case 'acc':
                $accountPackage = Account::find($accountID);
                $accountPackage->acc_assigned = $members;

                // if(isset($accountPackage->acc_assigned))
                // {
                //     $contract = Contract::where('is_active', 1)
                //                         ->where('prospect_id', $accountPackage->prospect_id)
                //                         ->update(['day_90' => Carbon::now()->addDays(90), 'day_120' => Carbon::now()->addDays(120)]);
                // }

                $accountPackage->acc_assistant = $other_members;
                $accountPackage->update();

                $main = !empty($accountPackage->acc_assigned) ? Employee::where('emp_id', $accountPackage->acc_assigned)->first()->f_name . ' ' . Employee::where('emp_id', $accountPackage->acc_assigned)->first()->l_name : '';
                $asst = !empty($accountPackage->acc_assistant) ? Employee::where('emp_id', $accountPackage->acc_assistant)->first()->f_name . ' ' . Employee::where('emp_id', $accountPackage->acc_assistant)->first()->l_name : '';
                break;
        }

        foreach($selected as $v)
        {
            $assignedMembers[] = $v;
        }

        if(isset($selected_other))
        {
            foreach($selected_other as $v)
            {
                $assignedMembers[] = $v;
            }
        }
      
        if(isset($selected2))
        {
            foreach($selected2 as $v)
            {
                $assignedMembers[] = $v;
            }
        }

        if(isset($selected2_other))
        {
            foreach($selected2_other as $v)
            {
                $assignedMembers[] = $v;
            }
        }

        if(isset($add_others))
        {
            foreach($add_others as $v)
            {
                $assignedMembers[] = $v;
            }
        }


        foreach(array_flatten($assignedMembers) as $k => $v)
        {
            if (empty($v))
                continue;

            $user = Employee::where('emp_id', $v)->first();
            
            $emailDetails = [ 'prospectname' =>  ucfirst($account->prospect->lead->company),
                              'name'     =>  $user->f_name . ' ' . $user->l_name,
                              'email'    =>  $user->userInfo->email,
                              'nature'   =>  strtoupper($package),
                              'accountid'=>  $accountID,
                              'account'  =>  $account,
                              'services' =>  $services
                            ];
            
            $this->emailAssign($emailDetails);   
        }

       foreach($assignedMembers as $row)
       {
           if($row == '0')
                continue;

           $emp = Employee::where('emp_id', '=', $row)->first();
           $allMems[] = $emp->f_name . ' ' . $emp->l_name;
       }

       $collectMembers = implode(', ', $allMems);
       $sales = User::where('userid', '=', $account->created_id)->first();
       $nature = strtoupper($package);
       $assignee = Auth::user()->userInfo->f_name . ' ' . Auth::user()->userInfo->l_name;

       $emailDetails2 = [
            'sales'           => $sales->userInfo->f_name,
            'nature'          => $nature,
            'assignee'        => $assignee,
            'services'        => $services,
            'package'         => $package,
            'company'         => $account->contract->company,
            'accountid'       => $account->id,
            'contract_number' => $account->contract->contract_number,
            'sales_email'     => $account->convert->email,
            'main'            => !empty($main) ? $main : '',
            'asst'            => !empty($asst) ? $asst : '',
            'main_link'       => !empty($main_link) ? $main_link : '',
            'asst_link'       => !empty($asst_link) ? $asst_link : '',
            'other'           => !empty($other) ? $other : ''
       ];
        
        \Mail::send('layouts.emails.accountnotifysales', $emailDetails2, function($message) use ($emailDetails2) {
            $message->to($emailDetails2['sales_email'])->subject
               ('CRM System - ' . $emailDetails2['assignee'] .' has assigned member(s) in your account ' . $emailDetails2['company'] . ' on ' . $emailDetails2['nature'] . ' service');
            $message->from('noreply@oom.com.sg','CRM Admin');
        });

        $sales->notify(new NotifySalesPersonWhenAssignedNotification($sales->userInfo, $account, $account->contract, $collectMembers, $nature));

        $receivers = $this->receivers($account);

        foreach($receivers as $row)
        {
            $row->notify(new AccountAssignedPerPackageNotification($account,$account->prospect,$account->prospect->lead,Auth::user()->UserInfo, strtoupper($package)));
        }

        if (!empty($user))
            app(__('global.Audit'))->logAuditTrail("MD003","AT005","Assigned member in account package ". $account->prospect->lead->company,null, null,request()->ip(),Auth::user()->userid,Auth::user()->role);
        else
            app(__('global.Audit'))->logAuditTrail("MD003","AT005","Removed member in account package ". $account->prospect->lead->company,null, null,request()->ip(),Auth::user()->userid, Auth::user()->role);

        $team = Employee::whereIn('emp_id', $selected)->get();

        if(isset($other_members))
            $other_selected = Employee::whereIn('emp_id', $selected_other)->get();
        else
            $other_selected = '';

        return response()->json(['selected' => $selected, 'members' => $team, 'other_members' => $other_selected, 'all_members' => $collectMembers], 200);
    }
    
    public function viewAccount($id, $nature = null)
    {

        if (\Session::has('redirectURL'))
            \Session::forget('redirectURL');

        if (!Auth::user())
        {
            \Session::put('redirectURL','account/view/'.$id);
            return redirect()->route('user.login');
        }

        $user = Auth::user();
        
        // Accounts: previous and next
        $employee = Employee::where('emp_id', $user->emp_id)->first();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';

        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());
      
        $account = Account::findOrFail($id);

        if($user->role == 1 || ($teamcode == 'acc' && in_array($user->userInfo->emp_id, $managers)) )
        {
            $previous = Account::whereNull('deleted_at')
                        ->where('active_flag', 1)
                        ->where('id', '<', $account->id)
                        ->orderBy('updated_at','desc')
                        ->first();

            $next =  Account::whereNull('deleted_at')
                        ->where('active_flag', 1)
                        ->where('id', '>', $account->id)
                        ->orderBy('updated_at','asc')
                        ->first();
        }
        elseif(in_array($user->emp_id, $managers) && $user->role != 1)
        {
            $previous = $teamcode != 'dev' ? Account::whereNull('deleted_at')
                                                    ->where('active_flag', 1)
                                                    ->where('id', '<', $account->id)
                                                    ->where('nature','like',['%001%','%002%','%003%'])
                                                    ->orderBy('updated_at','desc')
                                                    ->first()
                                            : Account::whereNull('deleted_at')
                                                    ->where('active_flag', 1)
                                                    ->where('id', '<', $account->id)
                                                    ->where('nature','like','%NP004%')
                                                    ->orderBy('updated_at','desc')
                                                    ->first();
                                            

            $next =  $teamcode != 'dev' ? Account::whereNull('deleted_at')
                                                    ->where('active_flag', 1)
                                                    ->where('id', '>', $account->id)
                                                    ->where('nature','like',['%001%','%002%','%003%'])
                                                    ->orderBy('updated_at','asc')
                                                    ->first()
                                        : Account::whereNull('deleted_at')
                                                    ->where('active_flag', 1)
                                                    ->where('id', '>', $account->id)
                                                    ->where('nature','like','%NP004%')
                                                    ->orderBy('updated_at','asc')
                                                    ->first();                         
        }
        else 
        {
            $previous = Account::whereNull('deleted_at')
                        ->where('active_flag', 1)
                        ->where('id', '<', $account->id)
                        ->orderBy('updated_at','desc')
                        ->first();

            $next =  Account::whereNull('deleted_at')
                        ->where('active_flag', 1)
                        ->where('id', '>', $account->id)
                        ->orderBy('updated_at','asc')
                        ->first();
        }

        
        // if($user->role == 4 || ($user->role == 2 && !in_array($user->userInfo->emp_id, $managers)) )
        // {
        //     if($teamcode == 'sp')
        //         if($user->userInfo->isSEM)
        //         {
        //             $sp_previous = Account::viewPrevAccounts('sem', $account->id);
        //             $sp_next = Account::viewNextAccounts('sem', $account->id); 
        //         }
        
        // }
        
        // $previous = $user->role == 4 || ($user->role == 2 && in_array($user->userInfo->emp_id, $managers)) ? $sp_previous : $previous;
        // $next = $user->role == 4 || ($user->role == 2 && in_array($user->userInfo->emp_id, $managers)) ? $sp_next : $next;
        
        // dd($previous->id, $next);

        $team = $user->userInfo->team->team_code;

        if ($user->role == 4 && !in_array($team, ['busi','acc']))
        {
            if($team == 'dev' && !empty($account->web))
                $hasAccess = !in_array($user->userInfo->emp_id, explode('|',$account->web->dev_team)) ? false : true;
            else
                if ($user->UserInfo->isSEO && !empty($account->seo))
                    $hasAccess = !in_array($user->userInfo->emp_id, explode('|',$account->seo->seo_team)) ? false : true;
                else if($user->UserInfo->isSEM && !empty($account->sem))
                    $hasAccess = !in_array($user->userInfo->emp_id, explode('|',$account->sem->sem_team)) ? false : true;
                else
                    $hasAccess = '';
        }
        else
        {
            $hasAccess = true;
        }

        // if (!$hasAccess 
        //     && (!in_array($user->userInfo->emp_id,$managers)) 
        //     && $account->created_id != auth()->user()->userid 
        //     && $user->role != 1)
        // {
        //     app(__('global.Audit'))->logAuditTrail("MD003","AE999","Invalid access of account ".$account->prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);
            
        //     return 
        //             redirect()
        //             ->route('account.list')
        //             ->with([
        //                 'alert'   => 'error',
        //                 'message' => 'You do not have access in that account',
        //                 'button'  => 'OK',
        //                 'confirm' => 'true'
        //             ]);
        // }
        //$nature = System::where('systemtype','nature')->get();
        $notes = LeadNote::where('lead_id',$account->prospect->lead_id)->get();
        $teamcode = $user->userInfo->team->team_code;
        $team = Team::where('team_code',$teamcode)->first();
        
        $packages = explode('|',$account->nature);
        foreach($packages as $row)
        {
            $package = System::where('systemcode',$row)->first();
            $nature[] = $package->systemdesc;
        }

        //$team_name = $teamcode.'_team';
        
        $sp_team = explode('|',$account->sp_team);
        if(!empty(array_filter($sp_team)))
            foreach($sp_team as $row)
            {
                $emp = Employee::where('emp_id',$row)->first();
                $sp_members[] = $emp->f_name.' '.$emp->l_name;
            }
        
        if(!empty($account->sem->sem_team))
        {
            $sem_team = explode('|',$account->sem->sem_team);
            if(!empty(array_filter($sem_team)))
                foreach($sem_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $sem_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
        
        if(!empty($account->fb->sem_team))
        {
            $fb_team = explode('|',$account->fb->sem_team);
            if(!empty(array_filter($fb_team)))
                foreach($fb_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $fb_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
        
        if(!empty($account->seo->seo_team))
        {
            $seo_team = explode('|',$account->seo->seo_team);
            if(!empty(array_filter($seo_team)))
                foreach($seo_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $seo_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
        
        if(!empty($account->web->dev_team))
        {
            $dev_team = explode('|',$account->web->dev_team);
            if(!empty(array_filter($dev_team)))
                foreach($dev_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $dev_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
       
        if(!empty($account->busi_team))
        {
            $busi_team = explode('|',$account->busi_team);
            if(!empty(array_filter($busi_team)))
                foreach($busi_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $busi_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
       
        if (!empty($account->acc_assigned))
        {
            $acc_team = explode('|',$account->acc_assigned);
            if(!empty(array_filter($acc_team)))
                foreach($acc_team as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $acc_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }

        if (!empty($account->acc_assistant))
        {
            $acc_assistant = explode('|',$account->acc_assistant);
            if(!empty(array_filter($acc_assistant)))
                foreach($acc_assistant as $row)
                {
                    $emp = Employee::where('emp_id',$row)->first();
                    $acc_asst_members[] = $emp->f_name.' '.$emp->l_name;
                }
        }
        
        app(__('global.Audit'))->logAuditTrail("MD003","AT001","Viewed account ".$account->prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

        return view('account.view')->with(compact('managers','nature','account','teamcode','team','sp_members','sem_members','fb_members','seo_members','dev_members','busi_members','acc_members','user','notes','acc_asst_members','previous','next'));
    }

    public function search($account)
    {
        $pagination = PaginationPerPage::where('table_name','accounts')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($pagination->per_page) ? $pagination->per_page : '10';
        $column = ModifyAccountsColumn::where('userid', '=', Auth::user()->userid)->first();
        $filter = $this->getFilterSetting();
        $access = Auth::user()->userAccess->toArray();
        $role = Auth::user()->role;
        if ($account == 'null')
        {
            $accounts = Account::whereNull('delete_flag')
                                ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                    $query->where('created_id', $filter->person_filter);
                                })
                                ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                    $query->where('acc_assigned', $filter->person_filter);
                                })
                                ->when($filter->type_filter == 'contract_number', function($query) use ($filter) {
                                    $query->where('contract_id', $filter->person_filter);
                                })
                                ->where('active_flag',1)
                                ->paginate($perPage);

            $result = $this->tblAccounts($accounts);
            return response()->json(['result' => $result,'pagination' => $accounts],200);
        }

        $byCompany = Account::findCompany($account);
        $byContractNumber  = Account::findContractNumber($account);
        // $byStatus  = Account::FindStatus($account);

        if (!empty($byCompany->toArray()['data']))
            $accounts = $byCompany;
        else if (!empty($byContractNumber->toArray()['data']))
            $accounts = $byContractNumber;
        // else if(!empty($byStatus->toArray()['data']))
        //     $accounts = $byStatus;
        else
            $accounts = "";
        
        if (!empty($accounts))
        {
            foreach($accounts as $key => $row)
            {

                if(!empty($row->nature))
                {
                    $nature = explode('|',$row->nature);
                    $np = array();
                    foreach($nature as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
                                        
                    $nature = implode(', ', $np);
                }

                $holder = empty($row->acc_assigned) ? 'None' : $row->holder->UserInfo->f_name.' '.$row->holder->UserInfo->l_name; 
                $asst_holder = empty($row->acc_assistant) ? 'None' : $row->asst_holder->UserInfo->f_name.' '.$row->asst_holder->UserInfo->l_name; 

                $mp = array();
                $cp_start = array();
                $cp_end = array();
                $budget = array();

                if(!empty($row->sem))
                { 
                    if(!empty($row->sem->sem_strategist_assigned) || !empty($row->sem->asst_sem_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->sem->sem_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->sem->asst_sem_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }

                    if(!empty($row->sem->campaign_datefrom))
                    {
                        $cp_start[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_datefrom));
                    }

                    if(!empty($row->sem->campaign_dateto))
                    {
                        $cp_end[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_dateto));
                    }

                    if(!empty($row->sem->budget))
                    {
                        $budget[] = 'SEM: ' . $row->sem->currency .' ' .$row->sem->budget;
                    }
                }

                if(!empty($row->fb))
                {
                    if(!empty($row->fb->fb_strategist_assigned) || !empty($row->fb->asst_fb_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->fb->fb_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->fb->asst_fb_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }


                    if(!empty($row->fb->fb_campaign_dt_from))
                    {
                        $cp_start[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_from));
                    }

                    if(!empty($row->fb->fb_campaign_dt_to))
                    {
                        $cp_end[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_to));
                    }

                    if(!empty($row->fb->total_budget))
                    {
                        $budget[] = 'FB: ' . $row->fb->fb_currency . ' '. $row->fb->total_budget;
                    }
                }

                if(!empty($row->seo))
                {
                    if(!empty($row->seo->seo_strategist_assigned) || !empty($row->seo->asst_seo_strategist_assigned) || !empty($row->seo->seo_link_builder) || !empty($row->seo->asst_seo_link_builder))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->seo->seo_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_strategist_assigned)->first();
                        $team_link = Employee::where('emp_id', '=', $row->seo->seo_link_builder_assigned)->first();
                        $team_link_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_link_builder_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                        $mp[] = !empty($team_link) ? $team_link->f_name : null; 
                        $mp[] = !empty($team_link_asst) ? $team_link_asst->f_name : null;
                    }
                }

                $unique = array_unique($mp);
                $members = implode(', ', $unique);
                $campaign_start = implode("<br>", $cp_start);
                $campaign_end = implode("<br>", $cp_end);
                $total_budget = implode("<br", $budget);

                $array_contract_numbers = array_flatten(Contract::where('prospect_id', '=', $row->prospect->id)->select('contract_number')->whereNull('delete_flag')->orderBy('contract_number', 'desc')->get()->toArray());
                $contract_numbers = implode("<br>", $array_contract_numbers);
                $count_90_days = !empty($row->contract) ? ($row->contract->day_90 ? Carbon::parse($row->contract->day_90)->format('d M Y') : 'No Contract') : 'No Contract';
                $count_120_days = !empty($row->contract) ? ($row->contract->day_120 ? Carbon::parse($row->contract->day_120)->format('d M Y') : 'No Contract') : 'No Contract';
                
                $result[] = [
                    'id'         => $row->id,
                    'prospect'   => $row->prospect->id,
                    'contract_number'=> $row->contract ? $contract_numbers : 'No Contract',
                    'day_90'     => $count_90_days,
                    'day_120'    => $count_120_days,
                    'contracts'  => Contract::where('prospect_id',$row->prospect->id)->whereNull('delete_flag')->count(),
                    'created_id' => $row->created_id,
                    'company'    => $row->prospect->lead->company,
                    'holder'     => $holder,
                    'asst_holder'=> $asst_holder,
                    'sales'      => !empty($row->prospect->sales->UserInfo) ? $row->prospect->sales->UserInfo->f_name.' '.$row->prospect->sales->UserInfo->l_name : 'None',
                    'package'    => !empty($row->nature) ? $nature : 'None',
                    'status'     => $row->accountStatus->systemdesc,
                    'date'       => date('d M Y', strtotime($row->created_at)),
                    'am_fee'     => !empty($row->sem->am_fee) ? $row->sem->am_fee . '%' : 'Not Applicable',
                    'sp_assigned'=> !empty($members) ? $members : 'None',
                    'campaign_start'=> !empty($campaign_start) ? nl2br($campaign_start) : 'None',
                    'campaign_end'=> !empty($campaign_end) ? nl2br($campaign_end) : 'None',
                    'budget'     => !empty($total_budget) ? $total_budget : 'None',
                ];

            }
            
        }
        else
        {
            $result = '';
            $accounts = '';
        }
        
        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $access = Auth::user()->userAccess->toArray();

        $manager_list = Manager::select('emp_id')->get();
        $managers = array_flatten($manager_list->toArray());

        return response()->json([
            'result'     => $result,
            'pagination' => $accounts,
            'access'     => $access,
            'role'       => $role,
            'teamcode'   => $teamcode,
            'managers'   => $managers,
            'user'       => Auth::user(),
            'column'     => $column,
        ],200);
    }
    
     function refresh()
     {
         $data     = $this->tblAccounts();
         $accounts = $data['accounts'];
         $result   = $data['result'];
         $managers = $data['managers'];
         $teamcode = $data['teamcode'];
         $column   = $data['column'];
         $access = Auth::user()->userAccess->toArray();
         $role = Auth::user()->role;

         $manager_list = Manager::select('emp_id')->get();
         $managers = array_flatten($manager_list->toArray());

         return response()->json([
             'result'     => $result,
             'pagination' => $accounts,
             'access'     => $access,
             'role'       => $role,
             'teamcode'   => $teamcode,
             'managers'   => $managers,
             'user'       => Auth::user(),
             'column'     => $column,
         ],200);
     }
    

    public function sort($field,$order)
    {
        $accounts = Account::Sort($field,$order);
        $column = ModifyAccountsColumn::where('userid', '=', Auth::user()->userid)->first();
        
        if(!empty($accounts->toArray()['data']))
        {
            foreach($accounts as $key => $row)
            {
                if(!empty($row->nature))
                {
                    $nature = explode('|',$row->nature);
                    $np = array();
                    foreach($nature as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
                                        
                    $nature = implode(', ', $np);
                }

                $holder = empty($row->acc_assigned) ? 'None' : $row->holder->UserInfo->f_name.' '.$row->holder->UserInfo->l_name; 
                $asst_holder = empty($row->acc_assistant) ? 'None' : $row->asst_holder->UserInfo->f_name.' '.$row->asst_holder->UserInfo->l_name; 

                $mp = array();
                $cp_start = array();
                $cp_end = array();
                $budget = array();

                if(!empty($row->sem))
                { 
                    if(!empty($row->sem->sem_strategist_assigned) || !empty($row->sem->asst_sem_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->sem->sem_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->sem->asst_sem_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }

                    if(!empty($row->sem->campaign_datefrom))
                    {
                        $cp_start[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_datefrom));
                    }

                    if(!empty($row->sem->campaign_dateto))
                    {
                        $cp_end[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_dateto));
                    }

                    if(!empty($row->sem->budget))
                    {
                        $budget[] = 'SEM: ' . $row->sem->currency .' ' .$row->sem->budget;
                    }
                }

                if(!empty($row->fb))
                {
                    if(!empty($row->fb->fb_strategist_assigned) || !empty($row->fb->asst_fb_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->fb->fb_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->fb->asst_fb_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }


                    if(!empty($row->fb->fb_campaign_dt_from))
                    {
                        $cp_start[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_from));
                    }

                    if(!empty($row->fb->fb_campaign_dt_to))
                    {
                        $cp_end[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_to));
                    }

                    if(!empty($row->fb->total_budget))
                    {
                        $budget[] = 'FB: ' . $row->fb->fb_currency . ' '. $row->fb->total_budget;
                    }
                }

                if(!empty($row->seo))
                {
                    if(!empty($row->seo->seo_strategist_assigned) || !empty($row->seo->asst_seo_strategist_assigned) || !empty($row->seo->seo_link_builder) || !empty($row->seo->asst_seo_link_builder))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->seo->seo_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_strategist_assigned)->first();
                        $team_link = Employee::where('emp_id', '=', $row->seo->seo_link_builder_assigned)->first();
                        $team_link_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_link_builder_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                        $mp[] = !empty($team_link) ? $team_link->f_name : null; 
                        $mp[] = !empty($team_link_asst) ? $team_link_asst->f_name : null;
                    }
                }

                $unique = array_unique($mp);
                $members = implode(', ', $unique);
                $campaign_start = implode("<br>", $cp_start);
                $campaign_end = implode("<br>", $cp_end);
                $total_budget = implode("<br", $budget);

                $array_contract_numbers = array_flatten(Contract::where('prospect_id', '=', $row->prospect->id)->select('contract_number')->orderBy('contract_number', 'desc')->whereNull('delete_flag')->get()->toArray());
                $contract_numbers = implode("<br>", $array_contract_numbers);
                $count_90_days = !empty($row->contract) ? ($row->contract->day_90 ? Carbon::parse($row->contract->day_90)->format('d M Y') : 'No Contract') : 'No Contract';
                $count_120_days = !empty($row->contract) ? ($row->contract->day_120 ? Carbon::parse($row->contract->day_120)->format('d M Y') : 'No Contract') : 'No Contract';
                
                $result[] = [
                    'id'         => $row->id,
                    'prospect'   => $row->prospect->id,
                    'contract_number'=> $row->contract ? $contract_numbers : 'No Contract',
                    'day_90'     => $count_90_days,
                    'day_120'    => $count_120_days,
                    'contracts'  => Contract::where('prospect_id',$row->prospect->id)->whereNull('delete_flag')->count(),
                    'created_id' => $row->created_id,
                    'company'    => $row->prospect->lead->company,
                    'holder'     => $holder,
                    'asst_holder'=> $asst_holder,
                    'sales'      => !empty($row->prospect->sales->UserInfo) ? $row->prospect->sales->UserInfo->f_name.' '.$row->prospect->sales->UserInfo->l_name : 'None',
                    'package'    => !empty($row->nature) ? $nature : 'None',
                    'status'     => $row->accountStatus->systemdesc,
                    'date'       => date('d M Y', strtotime($row->created_at)),
                    'am_fee'     => !empty($row->sem->am_fee) ? $row->sem->am_fee . '%' : 'Not Applicable',
                    'sp_assigned'=> !empty($members) ? $members : 'None',
                    'campaign_start'=> !empty($campaign_start) ? nl2br($campaign_start) : 'None',
                    'campaign_end'=> !empty($campaign_end) ? nl2br($campaign_end) : 'None',
                    'budget'     => !empty($total_budget) ? $total_budget : 'None',
                ];
            }
        }
        else
        {
            $result = '';
        }
        
        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        
        $access = Auth::user()->userAccess->toArray();
        
        $role = Auth::user()->role;
        
        $manager_list = Manager::select('emp_id')->get();

        $managers = array_flatten($manager_list->toArray());
        
        return response()->json([
            'result'     => $result,
            'pagination' => $accounts,
            'field'      => $field,
            'order'      => $order,
            'access'     => $access,
            'role'       => $role,
            'teamcode'   => $teamcode,
            'managers'   => $managers,
            'user'       => Auth::user(),
            'column'     => $column
        ],200);
    }

    function receivers($account)
    {
        $manager_list = Manager::all();
        foreach($manager_list as $row)
        {
            if ($row->team->team_code == 'dev' && !in_array('NP004',explode('|',$account->nature)))
                continue;
            
            if ( $row->team->team_code == 'sem' || $row->team->team_code == 'seo' || $row->team->team_code == 'fb' && (!in_array('NP001',explode('|',$account->nature)) && !in_array('NP002',explode('|',$account->nature)) && !in_array('NP003',explode('|',$account->nature))) )
                continue;
            
            $emp = User::where('emp_id',$row->emp_id)->first();
            if ($row->dept_id == 2)
                continue;
            
            $receivers[] = $emp;
        }

        $administrators = User::where('role',1)->get();
        foreach($administrators as $admin)
        {
            $receivers[] = $admin;
        }

        if(!empty($account->web->dev_team))
            foreach(explode('|',$account->web->dev_team) as $dev)
            {
                $devteam = User::where('emp_id',$dev)->first();
                $receivers[] = $devteam;
            }

        if(!empty($account->seo->seo_team))
            foreach(explode('|',$account->seo->seo_team) as $seo)
            {
                $seoteam = User::where('emp_id',$seo)->first();
                $receivers[] = $seoteam;
            }

        if(!empty($account->sem->sem_team))
            foreach(explode('|',$account->sem->sem_team) as $sem)
            {
                $semteam = User::where('emp_id',$sem)->first();
                $receivers[] = $semteam;
            }
        
        if(!empty($account->fb->sem_team))
            foreach(explode('|',$account->fb->sem_team) as $fb)
            {
                $fbteam = User::where('emp_id',$fb)->first();
                $receivers[] = $fbteam;
            }
        
         if(!empty($account->acc_assigned))
            foreach(explode('|',$account->acc_assigned) as $acc)
            {
                $accteam = User::where('emp_id',$acc)->first();
                $receivers[] = $accteam;
            }
        
        foreach($receivers as $row)
        {
            if ($row['id'] == Auth::user()->id)
                continue;
            
            if($row == '')
                continue;

            $notifiable[] = $row;
        }

        return array_unique($notifiable);

    }

    function getFilterSetting()
    {
        $user = Auth::user();

        $initFilter = FilterSelection::where('table_name', 'account')->where('user_id', $user->userid)->first();
        return !empty($initFilter) ? $initFilter : '';
    }

    
    function tblAccounts()
    {
        $employee = Employee::where('emp_id',Auth::user()->emp_id)->first();
        $userlogged = Auth::user();
        $teamcode = !empty($employee->team) ? $employee->team->team_code : 'member';
        $user = $userlogged;

        $pagination = PaginationPerPage::where('table_name','accounts')->where('userid',$user->userid)->first();
        $perPage = !empty($pagination->per_page) ? $pagination->per_page : '10';
        $filter = $this->getFilterSetting();

        $column = ModifyAccountsColumn::where('userid', '=', $user->userid)->first();

        $manager_list = Manager::select('emp_id')->get();
        
        $managers = array_flatten($manager_list->toArray());

        if($user->role == 1 || (in_array($teamcode, ['acc', 'busi', 'sp']) !== false && in_array($user->userInfo->emp_id, $managers) !== false ))
        {
            $accounts = Account::whereNull('deleted_at')
                                ->when($filter->type_filter == 'busi_manager', function($query) use ($filter) {
                                    $query->where('created_id', $filter->person_filter);
                                })
                                ->when($filter->type_filter == 'account_holder', function($query) use ($filter) {
                                    $query->where('acc_assigned', $filter->person_filter);
                                })
                                ->when($filter->type_filter == 'contract_number', function($query) use ($filter) {
                                    $query->where('contract_id', $filter->person_filter);
                                    $query->orWhereNull('active_flag');
                                })
                                ->where('active_flag', 1)
                                ->orderBy('updated_at', 'desc')
                                ->paginate($perPage);
        }
        else if($teamcode == 'dev' && in_array($user->userInfo->emp_id, $managers) !== false)
        {
            $accounts = Account::whereNull('deleted_at')
                                ->where('active_flag', 1)
                                ->where('nature', 'like', '%NP003%')
                                ->orWhere('nature', 'like', '%NP004%')
                                ->orWhere('nature', 'like', '%NP008%')
                                ->orWhere('nature', 'like', '%NP009%')
                                ->orderBy('updated_at','desc')
                                ->paginate($perPage);
        }
        else if($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers) !== false)
        {
            $accounts = Account::whereNull('deleted_at')
                                ->where('nature', 'like', '%NP008%')
                                ->orderBy('updated_at','desc')
                                ->paginate($perPage);
        }
        else
        {
            $accounts = Account::whereNull('deleted_at')
                                ->where('created_id', $user->userid)
                                ->orWhere('acc_assigned', $user->userInfo->emp_id)
                                ->where('active_flag', 1)
                                ->orWhere('acc_assistant', $user->userInfo->emp_id)
                                ->orderBy('updated_at', 'desc')
                                ->paginate($perPage);
        }

        if ($user->role == 4 || ($user->role == 2 && !in_array($userlogged->userInfo->emp_id,$managers)) ) 
        {
            if($teamcode == 'sp')
            {
                if($user->UserInfo->isSEM)
                {
                    $list_accounts = Account::listAccounts('sem');
                }  
                else
                {
                    $list_accounts = Account::listAccounts('seo');
                }
                    
            }
            else if($teamcode == 'dev')
            {
                $list_accounts = Account::listAccounts('web');
            }

            else if($teamcode == 'con')
            {
                $list_accounts = Account::listAccountsForContent();
            }
            else if($teamcode == 'acc')
            {
                $list_accounts = Account::listAccountsForACCmembers();
            }
            else
            {
                $list_accounts = Account::salesLeads($user->userid);
            }    
        }
        elseif($user->role == 3)
        {
            $list_accounts = Account::FinanceView();
        }

        $accounts = in_array($user->role,[3,4]) || ($user->role == 2 && !in_array($userlogged->userInfo->emp_id,$managers)) ? $list_accounts : $accounts;

       
        if(count($accounts) > 0)
        {
            foreach($accounts as $key => $row)
            {
                if(!empty($row->nature))
                {
                    $nature = explode('|',$row->nature);
                    $np = array();
                    foreach($nature as $k => $v)
                    {
                        $package = System::where('systemcode',$v)->first();
                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                    }
                                        
                    $nature = implode(', ', $np);
                }

                $holder = empty($row->acc_assigned) ? 'None' : $row->holder->UserInfo->f_name.' '.$row->holder->UserInfo->l_name; 
                $asst_holder = empty($row->acc_assistant) ? 'None' : $row->asst_holder->UserInfo->f_name.' '.$row->asst_holder->UserInfo->l_name; 

                $mp = array();
                $cp_start = array();
                $cp_end = array();
                $budget = array();

                if(!empty($row->sem))
                { 
                    if(!empty($row->sem->sem_strategist_assigned) || !empty($row->sem->asst_sem_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->sem->sem_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->sem->asst_sem_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }

                    if(!empty($row->sem->campaign_datefrom))
                    {
                        $cp_start[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_datefrom));
                    }

                    if(!empty($row->sem->campaign_dateto))
                    {
                        $cp_end[] = 'SEM: ' . date('d M Y', strtotime($row->sem->campaign_dateto));
                    }

                    if(!empty($row->sem->budget))
                    {
                        $budget[] = 'SEM: ' . $row->sem->currency .' ' .$row->sem->budget;
                    }
                }

                if(!empty($row->fb))
                {
                    if(!empty($row->fb->fb_strategist_assigned) || !empty($row->fb->asst_fb_strategist_assigned))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->fb->fb_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->fb->asst_fb_strategist_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                    }


                    if(!empty($row->fb->fb_campaign_dt_from))
                    {
                        $cp_start[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_from));
                    }

                    if(!empty($row->fb->fb_campaign_dt_to))
                    {
                        $cp_end[] = 'FB: ' . date('d M Y', strtotime($row->fb->fb_campaign_dt_to));
                    }

                    if(!empty($row->fb->total_budget))
                    {
                        $budget[] = 'FB: ' . $row->fb->fb_currency . ' '. $row->fb->total_budget;
                    }
                }

                if(!empty($row->seo))
                {
                    if(!empty($row->seo->seo_strategist_assigned) || !empty($row->seo->asst_seo_strategist_assigned) || !empty($row->seo->seo_link_builder) || !empty($row->seo->asst_seo_link_builder))
                    {
                        $team_main = Employee::where('emp_id', '=', $row->seo->seo_strategist_assigned)->first();
                        $team_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_strategist_assigned)->first();
                        $team_link = Employee::where('emp_id', '=', $row->seo->seo_link_builder_assigned)->first();
                        $team_link_asst = Employee::where('emp_id', '=', $row->seo->asst_seo_link_builder_assigned)->first();
                        $mp[] = !empty($team_main) ? $team_main->f_name : null; 
                        $mp[] = !empty($team_asst) ? $team_asst->f_name : null;
                        $mp[] = !empty($team_link) ? $team_link->f_name : null; 
                        $mp[] = !empty($team_link_asst) ? $team_link_asst->f_name : null;
                    }
                }

                $unique = array_unique($mp);
                $members = implode(',', $unique);
                $campaign_start = implode("<br>", $cp_start);
                $campaign_end = implode("<br>", $cp_end);
                $total_budget = implode("<br", $budget);

                $array_contract_numbers = array_flatten(Contract::where('prospect_id', '=', $row->prospect->id)->select('contract_number')->whereNull('delete_flag')->orderBy('contract_number', 'desc')->get()->toArray());
                $contract_numbers = implode("<br>", $array_contract_numbers);
                $count_90_days = !empty($row->contract) ? ($row->contract->day_90 ? Carbon::parse($row->contract->day_90)->format('d M Y') : 'No Contract') : 'No Contract';
                $count_120_days = !empty($row->contract) ? ($row->contract->day_120 ? Carbon::parse($row->contract->day_120)->format('d M Y') : 'No Contract') : 'No Contract';
                
                $result[] = [
                    'id'         => $row->id,
                    'prospect'   => $row->prospect->id,
                    'contract_number'=> $row->contract ? $contract_numbers : 'No Contract',
                    'day_90'     => $count_90_days,
                    'day_120'    => $count_120_days,
                    'contracts'  => Contract::where('prospect_id',$row->prospect->id)->whereNull('delete_flag')->count(),
                    'created_id' => $row->created_id,
                    'company'    => $row->prospect->lead->company,
                    'holder'     => $holder,
                    'asst_holder'=> $asst_holder,
                    'sales'      => !empty($row->prospect->sales->UserInfo) ? $row->prospect->sales->UserInfo->f_name.' '.$row->prospect->sales->UserInfo->l_name : 'None',
                    'package'    => !empty($row->nature) ? $nature : 'None',
                    'status'     => $row->accountStatus->systemdesc,
                    'date'       => date('d M Y', strtotime($row->created_at)),
                    'am_fee'     => !empty($row->sem->am_fee) ? $row->sem->am_fee . '%' : 'Not Applicable',
                    'sp_assigned'=> !empty($members) ? $members : 'None',
                    'campaign_start'=> !empty($campaign_start) ? nl2br($campaign_start) : 'None',
                    'campaign_end'=> !empty($campaign_end) ? nl2br($campaign_end) : 'None',
                    'budget'     => !empty($total_budget) ? $total_budget : 'None',
                ];
            }
            // dd($result[0]['contract']);

        }
        else
        {
            $result = '';
        }
        
        $data = [
            'perpage'  => $pagination,
            'user'     => $user,
            'managers' => $managers,
            'teamcode' => $teamcode,
            'accounts' => $accounts,
            'result'   => $result,
            'role'     => $user->role,
            'column'  => $column,
            'filter'  => $filter
        ];
        
        // dd($data);
        return $data;
    }

    function array_push_assoc($array, $key, $value) {
        $array[$key] = $value;
        return $array;
    }
    
    function email($emailDetails)
    {
        \Mail::send('layouts.emails.userviewaccount',$emailDetails,function($message) use ($emailDetails){
            $message->to($emailDetails['email'])->subject
               ('CRM System - Converted Lead '.$emailDetails['leadname']);
            $message->from('noreply@oom.com.sg','CRM Admin');
            });
    }

    function emailAssign($emailDetails)
    {
        \Mail::send('layouts.emails.accountpackageassign',$emailDetails,function($message) use ($emailDetails){
            $message->to($emailDetails['email'])->subject
               ('CRM System - You are assigned in account '.$emailDetails['prospectname']);
            $message->from('noreply@oom.com.sg','CRM Admin');
            });
    }

    public function multipleHolderAssign(Request $request)
    {
        $arr = array();
        $data = array();
        $input = $request->except('_token');

        foreach($input as $k => $v)
        {
            $rules[$k] = 'required';
        }

        $messages = [
            'required' => 'You need to select at least one',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails())
            return response()->json($validator);

        if(!empty($input['account_ids']))
        {
            foreach($input['account_ids'] as $v)
            {
                Account::where('id', $v)->first()->update([
                    'acc_assigned' => $input['acc_assigned'],
                    'updated_at'   => Carbon::now()
                ]);

                $lists[] = Account::where('id', $v)->first();
            }

            foreach($lists as $row)
            {
                if(!empty($row->acc_assigned))
                {
                    $emp = Employee::where('emp_id', $row->acc_assigned)->first();
                    $assigned = $emp->f_name . ' ' . $emp->l_name;
                }

                if(!empty($row->created_id))
                {
                    $emp = User::where('userid', $row->created_id)->first()->userInfo;
                    $busiManager = $emp->f_name . ' ' . $emp->l_name;
                }
                    
                $data[] = [
                    'id'                => $row->id,
                    'company'           => $row->contract->company,
                    'contract_number'   => $row->contract->contract_number,
                    'acc_assigned'      => $assigned,
                    'busi_manager'      => !empty($row->created_id) ? $busiManager : 'No Business Manager',
                    'updated_at'        => Carbon::parse($row->updated_at)->format('d M Y')
                ];
            }
        }
        
        return response()->json(['result' => $data], 200);
    }

    function fetchItems($by, $item)
    {   
        switch ($by)
        {
            case 'busi_manager' :
                $name = Employee::salesPersonName();
                break;
            case 'account_holder' :
                $name = Employee::accTeam();
                break;
            case 'contract_number' :
                $name = Contract::contractNumber();
                break;
        }

        $filter = FilterSelection::where('table_name', 'account')
                                 ->where('user_id', Auth::user()->userid)
                                 ->first();
        
        return response()->json(['name' => $name, 'filter' => $filter], 200);
    }

    function setFilterItem($table, $by, $item)
    {
        $user = Auth::user();
        FilterSelection::updateOrCreate(['table_name' => $table, 'user_id' => $user->userid],['type_filter' => $by,'person_filter' => $item]);
    }

}
