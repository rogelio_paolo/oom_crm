<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class MultipleUpdateAMHolderPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_ids'  => 'required',
            'acc_assigned' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'account_ids.required' => 'You need to select at least one account',
            'acc_assigned.required' => 'You need to select at least one AM Holder'
        ];
    }

    // public function response(array $errors)
    // {
    //     if ($this->expectsJson()) {
    //         return new JsonResponse(['status' => 2, 'msg' => $errors], 422);
    //     }

    //     return parent::response($errors);
    // }
}
