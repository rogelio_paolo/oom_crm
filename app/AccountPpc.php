<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountPpc extends Model
{
    protected $table = 'accounts_ppc';

    protected $fillable = [
        'sem_strategist_assigned',
        'asst_sem_strategist_assigned',
        'sem_team',
        'target_market',
        'list_account',
        'payment_method',
        'media_channel',
        'campaign',
        'ad_scheduling',
        'ad_scheduling_remark',
        'budget',
        'google_monthly_budget',
        'google_daily_budget',
        'duration',
        'duration_interval',
        'duration_other',
        'campaign_datefrom',
        'campaign_dateto',
        'historical_data',
        'historical_data_fname',
        'currency',
        'am_fee'
    ];

    public $timestamps = false;
    
    public function market()
    {
        return $this->hasOne('App\Country','code','target_market');
    }
    
    public function mode()
    {
        return $this->hasOne('App\System','systemcode','payment_method');
    }

    public function listing()
    {
        return $this->hasOne('App\System','systemcode','list_account');
    }
    
    public function channel()
    {
        return $this->hasOne('App\System','systemcode','media_channel');
    }
    
    public function campaign_duration()
    {
        return $this->hasOne('App\System','systemcode','duration');
    }

    public function campaign_interval()
    {
        return $this->hasOne('App\System','systemcode','duration_interval');
    }

    public function assign()
    {
        return $this->hasOne('App\User','emp_id','sem_team');
    }
        
    public function TableColumns()
    {
        return \Schema::getColumnListing($this->table);
    }

}
