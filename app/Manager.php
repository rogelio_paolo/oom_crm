<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $table = 'managers';

    protected $fillable = [
        'emp_id',
        'dept_code'
    ];

    public $timestamps = false;
    
    public function team()
    {
        return $this->hasOne('App\Team','dept_code','dept_code');
    }

    public function info()
    {
        return $this->hasOne('App\Employee','emp_id','emp_id');
    }

}
