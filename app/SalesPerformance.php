<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

use App\PaginationPerPage;
use App\FilterSelection;


class SalesPerformance extends Model
{
    protected $table = 'sales_performance_list';

    public $timestamps = false;

    // protected $guarded = [];

    protected $fillable = [
        'id',
        'company',
        'client_name',
        'contract_value',
        'contact_number',
        'services',
        'landing_page',
        'status',
        'created_at',
        'created_by',
        'status_updated_by',
    ];

    protected $dates = [
        'status_updated_at',
        'updated_at'
    ];

    public static function scopeGetPaginationPerPage()
    {
        $user = Auth::user();
        $items = PaginationPerPage::where('table_name', 'performance')->where('userid', '=', $user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        return $perPage;
    }

    public static function scopeGetFilterSelection()
    {
        $user = Auth::user();
        $filter = FilterSelection::where('table_name', 'performance')->where('user_id', '=', $user->userid)->first();
        return !empty($filter) ? $filter->person_filter : 'all';
    }

    public function getUser()
    {
        return $this->hasOne('App\User', 'userid', 'created_by');
    }

    public function getStatusUpdatedBy()
    {
        return $this->hasOne('App\User', 'userid', 'status_updated_by');
    }

    public function getStatus()
    {
        return $this->hasOne('App\System', 'systemcode', 'status');
    }


    public function scopeFindCompany($query, $key)
    {
        $user = Auth::user();

        if ($user->role == 1)
        {
            return $query->where('company','like','%'.$key.'%')
                ->paginate(self::scopeGetPaginationPerPage());
        }
        else
        {
            return $query->where('company','like','%'.$key.'%')
                ->where('created_by', $user->userid)
                ->paginate(self::scopeGetPaginationPerPage());
        }
    }

    public function scopeFindName($query, $key)
    {
        $user = Auth::user();

        if ($user->role == 1)
        {
            return $query->where('client_name','like','%'.$key.'%')
                ->paginate(self::scopeGetPaginationPerPage());
        }
        else
        {
            return $query->where('client_name','like','%'.$key.'%')
                ->where('created_by', $user->userid)
                ->paginate(self::scopeGetPaginationPerPage());
        }
    }

    public function scopeFindContact($query, $key)
    {
        $user = Auth::user();

        if ($user->role == 1)
        {
            return $query->where('contact_number','like','%'.$key.'%')
                ->paginate(self::scopeGetPaginationPerPage());
        }
        else
        {
            return $query->where('contact_number','like','%'.$key.'%')
                ->where('created_by', $user->userid)
                ->paginate(self::scopeGetPaginationPerPage());
        }
    }

    public function scopeFindContractValue($query, $key)
    {
        $user = Auth::user();

        if ($user->role == 1)
        {
            return $query->where('contract_value','like','%'.$key.'%')
                ->paginate(self::scopeGetPaginationPerPage());
        }
        else
        {
            return $query->where('contract_value','like','%'.$key.'%')
                ->where('created_by', $user->userid)
                ->paginate(self::scopeGetPaginationPerPage());
        }
    }

    public function scopeSort($query, $field, $order)
    {
        $user = Auth::user();
        $filter = self::scopeGetFilterSelection();

        if($user->role == 1)
        {
            if ($filter == 'all')
                return $query->orderBy($field, $order)
                    ->paginate(self::scopeGetPaginationPerPage());
            else
                return $query->where('created_by', $filter)->orderBy($field, $order)
                    ->paginate(self::scopeGetPaginationPerPage());
        }
        else
        {
            return $query->where('created_by', $user->userid)
                ->orderBy($field, $order)
                ->paginate(self::scopeGetPaginationPerPage());
        }

    }

    public function remarks()
    {
        return $this->hasMany('App\LeadNote', 'lead_id', 'id');
    }

}
