<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterSelection extends Model
{
    protected $table = 'filter_selection';

    protected $fillable = [
        'user_id',
        'type_filter',
        'person_filter',
        'table_name'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'userid', 'person_filter');
    }

    public function emp()
    {
        return $this->hasOne('App\Employee', 'emp_id', 'person_filter');
    }
}
