<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBlogContent extends Model
{
    protected $table = 'accounts_blog';

    protected $fillable = [
        'blog_content_team',
        'blog_total_post',
        'blog_monthly_post',
        'blog_content_brief',
        'blog_hist_data',
        'blog_hist_data_fname'
    ];

    public $timestamps = false;
}
