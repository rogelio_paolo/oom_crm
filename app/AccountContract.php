<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountContract extends Model
{
    protected $table = 'account_contracts';

    protected $fillable = [
            'account_id',
            'prospect_id',
            'contract_id',
            'is_active'
    ];

    public $timestamps = false;

    public function prospect()
    {
        return $this->hasMany('App\Prospect', 'id', 'prospect_id');
    }

    public function account() 
    {
        return $this->hasMany('App\Account', 'id', 'account_id');
    }

    public function contract() 
    {
        return $this->hasMany('App\Contract', 'id', 'contract_id');
    }


}
