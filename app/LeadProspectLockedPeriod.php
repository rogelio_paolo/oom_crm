<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadProspectLockedPeriod extends Model
{
    protected $table = 'lead_prospect_locked_period';

    protected $fillable = [
        'userid',
        'leadprospect_to_unlock'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User','userid','userid');
    }

}
