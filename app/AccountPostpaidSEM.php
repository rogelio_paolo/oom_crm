<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountPostpaidSEM extends Model
{
    protected $table = 'accounts_postpaid_sem';

    protected $fillable = [
        'postpaid_duration',
        'postpaid_duration_interval',
        'postpaid_duration_other',
        'postpaid_campaign_datefrom',
        'postpaid_campaign_dateto',
        'postpaid_media_budget',
        'postpaid_am_fee',
        'postpaid_am_fee_amount',
        'is_media_budget_paid',
        'postpaid_total',
    ];

    public $timestamps = false;

    

    public function campaign_duration()
    {
        return $this->hasOne('App\System','systemcode','postpaid_duration');
    }

    public function campaign_interval()
    {
        return $this->hasOne('App\System','systemcode','postpaid_duration_interval');
    }
}
