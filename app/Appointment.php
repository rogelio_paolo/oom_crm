<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use App\Manager;
use Carbon\Carbon;

class Appointment extends Model
{
    
    protected $table = 'appointments';

    protected $fillable = [
        'google_calendar_id',
        'google_calendarlist_id',
        'color',
        'lead_id',
        'client_name',
        'company',
        'title',
        'description',
        'date_start',
        'date_end',
        'guest',
        'status',
        'services',
        'created_by',
        'delete_flag',
        'deleted_at',
    ];

    private $user;
    private $userRole;
    private $teamCode;
    private $managers;

    function getUser()
    {
        $user = Auth::user();
        return $this->user = $user;
    }

    function getUserRole()
    {
        $userRole = $this->getUser()->role;
        return $this->userRole = $userRole;
    }

    function getTeamCode()
    {
        $team = $this->getUser()->userInfo->team->team_code;
        return $this->teamCode = $team;
    }

    function getManagers()
    {
        $managers_list = Manager::select('emp_id')->get();
        $managers = array_flatten($managers_list->toArray());
        return $this->managers = $managers;
    }

    public function eventColor()
    {
        return $this->hasOne('App\EventColor', 'id', 'color');
    }

    public function lead()
    {
        return $this->hasOne('App\Lead', 'id', 'lead_id');
    }

    public function apStatus()
    {
        return $this->hasOne('App\System', 'systemcode', 'status');
    }

    public function apServices()
    {
        return $this->hasOne('App\System', 'systemcode', 'services');
    }

    public function creator() {
        return $this->hasOne('App\User', 'userid', 'created_by');
    }

    public function scopeListAppointments($query)
    {
        if ($this->getUserRole() == 1 || ($this->getTeamCode() == 'acc' && in_array($this->getManagers(), $this->getUser()->emp_id)) ) // If admin or account manager
            return $query->whereNull('delete_flag')->orderBy('updated_at', 'desc')->get();
        else
            return $query->whereNull('delete_flag')->where('created_by', $this->getUser()->userid)->orderBy('updated_at', 'desc')->get(); // else normal users e.g sales team and acc team
    }

    public function scopeListPendingAppointments($query)
    {
        // if ($this->getUserRole() == 1 || ($this->getTeamCode() == 'acc' && in_array($this->getManagers(), $this->getUser()->emp_id)) ) // If admin or account manager
            // return $query->where('date_end', '<', Carbon::now()->toDateTimeString())->where('status', 'AP001')->whereNull('delete_flag')->orderBy('updated_at', 'desc')->get();
        // else
            return $query->where('date_end', '<', Carbon::now()->toDateTimeString())->where('created_by', $this->getUser()->userid)->where('status', 'AP001')->whereNull('delete_flag')->orderBy('updated_at', 'desc')->get(); // else normal users e.g sales team and acc team
            
    }

    public function scopeListCurrentAppointmentReport($query, $createdBy, $startDate, $endDate)
    {
        return $this->appointment($query, $createdBy, $startDate, $endDate);  
    }

    public function scopeListLastWeekAppointmentReport($query, $createdBy, $lastWeekStartDate, $lastWeekEndDate)
    {
        return $this->appointment($query, $createdBy, $lastWeekStartDate, $lastWeekEndDate);
    }

    public function scopeListCurrentPostponedAppointments($query, $createdBy, $startDate, $endDate) 
    {
        return $this->postponed($query, $createdBy, $startDate, $endDate);   
    }

    public function scopeListLastWeekPostponedAppointments($query, $createdBy, $lastWeekStartDate, $lastWeekEndDate) 
    {
        return $this->postponed($query, $createdBy, $lastWeekStartDate, $lastWeekEndDate);
    }

    // HELPER FUNCTIONS
    function appointment($query, $createdBy, $startDate, $endDate)
    {
        if ($this->getUserRole() == 1 || ($this->getTeamCode() == 'acc' && in_array($this->getManagers(), $this->getUser()->emp_id)) ) // If admin or account manager
        {
            if ($createdBy == 'all')
                return $query->whereNull('delete_flag')
                             ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_start', '>=', $startDate->toDateString());
                                $query->whereDate('date_start', '<=', $endDate->toDateString());
                             })
                             ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_end', '>=', $startDate->toDateString());
                                $query->whereDate('date_end', '<=', $endDate->toDateString());
                             })
                             ->orderBy('updated_at', 'desc')
                             ->get();
        
            return $query->where('created_by', $createdBy)
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_start', '>=', $startDate->toDateString());
                                $query->whereDate('date_start', '<=', $endDate->toDateString());
                            })
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_end', '>=', $startDate->toDateString());
                                $query->whereDate('date_end', '<=', $endDate->toDateString());
                            })
                         ->whereNull('delete_flag')
                         ->orderBy('updated_at', 'desc')
                         ->get();
        } 
        else
        {
            return $query->where('created_by', $this->getUser()->userid)
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_start', '>=', $startDate->toDateString());
                                $query->whereDate('date_start', '<=', $endDate->toDateString());
                            })
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_end', '>=', $startDate->toDateString());
                                $query->whereDate('date_end', '<=', $endDate->toDateString());
                            })
                         ->whereNull('delete_flag')
                         ->orderBy('updated_at', 'desc')
                         ->get(); // else normal users e.g sales team and acc team
        }
    }

    function postponed($query, $createdBy, $startDate, $endDate)
    {
        if ($this->getUserRole() == 1 || ($this->getTeamCode() == 'acc' && in_array($this->getManagers(), $this->getUser()->emp_id)) ) // If admin or account manager
        {
            if ($createdBy == 'all')
                return $query->where('status', 'AP004')
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_start', '>=', $startDate->toDateString());
                                $query->whereDate('date_start', '<=', $endDate->toDateString());
                            })
                            ->where(function($query) use ($startDate, $endDate) {
                                $query->whereDate('date_end', '>=', $startDate->toDateString());
                                $query->whereDate('date_end', '<=', $endDate->toDateString());
                            })
                             ->whereNull('delete_flag')
                             ->orderBy('updated_at', 'desc')
                             ->count();
            
            return $query->where('status', 'AP004')
                         ->whereNull('delete_flag')
                         ->where('created_by', $createdBy)
                         ->where(function($query) use ($startDate, $endDate) {
                            $query->whereDate('date_start', '>=', $startDate->toDateString());
                            $query->whereDate('date_start', '<=', $endDate->toDateString());
                         })
                         ->where(function($query) use ($startDate, $endDate) {
                            $query->whereDate('date_end', '>=', $startDate->toDateString());
                            $query->whereDate('date_end', '<=', $endDate->toDateString());
                         })
                         ->orderBy('updated_at', 'desc')
                         ->count();
        }
        else
        {
            return $query->whereNull('delete_flag')
                         ->where('status', 'AP004')
                         ->where('created_by', $this->getUser()->userid)
                         ->where(function($query) use ($startDate, $endDate) {
                            $query->whereDate('date_start', '>=', $startDate->toDateString());
                            $query->whereDate('date_start', '<=', $endDate->toDateString());
                         })
                         ->where(function($query) use ($startDate, $endDate) {
                            $query->whereDate('date_end', '>=', $startDate->toDateString());
                            $query->whereDate('date_end', '<=', $endDate->toDateString());
                         })
                         ->orderBy('updated_at', 'desc')
                         ->count(); // else normal users e.g sales team and acc team
        }
    }

}
