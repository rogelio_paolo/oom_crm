<?php

namespace App\Exports;

use App\SalesPerformance;
use Maatwebsite\Excel\Concerns\FromCollection;

class SalesPerformanceExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SalesPerformance::all();
    }
}
