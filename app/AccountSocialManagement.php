<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSocialManagement extends Model
{
    protected $table = 'accounts_social';

    protected $fillable = [
        'social_content_team',
        'social_duration',
        'social_duration_interval',
        'social_duration_other',
        'social_monthly_post',
        'social_content_brief',
        'social_media_budget',
        'social_hist_data',
        'social_hist_data_fname'
    ];

    public $timestamps = false;

    public function campaign_duration()
    {
        return $this->hasOne('App\System','systemcode','social_duration');
    }

    public function campaign_interval()
    {
        return $this->hasOne('App\System','systemcode','social_duration_interval');
    }
}
