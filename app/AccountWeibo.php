<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountWeibo extends Model
{
    protected $table = 'accounts_weibo';

    protected $fillable = [
        'weibo_location',
        'weibo_account',
        'has_blue_v_weibo',
        'weibo_campaign',
        'weibo_duration',
        'weibo_duration_interval',
        'weibo_duration_other',
        'weibo_campaign_datefrom',
        'weibo_campaign_dateto',
        'has_weibo_ad_scheduling',
        'weibo_ad_scheduling_remark',
        'weibo_currency',
        'weibo_budget',
        'weibo_monthly_budget',
        'weibo_daily_budget',
        'weibo_hist_data',
        'weibo_hist_data_fname'
    ];

    public $timestamps = false;

    public function campaign_duration()
    {
        return $this->hasOne('App\System','systemcode','weibo_duration');
    }

    public function campaign_interval()
    {
        return $this->hasOne('App\System','systemcode','weibo_duration_interval');
    }
}
