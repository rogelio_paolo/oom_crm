<?php

namespace App;

use Auth;
use App\PaginationPerPage;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = [
      "branch",
      "emp_id",
      "f_name",
      "m_name",
      "l_name",
      "gender",
      "nationality",
      "bday",
      "religion",
      "highest_education",
      "civil_status",
      "children",
      "mobile_number",
      "landline_number",
      "address",
      "personal_email",
      "med_history",
      "tin_number",
      "sss_number",
      "pagibig_number",
      "philhealth_number",
      "bank_name",
      "bank_acc_no",
      "payee_name",
      "bank_code",
      "branch_code",
      "sales_target",
      "sales_achieved",
      "emergency_contact_name",
      "emergency_contact_number",
      "emergency_contact_rel",
      "dept_code",
      "designation",
      "status",
      "date_hired",
      "confirmation_date",
      "exit_date",
      "nric_id",
      "delete_flag",
      "deleted_at"
    ];

    public function UserInfo()
    {
        return $this->hasOne('App\User','emp_id','emp_id');
    }

    public function emp_status()
    {
        return $this->hasOne('App\System','systemcode','status');
    }

    public function emp_department()
    {
        return $this->hasOne('App\System','systemcode','dept_code');
    }

    public function emp_designation()
    {
        return $this->hasOne('App\Designation','designation_code','designation');
    }

    public function team()
    {
        return $this->hasOne('App\Team','dept_code','dept_code');
    } 

    public function getFullNameAttribute()
    {
        return ucfirst($this->f_name) . ' ' . ucfirst($this->l_name);
    }

    public function scopeFindDesignation($query,$designation)
    {
        $items = PaginationPerPage::where('table_name','employees')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        return $query->select('employees.*')
                     ->join('designations','employees.designation','=','designations.designation_code')
                     ->orderBy('designations.designation','asc')
                     ->whereNull('employees.delete_flag')
                     ->where('designations.designation','like','%'.$designation.'%')
                     ->paginate($perPage);
    }

    public function scopeFindStatus($query,$status)
    {
        $items = PaginationPerPage::where('table_name','employees')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        return $query->select('employees.*')
                     ->join('system','employees.status','=','system.systemcode')
                     ->orderBy('system.systemdesc','asc')
                     ->whereNull('employees.delete_flag')
                     ->where('system.systemdesc','like','%'.$status.'%')
                     ->paginate($perPage);
    }

    public function scopeFindRole($query,$role)
    {
        $items = PaginationPerPage::where('table_name','employees')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        return $query->select('employees.*')
                     ->join('users','employees.emp_id','=','users.emp_id')
                     ->join('roles','users.role','=','roles.id')
                     ->join('system','roles.role','=','system.systemcode')
                     ->orderBy('system.systemdesc','asc')
                     ->whereNull('employees.delete_flag')
                     ->where('system.systemdesc','like','%'.$role.'%')
                     ->paginate($perPage);
    }

    public function scopeSort($query,$field,$order)
    {
        $items = PaginationPerPage::where('table_name','employees')->where('userid',Auth::user()->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';

        if ($field == 'role')
            return $query->select('employees.*')
                         ->join('users','employees.emp_id','users.emp_id')
                         ->join('roles','users.role','roles.id')
                         ->join('system','roles.role','system.systemcode')
                         ->whereNull('employees.delete_flag')
                         ->orderBy('system.systemdesc',$order)
                         ->paginate($perPage);
        elseif ($field == 'designation')
            return $query->select('employees.*')
                         ->join('designations','employees.designation','designations.designation_code')
                         ->whereNull('employees.delete_flag')
                         ->orderBy('designations.designation',$order)
                         ->paginate($perPage);
        elseif ($field == 'status')
            return $query->select('employees.*')
                         ->join('system','employees.status','system.systemcode')
                         ->whereNull('employees.delete_flag')
                         ->orderBy('system.systemdesc',$order)
                         ->paginate($perPage);
        else
            return $query->whereNull('delete_flag')->orderBy($field,$order)->paginate($perPage);
    }
    
    public function scopeAccountholder($query)
    {
        
        return $query->join('users','employees.emp_id','users.emp_id')
                     ->where('employees.dept_code', 'DP001')
                     ->whereNull('employees.delete_flag')
                     ->get();
    }

    public function scopeSales($query)
    {
        return $query->join('users','employees.emp_id','users.emp_id')
                     ->where('employees.dept_code', 'DP002')
                     ->whereNull('employees.delete_flag')
                     ->get();
    }

    public function scopeSalesPersonName($query)
    {
        return $query->select('users.userid', 'employees.emp_id', 'employees.f_name', 'employees.l_name')
                     ->join('users','employees.emp_id','users.emp_id')
                     ->where(function($query) {
                        $query->where('users.role', 1);
                        $query->where('employees.designation', '!=', 'DS031');
                        $query->orWhere('employees.designation', '=', 'DS027');
                     })
                     ->orWhere('employees.dept_code', 'DP002')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003','ES006'])
                     ->get();
    }

    public function isSEO()
    {
        return $this->hasOne('App\Designation','designation_code','designation')
                    ->whereIn('designation_code', ['DS016', 'DS017']);
    }

    public function isSEM()
    {
        return $this->hasOne('App\Designation','designation_code','designation')
                    ->whereIn('designation_code', ['DS023', 'DS024']);
    }

    public function isDEV()
    {
        return $this->hasOne('App\System','systemcode','dept_code')
                    ->where('dept_code', 'DP004');
    }

    public function isACC()
    {
        return $this->hasOne('App\System','systemcode', 'dept_code')
                    ->where('dept_code', 'DP001');
    }

    public function isContent()
    {
        return $this->hasOne('App\System', 'systemcode', 'dept_code')
                    ->where('dept_code', 'DP003');
    }

    public function scopeSeoTeam($query)
    {
        return $query->select('employees.*')
                    ->join('designations', 'employees.designation', 'designations.designation_code')
                    ->leftjoin('managers','employees.emp_id','managers.emp_id')
                    ->whereNull('employees.delete_flag')
                    ->whereNotIn('employees.status', ['ES003','ES006'])
                    //  ->whereIn('employees.designation', ['DS016', 'DS017'])
                    ->where('employees.dept_code','DP010')
                    ->whereNotIn('employees.designation', ['DS014', 'DS015'])
                    ->whereNull('managers.emp_id')
                        ->get();
    }

    public function scopeLinkBuilders($query)
    {
        return $query->select('employees.*')
                    ->join('designations', 'employees.designation', 'designations.designation_code')
                    ->whereNull('employees.delete_flag')
                    ->whereNotIn('employees.status', ['ES003', 'ES006'])
                    ->whereIn('employees.designation', ['DS014', 'DS015'])
                    ->get();
    }

    public function scopeStrategists($query)
    {
        return $query->select('employees.*')
                     ->join('system', 'employees.dept_code', 'system.systemcode')
                     ->join('designations', 'employees.designation', 'designations.designation_code')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003', 'ES006'])
                     ->whereIn('employees.designation', ['DS023', 'DS024'])
                     ->get();
    }

    public function scopeSemTeam($query)
    {
        return $query->select('employees.*')
                     ->join('designations', 'employees.designation', 'designations.designation_code')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003','ES006'])
                     ->whereIn('employees.designation', ['DS023', 'DS024']) 
                     ->get();
    }

    public function scopeDevTeam($query)
    {
        return $query->select('employees.*')
                     ->join('system', 'employees.dept_code', 'system.systemcode')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003','ES006'])
                     ->where('employees.dept_code', 'DP004')
                     ->get();
    }

    public function scopeAccTeam($query)
    {
        return $query->select('employees.*')
                     ->join('system', 'employees.dept_code', 'system.systemcode')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003','ES006'])
                     ->where('employees.dept_code', 'DP001')
                     ->get();
    }

    public function scopeContentTeam($query)
    {
        return $query->select('employees.*')
                     ->join('system', 'employees.dept_code', 'system.systemcode')
                     ->whereNull('employees.delete_flag')
                     ->whereNotIn('employees.status', ['ES003','ES006'])
                     ->where('employees.dept_code', 'DP003')
                     ->get();
    }
    
}
