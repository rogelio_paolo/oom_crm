<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBaidu extends Model
{
    protected $table = 'accounts_baidu';

    protected $fillable = [
        'baidu_account',
        'baidu_target_market',
        'baidu_city',
        'baidu_media_channel',
        'baidu_campaign',
        'baidu_duration',
        'baidu_duration_interval',
        'baidu_duration_other',
        'baidu_campaign_datefrom',
        'baidu_campaign_dateto',
        'has_baidu_ad_scheduling',
        'baidu_ad_scheduling_remark',
        'baidu_currency',
        'baidu_budget',
        'baidu_monthly_budget',
        'baidu_daily_budget',
        'baidu_hist_data',
        'baidu_hist_data_fname'
    ];

    public $timestamps = false;

    public function campaign_duration()
    {
        return $this->hasOne('App\System','systemcode','baidu_duration');
    }

    public function campaign_interval()
    {
        return $this->hasOne('App\System','systemcode','baidu_duration_interval');
    }
}


