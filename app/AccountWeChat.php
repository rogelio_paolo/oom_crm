<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountWeChat extends Model
{
    protected $table = 'accounts_wechat';

    protected $fillable = [
        'has_wechat_type',
        'wechat_type',
        'has_wechat_menu_setup',
        'wechat_advertising_type',
        'wechat_location',
        'wechat_age_from',
        'wechat_age_to',
        'wechat_marital',
        'wechat_gender',
        'wechat_handphone',
        'wechat_education',
        'wechat_hist_data',
        'wechat_hist_data_fname'
    ];

    public $timestamps = false;
}
