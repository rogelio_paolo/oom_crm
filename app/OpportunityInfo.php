<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\System;
use App\Industry;
use App\User;
use App\Opportunity;

use Carbon\Carbon;

class OpportunityInfo extends Model
{
    protected $table = 'opportunity_info';
    
    protected $fillable = [
        'opportunity_id',
        'status',
        'contract_type',
        'currency',
        'contract_value',
        'client_name',
        'office_number',
        'mobile_number',
        'email',
        'designation',
        'address',
        'postal_code',
        'city',
        'industry',
        'website_url',
        'services',
        'other_services',
        'source',
        'other_source',
        'landing_page',
        'other_landing',
        'created_by',
        'updated_by',
        'active_flag',
        'delete_flag'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function opportunity()
    {
        return $this->belongsTo('App\Opportunity', 'opportunity_id', 'id');
    }


    /*-------------------- MUTATOR/ACCESSOR HELPERS --------------------------- */
    public function getCompanyAttribute()
    {
        $opp_id = Opportunity::where('id', $this->opportunity_id)->first();
        return $opp_id->company;
    }

    public function getStatusDescAttribute()
    {
        return System::where('systemcode', ($this->status ?
            $this->status : 'LS002'))->first()->systemdesc;
    }

    public function getContractTypeDescAttribute()
    {
        return System::where('systemcode', ($this->contract_type ?
            $this->contract_type : 'New'))->first()->systemdesc;
    }

    public function getContractValueDescAttribute()
    {
        return !empty($this->contract_value) ? "{$this->currency} {$this->contract_value}" : 'Not Specified';
    }

    public function getClientNameDescAttribute()
    {
        return !empty($this->client_name)  ? ($this->client_name == ' ' ? 'Not Specified' : $this->client_name) : "Not Specified";
    }

    public function getOfficeNumberDescAttribute()
    {
        return !empty($this->office_number) ? $this->office_number : "Not Specified";
    }

    public function getMobileNumberDescAttribute()
    {
        return !empty($this->mobile_number) ? $this->mobile_number : "Not Specified";
    }  

    public function getEmailDescAttribute()
    {
        return !empty($this->email) ? implode(', ', explode('|', $this->email)) : "Not Specified";
    }  
    
    public function getDesignationDescAttribute()
    {
        return !empty($this->designation) ? $this->designation : "Not Specified";
    }
    
    public function getAddressDescAttribute()
    {
        return !empty($this->address) ? $this->address : "Not Specified";
    }  

    public function getPostalCodeDescAttribute()
    {
        return !empty($this->postal_code) ? $this->postal_code : "Not Specified";
    }  

    public function getCityDescAttribute()
    {
        return !empty($this->city) ? $this->city : "Not Specified";
    }  

    public function getIndustryDescAttribute()
    {
        $industry = Industry::where('code', $this->industry)->first();
        return !empty($industry) ? $industry->title : "Not Specified";
    }

    public function getWebsiteUrlDescAttribute()
    {
        return !empty($this->website_url) ? $this->website_url : "Not Specified";
    }
    
    public function getServicesDescAttribute()
    {
        if (!empty($this->services))
        {
            $array = array();
            $explodeSvc = explode('|', $this->services);

            foreach ($explodeSvc as $row)
            {
                $array = System::where('systemcode', $row)->first();
                $services[] = mb_strimwidth($array->systemdesc, 0, 50, '');
            }
    
            return implode(', ', $services);
        }

        return 'Not Specified';
    }

    public function getOtherServicesDescAttribute()
    {
        return !empty($this->other_services) ? $this->other_services : "Not Specified";
    }

    public function getSourceDescAttribute()
    {
        $source = System::where('systemcode', $this->source)->first();
        return !empty($source) ? $source->systemdesc : "Not Specified";
    }

    public function getOtherSourceDescAttribute()
    {
        return !empty($this->other_source) ? $this->other_source : "Not Specified";
    }

    public function getLandingPageDescAttribute()
    {
        $landing = System::where('systemcode', $this->landing_page)->first();
        return !empty($landing) ? $landing->systemdesc : "Not Specified";
    }

    public function getOtherLandingDescAttribute()
    {
        return !empty($this->other_landing) ? $this->other_landing : "Not Specified";
    }

    public function getCreatedAtDescAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y h:i A');
    }

    public function getUpdatedAtDescAttribute()
    {
        return !empty($this->updated_at) ? Carbon::parse($this->updated_at)->format('d M Y H:i A') : 'Not Updated';
    }

    public function getCreatedByDescAttribute()
    {
        $user = User::where('userid', $this->created_by)->first();
        return !empty($user) ? "{$user->userInfo->f_name} {$user->userInfo->l_name}" : "No Owner";
    }

    public function getUpdatedByDescAttribute()
    {
        $user = User::where('userid', $this->updated_by)->first();
        return !empty($user) ? "{$user->userInfo->f_name} {$user->userInfo->l_name}" : "Not Updated";
    }

    /*-------------------- END OF BLOCK (MUTATOR/ACCESSOR HELPERS) --------------------------- */
}
