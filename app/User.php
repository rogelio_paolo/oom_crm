<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class User extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'users';

    protected $fillable = [
      'emp_id',
      'userid',
      'email',
      'username',
      'password',
      'role',
      'login_count_today',
      'firstname',
      'lastname',
      'delete_flag',
      'deleted_at',
      'dept_code'
    ];

    protected $dates = [
        'current_login_at', 'last_login'
    ]; 

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
      // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
          parent::setAttribute($key, $value);
        }
    }

    public function UserInfo()
    {
        return $this->hasOne('App\Employee','emp_id','emp_id');
    }

    public function manager()
    {
        return $this->hasOne('App\Manager','emp_id','emp_id');
    }

    public function userRole()
    {
        return $this->hasOne('App\Role','id','role');
    }

    public function userAccess()
    {
        return $this->hasMany('App\Access','userid','userid');
    }
    
    public function scopeSort($query,$field,$order)
    {

        if ($field != 'role')
            return $query->whereNull('delete_flag')->orderBy($field,$order)->paginate(5);
        else
            return $query->join('roles','users.role','roles.id')
                         ->join('system','roles.role','system.systemcode')
                         ->whereNull('users.delete_flag')
                         ->orderBy('system.systemdesc',$order)
                         ->paginate(5);
    }

    public function comments()
    {
        return $this->hasMany(\Laravelista\Comments\Comments\Comment::class);
    }

    public function scopeEmailRecipientName($query)
    {
        return $query->select('users.email', 'employees.f_name', 'employees.l_name')
                    ->join('employees', 'users.emp_id', 'employees.emp_id')
                    ->whereNull('users.delete_flag')
                    ->whereNull('employees.delete_flag')
                    ->where('users.id', '!=', auth()->user()->id)
                    ->orderBy('role')
                    ->get();
    }
}
