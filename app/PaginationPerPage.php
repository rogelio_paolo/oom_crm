<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaginationPerPage extends Model
{
    protected $table = 'pagination_per_page';

    protected $fillable = ['userid','table_name','per_page'];

    public $timestamps = false;
}
