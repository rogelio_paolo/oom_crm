<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountWeb extends Model
{
    protected $table = 'accounts_webdev';

    protected $fillable = [
      'dev_team',
      'dev_content_team',
      'project_type',
      'cmstype',
      'project_brief',
      'pages',
      'objective',
      'company_brief',
      'target_audience',
      'existing_web_url',
      'reference_web',
      'web_ui',
      'color_scheme',
      'functionality',
      'web_hist_data',
      'web_hist_data_fname'

    ];

    public $timestamps = false;
    
    public function project()
    {
        return $this->hasOne('App\System','systemcode','project_type');
    }
    
    public function cms()
    {
        return $this->hasOne('App\System','systemcode','cmstype');
    }
}
