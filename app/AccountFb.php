<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountFb extends Model
{
    protected $table = 'accounts_fb';

    protected $fillable = [
        'fb_strategist_assigned',
        'asst_fb_strategist_assigned',
        'sem_team',
        'fb_target_country',
        'fb_list_account',
        'fb_payment',
        'fb_media_channel',
        'fb_campaign',
        'total_budget',
        'monthly_budget',
        'daily_budget',
        'fb_duration',
        'fb_duration_interval',
        'fb_duration_other',
        'fb_campaign_dt_from',
        'fb_campaign_dt_to',
        'gender',
        'age_from',
        'age_to',
        'interest',
        'fb_campaign_dt_to',
        'fb_hist_data',
        'fb_hist_data_fname',
        'fb_currency'
    ];

    public $timestamps = false;
    
    public function market()
    {
        return $this->hasOne('App\Country','code','fb_target_country');
    }
    
    public function mode()
    {
        return $this->hasOne('App\System','systemcode','fb_payment');
    }

    public function listing()
    {
        return $this->hasOne('App\System','systemcode','fb_list_account');
    }
    
    public function channel()
    {
        return $this->hasOne('App\System','systemcode','fb_media_channel');
    }
    
    public function duration()
    {
        return $this->hasOne('App\System','systemcode','fb_duration');
    }

    public function duration_interval()
    {
        return $this->hasOne('App\System','systemcode','fb_duration_interval');
    }
}
