<?php

namespace App;

use App\PaginationPerPage;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Opportunity extends Model
{
    protected $table = 'opportunities';

    protected $fillable = [
        'prospect_id',
        'company'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function prospect()
    {
        return $this->hasOne('App\Prospect', 'id', 'prospect_id')
                    ->orderBy('created_at', 'desc');
    }

    public function info()
    {
        return $this->hasMany('App\OpportunityInfo', 'opportunity_id', 'id')
                    ->whereNull('delete_flag');

    }

    public function scopeList($query, $field = false, $order = false)
    {
        $user = Auth::user();
        $selectedField = $field === 'company' || $field === 'created_at' ? 'opportunities.'.$field : 'opportunity_info.'.$field;

        return $user->role === 1 ? $query->select('opportunities.*', $selectedField)
                                        ->distinct()
                                        ->join('opportunity_info', 'opportunities.id', 'opportunity_info.opportunity_id')
                                        ->whereNull('opportunity_info.delete_flag')
                                        ->orderBy($selectedField, $order)
                                        ->get()
                                :  $query->select('opportunities.*', $selectedField)
                                        ->distinct()
                                        ->join('opportunity_info', 'opportunities.id', 'opportunity_info.opportunity_id')
                                        ->whereNull('opportunity_info.delete_flag')
                                        ->where('opportunity_info.created_by', $user->userid)
                                        ->orderBy($selectedField, $order)
                                        ->get();
    }

    public function scopeFindCompany($query, $string)
    {
        $user = Auth::user();

        if ($user->role === 1)
        {
            return $query->where('company', 'like', '%'.$string.'%')
                        ->orderBy('created_at', 'desc')
                        ->get();
        }
        else
        {
            return $query->select('opportunities.*')
                        ->distinct()
                        ->join('opportunity_info', 'opportunities.id', 'opportunity_info.opportunity_id')
                        ->where('opportunities.company', 'like', '%'.$string.'%')
                        ->where('opportunity_info.created_by', $user->userid)
                        ->orderBy('opportunities.created_at', 'desc')
                        ->get();
        }
    }

    public function scopeFindClientName($query, $string)
    {
        $user = Auth::user();

        if ($user->role === 1)
        {
            return $query->select('opportunities.*')
                        ->distinct()
                        ->join('opportunity_info', 'opportunities.id', 'opportunity_info.opportunity_id')
                        ->where('opportunity_info.client_name', 'like', '%'.$string.'%')
                        ->whereNull('opportunity_info.delete_flag')
                        ->orderBy('opportunities.created_at', 'desc')
                        ->get();
        }
        else
        {
            return $query->select('opportunities.*')
                        ->distinct()
                        ->join('opportunity_info', 'opportunities.id', 'opportunity_info.opportunity_id')
                        ->where('opportunity_info.client_name', 'like', '%'.$string.'%')
                        ->whereNull('opportunity_info.delete_flag')
                        ->where('opportunity_info.created_by', $user->userid)
                        ->orderBy('opportunities.created_at', 'desc')
                        ->get();
        }
    }
    
    public static function scopeGetPaginationPerPage()
    {
        $user = Auth::user();
        $items = PaginationPerPage::where('table_name', 'opportunity')->where('userid', '=', $user->userid)->first();
        $perPage = !empty($items->per_page) ? $items->per_page : '10';
        return $perPage;
    }

}
