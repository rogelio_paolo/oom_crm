<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $table = 'system';

    protected $fillable = [
        'systemcode',
        'systemtype',
        'systemdesc',
        'activeflag'
    ];

    public $timestamps = false;

    public function role()
    {
        return $this->hasOne('App\Role','role','systemcode');
    }
}
