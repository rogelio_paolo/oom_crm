<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

class UpdateLastLoginAt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $date_today = Carbon::now()->format('Y-m-d');
        $last_login = Carbon::parse($event->user->last_login)->format('Y-m-d') ? Carbon::parse($event->user->last_login)->format('Y-m-d') : Carbon::now()->format('Y-m-d');

        $event->user->last_login = $event->user->current_login_at ? $event->user->current_login_at : Carbon::now();
        $event->user->current_login_at = Carbon::now();

        if($date_today > $last_login)
            $event->user->login_count_today = 1;
        else
            $event->user->login_count_today += 1;
        
        $event->user->save();
    }
}
