<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpportunityClosed extends Model
{
    protected $table = 'opportunity_closed';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function opportunity_info()
    {
        return $this->belongsTo('App\OpportunityInfo', 'opportunity_info_id', 'id');
    }

    public function opportunity()
    {
        return $this->belongsTo('App\Opportunity', 'opportunity_id', 'id');
    }
}
