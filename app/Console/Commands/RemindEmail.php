<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Reminder;
use App\Account;
use App\User;

class RemindEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notification before';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminder = Reminder::orderBy('id','desc')->first()->toArray();
        $account = Account::find($reminder['accountid']);
        $receivers = explode('|', $reminder['receivers']);
        $email = $this->receivers($receivers);

        foreach($email as $receiver) 
        {
            $emailDetails = [
                'name' => $reminder['name'],
                'date' => $reminder['date'], 
                'receiver' => $receiver->email,
                'remark' => nl2br($reminder['remark']),
                'recipient' => $receiver->UserInfo->f_name . ' ' . $receiver->UserInfo->l_name,
                'company' => $account->lead->company 
            ];

            $this->email($emailDetails);
        }
    }

    function email($emailDetails)
    {
        \Mail::send('layouts.emails.emailreminder', $emailDetails ,function($message) use ($emailDetails) {
            $message->to($emailDetails['receiver'])->subject
               ('CRM System - Reminder Notification - '. $emailDetails['name']);
            $message->from('noreply@oom.com.sg','CRM Admin');
            });
    }

    function receivers($email)
    {
        foreach($email as $row)
        {
            $user = User::where('email',$row)->first();
            $receivers[] = $user;
        }

        return $receivers;

    }
}
