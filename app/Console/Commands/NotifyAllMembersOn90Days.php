<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Account;
use App\User;

class NotifyAllMembersOn90Days extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snotify:90day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily to all account/contract members when reached 90 days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = Account::whereNull('delete_flag')->whereNotNull('contract_id')->take(1)->get();
        $emails = array();

        foreach ($accounts as $k => $row)
        {
            $collects[] = [
                'id'            => $row->id,
                'company'       => $row->contract->company,
                'contract_no'   => $row->contract->contract_number,
                'day_90'        => $row->contract->day_90->format('Y-m-d'),
                'day_120'       => $row->contract->day_120->format('Y-m-d'),
                'created'       => $row->contract->created_at->format('Y-m-d'),
                'sem'           => !empty($row->ppc_id)? $row->sem->sem_strategist_assigned : '',
                'sem_asst'      => $row->sem ? $row->sem->asst_sem_strategist_assigned : '',
                'seo'           => $row->seo ? $row->seo->seo_strategist_assigned : '',
                'sem_asst'      => $row->seo ? $row->seo->asst_seo_strategist_assigned : '',
                'seo_link'      => $row->seo ? $row->seo->seo_link_builder_assigned : '',
                'asst_seo_link' => $row->seo ? $row->seo->asst_seo_link_builder_assigned : '',
                'fb'            => $row->fb ? $row->fb->fb_strategist_assigned : '',
                'fb_asst'       => $row->fb ? $row->fb->asst_fb_strategist_assigned : '',
                'fb_link'       => $row->fb ? $row->fb->fb_link_builder_assigned : '',
                'asst_fb_link'  => $row->fb ? $row->fb->asst_fb_link_builder_assigned : '',
                'web'           => $row->web ? $row->web->dev_team : '',
                'acc'           => $row->acc_assigned ? $row->acc_assigned : '',
                'acc_asst'      => $row->acc_assistant ? $row->acc_assistant : '',
                'busi_manager'  => $row->created_id ? User::where('userid', $row->created_id)->first()->emp_id : ''
            ];
        }

        foreach($collects as $k => $v)
        {
            $filtered_members[] = [
                $this->getEmail($v['sem']),
                $this->getEmail($v['sem_asst']),
                $this->getEmail($v['seo']),
                $this->getEmail($v['sem_asst']),
                $this->getEmail($v['seo_link']),
                $this->getEmail($v['asst_seo_link']),
                $this->getEmail($v['fb']),
                $this->getEmail($v['fb_asst']),
                $this->getEmail($v['fb_link']),
                $this->getEmail($v['asst_fb_link']),
                $this->getEmail($v['web']),
                $this->getEmail($v['acc']),
                $this->getEmail($v['acc_asst']),
                $this->getEmail($v['busi_manager'])
            ];
        }

        foreach($filtered_members as $k => $row)
        {
            foreach($row as $key => $r)
            {
                if (empty($r)) 
                {
                    unset($row[$key]);
                }            
            }

            $emails[] = array_flatten($row);
        }

        foreach($collects as $row)
        {
            if (Carbon::now()->format('Y-m-d') == Carbon::parse($row['day_90'])->addDay()->format('Y-m-d'))
            {
                foreach($emails as $email)
                {
                    foreach($email as $row2)
                    {
                        $rowUser = User::where("email", $row2)->first();
                        $userData[] = $rowUser->userInfo->f_name . ' ' . $rowUser->userInfo->l_name;
                    }
                    
                    $member = implode(', ', $userData);
        
                    $emailDetails = [
                        'company' => $row['company'],
                        'created' => Carbon::parse($row['created'])->format('d M Y'),
                        'day_90' => Carbon::parse($row['day_90'])->format('d M Y'),
                        'day_120' => Carbon::parse($row['day_120'])->format('d M Y'),
                        'contract_no' => $row['contract_no'],
                        'member'  => $member,
                        'accountId' => $row['id']
                    ];

                    \Mail::send('layouts.emails.notifyallmembers', $emailDetails, function($message) use ($emailDetails, $email) {
                        $message->to($email)->subject('CRM - ' . $emailDetails['company'] . ' with Contract # ' . $emailDetails['contract_no'] . ' reached 90 days');
                        $message->from('noreply@oom.com.sg','CRM Admin');
                    });
                }
            }
        }

        $this->info('Email Sent successfully');
    }

    function getEmail($row)
    {
        $user = User::where('emp_id', '=', $row)->first();
        return $row != '' ? $user->email : '';

    }
}
