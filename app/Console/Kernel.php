<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Reminder;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // 'App\Console\Commands\RemindEmail'
        'App\Console\Commands\NotifyAllMembersOn90Days'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('notify:90days')
        ->daily();
        // $reminder = Reminder::orderBy('id','desc')->first();

        // if($reminder->time == 'RT001')
            // $schedule->command('reminder:email')->everyMinute();
        // else if ($reminder->time == 'RT002')
        //     $schedule->command('reminder:email')->cron('10 * * * *');
        // else if ($reminder->time == 'RT003')
        //     $schedule->command('reminder:email')->cron('0 1 * * *');
        // else if ($reminder->time == 'RT004')
        //     $schedule->command('reminder:email')->cron('0 1 * * *');
    }   

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
