<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class SalesRemark extends Model
{
    protected $table = 'sales_remarks';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['sales_id', 'remarks', 'created_by'];

    public function createdUser()
    {
        return $this->hasOne(User::class, 'userid', 'created_by');
    }
}
