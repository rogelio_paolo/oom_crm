<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSeo extends Model
{
    protected $table = 'accounts_seo';

    protected $fillable = [
        'seo_strategist_assigned',
        'asst_seo_strategist_assigned',
        'seo_link_builder_assigned',
        'asst_seo_link_builder_assigned',
        'seo_team',
        'seo_content_team',
        'seo_target_countries',
        'keyword_themes',
        'keyword_themes_other',
        'keyword_focus',
        'keyword_maintenance',
        'seo_campaign',
        'seo_duration',
        'seo_duration_interval',
        'seo_duration_other',
        'seo_currency',
        'seo_total_fee',
        'seo_hist_data',
        'seo_hist_data_fname',
        'currency'
    ];

    public $timestamps = false;
    
    public function market()
    {
        return $this->hasOne('App\Country','code','seo_target_countries');
    }
    
    public function keywords()
    {
        return $this->hasOne('App\System','systemcode','keyword_themes');
    }
    
    public function duration()
    {
        return $this->hasOne('App\System','systemcode','seo_duration');
    }

    public function duration_interval()
    {
        return $this->hasOne('App\System','systemcode','seo_duration_interval');
    }
}
