// Webpack configuration
module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader?cacheDirectory',
        include: [
          path.resolve(__dirname, '/resources/assets/js/components/opportunities/Index.vue'),
          /vue2-datatable-component/ // <-- add this!
        ]
      }
    ]
  }