<?php

return [

    'website_url' => 'Website URL',
    'contact_page_url' => 'Contact Page URL',
    'ty_page_url' => 'Thank You Page URL',
    'web_login' => 'Website CMS/FTP Login',
    'media_channel' => 'Media Channel',
    'campaign' => 'Campaign Brief & Expectations',
    'duration' => 'Campaign Duration',
    'duration_other' => 'Campaign Duration',
    'campaign_datefrom' => 'Campaign Start Date',
    'campaign_dateto' => 'Campaign End Date',
    'budget' => 'Total Campaign Budget',
    'google_monthly_budget' => 'Monthly Campaign Budget',
    'google_daily_budget' => 'Daily Campaign Budget',
    'fb_media_channel' => 'Media Channel',
    'fb_campaign' => 'Campaign Brief & Expectations',
    'fb_duration' => 'Campaign Duration',
    'fb_duration_other' => 'Campaign Duration',
    'fb_campaign_dt_from' => 'Campaign Start Date',
    'fb_campaign_dt_to' => 'Campaign End Date',
    'total_budget' => 'Total Campaign Budget',
    'monthly_budget' => 'Monthly Campaign Budget',
    'daily_budget' => 'Daily Campaign Budget',
    'age_group' => 'Age Group',
    'interest' => 'Interest',
    'keyword_focus' => 'Keyword Theme Focus',
    'seo_campaign' => 'Campaign Brief & Expectations',
    'seo_duration' => 'Campaign Duration',
    'seo_duration_other' => 'Campaign Duration',
];
