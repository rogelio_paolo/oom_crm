
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.config.devtools = true;
Vue.config.performance = true;

Vue.component('notification', require('./components/notifications/list.vue').default);
Vue.component('notifications-all', require('./components/notifications/all.vue').default);
Vue.component('create-reminder-form', require('./components/accounts/reminder/create.vue').default);
Vue.component('reminders', require('./components/accounts/reminder/notifications.vue').default);
Vue.component('reminder-details', require('./components/accounts/reminder/show.vue').default);

// NPM Packages
import Datatable from 'vue2-datatable-component';
import VModal from 'vue-js-modal';

// Components and Libraries
import OpportunityComponent from './components/opportunities/Index.vue';
import OpportunityProspect from './components/prospects/OpportunityProspect.vue';
import Multiselect from 'vue-multiselect';
import VeeValidate from 'vee-validate';
import VuePaginate from 'vue-paginate';

Vue.use(VuePaginate);
Vue.use(VModal, { dialog: true, dynamic: true, injectModalsContainer: true, dynamicDefaults: { clickToClose: false }});
Vue.use(Datatable);
Vue.use(VeeValidate, {
    classes: true
});

// register globally
Vue.component('multiselect', Multiselect)

if(document.URL.includes('/opportunity/list')) {
    new Vue({
        el: '#app-opportunity',
        components: { OpportunityComponent }
    });
}

if(document.URL.includes('/prospect/list')) {
    new Vue({
        el: '#app-opportunity-prospect-modal',
        components: { OpportunityProspect }
    });
}

const app = new Vue({
    el: '#app-notifications',
    data: {
        notifications: ''
    },
    methods : {
        notifyMe : function(title, link) {
            $.notify({
                title : title, message : '', url: link, target: '_self'
            },{
                mouse_over: 'pause', type: 'info',
                offset: {
                    x: 20,
                    y: 60
                },
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                }
            });
        },
        ReadCreated: function(notifications, url){
            var data = {
                id: notifications.id
            };

            $(document).on('click', 'a#notif-hover',function() {
                axios.post('/notification/read', data).then(response => {
                    window.location.href = url;
                });
            });
        },
        Dismiss: function(notification){
            var data = {
                id: notification.id
            };

            $(document).on('click', 'a#notif-hover',function() {
                axios.post('/notification/read', data).then(response => {
                    window.location.href = location.href;   
                });
            });
        }
    },
    created(){
        axios.post('/notification/fetch').then(response => {
            this.notifications = response.data;
        });

        var userid = $('meta[name="notification-sender"]').attr('content');
        
        Echo.private('App.User.' + userid).notification((notification) => {
            this.notifications.push(notification);

            var str = notification.type;

            var str_arr = str.split("\\");

            switch(str_arr[2]) {
                case "AccountAssignedPerPackageNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' assigned members in account ' + notification.data.lead.company +'</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id  + '/#' + notification.data.package ));
                    break;
                case "AccountDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted account ' + notification.data.prospect.lead.company + '</a>', this.Dismiss(notification));
                    break;
                case "AccountNoteCreatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' added a note in account ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id ));
                    break;
                case "AccountNoteDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted a note in account ' + notification.data.lead.company +'</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id ));
                    break;
                case "AccountNoteUpdatedNotification" : 
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated a note in account ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id ));
                    break;
                case "AccountUpdatedNotification" : 
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated account ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id ));
                    break;
                case "LeadConvertedProspectNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' converted lead '+ notification.data.lead.company + ' into prospect</a>', this.ReadCreated(notification, '/prospect/edit/' + notification.data.prospect.id ));
                    break;
                case "LeadCreatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' created lead ' + notification.data.lead.company +'</a>', this.ReadCreated(notification, '/lead/edit/' + notification.data.lead.id ));
                    break;
                case "LeadDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted lead ' + notification.data.lead.company +'</a>', this.Dismiss(notification));
                    break;
                case "LeadNoteCreatedNotification" : 
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' added a note in lead ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/lead/edit/' + notification.data.lead.id ));
                    break;
                case "LeadNoteDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted a note in lead ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/lead/edit/' + notification.data.lead.id ));
                    break;
                case "LeadNoteUpdatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated a note in lead ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/lead/edit/' + notification.data.lead.id ));
                    break;
                case  "LeadUpdatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated lead ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/lead/edit/' + notification.data.lead.id ));
                    break;
                case "ProspectConvertedAccountNotification" : 
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' converted prospect '+ notification.data.lead.company + ' into account</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id ));
                    break;
                case "ProspectUpdatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated prospect ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/prospect/edit/' + notification.data.prospect.id ));
                    break;
                case "ProspectDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted prospect ' + notification.data.lead.company +'</a>', this.Dismiss(notification));
                    break;
                case "ProspectNoteCreatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' added a note in prospect ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/prospect/edit/' + notification.data.prospect.id ));
                    break;
                case "ProspectNoteDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted a note in prospect ' + notification.data.lead.company +'</a>', this.ReadCreated(notification, '/prospect/edit/' + notification.data.prospect.id ));
                    break;
                case "ProspectNoteUpdatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated a note in prospect ' + notification.data.lead.company + '</a>', this.ReadCreated(notification, '/prospect/edit/' + notification.data.prospect.id ));
                    break;
                case "AppointmentCreatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' created appointment ' + notification.data.appointment.company + ' dated ' + notification.data.appointment.scheduled_date + '</a>', this.ReadCreated(notification, '/appointment/list/'));
                    break;
                case "AppointmentUpdatedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' updated appointment ' + notification.data.appointment.company + ' dated ' + notification.data.appointment.scheduled_date + '</a>', this.ReadCreated(notification, '/appointment/list/'));
                    break;
                case "ContractDeletedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' deleted Contract # ' + notification.data.contract.contract_number + ' on ' + notification.data.contract.company + '</a>', this.ReadCreated(notification, '/account/list/'));
                    break;
                case "NotifySalesPersonWhenAssignedNotification" :
                    this.notifyMe('<a id="notif-hover">' + notification.data.employee.f_name + ' ' + notification.data.employee.l_name + ' assigned ' + notification.data.members + ' on your account ' + notification.data.contract.company + ' with a Contract # ' + notification.data.contract.contract_number +'</a>', this.ReadCreated(notification, '/account/view/' + notification.data.account.id));
                    break;
            }
        });
    }
});

if(document.URL.includes('/notification/list')) {
    const notifications = new Vue({
        el: '#notification-list',
        data: {
            notifications: $('base').attr('href')
        },
        created(){
            axios.post('/notification/all').then(response => {
                this.notifications = response.data
            });
        }
    });
}


// const reminder = new Vue({
//     el: '#app-create-reminder'
// });

// const reminderNotifs = new Vue({
//     el: '#app-reminders',
//     data: {
//         reminders: ''
//     },
//     methods : {
//         notifyMe : function(title) {
//             $.notify({
//                 title : title, message : ''
//             },{
//                 mouse_over: 'pause', type: 'success',
//                 offset: {
//                     x: 20,
//                     y: 60
//                 },
//                 animate: {
//                     enter: 'animated fadeInRight',
//                     exit: 'animated fadeOutRight'
//                 }
//             });
//         },
        // ReadCreated: function(notifications, url){
        //     var data = {
        //         id: notifications.id
        //     };

        //     $(document).on('click', 'a#notif-hover',function() {
        //         axios.post('/notification/read', data).then(response => {
        //             window.location.href = url;
        //         });
        //     });
        // },
        // eventdate : function (date){
        //     return moment(date).format('lll');
        // },
    // },
    // created(){
    //     axios.post('/notification/fetch/reminders').then(response => {
    //         this.reminders = response.data
    //     });

    //     var userid = $('meta[name="notification-sender"]').attr('content');
        
    //     Echo.private('App.User.' + userid).reminder((reminder) => {
    //         this.reminders.push(reminder);
    //         var str = reminder.type;
            
    //         // DEVELOP 
    //         var str_arr = str.split("\\");

    //         if (str_arr[2] == 'AccountReminderNotification') 
    //             this.notifyMe('<a id="reminder-hover">Reminder ' + reminder.data.reminder.name + ' dated ' + reminder.data.reminder.date + '</a>');
    //     });
    // }
// });

// const reminderShow = new Vue({
//     el: '#app-reminder-details'
// });
