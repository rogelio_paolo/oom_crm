// if some dynamic components are used frequently, a better way is to register them globally
export default {
    ShowOpp : require('../includes/td-ShowOpportunities.vue').default,
    MoreOpp: require('../includes/nested-MoreOpportunities.vue').default,
    AddOpportunity: require('../AddOpportunity.vue').default
}