@extends('layouts.master_v2')
@section('title')
Crm - Edit Lead
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('main_content')
@include('includes.leadmodal')

{{--  <div class="back-btn">
    <a href="{{ route('lead.list') }}" class="btn btn-danger btn-button-rect">
        <span class="fa fa-chevron-circle-left"></span> Back to Leads List
    </a>
</div>  --}}

<div class="container pull-left">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('lead.list') }}">Leads</a></li>
                <li class="active">{{ $lead->company }} - Edit</li>
            </ol>
        </div>
    </div>
</div>
        
<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="has-lead-note">
    <form class="outer-data-box" method="POST" action="{{ route('lead.update') }}">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $lead->id }}" />
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Edit Lead</span>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="true">
                            Details
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Company *</label>
                                        <input id="company" type="text" class="form-control" name="company" value="{{ empty(old('company')) ? $lead->company : old('company') }}">
                                        @if ($errors->has('company'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('company') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="currency">Currency</label>
                                    <select class="form-control" id="currency" name="currency">
                                        <option value="USD" {{ $lead->currency == 'USD' ? 'selected' : '' }}>US Dollar</option>
                                        <option value="SGD" {{ $lead->currency == 'SGD' ? 'selected' : '' }}>Singapore Dollar</option>
                                        <option value="PHP" {{ $lead->currency == 'PHP' ? 'selected' : '' }}>Philippine Peso</option>
                                        <option value="MYR" {{ $lead->currency == 'MYR' ? 'selected' : '' }}>Malaysian Ringgit</option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Contract Value</label>
                                        <div class="input-group">
                                            <span id="display-currency" class="input-group-addon"></span>
                                            <input id="contract_value" type="text" class="form-control" name="contract_value" value="{{ empty(old('contract_value')) ? $lead->contract_value : old('contract_value') }}">
                                        </div>
                                       
                                        @if ($errors->has('contract_value'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_value') }}</span>
                                        @endif
                                    </div>
                                </div>

                             
                               
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>First Name *</label>
                                        <input id="f_name" type="text" class="form-control" name="f_name" value="{{ empty(old('f_name')) ? $lead->f_name : old('f_name') }}">
                                        @if ($errors->has('f_name'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('f_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Last Name</label>
                                        <input id="l_name" type="text" class="form-control" name="l_name" value="{{ empty(old('l_name')) ? $lead->l_name : old('l_name') }}">
                                        @if ($errors->has('l_name'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('l_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Designation</label>
                                        <input id="designation" type="text" class="form-control" name="designation" value="{{ empty(old('designation')) ? $lead->designation : old('designation') }}">
                                        @if ($errors->has('designation'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('designation') }}</span>
                                        @endif
                                    </div>
                                </div>
                             
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Email Address *</label>
                                        <input id="email" type="text" class="form-control" name="email" value="{{ empty(old('email')) ? json_encode(explode('|', $lead->email)) : old('email') }}">
                                        @if ($errors->has('email'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Mobile Number</label>
                                        <input id="contact_number" type="text" class="form-control" name="contact_number" value="{{ empty(old('contact_number')) ? $lead->contact_number : old('contact_number') }}">
                                        @if ($errors->has('contact_number'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contact_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Office Number</label>
                                        <input id="office_number" type="text" class="form-control" name="office_number" value="{{ empty(old('office_number')) ? $lead->office_number : old('office_number') }}">
                                        @if ($errors->has('office_number'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('office_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                             
                            </div>                                    
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Address</label>
                                        <textarea class="form-control" id="street_address" name="street_address" rows="5">{!! empty(old('street_address')) ? $lead->street_address : old('street_address') !!}</textarea>
                                        @if ($errors->has('street_address'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('street_address') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Postal Code</label>
                                        <input id="zip_code" type="text" class="form-control" name="zip_code" value="{{ empty(old('zip_code')) ? $lead->zip_code : old('zip_code') }}">
                                        @if ($errors->has('zip_code'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('zip_code') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>City/Country</label>
                                        <input id="city" type="text" class="form-control" name="city" value="{{ empty(old('city')) ? $lead->city : old('city') }}">
                                        @if ($errors->has('city'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('city') }}</span>
                                        @endif
                                    </div>
                                </div>
                              
                               
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Industry *</label>
                                        <div class="select-box">
                                            <select class="selectpicker form-control" name="industry" data-live-search="true">
                                                @foreach($industries as $industry)
                                                    <option value="{{ $industry->code }}" {{ $lead->industry == $industry->code ? 'selected' : ''}}>{{ $industry->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('industry'))
                                                <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('industry') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Website URL</label>
                                        <input id="website" type="text" class="form-control" name="website" value="{{ empty(old('website')) ? $lead->website : old('website') }}">
                                        @if ($errors->has('website'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('website') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Services *</label>
                                        <div class="select-box">
                                            <select class="multiselect-ui form-control elipsis-text" id="services" name="services[]" multiple="multiple">
                                            @php $selected = explode('|',$lead->services)@endphp
                                            @foreach($services as $row)
                                            <option value="{{ $row->systemcode }}" {{ in_array($row->systemcode,$selected) ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                            @if ($row->systemcode == 'SV999')
                                            <input class="form-control {{ !empty(old('services')) && in_array('SV999',explode(',',old('services'))) ? '' : 'hidden' }} " type="text" id="service_other" name="service_other" value="{{ $lead->service_other }}" />
                                            @endif
                                            @endforeach
                                            </select>
                                            <input type="hidden" id="services_null" name="services" />
                                            <input type="hidden" id="services_array" name="services" value="{{ $lead->services }}"/>
                                            @if ($errors->has('services'))
                                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('services') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                             
                               
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Other Services</label>
                                        <input id="other_services" type="text" class="form-control" name="other_services" value="{{ empty(old('other_services')) ? $lead->other_services : old('other_services') }}">
                                        @if ($errors->has('other_services'))
                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('other_services') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Source of Leads *</label>
                                        <div class="select-box">
                                            <select class="selectpicker form-control" id="sources" name="source" data-live-search="true">
                                            @foreach($sources as $row)
                                            <option value="{{ $row->systemcode }}" {{ !empty(old('source')) && old('source') == $row->systemcode ? 'selected' : ($row->systemcode == $lead->source ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                            @if ($row->systemcode == 'SL999')
                                            <input class="form-control {{ !empty(old('source')) && old('source_other') == 'SL999' || !empty($lead->source) && $lead->source == 'SL999' ? '' : 'hidden' }} " type="text" id="source_other" name="source_other" value="{{ !empty(old('source_other')) ? old('source_other') : $lead->source_other }}" />
                                            @endif
                                            @endforeach
                                            </select>
                                            @if ($errors->has('source'))
                                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('source') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="landing-page {{ !empty(old('source')) && old('source') == 'SL002' ? 'hidden' : (!empty($lead->source) ? ($lead->source == 'SL002' ? 'hidden' : '' ) : '') }}">
                                        <div class="form-box">
                                            <label>Landing Page *</label>
                                            <div class="select-box">
                                                <select class="selectpicker form-control" id="landings" name="landing_page" data-live-search="true">
                                                @foreach($landingpages as $row)
                                                <option value="{{ $row->systemcode }}" {{ !empty(old('landing')) && old('landing_page') == $row->systemcode ? 'selected' : ($row->systemcode == $lead->landing_page ? 'selected' : '')}}>{{ $row->systemdesc }}</option>
                                                @if ($row->systemcode == 'LP999')
                                                <input class="form-control {{ !empty(old('landing')) && in_array('SV999',old('landing')) ? '' : 'hidden' }} " type="text" id="landing_other" name="landing_other" value="{{ old('landing_other') }}" />
                                                @endif
                                                @endforeach
                                                </select>
                                                @if ($errors->has('landing_page'))
                                                <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('landing_page') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Status</label>
                                        <div class="select-box">
                                            <select class="selectpicker form-control" name="status" data-live-search="true">
                                            @foreach($statuses as $row)
                                            @if ($row->systemdesc == 'Closed' || $row->systemdesc == 'Open')
                                            @continue
                                            @else
                                            <option value="{{ $row->systemcode }}" {{ $lead->status == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                            @endif
                                            @endforeach
                                            </select>
                                            @if ($errors->has('status'))
                                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('status') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label for="lock_this_lead">Lock this lead?</label>
                                        <div class="select-box">
                                            <select class="selectpicker form-control" name="lock_this_lead">
                                                    @if(empty($lockedin))
                                                        <option value="1">Yes</option>
                                                        <option value="0" selected>No</option>
                                                    @else
                                                        <option value="1" {{ $lead->lock_this_lead == 1 ? 'selected' : '' }}>Yes</option>
                                                        <option value="0" {{ $lead->lock_this_lead != 1 ? 'selected' : '' }}>No</option>
                                                    @endif
                                            </select>
                                        </div>
                                        <p class="error-p lead">Remaining Lockable Leads/Prospects: {{ empty($lockedin) ? '10' : $lockedin->leadprospect_to_unlock }}/10</p>
                                    </div>
                                </div>

                                @if ($user->role === 1)
                                    <div class="col-md-4">
                                        <div class="form-box">
                                            <label>Change Owner?</label>
                                            <div class="select-box">
                                                <select class="selectpicker form-control" id="created_by" name="created_by" data-live-search="true">
                                                    @foreach($salesPersons as $row)
                                                        <option value="{{ $row->userid }}" {{ $row->userid === $lead->created_by ? 'selected' : '' }}>{{ $row->f_name.' '.$row->l_name }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('created_by'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('created_by') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="bottom-buttons clearfix">
                        <div class="buttons-box">
                            <button type="submit" class="cta-btn add-btn" id="btn-update-lead" >Update Lead</button>
                            <a href="{{ route('lead.list') }}" class="cta-btn cancel-btn">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form class="outer-data-box" method="post" action="{{ route('lead.note.create') }}">
        <i id="loader" class="loader leadnotes hidden"></i>
        {{ csrf_field() }}
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Lead Notes</span>
                <div class="data-search-box">
                    Filter By:
                    <div class="select-box">
                        <select class="form-control" id="filterLeadNotes" name="filterLeadNotes">
                            <option value="all" selected>All</option>
                            <option value="date">Date</option>
                            <option value="commentor">Commentor</option>
                        </select>
                    </div>
                    <div id="filterButtons" class="form-inline pull-right graph-filter">
                        <button class="btn btn-default" id="FilterNotesAll"> FILTER </button>
                    </div>
                    <div id="filter-by-date" class="form-inline pull-right graph-filter hidden">
                    From <input type="text" class="form-control datepicker" id="filterDateFrom" name="filterDateFrom" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    To <input type="text" class="form-control datepicker" id="filterDateTo" name="filterDateTo" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    <button class="btn btn-default" id="FilterNotesByDate"> FILTER </button>
                    </div>
                    <div id="filter-by-commentor" class="form-inline pull-right graph-filter hidden">
                        <div class="select-box">
                            <select class="form-control" id="lead-commentors" name="filterNotes">
                                @foreach($commentors as $row)
                                    <option value="{{ $row->userid }}">{{ $row->user->UserInfo->f_name.' '.$row->user->UserInfo->l_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-default" id="FilterNotesByCommentor"> FILTER </button>
                    </div>
                </div>
            </div>
            <div class="lead-notes">
                        
                    @forelse($notes as $row)
                        <div class="lead-note">
                            <span> 
                                <p>{{ $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name }}</p> 
                                <p>{{ date_format($row->created_at,'Y-m-d g:i A') }}</p>
                            </span>
                            
                            @if ($row->userid == $user->userid)
                            <span class="pull-right">
                            <a id="edit-note" data-editnote="{{ $row->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            <a id="delete-note" data-delnote="{{ $row->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                            </span>
                            @endif

                            <div class="note-wrapper">
                                <span id="lead-note-{{ $row->id }}">{!! html_entity_decode($row->note) !!}</span>
                            </div>
                        </div>
                        <hr>
                    @empty
                        <div class="text-center">No Lead notes to be shown</div>
                    @endforelse
                    
            </div>
            <div class="pagination-box">
                {{ $notes->render() }}
            </div>

            <input type="hidden" id="leadid" name="leadid" value="{{ Request::segment(3) }}" />
            <textarea class="form-control" name="addnote" id="addnote" rows="10" placeholder="Leave your comments here"></textarea>
            @if($errors->has('addnote'))
            <span class="note error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('addnote') }}</span>
            @endif
            <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>
            <div class="form-group">
                <div class="text-right">
                    <button type="submit" class="cta-btn add-btn">
                        <i class="fa fa-save"></i> Add Note
                    </button>
                    <button type="button" id="clearNote" class="cta-btn cancel-btn">
                        <i class="fa fa-refresh"></i> Clear
                    </button>
                </div>
            </div>

        </div>
    </form>

    <div class="container pull-left">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <ol class="breadcrumb bg-none" >
                    <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                    <li><a href="{{ route('lead.list') }}">Leads</a></li>
                    <li class="active">{{ $lead->company }} - Edit</li>
                </ol>
            </div>
        </div>
    </div>

</div>

<div class="clearfix" style="margin-bottom: 1%">&nbsp;</div>

@section('scripts')
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script src="{{ asset('js/lead/create.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/lead/notes.js') }}" type="text/javascript"></script>
@endsection
@endsection