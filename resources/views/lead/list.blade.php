@extends('layouts.master_v2')

@section('title')
  Crm - Leads
@endsection

@section('modals')
@include('includes.leadmodal')
@endsection

@section('main_content')


<div class="outer-data-box">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-tags" aria-hidden="true"></i> Leads </span>

      <div class="data-search-box">

        <span>
          Search Lead:
          <div class="search-box">
            <input type="text" class="form-control search-area" name="searchLead" id="searchLead" placeholder="Search" onkeypress="triggerSearch(event)">
            <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </span>

      </div>

    </div>

    <div class="data-buttons-box clearfix">

      <div class="filter-box">

        <!-- <a href="#"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>

        <a href="#"><i class="fa fa-th" aria-hidden="true"></i> Group by</a>

        <a href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i> Sort by</a> -->

        <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>

        <span class="remaining-span">Remaining lockable Leads/Prospects: {{ !empty($lockedin) ? $lockedin->leadprospect_to_unlock : '10' }}/10</span>

      </div>

      <div class="buttons-box">

        <a href="{{ route('lead.create') }}" class="cta-btn add-btn">ADD NEW <i class="fa fa-plus" aria-hidden="true"></i></a>
        
      </div>

    </div>

    <div class="table-box">

      <i id="loader" class="loader hidden"></i>

      <table class="table main-table" id="lead-management">

        <thead>
            <tr>
                <th class="text-center">Name <a href="" class="sort-lead" data-sortby="f_name"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th class="text-center">Contact No </th>
                <th class="text-center">Company <a href="" class="sort-lead" data-sortby="company"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th class="text-center">Status <a href="" class="sort-lead" data-sortby="status"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th class="text-center">Created <a href="" class="sort-lead active" data-sortby="created_at"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                @if($user->role == 1)
                <th class="text-center">Owner <a href="" class="sort-lead" data-sortby="owner"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                @endif
                <th class="text-center">Lead locked? <a href="" class="sort-lead" data-sortby="lock_this_lead"><i id="sort-lead" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th class="text-center">Action </th>
            </tr>
        </thead>
        <tbody>
           @if(count($data) > 0)
                @foreach($data as $row)
                <tr>
                    <td class="text-center">{{ !empty($row->f_name) ? $row->f_name.' '.$row->l_name : 'Not specified' }}</td>
                    <td class="text-center">{{ !empty($row->contact_number) ? $row->contact_number : 'Not Specified' }}</td>
                    <td class="text-center">{{ $row->company }}</td>
                    <td class="text-center">{{ $row->leadStatus->systemdesc }}</td>
                    <td class="text-center">{{ date('d M Y', strtotime($row->updated_at)) }}</td>
                    @if($user->role == 1)
                    <td class="text-center">{{ !empty($row->sales->UserInfo) ? $row->sales->UserInfo->f_name.' '.$row->sales->UserInfo->l_name : 'None' }}</td>
                    @endif
                    <td class="text-center">{{ $row->lock_this_lead == 1 ? 'Yes' : 'No' }}</td>
                    <td class="text-center text-nowrap" id="lead-management">
                      @if($row->status != 'LS005')
                          <a href="{{ route('lead.view',$row->id) }}"  class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                          <a href="{{ route('lead.edit',$row->id) }}"  class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>

                          @if($user->role == 1 || in_array($user->userInfo->team->team_code,['acc']) || $row['created_by'] == $user->userid) 
                            <a id="leadDelete" data-leadid="{{ $row->id }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                          @endif

                          <a href="{{ route('lead.convert.prospect',$row->id) }}"  class="btn btn-primary btn-sm"><i class="fa fa-sign-out"></i> Convert</a>
                      @else
                          No actions
                      @endif
                    </td>
                </tr>
                @endforeach
           @else
               <tr><td align="center" colspan="9">No leads available</td></tr>
           @endif
        </tbody>

      </table>

      <div class="row">
          <div class="col-md-1">
              <label for="limit_entry">Items per page: </label>
          </div>
          <div class="col-md-2">
              <div class="select-box">
                  <select id="limit_entry" name="limit_entry" class="w-150">
                      <option value="10" {{ !empty($items) ? $items->per_page == 10 ? 'selected' : '' : '' }}>10</option>
                      <option value="20" {{ !empty($items) ? $items->per_page == 20 ? 'selected' : '' : '' }}>20</option>
                      <option value="30" {{ !empty($items) ? $items->per_page == 30 ? 'selected' : '' : '' }}>30</option>
                      <option value="40" {{ !empty($items) ? $items->per_page == 40 ? 'selected' : '' : '' }}>40</option>
                  </select>
              </div>
          </div>
      </div>

    </div>

    <div class="pagination-box">
      {{ $data->render() }}
    </div>

  </div>

</div>

@section('scripts')
    <script src="{{ asset('js/lead/list.js') }}" type="text/javascript"></script>
@endsection

@endsection
