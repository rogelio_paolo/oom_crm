@extends('layouts.master_v2')

@section('title')
  Crm - Create Lead
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('fonts_v1/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css_v1/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
@endsection

@section('main_content')

<div class="container pull-left">
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>

                <li><a href="{{ route('lead.list') }}">Leads</a></li>

                <li class="active">Create Lead</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<form class="outer-data-box" method="POST" action="{{ route('lead.create') }}">
{{ csrf_field() }}
<input type="hidden" name="created_by" value="{{ Auth::user()->userid }}" />
<div class="row">

<div class="col-lg-8 col-lg-offset-2">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-plus" aria-hidden="true"></i> Create Lead</span>

    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

      <div class="panel panel-default">

        <div class="panel-heading" role="tab">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="true">
              Details
            </a>
          </h4>
        </div>

        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">

          <div class="panel-body">

            <div class="row">

              <div class="col-md-6">

                <div class="form-box">
                    <label>Company *</label>
                    <input id="company" type="text" class="form-control" name="company" value="{{ old('company') }}">

                    @if ($errors->has('company'))
                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('company') }}</span>
                    @endif
                </div>

              </div>

              <div class="col-md-6">

                  <div class="form-box">
                      <label>Currency</label>
                      <select class="form-control" id="currency" name="currency">
                          <option value="USD" {{ !empty(old('currency')) ? (old('currency') == 'USD' ? 'selected' : '') : '' }}>US Dollar</option>
                          <option value="SGD" {{ empty(old('currency')) ? 'selected' : (old('currency') == 'SGD' ? 'selected' : '') }}>Singapore Dollar</option>
                          <option value="PHP" {{ !empty(old('currency')) ? (old('currency') == 'PHP' ? 'selected' : '') : '' }}>Philippine Peso</option>
                          <option value="MYR" {{ !empty(old('currency')) ? (old('currency') == 'MYR' ? 'selected' : '') : '' }}>Malaysian Ringgit</option>
                      </select>
  
                      @if ($errors->has('currency'))   
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('currency') }}</span>
                      @endif
                  </div>
                </div>

            
          </div>

          <div class="row">
              <div class="col-md-6">
                  <div class="form-box">
                      <label>Contract Value</label>
                      <div class="input-group">
                          <span id="display-currency" class="input-group-addon">SGD</span>
                          <input id="contract_value" type="text" class="form-control" name="contract_value" value="{{ old('contract_value') }}" onkeypress="return isNumberKey(event);">
                      </div>
                      
                      @if ($errors->has('contract_value'))
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_value') }}</span>
                      @endif
                  </div>
              </div>

              <div class="col-md-6">

                <div class="form-box">
                  <label>First Name *</label>
                  <input id="f_name" type="text" class="form-control" name="f_name" value="{{ old('f_name') }}">

                  @if ($errors->has('f_name'))
                      <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('f_name') }}</span>
                  @endif
                </div>

              </div>

           
          </div>

          <div class="row">
              <div class="col-md-6">

                <div class="form-box">
                  <label>Last Name</label>
                  <input id="l_name" type="text" class="form-control" name="l_name" value="{{ old('l_name') }}">

                  @if ($errors->has('l_name'))
                      <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('l_name') }}</span>
                  @endif
                </div>

              </div>
    
              <div class="col-md-6">

                <div class="form-box">
                    <label>Designation</label>
                    <input id="designation" type="text" class="form-control" name="designation" value="{{ old('designation') }}">

                    @if ($errors->has('designation'))
                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('designation') }}</span>
                    @endif
                </div>

              </div>
            </div>

            <div class="row">

                <div class="col-md-6">

                  <div class="form-box">
                      <label>Email Address *</label>
                      <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
  
                      @if ($errors->has('email'))
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                      @endif
                  </div>
  
                </div>

              <div class="col-md-6">

                <div class="form-box">
                    <label>Mobile Number</label>
                    <input id="contact_number" type="text" class="form-control" name="contact_number" value="{{ old('contact_number') }}">

                    @if ($errors->has('contact_number'))
                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contact_number') }}</span>
                    @endif
                </div>

              </div>

           

            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="form-box">
                        <label>Office Number</label>
                        <input id="office_number" type="text" class="form-control" name="office_number" value="{{ old('office_number') }}">
    
                        @if ($errors->has('office_number'))
                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('office_number') }}</span>
                        @endif
                    </div>
    
                  </div>
             

              <div class="col-md-6">

                <div class="form-box">
                  <label>Address</label>
                  <textarea name="street_address" class="form-control" id="street_address" rows="5">{{ old('street_address') }}</textarea>

                  @if ($errors->has('street_address'))
                      <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('street_address') }}</span>
                  @endif
                </div>

              </div>


            </div>

            <div class="row">

                <div class="col-md-6">
                  <div class="form-box">
                      <label>Postal Code</label>
                      <input id="zip_code" type="text" class="form-control" name="zip_code" value="{{ old('zip_code') }}">
  
                      @if ($errors->has('zip_code'))
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('zip_code') }}</span>
                      @endif
                  </div>
                </div>

              <div class="col-md-6">

                <div class="form-box">
                    <label>City/Country</label>
                    <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">

                    @if ($errors->has('city'))
                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('city') }}</span>
                    @endif
                </div>

              </div>

           

            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-box">
                        <label>Industry *</label>
                        <div class="select-box">
                        <select class="selectpicker form-control" id="industry" name="industry" data-live-search="true">
                            @foreach($industries as $industry)
                                <option value="{{ $industry->code }}" {{ !empty(old('industry')) && old('industry') == $industry->code ? 'selected' : ''}}>{{ $industry->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('industry'))
                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('industry') }}</span>
                        @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">

                  <div class="form-box">
                      <label>Website URL</label>
                      <input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}">
  
                      @if ($errors->has('website'))
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('website') }}</span>
                      @endif
                  </div>
  
                </div>

            

             

            </div>

           
            <div class="row">
                <div class="col-md-6">

                    <div class="form-box">
                      <label>Services</label>
                      <div class="select-box">
                        <select class="multiselect-ui form-control elipsis-text" id="services" name="services[]" multiple="multiple">
                            @foreach($services as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('services')) && in_array($row->systemcode,explode(',',old('services'))) ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                @if ($row->systemcode == 'SV999')
                                    <input class="form-control {{ !empty(old('services')) && in_array('SV999',explode(',',old('services'))) ? '' : 'hidden' }} " type="text" id="service_other" name="service_other" value="{{ old('service_other') }}" />
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" id="services_null" name="services" />
                        <input type="hidden" id="services_array" name="services" value="{{ old('services') }}" />
                        @if ($errors->has('services'))
                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('services') }}</span>
                        @endif
                      </div>
                  </div>
    
                </div>
    

                <div class="col-md-6">

                  <div class="form-box">
                      <label>Other Services</label>
                      
                      <input id="other_services" type="text" class="form-control" name="other_services" value="{{ old('other_services') }}">
  
                      @if ($errors->has('other_services'))
                          <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('other_services') }}</span>
                      @endif
                  </div>
                  
                </div>
        
             

             

            </div>

            <div class="row">
                <div class="col-md-6">
                  <div class="form-box">
                      <label>Source of Leads</label>
                      <div class="select-box">
                        <select class="selectpicker form-control" id="sources" name="source" data-live-search="true">
                            @foreach($sources as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('source')) && old('source') == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                @if ($row->systemcode == 'SL999')
                                    <input class="form-control {{ !empty(old('source')) && old('source') == 'SL999' ? '' : 'hidden' }} " type="text" id="source_other" name="source_other" value="{{ old('source_other') }}" />
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('source'))
                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('source') }}</span>
                        @endif
                      </div>
                  </div>
                </div>


              <div class="col-md-6">
                <div class="landing-page {{ !empty(old('source')) ? (old('source') == 'SL002' ? 'hidden' : '') : '' }}">
                    <div class="form-box">
                        <label>Landing Page</label>
                        <div class="select-box">
                        <select class="selectpicker form-control" id="landings" name="landing_page" data-live-search="true">
                            @foreach($landingpages as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('landing')) && old('landing_page') == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                @if ($row->systemcode === 'LP999')
                                    <input class="form-control in-other {{ !empty(old('landing')) && in_array('LP999',old('landing')) ? '' : 'hidden' }} " type="text" id="landing_other" name="landing_other" value="{{ old('landing_other') }}" />
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('landing_page'))
                            <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('landing_page') }}</span>
                        @endif
                        </div>
                    </div>
                </div>
              </div>

              
            
            </div>

            <div class="row">

                <div class="col-md-6">

                    <div class="form-box">
                        <label>Status</label>
                        <div class="select-box">
                            <select class="selectpicker form-control" name="status" data-live-search="true">
                                @foreach($statuses as $row)
                                    @if ($row->systemdesc == 'Closed' || $row->systemdesc == 'Open')
                                        @continue
                                    @else
                                        <option value="{{ $row->systemcode }}" {{ empty(old('status')) && $row->systemcode == 'LS002' ? 'selected' : (old('status') == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('status'))
                                <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('status') }}</span>
                            @endif
                          </div>
                      </div>
                    </div> 

            </div>


          </div>

        </div>


    <div class="bottom-buttons clearfix">

      <div class="buttons-box">

        <button id="submit_lead" type="submit" class="cta-btn add-btn">Create Lead</button>

        <a href="{{ route('lead.list') }}" class="cta-btn cancel-btn">Cancel</a>

      </div>

    </div>

</form>

<div class="loader-overlay hidden"></div>


@section('scripts')
    <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>

    <script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
    </script>

    <script src="{{ asset('js/lead/create.js') }}" type="text/javascript"></script>
@endsection

@endsection
