
    <!-- ------- ADD APPOINTMENT MODAL ------- -->
    <div class="modal fade" id="add_appointment_model" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="loader-overlay hidden"></div>  
        <div class="modal-dialog" role="document">
            <div class="modal-content">

    <div class="row">
         
        <div class="col-md-12" style="width:500px">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Appointment</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>                                

             <div class="form-group">
                <p><em><strong>Note:</strong> Status automatically set to Pending</em></p>
            </div>   
            <br />     

            <div class="alert alert-danger hidden" id="error-appointment-message" role="alert">Please fill up required fields!</div>

            <div class="form-group" data-tip="To take effect immediately, click anywhere around calendar after date was selected.">
                <label for="datetimes">Scheduled Dates <span class="error-msg">*</span></label>
                <input type="text" id="datetimes" name="datetimes" class="form-control" autocomplete="off" />
            </div>

            <div class="form-group">
                <label for="guest">Guest Email</label>
                <select multiple name="guest[]" id="guest">
                    @foreach($emails as $row)
                        <option value="{{ $row->email }}">{{ $row->f_name.' '.$row->l_name }}</option>
                    @endforeach
                </select>  
            </div>

            <div class="form-group">
                <label for="color">Color</label>
                <div class="panel panel-default">
                    @foreach($event_colors as $color)
                        <input type="radio" name="color" value="{{ $color->id }}" {{ $color->id == '1' ? 'checked' : '' }} />
                    @endforeach
                </div>
            </div>

            <div class="alert alert-success alert-dismissible hidden" id="msg-quick-add"></div>

            <div class="form-group">

                <label for="company">Company  <span class="error-msg">*</span></label>

                <div class="select-box">
                    <select class="selectpicker form-control" id="company" name="company" data-live-search="true" placeholder="Select a Company">
                        <option value="" selected disabled hidden>Select a Company</option>
                        @foreach($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->company }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea cols="30" rows="5" name="description" id="description" class="form-control" ></textarea>
            </div>
                
            <div class="bottom-buttons clearfix">
                <div class="buttons-box">
                    <button type="button" id="btnCreateAppointment" class="btn btn-primary">Create Appointment</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div> <!-- div.col-md-12 -->
    </div> <!-- div.row -->

            </div> <!-- div.modal-content -->
        </div> <!-- div.modal-dialog -->
    </div> <!-- div.modal-fade -->


    <!-- -------- EDIT APPOINTMENT MODAL --------------- -->
    <div class="modal fade" tabindex="-1" role="dialog" id="update_appointment_model" data-keyboard="false" data-backdrop="static">
        <div class="loader-overlay hidden"></div>  
        <div class="modal-dialog" role="document">
            <div class="modal-content">
        <div class="row">
            <div class="col-md-12" style="width:500px">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Edit Appointment</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>              
                <br />    
                
                <div class="alert alert-danger hidden" id="error-update-appointment-message" role="alert">Please fill up required fields!</div>

                <div class="form-group" data-tip="To take effect immediately, click anywhere around calendar after date was selected.">
                    <label for="update_datetimes">Scheduled Date</label>
                    <input type="text" id="update_datetimes" name="update_datetimes" class="form-control" autocomplete="off"/>
                </div>

                <div class="form-group">
                    <label for="update_company">Company</label>
                    <input type="text" name="update_company" id="update_company" class="form-control" readonly="true"/>
                </div>


                <div class="form-group">
                    <label for="update_company">Status</label>
                    <div class="select-box">
                        <select class="selectpicker form-control" id="update_status" name="update_status" data-keyboard="false" data-backdrop="static">
                            @foreach($statuses as $status)
                                <option value="{{ $status->systemcode }}">{{ $status->systemdesc }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color">Color</label>
                    <div class="panel panel-default">
                        @foreach($event_colors as $color)
                            <input type="radio" name="update_color" value="{{ $color->id }}" {{ $color->id == '1' ? 'checked' : '' }} />
                        @endforeach
                    </div>
                </div>
                

                <div class="form-group">
                    <label for="update_description">Description</label>
                    <textarea cols="30" rows="5" name="update_description" id="update_description" class="form-control" ></textarea>
                </div>

                <div class="form-group">
                    <label for="update_client">Contact Person</label>
                    <input type="text" name="update_client" id="update_client" class="form-control" />
                </div>

                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="button" id="btnUpdateAppointment" class="btn btn-success">Update Appointment</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div> <!-- div.col-md-12 -->
        </div> <!-- div.row -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- -------- QUICK ADD LEADS --------------- -->
    <div class="modal fade" tabindex="-1" role="dialog" id="quick_add_leads" data-keyboard="false" data-backdrop="static">
            <div class="loader-overlay hidden"></div>  
            <div class="modal-dialog" role="document">
                <div class="modal-content">
            <div class="row">
                <div class="col-md-12" style="width:680px">
                    <div class="data-header-box clearfix">
                        <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Quick Lead Creation</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>              
                    <br />    
                    
                    <div class="alert alert-danger hidden" id="error-create-lead-message" role="alert">Please fill up valid required fields !</div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lead_company">Company Name <span class="error-msg">*</span></label>
                                <input type="text" name="lead_company" id="lead_company" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lead_client_name">Client Name</label>
                                <input type="text" name="lead_client_name" id="lead_client_name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lead_contact">Contact Number</label>
                                <input type="number" name="lead_contact" id="lead_contact" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lead_email">Email Address <span class="error-msg">*</span></label>
                                <input type="email" name="lead_email" id="lead_email" class="form-control" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapse-999" aria-expanded="false" class="collapsed">Other Details</a>
                                </h4>
                            </div>
                    
                            <div id="collapse-999" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_designation">Designation</label>
                                                <input type="text" name="lead_designation" id="lead_designation" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_office_number">Office Number</label>
                                                <input type="number" name="lead_office_number" id="lead_office_number" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="lead_address">Address</label>
                                                <textarea name="lead_address" id="lead_address" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_postal">Postal Code</label>
                                                <input type="text" name="lead_postal" id="lead_postal" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_city">City/Country</label>
                                                <input type="text" name="lead_city" id="lead_city" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_website">Website URL</label>
                                                <input type="text" name="lead_website" id="lead_website" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_services">Services</label>
                                                <div class="select-box">
                                                    <select name="lead_services[]" id="lead_services" class="multiselect-ui elipsis-text form-control" multiple="multiple">
                                                        @foreach($lead_services as $lead_service)
                                                            <option value="{{ $lead_service->systemcode }}">{{ $lead_service->systemdesc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_other_services">Other Services</label>
                                                <input type="text" name="lead_other_services" id="lead_other_services" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_source">Source of Leads</label>
                                                <div class="select-box">
                                                    <select name="lead_source" id="lead_source" class="form-control" data-live-search="true">
                                                        @foreach($lead_sources as $lead_source)
                                                            <option value="{{ $lead_source->systemcode }}">{{ $lead_source->systemdesc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_landing">Landing Page</label>
                                                <div class="select-box">
                                                    <select name="lead_landing" id="lead_landing" class="form-control" data-live-search="true">
                                                        @foreach($lead_landingpages as $lead_landingpage)
                                                            <option value="{{ $lead_landingpage->systemcode }}">{{ $lead_landingpage->systemdesc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lead_status">Status</label>
                                                <div class="select-box">
                                                    <select name="lead_status" id="lead_status" class="form-control" data-live-search="true">
                                                        @foreach($lead_statuses as $lead_status)
                                                            <option {{ $lead_status->systemcode == 'LS002' ? 'selected' : '' }} value="{{ $lead_status->systemcode }}">{{ $lead_status->systemdesc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
    
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="button" id="btnCreateLead" class="btn btn-success">Create Lead</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button> 
                    </div>
                </div>
            </div> <!-- div.col-md-12 -->
        </div> <!-- div.row -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>