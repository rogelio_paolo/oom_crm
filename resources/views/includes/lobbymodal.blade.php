<div class="modal fade" id="modifyColumn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Modify Columns</h4>
            </div>
            <div class="modal-body">
              <form id="form_modify">
                <div class="row">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="company_name" checked> Company Name</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="name" checked> Client Name</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="contact" checked> Client Number</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="email" checked> Client Email</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="category" checked> Category</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="ex_client"  checked> Ex-Client</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="sales"  checked> Business Manager</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="holder"  checked> Account Manager</label>
                        </div>
                        <div class="checkbox">
                            <label><input class="checkboxes" type="checkbox" name="status"  checked> Status</label>
                        </div>
                    </div>
                </div> 
            </div>
        <i id="loader-modal" class="loader hidden"></i>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="btnApply" onclick="modify();">Apply changes</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
          </form>
          </div>
        </div>
      </div>
      </div>
      
    </div>
    