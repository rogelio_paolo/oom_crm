<div class="modal fade" id="deleteEmployeeModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('employee.delete') }}">
                {{ csrf_field() }}
                <input type="hidden" name="empid" id="delete-employee" />
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-eraser" aria-hidden="true"></i> Delete Employee</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group">
                    <h3 class="text-center">Are you sure you want to delete this user?</h3>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
