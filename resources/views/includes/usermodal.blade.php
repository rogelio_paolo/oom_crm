<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center">Add User</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('user.management.create') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userAdd' && $errors->has('emp_id') ? ' has-error' : '' }}">
                                    <label for="emp_id">Employee ID:</label>
                                    <input type="text" class="form-control" name="emp_id" value="{{ Session::get('error_type') == 'userAdd' ? old('emp_id') : '' }}" />
                                    @if(Session::get('error_type') == 'userAdd' && $errors->has('emp_id'))
                                        <span class="help-block">
                                            {{ $errors->first('emp_id') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userAdd' && $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username">Username:</label>
                                    <input type="text" class="form-control" name="username" value="{{ Session::get('error_type') == 'userAdd' ? old('username') : '' }}" />
                                    @if(Session::get('error_type') == 'userAdd' && $errors->has('username'))
                                        <span class="help-block">
                                            {{ $errors->first('username') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userAdd' && $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control" name="email" value="{{ Session::get('error_type') == 'userAdd' ? old('email') : '' }}" />
                                    @if(Session::get('error_type') == 'userAdd' && $errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                                    <label for="role">Role:</label>
                                    <select class="form-control" name="role">
                                        <option value="role">Select a role</option>
                                        @foreach($roles as $row)
                                            <option value="{{ $row->id }}" {{ old('role') == $row->id ? 'selected' : '' }}> {{ $row->syscode->systemdesc }} </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('role'))
                                        <span class="help-block">
                                            {{ $errors->first('role') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Add User</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center">Edit User Information</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('user.management.update') }}">
                {{ csrf_field() }}
                <input type="hidden" name="userid" id="userid" />
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userAdd' && $errors->has('emp_id') ? ' has-error' : '' }}">
                                    <label for="emp_id">Employee ID:</label>
                                    <input type="text" class="form-control" name="emp_id" value="{{ Session::get('error_type') == 'userAdd' ? old('emp_id') : '' }}" />
                                    @if(Session::get('error_type') == 'userAdd' && $errors->has('emp_id'))
                                        <span class="help-block">
                                            {{ $errors->first('emp_id') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userUpdate' && $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username">Username:</label>
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" />
                                    @if(Session::get('error_type') == 'userUpdate' &&  $errors->has('username'))
                                        <span class="help-block">
                                            {{ $errors->first('username') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group {{ Session::get('error_type') == 'userUpdate' && $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email:</label>
                                    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" />
                                    @if(Session::get('error_type') == 'userUpdate' &&  $errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center">Delete User <i class="fa fa-warning"></i></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('user.management.delete') }}">
                {{ csrf_field() }}
                <input type="hidden" name="userid" id="delete-userid" />
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to delete this user?</h3>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Delete</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="userRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('user.role.update') }}">
            {{ csrf_field() }}
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-eraser" aria-hidden="true"></i> Edit Employee Role</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group">
                    <input type="hidden" name="emp_id" id="emp_id" />
                    <table class="table">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label for="firstname">Role:</label>

                                    <select class="selectpicker form-control" name="role" id="userRole">
                                    </select>

                                </div>
                            </td>
                        </tr>
                        {{--  <tr>
                            <td><h3 class="text-center">Module Access</h3></td>
                        </tr>

                        <tr>
                            <td style="border:none">
                                <div class="form-group accounts-module">
                                    <div class="col-md-4">
                                        <label for="accounts-module">Accounts Module:</label>
                                    </div>
                                    <div class="col-md-8" id="accounts-module">
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="accounts-module[]" value="C">Add
                                        </label>
                                        <label class="checkbox-inline">
                                           <input type="checkbox" name="accounts-module[]" value="R">View
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="accounts-module[]" value="U">Edit
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="accounts-module[]" value="D">Delete
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border:none">
                                <div class="form-group employees-module">
                                    <div class="col-md-4">
                                        <label for="employees-module">Employees Module:</label>
                                    </div>
                                    <div class="col-md-8" id="employees-module">
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="employees-module[]" value="C">Add
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="employees-module[]" value="R">View
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="employees-module[]" value="U">Edit
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="employees-module[]" value="D">Delete
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>  --}}
                    </table>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
