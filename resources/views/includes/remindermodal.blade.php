<div class="modal fade" id="addReminderModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('reminder.create') }}">
            {{ csrf_field() }}
                <input type="hidden" id="reminder-accountid" name="reminderaccountid" />
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Add reminder </span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group" id="app-create-reminder">
                    <create-reminder-form></create-reminder-form>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button id="reminder-create" type="submit" class="btn btn-success"> Create </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="reminderModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" id="reminder-accountid" name="reminderaccountid" />
            <div class="data-header-box clearfix">
                <span class="header-h"> Reminder Details </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="form-group" id="app-reminder-details">
                <reminder-details></reminder-details>
            </div>
            <div class="bottom-buttons clearfix">
                <div class="buttons-box">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
