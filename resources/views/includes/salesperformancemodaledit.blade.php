<!-- ------- EXPORT SALES REPORT MODULE ------- -->
<div class="modal fade" id="view_sales_report_modal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="loader-overlay hidden"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="row">
                <i id="loader-modal" class="loader hidden"></i>
                <div class="col-md-12" style="width:600px">
                    <div class="data-header-box clearfix">
                        <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Generate Sales Performance Report</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <br />

                    <div class="row" style="padding:4%">
                        <div class="col-md-5">
                            <label>List of Report to generate</label>
                            <ul>
                                <li><small>Appointment Report</small></li>
                                <li><small>Opportunity Report</small></li>
                            </ul>
                        </div>
                    </div>


                    <br />

                    <div class="row">
                        <div class="col-md-7" id="div-export-salesperson">
                            <div class="form-group">
                                <label for="export-salesperson">Salesperson</label>
                                <select id="export-salesperson" class="selectpicker">
                                    @if($loggedUser->role == 1)
                                        <option value="all">All</option>
                                        @foreach($salesPersonName as $row)
                                            <option value="{{ $row->userid }}">{{ $row->full_name }}</option>
                                        @endforeach
                                    @else
                                        <option value="{{ $loggedUser->userid }}">{{ $loggedUser->userInfo->full_name }}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group">
                                <input type="checkbox" id="opportunity-date-checked"><small>Opportunity Date (Default: Total target achieved for current month if not checked)</small>
                            </div>
                        </div>

                        <div class="panel panel-default hidden" id="opportunity-date-container">
                            <div class="panel-body">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="opportunity_daterange">Target date</label>
                                        <input class="form-control" type="text" id="opportunity_daterange" name="opportunity_daterange" />
                                        <input type="hidden" id="opportunity_start_date" />
                                        <input type="hidden" id="opportunity_end_date" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-11">
                            <div class="form-group">
                                <input type="checkbox" id="appointment-date-checked"><small>Appointment date (Default: Current week and last week will show if not checked)</small>
                            </div>
                        </div>

                        <div class="panel panel-default hidden" id="appointment-date-container">
                            <div class="panel-body">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="appointment_daterange">Target date</label>
                                        <input class="form-control" type="text" id="appointment_daterange" name="appointment_daterange" />
                                        <input type="hidden" id="appointment_start_date" />
                                        <input type="hidden" id="appointment_end_date" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-buttons clearfix">
                        <div class="buttons-box">
                            <a class="btn btn-primary" id="btn-export"><i class="fa fa-cloud-download"></i> Export</a>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                        </div>
                    </div>

                </div> <!-- div.col-md-12 -->
            </div> <!-- div.row -->

        </div> <!-- div.modal-content -->
    </div> <!-- div.modal-dialog -->
</div> <!-- div.modal-fade -->


<!-- ------- VIEW AND EDIT COMMENT MODAL ------- -->
<div class="modal fade" id="view_contract_modal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="loader-overlay hidden"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="row">
                <i id="loader-modal" class="loader hidden"></i>
                <div class="col-md-12" style="width:600px">
                    <div class="data-header-box clearfix">
                        <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> <span id="perf-company"></span></span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <br />

                    <input type="hidden" id="perf_id" value="" />

                    <div class="form-group">
                        <label for="txt-comment">Remarks</label>
                        <textarea cols="30" rows="5" name="txt-comment" id="txt-comment" class="form-control" ></textarea>
                    </div>

                    <div class="clearfix" style="margin-bottom:7%"></div>

                    <div class="lead-notes panel panel-default" id="sales-notes"></div>

                    <div class="bottom-buttons clearfix">
                        <div class="buttons-box">
                            <button type="button" class="btn btn-success" id="btn-add-remarks">Add Remarks</button>
                            <button type="button" class="btn btn-danger" id="btn-close-remark" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div> <!-- div.col-md-12 -->
            </div> <!-- div.row -->

        </div> <!-- div.modal-content -->
    </div> <!-- div.modal-dialog -->
</div> <!-- div.modal-fade -->

<!-- ------- VIEW LEAD/PROSPECT INFORMATION ------- -->
<div class="modal fade" id="view_information" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="loader-overlay hidden"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="row">
                <i id="loader-modal" class="loader hidden"></i>
                <div class="col-md-12" style="width:800px">
                    <div class="data-header-box clearfix">
                        <span class="header-h"><i class="fa fa-eye" aria-hidden="true"> </i> <span id="perf-company"></span></span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    {{--  <input type="hidden" id="edit_perf_id" value="" />  --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-status" class="col-md-5 control-label">Status</label>
                                <div class="col-md-6" id="view-status">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-company" class="col-md-5 control-label">Company Name</label>
                                <div class="col-md-6" id="view-company">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-contract-value" class="col-md-5 control-label">Contract Value</label>
                                <div class="col-md-6" id="view-contract-value">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-client-name" class="col-md-5 control-label">Client Name</label>
                                <div class="col-md-6" id="view-client-name">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-office-number" class="col-md-5 control-label">Office Number</label>
                                <div class="col-md-6" id="view-office-number">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-mobile-number" class="col-md-5 control-label">Mobile Number</label>
                                <div class="col-md-6" id="view-contact-number">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-email" class="col-md-5 control-label">Email Address</label>
                                <div class="col-md-6" id="view-email">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-designation" class="col-md-5 control-label">Designation</label>
                                <div class="col-md-6" id="view-designation">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-address" class="col-md-5 control-label">Address</label>
                                <div class="col-md-6" id="view-address">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-postal" class="col-md-5 control-label">Postal Code</label>
                                <div class="col-md-6" id="view-zip-code">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-city" class="col-md-5 control-label">City/Country</label>
                                <div class="col-md-6" id="view-city">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-website" class="col-md-5 control-label">Website URL</label>
                                <div class="col-md-6" id="view-website">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-services" class="col-md-5 control-label">Services</label>
                                <div class="col-md-6" id="view-services">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-other-services" class="col-md-5 control-label">Other Services</label>
                                <div class="col-md-6" id="view-other-services">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-source" class="col-md-5 control-label">Source of Leads</label>
                                <div class="col-md-6" id="view-source">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-source" class="col-md-5 control-label">Other Source</label>
                                <div class="col-md-6" id="view-source-other">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-landing" class="col-md-5 control-label">Landing Page</label>
                                <div class="col-md-6" id="view-landing">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-landing" class="col-md-5 control-label">Other Landing Page</label>
                                <div class="col-md-6" id="view-landing-other">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-created-by" class="col-md-5 control-label">Owner</label>
                                <div class="col-md-6" id="view-created-by">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label for="view-created-at" class="col-md-5 control-label">Date Created</label>
                                <div class="col-md-6" id="view-created-at">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-buttons clearfix">
                        <div class="buttons-box">
                            <button type="button" class="btn btn-danger" id="btn-close-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div> <!-- div.col-md-12 -->
            </div> <!-- div.row -->

        </div> <!-- div.modal-content -->
    </div> <!-- div.modal-dialog -->
</div> <!-- div.modal-fade -->