<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('role.create') }}">
            {{ csrf_field() }}
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-pencil" aria-hidden="true"></i> Add Role</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <input type="text" class="form-control" name="role" value="{{ old('role') }}" />
                    @if($errors->has('role'))
                        <span class="help-block">
                            {{ $errors->first('role') }}
                        </span>
                    @endif
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('role.update') }}">
            {{ csrf_field() }}
                <input type="hidden" name="roleid" id="roleid" />
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-pencil" aria-hidden="true"></i> Update Role</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-box">
                    <label for="role">Role</label>
                    <input type="text" class="form-control" name="role" id="role" value="{{ old('role') }}" />
                    @if($errors->has('role'))
                        <span class="help-block">
                            {{ $errors->first('role') }}
                        </span>
                    @endif
                </div>
                <div class="form-box">
                    <label class="checkbox-inline" id="isActive"></label>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Save Changes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addManagerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('role.add.manager') }}">
            {{ csrf_field() }}
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-pencil" aria-hidden="true"></i> Add Manager</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group">
                    <label for="role">Select Employee(s)</label>
                    <div class="select-box">
                        <select class="selectpicker form-control" name="managers[]" multiple="multiple">
                            @foreach($headofdepts as $row)
                                <option value="{{ $row->emp_id }}">{{ $row->UserInfo->f_name.' '.$row->UserInfo->l_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteManagerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('role.delete.manager') }}">
            {{ csrf_field() }}
                <input type="hidden" name="emp_id" id="delete-manager" />
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-eraser" aria-hidden="true"></i> Remove Manager </span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="form-group">
                    <h3 class="text-center">Are you sure you want to remove this manager?</h3>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="btn btn-success">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>