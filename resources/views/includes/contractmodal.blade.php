
<!-- Modal: Contract List -->
    <div class="modal fade" id="contract-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <!--Header-->
        <div class="modal-header">
            <h4 class="modal-title modal-align-center" id="myModalLabel">Contracts List</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>

        <!--Body-->
        <div class="modal-body">
            <div class="form-group">
                <div id="contract"></div>
            </div>
        </div>

        <!--Footer-->
        <div class="modal-footer modal-align-center">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
            <a id="add-contract" type="button" class="btn btn-success"> New Contract</a>
        </div>
        </div>
    </div>
    </div>
</div>
<!-- Modal: Contract List -->