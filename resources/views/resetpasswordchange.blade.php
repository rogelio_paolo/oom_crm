@extends('layouts.master')

@section('title')
  Crm - Reset Password
@endsection

@section('content')

<div class="wrapper login-box valign">

<div class="container">
<div class="row">

  <div class="col-md-8 col-md-offset-2">

    <div class="login-form clearfix">

      <div class="login-form--inner logo-box valign">

        <img class="logo-brand" src="{{ asset('images/logo.png') }}" alt="">

      </div>

      <i id="loader" class="loader-overlay hidden"></i>

        <form class="login-form--inner login-bg" method="POST" action="{{ route('user.password.reset.change') }}">
        {{ csrf_field() }}
        <input type="hidden" name="userid" value="{{ $userid }}">
        
        <span class="login-header">Enter new password</span>

        <div class="form-box login-pass">

            <input id="password" type="password" class="form-control" name="password" placeholder="Enter new password">

            @if ($errors->has('password'))
                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('password') }}</span>
            @endif

        </div>

        <div class="form-box confirm-pass">

            <input id="confirm_password" type="password" class="form-control" name="confirm_password" placeholder="Confirm new password">

            @if ($errors->has('confirm_password'))
                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('confirm_password') }}</span>
            @endif

        </div>

        <button id="change-password" type="submit" class="cta-btn sign-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
            CHANGE PASSWORD
        </button>

      </form>

    </div>

  </div>

</div>
</div>

</div><!--end of wrapper-->

@section('scripts')
    <script src="{{ asset('js/resetpassword.js') }}"></script>
@endsection

@endsection
