@extends('layouts.master')

@section('title')
  Internal Server Error
@endsection

@section('content')

<div class="wrapper login-box valign">

<div class="container">
<div class="row">

  <div class="col-md-8 col-md-offset-2">

    <div class="error-page-500">
    </div>
    <br>
    <h3 class="text-center">Oh snap! Something happened with our server. Please try to refresh the page.</h3>
    <hr>
    <div class="text-center col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-4 col-xs-4">
        <a href="{{ URL::to('/') }}" class="cta-btn sign-btn">
            Go back
        </a>
    </div>

  </div>

</div>
</div>

</div>

@endsection

