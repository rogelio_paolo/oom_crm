@extends('layouts.master_v2')

@section('title')
    Crm - Sales Performance List
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('modals')
    @include('includes.salesperformancemodaledit')
@endsection

@section('main_content')
    <div id="columns" class="outer-data-box">

        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-calendar"></i> Sales Performance</span>

                <div class="data-search-box">

                    <span>
                     Search Company:
                     <div class="search-box">
                       <input type="text" class="form-control search-area" name="searchSalesAcc" id="searchSalesAcc" placeholder="Search" onkeypress="triggerSearch(event)">
                       <button id="btn-search-perf" type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                     </div>
                   </span>

                </div>
            </div>

            <div class="data-buttons-box clearfix">
                <div class="filter-box">
                    <a href="javascript:void(0)" id="btn-refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>
                </div>


                <div class="buttons-box">
                    <a id="btn-sales-report-modal" data-toggle="modal" data-target="#view_sales_report_modal" class="btn btn-warning"><i class="fa fa-cloud-download"></i> Export Sales Report</a>
                </div>


                @if ($user->role === 1)
                    <div class="row">
                        <div class="col-md-3">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="filter-salesperson" class="col-md-6 control-label">Filter Salesperson: </label>
                                    <div class="col-md-6">
                                        <select id="filter-salesperson" class="form-control w-300">
                                            <option value="all" {{ $filter == 'all' ? 'selected' : '' }}>All Salesperson</option>
                                            @foreach($salesPersonName as $row)
                                                <option value="{{ $row->userid }}" {{ $filter == $row->userid  ? 'selected' : '' }}>{{ $row->full_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>


            <div class="table-box">
                <i id="loader" class="loader hidden"></i>
                <table class="table main-table" id="performance-list">
                    <thead>
                    <tr>
                        <th class="text-center company">Company <a href="" class="sort-perf" data-sortby="company"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                        <th class="text-center f_name" >Client Name <a href="" class="sort-perf" data-sortby="client_name"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                        <th class="text-center contact_number">Contact Number <a href="" class="sort-perf" data-sortby="contact_number"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                        <th class="text-center contract_value">Contract Value <a href="" class="sort-perf" data-sortby="contract_value"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                        <th class="text-center services">Services</th>
                        <th class="text-center created_at">Created at <a href="" class="sort-perf" data-sortby="created_at"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                        @if($user->role == 1) <th class="text-center created_by">Owner <a href="" class="sort-perf" data-sortby="created_by"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th> @endif
                        <th class="text-center status">Status <a href="" class="sort-perf" data-sortby="status"><i id="sort-perf" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                        <th class="text-center remarks">Latest Remarks</th>
                        <th class="text-center actions">Actions/Update</th>
                    </tr>
                    </thead>
                    <tbody id="performance-data">
                    @forelse($salesPerformance as $performance)
                        <tr data-performance="{{ $performance['id'] }}">
                            <td class="text-center company">{{ $performance['company'] }}</td>
                            <td class="text-center client_name">{{ $performance['client_name'] }}</td>
                            <td class="text-center contact_number">{{ $performance['contact_number'] }}</td>
                            <td class="text-center contract_value">{{ $performance['contract_value'] }}</td>
                            <td class="text-center services">{{ $performance['services'] }}</td>
                            <td class="text-center created_at">{{ $performance['created_at'] }}</td>
                            @if($user->role == 1) <td class="text-center created_by">{{ $performance['created_by'] }}</td> @endif
                            <td class="text-center status">
                            <span title="{!! !empty($performance['status_updated_at']) && !empty($performance['status_updated_by']) ?  'Last Updated: ' . $performance['status_updated_at'] . ' Last Updated by: ' . $performance['status_updated_by'] : 'No further updates!'  !!}" id="status-{{ $performance['id'] }}">
                                {{ $performance['status'] }}
                            </span>
                                <select id="change-status-{{ $performance['id'] }}" class="form-control w-150 hidden">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->systemcode }}" {{ $performance['status'] === $status->systemdesc ? 'selected' : '' }}>{{ $status->systemdesc }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="text-center remarks" id="td-comment-{{ $performance['id'] }}">
                                <span id="comment-box" data-perf-id="{{ $performance['id'] }}">{{ mb_strimwidth($performance['remarks'], 0, 16, '...') }} @if($performance['remarks_date'] != 'TBA') <small>({{ $performance['remarks_date'] }})</small> @endif</span>
                            </td>
                            <td class="text-center actions text-nowrap">
                                <button title="View more information" data-toggle="modal" data-target="#view_information" id="view-perf" data-perf-id="{{ $performance['id'] }}" class="btn btn-info"> <i class="fa fa-eye"></i></button>
                                <button title="Change status" id="btn-change-status" data-perf-id="{{ $performance['id'] }}" class="btn btn-danger"> <i class="fa fa-exchange"></i></button>
                                <span id="div-change-status-{{ $performance['id'] }}" class="hidden">
                                <button title="Cancel" id="btn-cancel-update" class="btn btn-danger" data-perf-id="{{ $performance['id'] }}"><i class="fa fa-times"></i></button>
                                <button title="Save" id="btn-apply-update" class="btn btn-success" disabled data-perf-id="{{ $performance['id'] }}"><i class="fa fa-save"></i></button>
                            </span>
                                <button title="Add remarks" data-toggle="modal" data-target="#view_contract_modal" id="edit-perf" data-perf-id="{{ $performance['id'] }}" class="btn btn-primary"> <i class="fa fa-edit"></i></button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12">No record found</td>
                        </tr>
                    @endforelse
                    </tbody>

                </table>

                <div class="row">
                    <div class="col-md-1">
                        <label for="limit_entry">Items per page: </label>
                    </div>
                    <div class="col-md-2">
                        <div class="select-box">
                            <select id="limit_entry" name="limit_entry" class="w-150">
                                <option value="10" {{ !empty($items) ? $items == 10 ? 'selected' : '' : '' }}>10</option>
                                <option value="20" {{ !empty($items) ? $items == 20 ? 'selected' : '' : '' }}>20</option>
                                <option value="30" {{ !empty($items) ? $items == 30 ? 'selected' : '' : '' }}>30</option>
                                <option value="40" {{ !empty($items) ? $items == 40 ? 'selected' : '' : '' }}>40</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div> <!-- div.table-box -->

            <div class="pagination-box">
                {{ $performances->render() }}
            </div>

        </div> <!-- div.main-data-box -->
    </div> <!-- div#columns -->

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/performance/list.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/performance/export.js') }}"></script>
@endsection
@endsection
