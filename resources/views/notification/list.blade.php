@extends('layouts.master_v2')

@section('title')
  Crm - Notifications
@endsection

@section('main_content')

<div class="outer-data-box" id="notification-list">

    <div class="main-data-box">
  
        <div class="data-header-box clearfix">

            <span class="header-h"><i class="fa fa-globe" aria-hidden="true"></i> Your notifications </span>

            <div class="data-search-box">

            </div>

        </div>

        <div class="data-buttons-box clearfix">

            <div class="filter-box">

            </div>

            <div class="buttons-box">

            </div>

        </div>

        <notifications-all v-bind:notifications="notifications"></notifications-all>
  
    </div>
  
</div>

@section('scripts')
    <script src="{{ asset('js/audit/list.js') }}" type="text/javascript"></script>
@endsection

@endsection
