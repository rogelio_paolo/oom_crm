@extends('layouts.master')

@section('title')
  Crm - Login
@endsection

@section('content')

<div class="wrapper login-box valign">

<div class="container">
<div class="row">

  <div class="col-md-8 col-md-offset-2">

    <div class="login-form clearfix">

      <div class="login-form--inner logo-box valign">

        <img class="logo-brand" src="{{ asset('images/logo.png') }}" alt="">

      </div>

      <form class="login-form--inner login-bg" method="POST" action="{{ route('user.login') }}">
        {{ csrf_field() }}
        <span class="login-header">OOM LOGIN</span>

        <div class="form-box login-email">

          <input id="email" type="text" class="form-control" name="login" value="{{ old('login') }}" placeholder="Email or Username" autofocus>

          @if ($errors->has('login'))
              <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('login') }}</span>
          @endif

        </div>

        <div class="form-box login-pass">

          <input id="password" type="password" class="form-control" name="password" placeholder="Password">

          @if ($errors->has('password'))
              <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('password') }}</span>
          @endif

        </div>

        <button type="submit" class="cta-btn sign-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
            SIGN IN
        </button>

        <div class="forgot-pass text-center">
            <p>
                <a class="btn" href="{{ route('user.password.reset') }}"> <i class="fa fa-info-circle"></i> forgot password? </a>
            </p>
        </div>

      </form>

    </div>

  </div>

</div>
</div>

</div><!--end of wrapper-->

@endsection
