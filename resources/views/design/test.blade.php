<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vue JS</title>   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        [v-cloak] {
            display: none;
        }
        .highlight {
            border: 3px solid red;
            color: red;
        }
        .shooting-star-leave-to, .shooting-star-enter {
            transform: translateX(150px) rotate(30deg);
            opacity: 0;
        }
        .shooting-star-enter-active, .shooting-star-leave-active {
            transition: all .5s ease;
        }
        .neo-list-leave-to, .neo-list-enter {
            opacity: 0;
            transform: translateY(30px);
        }
        .neo-list-enter-active, .neo-list-leave-active {
            transition: all 1s linear;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="container">

            // Chapter 1
            {{-- <h1 v-cloak>@{{ greeting }}</h1>
            <form v-show="!submitted"  action="" class="mt-5">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" v-model="email" class="form-control form-control-lg">
                </div>
        
                <button class="btn btn-primary" @click.prevent="process" type="submit">Submit</button>
            </form>
            <h2 v-show="submitted" class="mt-5">Thanks for signing up!</h2> --}}

            //Chapter 2
            <div class="card mt-5">
                <h2 class="card-header">Near-earth Objects</h2>
                <transition name="shooting-star">
                    <div class="m-3" v-cloak v-if="numAsteroids > 0 && showSummary">
                        <p>Showing: @{{ numAsteroids }} items</p>
                        <p>@{{ closestDistance }} has the shortest miss distance.</p>
                    </div>
                </transition>
               
                <div class="m-3">
                    <a href="#" @click="showSummary = !showSummary">Show/hide summary</a>
                </div>
                <table class="table table-striped" :class="[{'table-dark': false}, 'table-bordered']">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Close Approach Date</th>
                            <th>Miss Distance</th>
                            <th>Remove</th>
                        </tr>
                       
                    </thead>
                        <tbody is="transition-group" name="neo-list" v-cloak>
                            <tr v-for="(a, index) in asteroids" :key="a.neo_reference_id" :class="{ highlight: isMissingData(a), 'shadow-sm': true }">
                                <td>@{{ index + 1 }}</td>
                                <td>@{{ a.name }}</td>
                                <td>@{{ getApproachDate(a) }}</td>
                                <td>
                                    <ul v-if="a.close_approach_data.length > 0">
                                        <li v-for="(value, key, index) in a.close_approach_data[0].miss_distance">@{{ index + 1}} - @{{ key }}: @{{ value }}</li>
                                    </ul>
                                </td>
                                <td>
                                    <button @remove="remove" @click="remove(index)" class="btn btn-danger">Remove</button>
                                </td>
                            </tr>
                        </tbody>
                </table>
            </div>
           
            
        </div>
       
    </div>

  
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                asteroids: [],
                showSummary: true
            },
            created: function() {
                this.fetchAsteroids();
            },
            computed: {
                numAsteroids: function() {
                    return this.asteroids.length;
                },
                closestDistance: function() {
                    var neosHavingData = this.asteroids.filter(function(neo) {
                        return neo.close_approach_data.length > 0;
                    });

                    var simpleNeos = neosHavingData.map(function(neo) {
                        return {name: neo.name, miles: neo.close_approach_data[0].miss_distance.miles};
                    });

                    var sortedNeos = simpleNeos.sort(function(a, b) {
                        return a.miles - b.miles;
                    });

                    return sortedNeos[0].name;
                }
            },
            methods: {
                fetchAsteroids: function () {
                    var apiKey = '4dMmk5LZOXQod18Fs9RMii8ce76yPBiiT7YVdzP5';
                    var url = 'https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=' + apiKey;

                    axios.get(url)
                        .then(function (res) {
                            vm.asteroids = res.data.near_earth_objects.slice(0, 10);
                        });
                },
                getApproachDate: function (a) {
                    if (a.close_approach_data.length > 0) {
                        return a.close_approach_data[0].close_approach_date;
                    }
                    return 'N/A';
                },
                remove: function(index) {
                    // this.asteroids.splice(index, 1);
                    this.$emit('remove', index);
                },
                getRowStyle: function(a) {
                    if(a.close_approach_data.length == 0) {
                        return {
                            border: '2px solid red',
                            color: 'red'
                        } 
                    }
                },
                isMissingData: function(a) {
                    return a.close_approach_data.length == 0;
                }
            }
        });
        
    </script>
</body>
</html>