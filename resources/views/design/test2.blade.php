<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vue JS</title>   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="container">
            <another-component></another-component>
            <another-again></another-again>
            <my-component></my-component>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var anotherComponent = {
            data: function() {
                return {
                    msg: 'Hello from another component'
                }
            },
            template: '<h1>@{{ msg}}</h1>'
        };

        Vue.component('my-component', {
            template: '<strong>Static element like...</strong>'
        });

        Vue.component('another-again', {
            template: '<em>Another component again</em>'
        });

        var vm = new Vue({
            el: '#app',
            components: {
                'another-component': anotherComponent
            }
        });
    </script>
</body>
</html>