<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vue JS</title>   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="container">
            <a-pod date="2018-08-07"></a-pod>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        Vue.component('a-pod', {
            template: '<div> \
                      <p>@{{ title }}</p> \
                      <slot name="title"><h3>Unknown date</h3></slot> \
                      <p>@{{ explanation }}</p> \
                      <slot name="caption" :date="date"></slot> \
                      <p>@{{ date || today }}</p></div>',
            props: ['date'],
            data: function() {
                return {
                    title: '',
                    explanation: ''
                };
            },
            created: function() {
                this.fetchApod();
            },
            methods: {
                fetchApod: function() {
                    var apiKey = '4dMmk5LZOXQod18Fs9RMii8ce76yPBiiT7YVdzP5';
                    var url = 'https://api.nasa.gov/planetary/apod?api_key=' + apiKey;
                    if(this.date) {
                        url += '&date=' + this.date;
                    }
                    var self = this;
                    axios.get(url)
                        .then(function(res) {
                            console.clear();
                            console.log(res);
                            self.title = res.data.title;
                            self.explanation = res.data.explanation;
                        });
                }
            }
        });

        var vm = new Vue({
            el: '#app'
        })
    </script>
</body>
</html>