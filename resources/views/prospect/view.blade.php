
@extends('layouts.master_v2')
@section('title')
    Crm - View {{ $prospect->lead->company . "'s "}} prospect
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection

@section('main_content')

<div class="container pull-left">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('prospect.list') }}">Prospects</a></li>
                <li class="active">{{ $prospect->lead->company }} - View</li>
            </ol>
        </div>
    </div>
</div>

{{--  <div class="back-btn">
        <a href="{{ route('prospect.list') }}" class="btn btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Prospects List
        </a>
    </div>  --}}

<div class="modal fade" id="deleteNoteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" id="delete-leadnote" />
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-eraser" aria-hidden="true"></i> Delete Note</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="form-group">
                <h3 class="text-center">Are you sure you want to delete this note?</h3>
            </div>
            <div class="bottom-buttons clearfix">
                <div class="buttons-box">
                    <a id="delete-leadnote" data-leadnote="" class="btn btn-success">Delete</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="outer-data-box">
    <div class="row"> 
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h">View Prospect Details</span>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">

            <div class="form-group clearfix">
                <label for="status" class="col-md-4 control-label">Status</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->prospectStatus) ? $prospect->prospectStatus->systemdesc : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="company" class="col-md-4 control-label">Company Name</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->company) ? $prospect->lead->company : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                    <label for="contract_value" class="col-md-4 control-label">Contract Value</label>
                    <div class="col-md-6">
                        <p>{{ !empty($prospect->lead->contract_value) ?  (!empty($prospect->lead->currency) ? $prospect->lead->currency . ' ' : '') . ' ' . $prospect->lead->contract_value : 'Not Specified' }}</p>
                    </div>
                </div>
            
            <div class="form-group clearfix">
                <label for="f_name" class="col-md-4 control-label">Client Name</label>
                <div class="col-md-6">
                   <p>{{ !empty($prospect->lead->l_name) ? $prospect->lead->f_name.' '.$prospect->lead->l_name : $prospect->lead->f_name }}</p>
                </div>
            </div>
            
            <div class="form-group clearfix">
                <label for="office_number" class="col-md-4 control-label">Office Number</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->office_number) ? $prospect->lead->office_number : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                    <label for="contact_number" class="col-md-4 control-label">Mobile Number</label>
                    <div class="col-md-6">
                        <p>{{ !empty($prospect->lead->contact_number) ? $prospect->lead->contact_number : 'Not Specified' }}</p>
                    </div>
                </div>
            
            <div class="form-group clearfix">
                <label for="email" class="col-md-4 control-label">Email Address</label>
                <div class="col-md-6">
                    @if (!empty($prospect->lead->email))
                        @foreach (explode('|', $prospect->lead->email) as $email)
                            <p>{{ $email }}</p>
                        @endforeach
                    @else
                        <p>Not Specified</p>
                    @endif
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="designation" class="col-md-4 control-label">Designation</label>
                <div class="col-md-6">
                   <p>{{ !empty($prospect->lead->designation) ? $prospect->lead->designation : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="street_address" class="col-md-4 control-label">Address</label>
                <div class="col-md-6">
                    <p>{!! !empty($prospect->lead->street_address) ? nl2br($prospect->lead->street_address) : 'Not Specified' !!}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="zip_code" class="col-md-4 control-label">Postal Code</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->zip_code) ? $prospect->lead->zip_code : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="city" class="col-md-4 control-label">City/Country</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->city) ? $prospect->lead->city : 'Not Specified' }}</p> 
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="industry" class="col-md-4 control-label">Industry</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->industry) ? $prospect->lead->lead_industry->title : 'Not Specified' }}</p> 
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="website" class="col-md-4 control-label">Website URL</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->website) ? $prospect->lead->website : 'Not Specified' }}</p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="services" class="col-md-4 control-label">Services</label>
                <div class="col-md-6">
                    <p>
                        @if(!empty($prospect->lead->services))
                            @php
                                $services_arr = array();
                                $services = explode('|', $prospect->lead->services);
                                foreach($services as $row)
                                {
                                    $services_arr[] = \App\System::where('systemcode',$row)->first()->systemdesc;
                                }
                                $services = implode(', ',$services_arr);
                            @endphp
                            <p>{{ $services }}</p>
                        @else
                            Not Specified
                        @endif
                    </p>
                </div>
            </div>


            <div class="form-group clearfix">
                <label for="other_services" class="col-md-4 control-label">Other Services</label>
                <div class="col-md-6">
                   <p>{{ !empty($prospect->lead->other_services) ? $prospect->lead->other_services : 'Not Specified' }}</p>
                </div>
            </div>


            <div class="form-group clearfix">
                <label for="sources" class="col-md-4 control-label">Source of Leads</label>
                <div class="col-md-6">
                   <p>{{ !empty($prospect->lead->source) ? ($prospect->lead->source != 'SL999' ? $prospect->lead->source_leads->systemdesc : $prospect->lead->source_leads->systemdesc.'- '.$prospect->lead->source_other) : 'Not Specified' }}
                   </p>
                </div>
            </div>
            
            @if($prospect->lead->source != 'SL002')
                <div class="form-group clearfix">
                    <label for="landings" class="col-md-4 control-label">Landing Page</label>
                    <div class="col-md-6">
                        <p>{{ !empty($prospect->lead->landing_page) ? ($prospect->lead->landing_page != 'LP999' ? $prospect->lead->landing->systemdesc : $prospect->lead->landing->systemdesc.'- '.$prospect->lead->landing_other) : 'Not Specified' }}
                        </p>
                    </div>
                </div>
            @endif

            <div class="form-group clearfix">
                <label for="sources" class="col-md-4 control-label">Prospect Owner</label>
                <div class="col-md-6">
                   <p>
                        @if(!empty($prospect->created_by))
                            @php $user = \App\User::where('userid',$prospect->created_by)->first(); @endphp
                            {{ $user->UserInfo->f_name.' '.$user->UserInfo->l_name }}
                        @else
                            None
                        @endif
                   </p>
                </div>
            </div>

            <div class="form-group clearfix">
                <label for="sources" class="col-md-4 control-label">Date Created</label>
                <div class="col-md-6">
                   <p>{{ !empty($prospect->updated_at) ? $prospect->updated_at->format('d M Y h:i:s A') : 'Not Specified' }}</p>
                </div>
            </div>

                            <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <form class="outer-data-box" method="post" action="{{ route('lead.note.create') }}" enctype="multipart/form-data">
        <i id="loader" class="loader leadnotes hidden"></i>
        {{ csrf_field() }}
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Prospect Notes</span>
                <div class="data-search-box">
                    Filter By:
                    <div class="select-box">
                        <select class="form-control" id="filterLeadNotes" name="filterLeadNotes">
                            <option value="all" selected>All</option>
                            <option value="date">Date</option>
                            <option value="commentor">Commentor</option>
                        </select>
                    </div>
                    <div id="filterButtons" class="form-inline pull-right graph-filter">
                        <button class="btn btn-default" id="FilterNotesAll"> FILTER </button>
                    </div>
                    <div id="filter-by-date" class="form-inline pull-right graph-filter hidden">
                    From <input type="text" class="form-control datepicker" id="filterDateFrom" name="filterDateFrom" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    To <input type="text" class="form-control datepicker" id="filterDateTo" name="filterDateTo" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    <button class="btn btn-default" id="FilterNotesByDate"> FILTER </button>
                    </div>
                    <div id="filter-by-commentor" class="form-inline pull-right graph-filter hidden">
                        <div class="select-box">
                            <select class="form-control" id="lead-commentors" name="filterNotes">
                                @foreach($commentors as $row)
                                    <option value="{{ $row->userid }}">{{ $row->user->UserInfo->f_name.' '.$row->user->UserInfo->l_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-default" id="FilterNotesByCommentor"> FILTER </button>
                    </div>
                </div>
            </div>
            <div class="lead-notes">
                    
                    @forelse($notes as $row)
                        <div class="lead-note">
                            <span> 
                                <p>{{ $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name }}</p> 
                                <p>{{ date_format($row->created_at,'Y-m-d h:i A') }}</p>
                            </span>
                            
                            @if ($row->userid == $user->userid)
                            <span class="pull-right">
                            <a id="edit-note" data-editnote="{{ $row->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            <a id="delete-note" data-delnote="{{ $row->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                            </span>
                            @endif

                            <div class="note-wrapper">
                                <span id="lead-note-{{ $row->id }}">{!! html_entity_decode($row->note) !!}</span>
                            </div>
                        </div>
                        <hr>
                    @empty
                        <div class="text-center">No Lead notes to be shown</div>
                    @endforelse
                    
            </div>
            <div class="pagination-box">
                {{ $notes->render() }}
            </div>

            <input type="hidden" id="leadid" name="leadid" value="{{ $prospect->lead_id }}" />
            <input type="hidden" id="prospectid" name="prospectid" value="{{ $prospect->id }}" />
            <textarea class="form-control" name="addnote" id="addnote" rows="10" placeholder="Leave your comments here"></textarea>
            @if($errors->has('addnote'))
                <span class="note error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true">  </i> {{ $errors->first('addnote') }}</span>
            @endif
            <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>
            <div class="form-group">
                <div class="text-right">
                    <button type="submit" class="cta-btn add-btn">
                        <i class="fa fa-save"></i> Add Note
                    </button>
                    <button type="button" id="clearNote" class="cta-btn cancel-btn">
                        <i class="fa fa-refresh"></i> Clear
                    </button>
                </div>
            </div>
        </div>

    </form>

    <div class="clearfix" style="margin-bottom: 1%">&nbsp;</div>
    
    <div class="container pull-left">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <ol class="breadcrumb bg-none" >
                    <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                    <li><a href="{{ route('prospect.list') }}">Prospects</a></li>
                    <li class="active">{{ $prospect->lead->company }} - View</li>
                </ol>
            </div>
        </div>
    </div>

    {{--  <div class="back-btn">
        <a href="{{ route('prospect.list') }}" class="btn btn-lg btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Prospects List
        </a>
    </div>  --}}

    <div class="clearfix" style="margin-bottom: 1%">&nbsp;</div>
    

@section('scripts')
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script src="{{ asset('js/lead/create.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/lead/notes.js') }}" type="text/javascript"></script>
@endsection

@endsection