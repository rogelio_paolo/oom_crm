<div class="modal fade" id="prospect_opportunity_modal_2" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="loader-overlay hidden"></div>
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Opportunity
                    </h4>
                </div>
                <i class="loader hidden" id="loader-opportunity"></i>
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-group">
                        <i class="loader hidden" id="loader-prospect"></i>
                        <!-- Opportunity Table -->
                        <div id="table-div-opportunity" class="hidden" style="width: 860px; overflow-x:scroll">
                            <table id="table-opportunity" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Contract Type</th>
                                    <th>Contract Value</th>
                                    <th>Services</th>
                                    <th>Status</th>
                                    <th>Contact Number</th>
                                    <th>Created Date</th>
                                    <th>Created by</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="opportunity-infos">
                                </tbody>
                            </table>
                        </div>


                        <!-- Will show if opportunity is empty -->
                        <p class="text-center hidden">No opportunity found! <a id="createNewOpportunity" href="#">Create new one.</a></p>
                    </div>

                    <!-- Form input fields for creation of new prospect's opportunity with Vee-Validate -->
                    <form id="form-prospect-opp" role="form" class="hidden">

                        <input type="hidden" value="" id="prospect_id" />
                        <input type="hidden" value="" id="opportunity_id" />

                        <hr />
                        <!-- Button for closing of form input fields -->
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btnHideForm" class="btn btn-danger btn-lg circle"
                                        title="Hide the Opportunity form" data-toggle="tooltip" type="button"
                                >
                                    <i class="fa fa-arrow-up"></i>
                                </button>
                            </div>
                        </div>


                        <!-- First Row: Nature of Package/Service Field -->
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="services">Nature of Package/Services <span class="error-danger">*</span></label>
                                    <select id="services" name="services[]" multiple="multiple" class="multiselect-ui form-control" required>
                                        @foreach ($services as $service)
                                            <option value="{{ $service->systemcode }}"
                                                    {{ $service->systemcode == 'NP001' ? 'selected' : '' }}>
                                                {{ $service->systemdesc }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <p id="error-services" class="error-danger hidden">{{-- errors.first('services') --}}</p>
                                </div>
                            </div>
                        </div>

                        <!-- Second Row: Contract Type and Currency field -->
                        <div class="row">

                            <!-- Contract Type field-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contract_type">Contract Type <span class="error-danger">*</span></label>
                                    <select class="form-control" name="contract_type" id="contract_type" required>
                                        @foreach ($contract_types as $contract_type)
                                            <option value="{{ $contract_type->systemcode }}"
                                                    {{ $contract_type->systemcode == 'CT001' ? 'selected' : '' }}>
                                                {{ $contract_type->systemdesc }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <p id="error-contract-type" class="error-danger hidden">{{-- errors.first('contract_type') --}}</p>
                                </div>
                            </div>

                            <!-- Currency field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contract_type">Currency <span class="error-danger">*</span></label>
                                    <select id="currency" class="form-control" name="currency" required>
                                        <option value="USD">US Dollar</option>
                                        <option value="SGD" selected >Singapore Dollar</option>
                                        <option value="PHP">Philippine Peso</option>
                                        <option value="MYR">Malaysian Ringgit</option>

                                    </select>
                                    <p id="error-currency" class="error-danger hidden">{{-- errors.first('currency') --}}</p>
                                </div>
                            </div>
                        </div>

                        <!-- Third Row: Contract Value and Client Name field -->
                        <div class="row">

                            <!-- Contract type field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contract_value">Contract Value <span class="error-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="input-group-currency">SGD</span>
                                        <input type="text" class="form-control" id="contract_value" name="contract_value" required/>
                                    </div>
                                    <p id="error-contract-value" class="error-danger hidden">{{-- errors.first('contract_value') --}}</p>
                                </div>
                            </div>

                            <!-- Client name field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="client_name">Client Name <span class="error-danger">*</span></label>
                                    <input type="text" class="form-control" id="client_name" name="client_name" placeholder="Enter Client Name" required />

                                    <p id="error-client-name" class="error-danger hidden">{{-- errors.first('client_name') --}}</p>
                                </div>
                            </div>
                        </div>


                        <!-- Fourth Row: Mobile Number and Office Number field -->
                        <div class="row">
                            <!-- Mobile number field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile_number">Mobile Number <span class="error-danger">*</span></label>
                                    <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="Enter Mobile Number" required/>

                                    <p id="error-mobile-number" class="error-danger hidden">{{-- errors.first('mobile_number') --}}</p>
                                </div>
                            </div>

                            <!-- Office number field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="office_number">Office Number</label>
                                    <input type="text" class="form-control" name="office_number" id="office_number" placeholder="Enter Office Number"/>
                                </div>
                            </div>
                        </div>

                        <!-- Fifth Row: Designation and Email address field -->
                        <div class="row">

                            <!-- Designation field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="designation">Designation</label>
                                    <input type="text" class="form-control" name="designation"
                                           id="designation" placeholder="Enter Designation"/>
                                </div>
                            </div>

                            <!-- Email address field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email Address <span class="error-danger">*</span></label>
                                    <input type="email" class="form-control" name="email"
                                           id="email" placeholder="Enter Email Address"/>

                                    <p id="error-email" class="error-danger">{{-- errors.first('email') --}}</p>
                                </div>
                            </div>
                        </div>

                        <!-- Sixth Row: Address and City/Country field -->
                        <div class="row">

                            <!-- Address field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea class="form-control" cols="5" name="address"
                                              id="address" placeholder="Enter Street Address"></textarea>
                                </div>
                            </div>

                            <!-- City/Country field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="city">City/Country</label>
                                    <input type="text" class="form-control" name="city"
                                           id="city" placeholder="Enter City/Country"/>
                                </div>
                            </div>
                        </div>

                        <!-- Seventh row: Postal code and Industry field -->
                        <div class="row">

                            <!-- Postal code field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="postal_code">Postal Code</label>
                                    <input type="text" class="form-control" name="postal_code"
                                           id="postal_code" placeholder="Enter Postal Code"/>
                                </div>
                            </div>

                            <!-- Industry field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="industry">Industry</label>
                                    <select id="industry" class="form-control" name="industry">
                                        @foreach($industries as $industry)
                                            <option value="{{ $industry->code }}" {{ $industry->code == 'ID999' ? 'selected' : '' }}>{{ $industry->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Eighth row: Landing Page and Website URL field -->
                        <div class="row">

                            <!-- Landing page field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="landing_page">Landing Page</label>
                                    <select id="landing_page" class="form-control" name="landing_page">
                                        @foreach($landings as $landing)
                                            <option value="{{ $landing->systemcode }}" {{ $landing->systemcode == 'LP001' ? 'selected' : '' }}>{{ $landing->systemdesc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- Website URL field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="website_url">Website URL</label>
                                    <textarea class="form-control" cols="5" name="website_url"
                                              id="website_url" placeholder="Enter Website URL"></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- Ninth row: Source of Leads and Status field -->
                        <div class="row">

                            <!-- Source of Leads field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="source">Source of Leads <span class="error-danger">*</span></label>
                                    <select id="source" class="form-control" required>
                                        @foreach($lead_sources as $source)
                                            <option value="{{ $source->systemcode }}" {{ $source->systemcode == 'SL002' ? 'selected' : '' }}>{{ $source->systemdesc }}</option>
                                        @endforeach
                                    </select>

                                    <p id="error-source" class="error-danger hidden">{{-- errors.first('source') --}}</p>
                                </div>
                            </div>

                            <!-- Status field -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status">Status <span class="error-danger">*</span></label>
                                    <select id="status" class="form-control" name="status" required>
                                        @foreach($lead_statuses as $status)
                                            <option value="{{ $status->systemcode }}" {{ $status->systemcode == 'LS002' ? 'selected' : '' }}>{{ $status->systemdesc }}</option>
                                        @endforeach
                                    </select>

                                    <p id="error-status" class="error-danger hidden">{{-- errors.first('status') --}}</p>
                                </div>
                            </div>
                        </div>


                    <!-- End of Form input fields with Vee-Validate -->
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">

                    <!-- Close modal button -->
                    <button type="button" class="btn btn-success"
                            data-dismiss="modal">
                        <i class="fa fa-remove"></i> Close
                    </button>

                    <!-- Submit button -->
                    <button id="btnCreateNew" type="submit" class="btn btn-primary">
                        <i class="fa fa-plus-circle"></i> Create New
                    </button>

                    </form>


                </div>

    {{--            <v-dialog/> <!-- Modal dialog -->--}}
            </div> <!-- div.modal-content -->
        </div> <!-- div.modal-dialog -->
</div> <!-- div.modal-fade -->