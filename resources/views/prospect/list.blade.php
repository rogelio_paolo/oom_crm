@extends('layouts.master_v2')

@section('title')
    Crm - Prospects
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
@endsection

@section('modals')
    @include('includes.prospectmodal')
@endsection

@section('main_content')

<div id="columns" class="outer-data-box">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-book" aria-hidden="true"></i> Prospects</span>

      <div class="data-search-box">

         <span>
          Search Prospect:
          <div class="search-box">
            <input type="text" class="form-control search-area" name="searchProspect" id="searchProspect" placeholder="Search" onkeypress="triggerSearch(event)">
            <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </span> 

      </div>

    </div>

    <div class="data-buttons-box clearfix">

      <div class="filter-box">
        
        <a href="javascript:void(0)" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>

        <span class="remaining-span">Remaining lockable Leads/Prospects: {{ !empty($lockedin) ? $lockedin->leadprospect_to_unlock : '10' }}/10</span>

      </div>

    </div>

    <div class="table-box" id="app-opportunity-prospect-modal">
      
      <i id="loader" class="loader hidden"></i>
        
      <table class="table main-table" id="prospect-management">

        <thead>
            <tr>
                <th class="text-center">Company Name <a href="" class="sort-prospect" data-sortby="company"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                <th class="text-center">Name <a href="" class="sort-prospect" data-sortby="f_name"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                <th class="text-center">Services</th>
                <th class="text-center">Status <a href="" class="sort-prospect" data-sortby="status"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
            @if($user->role == 1)
                <th class="text-center">Owner <a href="" class="sort-prospect" data-sortby="created_by"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
            @endif
                <th class="text-center">Created <a href="" class="sort-prospect active" data-sortby="created_at"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                <th class="text-center">Ex-Client <a href="" class="sort-prospect" data-sortby="ex_client"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                <th class="text-center">Prospect locked? <a href="" class="sort-prospect" data-sortby="lock_this_prospect"><i id="sort-prospect" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($result))
                
                @if(!in_array($user->role,[3,4]) || in_array($user->userInfo->team->team_code, ['busi','acc']) )
                    @foreach($result as $row)
                    <tr>
                            <td class="text-center overlay">{{ $row['company'] }}</td>
                            <td class="text-center overlay">{{ $row['name'] }}</td>
                            <td class="text-center overlay">{{ $row['services'] }}</td>
                            <td class="text-center overlay">{{ $row['status'] }}</td>
                        @if($user->role == 1)
                            <td class="text-center overlay">{{ $row['created_by'] }}</td>
                        @endif
                            <td class="text-center overlay">{{ $row['created_date'] }}</td>
                            <td class="text-center overlay">{{ $row['ex_client'] }}</td>
                            <td class="text-center overlay">{{ $row['locked'] }}</td>
                        <td class="text-center text-nowrap overlay" id="prospect-management">

                            @if ($user->role == 1 || in_array($user->userInfo->team->team_code,['busi','acc']) )
                                <a href="{{ route('prospect.view',$row['id']) }}"  class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('prospect.edit',$row['id']) }}"  class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                            @endif  

                            @if ($user->role == 1 || in_array($user->userInfo->team->team_code,['acc']) || $row['created_by'] == Auth::user()->UserInfo->f_name.' '.Auth::user()->UserInfo->l_name ) 
                                <a id="prospectDelete" data-prospectid="{{ $row['id'] }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>                          
                            @endif

                            @if ($row['opportunity_exist'] === false)
                                <a href="{{ route('prospect.convert.step1', $row['id']) }}" class="btn btn-sm btn-primary"><i class="fa fa-sign-out"></i> Convert</a>
                            @endif

                            <opportunity-prospect :prospect="{{ $row['id'] }}"></opportunity-prospect>


                        </td>
                    </tr>
                    @endforeach
                @else
                    @forelse($result as $row)
                        <tr>
                            <td class="text-center overlay">{{ $row['company'] }}</td>
                            <td class="text-center overlay">{{ $row['name'] }}</td>
                            <td class="text-center overlay">{{ $row['services'] }}</td>
                            <td class="text-center overlay">{{ $row['status'] }}</td>
                        @if($user->role == 1)
                            <td class="text-center overlay">{{ $row['created_by'] }}</td>
                        @endif
                            <td class="text-center overlay">{{ $row['ex_client'] }}</td>
                            <td class="text-center overlay">{{ $row['locked'] }}</td>
                            <td class="text-center overlay" id="prospect-management">
                                <a href="{{ route('prospect.view',$row['id']) }}"  class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                            </td>
                            {{-- <td class="set-reminder hidden"><a data-accountid="{{ $row['id'] }}" class="btn sign-btn btn-sm"> <i class="fa fa-clock-o"></i> Set a reminder </a></td> --}}
                        </tr>
                    @empty
                        <tr><td colspan="7" align="center">No prospects found</td></tr>
                    @endforelse
                @endif
            @else
               <tr><td colspan="7" align="center">No prospects found</td></tr>
            @endif
        </tbody>

      </table>

       <div class="row">
          <div class="col-md-1">
              <label for="limit_entry">Items per page: </label>
          </div>
          <div class="col-md-2">
              <div class="select-box">
                  <select id="limit_entry" name="limit_entry" class="w-150">
                        <option value="10" {{ !empty($items) ? $items->per_page == 10 ? 'selected' : '' : '' }}>10</option>
                        <option value="20" {{ !empty($items) ? $items->per_page == 20 ? 'selected' : '' : '' }}>20</option>
                        <option value="30" {{ !empty($items) ? $items->per_page == 30 ? 'selected' : '' : '' }}>30</option>
                        <option value="40" {{ !empty($items) ? $items->per_page == 40 ? 'selected' : '' : '' }}>40</option>
                  </select>
              </div>
          </div>
      </div>
        @include('prospect.modal.prospect_opportunity_modal', [
            'services'          => $services,
            'contract_types'    => $contract_types,
            'lead_statuses'     => $lead_statuses,
            'lead_sources'      => $lead_sources,
            'industries'        => $industries,
            'landings'          => $landings
        ])
    </div>


    <div class="pagination-box">
        @if(!empty($prospects))
            {{ $prospects->render() }}
        @endif
    </div>

  </div>
  
  </div>
  
</div>

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

    <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/prospect/list.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/prospect/prospect_opportunity.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>


    <script type="text/javascript">
        $('#start-picker').datetimepicker();
        $('#end-picker').datetimepicker();
    </script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>



@endsection

@endsection
