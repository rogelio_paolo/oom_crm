
@extends('layouts.master_v2')
@section('title')
CRM - Lead Conversion
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/fileinput.min.css') }}" />
<link rel="stylesheet" href="{{ asset('themes/explorer/theme.min.css') }}" />
@endsection

@section('main_content')

<div class="outer-data-box">
        <div class="row"> 
            <div class="">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="active">
                        <a href="{{ route('lead.convert.prospect',$lead->id) }}">
                        <p class="list-group-item-text">Lead Conversion</p>
                        </a>
                    </li>
                </ul>
                <form method="post" action="{{ route('lead.convert.prospect', $lead->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="lead_id" value="{{ $lead->id }}" />
                    <div class="main-data-box">
                        <div class="data-header-box clearfix">
                            <span class="header-h">Lead Information</span>
                        </div>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="form-group clearfix">
                                            <label for="status" class="col-md-4 control-label">Status *</label>
                                            <div class="col-md-6">
                                                <div class="select-box">
                                                    <select class="selectpicker form-control" id="status" name="status" data-live-search="true">
                                                        @foreach($statuses as $row)
                                                            @if ($row->systemdesc == 'Closed' || $row->systemdesc == 'Open')
                                                                @continue
                                                            @else
                                                                <option value="{{ $row->systemcode }}" {{ $lead->leadStatus->systemcode == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('status'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('status') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="company" class="col-md-4 control-label">Company Name *</label>
                                            <div class="col-md-6">
                                                <input id="company" type="text" class="form-control" name="company" value="{{ empty(old('company')) ? $lead->company : old('company') }}">
                                                @if ($errors->has('company'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('company') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="contract_value" class="col-md-4 control-label">Contract Value</label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">SGD</span>
                                                    <input id="contract_value" type="text" class="form-control" name="contract_value" value="{{ empty(old('contract_value')) ? $lead->contract_value : old('contract_value') }}" onkeypress="return isNumberKey(event);">
                                                </div>
                                                @if ($errors->has('contract_value'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_value') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group clearfix">
                                            <label for="f_name" class="col-md-4 control-label">First Name *</label>
                                            <div class="col-md-6">
                                                <input id="f_name" type="text" class="form-control" name="f_name" value="{{ empty(old('f_name')) ? $lead->f_name : old('f_name') }}">
                                                @if ($errors->has('f_name'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('f_name') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                                <label for="l_name" class="col-md-4 control-label">Last Name</label>
                                                <div class="col-md-6">
                                                    <input id="l_name" type="text" class="form-control" name="l_name" value="{{ empty(old('l_name')) ? $lead->l_name : old('l_name') }}">
                                                    @if ($errors->has('l_name'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('l_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        
                                        <div class="form-group clearfix">
                                            <label for="office_number" class="col-md-4 control-label">Office Number</label>
                                            <div class="col-md-6">
                                                <input id="office_number" type="text" class="form-control" name="office_number" value="{{ empty(old('office_number')) ? $lead->office_number : old('office_number') }}">
                                                @if ($errors->has('office_number'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('office_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                                <label for="contact_number" class="col-md-4 control-label">Mobile Number</label>
                                                <div class="col-md-6">
                                                    <input id="contact_number" type="text" class="form-control" name="contact_number" value="{{ empty(old('contact_number')) ? $lead->contact_number : old('contact_number') }}">
                                                    @if ($errors->has('contact_number'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contact_number') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        
                                        <div class="form-group clearfix">
                                            <label for="email" class="col-md-4 control-label">Email Address *</label>
                                            <div class="col-md-6">
                                                <input id="email" type="text" class="form-control" name="email" value="{{ empty(old('email')) ? json_encode(explode('|', $lead->email)) : old('email') }}">
                                                @if ($errors->has('email'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="designation" class="col-md-4 control-label">Designation</label>
                                            <div class="col-md-6">
                                                <input id="designation" type="text" class="form-control" name="designation" value="{{ empty(old('designation')) ? $lead->designation : old('designation') }}">
                                                @if ($errors->has('designation'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('designation') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="street_address" class="col-md-4 control-label">Address</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" id="street_address" name="street_address" rows="5">{!! empty(old('street_address')) ? $lead->street_address : old('street_address') !!}</textarea>
                                                @if ($errors->has('street_address'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('street_address') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="zip_code" class="col-md-4 control-label">Postal Code</label>
                                            <div class="col-md-6">
                                                <input id="zip_code" type="text" class="form-control" name="zip_code" value="{{ empty(old('zip_code')) ? $lead->zip_code : old('zip_code') }}">
                                                @if ($errors->has('zip_code'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('zip_code') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="city" class="col-md-4 control-label">City/Country</label>
                                            <div class="col-md-6">
                                                <input id="city" type="text" class="form-control" name="city" value="{{ empty(old('city')) ? $lead->city : old('city') }}">
                                                @if ($errors->has('city'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="industry" class="col-md-4 control-label">Industry *</label>
                                            <div class="col-md-6">
                                                    <select class="selectpicker form-control" id="industry" name="industry" data-live-search="true">
                                                        @foreach($industries as $industry)
                                                            <option value="{{ $industry->code }}" {{ !empty(old('industry')) && old('industry') == $industry->code ? 'selected' : ($industry->code == $lead->industry ? 'selected' : '') }}>{{ $industry->title }}</option>
                                                        @endforeach
                                                    </select>
                                                @if ($errors->has('city'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="website" class="col-md-4 control-label">Website URL</label>
                                            <div class="col-md-6">
                                                <input id="website" type="text" class="form-control" name="website" value="{{ empty(old('website')) ? $lead->website : old('website') }}">
                                                @if ($errors->has('website'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('website') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="services" class="col-md-4 control-label">Services </label>
                                            <div class="col-md-6">
                                                <div class="select-box">
                                                    <select class="multiselect-ui form-control elipsis-text" id="services" name="services[]" multiple="multiple">
                                                        @php $selected = explode('|',$lead->services) @endphp
                                                        @foreach($services as $row)
                                                            <option value="{{ $row->systemcode }}" {{ in_array($row->systemcode, $selected) ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                                            @if ($row->systemcode == 'SV999')
                                                                <input class="form-control {{ !empty(old('services')) && in_array('SV999',explode(',',old('services'))) ? '' : 'hidden' }} " type="text" id="service_other" name="service_other" value="{{ $prospect->service_other }}" />
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" id="services_null" name="services" />
                                                    <input type="hidden" id="services_array" name="services" value="{{ $lead->services }}"/>

                                                    @if ($errors->has('services'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('services') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group clearfix">
                                            <label for="other_services" class="col-md-4 control-label">Other Services</label>
                                            <div class="col-md-6">
                                                <input id="other_services" type="text" class="form-control" name="other_services" value="{{ empty(old('other_services')) ? $lead->other_services : old('other_services') }}">
                                                @if ($errors->has('other_services'))
                                                    <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('other_services') }}</span>
                                                @endif
                                            </div>
                                        </div>
                

                                        <div class="form-group clearfix">
                                            <label for="sources" class="col-md-4 control-label">Source of Leads </label>
                                            <div class="col-md-6">
                                                <div class="select-box">
                                                    <select class="selectpicker form-control" id="sources" name="source" data-live-search="true">
                                                        @foreach($sources as $row)
                                                        <option value="{{ $row->systemcode }}" {{ !empty(old('source')) && old('source') == $row->systemcode ? 'selected' : ($row->systemcode == $lead->source ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                                        @if ($row->systemcode == 'SL999')
                                                            <input class="form-control {{ !empty(old('source')) && old('source_other') == 'SL999' || !empty($lead->source) && $lead->source == 'SL999' ? '' : 'hidden' }} " type="text" id="source_other" name="source_other" value="{{ !empty(old('source_other')) ? old('source_other') : $lead->source_other }}" />
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('source'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('source') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="landing-page {{ !empty(old('source')) ? (old('source') == 'SL002' ? 'hidden' : '') : '' }}">
                                            <div class="form-group clearfix">
                                                <label for="landings" class="col-md-4 control-label">Landing Page </label>
                                                <div class="col-md-6">
                                                    <div class="select-box">
                                                        <select class="selectpicker form-control" id="landings" name="landing_page" data-live-search="true">
                                                        @foreach($landingpages as $row)
                                                        <option value="{{ $row->systemcode }}" {{ !empty(old('landing')) && old('landing_page') == $row->systemcode ? 'selected' : ($row->systemcode == $lead->landing_page ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                                        @if ($row->systemcode == 'LP999')
                                                        <input class="form-control {{ !empty(old('landing')) && in_array('SV999',old('landing')) ? '' : 'hidden' }} " type="text" id="landing_other" name="landing_other" value="{{ old('landing_other') }}" />
                                                        @endif
                                                        @endforeach
                                                        </select>
                                                        @if ($errors->has('landing_page'))
                                                        <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('landing_page') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label for="add_opportunity" class="col-md-4 control-label">Create opportunity?</label>
                                            <div class="col-md-6">
                                               <input name="create_opportunity" type="checkbox" {{ !empty(old('create_opportunity')) ? 'checked' : '' }} /> <i>If checked, will create opportunity and use the informations provided above.</i>
                                            </div>
                                        </div>

                                        <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success btn-md">Convert to Prospect</button>
                                            <a href="{{ route('lead.list') }}" class="btn btn-danger btn-md">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<div class="loader-overlay hidden"></div>
@section('scripts')
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script src="{{ asset('js/prospect/convert.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fileinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/explorer/theme.min.js') }}"></script>
<script src=""></script>
    @if (Session::has('account_validation'))
        @include('includes.account.validation')
    @endif
@endsection

@endsection