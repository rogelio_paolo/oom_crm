@extends('layouts.master_v2')

@section('title')
    Crm - Bulk Upload/Import of Leads
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('fonts_v1/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css_v1/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
@endsection

@section('main_content')

<div class="container pull-left">
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>

                <li><a href="{{ route('lobby.list') }}">Lobby</a></li>

                <li class="active">Bulk Upload/Import of Leads</li>
            </ol>
        </div>
    </div>
</div>


    <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

    <form class="outer-data-box" method="POST" action="{{ route('lobby.upload') }}" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="col-lg-8 col-lg-offset-2">
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-upload" aria-hidden="true"></i> Import Leads</span>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">

                    @if(Session::has('existingEmail'))
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>Count of leads that not inserted: {{ count(Session::get('existingEmail')) }}</strong>
                                @foreach(Session::get('existingEmail') as $row)
                                    <p>Company: {{ $row['company'] }} w/ Email address: {{ $row['emailaddress'] }} is already exist.</p>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if ( Session::has('error'))
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif 
                    
                    @if (count($errors) > 0)
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <div>
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif 

                    <div class="col-md-6">
                        <div class="form-box">
                            <label for="import">Select a file to import</label>
                            <input type="file" class="form-control" name="import" id="import">
                            <i>File allowed: .xlsm. Download the template <a href="{{ route('lobby.download') }}">here</a></i>
                            <p class="error-p lead">
                                <i>Note: Red labels are required fields.</i>
                            </p>
                            @if($errors->has('import'))
                                <span class="error-p lead"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('import') }}</span>
                            @endif
                        </div>
                    </div>

                        </div>
                        <div class="bottom-buttons clearfix pull-left">
                            <button id="btn-import" type="submit" class="cta-btn add-btn disable-btn" disabled="true"> Upload</button>
                            <a href="{{ route('lobby.list') }}" class="cta-btn cancel-btn">Back to Lobby List</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </form>
    
   

    @section('scripts')
        <script type="text/javascript">
            $(document).on('change', '#import', function() {
                $('#btn-import').removeClass('disable-btn').prop("disabled",false);
            });
        </script>
    @endsection
@endsection