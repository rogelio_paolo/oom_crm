@extends('layouts.master_v2')

@section('title')
    Crm - Lobby Page
@endsection

@section('modals')
    @include('includes.lobbymodal')
@endsection

@section('main_content')

<div id="columns" class="outer-data-box">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-book" aria-hidden="true"></i> Lobby / Open List</span>

      <div class="data-search-box">

         <span>
          Search:
          <div class="search-box">
            <input type="text" class="form-control search-area" name="searchLobby" id="searchLobby" placeholder="Search" onkeypress="triggerSearch(event)">
            <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </span> 

      </div>

    </div>

    <div class="data-buttons-box clearfix">

      <div class="filter-box">
        
        <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>

        <a href="" id="filter-column"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>

      </div>

      <div id="filter-container" class="filter-container hidden">
            <br/><br/>
            <div class="row">
                <div id="filter-level-1" class="col-md-3">
                    <label for="filter">Filter by</label>
                    <select id="filter" class="form-control">
                        <option value=""></option>
                        <option value="created">Created</option>
                    </select>
                </div>
                
                <div id="filter-level-2" class="col-md-3 hidden">
                    <label for="filter-2">Value</label>
                    <select id="filter-2" class="form-control">
                        <option value=""></option>
                    </select>
                </div>

                <div class="filter-wrapper-3" class="col-md-3">
                    <br/>
                    <button id="btn-filter" class="btn btn-danger" disabled>Clear</button>
                </div>

            </div>
        </div>

      <div class="buttons-box">
          
            <button data-toggle="modal" data-target="#modifyColumn" class="btn btn-info pull-right"><i class="glyphicon glyphicon-filter"></i> Modify Column</button>

      </div>

      <div class="buttons-box">
          
            <a href="{{ route('lobby.upload') }}" class="cta-btn-warn warning-btn"><i class="fa fa-upload"> </i> Import Leads</a>
  
        </div>

    </div>

    @if (Session::has('insert'))
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong>Count of leads that inserted: {{ count(Session::get('insert')) }}</strong>
                @foreach(Session::get('insert') as $row)
                    <p>Company: {{ $row['company'] }} w/ Email address: {{ $row['emailaddress'] }} is inserted.</p>
                @endforeach
            </div>
        </div>
    @endif

    @if(Session::has('existingEmail'))
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong>Count of leads that not inserted: {{ count(Session::get('existingEmail')) }}</strong>
                @foreach(Session::get('existingEmail') as $row)
                    <p>Company: {{ $row['company'] }} w/ Email address: {{ $row['emailaddress'] }} is already exist.</p>
                @endforeach
            </div>
        </div>
    @endif

    <form action="{{ route('lobby.open') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        
        <div class="table-box">
        
        <i id="loader" class="loader hidden"></i>
            
        <table class="table main-table" id="lobby-management">

            <thead>
                <tr>
                    <th class="text-center overlay company_name">Company Name <a href="" class="sort-lobby" data-sortby="company"><i id="sort-lobby" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                    <th class="text-center overlay name">Client Name <a href="" class="sort-lobby" data-sortby="name"><i id="sort-lobby" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                    <th class="text-center overlay contact">Client Number</th>
                    <th class="text-center overlay email">Client Email <a href="" class="sort-lobby" data-sortby="email"><i id="sort-lobby" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                    <th class="text-center overlay status">Status <a href="" class="sort-lobby" data-sortby="status"><i id="sort-lobby" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                    <th class="text-center overlay created">Created <a href="" class="sort-lobby active" data-sortby="created"><i id="sort-lobby" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> </th>
                    <th class="text-center overlay">
                        <input id="check-all" class="check-all" type="checkbox"> Claim
                    </th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($lobby))
                    @if(!in_array($user->role,[3,4]) || in_array($user->userInfo->team->team_code, ['busi','acc']) )
                        @foreach($lobby as $row)
                        <tr>
                            <td class="text-center overlay company_name">{{ $row['company'] }}</td>
                            <td class="text-center overlay name">{{ $row['name'] }}</td>
                            <td class="text-center overlay contact">{{ $row['contact'] }}</td>
                            <td class="text-center overlay email">{{ $row['email'] }}</td>
                            <td class="text-center overlay status">{{ $row['status'] }}</td>
                            <td class="text-center overlay created">{{ $row['created'] }}</td>
                            <td class="text-center overlay" id="lobby-management">
                                @if($row['status'] == 'Open')
                                    <input type="checkbox" class="check-open" name="check-open[]" value="{{ $row['id'] }}">
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    @else
                        @forelse($lobby as $row)
                            <tr>
                                <td class="text-center overlay company_name">{{ $row['company'] }}</td>
                                <td class="text-center overlay name">{{  $row['name']  }}</td>
                                <td class="text-center overlay contact">{{ $row['contact'] }}</td>
                                <td class="text-center overlay email">{{ $row['email'] }}</td>
                                <td class="text-center overlay status">{{ $row['status'] }}</td>
                                <td class="text-center overlay created">{{ $row['created'] }}</td>
                                <td class="text-center overlay" id="lobby-management">
                                    @if($row['status'] == 'Open')
                                        <input type="checkbox" class="check-open" name="check-open[]" value="{{ $row['id'] }}">
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="7" align="center">No open leads/prospect/account found</td></tr>
                        @endforelse
                    @endif
                @else
                <tr><td colspan="7" align="center">No open leads/prospect/account found</td></tr>
                @endif
            

            @if(!empty($lobby))
                <tr>
                    <td class="company_name overlay "></td>
                    <td class="name overlay"></td>
                    <td class="contact overlay"></td>
                    <td class="email overlay"></td>
                    <td class="status overlay"></td>
                    <td class="created overlay"></td>
                    <td class="text-center overlay" id="lobby-management"> 
                        <button disabled='true' onclick="confirmClaim(event)" id="claim-btn" type="submit" class="btn btn-success">
                            <i class="fa fa-ticket"> </i> Claim
                        </button>
                    </td>
                </tr>
            @endif

            </tbody>

        </table>

        <div class="row">
                <div class="col-md-1">
                    <label for="limit_entry">Items per page: </label>
                </div>
                <div class="col-md-2">
                    <div class="select-box">
                        <select id="limit_entry" name="limit_entry" class="w-150">
                            <option value="10" {{ !empty($items) ? $items->per_page == 10 ? 'selected' : '' : '' }}>10</option>
                            <option value="20" {{ !empty($items) ? $items->per_page == 20 ? 'selected' : '' : '' }}>20</option>
                            <option value="30" {{ !empty($items) ? $items->per_page == 30 ? 'selected' : '' : '' }}>30</option>
                            <option value="40" {{ !empty($items) ? $items->per_page == 40 ? 'selected' : '' : '' }}>40</option>
                        </select>
                    </div>
            </div>
        </div>

        </div>
    
    </form>

    <div class="pagination-box">
        {{ $data->render() }}
    </div>

  </div>
  </div>
  
</div>

@section('scripts')
<script type="text/javascript" src="{{ asset('js/lobby/list.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>



@endsection

@endsection
