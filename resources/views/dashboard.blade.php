@extends('layouts.master_v2')

@section('title')
    Crm - Dashboard
@endsection

@section('main_content')
<div class="outer-data-box" style="padding: 0 25px">
    @if($user->role != 3)
    <div class="main-data-box">
        <div class="data-header-box clearfix">
            <span class="header-h">Accounts Graph</span>
            @if (Auth::user()->role == 1)
            <div class="data-search-box">
                Filter By:
                <div class="select-box">
                    <select class="form-control" id="filterAccounts" name="filterAccounts">
                        <option value="all" selected>All</option>
                        <option value="date">Date</option>
                        <option value="holder">Holder</option>
                    </select>
                </div>
                <div id="filterButtons" class="form-inline pull-right graph-filter">
                    <button class="btn btn-default" id="FilterAccountAll"> FILTER </button>
                    <a href="{{ route('filter.accounts.export',['all','null','null','null']) }}" class="btn btn-success" id="ExportAccountAll"> Export to CSV </a>
                </div>
                <div id="filter-by-date" class="form-inline pull-right graph-filter hidden">
                From <input type="text" class="form-control datepicker" id="filterDateFrom" name="filterDateFrom" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                To <input type="text" class="form-control datepicker" id="filterDateTo" name="filterDateTo" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                <button class="btn btn-default" id="FilterAccountByDate"> FILTER </button>
                <a href="" class="btn btn-success" id="ExportAccountByDate" disabled="disabled"> Export to CSV </a>
                </div>
                <div id="filter-by-holder" class="form-inline pull-right graph-filter hidden">
                    <div class="select-box">
                        <select class="form-control" id="account-holders" name="filterAccounts">
                            @foreach($holders as $row)
                                <option value="{{ $row->emp_id }}">{{ $row->f_name.' '.$row->l_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-default" id="FilterAccountByHolder"> FILTER </button>
                    <a href="" class="btn btn-success" id="ExportAccountByHolder" disabled="disabled"> Export to CSV </a>
                </div>
            </div>
            @endif
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel">
                <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">
                        <div class="row">
                            @if ($countAccounts > 0)
                                <div id="accounts-charts">
                                    <div id="chart-total-accounts" class="col-lg-4 col-sm-12 graph-md">
                                        <div>{!! $totalAccounts->container() !!}</div>
                                    </div>
                                    
                                    <div id="chart-accounts-perstatus" class="col-lg-4 col-sm-12 graph-md">
                                        <div>{!! $accountsPerStatus->container() !!}</div>
                                    </div>
                                    @if ($user->role == 1 || $user->userInfo->team->team_code == 'acc')
                                        <div id="chart-accounts-perpackage" class="col-lg-4 col-sm-12 graph-md">
                                            <div>{!! $accountsPerPackage->container() !!}</div>
                                        </div>
                                    @endif
                                </div>
                            @else
                                <div class="col-md-12 text-center">
                                    No graphs to show for Accounts
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($user->role == 1 || $user->userInfo->team->team_code == 'busi')
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h">Leads Graph</span>
                @if (Auth::user()->role == 1)
                <div class="data-search-box">
                    Filter By:
                    <div class="select-box">
                        <select class="form-control" id="filterLeads" name="filterLeads">
                            <option value="all" selected>All</option>
                            <option value="date">Date</option>
                            <option value="sales">Sales</option>
                        </select>
                    </div>
                    <div id="leadButtons" class="form-inline pull-right graph-filter">
                        <button class="btn btn-default" id="FilterLeadAll"> FILTER </button>
                        <a href="{{ route('filter.leads.export',['all','null','null','null']) }}" class="btn btn-success" id="ExportLeadAll"> Export to CSV </a>
                    </div>
                    <div id="lead-by-date" class="form-inline pull-right graph-filter hidden">
                    From <input type="text" class="form-control datepicker" id="leadDateFrom" name="leadDateFrom" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    To <input type="text" class="form-control datepicker" id="leadDateTo" name="leadDateTo" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                    <button class="btn btn-default" id="FilterLeadsByDate"> FILTER </button>
                    <a href="" class="btn btn-success" id="ExportLeadsByDate" disabled="disabled"> Export to CSV </a>
                    </div>
                    <div id="lead-by-sales" class="form-inline pull-right graph-filter hidden">
                        <div class="select-box">
                            <select class="form-control" id="lead-sales" name="filterLeads">
                                @foreach($sales as $row)
                                    <option value="{{ $row->UserInfo->userid }}">{{ $row->f_name.' '.$row->l_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-default" id="FilterLeadsBySales"> FILTER </button>
                        <a href="" class="btn btn-success" id="ExportLeadsBySales" disabled="disabled"> Export to CSV </a>
                    </div>
                </div>
                @endif
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                    <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">
            @if ($countLeads > 0)
                <div id="leads-charts">
                    <div class="row">
                        <div id="chart-total-leads" class="col-lg-4 col-sm-12 graph-md">
                            <div>{!! $totalLeads->container() !!}</div>
                        </div>
                        <div id="chart-leads-perstatus" class="col-lg-4 col-sm-12 graph-md">
                            <div>{!! $leadsPerStatus->container() !!}</div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-12 text-center">
                    No graphs to show for Leads
                </div>
            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($user->role == 1 || $user->role == 3)
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h">Employees Graph</span>
            </div>
            
            @if ($countEmployees > 0)
                <div class="row">
                    <div class="col-lg-4 col-sm-12 graph-md">
                        <div>{!! $totalEmployees->container() !!}</div>
                    </div>
                    <div class="col-lg-4 col-sm-12 graph-md">
                        <div>{!! $employeesPerStatus->container() !!}</div>
                    </div>
                    <div class="col-lg-4 col-sm-12 graph-md">
                        <div>{!! $employeesPerDepartment->container() !!}</div>
                    </div>
                </div>
            @else
                <div class="col-md-12 text-center">
                    No graphs to show for Employees
                </div>
            @endif
        </div>
    @endif
</div>

@section('scripts')
    <script src="{{ asset('js/highcharts.js') }}"></script>
    <script src="{{ asset('js/highcharts3d.js') }}"></script>
    <script src="{{ asset('js/highcharts-module.js') }}"></script>
    
    {!! $totalAccounts->script() !!}
    {!! $accountsPerStatus->script() !!}

    @if ($user->role == 1 || $user->userInfo->team->team_code == 'busi')
        {!! $accountsPerPackage->script() !!}
        {!! $totalLeads->script() !!}
        {!! $leadsPerStatus->script() !!}
    @endif

    @if( $user->role == 1 || $user->role == 3 )
        {!! $totalEmployees->script() !!}
        {!! $employeesPerStatus->script() !!}
        {!! $employeesPerDepartment->script() !!}
    @endif

    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    
@endsection



@endsection
