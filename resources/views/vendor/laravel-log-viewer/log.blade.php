@extends('layouts.master_v2')
@section('title')
Crm - Errors Logs
@endsection
@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style>
  h1 {
    font-size: 1.5em;
    margin-top: 0;
  }

  #table-log {
      font-size: 0.85rem;
  }

  .sidebar {
      font-size: 0.85rem;
      line-height: 1;
  }

  .btn {
      font-size: 0.7rem;
  }


  .stack {
    font-size: 0.85em;
  }

  .date {
    min-width: 75px;
  }

  .text {
    word-break: break-all;
  }

  a.llv-active {
    z-index: 2;
    background-color: #f5f5f5;
    border-color: #777;
  }

  .list-group-item {
    word-wrap: break-word;
  }
</style>
@endsection
@section('main_content')
<div class="error-logs-container">
  <div class="table-container">
    @if ($logs === null)
      <div>
        Log file >50M, please download it.
      </div>
    @else
      <table id="table-log" class="table table-striped">
        <thead>
        <tr>
          <th>Level</th>
          <th>Context</th>
          <th>Date</th>
          <th>Content</th>
        </tr>
        </thead>
        <tbody>

        @foreach($logs as $key => $log)
          <tr data-display="stack{{{$key}}}">
            <td class="text-{{{$log['level_class']}}}"><span class="fa fa-{{{$log['level_img']}}}"
                                                              aria-hidden="true"></span> &nbsp;{{$log['level']}}</td>
            <td class="text">{{$log['context']}}</td>
            <td class="date">{{{$log['date']}}}</td>
            <td class="text">
              @if ($log['stack']) <button type="button" class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                                      data-display="stack{{{$key}}}"><span
                    class="fa fa-search"></span></button>@endif
              {{{$log['text']}}}
              @if (isset($log['in_file'])) <br/>{{{$log['in_file']}}}@endif
              @if ($log['stack'])
                <div class="stack" id="stack{{{$key}}}"
                      style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                </div>@endif
            </td>
          </tr>
        @endforeach

        </tbody>
      </table>
    @endif
    <div class="p-3">
      @if($current_file)
        <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}"><span class="fa fa-download"></span>
          Download file</a>
        -
        <a id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}"><span
              class="fa fa-trash"></span> Delete file</a>
        @if(count($files) > 1)
          -
          <a id="delete-all-log" href="?delall=true"><span class="fa fa-trash"></span> Delete all files</a>
        @endif
      @endif
    </div>
  </div>
</div>
@section('scripts')
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function () {
    $('.table-container tr').on('click', function () {
      $('#' + $(this).data('display')).toggle();
    });
    $('#table-log').DataTable({
      "order": [1, 'desc'],
      "stateSave": true,
      "stateSaveCallback": function (settings, data) {
        window.localStorage.setItem("datatable", JSON.stringify(data));
      },
      "stateLoadCallback": function (settings) {
        var data = JSON.parse(window.localStorage.getItem("datatable"));
        if (data) data.start = 0;
        return data;
      }
    });
    $('#delete-log, #delete-all-log').click(function () {
      return confirm('Are you sure?');
    });
  });
</script>
@endsection
@endsection