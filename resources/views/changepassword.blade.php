@extends('layouts.master_v2')

@section('title')
  Crm - Change Password
@endsection

@section('main_content')
<div class="outer-data-box">
    <div class="row"> 
        <div class="col-lg-6 col-lg-offset-3">
            <form class="form-horizontal" method="POST" action="{{ route('user.changepassword') }}">
                {{ csrf_field() }}
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Change current password</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">

                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <div class="form-group clearfix">
                                        <label for="email" class="col-md-4 control-label">Old Password</label>

                                        <div class="col-md-6">
                                            <input id="oldpass" type="password" class="form-control" name="oldpass" value="{{ old('oldpass') }}">
                                            
                                            @if ($errors->has('oldpass'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('oldpass') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="newpass" class="col-md-4 control-label">New Password</label>

                                        <div class="col-md-6">
                                            <input id="newpass" type="password" class="form-control" name="newpass" value="{{ old('newpass') }}">

                                            @if ($errors->has('newpass'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('newpass') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="confirm_newpass" class="col-md-4 control-label">Confirm New Password</label>

                                        <div class="col-md-6">
                                            <input id="confirm_newpass" type="password" class="form-control" name="confirm_newpass" value="{{ old('confirm_newpass') }}">

                                            @if ($errors->has('confirm_newpass'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('confirm_newpass') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-success">
                                                Save Changes
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
