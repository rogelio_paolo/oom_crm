@extends('layouts.master')

@section('title')
  Crm - Reset Password
@endsection

@section('content')

<div class="wrapper login-box valign">

<div class="container">
<div class="row">

  <div class="col-md-8 col-md-offset-2">

    <div class="login-form clearfix">

      <div class="login-form--inner logo-box valign">

        <img class="logo-brand" src="{{ asset('images/logo.png') }}" alt="">

      </div>

      <i id="loader" class="loader-overlay hidden"></i>

      <form class="login-form--inner login-bg" method="POST" action="">
        {{ csrf_field() }}
        
        <span class="login-header">Reset Password</span>

        <div class="form-box reset-email">

          <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter your email address">

          @if ($errors->has('email'))
              <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
          @endif

        </div>

        <button id="reset-password" type="submit" class="cta-btn sign-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
            RESET PASSWORD
        </button>

        <div class="back-to-login text-center">
        <p>
            <a class="btn" href="{{ route('user.login') }}"> <i class="fa fa-reply"></i> Return to login page </a>
        </p>
        </div>

      </form>

    </div>

  </div>

</div>
</div>

</div><!--end of wrapper-->

@section('scripts')
    <script src="{{ asset('js/resetpassword.js') }}"></script>
@endsection

@endsection
