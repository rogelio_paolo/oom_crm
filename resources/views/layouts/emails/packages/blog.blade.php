<ul>
    <li style="text-align: left;">Total Number of Blog Post: 
        <b>
            @if(!empty($account->blog->blog_total_post))
                {{ $account->blog->blog_total_post }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Number of Blog Post Per Month: 
        <b>    
            @if(!empty($account->blog->blog_monthly_post))
                {{ $account->blog->blog_monthly_post }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Content Brief and Writing Style: 
        <p>
            @if(!empty($account->blog->blog_content_brief))
                {!! nl2br($account->blog->blog_content_brief) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

</ul>