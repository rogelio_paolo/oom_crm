<ul>
    <li style="text-align: left;">Target Market: 
        <b>
            @foreach(explode('|',$account->fb->fb_target_country) as $row)
                @php $fb_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $fb_countries->country . ', ' }}
            @endforeach
        </b>
    </li>

    <li style="text-align: left;">List Account as: 
        <b>    
            @if(!empty($account->fb->fb_list_account))
                    {{ $account->fb->listing->systemdesc }}
                @else
                    Not Specified
                @endif
        </b>
    </li>

    <li style="text-align: left;">Media Budget Payment: 
        <b>    
            @if(!empty($account->fb->fb_payment))
                 {{ $account->fb->mode->systemdesc }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Media Channels: 
        <b>
            @if(!empty($account->fb->fb_media_channel))
                 @foreach(explode('|',$account->fb->fb_media_channel) as $row)
                    {{ \App\System::where('systemcode',$row)->first()->systemdesc . ', ' }}
                 @endforeach
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Brief & Expectations: 
        <p>
            @if(!empty($account->fb->fb_campaign))
                 {!! nl2br($account->fb->fb_campaign) !!}
            @else
                 Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Total Campaign Budget: 
        <b>
            @if(!empty($account->fb->total_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->total_budget }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Monthly Campaign Budget: 
        <b>
            @if(!empty($account->fb->monthly_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->monthly_budget }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Daily Campaign Budget: 
        <b>
            @if(!empty($account->fb->daily_budget))
            {{ $account->fb->fb_currency.' '.$account->fb->daily_budget }}
        @else
            Not Specified
        @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->fb->fb_duration))
             {{ $account->fb->fb_duration != 'CD999' ? $account->fb->duration->systemdesc : $account->fb->fb_duration_other.' '.$account->fb->duration_interval->systemdesc  }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->fb->fb_campaign_dt_from))
                 {{ $account->fb->fb_campaign_dt_from }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->fb->fb_campaign_dt_to))
                 {{ $account->fb->fb_campaign_dt_to }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Gender: 
        <b>
            @if(!empty($account->fb->gender))
                {{ $account->fb->gender == 'A' ? 'All' : ($account->fb->gender == 'M' ? 'Male' : 'Female') }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Age Group: 
        <b>
            @if(!empty($account->fb->age_from))
                 {{ $account->fb->age_from }} to {{ $account->fb->age_to }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Interest: 
        <p>
            @if(!empty($account->fb->interest))
                {!! nl2br($account->fb->interest) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

</ul>