<ul>
    <li style="text-align: left;">WeChat OA Setup?: 
        <b>
            @if($account->wechat->has_wechat_type == 1)
                Yes
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">WeChat Type: 
        <b>    
            @if(!empty($account->wechat->wechat_type))
                {{ $account->wechat->wechat_type == 'serviced' ? 'Serviced' : 'Subscription' }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Requires Menu Setup?: 
        <b>
            @if($account->wechat->has_wechat_menu_setup == '1')
                Yes 
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">Requires WeChat Advertising?: 
        <b>
            @if(!empty($account->wechat->wechat_location))
                Yes 
            @else
                No
            @endif
        </b>
    </li>

    @if(!empty($account->wechat->wechat_location))
        <li style="text-align: left;">WeChat Advertising Type: 
            <b>
                @if(!empty($account->wechat->wechat_advertising_type))
                    @foreach(explode('|',$account->wechat->wechat_advertising_type) as $row)
                        {{ \App\System::where('systemcode',$row)->first()->systemdesc . ', ' }}
                    @endforeach
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Location: 
            <b>
                @if(!empty($account->wechat->wechat_location))
                    {{ $account->wechat->wechat_location }}
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Age Group: 
            <b>
                @if(!empty($account->wechat->wechat_age_from))
                    {{ $account->wechat->wechat_age_from }} to {{ $account->wechat->wechat_age_to }}
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Marital Status: 
            <b>
                @if(!empty($account->wechat->wechat_marital))
                    {{ $account->wechat->wechat_marital }}
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Gender: 
            <b>
                @if(!empty($account->wechat->wechat_gender))
                    {{ $account->wechat->wechat_gender == 'M' ? 'Male' : ($account->wechat->wechat_gender == 'F' ? 'Female' : 'Not Specified') }}
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Handphone Device: 
            <b>
                @if(!empty($account->wechat->wechat_handphone))
                    {{ $account->wechat->wechat_handphone == 'android' ? 'Android' : 'iOS' }}
                @else
                    Not Specified
                @endif
            </b>
        </li>

        <li style="text-align: left;">Education: 
            <b>
                @if(!empty($account->wechat->wechat_education))
                    {{ $account->wechat->wechat_education }}
                @else
                    Not Specified
                @endif
            </b>
        </li>
    @endif
</ul>