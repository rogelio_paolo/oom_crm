<ul>
    <li style="text-align: left;">Baidu Account User ID: 
        <b>
            @if(!empty($account->baidu->baidu_account))
                {{ $account->baidu->baidu_account }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Target Market: 
        <b>    
            @foreach(explode('|',$account->baidu->baidu_target_market) as $row)
                @php $baidu_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $baidu_countries->country . ', ' }}
            @endforeach
        </b>
    </li>

    <li style="text-align: left;">City: 
        <b>
            @if(!empty($account->baidu->baidu_city))
                {{ $account->baidu->baidu_city }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Media Channels: 
        <b>
            @if(!empty($account->baidu->baidu_media_channel))
                @foreach(explode('|',$account->baidu->baidu_media_channel) as $row)
                    {{ \App\System::where('systemcode',$row)->first()->systemdesc . ', ' }}
                @endforeach
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Brief & Expectations: 
        <p>
            @if(!empty($account->baidu->baidu_campaign))
                {!! nl2br($account->baidu->baidu_campaign) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Ad Scheduling: 
        <b>
            @if($account->baidu->baidu_ad_scheduling == '1')
                Yes - 
                {!! nl2br($account->baidu->baidu_ad_scheduling_remark) !!}
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->baidu->baidu_duration))
                {{ $account->baidu->baidu_duration != 'CD999' ? $account->baidu->campaign_duration->systemdesc : $account->baidu->baidu_duration_other.' '.$account->baidu->campaign_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->baidu->baidu_campaign_datefrom))
                {{ $account->baidu->baidu_campaign_datefrom }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->baidu->baidu_campaign_dateto))
                {{ $account->baidu->baidu_campaign_dateto }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Total Campaign Budget: 
        <b>
            @if(!empty($account->baidu->baidu_budget))
                {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Monthly Campaign Budget: 
        <b>
            @if(!empty($account->baidu->baidu_monthly_budget))
                {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_monthly_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Daily Campaign Budget: 
        <b>
            @if(!empty($account->baidu->baidu_daily_budget))
                {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_daily_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>
</ul>