<ul>
    <li style="text-align: left;">Location: 
        <b>
            @if(!empty($account->weibo->weibo_location))
                {{ $account->weibo->weibo_location }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Weibo Account User ID: 
        <b>    
            @if(!empty($account->weibo->weibo_account))
                {{ $account->weibo->weibo_account }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Requires New Blue V Weibo Setup?: 
        <b>
            @if($account->weibo->has_blue_v_weibo == '1')
                Yes
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Brief & Expectations: 
        <p>
            @if(!empty($account->weibo->weibo_campaign))
                {!! nl2br($account->weibo->weibo_campaign) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Ad Scheduling: 
        <b>
                @if($account->weibo->weibo_ad_scheduling == '1')
                Yes - 
                {!! nl2br($account->weibo->weibo_ad_scheduling_remark) !!}
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->weibo->weibo_duration))
                {{ $account->weibo->weibo_duration != 'CD999' ? $account->weibo->campaign_duration->systemdesc : $account->weibo->weibo_duration_other.' '.$account->weibo->campaign_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->weibo->weibo_campaign_datefrom))
                {{ $account->weibo->weibo_campaign_datefrom }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->weibo->weibo_campaign_dateto))
                {{ $account->weibo->weibo_campaign_dateto }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Total Campaign Budget: 
        <b>
            @if(!empty($account->weibo->weibo_budget))
                {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Monthly Campaign Budget: 
        <b>
            @if(!empty($account->weibo->weibo_monthly_budget))
                {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_monthly_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Daily Campaign Budget: 
        <b>
            @if(!empty($account->weibo->weibo_daily_budget))
                {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_daily_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>
</ul>