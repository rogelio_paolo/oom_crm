<ul>
    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->social->social_duration))
                {{ $account->social->social_duration != 'CD999' ? $account->social->campaign_duration->systemdesc : $account->social->social_duration_other.' '.$account->social->campaign_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Number of Blog Post Per Month: 
        <b>    
            @if(!empty($account->social->social_monthly_post))
                {{ $account->social->social_monthly_post }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Content Brief and Writing Style: 
        <p>
            @if(!empty($account->social->social_content_brief))
                {!! nl2br($account->social->social_content_brief) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Any FB Media Budget to boost post?: 
        <b>    
            @if(!empty($account->social->social_media_budget))
                Yes - ${{ $account->social->social_media_budget }}
            @else
                No
            @endif
        </b>
    </li>

</ul>