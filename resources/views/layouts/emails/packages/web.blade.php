<ul>
    <li style="text-align: left;"Project Type: 
        <b>
            @if(!empty($account->web->project_type))
                {{ $account->web->project->systemdesc }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">No. of Pages: 
        <b>    
            @if(!empty($account->web->pages))
                {{ $account->web->pages }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Objective: 
        <p>
            @if(!empty($account->web->objective))
                 {!! nl2br($account->web->objective) !!}
             @else
                 Not Specified
             @endif
        </p>
    </li>

    <li style="text-align: left;">Brief information about the Company: 
        <p>
            @if(!empty($account->web->company_brief))
                {!! nl2br($account->web->company_brief) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Target Audience: 
        <p>
            @if(!empty($account->web->target_audience))
                {!! nl2br($account->web->target_audience) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Existing Website (URL): 
        <b>
            @if(!empty($account->web->existing_web_url))
                {{ $account->web->existing_web_url }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Preferred Reference Website: 
        <p>
            @if(!empty($account->web->reference_web))
                {!! nl2br($account->web->reference_web) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align:left;">Look and Feel: 
        <p>
            @if(!empty($account->web->web_ui))
                 {!! nl2br($account->web->web_ui) !!}
            @else
                 Not Specified
            @endif
        </p>
    </li>

    <li style="text-align:left;">Color Scheme: 
        <p>
            @if(!empty($account->web->color_scheme))
                 {!! nl2br($account->web->color_scheme) !!}
             @else
                 Not Specified
             @endif
        </p>
    </li>

    <li style="text-align:left;">Special Functionality: 
        <p>
            @if(!empty($account->web->functionality))
                {!! nl2br($account->web->functionality) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

</ul>