<ul>
    <li style="text-align: left;">Target Market: 
        <b>
            @foreach(explode('|',$account->seo->seo_target_countries) as $row)
                @php $seo_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $seo_countries->country . ', ' }}
            @endforeach
        </b>
    </li>

    <li style="text-align: left;">No. of Keywords: 
        <b>    
            @if(!empty($account->seo->keyword_themes))
                 {{ $account->seo->keyword_themes != 'NK999' ? $account->seo->keywords->systemdesc : $account->seo->keyword_themes_other }}
            @else
                 Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Keyword Theme Focus: 
        <p>
            @if(!empty($account->seo->keyword_focus))
                {!! nl2br($account->seo->keyword_focus) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Keyword Maintenance Project: 
        <p>
            @if(!empty($account->seo->keyword_maintenance))
                {!! !is_int($account->seo->keyword_maintenance) ? nl2br($account->seo->keyword_maintenance) : 'No' !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Campaign Brief & Expectations: 
        <p>
            @if(!empty($account->seo->seo_campaign))
                 {!! nl2br($account->seo->seo_campaign) !!}
             @else
                 Not Specified
             @endif
        </p>
    </li>

    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->seo->seo_duration))
                {{ $account->seo->seo_duration != 'CD999' ? $account->seo->duration->systemdesc : $account->seo->seo_duration_other.' '.$account->seo->duration_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">SEO Total Fee: 
        <b>    
            @if(!empty($account->seo->seo_total_fee))
                {{ $account->seo->seo_currency.' '.$account->seo->seo_total_fee }}
            @else
                Not Specified
            @endif
        </b>
    </li>

</ul>

    {{--  <li style="text-align: left;">Total Campaign Budget: 
        <b>
            @if(!empty($account->fb->total_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->total_budget }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Monthly Campaign Budget: 
        <b>
            @if(!empty($account->fb->monthly_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->monthly_budget }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Daily Campaign Budget: 
        <b>
            @if(!empty($account->fb->daily_budget))
            {{ $account->fb->fb_currency.' '.$account->fb->daily_budget }}
        @else
            Not Specified
        @endif
        </b>
    </li>  --}}

    {{--  <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->fb->fb_campaign_dt_from))
                 {{ $account->fb->fb_campaign_dt_from }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->fb->fb_campaign_dt_to))
                 {{ $account->fb->fb_campaign_dt_to }}
             @else
                 Not Specified
             @endif
        </b>
    </li>  --}}

    {{--  <li style="text-align: left;">Gender: 
        <b>
            @if(!empty($account->fb->gender))
                {{ $account->fb->gender == 'A' ? 'All' : ($account->fb->gender == 'M' ? 'Male' : 'Female') }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Age Group: 
        <b>
            @if(!empty($account->fb->age_from))
                 {{ $account->fb->age_from }} to {{ $account->fb->age_to }}
             @else
                 Not Specified
             @endif
        </b>
    </li>

    <li style="text-align: left;">Interest: 
        <p
            @if(!empty($account->fb->interest))
                {!! nl2br($account->fb->interest) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>  --}}
