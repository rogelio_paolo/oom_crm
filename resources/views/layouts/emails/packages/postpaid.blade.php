<ul>
    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->postpaid->postpaid_duration))
                {{ $account->postpaid->postpaid_duration != 'CD999' ? $account->postpaid->campaign_duration->systemdesc : $account->postpaid->postpaid_duration_other.' '.$account->postpaid->campaign_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->postpaid->postpaid_campaign_datefrom))
                {{ $account->postpaid->postpaid_campaign_datefrom }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->postpaid->postpaid_campaign_dateto))
                {{ $account->postpaid->postpaid_campaign_dateto }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Media Budget Utilized: 
        <b>
            @if(!empty($account->postpaid->postpaid_media_budget))
                $ {{ $account->postpaid->postpaid_media_budget  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">AM Fee: 
        <b>
            @if(!empty($account->postpaid->postpaid_am_fee))
                {{ $account->postpaid->postpaid_am_fee . '%'}}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">AM Fee Amount: 
        <b>
            @if(!empty($account->postpaid->postpaid_am_fee_amount))
                {{ $account->postpaid->postpaid_am_fee_amount  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Is Media Budget Paid through OOm?: 
        <b>    
            @if($account->postpaid->is_media_budget_paid == 1)
                Yes - {{ '$ ' .$account->postpaid->postpaid_total }} (Media Budget Utilized + AM Fee Amount)
            @else
                No -  {{ '$ ' . $account->postpaid->am_fee_amount }} (AM Fee Amount)
            @endif
        </b>
    </li>

</ul>