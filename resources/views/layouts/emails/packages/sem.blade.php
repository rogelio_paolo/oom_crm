<ul>
    <li style="text-align: left;">Target Market: 
        <b>
            @foreach(explode('|',$account->sem->target_market) as $row)
                @php $sem_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $sem_countries->country . ', ' }}
            @endforeach
        </b>
    </li>

    <li style="text-align: left;">List Account as: 
        <b>    
            @if(!empty($account->sem->list_account))
                {{ $account->sem->listing->systemdesc }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Media Budget Payment: 
        <b>    
            @if(!empty($account->sem->payment_method))
                {{ $account->sem->mode->systemdesc }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Media Channels: 
        <b>
            @if(!empty($account->sem->media_channel))
                @foreach(explode('|',$account->sem->media_channel) as $row)
                    {{ \App\System::where('systemcode',$row)->first()->systemdesc . ', ' }}
                @endforeach
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Brief and Expectations: 
        <p>
            @if(!empty($account->sem->campaign))
                {!! nl2br($account->sem->campaign) !!}
            @else
                Not Specified
            @endif
        </p>
    </li>

    <li style="text-align: left;">Ad Scheduling: 
        <b>
            @if($account->sem->ad_scheduling == '1')
                Yes 
                <br/>
                {!! nl2br($account->sem->ad_scheduling_remark) !!}
            @else
                No
            @endif
        </b>
    </li>

    <li style="text-align: left;">Total Campaign Budget: 
        <b>
            @if(!empty($account->sem->budget))
                {{ $account->sem->currency.' '.$account->sem->budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Monthly Campaign Budget: 
        <b>
            @if(!empty($account->sem->google_monthly_budget))
                {{ $account->sem->currency.' '.$account->sem->google_monthly_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Daily Campaign Budget: 
        <b>
            @if(!empty($account->sem->google_daily_budget))
                {{ $account->sem->currency.' '.$account->sem->google_daily_budget }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Account Management Fee: 
        <b>
            @if(!empty($account->sem->am_fee))
                {{ $account->sem->currency.' '.$account->sem->am_fee }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Duration: 
        <b>
            @if(!empty($account->sem->duration))
                {{ $account->sem->duration != 'CD999' ? $account->sem->campaign_duration->systemdesc : $account->sem->duration_other.' '.$account->sem->campaign_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign Start Date: 
        <b>
            @if(!empty($account->sem->campaign_datefrom))
                {{ $account->sem->campaign_datefrom }}
            @else
                Not Specified
            @endif
        </b>
    </li>

    <li style="text-align: left;">Campaign End Date: 
        <b>
            @if(!empty($account->sem->campaign_dateto))
                {{ $account->sem->campaign_dateto }}
            @else
                Not Specified
            @endif
        </b>
    </li>
</ul>