<!DOCTYPE html>
<html>
    <body>
        <h4>Hi {{ $name }}!</h4>
        <p>Below are your credentials in the CRM system<p>
        <table border=0>
            <tr>
                <td>Username :</td>
                <td>{{ $username }}</td>
            </tr>
            <tr>
                <td>Email :</td>
                <td>{{ $email }}</td>
            </tr>
            <tr>
                <td>Password :</td>
                <td>{{ $password }}</td>
            </tr>
        </table>

        <p>Click <a href="{{ URL::to('/') }}">here</a> to login</a>
        <p>Please follow the steps in changing this default password upon signing in.<p>
        <br />
        <h5><i>Do not reply. This is a System Generated Message. Thank you.</i></h5>
    </body>
</html>
