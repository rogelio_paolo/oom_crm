<!DOCTYPE html>
<html>
    <body>
        <h4>Hi {{ $name }}</h4>
        <p>You received this email because you requested a reset of your password<p>
        <p>Please click the link below to proceed.</p>
        <br />
        <p>Username: {{ $username }}</p>
        <br />
        <p><a href="{{ route('user.password.reset.change.show',[uniqid() . md5($userid . str_random(20)),$userid]) }}">Reset Password</a></p>
        <br />
        <p><strong>If you didn't request a password reset, feel free to delete this email or ignore this message.</strong></p>
        <br />
        <hr>
        <h5><i>Do not reply. This is a System Generated Message. Thank you.</i></h5>
    </body>
</html>