<!DOCTYPE html>
<html>
    <body>
        <h4>Hi {{ $name }}!</h4>
        <p>You are assigned to {{ $lead }} account<p>
        <p>As a member, you can view the details of the account<p>

        <table border=0>
            <tr>
                <td>Lead Name :</td>
                <td>{{ $lead }}</td>
            </tr>
            <tr>
                <td>Nature of Package :</td>
                <td>{{ $nature }}</td>
            </tr>
        </table>

        <p>Click <a href="{{ route('account.view',$accountid) }}">here</a> to login and view the account.</p>
        <br />
        <h5><i>Do not reply. This is a System Generated Message. Thank you.</i></h5>
    </body>
</html>
