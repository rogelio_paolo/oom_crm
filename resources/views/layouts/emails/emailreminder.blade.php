<!DOCTYPE html>
<html>
    <head>
    <style>
    td {
        padding: 5px;
    }
    </style>
    </head>
    <body>
        <h4>Hello, {{ $recipient}}</h4>
        <p>You have a reminder for account <strong>{{ $company }}</strong></p>
        <br />
        <p>Details below:</p>
        <table style="width:30%">
            <tr>
                <td><strong>Title</strong></td>
                <td>{{ $name }}</td>
            </tr>
            <tr>
                <td><strong>Date</strong></td>
                <td>{{ $date }}</td>
            </tr>
            <tr>
                <td><strong>Remarks</strong></td>
                <td>{!! $remark !!}</td>
            </tr>
        </table>

        <br />
        <h5><em>Do not reply. This is a System Generated Message. Thank you.</em></h5>
    </body>
</html>
