<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">

<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />


<meta name="notification-sender" content="{{ Auth::user() ? Auth::user()->id : ''}}" />


<title>@yield('title')</title>

<base href="{{ env('HTTPS') == false ? URL::to('/') : secure_url('/') }}">

 <link rel="icon" type="image/png" href="https://www.oom.com.sg/favicon.png" /> 

 <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet"> 

<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/animate.css') }}" rel="stylesheet" />
<link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/reject.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('css/multiple-emails.css') }}">

<script src="{{ asset('js/sweet-alert.min.js') }}"></script>

<link href="{{ asset('css/main.css') }}" rel="stylesheet" />
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" />


<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

@yield('styles')

</head>

<body {{-- oncontextmenu="return false;" --}} >
<input id="reloadValue" type="hidden" name="reloadValue" value="" />
@if (Request::segment(2) != 'convert')
    {{ Session::forget('account_additional') }}
    {{ Session::forget('account_sem') }}
    {{ Session::forget('account_fb') }}
    {{ Session::forget('account_seo') }}
    {{ Session::forget('account_web') }}
    {{ Session::forget('account_weibo') }}
    {{ Session::forget('account_wechat') }}
    {{ Session::forget('account_wechat') }}
    {{ Session::forget('account_blog') }}
    {{ Session::forget('account_social') }}
    {{ Session::forget('account_postpaid') }}
@endif

@include('includes.remindermodal')

<div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="loader hidden"></div>  
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:110%">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-clock-o" aria-hidden="true"></i> Your appointments and reminders</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
               
                <div style="margin-bottom:3%" class="clearfix"></div>

      
                        <div class="panel panel-default">
                            <table class="table table-hover">
                                <thead></thead>
                                <tbody id="tbl-pending"></tbody>
                            </table>
                        </div>
    

                <div class="modal-footer" style="text-align:center">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal"> Close</button>
                </div>
        </div>
    </div>
</div>

@yield('modals')

<div class="wrapper dashboard-box">
 
  <div class="sidebar-box">

    <div class="inner-logo">
      <a href="#"><img class="logo-brand" src="{{ asset('images/logo.png') }}" alt=""></a>
    </div>

    <ul class="sidebar-menu">
      <li class="{{ Request::segment(1) == 'dashboard' ? 'active' : '' }}">
          <a href="{{ route('user.dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>
              Dashboard
          </a>
      </li>
      
      @if(Auth::user() && $user->userRole->activeflag == 1)

          {{--  @foreach($user->userAccess as $access)
              @if($access->module == 'MD002' && $access->access != '""')
              <li class="{{ Request::segment(1) == 'account' ? 'active' : '' }}">
                <a href="{{ route('account.list') }}"><i class="fa fa-book" aria-hidden="true"></i> Accounts</a>
              </li>
              @break
              @endif
          @endforeach  --}}

          {{-- @if($user->role != 3) --}}
          <li class="{{ Request::segment(1) == 'account' || Request::segment(1) == 'contract' ? 'active' : '' }}">
            <a href="{{ route('account.list') }}"><i class="fa fa-book" aria-hidden="true"></i> Accounts</a>
          </li>
          {{-- @endif --}} 

          @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']))
          <li class="{{ Request::segment(1) == 'lobby' ? 'active' : '' }}">
            <a href="{{ route('lobby.list') }}"><i class="fa fa-tty" aria-hidden="true"></i> Lobby</a>
          </li>
          @endif

           @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']))
            <li class="{{ Request::segment(1) == 'appointment' ? 'active' : '' }}">
                <a href="{{ route('appointment.list') }}"><i class="fa fa-calendar" aria-hidden="true"></i> Appointments</a>
            </li>
          @endif 

          @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']))
            <li class="{{ Request::segment(1) == 'performance' ? 'active' : '' }}">
                <a href="{{ route('performance.list') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Sales Performance</a>
            </li>
          @endif

          @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']))
            <li class="{{ Request::segment(1) == 'opportunity' ? 'active' : '' }}">
                <a href="{{ route('opportunity.list') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Opportunities</a>
            </li>
          @endif

          @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']))
          <li class="{{ Request::segment(1) == 'prospect' ? 'active' : '' }}">
            <a href="{{ route('prospect.list') }}"><i class="fa fa-key" aria-hidden="true"></i> Prospects</a>
          </li>
          @endif

          @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']))
          <li class="{{ Request::segment(1) == 'lead' ? 'active' : '' }}">
              <a href="{{ route('lead.list') }}"><i class="fa fa-tags" aria-hidden="true"></i> Leads</a>
          </li>
          @endif
          
          @if($user->role == 3 || $user->role == 1)
              <li class="{{ Request::segment(1) == 'employee' ? 'active' : '' }}">
                  <a href="{{ route('employee.list') }}"><i class="fa fa-users" aria-hidden="true"></i> Employees</a>
              </li>
          @endif
        
          @if($user->role == 1)
          <li class="{{ Request::segment(1) == 'roles' ? 'active' : '' }}">
              <a href="{{ route('roles.list') }}"><i class="fa fa-pencil" aria-hidden="true"></i> User Roles</a>
          </li>
          @endif

          @if($user->role == 1)
          <li class="{{ Request::segment(1) == 'audit' ? 'active' : '' }}">
              <a href="{{ route('audit.list') }}"><i class="fa fa-database" aria-hidden="true"></i> Audit Logs</a>
          </li>
          @endif

      @endif

    </ul>

  </div>

  <div class="main-box">
        <nav class="navbar navbar-default mobile-nav">
                <div class="container-fluid">
                  <ul class="nav navbar-nav">
                        <li class="{{ Request::segment(1) == 'dashboard' ? 'active' : '' }}">
                                <a href="{{ route('user.dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>
                                    Dashboard
                                </a>
                            </li>
                            
                            @if(Auth::user() && $user->userRole->activeflag == 1)
                      
                                {{--  @foreach($user->userAccess as $access)
                                    @if($access->module == 'MD002' && $access->access != '""')
                                    <li class="{{ Request::segment(1) == 'account' ? 'active' : '' }}">
                                      <a href="{{ route('account.list') }}"><i class="fa fa-book" aria-hidden="true"></i> Accounts</a>
                                    </li>
                                    @break
                                    @endif
                                @endforeach  --}}
                      
                                @if($user->role != 3)
                                <li class="{{ Request::segment(1) == 'account' ? 'active' : '' }}">
                                  <a href="{{ route('account.list') }}"><i class="fa fa-book" aria-hidden="true"></i> Accounts</a>
                                </li>
                                @endif

                                @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']))
                                <li class="{{ Request::segment(1) == 'lobby' ? 'active' : '' }}">
                                  <a href="{{ route('lobby.list') }}"><i class="fa fa-tty" aria-hidden="true"></i> Lobby</a>
                                </li>
                                @endif

                                @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']))
                                    <li class="{{ Request::segment(1) == 'appointment' ? 'active' : '' }}">
                                        <a href="{{ route('appointment.list') }}"><i class="fa fa-calendar" aria-hidden="true"></i> Appointments</a>
                                    </li>
                                @endif 

                                @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']))
                                    <li class="{{ Request::segment(1) == 'performance' ? 'active' : '' }}">
                                        <a href="{{ route('performance.list') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Sales Performance</a>
                                    </li>
                                @endif

                                @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']))
                                <li class="{{ Request::segment(1) == 'prospect' ? 'active' : '' }}">
                                  <a href="{{ route('prospect.list') }}"><i class="fa fa-key" aria-hidden="true"></i> Prospects</a>
                                </li>
                                @endif
                      
                                @if($user->role == 1 || $user->userInfo->department == 2)
                                <li class="{{ Request::segment(1) == 'lead' ? 'active' : '' }}">
                                    <a href="{{ route('lead.list') }}"><i class="fa fa-tags" aria-hidden="true"></i> Leads</a>
                                </li>
                                @endif
                                
                                @if($user->role == 3 || $user->role == 1)
                                    <li class="{{ Request::segment(1) == 'employee' ? 'active' : '' }}">
                                        <a href="{{ route('employee.list') }}"><i class="fa fa-users" aria-hidden="true"></i> Employees</a>
                                    </li>
                                @endif
                              
                                @if($user->role == 1)
                                <li class="{{ Request::segment(1) == 'roles' ? 'active' : '' }}">
                                    <a href="{{ route('roles.list') }}"><i class="fa fa-pencil" aria-hidden="true"></i> User Roles</a>
                                </li>
                                @endif
                      
                            @endif
                  </ul>
                </div>
              </nav>
    <div class="tool-box clearfix">

      <ul class="tool-list clearfix">
        {{--  <li class="vue-component" id="app-reminders"><reminders v-bind:reminders="reminders"></reminders></li>  --}}
        <li class="vue-component" id="app-notifications"><notification v-bind:notifications="notifications"></notification></li>
        <li>
          @guest

          @else
          <a href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-user fa-l" aria-hidden="true"></i>
            <span class="btn-p">{{ Auth::user()->username }}</span>
            <i class="fa fa-caret-down fa-r" aria-hidden="true"></i>
          </a>

          <ul class="dropdown-menu">
            <li><a href="{{ route('user.changepassword') }}"><i class="fa fa-cog fa-l" aria-hidden="true"></i> Change Password</a></li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="fa fa-key fa-l" aria-hidden="true"></i>
                    Logout
                </a>

                <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
          </ul>

          @endguest

        </li>
      </ul>

    </div>

  @yield('main_content')

  </div>

</div>

@yield('modals')
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('js/jquery.reject.min.js') }}"></script>
<script src="{{ asset('js/accounting.min.js') }}"></script> 
<script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/multiple-emails.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/global.js') }}"></script>

@yield('scripts')

<script type="text/javascript" async>
    try {
        $('[data-date-format]').datepicker();
    } catch(error) {
        console.log('Global DatePicker has been running but not used on this module!');
    }
</script>

@if(Session::has('alert'))
    <script>
        swal({
            title: "CRM System",
            text: "{!! Session::get('message') !!}",
            type: "{!! Session::get('alert') !!}",
            showConfirmButton: {!! Session::get('confirm') !!},
            timer: 5000
        });
    </script>
@endif

@if(Session::has('confirmButtonText'))
    <script>
        swal({
            title: 'Are you sure?',
            text: "{{!! Session::get('message') !!}}",
            type: "{{!! Session::get('alert') !!}}",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "{{!! Session::get('confirmButtonText') !!}}",
        }).then((result) => {
            if (result.value) {
                swal(
                    'Claimed!',
                    'Your lead/prospect has been open.',
                    'success'
                )
            }
        });
    </script>
@endif



</body>

</html>
