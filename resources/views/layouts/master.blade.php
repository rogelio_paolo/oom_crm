<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

@if(Route::is('firstlogin*'))
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endif

<title>@yield('title')</title>

<base href="{{ env('HTTPS') == false ? URL::to('/') : secure_url('/') }}">

<link rel="icon" type="image/png" href="images/favicon.png" />

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">

<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/reject.css') }}" rel="stylesheet">

<script src="{{ asset('js/sweet-alert.min.js') }}"></script>

<link href="{{ asset('css/main.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

@yield('styles')

</head>

<body {{-- oncontextmenu="return false;" --}} >

@if (!strpos('lead/convert',Route::getCurrentRoute()->uri()))
    {{ Session::forget('account_additional') }}
    {{ Session::forget('account_sem') }}
    {{ Session::forget('account_fb') }}
    {{ Session::forget('account_seo') }}
@endif

@yield('content')

<!-- JavaScript -->
<script
  src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('js/jquery.reject.min.js') }}"></script>
@yield('scripts')
<script src="{{ asset('js/main.js') }}"></script>

@if(Session::has('alert'))
    <script>
        var d = new Date();
        d = d.getTime();
        n = d.toString();

        if (n.substr(0,9) <= {!! substr(time(),0,9) !!}){
            swal({
              title: "CRM System",
              text: "{!! Session::get('message') !!}",
              type: "{!! Session::get('alert') !!}",
              button: "{!! Session::get('button') !!}",
              showConfirmButton: {!! Session::get('confirm') !!},
              timer: 5000
            });
        }
        
    </script>
@endif

</body>

</html>