<table>
    <thead>
        <tr>
        <th style="text-align: center">Company Name</th>
        <th style="text-align: center">Account Holder</th>
        <th style="text-align: center">Business Manager</th>
        <th style="text-align: center">Nature of Package</th>
        <th style="text-align: center">Status</th>
        <th style="text-align: center">Date Created</th>
        </tr>
    </thead>
    <tbody>
        @foreach($accounts as $row)
        <tr>
        <td style="text-align: center">{{ $row['company'] }}</td>
        <td style="text-align: center">{{ $row['holder'] }}</td>
        <td style="text-align: center">{{ $row['sales'] }}</td>
        <td style="text-align: center">{{ $row['package'] }}</td>
        <td style="text-align: center">{{ $row['status'] }}</td>
        <td style="text-align: center">{{ $row['date'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>