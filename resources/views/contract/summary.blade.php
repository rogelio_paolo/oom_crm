
@extends('layouts.master_v2')

@section('title')
    Crm - View {{ $prospect->lead->company . "'s " }} contract summary
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection

@section('main_content')
    <div class="back-btn">
        <a href="{{ route('account.list') }}" class="btn btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Accounts List
        </a>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('account.list') }}">Accounts</a></li>
                <li class="active">{{ $prospect->lead->company }} - Contracts</li>
            </ol>
        </div>
    </div>
</div>

    
<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="outer-data-box">
    <div class="row"> 
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h">Client Information</span>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">

            <div class="form-group clearfix">
                <label for="company" class="col-md-4 control-label">Company Name</label>
                <div class="col-md-6">
                    <p>{{ !empty($prospect->lead->company) ? $prospect->lead->company : 'Not Specified' }}</p>
                </div>
            </div>

            @if(!empty($prospect->lead->street_address))
                <div class="form-group clearfix">
                    <label for="street_address" class="col-md-4 control-label">Address</label>
                    <div class="col-md-6">
                        <p>{!! nl2br($prospect->lead->street_address) !!}</p>
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->zip_code))
                <div class="form-group clearfix">
                    <label for="zip_code" class="col-md-4 control-label">Postal Code</label>
                    <div class="col-md-6">
                        <p>{{ $prospect->lead->zip_code }}</p>
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->city))
                <div class="form-group clearfix">
                    <label for="city" class="col-md-4 control-label">City/Country</label>
                    <div class="col-md-6">
                        <p>{{ $prospect->lead->city }}</p> 
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->website))
                <div class="form-group clearfix">
                    <label for="website" class="col-md-4 control-label">Website URL</label>
                    <div class="col-md-6">
                        <p>{{ $prospect->lead->website }}</p>
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->services))
                <div class="form-group clearfix">
                    <label for="services" class="col-md-4 control-label">Services</label>
                    <div class="col-md-6">
                        <p>
                            @if(!empty($prospect->lead->services))
                                @php
                                    $services_arr = array();
                                    $services = explode('|', $prospect->lead->services);
                                    foreach($services as $row)
                                    {
                                        $services_arr[] = \App\System::where('systemcode',$row)->first()->systemdesc;
                                    }
                                    $services = implode(', ',$services_arr);
                                @endphp
                                <p>{{ $services }}</p>
                            @endif
                        </p>
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->industry))
                <div class="form-group clearfix">
                    <label for="packages" class="col-md-4 control-label">Industry</label>
                    <div class="col-md-6">
                        <p>{{ !empty($prospect->lead->industry) ? $prospect->lead->lead_industry->title : 'Not Specified' }}</p>
                    </div>
                </div>
            @endif

            @if(!empty($prospect->lead->other_services))
                <div class="form-group clearfix">
                    <label for="other_services" class="col-md-4 control-label">Other Services</label>
                    <div class="col-md-6">
                        <p>{{ !empty($prospect->lead->other_services) ? $prospect->lead->other_services : 'Not Specified' }}</p>
                    </div>
                </div>
            @endif

            <div class="form-group clearfix">
                <label for="sources" class="col-md-4 control-label">Salesperson</label>
                <div class="col-md-6">
                   <p>
                        @if(!empty($prospect->created_by))
                            @php $userFullName = \App\User::where('userid',$prospect->created_by)->first(); @endphp
                            {{ $userFullName->UserInfo->f_name.' '.$userFullName->UserInfo->l_name }}
                        @else
                            None
                        @endif
                   </p>
                </div>
            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Contracts List</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
    
            <!-- Active Contracts Container-->
            @if(count($active_contracts) > 0)
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapse-10" aria-expanded="true" class="svc-banner">
                                        Active Contracts
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-10" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                @forelse($active_contracts as $list)
                    @php
                        $semMembers = !empty(isset($list->account->sem->sem_strategist_assigned)) == 1 ? $list->account->sem->sem_strategist_assigned : '';
                        $fbMembers = !empty(isset($list->account->fb->fb_strategist_assigned)) == 1 ? $list->account->fb->fb_strategist_assigned : '';
                        $seoMembers = !empty(isset($list->account->seo->seo_strategist_assigned)) == 1 ? $list->account->seo->seo_strategist_assigned : '';
                        $seoMembers2 = !empty(isset($list->account->seo->seo_link_builder_assigned)) == 1 ? $list->account->seo->seo_link_builder_assigned : '';
                        $webMembers = !empty(isset($list->account->web->dev_team)) == 1 ? explode('|', $list->account->web->dev_team) : [];
                        $seoContentMembers = !empty(isset($list->account->seo->seo_content_team) == 1) ? explode('|', $list->account->seo->seo_content_team) : [];
                        $webContentMembers = !empty(isset($list->account->web->dev_content_team) == 1) ? explode('|', $list->account->web->dev_content_team) : [];
                        $blogContentMembers = !empty(isset($list->account->blog->blog_content_team) == 1) ? explode('|', $list->account->blog->blog_content_team) : [];
                        $socialContentMembers = !empty(isset($list->account->social->social_content_team) == 1) ? explode('|', $list->account->social->social_content_team) : [];
                        $accMembers = !empty(isset($list->account->acc_assigned) == 1) ? explode('|', $list->account->acc_assigned) : [];
                        $busiMember = !empty(isset($list->account->created_id) == 1) ? App\User::where('userid', $list->account->created_id)->first()->userid : '';
                    @endphp
                    
                    @if((in_array($user->role,[1,3]) || (in_array($teamcode, ['acc', 'con']) && in_array($ref_emp_id, $managers)) || in_array($ref_emp_id, $managers) ||
                        ($ref_emp_id == $semMembers || $ref_emp_id ==  $fbMembers || $ref_emp_id == $seoMembers  || $ref_emp_id == $seoMembers2 || in_array($ref_emp_id, $webMembers) != false ||
                                in_array($ref_emp_id, $seoContentMembers) != false || in_array($ref_emp_id, $webContentMembers) != false ||
                                    in_array($ref_emp_id, $blogContentMembers) != false || in_array($ref_emp_id, $accMembers) != false ||
                                        $user->userid === $busiMember ||
                                            in_array($ref_emp_id, $socialContentMembers) != false )) && !empty($list->account->nature) )

                        <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row toggle" id="dropdown-detail-{{ $list->id }}" data-toggle="detail-{{ $list->id }}">
                                        <div class="col-xs-10">
                                            <p>Contract Number: {{ $list->contract_number }}</p>
                                        </div>
                                        <div class="col-xs-2">
                                            <i id="up-down" class="fa fa-chevron-down pull-right"></i>
                                        </div>
                                    </div>
                                    <div id="detail-{{ $list->id }}">
                                        <hr>
        
                                        <div class="container-fluid">

                                            <div class="form-group clearfix">
                                                <label for="sources" class="col-md-4 control-label">Contract Value (without GST)</label>
                                                <div class="col-md-6">
                                                    <p>{{ !empty($list->contract_value) ?  $list->currency . ' ' . $list->contract_value : 'Not Specified' }}</p>
                                                </div>
                                            </div>

                                            
                                            <div class="form-group clearfix">
                                                <label for="sources" class="col-md-4 control-label">Contract Type</label>
                                                <div class="col-md-6">
                                                    <p>{{ !empty($list->contractType->systemdesc) ?  $list->contractType->systemdesc : 'Not Specified' }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group clearfix">
                                                <label for="sources" class="col-md-4 control-label">Contract Date Created</label>
                                                <div class="col-md-6">
                                                    <p>{{ !empty($list->created_at) ? $list->created_at->format('d M Y h:i:s A') : 'Not Specified' }}</p>
                                                </div>
                                            </div>

                                            @if (count($active_contracts) > 1 && $loop->last)
                                                <div class="form-group clearfix">
                                                    <label for="sources" class="col-md-4 control-label">90 Days</label>
                                                    <div class="col-md-6">
                                                    <strong>{{ !empty($list->day_90) ? $list->day_90->format('d M Y h:i:s A') : 'Not Specified' }}</strong>
                                                    </div>
                                                </div>
        
                                                <div class="form-group clearfix">
                                                    <label for="sources" class="col-md-4 control-label">120 Days</label>
                                                    <div class="col-md-6">
                                                        <strong>{{ !empty($list->day_120) ? $list->day_120->format('d M Y h:i:s A') : 'Not Specified' }}</strong>
                                                    </div>
                                                </div>
                                            @endif
        
                                            <div class="form-group clearfix">
                                                <label for="company" class="col-md-4 control-label">Contract Client Name</label>
                                                <div class="col-md-6">
                                                    <p>{{ !empty($list->f_name) ? $list->f_name . ' ' . $list->l_name : 'Not Specified' }}</p>
                                                </div>
                                            </div>
        
                                            <div class="form-group clearfix">
                                                <label for="company" class="col-md-4 control-label">Contract Client Email Address</label>
                                                <div class="col-md-6">
                                                    @if (!empty($list->email))
                                                        @foreach (explode('|', $list->email) as $email)
                                                            <p>{{ $email }}</p>
                                                        @endforeach
                                                    @else
                                                        <p>Not Specified</p>
                                                    @endif
                                                </div>
                                            </div>
        
                                            <div class="form-group clearfix">
                                                <label for="company" class="col-md-4 control-label">Contract Client Contact Number</label>
                                                <div class="col-md-6">
                                                    <p>{{ !empty($list->contact_number) ? $list->contact_number : 'Not Specified' }}</p>
                                                </div>
                                            </div>
        
        
                                            <div class="form-group clearfix">
                                                <label for="company" class="col-md-4 control-label">View Details</label>
                                                <div class="col-md-6">
                                                    <p>
                                                        @if(!empty($list->account_id))
                                                            @if(!in_array(Auth::user()->role, [3,4]) || in_array(Auth::user()->userInfo->team->team_code, ['busi','acc']))
                                                                @if (in_array($user->userInfo->team->team_code,['dev','acc','busi','sp','sem','seo','fb','con']) || $user->role == 1)
                                                                    <a title="View account details" class="btn btn-info" href="{{ route('account.view', $list->account_id) }}"><i class="fa fa-eye"></i></a> 
                                                                @endif 

                                                                @if ($user->role == 1 || in_array($user->userInfo->team->team_code,['busi','acc']) )
                                                                    <a title="Edit account details" href="{{ route('account.edit',$list->account_id) }}"  class="btn btn-warning"><i class="fa fa-pencil"></i></a> 
                                                                @endif 

                                                                @if (in_array($user->userInfo->emp_id,$managers) || $user->role == 1)
                                                                    <a title="Assign members on this account" href="{{ route('account.edit.assign',[$list->account_id,$teamcode]) }}"  class="btn btn-primary"><i class="fa fa-user-plus"></i></a>
                                                                @endif 

                                                                @if ($user->role == 1 || $user->userInfo->userid == $list->created_by)
                                                                    <a id="deleteContract" title="Delete this contract" data-contractid="{{ $list->id }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                                @endif 
                                                        
                                                                <a title="Schedule reminder" id="schedule-reminder" data-accountid="{{ $list->account_id }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal"><i class="fa fa-clock-o"></i></a>
                                                            @else 
                                                                <a title="View account details" class="btn btn-info" href="{{ route('account.view', $list->account_id) }}"><i class="fa fa-eye"></i></a>
                                                                <a title="Schedule reminder" id="schedule-reminder" data-accountid="{{ $list->account_id }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal"><i class="fa fa-clock-o"></i></a> 
                                                            @endif
                                                        @else 
                                                            No associated account
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="form-group clearfix">
                                                <label for="packages" class="col-md-4 control-label">Nature of Package/Services</label>
                                                <div class="col-md-6">
                                                    <p>
                                                        @php
                                                        if(!empty($list->account->nature))
                                                        {
                                                            $active_nature = explode('|',$list->account->nature);
                                                            $np = array();
                                                            foreach($active_nature as $k => $v)
                                                            {
                                                                $package = App\System::where('systemcode',$v)->first();
                                                                $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                                                            }
                                                    
                                                            $active_nature = implode(', ',$np);
                                                        }
                                                        else
                                                        {
                                                            $active_nature = 'None';
                                                        }                                                           
                                           
                                                        @endphp

                                                        {{ $active_nature }}
                                                    </p>
                                                </div>
                                            </div>
        
                                        </div>
                                    </div>
        

                                </li>
                            </ul>
                        @endif 
                    
                @empty 
                    <h4 class="text-center">No Assigned or Active Contracts</h4>
                @endforelse
                <!-- End of Active Contracts Container -->


                        
                                </div>
                            </div>
                        </div>
                    </div>
        @else 
            <h4 class="text-center">No Active Contracts</h4>
        @endif


        <!-- Past Contracts Container-->
        @if(count($past_contracts) > 0)
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-10" aria-expanded="true" class="svc-banner">
                                    Past Contracts
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-10" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
            @foreach($past_contracts as $list)   
                @php
                    $semMembers = !empty(isset($list->account->sem->sem_team)) == 1 ? explode('|', $list->account->sem->sem_team) : [];
                    $fbMembers = !empty(isset($list->account->fb->sem_team)) == 1 ? explode('|', $list->account->fb->sem_team) : [];
                    $seoMembers = !empty(isset($list->account->seo->seo_team)) == 1 ? explode('|', $list->account->seo->seo_team) : [];
                    $webMembers = !empty(isset($list->account->web->dev_team)) == 1 ? explode('|', $list->account->web->dev_team) : [];
                @endphp
            
                @if(in_array($user->role,[1,3]) || ($teamcode == 'acc' && in_array($ref_emp_id, $managers)) || in_array($ref_emp_id, $managers) ||
                    (in_array($ref_emp_id, $semMembers) != false || in_array($ref_emp_id, $fbMembers) != false || 
                        in_array($ref_emp_id, $seoMembers) != false || in_array($ref_emp_id, $webMembers) != false ) )
            
                <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row toggle" id="dropdown-detail-{{ $list->id }}" data-toggle="detail-{{ $list->id }}">
                                <div class="col-xs-10">
                                    <p>Contract Number: {{ $list->contract_number }}</p>
                                </div>
                                <div class="col-xs-2">
                                    <i id="up-down" class="fa fa-chevron-down pull-right"></i>
                                </div>
                            </div>
                            <div id="detail-{{ $list->id }}">
                                <hr>

                                <div class="container-fluid">

                                    <div class="form-group clearfix">
                                        <label for="company" class="col-md-4 control-label">Contract Client Name</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($list->f_name) ? $list->f_name . ' ' . $list->l_name : 'Not Specified' }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="company" class="col-md-4 control-label">Contract Client Designation</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($list->designation) ? $list->designation : 'Not Specified' }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="company" class="col-md-4 control-label">Contract Client Email Address</label>
                                        <div class="col-md-6">
                                            @if (!empty($list->email))
                                                @foreach (explode('|', $list->email) as $email)
                                                    <p>{{ $email }}</p>
                                                @endforeach
                                            @else
                                                <p>Not Specified</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="company" class="col-md-4 control-label">Contract Client Contact Number</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($list->contact_number) ? $list->contact_number : 'Not Specified' }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="sources" class="col-md-4 control-label">Contract Date Created</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($list->created_at) ? $list->created_at->format('d M Y h:i:s A') : 'Not Specified' }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="company" class="col-md-4 control-label">View Details</label>
                                        <div class="col-md-6">
                                            <p>
                                                @if(!empty($list->account_id))
                                                    <a title="View account details" class="btn btn-info" href="{{ route('account.view', $list->account_id) }}"><i class="fa fa-eye"></i></a> 
                                                    <a title="Schedule reminder" id="schedule-reminder" data-accountid="{{ $list->account_id }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal"><i class="fa fa-clock-o"></i></a>
                                                    {{--  <a title="Edit account details" href="{{ route('account.edit',$list->account_id) }}"  class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                                    <a title="Assign members on this account" href="{{ route('account.edit.assign',[$list->account_id,$teamcode]) }}"  class="btn btn-primary"><i class="fa fa-user-plus"></i></a>
                                                    <a title="Schedule reminder" id="schedule-reminder" data-accountid="{{ $list->account_id }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal"><i class="fa fa-clock-o"></i></a>  --}}
                                                @else 
                                                    No account is linked
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="packages" class="col-md-4 control-label">Nature of Package/Services</label>
                                        <div class="col-md-6">
                                            <p>
                                                @php
                                                if(!empty($list->account->nature))
                                                {
                                                    $past_nature = explode('|',$list->account->nature);
                                                    $np = array();
                                                    foreach($past_nature as $k => $v)
                                                    {
                                                        $package = App\System::where('systemcode',$v)->first();
                                                        $np[] = mb_strimwidth($package->systemdesc,0,50,'');
                                                    }
                                            
                                                    $past_nature = implode(', ',$np);
                                                }
                                                else
                                                {
                                                    $past_nature = 'None';
                                                }
                                                  
                                                @endphp

                                                {{ $past_nature }}
                                            </p>
                                        </div>
                                    </div>

                                    

                                </div>
                            </div>


                        </li>
                    </ul>
                @else 
                    <h4 class="text-center">No assigned Contracts</h4>
                @endif
            @endforeach
            <!-- End of Past Contracts Container -->


                    
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            
                                {{--  <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>  --}}
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

    <div class="clearfix" style="margin-bottom: 1%">&nbsp;</div>

    <div class="back-btn">
        <a href="{{ route('account.list') }}" class="btn btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Accounts List
        </a>
    </div>
    
    <div class="container pull-left">
            <div class="row">
                <div class="col-md-8 col-md-offset-1">
                    <ol class="breadcrumb bg-none" >
                        <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li><a href="{{ route('account.list') }}">Accounts</a></li>
                        <li class="active">{{ $prospect->lead->company }} - Contracts</li>
                    </ol>
                </div>
            </div>
        </div>
            
    
    {{--  <div class="back-btn">
        <a href="{{ route('account.list') }}" class="btn btn-lg btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Accounts List
        </a>
    </div>  --}}

    <div class="clearfix" style="margin-bottom: 1%">&nbsp;</div>

    <!-- DELETE CONTRACT MODAL -->
    <div class="modal fade" id="deleteContractModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="{{ route('contract.delete') }}">
                {{ csrf_field() }}
                    <input type="hidden" name="contractid" id="delete-contractid" />
                    <div class="data-header-box clearfix">
                        <span class="header-h"><i class="fa fa-eraser" aria-hidden="true"></i> Delete Contract</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="form-group">
                        <h3 class="text-center">Are you sure you want to delete this contract?</h3>
                    </div>
                    <div class="bottom-buttons clearfix">
                        <div class="buttons-box">
                            <button type="submit" class="btn btn-success">Delete</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    

@section('scripts')
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script src="{{ asset('js/lead/create.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/lead/notes.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/contract/summary.js') }}" type="text/javascript"></script>
@endsection

@endsection