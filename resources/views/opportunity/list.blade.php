@extends('layouts.master_v2')

@section('title')
    Crm - Opportunities List
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
@endsection

{{-- @section('modals')
    @include('includes.salesperformancemodaledit')
@endsection --}}

@section('main_content')
    <i id="loader-modal" class="loader hidden"></i>
    <div id="app-opportunity">
        <opportunity-component 
            role="{{ $user->role }}"
            user="{{ $user->userid }}"
        >
        </opportunity-component>
    </div>

    @section('scripts')
        <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $('.multiselect-ui').multiselect({
                    includeSelectAllOption: true
                });
            });
        </script>
        {{--  <script type="text/javascript" src="{{ asset('js/performance/opportunity/list.js') }}"></script>  --}}
    @endsection
@endsection
