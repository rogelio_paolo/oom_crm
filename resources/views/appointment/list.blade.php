@extends('layouts.master_v2')

@section('title')
    Crm - Appointments List
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.11.2/css/selectize.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery.paginate.css') }}" />
    
@endsection

@section('modals')
    @include('includes.appointmentmodal')
@endsection

@section('main_content')
    <div id="columns" class="outer-data-box">
        
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h"><i class="fa fa-calendar"></i> Appointments</span>
            </div>

            <div class="data-buttons-box clearfix">

                
                {{-- <div class="buttons-box pull-left">
                    <label for="filter-appointment">Filter by: </label>
                    <select id="filter-appointment" class="form-control">
                        <option value="services">Services</option>
                        <option value="date">Date</option>
                        <option value="salesperson">Salesperson</option>
                    </select> --}}

                    {{-- <select name="appt_filter" id="appt_filter" class="form-control hidden">
                        <option value="all">All</option>
                            @foreach($lead_services as $services)
                                <option value="{{ $services->systemcode }}">{{ $services->systemdesc }}</option>
                            @endforeach
                        <option value="none">None</option>
                    </select> --}}
                  
                {{-- </div> --}}

                <div class="buttons-box pull-right">
                    <button class="btn btn-success btn-lg" id="btnShowAddAppointmentModal" data-toggle="modal" data-target="#add_appointment_model">Add Appointment <i class="fa fa-plus-circle"></i></button>
                    <a href="http://calendar.google.com" target="_blank">
                        <button class="btn btn-default btn-sm">Visit Google Calendar Page</button>
                    </a>  
                </div>
            </div>

      
            {{--  <div class="row">
                <div class="col-xs-8 col-sm-9 m-left-appt">  --}}
                    {{--  <ul class="event-list">  --}}
                        {{--  @forelse($appointments as $appointment)  --}}
                            {{--  <li>
                                <time datetime="2014-07-20">
                                    <span class="day">4</span>
                                    <span class="month">Jul</span>
                                    <span class="year">2014</span>
                                    <span class="time">ALL DAY</span>
                                </time>  --}}
                                {{--  <img alt="Independence Day" src="https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg" />  --}}
                                {{--  <div class="info">
                                    <h2 class="title">Independence Day</h2>
                                    <p class="desc">United States Holiday</p>
                                </div>
                                <div class="social">
                                    <ul>
                                        <li class="facebook" style="width:33%;"><a href="#facebook"><span class="fa fa-facebook"></span></a></li>
                                        <li class="twitter" style="width:34%;"><a href="#twitter"><span class="fa fa-twitter"></span></a></li>
                                        <li class="google-plus" style="width:33%;"><a href="#google-plus"><span class="fa fa-google-plus"></span></a></li>
                                    </ul>
                                </div>
                            </li>  --}}
                        {{--  @empty  --}}
                            {{--  <p class="lead">No appointment found</p>
                        @endforelse  --}}
                    {{--  </ul>
                </div>  --}}
            {{--  </div>    --}}
        
    <div class="table-box">
        <table class="table main-table" id="lobby-management">
            <thead>
                <tr>
                    <th class="text-center dates">Scheduled Dates</th>
                    <th class="text-center company">Company</th>
                    <th class="text-center client">Contact Person</th>
                    <th class="text-center description">Brief Description</th>
                    <th class="text-center services">Services</th>
                    <th class="text-center guest">Receivers</th>
                    <th class="text-center created">Created by</th>
                    <th class="text-center status">Status</th>
                    <th class="text-center action">Action</th>
                </tr>
            </thead>
            <tbody id="appointment-data">
                @forelse($appointments as $appointment)
                    <tr data-appointment="{{ $appointment['id'] }}">
                        <td class="text-center dates text-nowrap">{{ $appointment['scheduled_date'] }}</td>
                        <td class="text-center company">{{ $appointment['company'] }}</td>
                        <td class="text-center client">{{ $appointment['client'] }}</td>
                        <td class="text-center description">{{ $appointment['description'] }}</td>
                        <td class="text-center services">{{ $appointment['services'] }}</td>
                        <td class="text-center guest">{{ $appointment['guest'] }}</td>
                        <td class="text-center person">{{ $appointment['created_by'] }}</td>
                        <td class="text-center status">{{ $appointment['status'] }}</td>
                        <td class="text-center action text-nowrap">
                            @if($user->role == 1 || ($team == 'acc' && in_array($user->emp_id, $managers)) || $user->userid == $appointment['owner'] )
                                @if($appointment['status'] == 'Pending' || $appointment['status'] == 'Ongoing')
                                    <button data-toggle="modal" data-target="#update_appointment_model" class="btn btn-info" id="btnShowEditAppointmentModal" data-appointment="{{ $appointment['id'] }}"><i class="fa fa-edit"></i></button>
                                @else 
                                    No action
                                @endif 
                            @endif               
                        </td>
                    </tr>
                @empty
                    <tr id="no-appointment">
                        <td colspan="8">No scheduled appointments</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div> <!-- div.table-box --> 

        </div> <!-- div.main-data-box -->
    </div> <!-- div#columns -->

    @section('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.11.2/js/standalone/selectize.js"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.paginate.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $('.multiselect-ui').multiselect({
                    includeSelectAllOption: true
                });
            });
        </script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/appointment/appointment.js') }}"></script>

        <script>
            $('#appointment-data').paginate({
                perPage: 10
            });
        </script>
    @endsection
@endsection
