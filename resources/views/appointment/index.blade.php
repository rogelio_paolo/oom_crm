@extends('layouts.master_v2')

@section('title')
    Crm - Appointments List
@endsection

@section('styles')
    <style>
        #mute {
          position: absolute;
        }
        #mute.on {
          opacity: 0.7;
          z-index: 1000;
          background: white;
          height: 100%;
          width: 100%;
        }
  </style>
@endsection

@section('modals')
@endsection


@section('main_content')
    <div id="mute"></div>
    <div id="app-appointment">
    </div>
@section('scripts')
{{--  <script type="text/javascript" src="{{ asset('js/lobby/list.js') }}"></script>  --}}
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>



@endsection
@endsection
