@extends('layouts.master_v2')

@section('title')
  Crm - Reminders
@endsection

@section('main_content')

<div class="outer-data-box" id="notification-list">

    <div class="main-data-box">
  
        <div class="data-header-box clearfix">

            <span class="header-h"><i class="fa fa-globe" aria-hidden="true"></i> Your reminders </span>

            <div class="data-search-box">

            </div>

        </div>

        <div class="data-buttons-box clearfix">

            <div class="filter-box">

            </div>

            <div class="buttons-box">

            </div>

        </div>

        <ul class="notifications-panel">
            @forelse($reminders as $row)
            <li class="{{ $row->read_at == null ? 'unread' : '' }}">
                <a href="#">
                Reminder {{ json_decode($row->data,true)['reminder']['name'] }} has been created  
                <br> <small>{{ $row->created_at->diff(\Carbon\Carbon::now())->days >= 2 ? date_format($row->created_at,'F d, Y') : ($row->created_at->diff(\Carbon\Carbon::now())->days == 1 ? 'Yesterday at '.date_format($row->created_at,'g:i A') : $row->created_at->diffForHumans()) }}</small>
                </a> 
            </li>
            @empty
            <li> No reminders to show </li>
            @endforelse
        </ul>
  
    </div>
  
</div>

@section('scripts')
    <script src="{{ asset('js/audit/list.js') }}" type="text/javascript"></script>
@endsection

@endsection
