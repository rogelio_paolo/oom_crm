@extends('layouts.master_v2')
@section('title')
    Crm - Roles
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
@endsection

@section('modals')
    @include('includes.rolemodal')
@endsection

@section('main_content')

<div class="has-lead-note">
    <div class="outer-data-box">
        <div class="col-lg-6">
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-pencil" aria-hidden="true"></i> Roles </span>
                    <div class="data-search-box">
                    <!-- <span>
                    Search Employee:
                    <div class="search-box">
                    <input type="text" class="form-control search-area" name="searchEmployee" id="searchEmployee" placeholder="Search" onkeypress="triggerSearch(event)">
                    <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                    </span> -->
                    </div>
                </div>
                <div class="data-buttons-box clearfix">
                    <div class="filter-box">
                    <!-- <a href="#"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                    <a href="#"><i class="fa fa-th" aria-hidden="true"></i> Group by</a>

                    <a href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i> Sort by</a> -->
                    <!-- <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a> -->
                    </div>
                    <div class="buttons-box">
                        <a href id="roleAdd" class="cta-btn add-btn">ADD NEW <i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="table-box">
                    <table class="table main-table" id="role-management">
                        <thead>
                            <tr>
                            <!-- <th class="text-center">Name <a href="" class="sort-user" data-sortby="name"><i id="sort-user" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th> -->
                                <th class="text-center">Role</th>
                                <th class="text-center">Is Active?</th>
                                <th class="text-center">action</th>
                            </tr>
                        </thead>
                        <i id="loader" class="loader fa fa-refresh fa-spin hidden" style=""></i>
                        <tbody>
                            @foreach($roles as $row)
                            <tr>
                                <!-- <td class="text-center"> {{ $row->firstname.' '.$row->lastname }} </td> -->
                                <td class="text-center"> {{ $row->syscode->systemdesc }} </td>
                                <td class="text-center"> {{ $row->activeflag == 1 ? 'Yes' : 'No' }} </td>
                                <td class="text-center" id="role-management">
                                <a id="roleEdit" data-roleid='{{ $row->id }}' class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-data-box">
        <div class="col-lg-6">
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-user-secret" aria-hidden="true"></i> Managers </span>
                    <div class="data-search-box">
                    <!-- <span>
                    Search Employee:
                    <div class="search-box">
                    <input type="text" class="form-control search-area" name="searchEmployee" id="searchEmployee" placeholder="Search" onkeypress="triggerSearch(event)">
                    <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                    </span> -->
                    </div>
                </div>
                <div class="data-buttons-box clearfix">
                    <div class="filter-box">
                    <!-- <a href="#"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                    <a href="#"><i class="fa fa-th" aria-hidden="true"></i> Group by</a>

                    <a href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i> Sort by</a> -->
                    <!-- <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a> -->
                    </div>
                    <div class="buttons-box">
                        <a href id="addManager" class="cta-btn add-btn">ADD NEW <i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="table-box">
                    <table class="table main-table" id="role-management">
                        <thead>
                            <tr>
                            <!-- <th class="text-center">Name <a href="" class="sort-user" data-sortby="name"><i id="sort-user" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th> -->
                                <th class="text-center">Department</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">action</th>
                            </tr>
                        </thead>
                        <i id="loader" class="loader fa fa-refresh fa-spin hidden" style=""></i>
                        <tbody>
                            @foreach($managers as $row)
                            <tr>
                                <td class="text-center"> {{ $row->info->emp_department->systemdesc }} </td>
                                <td class="text-center"> {{ $row->info->f_name.' '.$row->info->l_name }} </td>
                                <td class="text-center" id="role-management">
                                    <a id="deleteManager" data-manager='{{ $row->emp_id }}' class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script src="{{ asset('js_v1/role.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
@endsection

@endsection