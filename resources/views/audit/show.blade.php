@extends('layouts.master_v2')

@section('title')
  Crm - Audit Logs
@endsection

@section('main_content')

<div class="outer-data-box">

    <div class="main-data-box col-lg-8 col-lg-offset-2">
  
        <div class="data-header-box clearfix">

            <span class="header-h"><i class="fa fa-file" aria-hidden="true"></i> Audit Log of {{ $audit->user->UserInfo->f_name.' - '.date_format($audit->created_at,'F d, Y').' '.date_format($audit->created_at,'g:i A') }} </span>

            <div class="data-search-box">

            </div>

        </div>
        
        <hr />

        <div class="col-lg-12 clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <h5>IP Address </h5>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left">
                {{ $audit->ip }}
            </div>
        </div>

        <div class="col-lg-12 clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <h5>Module </h5>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left">
                {{ $audit->sysmodule->systemdesc }}
            </div>
        </div>

        <div class="col-lg-12 clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <h5>Action Taken </h5>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left">
                {{ $audit->sysaction->systemdesc }}
            </div>
        </div>

        <div class="col-lg-12 clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <h5>Message </h5>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left">
                {{ $audit->log }}
            </div>
        </div>

        <div class="col-lg-12 clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <h5>Browser </h5>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left">
                @if(!empty($browser))
                    <p> Name: {{ $browser['name'] }} </p>
                    <p> Version: {{ $browser['version'] }} </p>
                    <p> Platform: {{ $browser['platform'] }} </p>
                @else
                    Not Applicable for Administrators
                @endif
            </div>
        </div>

        @if($changes != 0)
            @php $oldval = json_decode($audit->oldval) @endphp
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <h5>Changes </h5>
                </div>
            </div>
            <div class="col-lg-12">
                <table class="table borderless">
                    
                    @foreach($changes as $field => $value)
                        <tr>
                            <td> {{ __('global.'.$field) }} </td>
                            <td> {!! empty($value[0]) ? 'None' : nl2br($value[0]) !!} </td>
                            <td> <i class="fa fa-arrow-circle-right"></i> </td>
                            <td> {!! empty($value[1]) ? 'None' : nl2br($value[1]) !!} </td>
                        </tr>
                    @endforeach
                    
                </table>
            </div>
        @endif
        <a href="{{ route('audit.list') }}" class="btn btn-danger pull-right btn-lg btn-button-rect">Back to Audit List</a>
    </div>
    
   
  
</div>

@endsection
