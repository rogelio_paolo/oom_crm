@extends('layouts.master_v2')

@section('title')
  Crm - Audit Logs
@endsection

@section('main_content')

<div class="outer-data-box">

    <div class="main-data-box">
  
        <div class="data-header-box clearfix">

            <span class="header-h"><i class="fa fa-database" aria-hidden="true"></i> Audit Trail Logs </span>

            <div class="data-search-box">

            </div>

        </div>

        <div class="data-buttons-box clearfix">

            <div class="filter-box">

                <!-- <a href="#"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>

                <a href="#"><i class="fa fa-th" aria-hidden="true"></i> Group by</a>

                <a href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i> Sort by</a> -->

                <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>

            </div>

            <div class="buttons-box">

            </div>

        </div>

        <div class="table-box">
        
            <i id="loader" class="loader hidden"></i>
            
            <table class="table main-table audit-logs" id="logs-management">
    
                <thead>
                    <tr>
                        <th>IP</th>
                        <th>Module</th>
                        <th>Action</th>
                        <th>Message</th>
                        <th>Employee</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <i id="loader" class="loader fa fa-refresh fa-spin hidden" style=""></i>
                <tbody>
                    @foreach($auditlogs as $row)
                    <tr>
                        <td> {{ $row->ip }} </td>
                        <td> {{ $row->sysmodule->systemdesc }} </td>
                        <td> {{ $row->sysaction->systemdesc }} </td>
                        <td> {{ str_limit($row->log,20) }} </td>
                        <td> {{ $row->user->UserInfo->f_name.' '.$row->user->UserInfo->l_name }} </td>
                        <td> {{ date_format($row->created_at,'F d, Y') }} </td>
                        <td> <a href="{{ route('audit.show',$row->id) }}" class="btn btn-warning"> <i class="fa fa-eye"></i> </a> </td>
                    </tr>
                    @endforeach
                </tbody>
    
            </table>
  
        </div>
  
        <div class="pagination-box">
            {{ $auditlogs->render() }}
        </div>
  
    </div>
  
</div>

@section('scripts')
    <script src="{{ asset('js/audit/list.js') }}" type="text/javascript"></script>
@endsection

@endsection
