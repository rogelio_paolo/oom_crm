@extends('layouts.master_v2')

@section('title')
  Crm - Accounts
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />

@endsection

@section('modals')
@include('includes.accountmodal')
@include('includes.contractmodal')
@endsection

@section('main_content')
<input type="hidden" id="user_role" value="{{ $user->role }}" />
<input type="hidden" id="team_code" value="{{ $user->userInfo->team->team_code }}" />   

<div id="columns" class="outer-data-box">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-book" aria-hidden="true"></i> Accounts</span>

      <div class="data-search-box">

         <span>
          Search Account:
          <div class="search-box">
            <input type="text" class="form-control search-area" name="searchAccount" id="searchAccount" placeholder="Search" onkeypress="triggerSearch(event)">
            <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </span> 

      </div>

    </div>

    <div class="data-buttons-box clearfix">
        <div class="filter-box">
            <a href="javascript:void(0)" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>
            @if($user->role == 1) <a href="javascript:void(0)" id="filter-account"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a> @endif

        </div>
        
        @if($user->role == 1)
            <div id="filter-container" class="hidden">
                <br/><br/>

                <div class="row">
                    <div class="col-md-3">
                        <label for="filter-by">Filter by</label>
                        <select id="filter-by" class="form-control">
                            <option value="all" {{ !empty($filter) ? ($filter->type_filter == 'all' ? 'selected' : '') : ''  }}>All</option>
                            <option value="busi_manager" {{ !empty($filter) ? ($filter->type_filter == 'busi_manager' ? 'selected' : '') : ''  }}>Business Manager</option>
                            <option value="account_holder" {{ !empty($filter) ? ($filter->type_filter == 'account_holder' ? 'selected' : '') : ''  }}>Account Holder</option>
                            <option value="contract_number" {{ !empty($filter) ? ($filter->type_filter == 'contract_number' ? 'selected' : '') : '' }}>Contract Number</option>
                        </select>
                        
                        <div class="row"></div>
                    </div>
                            
                    <div class="col-md-3" id="filter-item-container">
                        <label for="filter-item">Name</label>
                        <select id="filter-item" class="form-control"></select>
                    </div>

                    <div class="col-md-3" style="line-height: 70px;" id="filter-item-container">
                        <button id="filter-submit" class="btn btn-success" disabled>Submit</button>
                        <button id="filter-cancel" class="btn btn-danger">Cancel</button>
                    </div>

                </div>
            </div>
        @endif

      <div class="buttons-box">
          <button data-toggle="modal" data-target="#modifyColumn" class="btn btn-warning pull-right"><i class="glyphicon glyphicon-th-list"></i> Columns</button>
      </div>

      @if($user->role == 1 || (in_array($user->userInfo->team->team_code, ['acc']) && in_array($user->userInfo->emp_id, $managers)))
        <div class="buttons-box">          
            <button id="btn-bulk-assign" data-toggle="modal" data-target="#bulkAssigning" class="btn btn-info pull-right"><i class="fa fa-exchange"> </i> Bulk Assign AM</button>
        </div>
      @endif
    </div>

    @if($user->role == 1)
        @if(!empty($filter))
            @if($filter->type_filter != 'all')
                <div class="panel panel-default">
                    <div class="panel-title">
                        <i>
                            @if ($filter->type_filter == 'busi_manager')
                                Show Accounts of: <strong>{{ $filter->user->userInfo->full_name }}</strong>
                            @elseif ($filter->type_filter == 'account_holder')
                                Show Accounts holding by: <strong>{{ $filter->emp->full_name }}</strong>
                            @elseif ($filter->type_filter == 'contract_number')
                                Show Contract Number: <strong>{{ \App\Contract::find($filter->person_filter)->contract_number }}</strong>
                            @endif
                        </i>
                    </div>
                </div>
            @endif
        @endif
    @endif


    <div class="table-box">
      
      <i id="loader" class="loader hidden"></i>
        
      <table class="table main-table table-hover" id="account-management">

        <thead>
            <tr>
                <th class="text-center company_name {{ !empty($column) ? ($column->company_name ? '' : 'hidden') : '' }}">
                    Company Name <a href="" class="sort-account" data-sortby="company"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> 
                </th>
                <th class="text-center contract_number {{ !empty($column) ? ($column->contract_number ? '' :'hidden') : '' }}">
                    Contract No. <a href="" class="sort-account" data-sortby="contract_number"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i>
                </th>
                <th class="text-center day_90 {{ !empty($column) ? ($column->day_90 ? '' : 'hidden') : '' }}">
                    90 days <a href="" class="sort-account" data-sortby="day_90"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i>
                </th>
                <th class="text-center day_120 {{ !empty($column) ? ($column->day_120 ? '' : 'hidden') : '' }}">
                    120 days <a href="" class="sort-account" data-sortby="day_120"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i>  
                </th>
                <th class="text-center account_holder {{ !empty($column) ? ($column->account_holder ? '' : 'hidden') : '' }}">
                    Account Holder <a href="" class="sort-account" data-sortby="holder"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> 
                </th>
                <th class="text-center sp_assigned {{ !empty($column) ? ($column->sp_assigned ? '' : 'hidden') : '' }}">
                    SP Assigned
                </th>
                <th class="text-center busi_manager {{ !empty($column) ? ($column->busi_manager ? '' : 'hidden') : '' }}">
                    Business Manager <a href="" class="sort-account" data-sortby="sales"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> 
                </th>
                <th class="text-center nature {{ !empty($column) ? ($column->nature ? '' : 'hidden') : '' }}">
                    Services 
                </th>
                <th class="text-center am_fee {{ !empty($column) ? ($column->am_fee ? '' : 'hidden') : 'hidden' }}">
                    AM Fee 
                </th>
                <th class="text-center asst_account_holder {{ !empty($column) ? ($column->asst_account_holder ? '' : 'hidden') : 'hidden' }}">
                    Assistant Account Holder 
                </th>
                <th class="text-center campaign_start {{ !empty($column) ? ($column->campaign_start ? '' : 'hidden') : 'hidden' }}">
                    Campaign Start Date
                </th>
                <th class="text-center campaign_end {{ !empty($column) ? ($column->campaign_end ? '' : 'hidden') : 'hidden' }}">
                    Campaign End Date
                </th>
                <th class="text-center budget {{ !empty($column) ? ($column->budget ? '' : 'hidden') : 'hidden' }}">
                    Campaign Budget
                </th>
                <th class="text-center date_created {{ !empty($column) ? ($column->date_created ? '' : 'hidden') : '' }}">
                    Created <a href="" class="sort-account active" data-sortby="created_at"><i id="sort-account" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a> 
                </th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($result))
                @if(!in_array($role,[3, 4]) || in_array($teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1)
                    @foreach($result as $num => $row)
                        <tr>
                            <td class="text-center company_name {{ !empty($column) ? ($column->company_name ? '' : 'hidden') : '' }}">{{ $row['company'] }}</a>
                            <td class="text center contract_number {{ !empty($column) ? ($column->contract_number ? '' :'hidden') : '' }}">
                                @if($row['contracts'] > 1)
                                    <span id="ellipsis" class="ellipsis ellipsis-{{ $row['prospect'] }}" data-prospect-id="{{ $row['prospect'] }}"> {!! $row['contract_number'] !!}</span> <i id="btn-arrow-{{ $row['prospect'] }}" class="fa fa-ellipsis-v"></i>
                                @else 
                                    {{ $row['contract_number'] }}
                                @endif
                            </td>
                            <td class="text-center day_90 {{ !empty($column) ? ($column->day_90 ? '' : 'hidden') : '' }}">{{ $row['day_90'] }}</td>
                            <td class="text-center day_120 {{ !empty($column) ? ($column->day_120 ? '' : 'hidden') : '' }}">{{ $row['day_120'] }}</td>
                            <td class="text-center account_holder {{ !empty($column) ? ($column->account_holder ? '' : 'hidden') : '' }}">{{ $row['holder'] }}</td>
                            <td class="text-center sp_assigned {{ !empty($column) ? ($column->sp_assigned ? '' : 'hidden') : '' }}">{{ $row['sp_assigned'] }}</td>
                            <td class="text-center busi_manager {{ !empty($column) ? ($column->busi_manager ? '' : 'hidden') : '' }}">{{ $row['sales'] }}</td>
                            <td class="text-center nature {{ !empty($column) ? ($column->nature ? '' : 'hidden') : '' }}">{{ $row['package'] }}</td>
                            <td class="text-center am_fee {{ !empty($column) ? ($column->am_fee ? '' : 'hidden') : 'hidden' }}">{{ $row['am_fee'] }} </td>
                            <td class="text-center asst_account_holder {{ !empty($column) ? ($column->asst_account_holder ? '' : 'hidden') : 'hidden' }}">{{ $row['asst_holder'] }}</td>
                            <td class="text-center campaign_start {{ !empty($column) ? ($column->campaign_start ? '' : 'hidden') : 'hidden' }}">{!! $row['campaign_start'] !!}</td>
                            <td class="text-center campaign_end {{ !empty($column) ? ($column->campaign_end ? '' : 'hidden') : 'hidden' }}">{!! $row['campaign_end'] !!}</td>
                            <td class="text-center budget {{ !empty($column) ? ($column->budget ? '' : 'hidden') : 'hidden' }}">{!! $row['budget'] !!}</td>
                            <td class="text-center date_created {{ !empty($column) ? ($column->date_created ? '' : 'hidden') : '' }}">{{ $row['date'] }}</td>
                        <td class=" text-nowrap" id="account-management">
                                @if($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi', 'acc']) )
                                    <span class="dropup">
                                                <button class="btn btn-success dropdown-toggle margin-top default-dom" type="button" id="btnAddDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i> Add</button>

                                                <ul class="dropdown-menu" aria-labelledby="btnAddDropdown">
                                                    <li><a id="prospect-convert" data-prospectid="{{ $row['prospect'] }}" data-toggle="modal" data-target="#contract-modal"><i class="fa fa-plus-circle"></i> Contract</a></li>
                                                    <li><a id="prospect-opportunity" data-prospect-id="{{ $row['prospect'] }}" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Opportunity</a></li>
                                                </ul>
                                            </span>
                                @endif

                                @if($row['contracts'] > 1)
                                    @if (in_array($user->userInfo->team->team_code, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) || $user->role == 1)
                                        <a data-toggle="tooltip" title="View contract summary" href="{{ route('contract.summary',$row['prospect']) }}"  class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> 
                                    @endif
                                @else
                                    <a data-toggle="tooltip" title="Click to launch more buttons" href="javascript:void(0)" id="btn-group-single-acc" data-prospect-id="{{ $row['prospect'] }}" class="btn btn-info"><i class="glyphicon glyphicon-option-vertical"> </i></a>
                                    <div id="btn-group-acc-{{ $row['prospect'] }}" class="btn-group-vertical hidden" role="group">
                                            @if (in_array($user->userInfo->team->team_code, ['dev','acc','busi','sp','sem','seo','fb','con', 'hr']) || $user->role == 1)
                                                <a data-toggle="tooltip" title="View account details" href="{{ route('account.view',$row['id']) }}"  class="btn btn-info"><i class="fa fa-eye"></i></a> 
                                            @endif

                                            @if ($user->role == 1 || in_array($user->userInfo->team->team_code, ['busi','acc']) )
                                                <a data-toggle="tooltip" title="Edit account details" href="{{ route('account.edit',$row['id']) }}"  class="btn btn-warning"><i class="fa fa-pencil"></i></a> 
                                            @endif  
                                            
                                            @if(in_array($user->userInfo->emp_id,$managers) || $user->role == 1)
                                                <a data-toggle="tooltip" title="Assign members on this account" href="{{ route('account.edit.assign',$row['id']) }}"  class="btn btn-primary"><i class="fa fa-user-plus"></i></a> 
                                            @endif

                                            @if ($user->role == 1)
                                                <a data-toggle="tooltip" title="Delete account" id="accountDelete" data-accountid="{{ $row['id'] }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            @endif
                                    
{{--                                            <a title="Schedule reminder" id="schedule-reminder" data-accountid="{{ $row['id'] }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal">--}}
{{--                                                <i class="fa fa-clock-o"></i>--}}
{{--                                            </a>--}}
                                    </div>
                                @endif
{{--                                   <a id="prospect-convert" data-prospectid="{{ $row['prospect'] }}" data-toggle="modal" data-target="#contract-modal" class="btn btn-success margin-top default-dom"><i class="fa fa-plus"></i> Contract</a>--}}

                        </td>
                    </tr>
                    @endforeach
                @else
                    @forelse($result as $num => $row)
                        <tr>
                            <td class="text-center company_name {{ !empty($column) ? ($column->company_name ? '' : 'hidden') : '' }}">{{ $row['company'] }}</a>
                                <td class="text center contract_number {{ !empty($column) ? ($column->contract_number ? '' :'hidden') : '' }}">
                                    @if($row['contracts'] > 1)
                                        <span id="ellipsis" class="ellipsis ellipsis-{{ $row['prospect'] }}" data-prospect-id="{{ $row['prospect'] }}"> {!! $row['contract_number'] !!}</span> <i id="btn-arrow-{{ $row['prospect'] }}" class="fa fa-ellipsis-v"></i>
                                    @else 
                                        {{ $row['contract_number'] }}
                                    @endif
                                </td>
                                <td class="text-center day_90 {{ !empty($column) ? ($column->day_90 ? '' : 'hidden') : '' }}">{{ $row['day_90'] }}</td>
                                <td class="text-center day_120 {{ !empty($column) ? ($column->day_120 ? '' : 'hidden') : '' }}">{{ $row['day_120'] }}</td>
                                <td class="text-center account_holder {{ !empty($column) ? ($column->account_holder ? '' : 'hidden') : '' }}">{{ $row['holder'] }}</td>
                                <td class="text-center sp_assigned {{ !empty($column) ? ($column->sp_assigned ? '' : 'hidden') : '' }}">{{ $row['sp_assigned'] }}</td>
                                <td class="text-center busi_manager {{ !empty($column) ? ($column->busi_manager ? '' : 'hidden') : '' }}">{{ $row['sales'] }}</td>
                                <td class="text-center nature {{ !empty($column) ? ($column->nature ? '' : 'hidden') : '' }}">{{ $row['package'] }}</td>
                                <td class="text-center am_fee {{ !empty($column) ? ($column->am_fee ? '' : 'hidden') : 'hidden' }}">{{ $row['am_fee'] }} </td>
                                <td class="text-center asst_account_holder {{ !empty($column) ? ($column->asst_account_holder ? '' : 'hidden') : 'hidden' }}">{{ $row['asst_holder'] }}</td>
                                <td class="text-center campaign_start {{ !empty($column) ? ($column->campaign_start ? '' : 'hidden') : 'hidden' }}">{!! $row['campaign_start'] !!}</td>
                                <td class="text-center campaign_end {{ !empty($column) ? ($column->campaign_end ? '' : 'hidden') : 'hidden' }}">{!! $row['campaign_end'] !!}</td>
                                <td class="text-center budget {{ !empty($column) ? ($column->budget ? '' : 'hidden') : 'hidden' }}">{!! $row['budget'] !!}</td>
                                <td class="text-center date_created {{ !empty($column) ? ($column->date_created ? '' : 'hidden') : '' }}">{{ $row['date'] }}</td>
                                <td class="text-center text-nowrap" id="account-management">
                                    <div class="btn-group-vertical" role="group">
                                        @if($row['contracts'] > 1)
                                            <a data-toggle="tooltip" title="View contract summary" href="{{ route('contract.summary',$row['prospect']) }}"  class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> 
                                        @else
                                            <a data-toggle="tooltip" title="View account details" href="{{ route('account.view', $row['id']) }}"  class="btn btn-info"><i class="fa fa-eye"></i></a>
{{--                                            <a title="Schedule Reminder" id="schedule-reminder" data-accountid="{{ $row['id'] }}" class="btn btn-brown" data-toggle="modal" data-target="#addReminderModal">--}}
{{--                                                <i class="fa fa-clock-o"></i>--}}
{{--                                            </a>--}}
                                        @endif
                                    </div>
                                </td>
                        </tr>
                    @empty
                        <tr><td colspan="7" align="center">No accounts found</td></tr>
                    @endforelse
                @endif
            @else
               <tr><td colspan="7" align="center">No accounts found</td></tr>
            @endif
        </tbody>

      </table>

        <div class="row">
            <div class="col-md-1">
                <label for="limit_entry">Items per page: </label>
            </div>
            <div class="col-md-2">
                <div class="select-box">
                    <select id="limit_entry" name="limit_entry" class="w-150">
                        <option value="10" {{ !empty($items) ? $items->per_page == 10 ? 'selected' : '' : '' }}>10</option>
                        <option value="20" {{ !empty($items) ? $items->per_page == 20 ? 'selected' : '' : '' }}>20</option>
                        <option value="30" {{ !empty($items) ? $items->per_page == 30 ? 'selected' : '' : '' }}>30</option>
                        <option value="40" {{ !empty($items) ? $items->per_page == 40 ? 'selected' : '' : '' }}>40</option>
                    </select>
                </div>
            </div>
        </div>

        @include('prospect.modal.prospect_opportunity_modal', [
              'services'          => $services,
              'contract_types'    => $contract_types,
              'lead_statuses'     => $lead_statuses,
              'lead_sources'      => $lead_sources,
              'industries'        => $industries,
              'landings'          => $landings
          ])

    </div>

    <div class="pagination-box">
      {{ $accounts->render() }}
    </div>

  </div>

  
</div>

<div class="modal fade" id="modifyColumn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Modify Columns</h4>
        </div>
        <div class="modal-body">
          <form id="form_modify">
            <div class="row">
                <div class="col-md-6">
                    <div class="checkbox">
                        <label><input  type="checkbox" checked disabled> Company name</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" checked disabled> Date Created</label>
                    </div>
                    <div class="checkbox">
                        <label><input  type="checkbox" name="contract_number" {{ !empty($column) ? ($column->contract_number ? 'checked' : '') : 'checked' }}> Contract Number</label>
                    </div>
                    <div class="checkbox">
                        <label><input  type="checkbox" name="day_90" {{ !empty($column) ? ($column->day_90 ? 'checked' : '') : 'checked' }}> 90 days</label>
                    </div>
                    <div class="checkbox">
                        <label><input  type="checkbox" name="day_120" {{ !empty($column) ? ($column->day_120 ? 'checked' : '') : 'checked' }}> 120 days</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="nature" {{ !empty($column) ? ($column->nature ? 'checked' : '') : 'checked' }}> Services</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="account_holder" {{ !empty($column) ? ($column->account_holder ? 'checked' : '') : 'checked' }}> Account Holder</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="sp_assigned" {{ !empty($column) ? ($column->sp_assigned ? 'checked' : '') : 'checked' }}> SP Assigned</label>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="busi_manager" {{ !empty($column) ? ($column->busi_manager ? 'checked' : '') : 'checked' }}> Business Manager</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="am_fee" {{ !empty($column) ? ($column->am_fee ? 'checked' : '') : '' }}> AM Fee</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="asst_account_holder" {{ !empty($column) ? ($column->asst_account_holder ? 'checked' : '') : '' }}> Assistant Account Holder</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="campaign_start" {{ !empty($column) ? ($column->campaign_start ? 'checked' : '') : '' }}> Campaign Start Date</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="campaign_end" {{ !empty($column) ? ($column->campaign_end ? 'checked' : '') : '' }}> Campaign End Date</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="budget" {{ !empty($column) ? ($column->budget ? 'checked' : '') : '' }}> Campaign Budget</label>
                    </div>
                </div>
            </div> 
        </div>
        <i id="loader-modal" class="loader hidden"></i>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="btnApply" onclick="modify();">Apply changes</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  </div>

<div class="modal fade" id="bulkAssigning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:900px;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Accounts List</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="acc_assigned">Select AM <span class="error-msg">*</span></label>
                            <select class="form-control" name="acc_assigned" id="acc_assigned">
                                @foreach($acc_team as $row)
                                    <option value="{{ $row->emp_id }}">{{ $row->f_name .  ' ' . $row->l_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-10"> 
                        <div class="form-group">
                            <div id="error-acc-select" class="alert alert-warning hidden">
                                <strong>Warning!</strong> You need to select at least one account.
                            </div>
                            <label for="account">Select Account to Assign <span class="error-msg">*</span></label>
                            <select class="selectpicker form-control" name="account_select[]" id="account_select" data-live-search="true" data-actions-box="true" multiple="multiple">
                                @foreach($acc_list as $row)
                                    <option value="{{ $row->id }}">{{ $row->company }} • @if(!empty($row->contract_number)) (Latest: {{ $row->contract_number }}) @endif </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                  
                </div>
               

                <table id="tbl-list" class="table table-hovered table-responsive hidden">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Contract No.</th>
                            <th>New Account Holder</th>
                            <th>Business Manager</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody id="acc-list">
                    </tbody>
                </table>
            </div>

           
            <i id="loader-modal" class="loader hidden"></i>
            <div class="modal-footer">

              <button type="button" class="btn btn-success" id="btnApplyAMHolder">Assign</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      </div>

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

    <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/account/list.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/prospect/prospect_opportunity.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/account/bulk.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/account/filter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
    <script type="text/javascript">
        $('#start-picker').datetimepicker();
        $('#end-picker').datetimepicker();
    </script>






@endsection

@endsection
