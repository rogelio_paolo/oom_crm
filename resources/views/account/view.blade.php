@extends('layouts.master_v2')
@section('title')
Crm - View {{ !empty($account->contract->company) ? $account->contract->company."'s " : $account->prospect->lead->company . "'s "}} account
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link href="{{ asset('fonts_v1/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css_v1/custom.css') }}" rel="stylesheet">
<!-- <link rel="stylesheet" href="{{ asset('css_v1/account-create.css') }}" /> -->
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
@endsection
@section('main_content')
@include('includes.accountmodal')
    <div class="back-btn">
        <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
        </a>
    </div>

<div class="container pull-left">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('account.list') }}">Accounts</a></li>
                <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }}</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="has-lead-note">

       
        <form class="outer-data-box" method="post" action="{{ route('account.update.assign') }}">
            
            {{ csrf_field() }}
            <input type="hidden" id="accountid" name="accountid" value="{{ $account->id }}" />

            <div class="main-data-box">
                <div class="data-header-box clearfix">

                    @if (!($user->role == 4 || ($user->role == 2 && !in_array($user->userInfo->emp_id, $managers))))
                        <div class="pull-right btn-group">
                            @if(!empty($previous))
                                <a href="{{ URL::to('account/view/' . $previous->id ) }}" title="{{ $previous->prospect->lead->company }}" class="prev-next" href=""><i class="fa fa-chevron-circle-left"></i> Previous</a>
                            @endif

                            @if(!empty($next))
                                <a href="{{ URL::to('account/view/' . $next->id ) }}" title="{{ $next->prospect->lead->company }}" class="prev-next" href="">Next <i class="fa fa-chevron-circle-right"> </i></a>
                            @endif
                        </div>
                    @endif
                    <span class="header-h">Contract Details</span>   
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                <div class="form-group clearfix">
                                    <label for="company_name" class="col-md-4 control-label">Contract Value (without GST)</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->contract_value) ? $account->contract->currency . ' ' . $account->contract->contract_value : 'Not Specified' }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                        <label for="contract_type" class="col-md-4 control-label">Contract Type</label>
                                        <div class="col-md-6">
                                            <p><strong>{{ !empty($account->contract->contractType) ? $account->contract->contractType->systemdesc : 'Not Specified' }}</strong></p>
                                        </div>
                                    </div>

                                <div class="form-group clearfix">
                                    <label for="company_name" class="col-md-4 control-label">Company Name</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="company_name" class="col-md-4 control-label">Contract Number</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->contract_number) ? $account->contract->contract_number : 'No contract found' }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="client_name" class="col-md-4 control-label">Client Name</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->f_name) ? $account->contract->f_name . ' ' . $account->contract->l_name : (!empty($account->prospect->lead->f_name) ? $account->prospect->lead->f_name.' '.$account->prospect->lead->l_name : 'Not Specified') }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="client_designation" class="col-md-4 control-label">Client Designation</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->designation) ? $account->contract->designation : (!empty($account->prospect->lead->designation) ? $account->prospect->lead->designation : 'Not Specified') }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="client_number" class="col-md-4 control-label">Client Number</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->contract->contact_number) ? $account->contract->contact_number : (!empty($account->prospect->lead->contact_number) ? $account->prospect->lead->contact_number : 'Not Specified') }}</p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="client_email" class="col-md-4 control-label">Client Email Address</label>
                                    <div class="col-md-6">
                                        @if (!empty($account->contract->email))
                                            @foreach (explode('|', $account->contract->email) as $email)
                                                <p>{{ $email }}</p>
                                            @endforeach
                                        @elseif (!empty($account->prospect->lead->email))
                                            @foreach (explode('|', $account->prospect->lead->email) as $email)
                                                <p>{{ $email }}</p>
                                            @endforeach
                                        @else
                                            <p>Not Specified</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="street_address" class="col-md-4 control-label">Client Address</label>
                                    <div class="col-md-6">
                                        <p>{!! !empty($account->prospect->lead->street_address) ? nl2br($account->prospect->lead->street_address) : 'Not Specified' !!}</p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="client_email" class="col-md-4 control-label">Website URL</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->prospect->lead->website) ? $account->prospect->lead->website : 'Not specified' }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="industry" class="col-md-4 control-label">Industry</label>
                                    <div class="col-md-6">
                                        <p>{{ !empty($account->prospect->lead->industry) ? $account->prospect->lead->lead_industry->title : 'Not specified' }}</p>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label for="nature" class="col-md-4 control-label">Services </label>
                                    <div class="col-md-6">

                                            @foreach($nature as $i => $row)
                                                <p>{{ $row }}</p>
                                            @endforeach
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="contact_page_url" class="col-md-4 control-label">Contact Us Page URL</label>
                                    <div class="col-md-6">
                                        <p>
                                            @if(!empty($account->additional->contact_page_url))
                                            {{ $account->additional->contact_page_url }}
                                            @else
                                                Not Specified
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="ty_page_url" class="col-md-4 control-label">Thank You Page URL</label>
                                    <div class="col-md-6">
                                        <p>
                                            @if(!empty($account->additional->ty_page_url))
                                            {{ $account->additional->ty_page_url }}
                                            @else
                                                Not Specified
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="code_required" class="col-md-4 control-label">GA Required?</label>
                                    <div class="col-md-6">
                                        <p>
                                            {{ $account->additional->ga_required == 1 ? 'Yes' : 'No' }} {{ !empty($account->additional->ga_remark) ? '- '.$account->additional->ga_remark : '' }}
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="implementations" class="col-md-4 control-label">Implementations Needed from OOm?</label>
                                    <div class="col-md-6">
                                        <p>
                                            {{ $account->additional->implementations == 1 ? 'Yes' : 'No' }} {{ !empty($account->additional->implementations_remark) ? '- '.$account->additional->implementations_remark : '' }}
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label for="web_login" class="col-md-4 control-label">Website CMS / FTP Login</label>
                                    <div class="col-md-6">
                                        <p>
                                            @if(!empty($account->additional->web_login))
                                            {!! nl2br($account->additional->web_login) !!}
                                            @else
                                            Not Specified
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix" style="margin-bottom: 5%">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            @if (!empty($account->ppc_id))                              
            <div class="main-data-box" id="SEM">
                <div class="data-header-box clearfix">
                    <span class="header-h">SEM</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.sem')
                                
                        <hr />

                        <div class="form-group clearfix">
                            <label for="sem_strategist_assigned" class="col-md-4 control-label">Assigned SEM Strategist</label>
                            <div class="col-md-6">
                                <p>
                                    @if(!empty($account->sem->sem_strategist_assigned))
                                        @foreach(explode('|',$account->sem->sem_strategist_assigned) as $row)
                                            @php $sem_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $sem_strategist->f_name . ' ' . $sem_strategist->l_name }} <br />
                                        @endforeach
                                    @else
                                        No member assigned yet
                                    @endif
                                </p>
                            </div>
                        </div>

                        @if(!empty($account->sem->asst_sem_strategist_assigned))
                            <div class="form-group clearfix">
                                <label for="asst_sem_strategist_assigned" class="col-md-4 control-label">Assigned Assistant SEM Strategist</label>
                                <div class="col-md-6">
                                    <p>
                                        @foreach(explode('|',$account->sem->asst_sem_strategist_assigned) as $row)
                                            @php $asst_sem_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $asst_sem_strategist->f_name . ' ' . $asst_sem_strategist->l_name }} <br />
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->fb_id))                       
            <div class="main-data-box" id="FB">
                <div class="data-header-box clearfix">
                    <span class="header-h">Facebook Advertisement</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.fb')

                        <hr />

                        <div class="form-group clearfix">
                                <label for="fb_strategist_assigned" class="col-md-4 control-label">Assigned FB Strategist</label>
                                <div class="col-md-6">
                                    <p>
                                        @if(!empty($account->fb->fb_strategist_assigned))
                                            @foreach(explode('|',$account->fb->fb_strategist_assigned) as $row)
                                                @php $fb_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                                {{ $fb_strategist->f_name . ' ' . $fb_strategist->l_name }} <br />
                                            @endforeach
                                        @else
                                            No member assigned yet
                                        @endif
                                    </p>
                                </div>
                            </div>
    
                            @if(!empty($account->fb->asst_fb_strategist_assigned))
                                <div class="form-group clearfix">
                                    <label for="asst_sem_strategist_assigned" class="col-md-4 control-label">Assigned Assistant FB Strategist</label>
                                    <div class="col-md-6">
                                        <p>
                                            @foreach(explode('|',$account->fb->asst_fb_strategist_assigned) as $row)
                                                @php $asst_fb_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                                {{ $asst_fb_strategist->f_name . ' ' . $asst_fb_strategist->l_name }} <br />
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->seo_id))                             
            <div class="main-data-box" id="SEO">
                <div class="data-header-box">
                    <span class="header-h">SEO</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.seo')

                        <hr />

                        <div class="form-group clearfix">
                                <label for="seo_strategist_assigned" class="col-md-4 control-label">Assigned SEO Strategist</label>
                                <div class="col-md-6">
                                    <p>
                                        @if(!empty($account->seo->seo_strategist_assigned))
                                            @foreach(explode('|',$account->seo->seo_strategist_assigned) as $row)
                                                @php $seo_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                                {{ $seo_strategist->f_name . ' ' . $seo_strategist->l_name }} <br />
                                            @endforeach
                                        @else
                                            No member assigned yet
                                        @endif
                                    </p>
                                </div>
                            </div>
    
                            @if(!empty($account->fb->asst_seo_strategist_assigned))
                                <div class="form-group clearfix">
                                    <label for="asst_seo_strategist_assigned" class="col-md-4 control-label">Assigned Assistant SEO Strategist</label>
                                    <div class="col-md-6">
                                        <p>
                                            @foreach(explode('|',$account->seo->asst_seo_strategist_assigned) as $row)
                                                @php $asst_seo_strategist = \App\Employee::where('emp_id',$row)->first() @endphp
                                                {{ $asst_seo_strategist->f_name . ' ' . $asst_seo_strategist->l_name }} <br />
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group clearfix">
                                    <label for="seo_link_builder_assigned" class="col-md-4 control-label">Assigned SEO Link Builder</label>
                                    <div class="col-md-6">
                                        <p>
                                            @if(!empty($account->seo->seo_link_builder_assigned))
                                                @foreach(explode('|',$account->seo->seo_link_builder_assigned) as $row)
                                                    @php $seo_link_builder = \App\Employee::where('emp_id',$row)->first() @endphp
                                                    {{ $seo_link_builder->f_name . ' ' . $seo_link_builder->l_name }} <br />
                                                @endforeach
                                            @else
                                                No member assigned yet
                                            @endif
                                        </p>
                                    </div>
                                </div>
        
                                @if(!empty($account->seo->asst_seo_link_builder_assigned))
                                    <div class="form-group clearfix">
                                        <label for="asst_seo_link_builder_assigned" class="col-md-4 control-label">Assigned Assistant SEO Link Builder</label>
                                        <div class="col-md-6">
                                            <p>
                                                @foreach(explode('|',$account->seo->asst_seo_link_builder_assigned) as $row)
                                                    @php $asst_seo_link_builder = \App\Employee::where('emp_id',$row)->first() @endphp
                                                    {{ $asst_seo_link_builder->f_name . ' ' . $asst_seo_link_builder->l_name }} <br />
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($account->seo->seo_content_team))
                                    <div class="form-group clearfix">
                                        <label for="seo_content_team" class="col-md-4 control-label">Assigned SEO Content Team</label>
                                        <div class="col-md-6">
                                            <p>
                                                @foreach(explode('|',$account->seo->seo_content_team) as $row)
                                                    @php $seo_content = \App\Employee::where('emp_id',$row)->first() @endphp
                                                    {{ $seo_content->f_name . ' ' . $seo_content->l_name }} <br />
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
            @if (!empty($account->web_id))                             
            <div class="main-data-box" id="WEB">
                <div class="data-header-box">
                    <span class="header-h">Web Development</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.web')

                        <hr />

                        <div class="form-group clearfix">
                            <label for="target_market" class="col-md-4 control-label">Assigned Web Development Team Members</label>

                            <div class="col-md-6">

                                <p>
                                    @if(!empty($account->web->dev_team))
                                        @foreach(explode('|',$account->web->dev_team) as $row)
                                            @php $dev_team = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $dev_team->f_name . ' ' . $dev_team->l_name }} <br />
                                        @endforeach
                                    @else
                                        No members included yet
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="target_market" class="col-md-4 control-label">Assigned Web Development Content Team Members</label>

                            <div class="col-md-6">

                                <p>
                                    @if(!empty($account->web->dev_content_team))
                                        @foreach(explode('|',$account->web->dev_content_team) as $row)
                                            @php $dev_content_team = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $dev_content_team->f_name . ' ' . $dev_content_team->l_name }} <br />
                                        @endforeach
                                    @else
                                        No members included yet
                                    @endif
                                </p>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->baidu_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Baidu</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.baidu')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->weibo_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Weibo</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.weibo')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->wechat_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">WeChat</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.wechat')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->blog_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Blog Content</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.blog')

                            <hr />

                        <div class="form-group clearfix">
                            <label for="target_market" class="col-md-4 control-label">Assigned Blog Content Team Members</label>

                            <div class="col-md-6">

                                <p>
                                    @if(!empty($account->blog->blog_content_team))
                                        @foreach(explode('|',$account->blog->blog_content_team) as $row)
                                            @php $blog_content_team = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $blog_content_team->f_name . ' ' . $blog_content_team->l_name }} <br />
                                        @endforeach
                                    @else
                                        No members included yet
                                    @endif
                                </p>
                            </div>
                        </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->social_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Social Media Management</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.social')

                        <hr/>

                        <div class="form-group clearfix">
                            <label for="target_market" class="col-md-4 control-label">Assigned Social Media Content Team Members</label>

                            <div class="col-md-6">

                                <p>
                                    @if(!empty($account->social->social_content_team))
                                        @foreach(explode('|',$account->social->social_content_team) as $row)
                                            @php $social_content_team = \App\Employee::where('emp_id',$row)->first() @endphp
                                            {{ $social_content_team->f_name . ' ' . $social_content_team->l_name }} <br />
                                        @endforeach
                                    @else
                                        No members included yet
                                    @endif
                                </p>
                            </div>
                        </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if (!empty($account->postpaid_sem_id))                              
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h">Post Paid SEM</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                @include('account.assign.postpaid_sem')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="main-data-box" id="ACC">
                <div class="data-header-box clearfix">
                    <span class="header-h">Account Management</span>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
            
            <div class="form-group clearfix">
                <label for="busi_team" class="col-md-4 control-label">Main Account Holder </label>
                <div class="col-md-6">
                    @if(isset($acc_members))
                        @forelse($acc_members as $row)
                            <p>{{ $row }}</p>
                        @empty
                            <p>None</p>
                        @endforelse
                    @else
                        <p> None</p>
                    @endif
                </div>

                <label for="busi_team" class="col-md-4 control-label">Assistant Account Holder </label>
                <div class="col-md-6">
                    @if(isset($acc_asst_members))
                        @forelse($acc_asst_members as $row)
                            <p>{{ $row }}</p>
                        @empty
                            <p>None</p>
                        @endforelse
                    @else
                        <p> None</p>
                    @endif
                </div>
            </div>

            <div class="clearfix">&nbsp;</div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <form class="outer-data-box" method="post" action="{{ route('lead.note.create') }}">
            {{ csrf_field() }}
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Lead Notes</span>
                </div>
                <div class="lead-notes">
                    @if(count($notes) > 0)
                        @foreach($notes as $row)
                            <div class="lead-note">
                                <span> 
                                    <p>{{ $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name }}</p> 
                                    <p>{{ date_format($row->created_at,'Y-m-d h:i A') }}</p>
                                </span>
                                
                                @if ($row->userid == $user->userid)
                                <span class="pull-right">
                                <a id="edit-note" data-editnote="{{ $row->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                                <a id="delete-note" data-delnote="{{ $row->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                </span>
                                @endif

                                <div class="note-wrapper">
                                    <span id="lead-note-{{ $row->id }}">{!! nl2br($row->note) !!}</span>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    @endif
                </div>

                <input type="hidden" name="leadid" value="{{ $account->prospect->lead_id }}" />
                <input type="hidden" name="acc_id" value="{{ $account->id }}" />
                <textarea class="form-control" name="addnote" id="addnote" rows="10"></textarea>
                @if($errors->has('addnote'))
                <span class="note error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('addnote') }}</span>
                @endif
                <div class="clearfix" style="margin-bottom: 5%">&nbsp;</div>
                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="cta-btn add-btn">
                        Add Note
                        </button>
                    </div>
                </div>

            </div>
        </form>

        <div class="back-btn">
            <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
                <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
            </a>
        </div>

        <div class="container pull-left">
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <ol class="breadcrumb bg-none" >
                        <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li><a href="{{ route('account.list') }}">Accounts</a></li>
                        <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                        <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }}</li>
                    </ol>
                </div>
            </div>
        </div>
</div>


<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

@section('scripts')
<script type="text/javascript" src="{{ asset('js/account/edit.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
@endsection
@endsection