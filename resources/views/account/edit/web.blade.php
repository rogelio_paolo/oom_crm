<input type="hidden" name="web_included" id="web_included" value="{{ !empty($account->web) || old('web_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-6" aria-expanded="true" class="collapsed">
                Web Development
                </a>
            </h4>
        </div>
        <div id="collapse-6" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Project Type *</label>
                            <div class="select-box">
                                <div class="select-box">
                                    <select name="account[web][project_type]" id="project_type" class="form-control">
                                    @foreach($webtype as $row)
                                    <option value="{{ $row->systemcode }}" {{ !empty($account->web) ? ( $account->web->project_type == $row->systemcode ? 'selected' : ( !empty(old('project_type')) && old('project_type') == $row->systemcode ? 'selected' : '' ) ) : '' }} >{{ $row->systemdesc }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('project_type'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                {{ $errors->first('project_type') }}
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>CMS System *</label>
                            <div class="select-box">
                                <select name="account[web][cmstype]" id="cmstype" class="form-control">
                                @foreach($cmstype as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty($account->web) && $row->systemcode == $account->web->cmstype ? 'selected' : '' }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            @if ($errors->has('cmstype'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('cmstype') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Number of Pages *</label>
                            <input id="pages" type="text" class="form-control" name="account[web][pages]" value="{{ !empty($account->web) ? $account->web->pages : old('pages') }}">
                            @if ($errors->has('pages'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('pages') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Objective *</label>
                            <textarea class="form-control" id="objective" name="account[web][objective]" rows="5">{{ !empty($account->web) ? $account->web->objective : old('objective') }}</textarea>
                            @if ($errors->has('objective'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('objective') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Brief information about the Company *</label>
                            <textarea class="form-control" id="company_brief" name="account[web][company_brief]" rows="5">{{ !empty($account->web) ? $account->web->company_brief : old('company_brief') }}</textarea>
                            @if ($errors->has('company_brief'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('company_brief') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Target Audience *</label>
                            <textarea class="form-control" id="target_audience" name="account[web][target_audience]" rows="5">{{ !empty($account->web) ? $account->web->target_audience : old('target_audience') }}</textarea>
                            @if ($errors->has('target_audience'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('target_audience') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Existing Website (URL) *</label>
                            <input id="pages" type="text" class="form-control" name="account[web][existing_web_url]" value="{{ !empty($account->web) ? $account->web->existing_web_url : old('existing_web_url') }}">
                            @if ($errors->has('existing_web_url'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('existing_web_url') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Preferred Reference Website *</label>
                            <textarea class="form-control" id="reference_web" name="account[web][reference_web]" rows="5">{{ !empty($account->web) ? $account->web->reference_web : old('reference_web') }}</textarea>
                            @if ($errors->has('reference_web'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('reference_web') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Look and Feel *</label>
                            <textarea class="form-control" id="web_ui" name="account[web][web_ui]" rows="5">{{ !empty($account->web) ? $account->web->web_ui : old('web_ui') }}</textarea>
                            @if ($errors->has('web_ui'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('web_ui') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Color Scheme *</label>
                            <textarea class="form-control" id="color_scheme" name="account[web][color_scheme]" rows="5">{{ !empty($account->web) ? $account->web->color_scheme : old('color_scheme') }}</textarea>
                            @if ($errors->has('color_scheme'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('color_scheme') }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Special Functionality *</label>
                            <textarea class="form-control" id="functionality" name="account[web][functionality]" rows="5">{{ !empty($account->web) ? $account->web->functionality : old('functionality') }}</textarea>
                            @if ($errors->has('functionality'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ $errors->first('functionality') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="clearfix">&nbsp;</div>
                
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-box">
                                <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                                @if(!empty($account->web->web_hist_data))
                                    @php 
                                        $fname = explode('|',$account->web->web_hist_data_fname);
                                        $hash = explode('|',$account->web->web_hist_data);
                                    @endphp
                                    <hr />
                                    @foreach($fname as $n => $row)
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                            </div>
                                            <div class="col-md-4 text-right">
                                                <a href="{{ route('account.view.pdf',['web',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                                <a href="{{ route('account.historicaldata.delete',[$account->id,'web',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                            </div>
                                        </div>
                                        <hr />
                                    @endforeach
                                    @if (count($hash) < 5)
                                        <div class="file-uploading web-file-uploading">
                                            <div class="file-loading">
                                                <input id="web_hist_data" name="web_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="file-uploading web-file-uploading">
                                        <div class="file-loading">
                                            <input id="web_hist_data" name="web_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                        </div>
                                    </div>
                                @endif
    
                                @if ($errors->has('web_hist_data'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('web_hist_data') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>


            </div>
        </div>
    </div>
</div>