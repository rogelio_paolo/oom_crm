<input type="hidden" name="wechat_included" id="wechat_included" value="{{ !empty($account->wechat) || old('wechat_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-24" aria-expanded="true" class="collapsed">
                Wechat
                </a> 
            </h4>
        </div>
        <div id="collapse-24" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Wechat OA Setup? *</label>
                            <input type="radio" id="has_wechat_type" name="account[wechat][has_wechat_type]" value="1" {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == '1' ? 'checked' : '') : (!empty($account->wechat) ? ($account->wechat->has_wechat_type ? 'checked' : '' ) : '') }}/>Yes
                            <input type="radio" id="has_wechat_type" name="account[wechat][has_wechat_type]" value="0" {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == '0' ? 'checked' : '') : (!empty($account->wechat) ? (!$account->wechat->has_wechat_type ? 'checked' : '' ) : 'checked') }} />No
                        </div>
                    </div>

                    <div class="wechat-type {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == 1 ? '' : 'hidden') : (!empty($account->wechat) ? ($account->wechat->has_wechat_type == 1 ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <label>Type of Account</label>
                            <div class="select-box">
                                <select class="form-control" id="wechat_type" name="account[wechat][wechat_type]">
                                    <option value="serviced" {{ empty(old('wechat_type')) ? 'selected' : (!empty($wechat) ? ($wechat['wechat_type'] == 'Serviced' ? 'selected' : '') : (old('wechat_type') == 'Serviced' ? 'selected' : '') ) }}>Serviced</option>
                                    <option value="subscription" {{ !empty(old('wechat_type')) ? (old('wechat_type') == 'Subscription' ? 'selected' : '') : (!empty($wechat) ? ($wechat['wechat_type'] == 'Subscription' ? 'selected' : '') : '') }}>Subscription</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Requires Menu Setup? *</label>
                            <input type="radio" id="has_wechat_menu_setup" name="account[wechat][has_wechat_menu_setup]" value="1" {{ !empty(old('has_wechat_menu_setup')) ? (old('has_wechat_menu_setup') == 1 ? 'checked' : '') : ( !empty($account->wechat) ? ($account->wechat->has_wechat_menu_setup == 1 ? 'checked' : '') : '') }}/>Yes
                            <input type="radio" id="has_wechat_menu_setup" name="account[wechat][has_wechat_menu_setup]" value="0" {{  empty(old('has_wechat_menu_setup')) ? (!empty($account->wechat) ? ($account->wechat->has_wechat_menu_setup == 0 ? 'checked' : '') : 'checked') : ( old('has_wechat_menu_setup') == 0 ? 'checked' : '' ) }} />No
                            @if ($errors->has('has_wechat_menu_setup'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('has_wechat_menu_setup') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Requires WeChat Advertising? *</label>
                            <input type="radio" id="has_wechat_advertising" name="has_wechat_advertising" value="1" {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == '1' ? 'checked' : '') : (!empty($account->wechat) ? ($account->wechat->wechat_location ? 'checked' : '' ) : '') }}/>Yes
                            <input type="radio" id="has_wechat_advertising" name="has_wechat_advertising" value="0" {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == '0' ? 'checked' : '') : (!empty($account->wechat) ? (!$account->wechat->wechat_location ? 'checked' : '' ) : 'checked') }} />No
                        </div>
                    </div>

                    <div class="wechat-advertising {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == 1 ? '' : 'hidden') : (!empty($account->wechat) ? ($account->wechat->wechat_location ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Type of Advertising * </label>
                                <div class="select-box">
                                    @php 
                                        $current_advertising = !empty($account->wechat) ? explode('|',$account->wechat->wechat_advertising_type) : '';
                                    @endphp
                                    <select id="wechat_advertising_type" name="account[wechat][wechat_advertising_type][]" class="multiselect-ui form-control" multiple="multiple">
                                        @foreach($advertising as $row)
                                        <option value="{{ $row->systemcode }}" {{ !empty(old('wechat_advertising_type')) && in_array($row->systemcode,old('wechat_advertising_type')) ? 'selected' : (!empty($account->wechat) && in_array($row->systemcode,$current_advertising) ? 'selected' : '') }} >{{ $row->systemdesc }}</option>
                                        @endforeach
                                    </select>
                                    {{-- <input type="hidden" id="wechat_advertising_type_array" name="account[wechat][wechat_advertising_type]" value="{{ !empty(old('wechat_advertising_type')) ? old('wechat_advertising_type') : (!empty) ?['wechat_advertising_type'] : '') }}" /> --}}
                                    @if ($errors->has('wechat_advertising_type'))
                                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_advertising_type') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Location *</label>
                                <input id="wechat_location" type="text" class="form-control" name="account[wechat][wechat_location]" value="{{ !empty($account->wechat) ? $account->wechat->wechat_location : old('wechat_location') }}" />
                                @if ($errors->has('wechat_location'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_location') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="wechat-advertising {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == 1 ? '' : 'hidden') : (!empty($account->wechat) ? ($account->wechat->wechat_location ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <label for="age_group">Age Group</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="wechat_age_from" class="label-inline">From</label>
                                    <div class="select-box">
                                    <select id="wechat_age_from" name="account[wechat][wechat_age_from]">
                                        @for($i = 1; $i <= 65; $i++)
                                            <option value="{{ $i }}" {{ !empty(old('wechat_age_from')) ? ($i == old('wechat_age_from') ? 'selected' : '') : (!empty($account->wechat) && $account->wechat->wechat_age_from == $i ? 'selected' : '') }}> {{ $i }} </option>
                                        @endfor
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="wechat_age_to" class="label-inline">To</label>
                                    <div class="select-box">
                                    <select id="wechat_age_to" name="account[wechat][wechat_age_to]">
                                        @for($i = 1; $i <= 65; $i++)
                                            <option value="{{ $i }}" {{ !empty(old('wechat_age_to')) ? ($i == old('wechat_age_to') ? 'selected' : '') : (!empty($account->wechat) && $account->wechat->wechat_age_to == $i ? 'selected' : '') }}> {{ $i == 65 ? '65+' : $i }} </option>
                                        @endfor
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label for="wechat_marital">Marital Status</label>
                                <input type="text" class="form-control" id="wechat_marital" name="account[wechat][wechat_marital]" value="{{ !empty(old('wechat_marital')) ? old('wechat_marital') : (!empty($account->wechat) ? $account->wechat->wechat_marital : '') }}" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="wechat_gender">Gender</label>
                            <div class="select-box">
                                <select class="form-control" id="wechat_gender" name="account[wechat][wechat_gender]">
                                    <option value="N" {{ empty(old('wechat_gender')) ? (!empty($account->wechat) ? ($account->wechat->wechat_gender == 'N' ? 'selected' : '') : 'selected') : '' }}>Select a Gender</option>
                                    <option value="M" {{ !empty(old('wechat_gender')) ? ( old('wechat_gender') == 'M' ? 'selected' : '') : ( !empty($account->wechat) ? ($account->wechat->wechat_gender == 'M' ? 'selected' : '') : '' ) }}>Male</option>
                                    <option value="F" {{ !empty(old('wechat_gender')) ? ( old('wechat_gender') == 'F' ? 'selected' : '') : ( !empty($account->wechat) ? ($account->wechat->wechat_gender == 'F' ? 'selected' : '') : '' ) }}>Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="wechat-advertising {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == 1 ? '' : 'hidden') : (!empty($account->wechat) ? ($account->wechat->wechat_location ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <label for="wechat_handphone">Handphone Device</label>
                            <div class="select-box">
                                <select class="form-control" id="wechat_handphone" name="account[wechat][wechat_handphone]">
                                    <option value="android" {{ !empty(old('wechat_handphone')) ? ( old('wechat_handphone') == 'android' ? 'selected' : '') : ( !empty($account->wechat) ? ($account->wechat->wechat_handphone == 'android' ? 'selected' : '') : '' ) }}>Android</option>
                                    <option value="ios" {{ !empty(old('wechat_handphone')) ? ( old('wechat_handphone') == 'ios' ? 'selected' : '') : ( !empty($account->wechat) ? ($account->wechat->wechat_handphone == 'ios' ? 'selected' : '') : '' ) }}>iOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label for="wechat_education">Education </label>
                                <input type="text" class="form-control" id="wechat_education" name="account[wechat][wechat_education]" value="{{ !empty(old('wechat_education')) ? old('wechat_education') : (!empty($account->wechat) ? $account->wechat->wechat_education : '') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->wechat->wechat_hist_data))
                            @php 
                            $fname = explode('|',$account->wechat->wechat_hist_data_fname);
                            $hash = explode('|',$account->wechat->wechat_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                </div>
                                <div class="col-md-4 text-right">
                                        <a href="{{ route('account.view.pdf',['wechat',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                        <a href="{{ route('account.historicaldata.delete',[$account->id,'wechat',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="wechat-file-uploading">
                                <div class="file-loading">
                                    <input id="wechat_hist_data" name="wechat_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="wechat-file-uploading">
                                <div class="file-loading">
                                    <input id="wechat_hist_data" name="wechat_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('wechat_hist_data'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>