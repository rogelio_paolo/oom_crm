<input type="hidden" name="sem_included" id="sem_included" value="{{ !empty($account->sem) || old('sem_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-3" aria-expanded="true" class="collapsed">
                SEM
                </a> 
            </h4>
        </div>
        <div id="collapse-3" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Target Country *</label>
                            <div class="select-box">
                                @php 
                                    $sem_countries = !empty($account->sem) ? explode('|',$account->sem->target_market) : '';
                                    $old_sem_countries = !empty(old('target_market')) ? explode('|',old('target_market')) : '';     
                                @endphp
                                <select class="selectpicker form-control" id="target_market" name="account[sem][target_market][]" data-size="10" data-live-search="true" data-actions-box="true" multiple="multiple">
                                @foreach($countries as $row)
                                <option value="{{ $row->code }}" {{ !empty(old('target_market')) && in_array($row->code,$old_sem_countries) ? 'selected' : (!empty($account->sem) && in_array($row->code,$sem_countries) ? 'selected' : '')  }}>{{ $row->country }}</option>
                                @endforeach
                                </select>

                                <input type="hidden" id="target_market_null" name="account[sem][target_market]" />
                                <input type="hidden" id="target_market_array" name="account[sem][target_market]" value="{{ !empty(old('target_market')) ? ( is_array(old('target_market')) ? implode('|',old('target_market')) : old('target_market') ) : (!empty($account->sem) ? $account->sem->target_market : '' ) }}" />

                                @if ($errors->has('target_market'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('target_market') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>List Account as *</label>
                            <div class="select-box">
                                <select class="form-control" name="account[sem][list_account]">
                                @foreach($listings as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('list_account')) && $row->systemcode == old('list_account') ? 'selected' : (!empty($account->sem) && $account->sem->list_account == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('list_account'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('list_account') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Media Budget Payment *</label>
                            <div class="select-box">
                                <select class="form-control" name="account[sem][payment_method]">
                                @foreach($payment as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('payment_method')) && $row->systemcode == old('payment_method') ? 'selected' : (!empty($account->sem) && $account->sem->payment_method == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('payment_method'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('payment_method') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Media Channels *</label>
                            <div class="select-box">
                                @php 
                                    $current_channels = !empty($account->sem) ? explode('|',$account->sem->media_channel) : '';
                                @endphp
                                <select id="media_channel" name="account[sem][media_channel][]" class="multiselect-ui form-control" multiple="multiple">
                                @foreach($channels as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('media_channel')) && in_array($row->systemcode,old('media_channel')) ? 'selected' : (!empty($account->sem) && in_array($row->systemcode,$current_channels) ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            
                            
                            @if ($errors->has('media_channel'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('media_channel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Brief & Expectations *</label>
                            <textarea class="form-control" name="account[sem][campaign]" rows="5">{!! !empty(old('campaign')) ? old('campaign') : (!empty($account->sem) ? $account->sem->campaign : '') !!}</textarea>
                            @if ($errors->has('campaign'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                <select class="form-control" id="duration" name="account[sem][duration]">
                                @foreach($duration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('duration')) && $row->systemcode == old('duration') ? 'selected' : (!empty($account->sem) && $account->sem->duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div id="duration-other" class="input-group in-other {{ !empty(old('duration_other')) || old('duration') == 'CD999' ? '' : ( !empty($account->sem->duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                <select class="btn campaign-interval" id="duration_other_picker" name="account[sem][duration_interval]">
                                @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('duration_interval')) ? (old('duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->sem) ? ($account->sem->duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                </span>
                                <input type="text" class="form-control" id="duration_other" name="account[sem][duration_other]" value="{{ !empty(old('duration_other')) ? old('duration_other') : ( !empty($account->sem->duration_other) ? $account->sem->duration_other : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('duration'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('duration') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="campaign_date_set">Is the campaign date set?</label>                           
                            <input type="radio" id="campaign_date_set" name="campaign_date_set" value="1" {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == '1' ? 'checked' : '') : (!empty($account->sem) ? (!empty($account->sem->campaign_datefrom) && !empty($account->sem->campaign_dateto) ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="campaign_date_set" name="campaign_date_set" value="0" {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == '0' ? 'checked' : '') : (!empty($account->sem) ? (!empty($account->sem->campaign_datefrom) && empty(!$account->sem->campaign_dateto) ? '' : 'checked' ) : 'checked') }}/>No
                        </div>                                                              
                    </div>
                    <div class="sem-campaign {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == '1' ? '' : 'hidden') : (!empty($account->sem) ? ($account->sem->campaign_datefrom && $account->sem->campaign_dateto ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Campaign Start Date *</label>
                                <input type="text" class="form-control datepicker" id="campaign_datefrom" name="account[sem][campaign_datefrom]" value="{{ !empty(old('campaign_datefrom')) ? old('campaign_datefrom') : (!empty($account->sem) ? $account->sem->campaign_datefrom : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('campaign_datefrom'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign_datefrom') }}</span>
                                @endif
                            </div>
                            <div class="form-box">
                                <label>Campaign End Date *</label>
                                <input type="text" class="form-control datepicker" id="campaign_dateto" name="account[sem][campaign_dateto]" value="{{ !empty(old('campaign_dateto')) ? old('campaign_dateto') : (!empty($account->sem) ? $account->sem->campaign_dateto : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('campaign_dateto'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign_dateto') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Ad Scheduling</label>
                            <input type="radio" id="ad_scheduling" name="account[sem][ad_scheduling]" value="1" {{ !empty(old('ad_scheduling')) ? (old('ad_scheduling') == 1 ? 'checked' : '') : ( !empty($account->sem) ? ($account->sem->ad_scheduling == 1 ? 'checked' : '') : '') }} />Yes
                            <input type="radio" id="ad_scheduling" name="account[sem][ad_scheduling]" value="0" {{ empty(old('ad_scheduling')) ? (!empty($account->sem) ? ($account->sem->ad_scheduling == 0 ? 'checked' : '') : 'checked') : ( old('ad_scheduling') == 0 ? 'checked' : '' ) }} />No
                            <textarea class="form-control {{ !empty(old('ad_scheduling')) ? (old('ad_scheduling') == 1 ? '' : 'hidden') : (!empty($account->sem) ? ($account->sem->ad_scheduling == '1' ? '' : 'hidden') : 'hidden') }}" id="ad_scheduling_remark" name="account[sem][ad_scheduling_remark]" rows="5">{{ !empty(old('ad_scheduling_remark')) ? old('ad_scheduling_remark') : (!empty($account->sem) ? $account->sem->ad_scheduling_remark : '') }}</textarea>
                        </div>

                        @if($errors->has('ad_scheduling_remark'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('ad_scheduling_remark') }}</span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="currency">Currency</label>
                            <div class="select-box">
                                <select class="form-control" id="currency" name="account[sem][currency]">
                                <option value="USD" {{ !empty(old('currency')) ? (old('currency') == 'USD' ? 'selected' : '') : (!empty($account->sem) ? ($account->sem->currency == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                                <option value="SGD" {{ empty(old('currency')) ? 'selected' : (!empty($account->sem) ? ($account->sem->currency == 'SGD' ? 'selected' : '') : (old('currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                                <option value="PHP" {{ !empty(old('currency')) ? (old('currency') == 'PHP' ? 'selected' : '') : (!empty($account->sem) ? ($account->sem->currency == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                                <option value="MYR" {{ !empty(old('currency')) ? (old('currency') == 'MYR' ? 'selected' : '') : (!empty($account->sem) ? ($account->sem->currency == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                  
                    {{--  <div class="col-md-4">&nbsp;</div>  --}}
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Account Management Fee *</label>
                            <input type="text" class="form-control" id="am_fee" name="account[sem][am_fee]" value="{{ !empty(old('am_fee')) ? old('am_fee') : (!empty($account->sem) && $account->sem->am_fee ? $account->sem->am_fee : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('am_fee'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('am_fee') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Total Campaign Budget *</label>
                            <input type="text" class="form-control" id="budget" name="account[sem][budget]" value="{{ !empty(old('budget')) ? old('budget') : (!empty($account->sem) && $account->sem->budget ? $account->sem->budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('budget') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Monthly Campaign Budget *</label>
                            <input type="text" class="form-control" id="google_monthly_budget" name="account[sem][google_monthly_budget]" value="{{ !empty(old('google_monthly_budget')) ? old('google_monthly_budget') : (!empty($account->sem) && $account->sem->google_monthly_budget ? $account->sem->google_monthly_budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('google_monthly_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('google_monthly_budget') }}</span>
                            @endif
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Daily Campaign Budget *</label>
                            <input type="text" class="form-control" id="google_daily_budget" name="account[sem][google_daily_budget]" value="{{ !empty(old('google_daily_budget')) ? old('google_daily_budget') : (!empty($account->sem) && $account->sem->google_daily_budget ? $account->sem->google_daily_budget : '') }}" onkeypress="return isNumberKey(event);" {{ old('ad_scheduling') == 1 || (!empty($account->sem) && $account->sem->ad_scheduling == 1) ? '' : 'readonly' }}>
                            @if ($errors->has('google_daily_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('google_daily_budget') }}</span>
                            @endif
                        </div>
                    </div>   
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->sem->historical_data))
                                @php 
                                    $fname = explode('|',$account->sem->historical_data_fname);
                                    $hash = explode('|',$account->sem->historical_data);
                                @endphp
                                <hr />
                                @foreach($fname as $n => $row)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <a href="{{ route('account.view.pdf',['sem',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                            <a href="{{ route('account.historicaldata.delete',[$account->id,'sem',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                    <hr />
                                @endforeach
                                @if (count($hash) < 5)
                                    <div class="file-uploading sem-file-uploading">
                                        <div class="file-loading">
                                            <input id="historical_data" class="file" name="historical_data[]" multiple type="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="file-uploading sem-file-uploading">
                                    <div class="file-loading">
                                        <input id="historical_data" class="file" name="historical_data[]" multiple type="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                    </div>
                                </div>
                            @endif

                            @if ($errors->has('historical_data'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('historical_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>