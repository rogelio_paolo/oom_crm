<input type="hidden" name="fb_included" id="fb_included" value="{{ !empty($account->fb) || old('fb_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-4" aria-expanded="true" class="collapsed">
                Facebook Advertisement
                </a>
            </h4>
        </div>
        <div id="collapse-4" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Target Country *</label>
                            <div class="select-box">
                                @php 
                                    $fb_countries = !empty($account->fb) ? explode('|',$account->fb->fb_target_country) : '';
                                    $old_fb_countries = !empty(old('fb_target_country')) ? explode('|',old('fb_target_country')) : '';     
                                @endphp
                                <select class="selectpicker form-control" id="fb_target_country" name="account[fb][fb_target_country][]" data-live-search="true" data-actions-box="true" multiple="multiple">
                                @foreach($countries as $row)
                                <option value="{{ $row->code }}" {{ !empty(old('fb_target_country')) && in_array($row->code,$old_fb_countries) ? 'selected' : (!empty($account->fb) && in_array($row->code,$fb_countries) ? 'selected' : '') }}>{{ $row->country }}</option>
                                @endforeach
                                </select>

                                <input type="hidden" id="fb_targetmarket_null" name="account[fb][fb_target_country]" />
                                <input type="hidden" id="fb_targetmarket_array" name="account[fb][fb_target_country]" value="{{ !empty(old('fb_target_country')) ? ( is_array(old('fb_target_country')) ? implode('|',old('fb_target_country')) : old('fb_target_country') ) : (!empty($account->fb) ? $account->fb->fb_target_country : '' ) }}" />

                                @if ($errors->has('fb_target_country'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_target_country') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Listing Account as *</label>
                            <div class="select-box">
                                <select class="form-control" name="account[fb][fb_list_account]">
                                @foreach($listings_fb as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('fb_list_account')) ? ($row->systemcode == old('fb_list_account') ? 'selected' : '')  : (!empty($account->fb) &&  $account->fb->fb_list_account == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('fb_list_account'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_list_account') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Media Budget Payment *</label>
                            <div class="select-box">
                                <select class="form-control" name="account[fb][fb_payment]">
                                @foreach($payment as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('fb_payment')) ? ($row->systemcode == old('fb_payment') ? 'selected' : '')  : (!empty($account->fb) &&  $account->fb->fb_payment == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('fb_payment'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_payment') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Media Channel *</label>
                            <div class="select-box">
                                <select id="fb_media_channel" name="account[fb][fb_media_channel][]" class="multiselect-ui form-control" multiple="multiple">
                                @php $curr_channels = !empty($account->fb) ? explode('|',$account->fb->fb_media_channel) : '' @endphp
                                @foreach($fbchannels as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('fb_media_channel')) ? (in_array($row->systemcode,old('fb_media_channel')) ? 'selected' : '') : (!empty($account->fb) && in_array($row->systemcode,$curr_channels) ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            
                            @if ($errors->has('fb_media_channel'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_media_channel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Brief & Expectations *</label>
                            <textarea class="form-control" name="account[fb][fb_campaign]" rows="5">{!! !empty(old('fb_campaign')) ? old('fb_campaign') : (!empty($account->fb) ? $account->fb->fb_campaign : '') !!}</textarea>
                            @if ($errors->has('fb_campaign'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                <select class="form-control" id="fb_duration" name="account[fb][fb_duration]">
                                    @foreach($duration as $row)
                                    <option value="{{ $row->systemcode }}" {{ !empty(old('fb_duration')) ? ($row->systemcode == old('fb_duration') ? 'selected' : '') : (!empty($account->fb) && $account->fb->fb_duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div id="fb-duration-other" class="input-group in-other {{ !empty(old('fb_duration_other')) || old('fb_duration') == 'CD999' ? '' : ( !empty($account->fb->fb_duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                    <select class="btn campaign-interval" id="fb_other_picker" name="account[fb][fb_duration_interval]">
                                        @foreach($otherduration as $row)
                                            <option value="{{ $row->systemcode }}" {{ !empty(old('fb_duration_interval')) ? (old('fb_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->fb) ? ($account->fb->fb_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                        @endforeach
                                    </select>
                                </span>
                                <input type="text" class="form-control" id="fb_duration_other" name="account[fb][fb_duration_other]" value="{{ !empty(old('fb_duration_other')) ? old('fb_duration_other') : ( !empty($account->fb->fb_duration_other) ? $account->fb->fb_duration_other : '' ) }}" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('fb_duration'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_duration') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="fb_campaign_date_set">Is the campaign date set?</label>
                            <input type="radio" id="fb_campaign_date_set" name="fb_campaign_date_set" value="1" {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == '1' ? 'checked' : '') : (!empty($account->fb) ? (!empty($account->fb->fb_campaign_dt_from) && !empty($account->fb->fb_campaign_dt_to) ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="fb_campaign_date_set" name="fb_campaign_date_set" value="0" {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == '0' ? 'checked' : '') : (!empty($account->fb) ? (!empty($account->fb->fb_campaign_dt_from) && empty(!$account->fb->fb_campaign_dt_to) ? '' : 'checked' ) : 'checked') }}/>No
                        </div>
                    </div>
                    <div class="fb-campaign {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == 1 ? '' : 'hidden') : (!empty($account->fb) ? ($account->fb->fb_campaign_dt_from && $account->fb->fb_campaign_dt_to ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Campaign Start Date *</label>
                                <input type="text" class="form-control datepicker" id="fb_campaign_dt_from" name="account[fb][fb_campaign_dt_from]" value="{{ !empty(old('fb_campaign_dt_from')) ? old('fb_campaign_dt_from') : (!empty($account->fb) ? $account->fb->fb_campaign_dt_from : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('fb_campaign_dt_from'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign_dt_from') }}</span>
                                @endif
                            </div>
                            <div class="form-box">
                                <label>Campaign End Date *</label>
                                <input type="text" class="form-control datepicker" id="fb_campaign_dt_to" name="account[fb][fb_campaign_dt_to]" value="{{ !empty(old('fb_campaign_dt_to')) ? old('fb_campaign_dt_to') : (!empty($account->fb) ? $account->fb->fb_campaign_dt_to : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('fb_campaign_dt_to'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign_dt_to') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="fb_currency">Currency</label>
                            <div class="select-box">
                                <select class="form-control" id="fb_currency" name="account[fb][fb_currency]">
                                    <option value="USD" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'USD' ? 'selected' : '') : (!empty($account->fb) ? ($account->fb->fb_currency == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                                    <option value="SGD" {{ empty(old('fb_currency')) ? 'selected' : (!empty($account->fb) ? ($account->fb->fb_currency == 'SGD' ? 'selected' : '') : (old('fb_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                                    <option value="PHP" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'PHP' ? 'selected' : '') : (!empty($account->fb) ? ($account->fb->fb_currency == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                                    <option value="MYR" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'MYR' ? 'selected' : '') : (!empty($account->fb) ? ($account->fb->fb_currency == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Total Campaign Budget *</label>
                            <input type="text" class="form-control" id="total_budget" name="account[fb][total_budget]" value="{{ !empty(old('total_budget')) ? old('total_budget') : (!empty($account->fb) && $account->fb->total_budget ? $account->fb->total_budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('total_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('total_budget') }}</span>
                            @endif
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Monthly Campaign Budget *</label>
                            <input type="text" class="form-control" id="monthly_budget" name="account[fb][monthly_budget]" value="{{ !empty(old('monthly_budget')) ? old('monthly_budget') : (!empty($account->fb) && $account->fb->monthly_budget ? $account->fb->monthly_budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('monthly_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('monthly_budget') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Daily Campaign Budget *</label>
                            <input type="text" class="form-control" id="daily_budget" name="account[fb][daily_budget]" value="{{ !empty(old('daily_budget')) ? old('daily_budget') : (!empty($account->fb) && $account->fb->daily_budget ? $account->fb->daily_budget : '') }}" onkeypress="return isNumberKey(event);" readonly="true">
                            @if ($errors->has('daily_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('daily_budget') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Gender *</label>
                            <select class="form-control" name="account[fb][gender]">
                            <option value="A" {{ empty(old('gender')) ? (!empty($account->fb) ? ($account->fb->gender == 'A' ? 'selected' : '') : 'selected') : '' }}>All</option>
                            <option value="M" {{ !empty(old('gender')) ? ( old('gender') == 'M' ? 'selected' : '') : ( !empty($account->fb) ? ($account->fb->gender == 'M' ? 'selected' : '') : '' ) }}>Male</option>
                            <option value="F" {{ !empty(old('gender')) ? ( old('gender') == 'F' ? 'selected' : '') : ( !empty($account->fb) ? ($account->fb->gender == 'F' ? 'selected' : '') : '' ) }}>Female</option>
                            </select>
                            @if ($errors->has('gender'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Interest *</label>
                            <textarea class="form-control" id="interest" name="account[fb][interest]" rows="5">{!! !empty($account->fb) ? $account->fb->interest : '' !!}</textarea>
                            @if ($errors->has('interest'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('interest') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="age_group" class="control-label">Age Group *</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="age_from" class="label-inline">From</label>
                                    <div class="select-box">
                                        <select id="age_from" name="account[fb][age_from]">
                                        @for($i = 1; $i <= 65; $i++)
                                        <option value="{{ $i }}" {{ !empty(old('age_from')) ? ($i == old('age_from') ? 'selected' : '') : (!empty($account->fb) && $account->fb->age_from == $i ? 'selected' : '') }}> {{ $i }} </option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="age_to" class="label-inline">To</label>
                                    <div class="select-box">
                                        <select id="age_to" name="account[fb][age_to]">
                                        @for($i = 1; $i <= 65; $i++)
                                        <option value="{{ $i }}" {{ !empty(old('age_to')) ? ($i == old('age_to') ? 'selected' : '') : (!empty($account->fb) && $account->fb->age_to == $i ? 'selected' : '') }}> {{ $i == 65 ? '65+' : $i }} </option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                            </div>
                        </div>
                        @if ($errors->has('age_group'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('age_group') }}</span>
                        @endif
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(!empty($account->fb->fb_hist_data))
                            @php 
                            $fname = explode('|',$account->fb->fb_hist_data_fname);
                            $hash = explode('|',$account->fb->fb_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p> 
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ route('account.view.pdf',['fb',$hash[$n],$row] ) }}" target="_blank" class="btn btn-info">View</a>
                                    <a href="{{ route('account.historicaldata.delete',[$account->id,'fb',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="file-uploading fb-file-uploading">
                                <div class="file-loading">
                                    <input id="fb_hist_data" name="fb_hist_data[]" multiple="multiple" type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="file-uploading fb-file-uploading">
                                <div class="file-loading">
                                    <input id="fb_hist_data" name="fb_hist_data[]" multiple="multiple" type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('fb_hist_data'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>