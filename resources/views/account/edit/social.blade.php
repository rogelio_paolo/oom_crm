<input type="hidden" name="social_included" id="social_included" value="{{ !empty($account->social) || old('social_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-21" aria-expanded="true" class="collapsed">
                Social Media Management
                </a> 
            </h4>
        </div>
        <div id="collapse-21" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                <select class="form-control" id="social_duration" name="account[social][social_duration]">
                                @foreach($duration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('social_duration')) && $row->systemcode == old('social_duration') ? 'selected' : (!empty($account->social) && $account->social->social_duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div id="social-duration-other" class="input-group in-other {{ !empty(old('social-duration_other')) || old('social_duration') == 'CD999' ? '' : ( !empty($account->social->social->duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                <select class="btn campaign-interval" id="social_duration_other_picker" name="account[social][social_duration_interval]">
                                @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('social_duration_interval')) ? (old('social_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->social) ? ($account->social->social_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                </span>
                                <input type="text" class="form-control" id="social_duration_other" name="account[social][social_duration_other]" value="{{ !empty(old('social_duration_other')) ? old('social_duration_other') : ( !empty($account->social->social_duration_other) ? $account->social->social_duration_other : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('social_duration'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_duration') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Number of Blog Post Per Month *</label>
                            <input id="social_monthly_post" type="text" class="form-control" name="account[social][social_monthly_post]" value="{{ !empty($account->social) ? $account->social->social_monthly_post : old('social_monthly_post') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('social_monthly_post'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_monthly_post') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Content Brief and Writing Style *</label>
                            <textarea rows="5" id="social_content_brief" class="form-control" name="account[social][social_content_brief]">{{ !empty($account->social) ? $account->social->social_content_brief : old('social_content_brief') }}</textarea>
                            @if ($errors->has('social_content_brief'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_content_brief') }}</span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Any FB Media Budget to boost post? *</label>
                            <input type="radio" id="has_social_media_budget" name="has_social_media_budget" value="1" {{ !empty(old('has_social_media_budget')) ? (old('has_social_media_budget') == '1' ? 'checked' : '') : (!empty($account->social) ? ($account->social->social_media_budget ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="has_social_media_budget" name="has_social_media_budget" value="0" {{ !empty(old('has_social_media_budget')) ? (old('has_social_media_budget') == '0' ? 'checked' : '') : (!empty($account->social) ? (!$account->social->social_media_budget ? 'checked' : '' ) : 'checked') }} />No
                        </div>
                    </div>
                    <div class="social-media-budget {{ !empty(old('has_social_media_budget')) ? (old('has_social_media_budget') == 1 ? '' : 'hidden') : (!empty($account->social) ? ($account->social->social_media_budget ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Media Budget * </label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" id="social_media_budget" name="account[social][social_media_budget]" value="{{ !empty($account->social) ? $account->social->social_media_budget : old('social_media_budget') }}" onkeypress="return isNumberKey(event);">
                                </div>
                                @if ($errors->has('social_media_budget'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_media_budget') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->social->social_hist_data))
                            @php 
                            $fname = explode('|',$account->social->social_hist_data_fname);
                            $hash = explode('|',$account->social->social_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ route('account.view.pdf',['social',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                    <a href="{{ route('account.historicaldata.delete',[$account->id,'social',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="social-file-uploading">
                                <div class="file-loading">
                                    <input id="social_hist_data" name="social_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="social-file-uploading">
                                <div class="file-loading">
                                    <input id="social_hist_data" name="social_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('social_hist_data'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>