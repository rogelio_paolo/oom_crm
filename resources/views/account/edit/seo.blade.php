<input type="hidden" name="seo_included" id="seo_included" value="{{ !empty($account->seo) || old('seo_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-5" aria-expanded="true" class="collapsed">
                SEO
                </a>
            </h4>
        </div>
        <div id="collapse-5" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Target Country *</label>
                            <div class="select-box">
                                @php $seo_countries = !empty($account->seo) ? explode('|',$account->seo->seo_target_countries) : '' @endphp
                                <select class="selectpicker form-control" id="seo_target_countries" name="account[seo][seo_target_countries]" data-live-search="true" data-actions-box="true" multiple="multiple">
                                @foreach($countries as $row)
                                <option value="{{ $row->code }}" {{ !empty($account->seo) && in_array($row->code,$seo_countries) ? 'selected' : '' }}>{{ $row->country }}</option>
                                @endforeach
                                </select>

                                <input type="hidden" id="seo_targetmarket_null" name="account[seo][seo_target_countries]" />
                                <input type="hidden" id="seo_targetmarket_array" name="account[seo][seo_target_countries]" value="{{ !empty(old('seo_target_countries')) ? ( is_array(old('seo_target_countries')) ? implode('|',old('seo_target_countries')) : old('seo_target_countries') ) : (!empty($account->seo) ? $account->seo->seo_target_countries : '' ) }}" />
                                

                                @if ($errors->has('seo_target_countries'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_target_countries') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>No of Keywords *</label>
                            <div class="select-box">
                                <select class="form-control" id="keyword_themes_select" name="account[seo][keyword_themes]">
                                @foreach($keywordsno as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty($account->seo) && $account->seo->keyword_themes == $row->systemcode ? 'selected' : '' }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                <input class="form-control {{ !empty($account->seo) && $account->seo->keyword_themes != 'NK999' ? 'hidden' : '' }}" type="text" id="keyword_themes" name="package[seo][keyword_themes_other]" value="{{ !empty($account->seo) ? $account->seo->keyword_themes_other : '' }}" onkeypress="return isNumberKey(event);"/>
                                @if ($errors->has('keyword_themes'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_themes') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                <select class="form-control" id="seo_duration" name="account[seo][seo_duration]">
                                @foreach($duration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty($account->seo) && $account->seo->seo_duration == $row->systemcode ? 'selected' : '' }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div id="seo-duration-other" class="input-group in-other {{ !empty(old('seo_duration_other')) || old('seo_duration') == 'CD999' ? '' : ( !empty($account->seo->seo_duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                <select class="btn campaign-interval" id="seo_other_picker" name="account[seo][seo_duration_interval]">
                                @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('seo_duration_interval')) ? (old('seo_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->seo) ? ($account->seo->seo_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                </span>
                                <input type="text" class="form-control" id="seo_duration_other" name="account[seo][seo_duration_other]" value="{{ !empty(old('seo_duration_other')) ? old('seo_duration_other') : ( !empty($account->seo->seo_duration_other) ? $account->seo->seo_duration_other : '' ) }}" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('seo_duration'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_duration') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Brief & Expectations *</label>
                            <textarea class="form-control" name="account[seo][seo_campaign]" rows="5">{!! !empty($account->seo) && $account->seo->seo_campaign ? $account->seo->seo_campaign : old('seo_campaign')  !!}</textarea>
                            @if ($errors->has('seo_campaign'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_campaign') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Keyword Theme Focus *</label>
                            <textarea class="form-control" name="account[seo][keyword_focus]" rows="5">{{ !empty($account->seo) && $account->seo->keyword_focus ? $account->seo->keyword_focus : old('keyword_focus') }}</textarea>
                            @if ($errors->has('keyword_focus'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_focus') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Keyword Maintenance *</label>
                            <input type="radio" id="has_keyword_maintenance" name="has_keyword_maintenance" value="1" {{ !empty(old('has_keyword_maintenance')) && old('has_keyword_maintenance') == '1' ? 'checked' : (!empty($account->seo) && !empty($account->seo->keyword_maintenance) ? 'checked' : '') }} />Yes
                            <input type="radio" id="has_keyword_maintenance" name="has_keyword_maintenance" value="0" {{ !empty(old('has_keyword_maintenance')) ? (old('has_keyword_maintenance') == '0' ? 'checked' : '' ) : (!empty($account->seo) ? (empty($account->seo->keyword_maintenance) ? 'checked' : '') : 'checked') }} />No
                            <textarea class="form-control {{ !empty(old('account[seo][keyword_maintenance]')) ? (old('account[seo][keyword_maintenance]') == '1' ? '' : 'hidden') : (!empty($account->seo) ? (!empty($account->seo->keyword_maintenance) ? '' : 'hidden' ) : 'hidden') }}" id="keyword_maintenance" name="account[seo][keyword_maintenance]" rows="5">{!! !empty($account->seo) ? $account->seo->keyword_maintenance : null !!}</textarea>
                            @if ($errors->has('keyword_maintenance'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_maintenance') }}</span>
                            @endif
                        </div>
                    </div>
               

                <div class="col-md-4">
                    <div class="form-box">
                        <label for="currency">Currency</label>
                        <div class="select-box">
                            <select class="form-control" id="seo_currency" name="account[seo][seo_currency]">
                            <option value="USD" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'USD' ? 'selected' : '') : (!empty($account->seo) ? ($account->seo->seo_currency == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                            <option value="SGD" {{ empty(old('seo_currency')) ? 'selected' : (!empty($account->seo) ? ($account->seo->seo_currency == 'SGD' ? 'selected' : '') : (old('seo_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                            <option value="PHP" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'PHP' ? 'selected' : '') : (!empty($account->seo) ? ($account->seo->seo_currency == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                            <option value="MYR" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'MYR' ? 'selected' : '') : (!empty($account->seo) ? ($account->seo->seo_currency == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-box">
                        <label>SEO Total Fee *</label>
                        <input type="text" class="form-control" id="am_fee" name="account[seo][seo_total_fee]" value="{{ !empty(old('seo_total_fee')) ? old('seo_total_fee') : (!empty($account->seo) && $account->seo->seo_total_fee ? $account->seo->seo_total_fee : '') }}" onkeypress="return isNumberKey(event);">
                        @if ($errors->has('seo_total_fee'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_total_fee') }}</span>
                        @endif
                    </div>
                </div>

            </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(!empty($account->seo->seo_hist_data))
                                @php 
                                    $fname = explode('|',$account->seo->seo_hist_data_fname);
                                    $hash = explode('|',$account->seo->seo_hist_data);
                                @endphp
                                <hr />
                                @foreach($fname as $n => $row)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <a href="{{ route('account.view.pdf',['seo',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                            <a href="{{ route('account.historicaldata.delete',[$account->id,'seo',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                    <hr />
                                @endforeach
                                @if (count($hash) < 5)
                                    <div class="file-uploading seo-file-uploading">
                                        <div class="file-loading">
                                            <input id="seo_hist_data" name="seo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="file-uploading seo-file-uploading">
                                    <div class="file-loading">
                                        <input id="seo_hist_data" name="seo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                    </div>
                                </div>
                            @endif

                            @if ($errors->has('seo_hist_data'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>