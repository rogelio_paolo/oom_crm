<input type="hidden" name="blog_included" id="blog_included" value="{{ !empty($account->blog) || old('blog_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-20" aria-expanded="true" class="collapsed">
                Blog Content
                </a> 
            </h4>
        </div>
        <div id="collapse-20" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Total Number of Blog Post *</label>
                            <input id="blog_total_post" type="text" class="form-control" name="account[blog][blog_total_post]" value="{{ !empty($account->blog) ? $account->blog->blog_total_post : old('blog_total_post') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('blog_total_post'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('blog_total_post') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Number of Blog Post Per Month</label>
                            <input id="blog_monthly_post" type="text" class="form-control" name="account[blog][blog_monthly_post]" value="{{ !empty($account->blog) ? $account->blog->blog_monthly_post : old('blog_monthly_post') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('blog_monthly_post'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('blog_monthly_post') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Content Brief and Writing Style *</label>
                            <textarea id="blog_content_brief" class="form-control" name="account[blog][blog_content_brief]" rows="5">{{ !empty($account->blog) ? $account->blog->blog_content_brief : old('blog_content_brief') }}</textarea>
                            @if ($errors->has('blog_content_brief'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('blog_content_brief') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->blog->blog_hist_data))
                            @php 
                            $fname = explode('|',$account->blog->blog_hist_data_fname);
                            $hash = explode('|',$account->blog->blog_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ route('account.view.pdf',['blog',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                    <a href="{{ route('account.historicaldata.delete',[$account->id,'blog',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="blog-file-uploading">
                                <div class="file-loading">
                                    <input id="blog_hist_data" name="blog_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="blog-file-uploading">
                                <div class="file-loading">
                                    <input id="blog_hist_data" name="blog_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('blog_hist_data'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('blog_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>