<input type="hidden" name="weibo_included" id="weibo_included" value="{{ !empty($account->weibo) || old('weibo_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-23" aria-expanded="true" class="collapsed">
                Weibo
                </a> 
            </h4>
        </div>
        <div id="collapse-23" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Location *</label>
                            <input id="weibo_location" type="text" class="form-control" name="account[weibo][weibo_location]" value="{{ !empty($account->weibo) ? $account->weibo->weibo_location : old('weibo_location') }}"/>
                            @if ($errors->has('weibo_location'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_location') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Existing Weibo Account? *</label>
                            <input type="radio" id="has_weibo_account" name="has_weibo_account" value="1" {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == '1' ? 'checked' : '') : (!empty($account->weibo) ? ($account->weibo->weibo_account ? 'checked' : '' ) : '') }} />Yes
                            <input type="radio" id="has_weibo_account" name="has_weibo_account" value="0" {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == '0' ? 'checked' : '') : (!empty($account->weibo) ? (!$account->weibo->weibo_account ? 'checked' : '' ) : 'checked') }} />No
                        </div>
                    </div>
                    <div class="weibo-account {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == 1 ? '' : 'hidden') : (!empty($account->weibo) ? ($account->weibo->weibo_account ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <label for="baidu_target_market">Weibo Account User ID *</label>
                            <input id="weibo_account" type="text" class="form-control" name="account[weibo][weibo_account]" value="{{ !empty($account->weibo) ? $account->weibo->weibo_account : old('weibo_account') }}" />
                            @if ($errors->has('weibo_account'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_account') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Requires New Blue V Weibo Setup? *</label>
                            <input type="radio" id="has_blue_v_weibo" name="has_blue_v_weibo" value="1" {{ !empty(old('has_blue_v_weibo')) ? (old('has_blue_v_weibo') == 1 ? 'checked' : '') : ( !empty($account->weibo) ? ($account->weibo->has_blue_v_weibo == 1 ? 'checked' : '') : '') }}/>Yes
                            <input type="radio" id="has_blue_v_weibo" name="has_blue_v_weibo" value="0" {{  empty(old('has_blue_v_weibo')) ? (!empty($account->weibo) ? ($account->weibo->has_blue_v_weibo == 0 ? 'checked' : '') : 'checked') : ( old('has_blue_v_weibo') == 0 ? 'checked' : '' ) }} />No
                        </div>
                        @if ($errors->has('has_blue_v_weibo'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('has_blue_v_weibo') }}</span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Brief & Expectations</label>
                            <textarea name="account[weibo][weibo_campaign]" class="form-control" id="weibo_campaign" rows="3">{!! !empty($account->weibo) ? $account->weibo->weibo_campaign : old('weibo_campaign') !!}</textarea>
                            @if ($errors->has('weibo_campaign'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration</label>
                            <div class="select-box">
                                <select class="form-control" id="weibo_duration" name="account[weibo][weibo_duration]">
                                @foreach($duration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('weibo_duration')) && $row->systemcode == old('weibo_duration') ? 'selected' : (!empty($account->weibo) && $account->weibo->weibo_duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div id="weibo-duration-other" class="input-group in-other {{ !empty(old('weibo-duration_other')) || old('weibo_duration') == 'CD999' ? '' : ( !empty($account->weibo->weibo_duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                <select class="btn campaign-interval" id="weibo_duration_other_picker" name="account[weibo][weibo_duration_interval]">
                                @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('weibo_duration_interval')) ? (old('weibo_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->weibo) ? ($account->weibo->weibo_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                </span>
                                <input type="text" class="form-control" id="weibo_duration_other" name="account[weibo][weibo_duration_other]" value="{{ !empty(old('weibo_duration_other')) ? old('weibo_duration_other') : ( !empty($account->weibo->weibo_duration_other) ? $account->weibo->weibo_duration_other : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('weibo_duration'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_duration') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="has_weibo_campaign_dateset">Is the Campaign Date Set?</label>
                        <div class="form-box">
                            <input type="radio" id="has_weibo_campaign_dateset" name="has_weibo_campaign_dateset" value="1" {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == '1' ? 'checked' : '') : (!empty($account->weibo) ? (!empty($account->weibo->weibo_campaign_datefrom) && !empty($account->weibo->weibo_campaign_dateto) ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="has_weibo_campaign_dateset" name="has_weibo_campaign_dateset" value="0" {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == '0' ? 'checked' : '') : (!empty($account->weibo) ? (!empty($account->weibo->weibo_campaign_datefrom) && empty(!$account->weibo->weibo_campaign_dateto) ? '' : 'checked' ) : 'checked') }}/>No
                        </div>
                    </div>

                    <div class="weibo-campaign {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == 1 ? '' : 'hidden') : (!empty($account->weibo) ? ($account->weibo->weibo_campaign_datefrom && $account->weibo->weibo_campaign_dateto ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label for="weibo_campaign_datefrom">Campaign Start Date *</label>
                                <input type="text" class="form-control datepicker" id="weibo_campaign_datefrom" name="account[weibo][weibo_campaign_datefrom]" value="{{ !empty(old('weibo_campaign_datefrom')) ? old('weibo_campaign_datefrom') : (!empty($account->weibo) ? $account->weibo->weibo_campaign_datefrom : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('weibo_campaign_datefrom'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign_datefrom') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label for="weibo_campaign_dateto">Campaign End Date *</label>
                                <input type="text" class="form-control datepicker" id="weibo_campaign_dateto" name="account[weibo][weibo_campaign_dateto]" value="{{ !empty(old('weibo_campaign_dateto')) ? old('weibo_campaign_dateto') : (!empty($account->weibo) ? $account->weibo->weibo_campaign_dateto : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('weibo_campaign_dateto'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign_dateto') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="has_weibo_ad_scheduling">Ad Scheduling</label>
                        <div class="form-box">
                            <input type="radio" id="has_weibo_ad_scheduling" name="account[weibo][has_weibo_ad_scheduling]" value="1" {{ !empty(old('has_weibo_ad_scheduling')) ? (old('has_weibo_ad_scheduling') == 1 ? 'checked' : '') : ( !empty($account->weibo) ? ($account->weibo->has_weibo_ad_scheduling == 1 ? 'checked' : '') : '') }}/>Yes
                            <input type="radio" id="has_weibo_ad_scheduling" name="account[weibo][has_weibo_ad_scheduling]" value="0" {{  empty(old('has_weibo_ad_scheduling')) ? (!empty($account->weibo) ? ($account->weibo->has_weibo_ad_scheduling == 0 ? 'checked' : '') : 'checked') : ( old('has_weibo_ad_scheduling') == 0 ? 'checked' : '' ) }} />No
                        </div>
                        <div class="form-box">
                            <textarea class="form-control {{ !empty(old('has_weibo_ad_scheduling')) ? (old('has_weibo_ad_scheduling') == 1 ? '' : 'hidden') : (!empty($account->weibo) ? ($account->weibo->has_weibo_ad_scheduling == '1' ? '' : 'hidden') : 'hidden') }}" id="weibo_ad_scheduling_remark" name="account[weibo][weibo_ad_scheduling_remark]" rows="2" >{{ !empty(old('weibo_ad_scheduling_remark')) ? old('weibo_ad_scheduling_remark') : (!empty($account->weibo) ? $account->weibo->weibo_ad_scheduling_remark : '') }}</textarea>
                            @if ($errors->has('weibo_ad_scheduling_remark'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_ad_scheduling_remark') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="baidu_currency">Currency</label>
                        <div class="select-box">
                            <select class="form-control" id="weibo_currency" name="account[weibo][weibo_currency]">
                                    <option value="USD" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'USD' ? 'selected' : '') : (!empty($account->weibo) ? ($account->weibo->weibo_currency == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                                    <option value="SGD" {{ empty(old('weibo_currency')) ? 'selected' : (!empty($account->weibo) ? ($account->weibo->weibo_currency == 'SGD' ? 'selected' : '') : (old('baidu_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                                    <option value="PHP" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'PHP' ? 'selected' : '') : (!empty($account->weibo) ? ($account->weibo->weibo_currency == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                                    <option value="MYR" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'MYR' ? 'selected' : '') : (!empty($account->weibo) ? ($account->weibo->weibo_currency == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                            </select>
                            @if ($errors->has('weibo_currency'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_currency') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="weibo_budget">Total Campaign Budget</label>
                        <input type="text" class="form-control" id="weibo_budget" name="account[weibo][weibo_budget]" value="{{ !empty(old('weibo_budget')) ? old('weibo_budget') : (!empty($account->weibo) ? $account->weibo->weibo_budget : '') }}" onkeypress="return isNumberKey(event);">
                        @if ($errors->has('weibo_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_budget') }}</span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="weibo_monthly_budget">Monthly Campaign Budget</label>
                        <input type="text" class="form-control" id="weibo_monthly_budget" name="account[weibo][weibo_monthly_budget]" value="{{ !empty(old('weibo_monthly_budget')) ? old('weibo_monthly_budget') : (!empty($account->weibo) ? $account->weibo->weibo_monthly_budget : '') }}" onkeypress="return isNumberKey(event);">
                        @if ($errors->has('weibo_monthly_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_monthly_budget') }}</span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="weibo_daily_budget">Daily Campaign Budget</label>
                        <input type="text" class="form-control" id="weibo_daily_budget" name="account[weibo][weibo_daily_budget]" value="{{ !empty(old('weibo_daily_budget')) ? old('weibo_daily_budget') : (!empty($account->weibo) ? $account->weibo->weibo_daily_budget : '') }}" readonly onkeypress="return isNumberKey(event);">
                        @if ($errors->has('weibo_daily_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_daily_budget') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->weibo->weibo_hist_data))
                            @php 
                            $fname = explode('|',$account->weibo->weibo_hist_data_fname);
                            $hash = explode('|',$account->weibo->weibo_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                </div>
                                <div class="col-md-4 text-right">
                                        <a href="{{ route('account.view.pdf',['weibo',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                        <a href="{{ route('account.historicaldata.delete',[$account->id,'weibo',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="weibo-file-uploading">
                                <div class="file-loading">
                                    <input id="weibo_hist_data" name="weibo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="weibo-file-uploading">
                                <div class="file-loading">
                                    <input id="weibo_hist_data" name="weibo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('weibo_hist_data'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>