<input type="hidden" name="postpaid_included" id="postpaid_included" value="{{ !empty($account->postpaid) || old('postpaid_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-44" aria-expanded="true" class="collapsed">
                Post Paid SEM
                </a> 
            </h4>
        </div>
        <div id="collapse-44" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                    <select class="form-control" id="postpaid_duration" name="account[postpaid][postpaid_duration]">
                                    @foreach($duration as $row)
                                        <option value="{{ $row->systemcode }}" {{ !empty(old('postpaid_duration')) && $row->systemcode == old('postpaid_duration') ? 'selected' : (!empty($account->postpaid) && $account->postpaid->postpaid_duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div id="postpaid-duration-other" class="input-group in-other {{ !empty(old('postpaid_duration_other')) || old('postpaid_duration') == 'CD999' ? '' : ( !empty($account->postpaid->postpaid_duration_other) ? '' : 'hidden' ) }}">
                                    <span class="input-group-btn">
                                    <select class="btn campaign-interval" id="postpaid_duration_other_picker" name="account[postpaid][postpaid_duration_interval]">
                                    @foreach($otherduration as $row)
                                        <option value="{{ $row->systemcode }}" {{ !empty(old('postpaid_duration_interval')) ? (old('postpaid_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->postpaid) ? ($account->postpaid->postpaid_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                    @endforeach
                                    </select>
                                    </span>
                                    <input type="text" class="form-control" id="postpaid_duration_other" name="account[postpaid][postpaid_duration_other]" value="{{ !empty(old('postpaid_duration_other')) ? old('postpaid_duration_other') : ( !empty($account->postpaid->postpaid_duration_other) ? $account->postpaid->postpaid_duration_other : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                                </div>

                                @if ($errors->has('postpaid_duration_other'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_duration_other') }}</span>
                                @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="postpaid_campaign_date_set">Is the campaign date set?</label>                           
                            <input type="radio" id="postpaid_campaign_date_set" name="postpaid_campaign_date_set" value="1" {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == '1' ? 'checked' : '') : (!empty($account->postpaid) ? (!empty($account->postpaid->postpaid_campaign_datefrom) && !empty($account->postpaid->postpaid_campaign_dateto) ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="postpaid_campaign_date_set" name="postpaid_campaign_date_set" value="0" {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == '0' ? 'checked' : '') : (!empty($account->postpaid) ? (!empty($account->postpaid->postpaid_campaign_datefrom) && empty(!$account->postpaid->postpaid_campaign_dateto) ? '' : 'checked' ) : 'checked') }}/>No
                        </div>                                                              
                    </div>

                    <div class="postpaid-campaign {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == '1' ? '' : 'hidden') : (!empty($account->postpaid) ? ($account->postpaid->postpaid_campaign_datefrom && $account->postpaid->postpaid_campaign_dateto ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Campaign Start Date *</label>
                                <input type="text" class="form-control datepicker" id="postpaid_campaign_datefrom" name="account[postpaid][postpaid_campaign_datefrom]" value="{{ !empty(old('postpaid_campaign_datefrom')) ? old('postpaid_campaign_datefrom') : (!empty($account->postpaid) ? $account->postpaid->postpaid_campaign_datefrom : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                
                                @if ($errors->has('postpaid_campaign_datefrom'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_campaign_datefrom') }}</span>
                                @endif
                            </div>
                            <div class="form-box">
                                <label>Campaign End Date *</label>
                                <input type="text" class="form-control datepicker" id="postpaid_campaign_dateto" name="account[postpaid][postpaid_campaign_dateto]" value="{{ !empty(old('postpaid_campaign_dateto')) ? old('postpaid_campaign_dateto') : (!empty($account->postpaid) ? $account->postpaid->postpaid_campaign_dateto : '') }}" data-date-format="yyyy-mm-dd" readonly>
                                @if ($errors->has('postpaid_campaign_dateto'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_campaign_dateto') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">&nbsp;</div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Media Budget Utilized *</label>
                            <span id="media_budget_err" class="error-p account"></span>
                            <div class="input-group" id="input-group-media-budget">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="postpaid_media_budget" name="account[postpaid][postpaid_media_budget]" value="{{ !empty(old('postpaid_media_budget')) ? old('postpaid_media_budget') : (!empty($account->postpaid) && $account->postpaid->postpaid_media_budget ? $account->postpaid->postpaid_media_budget : '') }}" onkeypress="return isNumberKey(event);">
                            </div>
                            
                            @if ($errors->has('postpaid_media_budget'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_media_budget') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-box">
                            <label>AM Fee % *</label>
                            <span id="am_fee_err" class="error-p account"></span>
                            <input type="text" maxlength="3" class="form-control" id="postpaid_am_fee" name="account[postpaid][postpaid_am_fee]" value="{{ !empty(old('postpaid_am_fee')) ? old('postpaid_am_fee') : (!empty($account->postpaid) && $account->postpaid->postpaid_am_fee ? $account->postpaid->postpaid_am_fee : '') }}" onkeypress="return isNumberKey(event);">
                            
                            @if ($errors->has('postpaid_am_fee'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_am_fee') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>AM Fee Amount *</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input readonly="true" type="text" class="form-control" id="postpaid_am_fee_amount" name="account[postpaid][postpaid_am_fee_amount]" value="{{ !empty(old('postpaid_am_fee_amount')) ? old('postpaid_am_fee_amount') : (!empty($account->postpaid) && $account->postpaid->postpaid_am_fee_amount ? $account->postpaid->postpaid_am_fee_amount : '') }}" onkeypress="return isNumberKey(event);">
                            </div>

                            @if ($errors->has('postpaid_am_fee_amount'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_am_fee_amount') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="is_media_budget_paid">Is Media Budget paid through OOm? *</label>
                            <input type="radio" id="is_media_budget_paid" name="account[postpaid][is_media_budget_paid]" value="1" {{ !empty(old('is_media_budget_paid')) ? (old('is_media_budget_paid') == 1 ? 'checked' : '') : ( !empty($account->postpaid) ? ($account->postpaid->is_media_budget_paid == 1 ? 'checked' : '') : '') }} />Yes
                            <input type="radio" id="is_media_budget_paid" name="account[postpaid][is_media_budget_paid]" value="0" {{ empty(old('is_media_budget_paid')) ? (!empty($account->postpaid) ? ($account->postpaid->is_media_budget_paid == 0 ? 'checked' : '') : 'checked') : ( old('is_media_budget_paid') == 0 ? 'checked' : '' ) }} />No
                        </div>

                        @if($errors->has('is_media_budget_paid'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('is_media_budget_paid') }}</span>
                        @endif
                    </div>

                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Total Amount *</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input readonly="true" type="text" class="form-control" id="postpaid_total" name="account[postpaid][postpaid_total]" value="{{ !empty(old('postpaid_total')) ? old('postpaid_total') : (!empty($account->postpaid) && $account->postpaid->postpaid_total ? $account->postpaid->postpaid_total : '') }}" onkeypress="return isNumberKey(event);">
                            </div>
                            @if ($errors->has('postpaid_total'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_total') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span class="text-left">If <strong><u>Yes</u></strong> is selected the amount is <strong>AM Fee Amount</strong> + <strong>Media Budget Utilized</strong></span><br>
                    <span class="text-left">If <strong><u>No</u></strong> is selected the amount is <strong>AM Fee Amount</strong></span>
                </div>
            </div>
        </div>
    </div>
</div>