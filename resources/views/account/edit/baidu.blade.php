<input type="hidden" name="baidu_included" id="baidu_included" value="{{ !empty($account->baidu) || old('baidu_included') == 'yes' ? 'yes' : 'no' }}" />
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapse-22" aria-expanded="true" class="collapsed">
                Baidu
                </a> 
            </h4>
        </div>
        <div id="collapse-22" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Existing Baidu Account? *</label>
                            <input type="radio" id="has_baidu_account" name="has_baidu_account" value="1" {{ !empty(old('has_baidu_account')) ? (old('has_baidu_account') == '1' ? 'checked' : '') : (!empty($account->baidu) ? ($account->baidu->baidu_account ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="has_baidu_account" name="has_baidu_account" value="0" {{ !empty(old('has_baidu_account')) ? (old('has_baidu_account') == '0' ? 'checked' : '') : (!empty($account->baidu) ? (!$account->baidu->baidu_account ? 'checked' : '' ) : 'checked') }} />No
                        </div>
                    </div>
                    <div class="baidu-account {{ !empty(old('has_baidu_account')) ? (old('has_baidu_account') == 1 ? '' : 'hidden') : (!empty($account->baidu) ? ($account->baidu->baidu_account ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Baidu Account User ID *</label>
                                <input id="baidu_account" type="text" class="form-control" name="account[baidu][baidu_account]" value="{{ !empty($account->baidu) ? $account->baidu->baidu_account : old('baidu_account') }}">
                                @if ($errors->has('baidu_account'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_account') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Target Market *</label>
                            <div class="select-box">
                                @php 
                                    $baidu_countries = !empty($account->baidu) ? explode('|',$account->baidu->baidu_target_market) : '';
                                    $old_baidu_countries = !empty(old('baidu_target_market')) ? explode('|',old('baidu_target_market')) : '';     
                                @endphp
                                <select class="selectpicker form-control" id="baidu_target_market" name="account[baidu][baidu_target_market][]" data-size="10" data-live-search="true" data-actions-box="true" multiple="multiple">
                                @foreach($countries as $row)
                                <option value="{{ $row->code }}" {{ !empty(old('baidu_target_market')) && in_array($row->code,$old_baidu_countries) ? 'selected' : (!empty($account->baidu) && in_array($row->code,$baidu_countries) ? 'selected' : '')  }}>{{ $row->country }}</option>
                                @endforeach
                                </select>

                                {{-- <input type="hidden" id="baidu_target_market_null" name="account[baidu][baidu_target_market]" />
                                <input type="hidden" id="baidu_target_market_array" name="account[baidu][baidu_target_market]" value="{{ !empty(old('baidu_target_market')) ? ( is_array(old('baidu_target_market')) ? implode('|',old('baidu_target_market')) : old('baidu_target_market') ) : (!empty($account->baidu) ? $account->baidu->baidu_target_market : '' ) }}" /> --}}

                                @if ($errors->has('baidu_target_market'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_target_market') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label for="baidu_city">City *</label>
                            <input id="baidu_city" type="text" class="form-control" name="account[baidu][baidu_city]" value="{{ !empty($account->baidu) ? $account->baidu->baidu_city : old('baidu_city') }}">
                            @if ($errors->has('baidu_city'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_city') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="baidu_media_channel">Media Channels *</label>
                        <div class="select-box">
                            @php 
                                $current_channels = !empty($account->baidu) ? explode('|',$account->baidu->baidu_media_channel) : '';
                            @endphp
                            <select id="baidu_media_channel" name="account[baidu][baidu_media_channel][]" class="multiselect-ui form-control" multiple="multiple">
                                @foreach($baidu_channels as $row)
                                    <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_media_channel')) && in_array($row->systemcode,old('baidu_media_channel')) ? 'selected' : (!empty($account->baidu) && in_array($row->systemcode,$current_channels) ? 'selected' : '') }} >{{ $row->systemdesc }}</option>
                                @endforeach
                            </select>
                            {{-- <input type="hidden" id="baidu_media_channel_array" name="account[baidu][baidu_media_channel]" value="{{ !empty(old('baidu_media_channel')) ? old('baidu_media_channel') : (!empty) ?['baidu_media_channel'] : '') }}" /> --}}
                            @if ($errors->has('baidu_media_channel'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_media_channel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Brief & Expectations *</label>
                            <textarea class="form-control" id="baidu_campaign" name="account[baidu][baidu_campaign]" rows="5" >{!! !empty($account->baidu) ? $account->baidu->baidu_campaign : old('baidu_campaign') !!}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-box">
                            <label>Campaign Duration *</label>
                            <div class="select-box">
                                <select class="form-control" id="baidu_duration" name="account[baidu][baidu_duration]">
                                @foreach($duration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_duration')) && $row->systemcode == old('baidu_duration') ? 'selected' : (!empty($account->baidu) && $account->baidu->baidu_duration == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div id="baidu-duration-other" class="input-group in-other {{ !empty(old('baidu-duration_other')) || old('baidu_duration') == 'CD999' ? '' : ( !empty($account->baidu->baidu_duration_other) ? '' : 'hidden' ) }}">
                                <span class="input-group-btn">
                                <select class="btn campaign-interval" id="baidu_duration_other_picker" name="account[baidu][baidu_duration_interval]">
                                @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_duration_interval')) ? (old('baidu_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($account->baidu) ? ($account->baidu->baidu_duration_interval == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                                </span>
                                <input type="text" class="form-control" id="baidu_duration_other" name="account[baidu][baidu_duration_other]" value="{{ !empty(old('baidu_duration_other')) ? old('baidu_duration_other') : ( !empty($account->baidu->baidu_duration_other) ? $account->baidu->baidu_duration_other : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                            </div>
                            @if ($errors->has('baidu_duration'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_duration') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="has_baidu_campaign_dateset">Is the Campaign Date Set?</label>
                        <div class="form-box">
                            <input type="radio" id="has_baidu_campaign_dateset" name="has_baidu_campaign_dateset" value="1" {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == '1' ? 'checked' : '') : (!empty($account->baidu) ? (!empty($account->baidu->baidu_campaign_datefrom) && !empty($account->baidu->baidu_campaign_dateto) ? 'checked' : '' ) : '') }}/>Yes
                            <input class="clearfix" type="radio" id="has_baidu_campaign_dateset" name="has_baidu_campaign_dateset" value="0" {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == '0' ? 'checked' : '') : (!empty($account->baidu) ? (!empty($account->baidu->baidu_campaign_datefrom) && empty(!$account->baidu->baidu_campaign_dateto) ? '' : 'checked' ) : 'checked') }}/>No
                        </div>
                    </div>
                    <div class="baidu-campaign {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == 1 ? '' : 'hidden') : (!empty($account->baidu) ? ($account->baidu->baidu_campaign_datefrom && $account->baidu->baidu_campaign_dateto ? '' : 'hidden') : 'hidden') }}">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label for="baidu_campaign_datefrom">Campaign Start Date *</label>
                                <input type="text" class="form-control datepicker" id="baidu_campaign_datefrom" name="account[baidu][baidu_campaign_datefrom]" value="{{ !empty(old('baidu_campaign_datefrom')) ? old('baidu_campaign_datefrom') : (!empty($account->baidu) ? $account->baidu->baidu_campaign_datefrom : '') }}" data-date-format="yyyy-mm-dd" readonly>   
                            </div>

                            @if ($errors->has('baidu_campaign_datefrom'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_campaign_datefrom') }}</span>
                            @endif

                            <div class="form-box">
                                <label for="baidu_campaign_dateto">Campaign End Date *</label>
                                <input type="text" class="form-control datepicker" id="baidu_campaign_dateto" name="account[baidu][baidu_campaign_dateto]" value="{{ !empty(old('baidu_campaign_dateto')) ? old('baidu_campaign_dateto') : (!empty($account->baidu) ? $account->baidu->baidu_campaign_dateto : '') }}" data-date-format="yyyy-mm-dd" readonly>
                            </div>

                            @if ($errors->has('baidu_campaign_dateto'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_campaign_dateto') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="has_baidu_ad_scheduling">Ad Scheduling</label>
                        <div class="form-box">
                            <input type="radio" id="has_baidu_ad_scheduling" name="account[baidu][has_baidu_ad_scheduling]" value="1" {{ !empty(old('has_baidu_ad_scheduling')) ? (old('has_baidu_ad_scheduling') == 1 ? 'checked' : '') : ( !empty($account->baidu) ? ($account->baidu->has_baidu_ad_scheduling == 1 ? 'checked' : '') : '') }}  />Yes
                            <input type="radio" id="has_baidu_ad_scheduling" name="account[baidu][has_baidu_ad_scheduling]" value="0" {{  empty(old('has_baidu_ad_scheduling')) ? (!empty($account->baidu) ? ($account->baidu->has_baidu_ad_scheduling == 0 ? 'checked' : '') : 'checked') : ( old('has_baidu_ad_scheduling') == 0 ? 'checked' : '' ) }} />No
                        </div>
                        <div class="form-box">
                            <textarea class="form-control {{ !empty(old('has_baidu_ad_scheduling')) ? (old('has_baidu_ad_scheduling') == 1 ? '' : 'hidden') : (!empty($account->baidu) ? ($account->baidu->has_baidu_ad_scheduling == '1' ? '' : 'hidden') : 'hidden') }}" id="baidu_ad_scheduling_remark" name="account[baidu][baidu_ad_scheduling_remark]" rows="2" >{{ !empty(old('baidu_ad_scheduling_remark')) ? old('baidu_ad_scheduling_remark') : (!empty($account->baidu) ? $account->baidu->baidu_ad_scheduling_remark : '') }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="baidu_currency">Currency</label>
                        <div class="select-box">
                            <select class="form-control" id="baidu_currency" name="account[baidu][baidu_currency]">
                                <option value="USD" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'USD' ? 'selected' : '') : (!empty($account->baidu) ? ($account->baidu->baidu_currency == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                                <option value="SGD" {{ empty(old('baidu_currency')) ? 'selected' : (!empty($account->baidu) ? ($account->baidu->baidu_currency == 'SGD' ? 'selected' : '') : (old('baidu_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                                <option value="PHP" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'PHP' ? 'selected' : '') : (!empty($account->baidu) ? ($account->baidu->baidu_currency == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                                <option value="MYR" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'MYR' ? 'selected' : '') : (!empty($account->baidu) ? ($account->baidu->baidu_currency == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="baidu_budget">Total Campaign Budget *</label>
                        <div class="form-box">
                            <input type="text" class="form-control" id="baidu_budget" name="account[baidu][baidu_budget]" value="{{ !empty(old('baidu_budget')) ? old('baidu_budget') : (!empty($account->baidu) ? $account->baidu->baidu_budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('baidu_budget'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_budget') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="baidu_monthly_budget">Monthly Campaign Budget *</label>
                        <div class="form-box">
                            <input type="text" class="form-control" id="baidu_monthly_budget" name="account[baidu][baidu_monthly_budget]" value="{{ !empty(old('baidu_monthly_budget')) ? old('baidu_monthly_budget') : (!empty($account->baidu) ? $account->baidu->baidu_monthly_budget : '') }}" onkeypress="return isNumberKey(event);">
                            @if ($errors->has('baidu_monthly_budget'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_monthly_budget') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="baidu_daily_budget">Daily Campaign Budget *</label>
                        <div class="form-box">
                            <input type="text" class="form-control" id="baidu_daily_budget" name="account[baidu][baidu_daily_budget]" value="{{ !empty(old('baidu_daily_budget')) ? old('baidu_daily_budget') : (!empty($account->baidu) ? $account->baidu->baidu_daily_budget : '') }}" readonly onkeypress="return isNumberKey(event);">
                            @if ($errors->has('baidu_daily_budget'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_daily_budget') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-box">
                            <label>Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
                            @if(isset($account->baidu->baidu_hist_data))
                            @php 
                            $fname = explode('|',$account->baidu->baidu_hist_data_fname);
                            $hash = explode('|',$account->baidu->baidu_hist_data);
                            @endphp
                            <hr />
                            @foreach($fname as $n => $row)
                            <div class="row">
                                <div class="col-md-8">
                                    <p><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</p>
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ route('account.view.pdf',['baidu',$hash[$n],$row] ) }}" class="btn btn-info" target="_blank">View</a>
                                    <a href="{{ route('account.historicaldata.delete',[$account->id,'baidu',$hash[$n]] ) }}" class="btn btn-danger">Remove</a>
                                </div>
                            </div>
                            <hr />
                            @endforeach
                            @if (count($hash) < 5)
                            <div class="baidu-file-uploading">
                                <div class="file-loading">
                                    <input id="baidu_hist_data" name="baidu_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="baidu-file-uploading">
                                <div class="file-loading">
                                    <input id="baidu_hist_data" name="baidu_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                                </div>
                            </div>
                            @endif
                            @if ($errors->has('baidu_hist_data'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_hist_data') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>