
@foreach($account_hist as $history)
    <ul class="list-group">
        <li class="list-group-item">
            <div class="row toggle" id="dropdown-detail-{{ $history->id }}" data-toggle="detail-{{ $history->id }}">
                <div class="col-xs-10">
                    <p>Contract Number: {{ !empty($history->contract->contract_number) ? $history->contract->contract_number : 'No contract number' }}</p>
                    <p>Contract Status: 
                        <label for="" class="label label-{{ isset($history->contract->is_active) ? ($history->contract->is_active == 1 ? 'success' : 'danger') : 'info' }}">
                            {{ isset($history->contract->is_active) ? ($history->contract->is_active == 1 ? 'Active' : 'Past') : 'No contract' }}
                        </label></p>
                    <p>Date Created: {{ !empty($history->contract->created_at) ? $history->contract->created_at->format('d M Y h:i:s A') : $history->created_at->format('d M Y h:i:s A')  }}</p>
                </div>
                <div class="col-xs-2">
                    <i id="up-down" class="fa fa-chevron-down pull-right"></i>
                </div>
            </div>
            <div id="detail-{{ $history->id }}">
                <hr>
                <div class="container-fluid">
                    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Main Account Holder</label>
                    <span>{{ empty($history->acc_assigned) ? 'None' : $history->holder->UserInfo->f_name.' '.$history->holder->UserInfo->l_name }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Assistant Account Holder</label>
                    <span>{{ empty($history->acc_assistant) ? 'None' : $history->asst_holder->UserInfo->f_name.' '.$history->asst_holder->UserInfo->l_name }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Industry</label>
                    <span>{{ empty($history->prospect->lead->industry) ? 'Not Specified' : $history->prospect->lead->lead_industry->title }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Company</label>
                    <span>{{ !empty($history->contract->company) ? $history->contract->company : 'Not Specified' }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Contract Value</label>
                    <span>{{ !empty($history->contract->contract_value) ? (!empty($history->contract->currency) ? $history->contract->currency . ' ' : 'SGD ') . $history->contract->contract_value : 'Not Specified' }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Client Name</label>
                    <span>{{ !empty($history->contract->f_name) ? $history->contract->f_name.' '.$history->contract->l_name : 'Not Specified' }}</span>
                </div>
            </div>
         
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Client Number</label>
                    <span>{{ !empty($history->prospect->lead->contact_number) ? $history->prospect->lead->contact_number : 'Not Specified' }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Client Email</label>
                    <span>{{ $history->prospect->lead->email }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Website URL</label>
                    <span>{{ !empty($history->prospect->lead->contact_number) ? $history->prospect->lead->contact_number : 'Not Specified' }}</span>
                </div>
            </div>
          
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Website CMS/FTP Login</label>
                    <span>{{ !empty($history->prospect->lead->website) ? $history->prospect->lead->website : 'Not Specified' }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Contact Us Page URL</label>
                    <span>{{ !empty($history->additional->contact_page_url) ? $history->additional->contact_page_url : 'Not Specified' }}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Thank You Page URL</label>
                    <span>{{ !empty($history->additional->ty_page_url) ? $history->additional->ty_page_url : 'Not Specified' }}</span>
                </div>
            </div>
          
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>GA Required?</label>
                    <span>
                        {{ !empty($history->additional->ga_required) ? $history->additional->ga_required == 1 ? 'Yes' : 'No' : 'No' }}
                        {{ !empty($history->additional->ga_remark) ? ' - ' . $history->additional->ga_remark : '' }}
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Need implementations from OOm?</label>
                    <span>
                        {{ !empty($history->additional->implementations) ? $history->additional->implementations == 1 ? 'Yes' : 'No' : 'No' }}
                        {{ !empty($history->additional->implementations_remark) ? ' - ' . $history->additional->implementations_remark : '' }}
                    </span>
                </div>
            </div>
    
            <div class="col-md-4">
                <div class="form-box">
                    <label>Status</label>
                    <span>{{ !empty($history->status) ? $history->accountStatus->systemdesc : 'Not Specified' }}</span>
                </div>
            </div>
        </div>

        <details>
                <summary>Nature of Package</summary>
                <div class="panel-body">
                    @if(!empty($history->ppc_id))
                        <div class="sem-included">
                            @include('account.history.sem')
                        </div>
                    @endif
        
                    @if(!empty($history->fb_id))
                        <div class="fb-included">
                            @include('account.history.fb')
                        </div>
                    @endif
        
                    @if(!empty($history->seo_id))
                        <div class="seo-included">
                            @include('account.history.seo')
                        </div>
                    @endif
        
                    @if(!empty($history->web_id))
                        <div class="web-included">
                            @include('account.history.web')
                        </div>
                    @endif
        
                    @if(!empty($history->baidu_id))
                        <div class="baidu-included">
                            @include('account.history.baidu')
                        </div>
                    @endif
        
                    @if(!empty($history->weibo_id))
                        <div class="weibo-included">
                            @include('account.history.weibo')
                        </div>
                    @endif
        
                    @if(!empty($history->wechat_id))
                        <div class="wechat-included">
                            @include('account.history.wechat')
                        </div>
                    @endif
        
                    @if(!empty($history->blog_id))
                        <div class="blog-included">
                            @include('account.history.blog')
                        </div>
                    @endif
        
                    @if(!empty($history->social_id))
                        <div class="social-included">
                            @include('account.history.social')
                        </div>
                    @endif

                    @if(!empty($history->postpaid_sem_id))
                    <div class="postpaid-included">
                        @include('account.history.postpaid_sem')
                    </div>
                @endif
                </div>
                
            </details>

                   

                </div>
            </div>



        </li>
    </ul>
@endforeach
