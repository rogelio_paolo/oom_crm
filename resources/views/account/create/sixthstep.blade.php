@extends('layouts.master_v2')

@section('title')
  CRM - Prospect Conversion
@endsection

@section('styles')
  <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('themes/explorer/theme.min.css') }}" />
@endsection

@section('main_content')

<div class="outer-data-box">
    <div class="row">
@php
    $nature = Session::get('account_additional')['nature'];
    $baidu = Session::get('account_baidu');
@endphp

<div class="">
    <ul class="nav nav-pills nav-justified thumbnail setup-panel">
         @php $additional = Session::has('account_additional') ? Session::get('account_additional') : '' @endphp
         <li><a href="{{ route('prospect.convert.step1',$prospect->id) }}">
             <!-- <h4 class="list-group-item-heading">Step 1</h4> -->
             <p class="list-group-item-text">Confirm Prospect Information</p>
         </a></li>
         <li class="NP001 {{ !empty($additional) && in_array('NP001',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step2',$prospect->id) }}">
             <!-- <h4 class="list-group-item-heading">Step 2</h4> -->
             <p class="list-group-item-text">SEM</p>
         </a></li>
         <li class="NP002 {{ !empty($additional) && in_array('NP002',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step3',$prospect->id) }}">
             <!-- <h4 class="list-group-item-heading">Step 3</h4> -->
             <p class="list-group-item-text">Facebook</p>
         </a></li>
         <li class="NP003 {{ !empty($additional) && in_array('NP003',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step4',$prospect->id) }}">
             <!-- <h4 class="list-group-item-heading">Step 4</h4> -->
             <p class="list-group-item-text">SEO</p>
         </a></li>
        <li class="step-5 {{ !empty($additional) && in_array('NP004',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step5',$prospect->id) }}">
             <!-- <h4 class="list-group-item-heading">Step 5</h4> -->
             <p class="list-group-item-text">Web Development</p>
         </a></li>
        <li class="active step-6 {{ !empty($additional) && in_array('NP005',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step6',$prospect->id) }}">
            <p class="list-group-item-text">Baidu</p>
        </a></li>
        <li class="step-7 {{ !empty($additional) && in_array('NP006',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step7',$prospect->id) }}">
            <p class="list-group-item-text">Weibo</p>
        </a></li>
        <li class="step-8 {{ !empty($additional) && in_array('NP007',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step8',$prospect->id) }}">
            <p class="list-group-item-text">WeChat</p>
        </a></li>
        <li class="step-9 {{ !empty($additional) && in_array('NP008',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step9',$prospect->id) }}">
            <p class="list-group-item-text">Blog Content</p>
        </a></li>
        <li class="step-10 {{ !empty($additional) && in_array('NP009',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step10',$prospect->id) }}">
            <p class="list-group-item-text">Social Media Management</p>
        </a>
        <li class="step-11 {{ !empty($additional) && in_array('NP010',explode('|',$additional['nature'])) ? '' : 'hidden' }}"><a href="{{ route('prospect.convert.step11',$prospect->id) }}">
            <p class="list-group-item-text">Post Paid SEM</p>
        </a></li>
		
     </ul>
    <div class="loader-overlay hidden"></div>   
    <form method="post" action="{{ route('prospect.convert.step6', $prospect->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="main-data-box">
            <div class="data-header-box clearfix">
                <span class="header-h">Baidu</span>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                   
                    <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">
                            @include('account.create.baidu')
                            
                            <div class="clearfix" style="margin-bottom: 5%">&nbsp;</div>
                            
                            <div class="text-center">
                                @php
                                    $prevstep = in_array('NP004',explode('|',$nature)) ? 'step5' :( in_array('NP003',explode('|',$nature)) ? 'step4' : ( in_array('NP002',explode('|',$nature)) ? 'step3' :( in_array('NP001',explode('|',$nature)) ? 'step2' : 'step1' ) ) );
                                @endphp

                                <a href="{{ route('prospect.convert.'.$prevstep,$prospect) }}" class="btn btn-warning btn-md">Previous</a>
                                @if ( 
                                    in_array('NP006',explode('|',$nature)) ||
                                    in_array('NP007',explode('|',$nature)) || 
                                    in_array('NP008',explode('|',$nature)) || 
                                    in_array('NP009',explode('|',$nature)) ||
                                    in_array('NP010',explode('|',$nature)))
                                    <button type="submit" class="btn btn-primary btn-md next-step">Next</button>
                                @else
                                    <button type="submit" class="btn btn-success btn-md next-step">Convert to Account</button>
                                    <a href="{{ route('prospect.list') }}" class="btn btn-danger btn-md">Cancel</a>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                    
                </div>

            </div> 
        </div>
                                       
    </form>
</div>            
   
    </div>
</div>

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/lead/convert.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/purify.min.js" type="text/javascript"></script>
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/themes/fas/theme.min.js"></script>
    
    <script>
    $("#baidu_hist_data").fileinput({
        dropZoneEnabled: true,
        maxFilesNum: 5,
        overwriteInitial: true,
        uploadUrl: $('base').attr('href') + "/prospect/convert/{{ $prospect }}/step-6",
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        allowedFileExtensions: ['pdf','doc','docx'],
        previewFileIconSettings: {
            'doc': '<i class="fa fa-file-word-o text-primary"></i>',
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
        }
    });

    var $zone = $('.file-drop-zone.clickable');
    $zone.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('#baidu_hist_data')[0].files = e.originalEvent.dataTransfer.files;
    });
    </script>

@endsection

@endsection
