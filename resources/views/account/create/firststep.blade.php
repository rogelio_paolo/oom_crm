
@extends('layouts.master_v2')
@section('title')
CRM - Prospect Conversion
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
@endsection
@section('main_content')

<div class="outer-data-box">
    <div class="row"> 
        <div class="">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                @php 
                    $additional = Session::has('account_additional') ? Session::get('account_additional') : '';
                @endphp
                
                <li class="active">
                    <a href="{{ route('prospect.convert.step1',$prospect->id) }}">
                    <p class="list-group-item-text">Prospect Conversion</p>
                    </a>
                </li>
            </ul>
            <form method="post" action="{{ route('prospect.convert.step1', $prospect->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $prospect->id }}" />
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Prospect Information</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <div class="form-group clearfix">
                                        <label for="company_name" class="col-md-4 control-label">Company Name</label>
                                        <div class="col-md-6">
                                            <p class="text-left">{{ $prospect->lead->company }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="contract_value" class="col-md-4 control-label">Contract Value (without GST) *</label>
                                        <div class="col-md-6">   
                                                <select class="form-control" id="currency" name="currency">
                                                    <option value="USD" {{ !empty($opportunity) ? ($opportunity['currency'] == 'USD' ? 'selected' : '') : (!empty($additional) ? ($additional['currency'] == 'USD' ? 'selected' : '') : (!empty($prospect->lead->currency) ? ($prospect->lead->currency == 'USD' ? 'selected' : '') : '')) }}>US Dollar</option>
                                                    <option value="SGD" {{ !empty($opportunity) ? ($opportunity['currency'] == 'SGD' ? 'selected' : '') : (!empty($additional) ? ($additional['currency'] == 'SGD' ? 'selected' : '') : (!empty($prospect->lead->currency) ? ($prospect->lead->currency == 'SGD' ? 'selected' : '') : '')) }}>Singapore Dollar</option>
                                                    <option value="PHP" {{ !empty($opportunity) ? ($opportunity['currency'] == 'PHP' ? 'selected' : '') : (!empty($additional) ? ($additional['currency'] == 'PHP' ? 'selected' : '') : (!empty($prospect->lead->currency) ? ($prospect->lead->currency == 'PHP' ? 'selected' : '') : '')) }}>Philippine Peso</option>
                                                    <option value="MYR" {{ !empty($opportunity) ? ($opportunity['currency'] == 'MYR' ? 'selected' : '') : (!empty($additional) ? ($additional['currency'] == 'MYR' ? 'selected' : '') : (!empty($prospect->lead->currency) ? ($prospect->lead->currency == 'MYR' ? 'selected' : '') : '')) }}>Malaysian Ringgit</option>
                                                </select><br/>
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{ !empty($prospect->lead->currency) ? $prospect->lead->currency : (!empty($additional) ? $additional['currency'] : ( !empty($opportunity) ? $opportunity['currency'] : 'SGD' )) }}</span>
                                                    <input class="form-control" 
                                                        type="text" id="contract_value" 
                                                        name="contract_value" 
                                                        value="{{ !empty($opportunity) ? $opportunity['contract_value']
                                                                    : (!empty($additional) ? $additional['contract_value']  
                                                                        : (!empty($prospect->lead->contract_value) ? $prospect->lead->contract_value 
                                                                            : (!empty(old('contract_value')) ? old('contract_value') : '') ) ) }}" 
                                                        onkeypress="return isNumberKey(event);"
                                                    />
                                                </div>

                                                @if($errors->has('contract_value'))
                                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_value') }}</span>
                                                @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="contract_type" class="col-md-4 control-label">Contract Type *</label>
                                        <div class="col-md-6">
                                                <select class="form-control" id="contract_type" name="contract_type">
                                                   @foreach($contract_type as $type)
                                                        <option value="{{ $type->systemcode }}" {{ !empty($opportunity) ? 
                                                            ($opportunity['contract_type'] == $type->systemcode ? 'selected' : '') : (!empty($additional) ? 
                                                                ($additional['contract_type'] == $type->systemcode ? 'selected' : 
                                                                    ( !empty(old('contact_type')) ? (old('contract_type') == $type->systemcode ? 'selected' : '') : '' )  ) : '') }}>{{ $type->systemdesc }}</option>
                                                   @endforeach
                                                </select><br/>

                                                @if($errors->has('contract_type'))
                                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_type') }}</span>
                                                @endif
                                        </div>
                                    </div>
                                    
                                    <div class="form-group clearfix">
                                        <label for="client_name" class="col-md-4 control-label">Client First Name *</label>
                                        <div class="col-md-6">
                                            {{--  <p class="text-left">{{ !empty($prospect->lead->f_name || !empty($prospect->lead->l_name)) ? $prospect->lead->f_name.' '.$prospect->lead->l_name : 'Not Specified' }}</p>  --}}
                                            <input class="form-control" type="text" id="f_name" name="f_name" 
                                            value="{{ !empty($opportunity) ? $opportunity['client_name']
                                                     : (!empty($direct_opportunity) ? $direct_opportunity['client_name']
                                                        : (!empty($additional) ? $additional['f_name']
                                                            : (!empty($prospect->lead->f_name) ? $prospect->lead->f_name
                                                                : (!empty(old('f_name')) ? old('f_name') : '') ) ) ) }}"/>
                                        
                                            @if($errors->has('f_name'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('f_name') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="client_name" class="col-md-4 control-label">Client Last Name</label>
                                        <div class="col-md-6">
                                            {{--  <p class="text-left">{{ !empty($prospect->lead->f_name || !empty($prospect->lead->l_name)) ? $prospect->lead->f_name.' '.$prospect->lead->l_name : 'Not Specified' }}</p>  --}}
                                            <input class="form-control" type="text" id="l_name" name="l_name"
                                                   value="{{ !empty($opportunity) ? ''
                                                     : (!empty($direct_opportunity) ? ''
                                                        : (!empty($additional) ? $additional['l_name']
                                                            : (!empty($prospect->lead->l_name) ? $prospect->lead->l_name
                                                                : (!empty(old('l_name')) ? old('l_name') : '') ) ) ) }}"/>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="client_name" class="col-md-4 control-label">Client Designation</label>
                                        <div class="col-md-6">
                                            {{--  <p class="text-left">{{ !empty($prospect->lead->f_name || !empty($prospect->lead->l_name)) ? $prospect->lead->f_name.' '.$prospect->lead->l_name : 'Not Specified' }}</p>  --}}
                                            <input class="form-control" type="text" id="designation" name="designation" 
                                            value="{{ !empty($opportunity) ? $opportunity['designation']
                                                        : (!empty($direct_opportunity) ? $direct_opportunity['designation']
                                                            : (!empty($additional) ? $additional['designation']
                                                                : (!empty($prospect->lead->designation) ? $prospect->lead->designation
                                                                    : (!empty(old('designation')) ? old('designation') : '') ) ) ) }}"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group clearfix">
                                        <label for="client_number" class="col-md-4 control-label">Client Contact Number</label>
                                        <div class="col-md-6">
                                            {{--  <p class="text-left">{{ !empty($prospect->lead->contact_number) ? $prospect->lead->contact_number : 'Not specified' }}</p>  --}}
                                            <input class="form-control" type="text" id="contact_number" name="contact_number" 
                                            value="{{ !empty($opportunity) ? $opportunity['contact_number']
                                                        : (!empty($direct_opportunity) ? $direct_opportunity['mobile_number']
                                                            : (!empty($additional) ? $additional['contact_number']
                                                                : (!empty($prospect->lead->contact_number) ? $prospect->lead->contact_number
                                                                    : (!empty(old('contact_number')) ? old('contact_number') : '') ) ) ) }}" />
                                        </div>
                                    </div>
                                    
                                    {{--  <div class="form-group clearfix">
                                        <label for="client_email" class="col-md-4 control-label">Client Email Address *</label>
                                        <div class="col-md-6">
                                            <input class="form-control" type=\"email" id="email" name="email" 
                                            value="{{ !empty($opportunity) ? $opportunity['email']
                                                        : (!empty($direct_opportunity) ? $direct_opportunity['email']
                                                            : (!empty($additional) ? $additional['email']
                                                                : (!empty($prospect->lead->email) ? $prospect->lead->email
                                                                    : (!empty(old('email')) ? old('email') : '') ) ) ) }}"/>

                                            @if($errors->has('email'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>  --}}

                                    <div class="form-group clearfix">
                                        <label for="client_email" class="col-md-4 control-label">Client Email Address *</label>
                                        <div class="col-md-6">
                                            <input type="text" id="email" name="email" class="form-control"
                                                   value="{{  !empty($additional['email']) ?  json_encode(explode('|', $additional['email']))
                                                                : (!empty($prospect->lead->email) ? json_encode(explode('|', $prospect->lead->email))
                                                                    : (!empty(old('email')) ? (count(old('email') > 0) ? old('email') : '')
                                                                    : ''))  }}">

                                            @if($errors->has('email'))
                                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label for="industry" class="col-md-4 control-label">Industry</label>
                                        <div class="col-md-6">
                                            <p class="text-left">
                                                {{ !empty($direct_opportunity) ? $direct_opportunity['industry']
                                                    : (!empty($prospect->lead->industry) ? $prospect->lead->lead_industry->title : 'Not Specified')  }}
                                            </p>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group clearfix">
                                        <label for="client_email" class="col-md-4 control-label">Website URL</label>
                                        <div class="col-md-6">
                                            <p class="text-left">
                                                {{ !empty($direct_opportunity) ? $direct_opportunity['website_url']
                                                    : (!empty($prospect->lead->website) ? $prospect->lead->website : 'Not Specified') }}
                                            </p>
                                        </div>
                                    </div>
                                    
                                    @if (Session::has('account_additional'))
                                    @php
                                        $additional = Session::get('account_additional');
                                    @endphp
                                    @endif
                                    <div class="form-group clearfix">
                                        <label for="nature" class="col-md-4 control-label">Services *</label>
                                        <div class="col-md-6">
                                            
                                            <div class="select-box">
                                                <select id="nature" name="nature[]" class="multiselect-ui form-control" multiple="multiple">
                                                @foreach($nature as $row)
                                                <option value="{{ $row->systemcode }}" 
                                                    {{ !empty($opportunity_services) ? (in_array($row->systemcode, $opportunity_services) ? 'selected' : '') : 
                                                        (!empty($additional) ? (in_array($row->systemcode,explode('|',$additional['nature'])) ? 'selected' : '') : 
                                                            (!empty(old('nature')) ? (in_array($row->systemcode,explode(',',old('nature'))) ? 'selected' : '') : '') ) }}
                                                >
                                                {{ $row->systemdesc }}
                                                </option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <input type="hidden" id="nature_array" name="nature" value="{{ !empty(old('nature')) ? old('nature') : (!empty($opportunity_services) ? implode(',' ,$opportunity_services) : ( !empty($additional) ? $additional['nature'] : '' )) }}" />
                                            @if($errors->has('nature'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('nature') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="form-group clearfix">
                                        <label for="contact_page_url" class="col-md-4 control-label">Contact Us Page URL</label>
                                        <div class="col-md-6">
                                            <input id="contact_page_url" type="text" class="form-control" name="contact_page_url" 
                                            value="{{ !empty($opportunity['contact_us']) ? $opportunity['contact_us']
                                                        : (!empty($additional) ? $additional['contact_page_url']
                                                            : (!empty(old('contact_page_url') ? old('contact_page_url') : '') ) ) }}">
                                            @if($errors->has('contact_page_url'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contact_page_url') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="ty_page_url" class="col-md-4 control-label">Thank You Page URL</label>
                                        <div class="col-md-6">
                                            <input id="ty_page_url" type="text" class="form-control" name="ty_page_url" 
                                            value="{{ !empty($opportunity['thank_you']) ? $opportunity['thank_you'] 
                                                    : (!empty($additional) ? $additional['ty_page_url'] 
                                                        : (!empty(old('ty_page_url') ? old('ty_page_url') : '') ) ) }}">
                                            @if($errors->has('ty_page_url'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('ty_page_url') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="ga_required" class="col-md-4 control-label">GA Required?</label>
                                        <div class="col-md-6 text-left">
                                            <input type="radio" id="ga_required" name="ga_required" value="1" {{ !empty($opportunity) ? ($opportunity['ga_required']  == 1 ? 'checked' : ''): (!empty($additional) ? ($additional['ga_required'] == 1 ? 'checked' : '') : (!empty(old('ga_required')) ? (old('ga_required') == 1 ? 'checked' : '') : '' )) }} />Yes
                                            <input type="radio" id="ga_required" name="ga_required" value="0" {{ !empty($opportunity) ? ($opportunity['ga_required']  == 0 ? 'checked' : ''): (!empty($additional) ? ($additional['ga_required'] == 0 ? 'checked' : '') : (!empty(old('ga_required')) ? (old('ga_required') == 0 ? 'checked' : '') : 'checked')) }} />No
                                            <input type="text" class="form-control" name="ga_remark" 
                                            value="{{ !empty($opportunity['ga_required_value']) ? $opportunity['ga_required_value'] 
                                                    : (!empty($additional) ? $additional['ga_remark'] 
                                                        : (!empty(old('ga_remark') ? old('ga_remark') : '') ) ) }}" />
                                            @if($errors->has('ga_required'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('ga_required') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="implementations" class="col-md-4 control-label">Implementations Needed from OOm?</label>
                                        <div class="col-md-6 text-left">
                                            <input type="radio" id="implementations" name="implementations" value="1" {{ !empty($opportunity) ? ($opportunity['implementations']  == 1 ? 'checked' : '') : (!empty($additional) ? ($additional['implementations'] == 1 ? 'checked' : '') : (!empty(old('implementations')) ? (old('implementations') == 1 ? 'checked' : '') : '')) }}/>Yes
                                            <input type="radio" id="implementations" name="implementations" value="0" {{ !empty($opportunity) ? ($opportunity['implementations']  == 0 ? 'checked' : '') : (!empty($additional) ? ($additional['implementations'] == 0 ? 'checked' : '') : (!empty(old('implementations')) ? (old('implementations') == 0 ? 'checked' : '') : 'checked')) }}/>No
                                            <input type="text" class="form-control"  name="implementations_remark"
                                            value="{{ !empty($opportunity['implementations_value']) ? $opportunity['implementations_value'] 
                                                    : (!empty($additional) ? $additional['implementations_remark'] 
                                                        : (!empty(old('implementations_remark') ? old('implementations_remark') : '') ) ) }}" />
                                            @if($errors->has('implementations'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('implementations') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="web_login" class="col-md-4 control-label">Website CMS / FTP Login</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" id="web_login" name="web_login" rows="5">{{ !empty($opportunity['ftp_login']) ? $opportunity['ftp_login'] : (!empty($additional) ? $additional['web_login'] : (!empty(old('web_login') ? old('web_login') : '') ) ) }}</textarea>
                                            @if($errors->has('web_login'))
                                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('web_login') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                
                                    <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary btn-md next-step">Next</button>
                                        <a href="{{ strpos(url()->previous(), 'account') ? route('account.list') : route('prospect.list') }}" class="btn btn-danger btn-md">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
      
@section('scripts')
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lead/convert.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>


@if (Session::has('account_validation'))
    @include('includes.account.validation')
@endif
{{-- <!-- @if (Session::has('errors'))
    <script>
        $('div#account_create_errors').modal('show');
    </script>
    @endif -->
<!-- <script type="text/javascript" src="{{ asset('js/account-validation.js') }}"></script> --> --}}
@endsection
@endsection