<fieldset>
    <input type="hidden" name="wechat_included" id="wechat_included" value="{{ !empty($account->wechat) || old('wechat_included') == 'yes' ? 'yes' : 'no' }}" />

    <div class="form-group clearfix">
        <label for="has_wechat_type" class="col-md-4 control-label">WeChat OA Setup? *</label>
        <div class="col-md-6">
            <input type="radio" id="has_wechat_type" name="has_wechat_type" value="1" {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == '1' ? 'checked' : '') : (!empty($wechat['has_wechat_type']) ? ($wechat['has_wechat_type'] == '1' ? 'checked' : '' ) : '') }}/>Yes
            <input type="radio" id="has_wechat_type" name="has_wechat_type" value="0" {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == '0' ? 'checked' : '') : (!empty($wechat['has_wechat_type']) ? ($wechat['has_wechat_type'] == '0' ? 'checked' : '' ) : 'checked') }} />No
        </div>
    </div>
    
    <div class="wechat-account {{ !empty(old('has_wechat_type')) ? (old('has_wechat_type') == 1 ? '' : 'hidden') : (!empty($wechat) ? (!empty($wechat['has_wechat_type']) ? '' : 'hidden') : 'hidden') }}">
        <div class="form-group clearfix">
            <label for="wechat_type" class="col-md-4 control-label">Type of Account</label>
            <div class="col-md-6 text-left">
                <div class="select-box">
                    <select class="form-control" id="wechat_type" name="wechat_type">
                        <option value="serviced" {{ empty(old('wechat_type')) ? 'selected' : (!empty($wechat) ? ($wechat['wechat_type'] == 'Serviced' ? 'selected' : '') : (old('wechat_type') == 'Serviced' ? 'selected' : '') ) }}>Serviced</option>
                        <option value="subscription" {{ !empty(old('wechat_type')) ? (old('wechat_type') == 'Subscription' ? 'selected' : '') : (!empty($wechat) ? ($wechat['wechat_type'] == 'Subscription' ? 'selected' : '') : '') }}>Subscription</option>
                    </select>
                </div>

                @if ($errors->has('wechat_type'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_type') }}</span>
                @endif
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="has_wechat_menu_setup" class="col-md-4 control-label">Requires Menu Setup? *</label>
            <div class="col-md-6">
                <input type="radio" id="has_wechat_menu_setup" name="has_wechat_menu_setup" value="1" {{ !empty(old('has_wechat_menu_setup')) ? (old('has_wechat_menu_setup') == '1' ? 'checked' : '') : (!empty($wechat['has_wechat_menu_setup']) ? ($wechat['has_wechat_menu_setup'] == '1' ? 'checked' : '' ) : '') }} />Yes
                <input type="radio" id="has_wechat_menu_setup" name="has_wechat_menu_setup" value="0" {{ !empty(old('has_wechat_menu_setup')) ? (old('has_wechat_menu_setup') == '0' ? 'checked' : '') : (!empty($wechat['has_wechat_menu_setup']) ? ($wechat['has_wechat_menu_setup'] == '0' ? 'checked' : '' ) : 'checked') }} />No
            </div>

            @if ($errors->has('has_wechat_menu_setup'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('has_wechat_menu_setup') }}</span>
            @endif
        </div>
        <hr/>
    </div>

    <div class="form-group clearfix">
            <label for="has_wechat_advertising" class="col-md-4 control-label">Requires WeChat Advertising? *</label>
            <div class="col-md-6">
                <input type="radio" id="has_wechat_advertising" name="has_wechat_advertising" value="1" {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == '1' ? 'checked' : '') : (!empty($wechat['has_wechat_advertising']) ? ($wechat['has_wechat_advertising'] == '1' ? 'checked' : '' ) : '') }} />Yes
                <input type="radio" id="has_wechat_advertising" name="has_wechat_advertising" value="0" {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == '0' ? 'checked' : '') : (!empty($wechat['has_wechat_advertising']) ? ($wechat['has_wechat_advertising'] == '0' ? 'checked' : '' ) : 'checked') }}/>No
            </div>
        </div>
    
    <div class="wechat-advertising {{ !empty(old('has_wechat_advertising')) ? (old('has_wechat_advertising') == 1 ? '' : 'hidden') : (!empty($wechat) ? (!empty($wechat['has_wechat_advertising']) ? '' : 'hidden') : 'hidden') }}"> 
        <div class="form-group clearfix">
        <label for="wechat_advertising_type" class="col-md-4 control-label">Types of Advertising *</label>

        <div class="col-md-6">
            <div class="select-box">
                <select id="wechat_advertising_type" name="wechat_advertising_type[]" class="multiselect-ui form-control" multiple="multiple">
                    @foreach($advertising as $row)
                        <option value="{{ $row->systemcode }}" {{ !empty(old('wechat_advertising_type')) ? (in_array($row->systemcode,old('wechat_advertising_type')) ? 'selected' : '') : (!empty($wechat) ? (in_array($row->systemcode,explode('|',$wechat['wechat_advertising_type'])) ? 'selected' : '') : ($row->systemcode == 'WA001' ? 'selected' : '')) }} >{{ $row->systemdesc }}</option>
                    @endforeach
                </select>
                
                
            </div>

            @if ($errors->has('wechat_advertising_type'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_advertising_type') }}</span>
            @endif
        </div>

    </div>
        
        <div class="form-group clearfix">
            <label for="wechat_location" class="col-md-4 control-label">Location *</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="wechat_location" name="wechat_location" value="{{ !empty(old('wechat_location')) ? old('wechat_location') : (!empty($wechat) ? $wechat['wechat_location'] : '' ) }}" />

                @if ($errors->has('wechat_location'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_location') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="age_group" class="col-md-4 control-label">Age Group</label>

            <div class="row col-md-6">
                
                <div class="col-md-6">
                    <label for="wechat_age_from" class="label-inline">From</label>
                    <div class="select-box">
                    <select id="wechat_age_from" name="wechat_age_from">
                        @for($i = 1; $i <= 65; $i++)
                            <option value="{{ $i }}" {{ !empty(old('wechat_age_from')) ? (old('wechat_age_from') == $i ? 'selected' : '') : (!empty($wechat) && $wechat['wechat_age_from'] == $i ? 'selected' : '') }}> {{ $i }} </option>
                        @endfor
                    </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="wechat_age_to" class="label-inline">To</label>
                    <div class="select-box">
                    <select id="wechat_age_to" name="wechat_age_to">
                        @for($i = 1; $i <= 65; $i++)
                            <option value="{{ $i }}" {{ !empty(old('wechat_age_to')) ? (old('wechat_age_to') == $i ? 'selected' : '') : (!empty($wechat) && $wechat['wechat_age_to'] == $i ? 'selected' : '') }}> {{ $i == 65 ? '65+' : $i }} </option>
                        @endfor
                    </select>
                    </div>
                </div>

                <div class="clearfix">&nbsp;</div>

            </div>
        </div>

        <div class="form-group clearfix">
            <label for="wechat_marital" class="col-md-4 control-label">Marital Status</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="wechat_marital" name="wechat_marital" value="{{ !empty(old('wechat_marital')) ? old('wechat_marital') : (!empty($wechat) ? $wechat['wechat_marital'] : '' ) }}" />
            </div>
        </div>
        
        <div class="form-group clearfix">
            <label for="wechat_gender" class="col-md-4 control-label">Gender</label>
            <div class="col-md-6">
                <div class="select-box">
                    <select class="form-control" id="wechat_gender" name="wechat_gender">
                        <option value="A" {{ empty(old('wechat_gender')) ? (!empty($wechat) ? ($wechat['wechat_gender'] == 'A' ? 'selected' : '') : 'selected') : '' }}>All</option>
                        <option value="M" {{ !empty(old('wechat_gender')) ? ( old('wechat_gender') == 'M' ? 'selected' : '') : ( !empty($wechat) ? ($wechat['wechat_gender'] == 'M' ? 'selected' : '') : '' ) }}>Male</option>
                        <option value="F" {{ !empty(old('wechat_gender')) ? ( old('wechat_gender') == 'F' ? 'selected' : '') : ( !empty($wechat) ? ($wechat['wechat_gender'] == 'F' ? 'selected' : '') : '' ) }}>Female</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="wechat_handphone" class="col-md-4 control-label">Handphone Device</label>
            <div class="col-md-6">
                <div class="select-box">
                    <select class="form-control" id="wechat_handphone" name="wechat_handphone">
                        <option value="android" {{ empty(old('wechat_handphone')) ? (!empty($wechat) ? ($wechat['wechat_handphone'] == 'android' ? 'selected' : '') : 'selected') : '' }}>Android</option>
                        <option value="ios"     {{!empty(old('wechat_handphone')) ? ( old('wechat_handphone') == 'ios' ? 'selected' : '') : ( !empty($wechat) ? ($wechat['wechat_handphone'] == 'ios' ? 'selected' : '') : '' ) }}>iOS</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
                <label for="wechat_education" class="col-md-4 control-label">Education</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="wechat_education" name="wechat_education" value="{{ !empty(old('wechat_education')) ? old('wechat_education') : (!empty($wechat) ? $wechat['wechat_education'] : '' ) }}" />
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="wechat_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
            <div class="col-md-6 text-left">
                <input type="radio" id="has_wechat_hisdata" name="has_wechat_hisdata" value="1" {{ !empty(old('has_wechat_hisdata')) && old('has_wechat_hisdata') == '1' ? 'checked' : (!empty($wechat) && !empty($wechat['has_wechat_hisdata']) ? 'checked' : '') }}/>Yes
                <input type="radio" id="has_wechat_hisdata" name="has_wechat_hisdata" value="0" {{ !empty(old('has_wechat_hisdata')) ? (old('has_wechat_hisdata') == '0' ? 'checked' : '' ) : (!empty($wechat) ? (empty($wechat['has_wechat_hisdata']) ? 'checked' : '') : 'checked') }} />No
                <br>
            </div>
            <div class="col-md-12">
                <div class="wechat-file-uploading {{ !empty(old('has_wechat_hisdata')) ? (old('has_wechat_hisdata') == '0' ? 'hidden' : '' ) : (empty($wechat['wechat_hist_data']) ? 'hidden' : '') }}">
                    <div class="file-loading">
                        <input id="wechat_hist_data" class="hist_data" name="wechat_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                    </div>
    
                    @if ($errors->has('wechat_hist_data'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('wechat_hist_data') }}</span>
                    @endif
                </div>
            </div>
        </div>

   </fieldset>