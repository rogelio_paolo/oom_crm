<fieldset>

        <div class="form-group clearfix">
            <label for="target_market" class="col-md-4 control-label">Target Market *</label>
     
            <div class="col-md-6">
                <div class="select-box">
                    <select class="selectpicker form-control" id="target_market" name="target_market[]" data-live-search="true" data-actions-box="true" multiple="multiple">
                       @foreach($countries as $row)
                           @if (empty($sem) && empty(old('target_market')) && $row->code == 'SG')
                               <option value="{{ $row->code }}" selected>{{ $row->country }}</option>
                           @else
                               <option value="{{ $row->code }}" {{ !empty(old('target_market')) ? (in_array($row->code,old('target_market')) ? 'selected' : '') : (!empty($sem) ? (in_array($row->code,explode('|',$sem['target_market'])) ? 'selected' : '') : '') }}>{{ $row->country }}</option>
                           @endif
                       @endforeach
                    </select>
                </div>
     
                @if ($errors->has('target_market'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('target_market') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
             <label for="list_account" class="col-md-4 control-label">List Account as *</label>
      
             <div class="col-md-6">
                 <div class="select-box">
                     <select class="selectpicker form-control" name="list_account" data-live-search="true">
                        @foreach($listings as $row)
                            <option value="{{ $row->systemcode }}" {{ !empty(old('list_account')) ? ($row->systemcode == old('list_account') ? 'selected' : '') : (!empty($sem) ? ($row->systemcode == $sem['list_account'] ? 'selected' : '') : '') }}>{{ $row->systemdesc }}</option>
                        @endforeach
                     </select>
                 </div>
      
                 @if ($errors->has('list_account'))
                     <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('list_account') }}</span>
                 @endif
      
             </div>
         </div>
     
        <div class="form-group clearfix">
            <label for="payment_method" class="col-md-4 control-label">Media Budget Payment *</label>
     
            <div class="col-md-6">
                <div class="select-box">
                    <select class="selectpicker form-control" name="payment_method" data-live-search="true">
                       @foreach($payment as $row)
                           <option value="{{ $row->systemcode }}" {{ !empty(old('payment_method')) ? ($row->systemcode == old('payment_method') ? 'selected' : '') : (!empty($sem) ? ($row->systemcode == $sem['payment_method'] ? 'selected' : '') : '') }}>{{ $row->systemdesc }}</option>
                       @endforeach
                    </select>
                </div>
     
                @if ($errors->has('payment_method'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('payment_method') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="media_channel" class="col-md-4 control-label">Media Channels *</label>
     
            <div class="col-md-6">
                <div class="select-box">
                    <select id="media_channel" name="media_channel[]" class="multiselect-ui form-control" multiple="multiple">
                       @foreach($channels as $row)
                           <option value="{{ $row->systemcode }}" {{ !empty(old('media_channel')) ? (in_array($row->systemcode,old('media_channel')) ? 'selected' : '') : (!empty($sem) ? (in_array($row->systemcode,explode('|',$sem['media_channel'])) ? 'selected' : '') : ($row->systemcode == 'MC001' ? 'selected' : '')) }} >{{ $row->systemdesc }}</option>
                       @endforeach
                    </select>
                </div>
                

                @if ($errors->has('media_channel'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('media_channel') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="campaign" class="col-md-4 control-label">Campaign Brief & Expectations *</label>
     
            <div class="col-md-6">
                <textarea class="form-control" id="campaign" name="campaign" rows="5">{!! !empty(old('campaign')) ? old('campaign') : (!empty($sem) ? $sem['campaign'] : '')  !!}</textarea>
     
                @if ($errors->has('campaign'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
             <label for="duration" class="col-md-4 control-label">Campaign Duration *</label>
      
             <div class="col-md-6">
                 <div class="select-box">
                   <select class="selectpicker form-control" id="duration" name="duration" data-live-search="true">
                      @foreach($duration as $row)
                          <option value="{{ $row->systemcode }}" {{ !empty(old('duration')) ? (old('duration') == $row->systemcode ? 'selected' : '') : ( !empty($sem) ? ($sem['duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                      @endforeach
                   </select>
                 </div>
      
                  <div id="duration-other" class="input-group in-other {{ !empty(old('duration_other')) || old('duration') == 'CD999' ? '' : ( !empty($sem['duration_other']) ? '' : 'hidden' ) }}">
                      <span class="input-group-btn">
                          <select class="btn campaign-interval" id="duration_other_picker" name="duration_interval">
                              @foreach($otherduration as $row)
                                  <option value="{{ $row->systemcode }}" {{ !empty(old('duration_interval')) ? (old('duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($sem) ? ($sem['duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                              @endforeach
                          </select>
                      </span>
                      <input type="text" class="form-control" id="duration_other" name="duration_other" value="{{ !empty(old('duration_other')) ? old('duration_other') : ( !empty($sem['duration_other']) ? $sem['duration_other'] : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                  </div>
                  @if ($errors->has('duration'))
                      <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('duration') }}</span>
                  @endif
      
                  @if ($errors->has('duration_other'))
                      <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('duration_other') }}</span>
                  @endif
      
             </div>
         </div>
     
        <div class="form-group clearfix">
             <label for="campaign_date_set" class="col-md-4 control-label">is the campaign date set?</label>
     
             <div class="col-md-6">
                 <input type="radio" id="campaign_date_set" name="campaign_date_set" value="1" {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == '1' ? 'checked' : '') : (!empty($sem['campaign_date_set']) ? ($sem['campaign_date_set'] == '1' ? 'checked' : '' ) : '') }}/>Yes
                 <input class="clearfix" type="radio" id="campaign_date_set" name="campaign_date_set" value="0" {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == '0' ? 'checked' : '') : (!empty($sem['campaign_date_set']) ? ($sem['campaign_date_set'] == '0' ? 'checked' : '' ) : 'checked') }}/>No
             </div>
         </div>
     <div class="sem-campaign {{ !empty(old('campaign_date_set')) ? (old('campaign_date_set') == 1 ? '' : 'hidden') : (!empty($sem['campaign_date_set']) ? ($sem['campaign_date_set'] == 1 ? '' : 'hidden') : 'hidden') }}">
             <div class="form-group clearfix">
                 <label for="campaign_datefrom" class="col-md-4 control-label">Campaign Start Date *</label>
     
                 <div class="col-md-6">
                     {{-- <!-- <input id="campaign_datefrom" type="date" class="form-control" name="campaign_datefrom" value="{{ old('campaign_datefrom') }}"> --> --}}
                     <input type="text" class="form-control datepicker" id="campaign_datefrom" name="campaign_datefrom" value="{{ !empty(old('campaign_datefrom')) ? old('campaign_datefrom') : ( !empty($sem['campaign_datefrom']) ? $sem['campaign_datefrom'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
     
                     @if ($errors->has('campaign_datefrom'))
                         <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign_datefrom') }}</span> 
                     @endif
     
                 </div>
             </div>
     
             <div class="form-group clearfix">
                 <label for="campaign_dateto" class="col-md-4 control-label">Campaign End Date *</label>
     
                 <div class="col-md-6">
                     {{-- <!-- <input id="campaign_dateto" type="date" class="form-control" name="campaign_dateto" value="{{ old('campaign_dateto') }}"> --> --}}
                     <input type="text" class="form-control datepicker" id="campaign_dateto" name="campaign_dateto" value="{{ !empty(old('campaign_dateto')) ? old('campaign_dateto') : ( !empty($sem['campaign_dateto']) ? $sem['campaign_dateto'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
     
                     @if ($errors->has('campaign_dateto'))
                         <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('campaign_dateto') }}</span>
                     @endif
                 </div>
             </div>
        </div>
     
        <div class="form-group clearfix">
             <label for="ad_scheduling" class="col-md-4 control-label">Ad Scheduling</label>
     
             <div class="col-md-6">
                 <div>
                 <input type="radio" id="ad_scheduling" name="ad_scheduling" value="1" {{ !empty(old('ad_scheduling')) ? (old('ad_scheduling') == 1 ? 'checked' : '') : ( !empty($sem) ? ($sem['ad_scheduling'] == 1 ? 'checked' : '') : '' ) }}/>Yes
                 <input class="clearfix" type="radio" id="ad_scheduling" name="ad_scheduling" value="0" {{ !empty(old('ad_scheduling')) ? (old('ad_scheduling') == 0 ? 'checked' : '') : 'checked' }}/>No
                 </div>
                 <textarea class="form-control {{  !empty(old('ad_scheduling')) ? (old('ad_scheduling') == 1 ? '' : 'hidden') : ( !empty($sem) ? ($sem['ad_scheduling'] == 1 ? '' : 'hidden') : 'hidden' ) }}" id="ad_scheduling_remark" name="ad_scheduling_remark" rows="5"></textarea>
             </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="currency" class="col-md-4 control-label">Currency</label>
     
            <div class="col-md-6 text-left">
                <div class="select-box">
                    <select class="form-control" id="currency" name="currency">
                        <option value="USD" {{ !empty(old('currency')) ? (old('currency') == 'USD' ? 'selected' : '') : (!empty($sem) ? ($sem['currency'] == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                        <option value="SGD" {{ empty(old('currency')) ? 'selected' : (!empty($sem) ? ($sem['currency'] == 'SGD' ? 'selected' : '') : (old('currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                        <option value="PHP" {{ !empty(old('currency')) ? (old('currency') == 'PHP' ? 'selected' : '') : (!empty($sem) ? ($sem['currency'] == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                        <option value="MYR" {{ !empty(old('currency')) ? (old('currency') == 'MYR' ? 'selected' : '') : (!empty($sem) ? ($sem['currency'] == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                    </select>
                </div>
            </div>
     
        </div>
     
        <div class="form-group clearfix">
            <label for="budget" class="col-md-4 control-label">Total Campaign Budget *</label>
     
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency"></span></div> 
                    <div class="col-md-10">
                        <input id="budget" type="text" class="form-control" name="budget" value="{{ !empty(old('budget')) ? old('budget') : (!empty($sem) ? $sem['budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                    </div>
                </div>
     
                @if ($errors->has('budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('budget') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget *</label>
     
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency"></span></div>
                    <div class="col-md-10">
                        <input id="google_monthly_budget" type="text" class="form-control" name="google_monthly_budget" value="{{ !empty(old('google_monthly_budget')) ? old('google_monthly_budget') : (!empty($sem) ? $sem['google_monthly_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                    </div>
                </div>
     
                @if ($errors->has('google_monthly_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('google_monthly_budget') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_daily_budget" class="col-md-4 control-label">Daily Campaign Budget *</label>
     
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency"></span></div>
                    <div class="col-md-10">
                        <input id="google_daily_budget" type="text" class="form-control" name="google_daily_budget" value="{{ !empty(old('google_daily_budget')) ? old('google_daily_budget') : (!empty($sem) ? $sem['google_daily_budget'] : '' ) }}" readonly>
                    </div>
                </div>
     
                @if ($errors->has('google_daily_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('google_daily_budget') }}</span>
                @endif
     
            </div>
        </div>
     
        <div class="form-group clearfix">
             <label for="am_fee" class="col-md-4 control-label">Account Management Fee *</label>
      
             <div class="col-md-6 text-left">
                 <div class="row">
                     <div class="col-md-2"><span id="campaign-currency"></span></div> 
                     <div class="col-md-10">
                         <input id="am_fee" type="text" class="form-control" name="am_fee" value="{{ !empty(old('am_fee')) ? old('am_fee') : (!empty($sem) ? $sem['am_fee'] : '' ) }}" onkeypress="return isNumberKey(event);">
                     </div>
                 </div>
      
                 @if ($errors->has('am_fee'))
                     <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('am_fee') }}</span>
                 @endif
      
             </div>
         </div>
     
        
     
        <div class="form-group clearfix">
            <label for="historical_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
     
            <div class="col-md-6 text-left">
              <input type="radio" id="has_historical_data" name="has_historical_data" value="1" {{ old('has_historical_data') == 1 ? 'checked' : '' }}/>Yes
              <input type="radio" id="has_historical_data" name="has_historical_data" value="0" {{ empty(old('has_historical_data')) ? 'checked' : (old('has_historical_data') == 0 ? 'checked' : '') }}/>No
            </div>
            <div class="col-md-12">
              <div class="file-uploading {{ old('has_historical_data') == 1 ? '' : 'hidden' }}">
                   
                   <div class="file-loading">
                       <input id="historical_data" name="historical_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                   </div>
     
                   @if($errors->has('historical_data'))
                       <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('historical_data') }}</span>
                   @endif
                   
              </div>
     
            </div>
        </div>
     
     </fieldset>
     