<fieldset>
        <input type="hidden" name="weibo_included" id="weibo_included" value="{{ !empty($account->weibo) || old('weibo_included') == 'yes' ? 'yes' : 'no' }}" />
    
         <div class="form-group clearfix">
            <label for="weibo_location" class="col-md-4 control-label">Location *</label>
            <div class="col-md-6 text-left">
                <input type="text" class="form-control" id="weibo_location" name="weibo_location" value="{{ !empty(old('weibo_location')) ? old('weibo_location') : (!empty($weibo) ? $weibo['weibo_location'] : '' ) }}" />
            
                @if ($errors->has('weibo_location'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_location') }}</span>
                @endif
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="has_weibo_account" class="col-md-4 control-label">Existing Weibo Account? *</label>
            <div class="col-md-6 text-left">
                <input type="radio" id="has_weibo_account" name="has_weibo_account" value="1" {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == '1' ? 'checked' : '') : (!empty($weibo['has_weibo_account']) ? ($weibo['has_weibo_account'] == '1' ? 'checked' : '' ) : '') }}/>Yes
                <input type="radio" id="has_weibo_account" name="has_weibo_account" value="0" {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == '0' ? 'checked' : '') : (!empty($weibo['has_weibo_account']) ? ($weibo['has_weibo_account'] == '0' ? 'checked' : '' ) : 'checked') }} />No
            </div>
        </div>
    
        <div class="weibo-account {{ !empty(old('has_weibo_account')) ? (old('has_weibo_account') == 1 ? '' : 'hidden') : (!empty($weibo) ? (!empty($weibo['has_weibo_account']) ? '' : 'hidden') : 'hidden') }}">
            <div class="form-group clearfix">
                <label for="weibo_account" class="col-md-4 control-label">Weibo Account User ID *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="weibo_account" name="weibo_account" value="{{ !empty(old('weibo_account')) ? old('weibo_account') : (!empty($weibo) ? $weibo['weibo_account'] : '' ) }}" />
                
                    @if ($errors->has('weibo_account'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_account') }}</span>
                    @endif
                </div>
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="has_blue_v_weibo" class="col-md-4 control-label">Requires New Blue V Weibo Setup? *</label>
            <div class="col-md-6">
                <input type="radio" id="has_blue_v_weibo" name="has_blue_v_weibo" value="1" {{ !empty(old('has_blue_v_weibo')) ? (old('has_blue_v_weibo') == '1' ? 'checked' : '') : (!empty($weibo['has_blue_v_weibo']) ? ($weibo['has_blue_v_weibo'] == '1' ? 'checked' : '' ) : '') }}/>Yes
                <input type="radio" id="has_blue_v_weibo" name="has_blue_v_weibo" value="0" {{ !empty(old('has_blue_v_weibo')) ? (old('has_blue_v_weibo') == '0' ? 'checked' : '') : (!empty($weibo['has_blue_v_weibo']) ? ($weibo['has_blue_v_weibo'] == '0' ? 'checked' : '' ) : 'checked') }} />No
            </div>
    
            @if ($errors->has('has_blue_v_weibo'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('has_blue_v_weibo') }}</span>
            @endif
        </div>
    
        <div class="form-group clearfix">
           <label for="weibo_campaign" class="col-md-4 control-label">Campaign Brief & Expectations</label>
    
           <div class="col-md-6">
               <textarea class="form-control" id="weibo_campaign" name="weibo_campaign" rows="5">{!! !empty(old('weibo_campaign')) ? old('weibo_campaign') : (!empty($weibo) ? $weibo['weibo_campaign'] : '' ) !!}</textarea>
    
               @if ($errors->has('weibo_campaign'))
                   <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign') }}</span>
               @endif
    
           </div>
       </div>
    
       <div class="form-group clearfix">
            <label for="weibo_duration" class="col-md-4 control-label">Campaign Duration</label>
    
            <div class="col-md-6">
                <div class="select-box">
                  <select class="selectpicker form-control" id="weibo_duration" name="weibo_duration" data-live-search="true">
                     @foreach($duration as $row)
                         <option value="{{ $row->systemcode }}" {{ !empty(old('weibo_duration')) ? (old('weibo_duration') == $row->systemcode ? 'selected' : '') : ( !empty($weibo) ? ($weibo['weibo_duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                     @endforeach
                  </select>
                </div>
    
                <div id="weibo-duration-other" class="input-group in-other {{ !empty(old('weibo_duration_other')) || old('weibo_duration') == 'CD999' ? '' : ( !empty($weibo['weibo_duration_other']) ? '' : 'hidden' ) }}">
                    <span class="input-group-btn">
                        <select class="btn campaign-interval" id="weibo_duration_other_picker" name="weibo_duration_interval">
                            @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('weibo_duration_interval')) ? (old('weibo_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($weibo) ? ($weibo['weibo_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                            @endforeach
                        </select>
                    </span>
                    <input type="text" class="form-control" id="weibo_duration_other" name="weibo_duration_other" value="{{ !empty(old('weibo_duration_other')) ? old('weibo_duration_other') : ( !empty($weibo['weibo_duration_other']) ? $weibo['weibo_duration_other'] : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                </div>
    
                    @if ($errors->has('weibo_duration'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_duration') }}</span>
                    @endif
    
                    @if ($errors->has('weibo_duration_other'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_duration_other') }}</span>
                    @endif
                </div>
            </div>
    
        <div class="form-group clearfix">
            <label for="has_weibo_campaign_dateset" class="col-md-4 control-label">Is the campaign date set? </label>
    
            <div class="col-md-6 text-left">
                <input type="radio" id="has_weibo_campaign_dateset" name="has_weibo_campaign_dateset" value="1" {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == '1' ? 'checked' : '') : (!empty($weibo['has_weibo_campaign_dateset']) ? ($weibo['has_weibo_campaign_dateset'] == '1' ? 'checked' : '' ) : '') }} />Yes
                <input type="radio" id="has_weibo_campaign_dateset" name="has_weibo_campaign_dateset" value="0" {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == '0' ? 'checked' : '') : (!empty($weibo['has_weibo_campaign_dateset']) ? ($weibo['has_weibo_campaign_dateset'] == '0' ? 'checked' : '' ) : 'checked') }} />No
                <br>
            </div>
        </div>
    
        <div class="weibo-campaign {{ !empty(old('has_weibo_campaign_dateset')) ? (old('has_weibo_campaign_dateset') == 1 ? '' : 'hidden') : (!empty($weibo) ? (!empty($weibo['has_weibo_campaign_dateset']) || !empty($weibo['has_weibo_campaign_dateset']) ? '' : 'hidden') : 'hidden') }}">
            <div class="form-group clearfix">
                <label for="weibo_campaign_datefrom" class="col-md-4 control-label"> Campaign Start Date *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control datepicker" id="weibo_campaign_datefrom" name="weibo_campaign_datefrom" value="{{ !empty(old('weibo_campaign_datefrom')) ? old('weibo_campaign_datefrom') : ( !empty($weibo['weibo_campaign_datefrom']) ? $weibo['weibo_campaign_datefrom'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
                
                    @if ($errors->has('weibo_campaign_datefrom'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign_datefrom') }}</span> 
                    @endif
                </div>
            </div>
        
            <div class="form-group clearfix">
                <label for="weibo_campaign_dateto" class="col-md-4 control-label"> Campaign End Date *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control datepicker" id="weibo_campaign_dateto" name="weibo_campaign_dateto" value="{{ !empty(old('weibo_campaign_dateto')) ? old('weibo_campaign_dateto') : ( !empty($weibo['weibo_campaign_dateto']) ? $weibo['weibo_campaign_dateto'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
                
                    @if ($errors->has('weibo_campaign_dateto'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_campaign_dateto') }}</span> 
                    @endif
                </div>
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="weibo_ad_scheduling" class="col-md-4 control-label">Ad Scheduling</label>
    
            <div class="col-md-6">
                <div>
                <input type="radio" id="weibo_ad_scheduling" name="weibo_ad_scheduling" value="1" {{ !empty(old('weibo_ad_scheduling')) ? (old('weibo_ad_scheduling') == 1 ? 'checked' : '') : ( !empty($weibo) ? ($weibo['weibo_ad_scheduling'] == 1 ? 'checked' : '') : '' ) }}/>Yes
                <input class="clearfix" type="radio" id="weibo_ad_scheduling" name="weibo_ad_scheduling" value="0" {{ !empty(old('weibo_ad_scheduling')) ? (old('weibo_ad_scheduling') == 0 ? 'checked' : '') : 'checked' }}/>No
                </div>
                <textarea class="form-control {{  !empty(old('weibo_ad_scheduling')) ? (old('weibo_ad_scheduling') == 1 ? '' : 'hidden') : ( !empty($weibo) ? ($weibo['weibo_ad_scheduling'] == 1 ? '' : 'hidden') : 'hidden' ) }}" id="weibo_ad_scheduling_remark" name="weibo_ad_scheduling_remark" rows="5"></textarea>
            </div>
       </div>
    
        <div class="form-group clearfix">
           <label for="weibo_currency" class="col-md-4 control-label">Currency</label>
           <div class="col-md-6 text-left">
               <div class="select-box">
                   <select class="form-control" id="weibo_currency" name="weibo_currency">
                       <option value="USD" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'USD' ? 'selected' : '') : (!empty($weibo) ? ($weibo['weibo_currency'] == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                       <option value="SGD" {{ empty(old('weibo_currency')) ? 'selected' : (!empty($weibo) ? ($weibo['weibo_currency'] == 'SGD' ? 'selected' : '') : (old('weibo_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                       <option value="PHP" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'PHP' ? 'selected' : '') : (!empty($weibo) ? ($weibo['weibo_currency'] == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                       <option value="MYR" {{ !empty(old('weibo_currency')) ? (old('weibo_currency') == 'MYR' ? 'selected' : '') : (!empty($weibo) ? ($weibo['weibo_currency'] == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                   </select>
               </div>
           </div>
       </div>
    
        <div class="form-group clearfix">
           <label for="weibo_budget" class="col-md-4 control-label">Total Campaign Budget</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="weibo-campaign-currency"></span></div> 
                   <div class="col-md-10">
                       <input id="weibo_budget" type="text" class="form-control" name="weibo_budget" value="{{ !empty(old('weibo_budget')) ? old('weibo_budget') : (!empty($weibo) ? $weibo['weibo_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                        
                       @if ($errors->has('weibo_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
       <div class="form-group clearfix">
           <label for="weibo_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="weibo-campaign-currency"></span></div>
                   <div class="col-md-10">
                       <input id="weibo_monthly_budget" type="text" class="form-control" name="weibo_monthly_budget" value="{{ !empty(old('weibo_monthly_budget')) ? old('weibo_monthly_budget') : (!empty($weibo) ? $weibo['weibo_monthly_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                   
                        @if ($errors->has('weibo_monthly_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_monthly_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
       <div class="form-group clearfix">
           <label for="weibo_daily_budget" class="col-md-4 control-label">Daily Campaign Budget</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="weibo-campaign-currency"></span></div>
                   <div class="col-md-10">
                       <input id="weibo_daily_budget" type="text" class="form-control" name="weibo_daily_budget" value="{{ !empty(old('weibo_daily_budget')) ? old('weibo_daily_budget') : (!empty($weibo) ? $weibo['weibo_daily_budget'] : '' ) }}" readonly onkeypress="return isNumberKey(event);">
                   
                        @if ($errors->has('weibo_daily_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_daily_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
        <div class="form-group clearfix">
            <label for="weibo_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
            <div class="col-md-6 text-left">
                <input type="radio" id="has_weibo_hisdata" name="has_weibo_hisdata" value="1" {{ !empty(old('has_weibo_hisdata')) && old('has_weibo_hisdata') == '1' ? 'checked' : (!empty($weibo) && !empty($weibo['has_weibo_hisdata']) ? 'checked' : '') }}/>Yes
                <input type="radio" id="has_weibo_hisdata" name="has_weibo_hisdata" value="0" {{ !empty(old('has_weibo_hisdata')) ? (old('has_weibo_hisdata') == '0' ? 'checked' : '' ) : (!empty($weibo) ? (empty($weibo['has_weibo_hisdata']) ? 'checked' : '') : 'checked') }} />No
                <br>
            </div>
            <div class="col-md-12">
                <div class="weibo-file-uploading {{ !empty(old('has_weibo_hisdata')) ? (old('has_weibo_hisdata') == '0' ? 'hidden' : '' ) : (empty($weibo['weibo_hist_data']) ? 'hidden' : '') }}">
                    <div class="file-loading">
                        <input id="weibo_hist_data" class="hist_data" name="weibo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..."  />
                    </div>
    
                    @if ($errors->has('weibo_hist_data'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('weibo_hist_data') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </fieldset>