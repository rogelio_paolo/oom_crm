<fieldset>

        <div class="form-group clearfix">
            <label for="fb_target_country" class="col-md-4 control-label">Target Country *</label>
   
            <div class="col-md-6">
                <div class="select-box">
                    <select class="selectpicker form-control" id="fb_target_country" name="fb_target_country[]" data-live-search="true" data-actions-box="true" multiple="multiple">
                       @foreach($countries as $row)
                           @if (empty($fb) && empty(old('fb_target_country')) && $row->code == 'SG')
                               <option value="{{ $row->code }}" selected>{{ $row->country }}</option>
                           @else
                               <option value="{{ $row->code }}" {{ !empty(old('fb_target_country')) ? (in_array($row->code,old('fb_target_country')) ? 'selected' : '') : ( !empty($fb) ? ( in_array($row->code,explode('|',$fb['fb_target_country'])) ? 'selected' : '' ) : '' ) }}>{{ $row->country }}</option>
                           @endif
                       @endforeach
                    </select>
                </div>
   
                @if ($errors->has('fb_target_country'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_target_country') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
               <label for="list_account" class="col-md-4 control-label">List Account as *</label>
        
               <div class="col-md-6">
                   <div class="select-box">
                       <select class="selectpicker form-control" name="fb_list_account" data-live-search="true">
                          @foreach($listings as $row)
                              <option value="{{ $row->systemcode }}" {{ !empty(old('fb_list_account')) ? ($row->systemcode == old('fb_list_account') ? 'selected' : '') : (!empty($fb) ? ($row->systemcode == $fb['fb_list_account'] ? 'selected' : '') : '') }}>{{ $row->systemdesc }}</option>
                          @endforeach
                       </select>
                   </div>
        
                   @if ($errors->has('fb_list_account'))
                       <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_list_account') }}</span>
                   @endif
        
               </div>
           </div>
   
        <div class="form-group clearfix">
            <label for="fb_payment" class="col-md-4 control-label">Media Budget Payment *</label>
   
            <div class="col-md-6">
                <div class="select-box">
                    <select class="selectpicker form-control" name="fb_payment" data-live-search="true">
                       @foreach($payment as $row)
                           <option value="{{ $row->systemcode }}" {{ !empty(old('fb_payment')) ? (old('fb_payment') == $row->code ? 'selected' : '') : ( !empty($fb) ? ( $fb['fb_payment'] == $row->code ? 'selected' : '' ) : '' ) }}>{{ $row->systemdesc }}</option>
                       @endforeach
                    </select>
                </div>
   
                @if ($errors->has('fb_payment'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_payment') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="fb_media_channel" class="col-md-4 control-label">Media Channel *</label>
   
            <div class="col-md-6">
                <select id="fb_media_channel" name="fb_media_channel[]" class="multiselect-ui form-control" multiple="multiple">
                   @foreach($fb_channels as $row)
                       <option value="{{ $row->systemcode }}" {{ !empty(old('fb_media_channel')) ? (in_array($row->systemcode,old('fb_media_channel')) ? 'selected' : '') : (!empty($fb) ? (in_array($row->systemcode,explode('|',$fb['fb_media_channel'])) ? 'selected' : '') : ($row->systemcode == 'FM001' ? 'selected' : '') ) }} >{{ $row->systemdesc }}</option>
                   @endforeach
                </select>
                
                @if ($errors->has('fb_media_channel'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_media_channel') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="fb_campaign" class="col-md-4 control-label">Campaign Brief & Expectation *</label>
   
            <div class="col-md-6">
                <textarea class="form-control" id="fb_campaign" name="fb_campaign" rows="5">{!! !empty(old('fb_campaign')) ? old('fb_campaign') : (!empty($fb) ? $fb['fb_campaign'] : '' ) !!}</textarea>
   
                @if ($errors->has('fb_campaign'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="fb_duration" class="col-md-4 control-label">Campaign Duration *</label>
   
            <div class="col-md-6">
                <div class="select-box">
                  <select class="selectpicker form-control" id="fb_duration" name="fb_duration" data-live-search="true">
                     @foreach($duration as $row)
                         <option value="{{ $row->systemcode }}" {{ !empty(old('fb_duration')) ? (old('fb_duration') == $row->systemcode ? 'selected' : '') : ( !empty($fb) ? ( $fb['fb_duration'] == $row->systemcode ? 'selected' : '' ) : '' ) }} >{{ $row->systemdesc }}</option>
                     @endforeach
                  </select>
                </div>
                
               <div id="fb-duration-other" class="input-group in-other {{ !empty(old('fb_duration_other')) || old('fb_duration') == 'CD999' ? '' : ( !empty($fb['fb_duration_other']) ? '' : 'hidden' ) }}">
                   <span class="input-group-btn">
                       <select class="btn campaign-interval" id="fb_other_picker" name="fb_duration_interval">
                           @foreach($otherduration as $row)
                               <option value="{{ $row->systemcode }}" {{ !empty(old('fb_duration_interval')) ? (old('fb_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($fb) ? ($fb['fb_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                           @endforeach
                       </select>
                   </span>
                   <input type="text" class="form-control" id="fb_duration_other" name="fb_duration_other" value="{{ !empty(old('fb_duration_other')) ? old('fb_duration_other') : ( !empty($fb['fb_duration_other']) ? $fb['fb_duration_other'] : '' ) }}" onkeypress="return isNumberKey(event);" />
               </div>
                @if ($errors->has('fb_duration'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_duration') }}</span>
                @endif
   
                @if ($errors->has('fb_duration_other'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_duration_other') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
           <label for="fb_campaign_dt_from" class="col-md-4 control-label">is the campaign date set?</label>
   
           <div class="col-md-6">
               <input type="radio" id="fb_campaign_date_set" name="fb_campaign_date_set" value="1" {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == '1' ? 'checked' : '') : (!empty($fb['fb_campaign_date_set']) ? ($fb['fb_campaign_date_set'] == '1' ? 'checked' : '' ) : '') }}/>Yes
               <input class="clearfix" type="radio" id="fb_campaign_date_set" name="fb_campaign_date_set" value="0" {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == '0' ? 'checked' : '') : (!empty($fb['fb_campaign_date_set']) ? ($fb['fb_campaign_date_set'] == '0' ? 'checked' : '' ) : 'checked') }}/>No
           </div>
       </div>
   
       <div class="fb-campaign {{ !empty(old('fb_campaign_date_set')) ? (old('fb_campaign_date_set') == 1 ? '' : 'hidden') : (!empty($fb['fb_campaign_date_set']) ? ($fb['fb_campaign_date_set'] == 1 ? '' : 'hidden') : 'hidden') }}">
           <div class="form-group clearfix">
               <label for="fb_campaign_dt_from" class="col-md-4 control-label">Campaign Date Start *</label>
      
               <div class="col-md-6">
                   <!-- <input id="fb_campaign_dt_from" type="date" class="form-control" name="fb_campaign_dt_from" value="{{ old('fb_campaign_dt_from') }}"> -->
                   <input type="text" class="form-control datepicker" id="fb_campaign_dt_from" name="fb_campaign_dt_from" value="{{ !empty(old('fb_campaign_dt_from')) ? old('fb_campaign_dt_from') : (!empty($fb) ? $fb['fb_campaign_dt_from'] : '')  }}" data-date-format="yyyy-mm-dd" readonly>
      
                   @if ($errors->has('fb_campaign_dt_from'))
                       <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign_dt_from') }}</span>
                   @endif
      
               </div>
           </div>
      
           <div class="form-group clearfix">
               <label for="fb_campaign_dt_to" class="col-md-4 control-label">Campaign Date End *</label>
      
               <div class="col-md-6">
                   <!-- <input id="fb_campaign_dt_to" type="date" class="form-control" name="fb_campaign_dt_to" value="{{ old('fb_campaign_dt_to') }}"> -->
                   <input type="text" class="form-control datepicker" id="fb_campaign_dt_to" name="fb_campaign_dt_to" value="{{ !empty(old('fb_campaign_dt_to')) ? old('fb_campaign_dt_to') : (!empty($fb) ? $fb['fb_campaign_dt_to'] : '')  }}" data-date-format="yyyy-mm-dd" readonly>
      
                   @if ($errors->has('fb_campaign_dt_to'))
                       <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_campaign_dt_to') }}</span>
                   @endif
      
               </div>
           </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="fb_currency" class="col-md-4 control-label">Currency</label>
   
            <div class="col-md-6 text-left">
                <div class="select-box">
                    <select class="form-control" id="currency" name="fb_currency">
                       <option value="USD" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'USD' ? 'selected' : '') : (!empty($fb) ? ($fb['fb_currency'] == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                       <option value="SGD" {{ empty(old('fb_currency')) ? 'selected' : (!empty($fb) ? ($fb['fb_currency'] == 'SGD' ? 'selected' : '') : (old('fb_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                       <option value="PHP" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'PHP' ? 'selected' : '') : (!empty($fb) ? ($fb['fb_currency'] == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                       <option value="MYR" {{ !empty(old('fb_currency')) ? (old('fb_currency') == 'MYR' ? 'selected' : '') : (!empty($fb) ? ($fb['fb_currency'] == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                    </select>
                </div>
            </div>
   
        </div>
   
        <div class="form-group clearfix">
            <label for="total_budget" class="col-md-4 control-label">Total Campaign Budget *</label>
   
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency" ></span></div> 
   
                    <div class="col-md-10"><input id="total_budget" type="text" class="form-control" name="total_budget" value="{{ !empty(old('total_budget')) ? old('total_budget') : (!empty($fb) ? $fb['total_budget'] : '' ) }}" onkeypress="return isNumberKey(event);"></div>
                </div>
                @if ($errors->has('total_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('total_budget') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget *</label>
   
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency" ></span></div>
                    <div class="col-md-10">
                       <input id="monthly_budget" type="text" class="form-control" name="monthly_budget" value="{{ !empty(old('monthly_budget')) ? old('monthly_budget') : (!empty($fb) ? $fb['monthly_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                    </div>
                </div>
                
                @if ($errors->has('monthly_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('monthly_budget') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="daily_budget" class="col-md-4 control-label">Daily Campaign Budget *</label>
   
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency" ></span></div>
                    <div class="col-md-10">
                        <input id="daily_budget" type="text" class="form-control" name="daily_budget" value="{{ !empty(old('daily_budget')) ? old('daily_budget') : (!empty($fb) ? $fb['daily_budget'] : '' ) }}" readonly>
                    </div>
                </div>
   
                @if ($errors->has('daily_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('daily_budget') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="gender" class="col-md-4 control-label">Gender *</label>
   
            <div class="col-md-6 text-left">
                <select class="form-control" name="gender">
                   <option value="A" {{ empty(old('gender')) ? (!empty($fb) ? ($fb['gender'] == 'A' ? 'selected' : '') : 'selected') : '' }}>All</option>
                   <option value="M" {{ !empty(old('gender')) ? ( old('gender') == 'M' ? 'selected' : '') : ( !empty($fb) ? ($fb['gender'] == 'M' ? 'selected' : '') : '' ) }}>Male</option>
                   <option value="F" {{ !empty(old('gender')) ? ( old('gender') == 'F' ? 'selected' : '') : ( !empty($fb) ? ($fb['gender'] == 'F' ? 'selected' : '') : '' ) }}>Female</option>
                </select>
                @if ($errors->has('gender'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('gender') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="age_group" class="col-md-4 control-label">Age Group *</label>
   
            <div class="row col-md-6">
                
                <div class="col-md-6">
                    <label for="age_from" class="label-inline">From</label>
                    <div class="select-box">
                       <select id="age_from" name="age_from">
                           @for($i = 1; $i <= 65; $i++)
                               <option value="{{ $i }}" {{ !empty(old('age_from')) ? (old('age_from') == $i ? 'selected' : '') : (!empty($fb) && $fb['age_from'] == $i ? 'selected' : '') }}> {{ $i }} </option>
                           @endfor
                       </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="age_to" class="label-inline">To</label>
                    <div class="select-box">
                       <select id="age_to" name="age_to">
                           @for($i = 1; $i <= 65; $i++)
                               <option value="{{ $i }}" {{ !empty(old('age_to')) ? (old('age_to') == $i ? 'selected' : '') : (!empty($fb) && $fb['age_to'] == $i ? 'selected' : '') }}> {{ $i == 65 ? '65+' : $i }} </option>
                           @endfor
                       </select>
                    </div>
                </div>
   
                <div class="clearfix">&nbsp;</div>
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="interest" class="col-md-4 control-label">Interest *</label>
   
            <div class="col-md-6">
                <textarea class="form-control" id="interest" name="interest" rows="5">{{ !empty(old('interest')) ? old('interest') : ( !empty($fb) ? $fb['interest'] : '' ) }}</textarea>
                @if ($errors->has('interest'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('interest') }}</span>
                @endif
   
            </div>
        </div>
   
        <div class="form-group clearfix">
            <label for="fb_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
   
            <div class="col-md-6 text-left">
              <input type="radio" id="has_fb_hisdata" name="has_fb_hisdata" value="1" {{ (old('has_fb_hisdata') == 1 ? 'checked' : '') }}/>Yes
              <input type="radio" id="has_fb_hisdata" name="has_fb_hisdata" value="0" {{ empty(old('has_fb_hisdata')) ? 'checked' : (old('has_fb_hisdata') == 0 ? 'checked' : '') }}/>No
              <br>
            </div>
            <div class="col-md-12">
              <div class="fb-file-uploading {{ old('has_fb_hisdata') == 1 ? '' : 'hidden' }}">
                 
                 <div class="file-loading">
                     <input id="fb_hist_data" name="fb_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                 </div>
   
                 @if($errors->has('fb_hist_data'))
                     <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('fb_hist_data') }}</span>
                 @endif
                 
              </div>
   
            </div>
        </div>
   
   </fieldset>
   