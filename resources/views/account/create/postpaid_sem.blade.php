<fieldset>
    <input type="hidden" name="postpaid_included" id="postpaid_included" value="{{ !empty($account->postpaid) || old('postpaid_included') == 'yes' ? 'yes' : 'no' }}" />

    <div class="form-group clearfix">
        <label for="postpaid_duration" class="col-md-4 control-label">Campaign Duration *</label>

        <div class="col-md-6">
            <div class="select-box">
              <select class="selectpicker form-control" id="postpaid_duration" name="postpaid_duration" data-live-search="true">
                 @foreach($duration as $row)
                     <option value="{{ $row->systemcode }}" {{ !empty(old('postpaid_duration')) ? (old('postpaid_duration') == $row->systemcode ? 'selected' : '') : ( !empty($postpaid) ? ($postpaid['postpaid_duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                 @endforeach
              </select>
            </div>

            <div id="postpaid-duration-other" class="input-group in-other {{ !empty(old('postpaid_duration_other')) || old('postpaid_duration') == 'CD999' ? '' : ( !empty($postpaid['postpaid_duration_other']) ? '' : 'hidden' ) }}">
                <span class="input-group-btn">
                    <select class="btn campaign-interval" id="postpaid_duration_other_picker" name="postpaid_duration_interval">
                        @foreach($otherduration as $row)
                            <option value="{{ $row->systemcode }}" {{ !empty(old('postpaid_duration_interval')) ? (old('postpaid_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($postpaid) ? ($postpaid['postpaid_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                        @endforeach
                    </select>
                </span>
                <input type="text" class="form-control" id="postpaid_duration_other" name="postpaid_duration_other" value="{{ !empty(old('postpaid_duration_other')) ? old('postpaid_duration_other') : ( !empty($postpaid['postpaid_duration_other']) ? $postpaid['postpaid_duration_other'] : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
            </div>

            @if ($errors->has('postpaid_duration'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_duration') }}</span>
            @endif

            @if ($errors->has('postpaid_duration_other'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_duration_other') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="postpaid_campaign_date_set" class="col-md-4 control-label">Is the campaign date set? *</label>

        <div class="col-md-6">
            <input type="radio" id="postpaid_campaign_date_set" name="postpaid_campaign_date_set" value="1" {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == '1' ? 'checked' : '') : (!empty($postpaid['postpaid_campaign_date_set']) ? ($postpaid['postpaid_campaign_date_set'] == '1' ? 'checked' : '' ) : '') }}/>Yes
            <input class="clearfix" type="radio" id="postpaid_campaign_date_set" name="postpaid_campaign_date_set" value="0" {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == '0' ? 'checked' : '') : (!empty($postpaid['postpaid_campaign_date_set']) ? ($postpaid['postpaid_campaign_date_set'] == '0' ? 'checked' : '' ) : 'checked') }}/>No
        </div>
    </div>

    <div class="postpaid-campaign {{ !empty(old('postpaid_campaign_date_set')) ? (old('postpaid_campaign_date_set') == 1 ? '' : 'hidden') : (!empty($postpaid['postpaid_campaign_date_set']) ? ($postpaid['postpaid_campaign_date_set'] == 1 ? '' : 'hidden') : 'hidden') }}">
        <div class="form-group clearfix">
            <label for="postpaid_campaign_datefrom" class="col-md-4 control-label">Campaign Start Date *</label>

            <div class="col-md-6">
                {{-- <!-- <input id="postpaid_campaign_datefrom" type="date" class="form-control" name="postpaid_campaign_datefrom" value="{{ old('postpaid_campaign_datefrom') }}"> --> --}}
                <input type="text" class="form-control datepicker" id="postpaid_campaign_datefrom" name="postpaid_campaign_datefrom" value="{{ !empty(old('postpaid_campaign_datefrom')) ? old('postpaid_campaign_datefrom') : ( !empty($postpaid['postpaid_campaign_datefrom']) ? $postpaid['postpaid_campaign_datefrom'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>

                @if ($errors->has('postpaid_campaign_datefrom'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_campaign_datefrom') }}</span> 
                @endif

            </div>
        </div>

        <div class="form-group clearfix">
            <label for="postpaid_campaign_dateto" class="col-md-4 control-label">Campaign End Date *</label>

            <div class="col-md-6">
                {{-- <!-- <input id="postpaid_campaign_dateto" type="date" class="form-control" name="postpaid_campaign_dateto" value="{{ old('postpaid_campaign_dateto') }}"> --> --}}
                <input type="text" class="form-control datepicker" id="postpaid_campaign_dateto" name="postpaid_campaign_dateto" value="{{ !empty(old('postpaid_campaign_dateto')) ? old('postpaid_campaign_dateto') : ( !empty($postpaid['postpaid_campaign_dateto']) ? $postpaid['postpaid_campaign_dateto'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>

                @if ($errors->has('postpaid_campaign_dateto'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_campaign_dateto') }}</span>
                @endif
            </div>
        </div>
   </div>

    <div class="form-group clearfix">
        <label for="postpaid_media_budget" class="col-md-4 control-label">Media Budget Utilized *</label>
            
        <div class="col-md-6">
            <span id="media_budget_err" class="error-p account"></span>
            <div class="input-group" id="input-group-media-budget">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" id="postpaid_media_budget" name="postpaid_media_budget" value="{{ !empty(old('postpaid_media_budget')) ? old('postpaid_media_budget') : (!empty($postpaid) ? $postpaid['postpaid_media_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
            </div>
            
            @if ($errors->has('postpaid_media_budget'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_media_budget') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="postpaid_am_fee" class="col-md-4 control-label">AM Fee % *</label>
            
        <div class="col-md-6">
            <span id="am_fee_err" class="error-p account"></span>
            <input type="text" maxlength="3" class="form-control" id="postpaid_am_fee" name="postpaid_am_fee" value="{{ !empty(old('postpaid_am_fee')) ? old('postpaid_am_fee') : (!empty($postpaid) ? $postpaid['postpaid_am_fee'] : '' ) }}" onkeypress="return isNumberKey(event);"/>
            
            @if ($errors->has('postpaid_am_fee'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_am_fee') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="postpaid_am_fee_amount" class="col-md-4 control-label">AM Fee Amount</label>
            
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" id="postpaid_am_fee_amount" name="postpaid_am_fee_amount" value="{{ !empty(old('postpaid_am_fee_amount')) ? old('postpaid_am_fee_amount') : (!empty($postpaid) ? $postpaid['postpaid_am_fee_amount'] : '' ) }}" onkeypress="return isNumberKey(event);" readonly="true"/>
            </div>
            @if ($errors->has('postpaid_am_fee_amount'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_am_fee_amount') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="has_postpaid_media_budget" class="col-md-4 control-label">Is Media Budget Paid through OOm? *</label>

        <div class="col-md-6 text-left">
            <input type="radio" id="is_media_budget_paid" name="is_media_budget_paid" value="1" {{ !empty(old('is_media_budget_paid')) && old('is_media_budget_paid') == '1' ? 'checked' : (!empty($postpaid) && !empty($postpaid['postpaid_media_budget']) ? 'checked' : '') }}/>Yes
            <input type="radio" id="is_media_budget_paid" name="is_media_budget_paid" value="0" {{ !empty(old('is_media_budget_paid')) ? (old('is_media_budget_paid') == '0' ? 'checked' : '' ) : (!empty($postpaid) ? (empty($postpaid['postpaid_media_budget']) ? 'checked' : '') : 'checked') }} />No
            <br>

            <span class="text-left">If <strong><u>Yes</u></strong> is selected the amount is <strong>AM Fee Amount</strong> + <strong>Media Budget Utilized</strong></span><br>
            <span class="text-left">If <strong><u>No</u></strong> is selected the amount is <strong>AM Fee Amount</strong></span>
        </div>

    </div>

    <div class="form-group clearfix">
        <label for="postpaid_total" class="col-md-4 control-label">Total Amount</label>
            
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" id="postpaid_total" name="postpaid_total" value="{{ !empty(old('postpaid_total')) ? old('postpaid_total') : (!empty($postpaid) ? $postpaid['postpaid_total'] : '' ) }}" onkeypress="return isNumberKey(event);" readonly="true"/>
            </div>
            @if ($errors->has('postpaid_total'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('postpaid_total') }}</span>
            @endif
        </div>
    </div>

   

</fieldset>