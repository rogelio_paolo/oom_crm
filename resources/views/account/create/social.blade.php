<fieldset>
    <input type="hidden" name="social_included" id="social_included" value="{{ !empty($account->social) || old('social_included') == 'yes' ? 'yes' : 'no' }}" />

    <div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Campaign Duration *</label>

        <div class="col-md-6">
            <div class="select-box">
              <select class="selectpicker form-control" id="social_duration" name="social_duration" data-live-search="true">
                 @foreach($duration as $row)
                     <option value="{{ $row->systemcode }}" {{ !empty(old('social_duration')) ? (old('social_duration') == $row->systemcode ? 'selected' : '') : ( !empty($social) ? ($social['social_duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                 @endforeach
              </select>
            </div>

            <div id="social-duration-other" class="input-group in-other {{ !empty(old('social_duration_other')) || old('social_duration') == 'CD999' ? '' : ( !empty($social['social_duration_other']) ? '' : 'hidden' ) }}">
                <span class="input-group-btn">
                    <select class="btn campaign-interval" id="social_duration_other_picker" name="social_duration_interval">
                        @foreach($otherduration as $row)
                            <option value="{{ $row->systemcode }}" {{ !empty(old('social_duration_interval')) ? (old('social_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($social) ? ($social['social_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                        @endforeach
                    </select>
                </span>
                <input type="text" class="form-control" id="social_duration_other" name="social_duration_other" value="{{ !empty(old('social_duration_other')) ? old('social_duration_other') : ( !empty($social['social_duration_other']) ? $social['social_duration_other'] : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
            </div>

            @if ($errors->has('social_duration'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_duration') }}</span>
            @endif

            @if ($errors->has('social_duration_other'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_duration_other') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">Number of Blog Post Per Month *</label>
            
        <div class="col-md-6">
            <input type="text" class="form-control" id="social_monthly_post" name="social_monthly_post" value="{{ !empty(old('social_monthly_post')) ? old('social_monthly_post') : (!empty($social) ? $social['social_monthly_post'] : '' ) }}" onkeypress="return isNumberKey(event);"/>
            
            @if ($errors->has('social_monthly_post'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_monthly_post') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="social_content_brief" class="col-md-4 control-label">Content Brief and Writing Style *</label>
            
        <div class="col-md-6">
            <textarea rows="5" class="form-control" id="social_content_brief" name="social_content_brief">{{ !empty(old('social_content_brief')) ? old('social_content_brief') : (!empty($social) ? $social['social_content_brief'] : '' ) }}</textarea>
            @if ($errors->has('social_content_brief'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_content_brief') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="has_social_media_budget" class="col-md-4 control-label">Any FB Media Budget to Boost Post *</label>

        <div class="col-md-6 text-left">
            <input type="radio" id="has_social_media_budget" name="has_social_media_budget" value="1" {{ !empty(old('has_social_media_budget')) && old('has_social_media_budget') == '1' ? 'checked' : (!empty($social) && !empty($social['social_media_budget']) ? 'checked' : '') }}/>Yes
            <input type="radio" id="has_social_media_budget" name="has_social_media_budget" value="0" {{ !empty(old('has_social_media_budget')) ? (old('has_social_media_budget') == '0' ? 'checked' : '' ) : (!empty($social) ? (empty($social['social_media_budget']) ? 'checked' : '') : 'checked') }} />No
            <br>
        </div>
    </div>

<div class="social-media-budget {{ !empty(old('has_social_media_budget')) ? (old('has_social_media_budget') == 1 ? '' : 'hidden') : (!empty($social) ? (!empty($social['social_media_budget']) ? '' : 'hidden') : 'hidden') }}">
        <div class="form-group clearfix">
            <label for="social_media_budget" class="col-md-4 control-label"> Media Budget *</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" id="social_media_budget" name="social_media_budget" value="{{ !empty(old('social_media_budget')) ? old('social_media_budget') : (!empty($social) ? $social['social_media_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                </div>
                
                @if ($errors->has('social_media_budget'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_media_budget') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="social_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
        <div class="col-md-6 text-left">
            <input type="radio" id="has_social_hisdata" name="has_social_hisdata" value="1" {{ !empty(old('has_social_hisdata')) && old('has_social_hisdata') == '1' ? 'checked' : (!empty($social) && !empty($social['has_social_hisdata']) ? 'checked' : '') }} />Yes
            <input type="radio" id="has_social_hisdata" name="has_social_hisdata" value="0" {{ !empty(old('has_social_hisdata')) ? (old('has_social_hisdata') == '0' ? 'checked' : '' ) : (!empty($social) ? (empty($social['has_social_hisdata']) ? 'checked' : '') : 'checked') }} />No
            <br>
        </div>
        <div class="col-md-12">
            <div class="social-file-uploading {{ !empty(old('has_social_hisdata')) ? (old('has_social_hisdata') == '0' ? 'hidden' : '' ) : (empty($social['social_hist_data']) ? 'hidden' : '') }}">
                <div class="file-loading">
                    <input id="social_hist_data" name="social_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                </div>

                @if ($errors->has('social_hist_data'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('social_hist_data') }}</span>
            @endif
            </div>
        </div>
    </div>
</fieldset>