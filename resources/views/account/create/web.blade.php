<fieldset>

   <div class="form-group clearfix">
       <label for="project_type" class="col-md-4 control-label">Project Type *</label>

       <div class="col-md-6">
           <div class="select-box">
               <select name="project_type" id="project_type" class="form-control">
                   @foreach($webtype as $row)
                       <option value="{{ $row->systemcode }}">{{ $row->systemdesc }}</option>
                   @endforeach
               </select>
           </div>

           @if ($errors->has('project_type'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('project_type') }}
               </span>
           @endif
       </div>
   </div>
   
   <div class="form-group clearfix">
       <label for="cmstype" class="col-md-4 control-label">CMS System *</label>

       <div class="col-md-6">
           <div class="select-box">
               <select name="cmstype" id="cmstype" class="form-control">
                   @foreach($cmstype as $row)
                       <option value="{{ $row->systemcode }}">{{ $row->systemdesc }}</option>
                   @endforeach
               </select>
           </div>

           @if ($errors->has('cmstype'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('cmstype') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="pages" class="col-md-4 control-label">Number of Pages *</label>

       <div class="col-md-6">
           <input id="pages" type="text" class="form-control" name="pages" value="{{ old('pages') }}">

           @if ($errors->has('pages'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('pages') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="objective" class="col-md-4 control-label">Objective *</label>

       <div class="col-md-6">
                     
           <textarea class="form-control" id="objective" name="objective" rows="5">{{ old('objective') }}</textarea>

           @if ($errors->has('objective'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('objective') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
        <label for="company_brief" class="col-md-4 control-label">Brief information about the Company *</label>

        <div class="col-md-6">
                        
            <textarea class="form-control" id="company_brief" name="company_brief" rows="5">{{ old('company_brief') }}</textarea>

            @if ($errors->has('company_brief'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    {{ $errors->first('company_brief') }}
                </span>
            @endif
        </div>
    </div>

   <div class="form-group clearfix">
        <label for="target_audience" class="col-md-4 control-label">Target Audience *</label>

        <div class="col-md-6">
                        
            <textarea class="form-control" id="target_audience" name="target_audience" rows="5">{{ old('target_audience') }}</textarea>

            @if ($errors->has('target_audience'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    {{ $errors->first('target_audience') }}
                </span>
            @endif
        </div>
    </div>



   <div class="form-group clearfix">
       <label for="existing_web_url" class="col-md-4 control-label">Existing Website (Url) *</label>

       <div class="col-md-6">
           <input id="existing_web_url" type="text" class="form-control" name="existing_web_url" value="{{ old('existing_web_url') }}">

           @if ($errors->has('existing_web_url'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('existing_web_url') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="reference_web" class="col-md-4 control-label">Preferred Reference Website *</label>

       <div class="col-md-6">
                     
           <textarea class="form-control" id="reference_web" name="reference_web" rows="5">{{ old('reference_web') }}</textarea>
           
           @if ($errors->has('reference_web'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('reference_web') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="web_ui" class="col-md-4 control-label">Look and Feel *</label>

       <div class="col-md-6">
           
           <textarea class="form-control" id="web_ui" name="web_ui" rows="5">{{ old('web_ui') }}</textarea>
           
           @if ($errors->has('web_ui'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('web_ui') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="color_scheme" class="col-md-4 control-label">Color Scheme *</label>

       <div class="col-md-6">
                     
           <textarea class="form-control" id="color_scheme" name="color_scheme" rows="5">{{ old('color_scheme') }}</textarea>
           
           @if ($errors->has('color_scheme'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('color_scheme') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="functionality" class="col-md-4 control-label">Special Functionality *</label>

       <div class="col-md-6">
           
           <textarea class="form-control" id="functionality" name="functionality" rows="5">{{ old('functionality') }}</textarea>

           @if ($errors->has('functionality'))
               <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   {{ $errors->first('functionality') }}
               </span>
           @endif
       </div>
   </div>

   <div class="form-group clearfix">
        <label for="web_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>

        <div class="col-md-6 text-left">
        <input type="radio" id="has_web_hisdata" name="has_web_hisdata" value="1" {{ old('has_web_hisdata') == 1 ? 'checked' : '' }}/>Yes
        <input type="radio" id="has_web_hisdata" name="has_web_hisdata" value="0" {{ empty(old('has_web_hisdata')) ? 'checked' : (old('has_web_hisdata') == 0 ? 'checked' : '') }}/>No
        </div>
        <div class="col-md-12">
            <div class="web-file-uploading {{ old('has_web_hisdata') == 1 ? '' : 'hidden' }}">
                
                <div class="file-loading">
                    <input id="web_hist_data" name="web_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                </div>

                @if($errors->has('web_hist_data'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('web_hist_data') }}</span>
                @endif
                
            </div>
        </div>
    </div>

</fieldset>
