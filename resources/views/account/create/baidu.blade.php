{{-- @php dd($baidu); @endphp --}}
<fieldset>
        <input type="hidden" name="baidu_included" id="baidu_included" value="{{ !empty($account->baidu) || old('baidu_included') == 'yes' ? 'yes' : 'no' }}" />
    
        <div class="form-group clearfix">
            <label for="has_baidu_account" class="col-md-4 control-label">Existing Baidu Account? *</label>
            <div class="col-md-6">
                <input type="radio" id="has_baidu_account" name="has_baidu_account" value="1" {{ !empty(old('has_baidu_account')) && old('has_baidu_account') == '1' ? 'checked' : (!empty($baidu) && !empty($baidu['has_baidu_account']) ? 'checked' : '') }}/>Yes
                <input type="radio" id="has_baidu_account" name="has_baidu_account" value="0" {{ !empty(old('has_baidu_account')) ? (old('has_baidu_account') == '0' ? 'checked' : '' ) : (!empty($baidu) ? (empty($baidu['has_baidu_account']) ? 'checked' : '') : 'checked') }} />No
            </div>
        </div>
    
        <div class="baidu-account {{ !empty(old('has_baidu_account')) ? (old('has_baidu_account') == 1 ? '' : 'hidden') : (!empty($baidu) ? (!empty($baidu['baidu_account']) ? '' : 'hidden') : 'hidden') }}">
            <div class="form-group clearfix">
                <label for="baidu_account" class="col-md-4 control-label">Baidu Account User ID *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="baidu_account" name="baidu_account" value="{{ !empty(old('baidu_account')) ? old('baidu_account') : (!empty($baidu) ? $baidu['baidu_account'] : '' ) }}" />
                
                    @if ($errors->has('baidu_account'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_account') }}</span>
                    @endif
                </div>
            </div>
        </div>
    
        <div class="form-group clearfix">
           <label for="baidu_target_market" class="col-md-4 control-label">Target Market *</label>
    
           <div class="col-md-6">
               <div class="select-box">
                   <select class="selectpicker form-control" id="baidu_target_market" name="baidu_target_market[]" data-live-search="true" data-actions-box="true" multiple="multiple">
                      @foreach($countries as $row)
                          @if (empty($baidu) && empty(old('baidu_target_market')) && $row->code == 'SG') --}}
                              <option value="{{ $row->code }}" selected>{{ $row->country }}</option>
                          @else
                              <option value="{{ $row->code }}" {{ !empty(old('baidu_target_market')) ? (in_array($row->code,old('baidu_target_market')) ? 'selected' : '') : (!empty($baidu) ? (in_array($row->code,explode('|',$baidu['baidu_target_market'])) ? 'selected' : '') : '') }}>{{ $row->country }}</option>
                          @endif
                      @endforeach
                   </select>
                   {{-- <input type="hidden" id="baidu_target_market_array" name="baidu_target_market" value="{{ !empty(old('baidu_target_market')) ? old('baidu_target_market') : (!empty($baidu) ? $baidu['baidu_target_market'] : '') }}" /> --}}
               </div>
    
               @if ($errors->has('baidu_target_market'))
                   <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_target_market') }}</span>
               @endif
    
           </div>
       </div>
    
        <div class="form-group clearfix">
            <label for="baidu_city" class="col-md-4 control-label">City *</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="baidu_city" name="baidu_city" value="{{ !empty(old('baidu_city')) ? old('baidu_city') : (!empty($baidu) ? $baidu['baidu_city'] : '' ) }}" />
            
                @if ($errors->has('baidu_city'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_city') }}</span>
                @endif
            </div>
        </div>
    
       <div class="form-group clearfix">
           <label for="baidu_media_channel" class="col-md-4 control-label">Media Channels *</label>
    
           <div class="col-md-6">
               <div class="select-box">
                   <select id="baidu_media_channel" name="baidu_media_channel[]" class="multiselect-ui form-control" multiple="multiple">
    
                      @foreach($baidu_channels as $row)
                            <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_media_channel')) ? (in_array($row->systemcode,old('baidu_media_channel')) ? 'selected' : '') : (!empty($baidu) ? (in_array($row->systemcode,explode('|',$baidu['baidu_media_channel'])) ? 'selected' : '') : ($row->systemcode == 'BC001' ? 'selected' : '')) }}>{{ $row->systemdesc }}</option>
                      @endforeach
                   </select>
                   {{-- <input type="hidden" id="baidu_media_channel_array" name="baidu_media_channel" value="{{ !empty(old('baidu_media_channel')) ? old('baidu_media_channel') : (!empty($baidu) ? $baidu['baidu_media_channel'] : '') }}" /> --}}
                    @if ($errors->has('baidu_media_channel'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_media_channel') }}</span>
                    @endif
               </div>
           </div>
       </div>
    
       <div class="form-group clearfix">
           <label for="baidu_campaign" class="col-md-4 control-label">Campaign Brief & Expectations *</label>
    
           <div class="col-md-6">
               <textarea class="form-control" id="baidu_campaign" name="baidu_campaign" rows="5">{!! !empty(old('baidu_campaign')) ? old('baidu_campaign') : (!empty($baidu) ? $baidu['baidu_campaign'] : '' ) !!}</textarea>
    
               @if ($errors->has('baidu_campaign'))
                   <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_campaign') }}</span>
               @endif
    
           </div>
       </div>
    
       <div class="form-group clearfix">
            <label for="baidu_duration" class="col-md-4 control-label">Campaign Duration *</label>
    
            <div class="col-md-6">
                <div class="select-box">
                  <select class="selectpicker form-control" id="baidu_duration" name="baidu_duration" data-live-search="true">
                     @foreach($duration as $row)
                         <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_duration')) ? (old('baidu_duration') == $row->systemcode ? 'selected' : '') : ( !empty($baidu) ? ($baidu['baidu_duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                     @endforeach
                  </select>
                </div>
    
                <div id="baidu-duration-other" class="input-group in-other {{ !empty(old('baidu_duration_other')) || old('baidu_duration') == 'CD999' ? '' : ( !empty($baidu['baidu_duration_other']) ? '' : 'hidden' ) }}">
                    <span class="input-group-btn">
                        <select class="btn campaign-interval" id="baidu_duration_other_picker" name="baidu_duration_interval">
                            @foreach($otherduration as $row)
                                <option value="{{ $row->systemcode }}" {{ !empty(old('baidu_duration_interval')) ? (old('baidu_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($baidu) ? ($baidu['baidu_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                            @endforeach
                        </select>
                    </span>
                    <input type="text" class="form-control" id="baidu_duration_other" name="baidu_duration_other" value="{{ !empty(old('baidu_duration_other')) ? old('baidu_duration_other') : ( !empty($baidu['baidu_duration_other']) ? $baidu['baidu_duration_other'] : '' ) }}" maxlength="3" onkeypress="return isNumberKey(event);" />
                </div>
    
                @if ($errors->has('baidu_duration'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_duration') }}</span>
                @endif
    
                @if ($errors->has('baidu_duration_other'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_duration_other') }}</span>
                @endif
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="has_baidu_campaign_dateset" class="col-md-4 control-label">Is the campaign date set? </label>
    
            <div class="col-md-6 text-left">
                <input type="radio" id="has_baidu_campaign_dateset" name="has_baidu_campaign_dateset" value="1" {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == '1' ? 'checked' : '') : (!empty($baidu['has_baidu_campaign_dateset']) ? ($baidu['has_baidu_campaign_dateset'] == '1' ? 'checked' : '' ) : '') }}/>Yes
                <input type="radio" id="has_baidu_campaign_dateset" name="has_baidu_campaign_dateset" value="0" {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == '0' ? 'checked' : '') : (!empty($baidu['has_baidu_campaign_dateset']) ? ($baidu['has_baidu_campaign_dateset'] == '0' ? 'checked' : '' ) : 'checked') }} />No
                <br>
            </div>
        </div>
    
        <div class="baidu-campaign {{ !empty(old('has_baidu_campaign_dateset')) ? (old('has_baidu_campaign_dateset') == 1 ? '' : 'hidden') : (!empty($baidu) ? (!empty($baidu['baidu_campaign_datefrom']) || !empty($baidu['baidu_campaign_dateto']) ? '' : 'hidden') : 'hidden') }}">
            <div class="form-group clearfix">
                <label for="baidu_campaign_datefrom" class="col-md-4 control-label"> Campaign Start Date *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control datepicker" id="baidu_campaign_datefrom" name="baidu_campaign_datefrom" value="{{ !empty(old('baidu_campaign_datefrom')) ? old('baidu_campaign_datefrom') : ( !empty($baidu['baidu_campaign_datefrom']) ? $baidu['baidu_campaign_datefrom'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
                
                    @if ($errors->has('baidu_campaign_datefrom'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_campaign_datefrom') }}</span> 
                    @endif
                </div>
            </div>
    
            <div class="form-group clearfix">
                <label for="baidu_campaign_dateto" class="col-md-4 control-label"> Campaign End Date *</label>
                <div class="col-md-6">
                    <input type="text" class="form-control datepicker" id="baidu_campaign_dateto" name="baidu_campaign_dateto" value="{{ !empty(old('baidu_campaign_dateto')) ? old('baidu_campaign_dateto') : ( !empty($baidu['baidu_campaign_dateto']) ? $baidu['baidu_campaign_dateto'] : '' ) }}" data-date-format="yyyy-mm-dd" readonly>
                
                    @if ($errors->has('baidu_campaign_dateto'))
                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_campaign_dateto') }}</span> 
                    @endif
                </div>
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="baidu_ad_scheduling" class="col-md-4 control-label">Ad Scheduling</label>
    
            <div class="col-md-6">
                <div>
                <input type="radio" id="baidu_ad_scheduling" name="baidu_ad_scheduling" value="1" {{ !empty(old('baidu_ad_scheduling')) ? (old('baidu_ad_scheduling') == 1 ? 'checked' : '') : ( !empty($baidu) ? ($baidu['baidu_ad_scheduling'] == 1 ? 'checked' : '') : '' ) }}/>Yes
                <input class="clearfix" type="radio" id="baidu_ad_scheduling" name="baidu_ad_scheduling" value="0" {{ !empty(old('baidu_ad_scheduling')) ? (old('baidu_ad_scheduling') == 0 ? 'checked' : '') : 'checked' }}/>No
                </div>
                <textarea class="form-control {{  !empty(old('baidu_ad_scheduling')) ? (old('baidu_ad_scheduling') == 1 ? '' : 'hidden') : ( !empty($baidu) ? ($baidu['baidu_ad_scheduling'] == 1 ? '' : 'hidden') : 'hidden' ) }}" id="baidu_ad_scheduling_remark" name="baidu_ad_scheduling_remark" rows="5"></textarea>
            </div>
       </div>
    
        <div class="form-group clearfix">
           <label for="baidu_currency" class="col-md-4 control-label">Currency</label>
           <div class="col-md-6 text-left">
               <div class="select-box">
                   <select class="form-control" id="baidu_currency" name="baidu_currency">
                       <option value="USD" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'USD' ? 'selected' : '') : (!empty($baidu) ? ($baidu['baidu_currency'] == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                       <option value="SGD" {{ empty(old('baidu_currency')) ? 'selected' : (!empty($baidu) ? ($baidu['baidu_currency'] == 'SGD' ? 'selected' : '') : (old('baidu_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                       <option value="PHP" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'PHP' ? 'selected' : '') : (!empty($baidu) ? ($baidu['baidu_currency'] == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                       <option value="MYR" {{ !empty(old('baidu_currency')) ? (old('baidu_currency') == 'MYR' ? 'selected' : '') : (!empty($baidu) ? ($baidu['baidu_currency'] == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                   </select>
               </div>
    
                @if ($errors->has('baidu_currency'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_currency') }}</span> 
                @endif
           </div>
       </div>
    
        <div class="form-group clearfix">
           <label for="baidu_budget" class="col-md-4 control-label">Total Campaign Budget *</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="baidu-campaign-currency"></span></div> 
                   <div class="col-md-10">
                       <input id="baidu_budget" type="text" class="form-control" name="baidu_budget" value="{{ !empty(old('baidu_budget')) ? old('baidu_budget') : (!empty($baidu) ? $baidu['baidu_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
    
                        @if ($errors->has('baidu_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
       <div class="form-group clearfix">
           <label for="baidu_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget *</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="baidu-campaign-currency"></span></div>
                   <div class="col-md-10">
                       <input id="baidu_monthly_budget" type="text" class="form-control" name="baidu_monthly_budget" value="{{ !empty(old('baidu_monthly_budget')) ? old('baidu_monthly_budget') : (!empty($baidu) ? $baidu['baidu_monthly_budget'] : '' ) }}" onkeypress="return isNumberKey(event);">
                   
                        @if ($errors->has('baidu_monthly_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_monthly_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
       <div class="form-group clearfix">
           <label for="baidu_daily_budget" class="col-md-4 control-label">Daily Campaign Budget *</label>
    
           <div class="col-md-6 text-left">
               <div class="row">
                   <div class="col-md-2"><span id="baidu-campaign-currency"></span></div>
                   <div class="col-md-10">
                       <input id="baidu_daily_budget" type="text" class="form-control" name="baidu_daily_budget" value="{{ !empty(old('baidu_daily_budget')) ? old('baidu_daily_budget') : (!empty($baidu) ? $baidu['baidu_daily_budget'] : '' ) }}" readonly onkeypress="return isNumberKey(event);">
                   
                        @if ($errors->has('baidu_daily_budget'))
                            <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_daily_budget') }}</span>
                        @endif
                    </div>
               </div>
           </div>
       </div>
    
        <div class="form-group clearfix">
            <label for="baidu_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
            <div class="col-md-6 text-left">
                <input type="radio" id="has_baidu_hisdata" name="has_baidu_hisdata" value="1" {{ !empty(old('has_baidu_hisdata')) && old('has_baidu_hisdata') == '1' ? 'checked' : (!empty($baidu) && !empty($baidu['has_baidu_hisdata']) ? 'checked' : '') }}/>Yes
                <input type="radio" id="has_baidu_hisdata" name="has_baidu_hisdata" value="0" {{ !empty(old('has_baidu_hisdata')) ? (old('has_baidu_hisdata') == '0' ? 'checked' : '' ) : (!empty($baidu) ? (empty($baidu['has_baidu_hisdata']) ? 'checked' : '') : 'checked') }} />No
                <br>
            </div>
            <div class="col-md-12">
                <div class="baidu-file-uploading {{ !empty(old('has_baidu_hisdata')) ? (old('has_baidu_hisdata') == '0' ? 'hidden' : '' ) : (empty($baidu['baidu_hist_data']) ? 'hidden' : '') }}">
                    <div class="file-loading">
                        <input id="baidu_hist_data" class="hist_data" name="baidu_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..."  />
                    </div>
                </div>
    
                @if ($errors->has('baidu_hist_data'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('baidu_hist_data') }}</span>
                @endif
            </div>
        </div>
    </fieldset>