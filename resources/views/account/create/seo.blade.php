<fieldset>
        <div class="form-group clearfix">
            <label for="seo_target_countries" class="col-md-4 control-label">Target Country *</label>
            <div class="col-md-6">
                <div class="select-box">
                    <select class="selectpicker form-control" id="seo_target_countries" name="seo_target_countries[]" data-live-search="true" data-actions-box="true" multiple="multiple">
                    @foreach($countries as $row)
                    @if (empty($seo) && empty(old('seo_target_countries')) && $row->code == 'SG')
                    <option value="{{ $row->code }}" selected>{{ $row->country }}</option>
                    @else
                    <option value="{{ $row->code }}" {{ !empty(old('seo_target_countries')) && in_array($row->code,old('seo_target_countries')) ? 'selected' : ( !empty($seo) ? (in_array($row->code,explode('|',$seo['seo_target_countries'])) ? 'selected' : '') : '' ) }}>{{ $row->country }}</option>
                    @endif
                    @endforeach
                    </select>
                </div>
    
                @if ($errors->has('seo_target_countries'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_target_countries') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <label for="keyword_themes" class="col-md-4 control-label">No. of Keywords *</label>
            <div class="col-md-6">
                <div class="select-box">
                    <select class="form-control" id="keyword_themes_select" name="keyword_themes">
                    @foreach($keywordsno as $row)
                    <option value="{{ $row->systemcode }}" {{ !empty(old('keyword_themes')) ? (old('keyword_themes') == $row->systemcode ? 'selected' : '') : ( !empty($seo) ? ($seo['keyword_themes'] == $row->systemcode ? 'selected' : '' ) : '') }}>{{ $row->systemdesc }}</option>
                    @endforeach
                    </select>
                </div>
                <input type="text" class="form-control in-other {{ !empty(old('keyword_themes')) ? (old('keyword_themes') == 'NK999' ? '' : 'hidden') : ( !empty($seo) ? ($seo['keyword_themes'] == 'NK999' ? '' : 'hidden') : 'hidden' ) }}" id="keyword_themes" name="keyword_themes_other" value="{{ !empty(old('keyword_themes')) ? (old('keyword_themes') == 'NK999' ? old('keyword_themes_other') : '' ) : ( !empty($seo) ? ($seo['keyword_themes'] == 'NK999' ? $seo['keyword_themes_other'] : '') : '') }}" {{ !empty(old('keyword_themes')) ? (old('keyword_themes') == 'NK999' ? '' : 'disabled') : ( !empty($seo) ? ($seo['keyword_themes'] == 'NK999' ? '' : 'disabled') : '' ) }} maxlength="3" onkeypress="return isNumberKey(event);"/>
                @if ($errors->has('keyword_themes'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_themes') }}</span>
                @endif
                @if ($errors->has('keyword_themes_other'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_themes_other') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <label for="keyword_focus" class="col-md-4 control-label">Keyword Theme Focus *</label>
            <div class="col-md-6">
                <textarea class="form-control" name="keyword_focus" rows="5">{{ !empty(old('keyword_focus')) ? old('keyword_focus') : ( !empty($seo) ? $seo['keyword_focus'] : '' ) }}</textarea>
                @if ($errors->has('keyword_focus'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_focus') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <label for="keyword_focus" class="col-md-4 control-label">Keyword Maintenance Project *</label>
            <div class="col-md-6">
                <input type="radio" id="has_keyword_maintenance" name="has_keyword_maintenance" value="1" {{ !empty(old('has_keyword_maintenance')) ? (old('has_keyword_maintenance') == 1 ? 'checked' : '') : ( !empty($seo) ? ($seo['has_keyword_maintenance'] == 1 ? 'checked' : '') : '' ) }}/>Yes
                <input type="radio" id="has_keyword_maintenance" name="has_keyword_maintenance" value="0" {{ !empty(old('has_keyword_maintenance')) ? (old('has_keyword_maintenance') == 0 ? 'checked' : '') : ( !empty($seo) ? ($seo['has_keyword_maintenance'] == 0 ? 'checked' : '') : 'checked' ) }}/>No
                <textarea class="form-control {{ !empty(old('has_keyword_maintenance')) ? ( old('has_keyword_maintenance') == 1 ? '' : 'hidden') : ( !empty($seo) ? ($seo['has_keyword_maintenance'] == 1 ? '' : 'hidden') : 'hidden' ) }}" type="text" id="keyword_maintenance" name="keyword_maintenance" {{ old('has_keyword_maintenance') == 1 || (!empty($seo) && $seo['has_keyword_maintenance'] == 1) ? '' : 'disabled' }} rows="5">{!! !empty(old('keyword_maintenance') && old('keyword_maintenance') ) ? old('keyword_maintenance') : (!empty($seo['keyword_maintenance']) ? $seo['keyword_maintenance'] : '') !!}</textarea>
                @if ($errors->has('keyword_maintenance'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('keyword_maintenance') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <label for="seo_campaign" class="col-md-4 control-label">Campaign Brief & Expectations *</label>
            <div class="col-md-6">
                <textarea class="form-control" name="seo_campaign" rows="5">{!! !empty(old('seo_campaign')) ? old('seo_campaign') : ( !empty($seo) ? $seo['seo_campaign'] : '' ) !!}</textarea>
                @if ($errors->has('seo_campaign'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_campaign') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <label for="seo_duration" class="col-md-4 control-label">Campaign Duration *</label>
            <div class="col-md-6">
                <div class="select-box">
                    <select class="form-control" id="seo_duration" name="seo_duration">
                    @foreach($duration as $row)
                    <option value="{{ $row->systemcode }}" {{ !empty(old('seo_duration')) ? (old('seo_duration') == $row->systemcode ? 'selected' : '') : ( !empty($seo) ? ($seo['seo_duration'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                    @endforeach
                    </select>
                </div>
                <div id="seo-duration-other" class="input-group in-other {{ !empty(old('seo_duration_other')) || old('seo_duration') == 'CD999' ? '' : ( !empty($seo['seo_duration_other']) ? '' : 'hidden' ) }}">
                    <span class="input-group-btn">
                    <select class="btn campaign-interval" id="seo_other_picker" name="seo_duration_interval">
                    @foreach($otherduration as $row)
                    <option value="{{ $row->systemcode }}" {{ !empty(old('seo_duration_interval')) ? (old('seo_duration_interval') == $row->systemcode ? 'selected' : '') : ( !empty($seo) ? ($seo['seo_duration_interval'] == $row->systemcode ? 'selected' : '') : '' ) }}>{{ $row->systemdesc }}</option>
                    @endforeach
                    </select>
                    </span>
                    <input type="text" class="form-control" id="seo_duration_other" name="seo_duration_other" value="{{ !empty(old('seo_duration_other')) ? old('seo_duration_other') : ( !empty($seo['seo_duration_other']) ? $seo['seo_duration_other'] : '' ) }}" onkeypress="return isNumberKey(event);" />
                </div>
                @if ($errors->has('seo_duration'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_duration') }}</span>
                @endif
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="currency" class="col-md-4 control-label">Currency</label>
        
            <div class="col-md-6 text-left">
                <div class="select-box">
                    <select class="form-control" id="currency" name="seo_currency">
                        <option value="USD" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'USD' ? 'selected' : '') : (!empty($seo) ? ($seo['seo_currency'] == 'USD' ? 'selected' : '') : '') }}>US Dollar</option>
                        <option value="SGD" {{ empty(old('seo_currency')) ? 'selected' : (!empty($seo) ? ($seo['seo_currency'] == 'SGD' ? 'selected' : '') : (old('seo_currency') == 'SGD' ? 'selected' : '') ) }}>Singapore Dollar</option>
                        <option value="PHP" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'PHP' ? 'selected' : '') : (!empty($seo) ? ($seo['seo_currency'] == 'PHP' ? 'selected' : '') : '') }}>Philippine Peso</option>
                        <option value="MYR" {{ !empty(old('seo_currency')) ? (old('seo_currency') == 'MYR' ? 'selected' : '') : (!empty($seo) ? ($seo['seo_currency'] == 'MYR' ? 'selected' : '') : '') }}>Malaysian Ringgit</option>
                    </select>
                </div>
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="am_fee" class="col-md-4 control-label">Total SEO Fee *</label>
        
            <div class="col-md-6 text-left">
                <div class="row">
                    <div class="col-md-2"><span id="campaign-currency"></span></div> 
                    <div class="col-md-10">
                        <input id="seo_total_fee" type="text" class="form-control" name="seo_total_fee" value="{{ !empty(old('seo_total_fee')) ? old('seo_total_fee') : (!empty($seo) ? $seo['seo_total_fee'] : '' ) }}" onkeypress="return isNumberKey(event);">
                    </div>
                </div>
        
                @if ($errors->has('seo_total_fee'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_total_fee') }}</span>
                @endif
        
            </div>
        </div>
    
        <div class="form-group clearfix">
            <label for="seo_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
            <div class="col-md-6 text-left">
                <input type="radio" id="has_seo_hisdata" name="has_seo_hisdata" value="1" {{ (old('has_seo_hisdata') == 1 ? 'checked' : '') }}/>Yes
                <input type="radio" id="has_seo_hisdata" name="has_seo_hisdata" value="0" {{ empty(old('has_seo_hisdata')) ? 'checked' : (old('has_seo_hisdata') == 0 ? 'checked' : '') }} />No
                <br>
            </div>
            <div class="col-md-12">
                <div class="seo-file-uploading {{ old('has_seo_hisdata') == 1 ? '' : 'hidden' }}">
                    <div class="file-loading">
                    <input id="seo_hist_data" name="seo_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..." />
                    </div>
                    @if($errors->has('seo_hist_data'))
                    <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('seo_hist_data') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </fieldset>