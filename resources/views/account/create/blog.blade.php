<fieldset>
    <input type="hidden" name="blog_included" id="blog_included" value="{{ !empty($account->blog) || old('blog_included') == 'yes' ? 'yes' : 'no' }}" />

    <div class="form-group clearfix">
        <label for="blog_total_post" class="col-md-4 control-label">Total Blog Post *</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="blog_total_post" name="blog_total_post" value="{{ !empty(old('blog_total_post')) ? old('blog_total_post') : (!empty($blog) ? $blog['blog_total_post'] : '' ) }}" onkeypress="return isNumberKey(event);"/>
            
            @if ($errors->has('blog_total_post'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('blog_total_post') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="blog_monthly_post" class="col-md-4 control-label">Number of Blog Post Per Month</label>
            
        <div class="col-md-6">
            <input type="text" class="form-control" id="blog_monthly_post" name="blog_monthly_post" value="{{ !empty(old('blog_monthly_post')) ? old('blog_monthly_post') : (!empty($blog) ? $blog['blog_monthly_post'] : '' ) }}" onkeypress="return isNumberKey(event);" />
        
            @if ($errors->has('blog_monthly_post'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('blog_monthly_post') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="blog_content_brief" class="col-md-4 control-label">Content Brief and Writing Style *</label>
            
        <div class="col-md-6">
            <textarea rows="5" class="form-control" id="blog_content_brief" name="blog_content_brief">{{ !empty(old('blog_content_brief')) ? old('blog_content_brief') : (!empty($blog) ? $blog['blog_content_brief'] : '' ) }}</textarea>
            @if ($errors->has('blog_content_brief'))
                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('blog_content_brief') }}</span>
            @endif
        </div>
    </div>


    <div class="form-group clearfix">
        <label for="blog_hist_data" class="col-md-4 control-label">Booking Form <span class="error-p account">(Note: PDF files only)</span></label>
        <div class="col-md-6 text-left">
            <input type="radio" id="has_blog_hisdata" name="has_blog_hisdata" value="1" {{ !empty(old('has_blog_hisdata')) && old('has_blog_hisdata') == '1' ? 'checked' : (!empty($blog) && !empty($blog['blog_hist_data']) ? 'checked' : '') }} />Yes
            <input type="radio" id="has_blog_hisdata" name="has_blog_hisdata" value="0" {{ !empty(old('has_blog_hisdata')) ? (old('has_blog_hisdata') == '0' ? 'checked' : '' ) : (!empty($blog) ? (empty($blog['blog_hist_data']) ? 'checked' : '') : 'checked') }} />No
            <br>
        </div>
        <div class="col-md-12">
            <div class="blog-file-uploading {{ !empty(old('has_blog_hisdata')) ? (old('has_blog_hisdata') == '0' ? 'hidden' : '' ) : (empty($blog['blog_hist_data']) ? 'hidden' : '') }}">
                <div class="file-loading">
                    <input id="blog_hist_data" name="blog_hist_data[]" multiple type="file" class="file" data-allowed-file-extensions='["pdf", "doc", "docx"]' data-browse-on-zone-click="true" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..."/>
                </div>

                @if ($errors->has('blog_hist_data'))
                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('blog_hist_data') }}</span>
                @endif
            </div>
        </div>
    </div>
</fieldset>