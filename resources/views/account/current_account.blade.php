<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading lead-info" role="tab">
                <h4 class="panel-title">
                    <a role="button" aria-expanded="true">
                    Prospect Information
                    </a>
                </h4>
            </div>
            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Contract Number</label>
                            {!! ($user->role == 1 || in_array($user->userInfo->team->team_code, ['hr'])) ? '<div class="input-group">' : '<div class="form-box">' !!}

                                <input id="contract_number" type="text" class="form-control" name="contract_number" value="{{ !empty($account->contract->contract_number) ? $account->contract->contract_number : '' }}" readonly />
                                {!!  ($user->role == 1 || in_array($user->userInfo->team->team_code, ['hr'])) ? '<span class="input-group-addon" id="basic-unlock"><i data-unlockcontract="1" id="unlock-contract" class="fa fa-lock"></i></span>' : '' !!}
                    
                            </div>

                            @if ($errors->has('contract_number'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_number') }}</span>
                            @endif
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Contract Value</label>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ !empty($account->contract->currency) ? $account->contract->currency : 'SGD' }}</span>
                                    <input id="contract_value" type="text" class="form-control" name="account[contract][contract_value]" value="{{ !empty($account->contract->contract_value) ? $account->contract->contract_value : '' }}">
                                </div>
                                
                                @if ($errors->has('contract_value'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_value') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                                <div class="form-box">
                                <label for="contract_type">Contract Type</label>
                                <div class="select-box">
                                    <select class="form-control" name="account[contract][contract_type]">
                                        @foreach($contract_type as $row)
                                            <option value="{{ $row->systemcode }}" {{ !empty(old('contract_type')) && $row->systemcode == old('contract_type') ? 'selected' : (!empty($account->contract) && $account->contract->contract_type == $row->systemcode ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('contract_type'))
                                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contract_type') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                     
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Company</label>
                                <input id="company" type="text" class="form-control" name="account[contract][company]" value="{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }}" {{ $user->role == 1 || $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}>
                                @if ($errors->has('company'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('company') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Client Name</label>
                                <input id="client_name" type="text" class="form-control" name="account[contract][client_name]" value="{{ !empty($account->contract->f_name) ? $account->contract->f_name.' '.$account->contract->l_name : $account->prospect->lead->f_name.' '.$account->prospect->lead->l_name }}" {{ $user->role == 1 || $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}>
                                @if ($errors->has('client_name'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('client_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Client Number</label>
                                <input id="client_number" type="text" class="form-control" name="account[contract][client_number]" value="{{ !empty($account->contract->contact_number) ? $account->contract->contact_number : $account->prospect->lead->contact_number }}" {{  $user->role == 1 || $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}>
                                    @if ($errors->has('client_number'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('client_number') }}</span>
                                @endif
                            </div>
                        </div>
                      
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Client Email</label>
                                <input id="client_email" type="text" class="form-control" name="account[contract][email]" value="{{ !empty($account->contract->email) ? $account->contract->email : $account->prospect->lead->email }}" {{ $user->role == 1 || $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}> 
                                @if ($errors->has('client_email'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('client_email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Client Address</label>
                                <textarea id="street_address" name="account[prospect][street_address]" class="form-control" rows="5" {{ $user->role == 1 || $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}>{!! !empty($account->prospect->lead->street_address) ? $account->prospect->lead->street_address : '' !!}</textarea>
                                
                                @if ($errors->has('street_address'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('street_address') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Website URL</label>
                                <input id="website" type="text" class="form-control" name="account[prospect][website]" value="{{ $account->prospect->lead->website }}" {{ $account->acc_team == $user->emp_id || ($account->created_id == $user->userid && in_array($user->userInfo->team->team_code, ['busi','acc'])) ? '' : 'readonly' }}>
                                @if ($errors->has('website'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('website') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Website CMS/FTP Login</label>
                                <textarea id="web_login" name="account[additional][web_login]" class="form-control" rows="5">{!! !empty($account->additional->web_login) ? $account->additional->web_login : '' !!}</textarea>
                                @if ($errors->has('web_login'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('web_login') }}</span>
                                @endif
                            </div>
                        </div>

                       
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Industry</label>
                                <select class="selectpicker form-control" name="account[prospect][industry]" id="industry" data-live-search="true">
                                    @foreach($industries as $industry)
                                        <option value="{{ $industry->code }}" {{ !empty($account->prospect->lead->industry) ? ($account->prospect->lead->industry == $industry->code ? 'selected' : '') : '' }}>{{ $industry->title }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('industry'))
                                    <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('industry') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Contact Us Page URL</label>
                                <input id="contact_page_url" type="text" class="form-control" name="account[additional][contact_page_url]" value="{{ !empty($account->additional->contact_page_url) ?  $account->additional->contact_page_url : '' }}">
                                @if ($errors->has('contact_page_url'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('contact_page_url') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box">
                                <label>Thank You Page URL</label>
                                <input id="ty_page_url" type="text" class="form-control" name="account[additional][ty_page_url]" value="{{ !empty($account->additional->ty_page_url) ?  $account->additional->ty_page_url : '' }}">
                                @if ($errors->has('ty_page_url'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('ty_page_url') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-box">
                                <label>GA Required?</label>
                                <input type="radio" id="ga_required" name="account[additional][ga_required]" value="1" {{ $account->additional->ga_required == 1 ? 'checked' : '' }} />Yes
                                <input type="radio" id="ga_required" name="account[additional][ga_required]" value="0" {{ $account->additional->ga_required == 0 ? 'checked' : '' }} />No
                                <input class="form-control {{-- $account->additional->ga_required == 1 ? '' : 'hidden' --}}" type="text" name="account[additional][ga_remark]" value="{{ empty(old('ga_remark')) ? $account->additional->ga_remark : old('ga_remark') }}" />
                                @if ($errors->has('ga_required'))
                                <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('ga_required') }}</span>
                                @endif
                            </div>
                        </div>

                        @if($user->role == 1 || ($user->UserInfo->team->team_code == 'acc' && in_array($user->emp_id, $managers) ))
                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Need implementations from OOm?</label>
                                        <input type="radio" id="implementations" name="account[additional][implementations]" value="1" {{ $account->additional->implementations == 1 ? 'checked' : '' }}/>Yes
                                        <input type="radio" id="implementations" name="account[additional][implementations]" value="0" {{ $account->additional->implementations == 0 ? 'checked' : '' }}/>No
                                        <input class="form-control {{-- $account->additional->implementations == 1 ? '' : 'hidden' --}}" type="text" name="account[additional][implementations_remark]" value="{{ empty(old('implementations_remark')) ? $account->additional->implementations_remark : old('implementations_remark') }}" />
                                        @if ($errors->has('implementations'))
                                        <span class="error-p account"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('implementations') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-box">
                                        <label>Status</label>
                                        <div class="select-box">
                                            <select id="status" name="status" class="form-control">
                                            
                                            @foreach($statuses as $row)
                                                <option value="{{ $row->systemcode }}" {{ $row->systemcode == $account->status ? 'selected' : (!empty(old('status')) && $row->systemcode == old('status') ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                         @endif
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
    
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse-10" aria-expanded="true" class="svc-banner">
                            Services
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-10" class="panel-collapse collapse in" role="tabpanel">
                        
                        <div class="panel-body">
                            <div class="sem-included {{ !empty($account->sem) ? '' : (!empty(old('nature')) && in_array('NP001',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.sem')
                            </div>
                            <div class="fb-included {{ !empty($account->fb) ? '' : (!empty(old('nature')) && in_array('NP002',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.fb')
                            </div>
                            <div class="seo-included {{ !empty($account->seo) ? '' : (!empty(old('nature')) && in_array('NP003',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.seo')
                            </div>
                            <div class="web-included {{ !empty($account->web) ? '' : (!empty(old('nature')) && in_array('NP004',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.web')
                            </div>
                            <div class="baidu-included {{ !empty($account->baidu) ? '' : (!empty(old('nature')) && in_array('NP005',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.baidu')
                            </div>
                            <div class="weibo-included {{ !empty($account->weibo) ? '' : (!empty(old('nature')) && in_array('NP006',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.weibo')
                            </div>
                            <div class="wechat-included {{ !empty($account->wechat) ? '' : (!empty(old('nature')) && in_array('NP007',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.wechat')
                            </div>
                            <div class="blog-included {{ !empty($account->blog) ? '' : (!empty(old('nature')) && in_array('NP008',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.blog')
                            </div>
                            <div class="social-included {{ !empty($account->social) ? '' : (!empty(old('nature')) && in_array('NP009',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.social')
                            </div>
                            <div class="postpaid-included {{ !empty($account->postpaid) ? '' : (!empty(old('nature')) && in_array('NP010',old('nature')) ? '' : 'hidden') }}">
                                @include('account.edit.postpaid_sem')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom-buttons clearfix">
                <div class="buttons-box">
                    <button type="submit" class="cta-btn add-btn">Update Account</button>
                    <a href="{{ route('account.list') }}" class="cta-btn cancel-btn">Cancel</a>
                </div>
            </div>
            <br><br>

            