<div class="form-group clearfix">
    <label for="seo_duration" class="col-md-4 control-label">Campaign Duration</label>

    <div class="col-md-6">

        <p>
            @if(!empty($account->seo->seo_duration))
                {{ $account->seo->seo_duration != 'CD999' ? $account->seo->duration->systemdesc : $account->seo->seo_duration_other.' '.$account->seo->duration_interval->systemdesc  }}
            @else
                Not Specified
            @endif
        </p>
    </div>
</div>

<div class="form-group clearfix">
        <label for="seo_hist_data" class="col-md-4 control-label">Booking Form</label>
   
        <div class="col-md-6">
   
            @if(!empty($account->seo->seo_hist_data))
                @php 
                $fname = explode('|',$account->seo->seo_hist_data_fname);
                $hash = explode('|',$account->seo->seo_hist_data);
                @endphp
                @foreach($fname as $n => $row)
                <p><a href="{{ route('account.view.pdf',['seo',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                @endforeach
            @else
                Not Specified
            @endif
        </div>
    </div>
   

 <div class="form-group clearfix">
     <label for="seo_target_countries" class="col-md-4 control-label">Target Market</label>

     <div class="col-md-6">

         <p>
            @foreach(explode('|',$account->seo->seo_target_countries) as $row)
                @php $seo_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $seo_countries->country }} <br />
            @endforeach
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="keyword_themes" class="col-md-4 control-label">No. of Keywords</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->seo->keyword_themes))
                 {{ $account->seo->keyword_themes != 'NK999' ? $account->seo->keywords->systemdesc : $account->seo->keyword_themes_other }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="keyword_focus" class="col-md-4 control-label">Keyword Theme Focus</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->seo->keyword_focus))
                 {!! nl2br($account->seo->keyword_focus) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>
 
 <div class="form-group clearfix">
     <label for="keyword_focus" class="col-md-4 control-label">Keyword Maintenance Project</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->seo->keyword_maintenance))
                 {!! !is_int($account->seo->keyword_maintenance) ? nl2br($account->seo->keyword_maintenance) : 'No' !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="seo_campaign" class="col-md-4 control-label">Campaign Brief & Expectations</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->seo->seo_campaign))
                 {!! nl2br($account->seo->seo_campaign) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
        <label for="seo_total_fee" class="col-md-4 control-label">SEO Total Fee</label>
    
        <div class="col-md-6">
    
            <p>
              @if(!empty($account->seo->seo_total_fee))
                  {{ $account->seo->seo_currency.' '.$account->seo->seo_total_fee }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>

<div class="clearfix">&nbsp;</div>