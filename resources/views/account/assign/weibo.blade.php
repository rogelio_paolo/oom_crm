<div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Campaign Duration</label>
    
        <div class="col-md-6">
            <p>
                @if(!empty($account->weibo->weibo_duration))
                    {{ $account->weibo->weibo_duration != 'CD999' ? $account->weibo->campaign_duration->systemdesc : $account->weibo->weibo_duration_other.' '.$account->weibo->campaign_interval->systemdesc  }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign_datefrom" class="col-md-4 control-label">Campaign Start Date</label>
    
        <div class="col-md-6">
    
            <p>
                @if(!empty($account->weibo->weibo_campaign_datefrom))
                    {{ $account->weibo->weibo_campaign_datefrom }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign_dateto" class="col-md-4 control-label">Campaign End Date</label>
    
        <div class="col-md-6">
    
            <p>
                @if(!empty($account->weibo->weibo_campaign_dateto))
                    {{ $account->weibo->weibo_campaign_dateto }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="historical_data" class="col-md-4 control-label">Booking Form</label>
     
            <div class="col-md-6">
     
                @if(!empty($account->weibo->weibo_hist_data))
                     @php 
                     $fname = explode('|',$account->weibo->weibo_hist_data_fname);
                     $hash = explode('|',$account->weibo->weibo_hist_data);
                     @endphp
                     @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['weibo',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                     @endforeach
                @else
                    Not Specified
                @endif
            </div>
        </div>
    
    <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">Location</label>

        <div class="col-md-6">
            <p>
                @if(!empty($account->weibo->weibo_location))
                    {{ $account->weibo->weibo_location }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

   <div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Weibo Account User ID</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->weibo->weibo_account))
                    {{ $account->weibo->weibo_account }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign" class="col-md-4 control-label">Requires New Blue V Weibo Setup?</label>
 
        <div class="col-md-6">
            <p>
                @if($account->weibo->has_blue_v_weibo == '1')
                    Yes 
                <br/>
                @else
                    No
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="social_content_brief" class="col-md-4 control-label">Campaign Brief & Expectations</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->weibo->weibo_campaign))
                    {!! nl2br($account->weibo->weibo_campaign) !!}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign" class="col-md-4 control-label">Ad Scheduling</label>
 
        <div class="col-md-6">
 
            <p>
                @if($account->weibo->weibo_ad_scheduling == '1')
                Yes 
                <br/>
                {!! nl2br($account->weibo->weibo_ad_scheduling_remark) !!}
              @else
                No
              @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="budget" class="col-md-4 control-label">Total Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->weibo->weibo_budget))
                      {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->weibo->weibo_monthly_budget))
                      {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_monthly_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_daily_budget" class="col-md-4 control-label">Daily Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->weibo->weibo_daily_budget))
                      {{ $account->weibo->weibo_currency.' '.$account->weibo->weibo_daily_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>

    
    <div class="clearfix">&nbsp;</div>
 