<div class="form-group clearfix">
        <label for="historical_data" class="col-md-4 control-label">Booking Form</label>
 
        <div class="col-md-6">
 
            @if(!empty($account->blog->blog_hist_data))
                 @php 
                 $fname = explode('|',$account->blog->blog_hist_data_fname);
                 $hash = explode('|',$account->blog->blog_hist_data);
                 @endphp
                 @foreach($fname as $n => $row)
                    <p><a href="{{ route('account.view.pdf',['blog',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                 @endforeach
            @else
                Not Specified
            @endif
        </div>
    </div>
    
   <div class="form-group clearfix">
        <label for="blog_total_post" class="col-md-4 control-label">Total Number of Blog Post</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->blog->blog_total_post))
                    {{ $account->blog->blog_total_post }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="blog_monthly_post" class="col-md-4 control-label">Number of Blog Post Per Month</label>
    
        <div class="col-md-6">
            <p>
                @if(!empty($account->blog->blog_monthly_post))
                    {{ $account->blog->blog_monthly_post }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="media_channel" class="col-md-4 control-label">Content Brief and Writing Style</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->blog->blog_content_brief))
                    {!! nl2br($account->blog->blog_content_brief) !!}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>


    <div class="clearfix">&nbsp;</div>

    

   


 