<div class="form-group clearfix">
        <label for="fb_duration" class="col-md-4 control-label">Campaign Duration</label>
   
        <div class="col-md-6">
   
            <p>
                @if(!empty($account->fb->fb_duration))
                {{ $account->fb->fb_duration != 'CD999' ? $account->fb->duration->systemdesc : $account->fb->fb_duration_other.' '.$account->fb->duration_interval->systemdesc  }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
   
    <div class="form-group clearfix">
        <label for="fb_campaign_dt_from" class="col-md-4 control-label">Campaign Start Date</label>
   
        <div class="col-md-6">
   
            <p>
                @if(!empty($account->fb->fb_campaign_dt_from))
                    {{ $account->fb->fb_campaign_dt_from }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
   
    <div class="form-group clearfix">
        <label for="fb_campaign_dt_to" class="col-md-4 control-label">Campaign End Date</label>
   
        <div class="col-md-6">
   
            <p>
                @if(!empty($account->fb->fb_campaign_dt_to))
                    {{ $account->fb->fb_campaign_dt_to }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="fb_hist_data" class="col-md-4 control-label">Booking Form</label>
       
            <div class="col-md-6">
       
                @if(!empty($account->fb->fb_hist_data))
                   @php 
                   $fname = explode('|',$account->fb->fb_hist_data_fname);
                   $hash = explode('|',$account->fb->fb_hist_data);
                   @endphp
                   @foreach($fname as $n => $row)
                    <p><a href="{{ route('account.view.pdf',['fb',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                   @endforeach
                @else
                    Not Specified
                @endif
            </div>
        </div>
    
 <div class="form-group clearfix">
     <label for="target_countries" class="col-md-4 control-label">Target Market</label>

     <div class="col-md-6">

         <p>
            @foreach(explode('|',$account->fb->fb_target_country) as $row)
            @php $fb_countries = \App\Country::where('code',$row)->first() @endphp
            {{ $fb_countries->country }} <br />
            @endforeach
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
        <label for="fb_list_account" class="col-md-4 control-label">List Account as</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->fb->fb_list_account))
                  <strong>{{ $account->fb->listing->systemdesc }}</strong>
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>
 
 <div class="form-group clearfix">
     <label for="fb_payment" class="col-md-4 control-label">Media Budget Payment</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->fb_payment))
                 {{ $account->fb->mode->systemdesc }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_media_channel" class="col-md-4 control-label">Media Channel</label>

     <div class="col-md-6">
         <p>
             @if(!empty($account->fb->fb_media_channel))
                 @foreach(explode('|',$account->fb->fb_media_channel) as $row)
                      <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                 @endforeach
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_campaign" class="col-md-4 control-label">Campaign Brief & Expectation</label>

     <div class="col-md-6">

         <p>
            @if(!empty($account->fb->fb_campaign))
                 {!! nl2br($account->fb->fb_campaign) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="total_budget" class="col-md-4 control-label">Total Campaign Budget</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->total_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->total_budget }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->monthly_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->monthly_budget }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="daily_budget" class="col-md-4 control-label">Daily Campaign Budget</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->daily_budget))
                 {{ $account->fb->fb_currency.' '.$account->fb->daily_budget }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_campaign_dt_to" class="col-md-4 control-label">Gender</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->gender))
                 {{ $account->fb->gender == 'A' ? 'All' : ($account->fb->gender == 'M' ? 'Male' : 'Female') }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>
 
 <div class="form-group clearfix">
     <label for="fb_campaign_dt_to" class="col-md-4 control-label">Age Group</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->age_from))
                 {{ $account->fb->age_from }} to {{ $account->fb->age_to }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>
 
 <div class="form-group clearfix">
     <label for="fb_campaign_dt_to" class="col-md-4 control-label">Interest</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->fb->interest))
                 {!! nl2br($account->fb->interest) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>
 
 <div class="clearfix">&nbsp;</div>
