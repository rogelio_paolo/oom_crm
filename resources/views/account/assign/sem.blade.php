    <div class="form-group clearfix">
        <label for="duration" class="col-md-4 control-label">Campaign Duration</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->sem->duration))
                  {{ $account->sem->duration != 'CD999' ? $account->sem->campaign_duration->systemdesc : $account->sem->duration_other.' '.$account->sem->campaign_interval->systemdesc  }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="campaign_datefrom" class="col-md-4 control-label">Campaign Start Date</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->sem->campaign_datefrom))
                  {{ $account->sem->campaign_datefrom }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="campaign_dateto" class="col-md-4 control-label">Campaign End Date</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->sem->campaign_dateto))
                  {{ $account->sem->campaign_dateto }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="historical_data" class="col-md-4 control-label">Booking Form</label>
        
            <div class="col-md-6">
        
                @if(!empty($account->sem->historical_data))
                        @php 
                        $fname = explode('|',$account->sem->historical_data_fname);
                        $hash = explode('|',$account->sem->historical_data);
                        @endphp
                        @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['sem',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                        @endforeach
                @else
                    Not Specified
                @endif
            </div>
        </div>

   <div class="form-group clearfix">
       <label for="target_market" class="col-md-4 control-label">Target Market</label>

       <div class="col-md-6">

           <p>
                @foreach(explode('|',$account->sem->target_market) as $row)
                @php $sem_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $sem_countries->country }} <br />
                @endforeach
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
        <label for="list_account" class="col-md-4 control-label">List Account as</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->sem->list_account))
                  <strong>{{ $account->sem->listing->systemdesc }}</strong>
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>

   <div class="form-group clearfix">
       <label for="payment_method" class="col-md-4 control-label">Media Budget Payment</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->payment_method))
                 {{ $account->sem->mode->systemdesc }}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="media_channel" class="col-md-4 control-label">Media Channels </label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->media_channel))
                 @foreach(explode('|',$account->sem->media_channel) as $row)
                      <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                 @endforeach
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="campaign" class="col-md-4 control-label">Campaign Brief & Expectations</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->campaign))
                 {!! nl2br($account->sem->campaign) !!}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
        <label for="campaign" class="col-md-4 control-label">Ad Scheduling</label>
 
        <div class="col-md-6">
 
            <p>
                @if($account->sem->ad_scheduling == '1')
                Yes 
                <br/>
                {!! nl2br($account->sem->ad_scheduling_remark) !!}
              @else
                No
              @endif
            </p>
        </div>
    </div>

   <div class="form-group clearfix">
       <label for="budget" class="col-md-4 control-label">Total Campaign Budget</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->budget))
                 {{ $account->sem->currency.' '.$account->sem->budget }}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="google_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->google_monthly_budget))
                 {{ $account->sem->currency.' '.$account->sem->google_monthly_budget }}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="google_daily_budget" class="col-md-4 control-label">Daily Campaign Budget</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->sem->google_daily_budget))
                 {{ $account->sem->currency.' '.$account->sem->google_daily_budget }}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
    <label for="campaign_dateto" class="col-md-4 control-label">Account Management Fee</label>

    <div class="col-md-6">

        <p>
          @if(!empty($account->sem->am_fee))
              {{ $account->sem->currency.' '.$account->sem->am_fee }}
          @else
              Not Specified
          @endif
        </p>
    </div>
</div>

   
   <div class="clearfix">&nbsp;</div>
    
