<div class="form-group clearfix">
        <label for="wechat_historical_data" class="col-md-4 control-label">Booking Form</label>
 
        <div class="col-md-6">
 
            @if(!empty($account->wechat->wechat_hist_data))
                 @php 
                 $fname = explode('|',$account->wechat->wechat_hist_data_fname);
                 $hash = explode('|',$account->wechat->wechat_hist_data);
                 @endphp
                 @foreach($fname as $n => $row)
                    <p><a href="{{ route('account.view.pdf',['wechat',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                 @endforeach
            @else
                Not Specified
            @endif
        </div>
    </div>
    
   <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">WeChat OA Setup?</label>

        <div class="col-md-6">
            <p>
                @if($account->wechat->has_wechat_type == 1)
                    Yes
                    <br/>
                @else
                    No
                @endif
            </p>
        </div>
    </div>

    @if (!empty($account->wechat->has_wechat_type))
    <div class="form-group clearfix">
            <label for="social_duration" class="col-md-4 control-label">WeChat Type</label>
    
            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_type))
                        {{ $account->wechat->wechat_type == 'serviced' ? 'Serviced' : 'Subscription' }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="campaign" class="col-md-4 control-label">Requires Menu Setup?</label>
    
            <div class="col-md-6">
                <p>
                    @if($account->wechat->has_wechat_menu_setup == '1')
                        Yes 
                    <br/>
                    @else
                        No
                    @endif
                </p>
            </div>
        </div>
    @endif

    <div class="form-group clearfix">
        <label for="campaign" class="col-md-4 control-label">Requires WeChat Advertising?</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->wechat->wechat_location))
                    Yes 
                <br/>
                @else
                    No
                @endif
            </p>
        </div>
    </div>

    @if(!empty($account->wechat->wechat_location))
        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">WeChat Advertising Type</label>
        
            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_advertising_type))
                        @foreach(explode('|',$account->wechat->wechat_advertising_type) as $row)
                            <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                        @endforeach
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Location</label>

            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_location))
                        {{ $account->wechat->wechat_location }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="fb_campaign_dt_to" class="col-md-4 control-label">Age Group</label>
    
            <div class="col-md-6">
    
                <p>
                    @if(!empty($account->wechat->wechat_age_from))
                        {{ $account->wechat->wechat_age_from }} to {{ $account->wechat->wechat_age_to }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Marital Status</label>

            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_marital))
                        {{ $account->wechat->wechat_marital }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Gender</label>

            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_gender))
                        {{ $account->wechat->wechat_gender == 'M' ? 'Male' : ($account->wechat->wechat_gender == 'F' ? 'Female' : 'Not Specified') }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Handphone Device</label>

            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_handphone))
                        {{ $account->wechat->wechat_handphone == 'android' ? 'Android' : 'iOS' }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>
        
        <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Education</label>

            <div class="col-md-6">
                <p>
                    @if(!empty($account->wechat->wechat_education))
                        {{ $account->wechat->wechat_education }}
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>
    @endif
    
   
    <div class="clearfix">&nbsp;</div>
 