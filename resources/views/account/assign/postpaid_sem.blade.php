<div class="form-group clearfix">
        <label for="duration" class="col-md-4 control-label">Campaign Duration</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->postpaid->postpaid_duration))
                  {{ $account->postpaid->postpaid_duration != 'CD999' ? $account->postpaid->campaign_duration->systemdesc : $account->postpaid->postpaid_duration_other.' '.$account->postpaid->campaign_interval->systemdesc  }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="campaign_datefrom" class="col-md-4 control-label">Campaign Start Date</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->postpaid->postpaid_campaign_datefrom))
                  {{ $account->postpaid->postpaid_campaign_datefrom }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="campaign_dateto" class="col-md-4 control-label">Campaign End Date</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->postpaid->postpaid_campaign_dateto))
                  {{ $account->postpaid->postpaid_campaign_dateto }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>

   <div class="form-group clearfix">
       <label for="target_market" class="col-md-4 control-label">Media Budget Utilized</label>

       <div class="col-md-6">
           <p>
              @if(!empty($account->postpaid->postpaid_media_budget))
                 {{ '$ ' .$account->postpaid->postpaid_media_budget }}
              @else
                Not Specified
              @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
       <label for="payment_method" class="col-md-4 control-label">AM Fee</label>

       <div class="col-md-6">

           <p>
             @if(!empty($account->postpaid->postpaid_am_fee))
                 {{ $account->postpaid->postpaid_am_fee . '%' }}
             @else
                 Not Specified
             @endif
           </p>
       </div>
   </div>

   <div class="form-group clearfix">
        <label for="payment_method" class="col-md-4 control-label">AM Fee Amount</label>
 
        <div class="col-md-6">
 
            <p>
              @if(!empty($account->postpaid->postpaid_am_fee_amount))
                  {{ '$ ' . $account->postpaid->postpaid_am_fee_amount }}
              @else
                  Not Specified
              @endif
            </p>
        </div>
    </div>


   <div class="form-group clearfix">
       <label for="google_monthly_budget" class="col-md-4 control-label">Is Media Budget paid through OOm?</label>

       <div class="col-md-6">

           <p>
             @if($account->postpaid->is_media_budget_paid == 1)
                 Yes - {{ '$ '. $account->postpaid->postpaid_total }} (Media Budget Utilized + AM Fee Amount)
             @else
                 No - {{ '$ ' . $account->postpaid->postpaid_am_fee_amount }} (AM Fee Amount)
             @endif
           </p>
       </div>
   </div>
   
   <div class="clearfix">&nbsp;</div>
    
