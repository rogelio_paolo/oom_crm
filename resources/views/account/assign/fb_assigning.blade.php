<div class="col-md-12">
    <div class="panel panel-primary assign-border" >
        <div class="panel-heading assign-background">
          <h3 class="panel-title assign-padding">Assign FB Team Members</h3>
        </div>
        <div class="panel-body">
            <label for="fb_strategist_assigned" class="col-md-4 control-label">FB Strategist </label>

            <div class="col-md-6">
                <div class="select-box">
                    <input type="hidden" name="accountfbid" value="{{ $account->fb_id }}">

                    @php 
                        $current_fb_strategist = $account->fb->fb_strategist_assigned; 
                    @endphp

                    <select id="fb_strategist_assigned" name="fb_strategist_assigned" class="selectpicker form-control">
                        <option value="" {{ !isset($current_fb_strategist) ? 'selected' : '' }}>None Selected</option>
                        @foreach($strategists as $i => $row)
                            <option value="{{ $row['emp_id'] }}" @if(isset($current_fb_strategist)) {{ $row['emp_id'] == $current_fb_strategist ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <label for="asst_fb_strategist_assigned" class="col-md-4 control-label">Assistant FB Strategist (Optional)</label>
            <div class="col-md-6">
                <div class="select-box">
                    @php 
                        $current_asst_fb_strategist = $account->fb->asst_fb_strategist_assigned; 
                    @endphp

                    <select id="asst_fb_strategist_assigned" name="asst_fb_strategist_assigned" class="selectpicker form-control">
                        <option value="" {{ !isset($current_asst_fb_strategist) ? 'selected' : '' }}>None Selected</option>
                        @foreach($strategists as $i => $row)
                            <option value="{{ $row['emp_id'] }}" @if(isset($current_asst_fb_strategist)) {{ $row['emp_id'] == $current_asst_fb_strategist ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <input id="btn-assign-fb" type="button" class="btn btn-success assign-btn" {{ !empty($current_fb_strategist) || !empty($current_asst_fb_strategist) ? '' : 'disabled' }} value="Assign members">
            </div>
        </div>

        
    </div>
</div>