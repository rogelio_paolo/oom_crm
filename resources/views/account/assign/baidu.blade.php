<div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Campaign Duration</label>
    
        <div class="col-md-6">
            <p>
                @if(!empty($account->baidu->baidu_duration))
                    {{ $account->baidu->baidu_duration != 'CD999' ? $account->baidu->campaign_duration->systemdesc : $account->baidu->baidu_duration_other.' '.$account->baidu->campaign_interval->systemdesc  }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign_datefrom" class="col-md-4 control-label">Campaign Start Date</label>
    
        <div class="col-md-6">
    
            <p>
                @if(!empty($account->baidu->baidu_campaign_datefrom))
                    {{ $account->baidu->baidu_campaign_datefrom }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign_dateto" class="col-md-4 control-label">Campaign End Date</label>
    
        <div class="col-md-6">
    
            <p>
                @if(!empty($account->baidu->baidu_campaign_dateto))
                    {{ $account->baidu->baidu_campaign_dateto }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="historical_data" class="col-md-4 control-label">Booking Form</label>
     
            <div class="col-md-6">
     
                @if(!empty($account->baidu->baidu_hist_data))
                     @php 
                     $fname = explode('|',$account->baidu->baidu_hist_data_fname);
                     $hash = explode('|',$account->baidu->baidu_hist_data);
                     @endphp
                     @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['baidu',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                     @endforeach
                @else
                    Not Specified
                @endif
            </div>
        </div>

   <div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Baidu Account User ID</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->baidu->baidu_account))
                    {{ $account->baidu->baidu_account }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">Target Market</label>
    
        <div class="col-md-6">
            <p>
                @foreach(explode('|',$account->baidu->baidu_target_market) as $row)
                @php $baidu_countries = \App\Country::where('code',$row)->first() @endphp
                {{ $baidu_countries->country }} <br />
                @endforeach
            </p>
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">City</label>
    
        <div class="col-md-6">
            <p>
                @if(!empty($account->baidu->baidu_city))
                    {{ $account->baidu->baidu_city }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="social_monthly_post" class="col-md-4 control-label">Media Channels</label>
        
            <div class="col-md-6">
                <p>
                    @if(!empty($account->baidu->baidu_media_channel))
                        @foreach(explode('|',$account->baidu->baidu_media_channel) as $row)
                            <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                        @endforeach
                    @else
                        Not Specified
                    @endif
                </p>
            </div>
        </div>

    <div class="form-group clearfix">
        <label for="social_content_brief" class="col-md-4 control-label">Campaign Brief & Expectations</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->baidu->baidu_campaign))
                    {!! nl2br($account->baidu->baidu_campaign) !!}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="campaign" class="col-md-4 control-label">Ad Scheduling</label>
 
        <div class="col-md-6">
 
            <p>
                @if($account->baidu->baidu_ad_scheduling == '1')
                Yes 
                <br/>
                {!! nl2br($account->baidu->baidu_ad_scheduling_remark) !!}
              @else
                No
              @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="budget" class="col-md-4 control-label">Total Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->baidu->baidu_budget))
                      {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_monthly_budget" class="col-md-4 control-label">Monthly Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->baidu->baidu_monthly_budget))
                      {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_monthly_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>
     
        <div class="form-group clearfix">
            <label for="google_daily_budget" class="col-md-4 control-label">Daily Campaign Budget</label>
     
            <div class="col-md-6">
     
                <p>
                  @if(!empty($account->baidu->baidu_daily_budget))
                      {{ $account->baidu->baidu_currency.' '.$account->baidu->baidu_daily_budget }}
                  @else
                      Not Specified
                  @endif
                </p>
            </div>
        </div>
 
    
    <div class="clearfix">&nbsp;</div>
 