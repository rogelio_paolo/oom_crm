
   <div class="form-group clearfix">
        <label for="social_duration" class="col-md-4 control-label">Campaign Duration</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->social->social_duration))
                    {{ $account->social->social_duration != 'CD999' ? $account->social->campaign_duration->systemdesc : $account->social->social_duration_other.' '.$account->social->campaign_interval->systemdesc  }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
            <label for="historical_data" class="col-md-4 control-label">Booking Form</label>
     
            <div class="col-md-6">
     
                @if(!empty($account->social->social_hist_data))
                     @php 
                     $fname = explode('|',$account->social->social_hist_data_fname);
                     $hash = explode('|',$account->social->social_hist_data);
                     @endphp
                     @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['social',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                     @endforeach
                @else
                    Not Specified
                @endif
            </div>
        </div>
 
    <div class="form-group clearfix">
        <label for="social_monthly_post" class="col-md-4 control-label">Number of Blog Post Per Month</label>
    
        <div class="col-md-6">
            <p>
                @if(!empty($account->social->social_monthly_post))
                    {{ $account->social->social_monthly_post }}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>
 
    <div class="form-group clearfix">
        <label for="social_content_brief" class="col-md-4 control-label">Content Brief and Writing Style</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->social->social_content_brief))
                    {!! nl2br($account->social->social_content_brief) !!}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

    <div class="form-group clearfix">
        <label for="social_content_brief" class="col-md-4 control-label">Any FB Media Budget to boost post?</label>
 
        <div class="col-md-6">
            <p>
                @if(!empty($account->social->social_media_budget))
                    Yes - ${{ $account->social->social_media_budget }}
                @else
                    No
                @endif
            </p>
        </div>
    </div>

    
    <div class="clearfix">&nbsp;</div>
 