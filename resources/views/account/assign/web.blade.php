<div class="form-group clearfix">
        <label for="web_hist_data" class="col-md-4 control-label">Booking Form</label>
    
        <div class="col-md-6">
    
            @if(!empty($account->web->web_hist_data))
                @php 
                $fname = explode('|',$account->web->web_hist_data_fname);
                $hash = explode('|',$account->web->web_hist_data);
                @endphp
                @foreach($fname as $n => $row)
                <p><a href="{{ route('account.view.pdf',['web',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                @endforeach
            @else
                Not Specified
            @endif
        </div>
    </div>
    
 <div class="form-group clearfix">
     <label for="target_countries" class="col-md-4 control-label">Project Type</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->project_type))
                 {{ $account->web->project->systemdesc }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_media_channel" class="col-md-4 control-label">No. of Pages</label>

     <div class="col-md-6">
         <p>
             @if(!empty($account->web->pages))
                 {{ $account->web->pages }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_campaign" class="col-md-4 control-label">Objective</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->objective))
                 {!! nl2br($account->web->objective) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
    <label for="fb_campaign" class="col-md-4 control-label">Brief information about the Company</label>

    <div class="col-md-6">

        <p>
            @if(!empty($account->web->company_brief))
                {!! nl2br($account->web->company_brief) !!}
            @else
                Not Specified
            @endif
        </p>
    </div>
</div>

<div class="form-group clearfix">
        <label for="fb_campaign" class="col-md-4 control-label">Target Audience</label>
    
        <div class="col-md-6">
    
            <p>
                @if(!empty($account->web->target_audience))
                    {!! nl2br($account->web->target_audience) !!}
                @else
                    Not Specified
                @endif
            </p>
        </div>
    </div>

 <div class="form-group clearfix">
     <label for="total_budget" class="col-md-4 control-label">Existing Website (URL) </label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->existing_web_url))
                 {{ $account->web->existing_web_url }}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="monthly_budget" class="col-md-4 control-label">Preferred Reference Website</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->reference_web))
                 {!! nl2br($account->web->reference_web) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="daily_budget" class="col-md-4 control-label">Look and Feel</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->web_ui))
                 {!! nl2br($account->web->web_ui) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_duration" class="col-md-4 control-label">Color Scheme</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->color_scheme))
                 {!! nl2br($account->web->color_scheme) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 <div class="form-group clearfix">
     <label for="fb_campaign_dt_from" class="col-md-4 control-label">Special Functionality</label>

     <div class="col-md-6">

         <p>
             @if(!empty($account->web->functionality))
                 {!! nl2br($account->web->functionality) !!}
             @else
                 Not Specified
             @endif
         </p>
     </div>
 </div>

 
 <div class="clearfix">&nbsp;</div>
