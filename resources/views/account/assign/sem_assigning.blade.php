<div class="col-md-12">
    <div class="panel panel-primary assign-border" >
        <div class="panel-heading assign-background">
          <h3 class="panel-title assign-padding">Assign SEM Team Members</h3>
        </div>
        <div class="panel-body">
            <label for="sem_strategist_assigned" class="col-md-4 control-label">SEM Strategist </label>

            <div class="col-md-6">
                <div class="select-box">
                    <input type="hidden" name="accountsemid" value="{{ $account->ppc_id }}">

                    @php 
                        $current_sem_strategist = $account->sem->sem_strategist_assigned; 
                    @endphp

                    <select id="sem_strategist_assigned" name="sem_strategist_assigned" class="selectpicker form-control">
                        <option value="" {{ !isset($current_sem_strategist) ? 'selected' : '' }}>None Selected</option>
                        @foreach($strategists as $i => $row)
                            <option value="{{ $row['emp_id'] }}" @if(isset($current_sem_strategist)) {{ $row['emp_id'] == $current_sem_strategist ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <label for="asst_sem_strategist_assigned" class="col-md-4 control-label">Assistant SEM Strategist (Optional)</label>
            <div class="col-md-6">
                <div class="select-box">
                    @php 
                        $current_asst_sem_strategist = $account->sem->asst_sem_strategist_assigned; 
                    @endphp

                    <select id="asst_sem_strategist_assigned" name="asst_sem_strategist_assigned" class="selectpicker form-control">
                        <option value="" {{ !isset($current_asst_sem_strategist) ? 'selected' : '' }}>None Selected</option>
                        @foreach($strategists as $i => $row)
                            <option value="{{ $row['emp_id'] }}" @if(isset($current_asst_sem_strategist)) {{ $row['emp_id'] == $current_asst_sem_strategist ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <input id="btn-assign-sem" type="button" class="btn btn-success assign-btn" {{ !empty($current_sem_strategist) || !empty($current_asst_sem_strategist) ? '' : 'disabled' }} value="Assign members">
            </div>
        </div>

        
    </div>
</div>