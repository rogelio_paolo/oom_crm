@extends('layouts.master_v2')
@section('title')
    Crm - Edit {{ !empty($account->contract->company) ? $account->contract->company."'s " : $account->prospect->lead->company . "'s "}} account
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
{{-- <link rel="stylesheet" href="{{ asset('css/fileinput.min.css') }}" /> --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
@endsection
@section('main_content')

@include('includes.accountmodal')

    <div class="back-btn">
        <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
            <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
        </a>
    </div>

<div class="container pull-left">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <ol class="breadcrumb bg-none" >
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('account.list') }}">Accounts</a></li>
                <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company}} - Edit</li>
            </ol>
        </div>
    </div>
</div>
    

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="has-lead-note">
        <form class="outer-data-box" method="POST" action="{{ route('account.update') }}" enctype="multipart/form-data">
            <input type="hidden" id="accountid" name="accountid" value="{{ $account->id }}" />
            <input type="hidden" name="prospect_included" value="yes" />
            <input type="hidden" name="contract_included" value="yes" />
            <input type="hidden" name="additional_included" value="yes" />
            {{ csrf_field() }}
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <div class="clearfix">
                    <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Edit Account</span>
                    <div class="data-search-box">
                        <span>
                            Services: <em>Add more Services?</em>
                            <div class="select-box">
                                <select id="nature" name="nature[]" class="multiselect-ui form-control" multiple="multiple">
                                @php $package = explode('|',$account->nature) @endphp
                                @foreach($nature as $row)
                                <option value="{{ $row->systemcode }}" {{ in_array($row->systemcode,$package) ? 'selected' : (!empty(old('nature')) && in_array($row->systemcode,old('nature')) ? 'selected' : '') }}>{{ $row->systemdesc }}</option>
                                @endforeach
                                </select>
                            </div>
                        </span>
                    </div>
                    </div>
                </div>

                <div class="account-owner-info">
                    <p class="account-owners">
                        Business Manager : {{ $account->prospect->sales->UserInfo->f_name.' '.$account->prospect->sales->UserInfo->l_name }}
                    </p>
                    <p class="account-owners">
                        Main Account Holder : {{ empty($account->acc_assigned) ? 'None' : $account->holder->UserInfo->f_name.' '.$account->holder->UserInfo->l_name }}
                    </p>
                    <p class="account-owners">
                        Assistant Account Holder : {{ empty($account->acc_assistant) ? 'None' : $account->asst_holder->UserInfo->f_name.' '.$account->asst_holder->UserInfo->l_name }}
                    </p>
                    <br>

                    <!-- SEM Members -->
                    @if(!empty($account->sem))
                        @if(!empty($account->sem->sem_strategist_assigned))
                            @php $sem_strategist = \App\Employee::where('emp_id','=',$account->sem->sem_strategist_assigned)->first(); @endphp
                            <p class="account-owners">SEM Strategist: {{ $sem_strategist->f_name. ' ' . $sem_strategist->l_name }}</p>
                        @endif
                        @if(!empty($account->sem->asst_sem_strategist_assigned))
                            @php $asst_sem_strategist = \App\Employee::where('emp_id','=',$account->sem->asst_sem_strategist_assigned)->first(); @endphp
                            <p class="account-owners">Assistant SEM Strategist: {{ $asst_sem_strategist->f_name. ' ' . $asst_sem_strategist->l_name }}</p>
                        @endif
                    @endif

                    <!-- FB Members -->
                    @if(!empty($account->fb))
                        @if(!empty($account->fb->fb_strategist_assigned))
                            @php $fb_strategist = \App\Employee::where('emp_id','=',$account->fb->fb_strategist_assigned)->first(); @endphp
                            <p class="account-owners">FB Strategist: {{ $fb_strategist->f_name. ' ' . $fb_strategist->l_name }}</p>
                        @endif
                        @if(!empty($account->fb->asst_fb_strategist_assigned))
                            @php $asst_fb_strategist = \App\Employee::where('emp_id','=',$account->fb->asst_fb_strategist_assigned)->first(); @endphp
                            <p class="account-owners">Assistant FB Strategist: {{ $asst_fb_strategist->f_name. ' ' . $asst_fb_strategist->l_name }}</p>
                        @endif
                    @endif

                    <!-- FB Members -->
                    @if(!empty($account->seo))
                        @if(!empty($account->seo->seo_strategist_assigned))
                            @php $seo_strategist = \App\Employee::where('emp_id','=',$account->seo->seo_strategist_assigned)->first(); @endphp
                            <p class="account-owners">SEO Strategist: {{ $seo_strategist->f_name. ' ' . $seo_strategist->l_name }}</p>
                        @endif
                        @if(!empty($account->seo->asst_seo_strategist_assigned))
                            @php $asst_seo_strategist = \App\Employee::where('emp_id','=',$account->seo->asst_seo_strategist_assigned)->first(); @endphp
                            <p class="account-owners">Assistant SEO Strategist: {{ $asst_seo_strategist->f_name. ' ' . $asst_seo_strategist->l_name }}</p>
                        @endif
                        @if(!empty($account->seo->seo_link_builder_assigned))
                            @php $seo_link = \App\Employee::where('emp_id','=',$account->seo->seo_link_builder_assigned)->first(); @endphp
                            <p class="account-owners">SEO Link Builder: {{ $seo_link->f_name. ' ' . $seo_link->l_name }}</p>
                        @endif
                        @if(!empty($account->seo->asst_seo_link_builder_assigned))
                            @php $asst_seo_link = \App\Employee::where('emp_id','=',$account->seo->asst_seo_link_builder_assigned)->first(); @endphp
                            <p class="account-owners">Assistant SEO Link Builder: {{ $asst_seo_link->f_name. ' ' . $asst_seo_link->l_name }}</p>
                        @endif
                        @if(!empty($account->seo->seo_content_team))
                            @php 
                                $sc = array();
                                foreach(explode('|', $account->seo->seo_content_team) as $row) {
                                    $seo_content = \App\Employee::where('emp_id','=',$row)->first(); 
                                    $sc[] = $seo_content->f_name . ' ' . $seo_content->l_name;
                                }
                            @endphp
                            <p class="account-owners"> SEO Content Team: {{ implode(', ', $sc) }}</p>
                        @endif
                    @endif

                    <!-- WEB Members -->
                    @if(!empty($account->web))
                        @if(!empty($account->web->dev_team))
                            @php 
                                $wt = array();
                                foreach(explode('|', $account->web->dev_team) as $row) {
                                    $dev = \App\Employee::where('emp_id','=',$row)->first(); 
                                    $wt[] = $dev->f_name . ' ' . $dev->l_name;
                                }
                            @endphp
                            <p class="account-owners"> Web Development Team: {{ implode(', ', $wt) }}</p>
                        @endif
                        @if(!empty($account->web->dev_content_team))
                            @php 
                                $wct = array();
                                foreach(explode('|', $account->web->dev_content_team) as $row) {
                                    $dev_content = \App\Employee::where('emp_id','=',$row)->first(); 
                                    $wct[] = $dev_content->f_name . ' ' . $dev_content->l_name;
                                }
                            @endphp
                            <p class="account-owners"> Web Development Content Team: {{ implode(', ', $wct) }}</p>
                        @endif
                    @endif

                    <!-- BLOG CONTENT Members -->
                    @if(!empty($account->blog))
                        @if(!empty($account->blog->blog_content_team))
                            @php 
                                $bct = array();
                                foreach(explode('|', $account->blog->blog_content_team) as $row) {
                                    $blog_content = \App\Employee::where('emp_id','=',$row)->first(); 
                                    $bct[] = $blog_content->f_name . ' ' . $blog_content->l_name;
                                }
                            @endphp
                            <p class="account-owners"> Blog Content Team: {{ implode(', ', $bct) }}</p>
                        @endif
                    @endif

                    <!-- SOCIAL CONTENT Members -->
                    @if(!empty($account->social))
                        @if(!empty($account->social->social_content_team))
                            @php 
                                $sct = array();
                                foreach(explode('|', $account->social->social_content_team) as $row) {
                                    $social_content = \App\Employee::where('emp_id','=',$row)->first(); 
                                    $sct[] = $social_content->f_name . ' ' . $social_content->l_name;
                                }
                            @endphp
                            <p class="account-owners"> Social Media Content Team: {{ implode(', ', $sct) }}</p>
                        @endif
                    @endif
                </div>

               
                <div class="pull-right">
                    <div class="text-right">
                        <span class="span-contract-num {{ !empty($account->contract->contract_number) ? 'active-c' : 'past-c' }}"> Contract #: {{ !empty($account->contract) ? $account->contract->contract_number : 'Not Specified' }} </span>
                    </div>
                    
                    @if(!empty($account->updated_by))
                        <p class="account-updated-by">
                            Last updated by {{ $account->editor->UserInfo->f_name.' '.$account->editor->UserInfo->l_name }} on {{ date('F d, Y  g:i A',strtotime($account->updated_at)) }} 
                        </p>
                    @endif
                </div>
                

                @if(count($account_hist) > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-success">
                                <div class="panel-heading">
                                        <ul class="nav nav-tabs">
                                            <li><a href="#tab1info" data-toggle="tab">Account History</a></li>
                                            <li class="active"><a href="#tab2info" data-toggle="tab">Current Account</a></li>
                                        </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="tab1info">
                                            @include('account.history_account')
                                        </div>
                                        <div class="tab-pane fade in active" id="tab2info">
                                            @include('account.current_account')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    @include('account.current_account')
                @endif
            </div>
        </form>

        <form class="outer-data-box" method="post" action="{{ route('lead.note.create') }}" enctype="multipart/form-data">
            <i id="loader" class="loader leadnotes hidden"></i>
            {{ csrf_field() }}
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-edit" aria-hidden="true"></i> Lead Notes</span>
                    <div class="data-search-box">
                        Filter By:
                        <div class="select-box">
                            <select class="form-control" id="filterLeadNotes" name="filterLeadNotes">
                                <option value="all" selected>All</option>
                                <option value="date">Date</option>
                                <option value="commentor">Commentor</option>
                            </select>
                        </div>
                        <div id="filterButtons" class="form-inline pull-right graph-filter">
                            <button class="btn btn-default" id="FilterNotesAll"> FILTER </button>
                        </div>
                        <div id="filter-by-date" class="form-inline pull-right graph-filter hidden">
                        From <input type="text" class="form-control datepicker" id="filterDateFrom" name="filterDateFrom" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                        To <input type="text" class="form-control datepicker" id="filterDateTo" name="filterDateTo" value="{{ date('Y-m-d') }}" data-date-format="yyyy-mm-dd" readonly>
                        <button class="btn btn-default" id="FilterNotesByDate"> FILTER </button>
                        </div>
                        <div id="filter-by-commentor" class="form-inline pull-right graph-filter hidden">
                            <div class="select-box">
                                <select class="form-control" id="lead-commentors" name="filterNotes">
                                    @foreach($commentors as $row)
                                        <option value="{{ $row->userid }}">{{ $row->user->UserInfo->f_name.' '.$row->user->UserInfo->l_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-default" id="FilterNotesByCommentor"> FILTER </button>
                        </div>
                    </div>
                </div>
                <div class="lead-notes" >
                        
                        @forelse($notes as $row)
                            <div class="lead-note">
                                <span> 
                                    <p>{{ $row->user->userInfo->f_name.' '.$row->user->userInfo->l_name }}</p> 
                                    <p>{{ date_format($row->created_at,'Y-m-d h:i A') }}</p>
                                </span>
                                
                                @if ($row->userid == $user->userid)
                                    <span class="pull-right">
                                    <a id="edit-note" data-editnote="{{ $row->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                                    <a id="delete-note" data-delnote="{{ $row->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                    </span>
                                @endif

                                <div class="note-wrapper">
                                    <span id="lead-note-{{ $row->id }}">{!! html_entity_decode($row->note) !!}</span>
                                </div>
                            </div>
                            <hr>
                        @empty
                            <div class="text-center">No Lead notes to be shown</div>
                        @endforelse
                        
                </div>
                <div class="pagination-box">
                    {{ $notes->render() }}
                </div>

                <input type="hidden" id="leadid" name="leadid" value="{{ $account->prospect->lead_id }}" />
                <input type="hidden" name="acc_id" value="{{ $account->id }}" />
                <textarea class="form-control" name="addnote" id="addnote" rows="10" placeholder="Leave your comments here"></textarea>
                @if($errors->has('addnote'))
                    <span class="note error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true">  </i> {{ $errors->first('addnote') }}</span>
                @endif
                <div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>
                <div class="form-group">
                    <div class="text-right">
                        <button type="submit" class="cta-btn add-btn">
                            <i class="fa fa-save"></i> Add Note
                        </button>
                        <button type="button" id="clearNote" class="cta-btn cancel-btn">
                            <i class="fa fa-refresh"></i> Clear
                        </button>
                    </div>
                </div>
            </div>
          
        </form>

        <div class="back-btn">
            <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
                <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
            </a>
        </div>

        <div class="container pull-left">
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <ol class="breadcrumb bg-none">
                        <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li><a href="{{ route('account.list') }}">Accounts</a></li>
                        <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                        <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company}} - Edit</li>
                    </ol>
                </div>
            </div>
        </div>
     
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>


@section('scripts')
<script type="text/javascript" src="{{ asset('js/account/edit.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lead/notes.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-multiselect.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true,
        });
    });
</script>

{{-- <script type="text/javascript" src="{{ asset('js/fileinput.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/purify.min.js" type="text/javascript"></script>
{{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/themes/fas/theme.min.js"></script>
<script type="text/javascript" src="{{ asset('themes/explorer/theme.min.js') }}"></script>
<script>
    $('input[type="file"]').each(function() {
        $(this).fileinput({
            dropZoneEnabled: true,
            maxFilesNum: 5,
            overwriteInitial: true,
            uploadUrl: $('base').attr('href') + "/account/update",
            previewFileIcon: '<i class="fa fa-file"></i>',
            allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
            allowedFileExtensions: ['pdf','doc','docx'],
            previewFileIconSettings: {
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            }
        });
    });

   var $semForm = $('div.sem-file-uploading .file-drop-zone.clickable'),
       $fbForm = $('div.fb-file-uploading .file-drop-zone.clickable'),
       $seoForm = $('div.seo-file-uploading .file-drop-zon.clickable'),
       $webForm = $('div.web-file-uploading .file-drop-zone.clickable'),
       $baiduForm = $('div.baidu-file-uploading .file-drop-zone.clickable'),
       $weiboForm = $('div.weibo-file-uploading .file-drop-zone.clickable'),
       $wechatForm = $('div.wechat-file-uploading .file-drop-zone.clickable'),
       $blogForm = $('div.blog-file-uploading .file-drop-zone.clickable'),
       $socialForm = $('div.social-file-uploading .file-drop-zone.clickable');

    $semForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
            $('input[id="historical_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });
    
    $fbForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="fb_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });


    $seoForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="seo_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

    $webForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="web_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

    $baiduForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="baidu_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

    $weiboForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="weibo_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

    $wechatForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="wechat_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

     $blogForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="blog_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });

    $socialForm.on('dragover drop', function(e) {
        e.preventDefault();
        }).on('drop', function(e) {
        $('input[id="social_hist_data"]')[0].files = e.originalEvent.dataTransfer.files;
    });
</script>
@endsection
@endsection