<details>
    <summary>Baidu</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Baidu Account User ID</label>
                <span>
                    @if(!empty($history->baidu->baidu_account))
                        {{ $history->baidu->baidu_account }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Target Market</label>
                <span>
                    @foreach(explode('|',$history->baidu->baidu_target_market) as $row)
                        @php $baidu_countries = \App\Country::where('code',$row)->first() @endphp
                        {{ $baidu_countries->country }} <br />
                    @endforeach
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>City</label>
                <span>
                    @if(!empty($history->baidu->baidu_city))
                        {{ $history->baidu->baidu_city }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Media Channels</label>
                <span>
                    @if(!empty($history->baidu->baidu_media_channel))
                        @foreach(explode('|',$history->baidu->baidu_media_channel) as $row)
                            <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                        @endforeach
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Brief & Expectations</label>
                <span>
                    @if(!empty($history->baidu->baidu_campaign))
                        {!! nl2br($history->baidu->baidu_campaign) !!}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Ad Scheduling</label>
                <span>
                    @if($history->baidu->baidu_ad_scheduling == '1')
                        Yes 
                        <br/>
                        {!! nl2br($history->baidu->baidu_ad_scheduling_remark) !!}
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Duration</label>
                <span>
                    @if(!empty($history->baidu->baidu_duration))
                        {{ $history->baidu->baidu_duration != 'CD999' ? $history->baidu->campaign_duration->systemdesc : $history->baidu->baidu_duration_other.' '.$history->baidu->campaign_interval->systemdesc  }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Start Date</label>
                <span>
                    @if(!empty($history->baidu->baidu_campaign_datefrom))
                        {{ $history->baidu->baidu_campaign_datefrom }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Campaign End Date</label>
                <span>
                    @if(!empty($history->baidu->baidu_campaign_dateto))
                        {{ $history->baidu->baidu_campaign_dateto }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Total Campaign Budget</label>
                <span>
                    @if(!empty($history->baidu->baidu_budget))
                        {{ $history->baidu->baidu_currency.' '.$history->baidu->baidu_budget }}
                    @else
                        Not Specified
                    @endif 
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Monthly Campaign Budget</label>
                <span>
                    @if(!empty($history->baidu->baidu_monthly_budget))
                        {{ $history->baidu->baidu_currency.' '.$history->baidu->baidu_monthly_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Daily Campaign Budget</label>
                <span>
                    @if(!empty($history->baidu->baidu_daily_budget))
                        {{ $history->baidu->baidu_currency.' '.$history->baidu->baidu_daily_budget }}
                    @else
                      Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="form-box">
                <label>Booking Form</label>
                <span>
                    @if(!empty($history->baidu->baidu_hist_data))
                         @php 
                         $fname = explode('|',$history->baidu->baidu_hist_data_fname);
                         $hash = explode('|',$history->baidu->baidu_hist_data);
                         @endphp
                         @foreach($fname as $n => $row)
                            <p><a href="{{ route('account.view.pdf',['baidu',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                         @endforeach
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>