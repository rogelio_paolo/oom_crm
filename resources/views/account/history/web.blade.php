<details>
        <summary>
            Web Development - 
    
            @if(!empty($history->web))
                @if(!empty($history->web->dev_team))
                    @php
                        $members = explode('|',$history->web->dev_team);
                        $mp = array();
    
                        foreach($members as $row)
                        {
                            $team = \App\Employee::where('emp_id',$row)->first();
                            $mp[] = mb_strimwidth($team->f_name.' '.$team->l_name, 0,50,''); 
                        }
    
                        $members = implode(', ',$mp);
                    @endphp
    
                    &nbsp; Members: {{ $members }}</p>
                @else
                    &nbsp; Members: None</p>
                @endif
            @endif
        </summary>

        <div class="panel panel-default">
            <div class="panel-body">
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Project Type</label>
                    <span>
                        @if(!empty($history->web->project_type))
                            {{ $history->web->project->systemdesc }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>CMS System</label>
                    <span>
                        @if(!empty($history->web->cmstype))
                            {{ $history->web->cms->systemdesc }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>No. of Pages</label>
                    <span>
                        @if(!empty($history->web->pages))
                            {{ $history->web->pages }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Objective</label>
                    <span>
                        @if(!empty($history->web->objective))
                            {!! nl2br($history->web->objective) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Brief Information about the Company</label>
                    <span>
                        @if(!empty($history->web->company_brief))
                            {!! nl2br($history->web->company_brief) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Target Audience</label>
                    <span>
                        @if(!empty($history->web->target_audience))
                            {!! nl2br($history->web->target_audience) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Existing Website (URL)</label>
                    <span>
                        @if(!empty($history->web->existing_web_url))
                            {{ $history->web->existing_web_url }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Preferred Reference Website</label>
                    <span>
                        @if(!empty($history->web->reference_web))
                            {!! nl2br($history->web->reference_web) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Look and Feel</label>
                    <span>
                        @if(!empty($history->web->web_ui))
                            {!! nl2br($history->web->web_ui) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Color Scheme</label>
                    <span>
                        @if(!empty($history->web->color_scheme))
                            {!! nl2br($history->web->color_scheme) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Special Functionality</label>
                    <span>
                        @if(!empty($history->web->functionality))
                            {!! nl2br($history->web->functionality) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Booking Form</label>
                    <span>
                        @if(!empty($history->web->web_hist_data))
                            @php 
                                $fname = explode('|',$history->web->web_hist_data_fname);
                                $hash = explode('|',$history->web->web_hist_data);
                            @endphp
                            @foreach($fname as $n => $row)
                                <p><a href="{{ route('account.view.pdf',['web',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                            @endforeach
                            @else
                                Not Specified
                            @endif
                    </span>
                </div>
            </div>
        </div>
    
            </div>
        </div>
    </details>