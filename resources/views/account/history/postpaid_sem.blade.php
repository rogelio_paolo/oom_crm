<details>
    <summary>Post Paid SEM</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Duration</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_duration))
                        {{ $history->postpaid->postpaid_duration != 'CD999' ? $history->postpaid->campaign_duration->systemdesc : $history->postpaid->postpaid_duration_other.' '.$history->postpaid->campaign_interval->systemdesc  }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Start Date</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_campaign_datefrom))
                        {{ $history->postpaid->postpaid_campaign_datefrom }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign End Date</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_campaign_dateto))
                        {{ $history->postpaid->postpaid_campaign_dateto }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Media Budget Utilized</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_media_budget))
                        {{ '$ ' . $history->postpaid->postpaid_media_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>AM Fee</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_am_fee))
                        {{ $history->postpaid->postpaid_am_fee . '%'}}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>AM Fee Amount</label>
                <span>
                    @if(!empty($history->postpaid->postpaid_am_fee_amount))
                        {{ '$ ' . $history->postpaid->postpaid_am_fee_amount }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Is Media Budget paid through OOm?</label>
                <span>
                    @if($history->postpaid->is_media_budget_paid == 1)
                        Yes - {{ '$ ' . $history->postpaid->postpaid_total }} (Media Budget Utilized + AM Fee Amount)
                    @else
                        No - {{ '$ ' . $history->postpaid->postpaid_am_fee_amount }} (AM Fee Amount)
                    @endif
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>