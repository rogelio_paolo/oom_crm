<details>
    <summary>Weibo</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Location</label>
                <span>
                    @if(!empty($history->weibo->weibo_location))
                        {{ $history->weibo->weibo_location }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Weibo Account User ID</label>
                <span>
                    @if(!empty($history->weibo->weibo_account))
                        {{ $history->weibo->weibo_account }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Requires New Blue V Weibo Setup?</label>
                <span>
                    @if($history->weibo->has_blue_v_weibo == '1')
                        Yes 
                    <br/>
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Brief & Expectations</label>
                <span>
                    @if(!empty($history->weibo->weibo_campaign))
                        {!! nl2br($history->weibo->weibo_campaign) !!}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Ad Scheduling</label>
                <span>
                    @if($history->weibo->weibo_ad_scheduling == '1')
                        Yes 
                        <br/>
                        {!! nl2br($history->weibo->weibo_ad_scheduling_remark) !!}
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Duration</label>
                <span>
                    @if(!empty($history->weibo->weibo_duration))
                        {{ $history->weibo->weibo_duration != 'CD999' ? $history->weibo->campaign_duration->systemdesc : $history->weibo->weibo_duration_other.' '.$history->weibo->campaign_interval->systemdesc  }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Start Date</label>
                <span>
                    @if(!empty($history->weibo->weibo_campaign_datefrom))
                        {{ $history->weibo->weibo_campaign_datefrom }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Campaign End Date</label>
                <span>
                    @if(!empty($history->weibo->weibo_campaign_dateto))
                        {{ $history->weibo->weibo_campaign_dateto }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Total Campaign Budget</label>
                <span>
                    @if(!empty($history->weibo->weibo_budget))
                        {{ $history->weibo->weibo_currency.' '.$history->weibo->weibo_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Monthly Campaign Budget</label>
                <span>
                    @if(!empty($history->weibo->weibo_monthly_budget))
                        {{ $history->weibo->weibo_currency.' '.$history->weibo->weibo_monthly_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-box">
                <label>Daily Campaign Budget</label>
                <span>
                    @if(!empty($history->weibo->weibo_daily_budget))
                        {{ $history->weibo->weibo_currency.' '.$history->weibo->weibo_daily_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="form-box">
                <label>Booking Form</label>
                <span>
                @if(!empty($history->weibo->weibo_hist_data))
                     @php 
                     $fname = explode('|',$history->weibo->weibo_hist_data_fname);
                     $hash = explode('|',$history->weibo->weibo_hist_data);
                     @endphp
                     @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['weibo',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                     @endforeach
                @else
                    Not Specified
                @endif  
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>