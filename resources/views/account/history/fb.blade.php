<details>
        <summary>
            Facebook -
    
            @if(!empty($history->fb))
                @if(!empty($history->fb->sem_team))
                    @php
                        $members = explode('|',$history->fb->sem_team);
                        $mp = array();
    
                        foreach($members as $row)
                        {
                            $team = \App\Employee::where('emp_id',$row)->first();
                            $mp[] = mb_strimwidth($team->f_name.' '.$team->l_name, 0,50,''); 
                        }
    
                        $members = implode(', ',$mp);
                    @endphp
    
                    &nbsp; Members: {{ $members }}</p>
                @else
                    &nbsp; Members: None</p>
                @endif
            @endif
        </summary>

        <div class="panel panel-default">
            <div class="panel-body">
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Target Country</label>
                    <span>
                        @foreach(explode('|',$history->fb->fb_target_country) as $row)
                            @php $fb_countries = \App\Country::where('code',$row)->first() @endphp
                            {{ $fb_countries->country }}
                        @endforeach
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>List Account as</label>
                    <span>
                        @if(!empty($history->fb->fb_list_account))
                            <strong>{{ $history->fb->listing->systemdesc }}</strong>
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Media Budget Payment</label>
                    <span>
                        @if(!empty($history->fb->fb_payment_method))
                            {{ $history->fb->mode->systemdesc }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Media Channels</label>
                    <span>
                        @if(!empty($history->fb->fb_media_channel))
                            @php 
                                $fb_ch = array();
                                foreach(explode('|',$history->fb->fb_media_channel) as $row)
                                {
                                    $fb_channels = \App\System::where('systemcode',$row)->first()->systemdesc;
                                    $fb_ch[] = mb_strimwidth($fb_channels, 0, 20, '');
                                }
                                $fb_media_channels = implode(', ', $fb_ch);
                            @endphp
                            {{ $fb_media_channels }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign Brief & Expectations</label>
                    <span>
                        @if(!empty($history->fb->fb_campaign))
                            {!! nl2br($history->fb->fb_campaign) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign Duration</label>
                    <span>
                        @if(!empty($history->fb->fb_duration))
                            {{ $history->fb->fb_duration != 'CD999' ? $history->fb->duration->systemdesc : $history->fb->duration_other.' '.$history->fb->duration_interval->systemdesc  }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign Start Date</label>
                    <span>
                        @if(!empty($history->fb->fb_campaign_dt_from))
                            {{ $history->fb->fb_campaign_dt_from }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign End Date</label>
                    <span>
                        @if(!empty($history->fb->fb_campaign_dt_to))
                            {{ $history->fb->fb_campaign_dt_to }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Total Campaign Budget</label>
                    <span>
                        @if(!empty($history->fb->total_budget))
                            {{ $history->fb->fb_currency.' '.$history->fb->total_budget }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Monthly Campaign Budget</label>
                    <span>
                        @if(!empty($history->fb->monthly_budget))
                            {{ $history->fb->fb_currency.' '.$history->fb->monthly_budget }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Daily Campaign Budget</label>
                    <span>
                        @if(!empty($history->fb->daily_budget))
                            {{ $history->fb->fb_currency.' '.$history->fb->daily_budget }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Gender</label>
                    <span>
                        @if(!empty($history->fb->gender))
                            {{ $history->fb->gender == 'A' ? 'All' : ($history->fb->gender == 'M' ? 'Male' : 'Female') }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Interest</label>
                    <span>
                        @if(!empty($history->fb->interest))
                            {!! nl2br($history->fb->interest) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Booking Form</label>
                    <span>
                        @if(!empty($history->fb->fb_hist_data))
                            @php 
                                $fname = explode('|',$history->fb->fb_hist_data_fname);
                                $hash = explode('|',$history->fb->fb_hist_data);
                            @endphp
                            @foreach($fname as $n => $row)
                                <p><a href="{{ route('account.view.pdf',['fb',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                            @endforeach
                         @else
                             Not Specified
                         @endif
                    </span>
                </div>
            </div>
        </div>
    
            </div>
        </div>
    </details>