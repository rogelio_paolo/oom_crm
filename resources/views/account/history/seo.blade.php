<details>
        <summary>
                SEO -
        
                @if(!empty($history->seo))
                    @if(!empty($history->seo->seo_team))
                        @php
                            $members = explode('|',$history->seo->seo_team);
                            $mp = array();
        
                            foreach($members as $row)
                            {
                                $team = \App\Employee::where('emp_id',$row)->first();
                                $mp[] = mb_strimwidth($team->f_name.' '.$team->l_name, 0,50,''); 
                            }
        
                            $members = implode(', ',$mp);
                        @endphp
        
                        &nbsp; Members: {{ $members }}</p>
                    @else
                        &nbsp; Members: None</p>
                    @endif
                @endif
            </summary>
        <div class="panel panel-default">
            <div class="panel-body">
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Target Country</label>
                    <span>
                        @foreach(explode('|',$history->seo->seo_target_countries) as $row)
                            @php $seo_countries = \App\Country::where('code',$row)->first() @endphp
                            {{ $seo_countries->country }}
                        @endforeach
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>No. of Keywords</label>
                    <span>
                        @if(!empty($history->seo->keyword_themes))
                            {{ $history->seo->keyword_themes != 'NK999' ? $history->seo->keywords->systemdesc : $history->seo->keyword_themes_other }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign Duration</label>
                    <span>
                        @if(!empty($history->seo->seo_duration))
                            {{ $history->seo->seo_duration != 'CD999' ? $history->seo->duration->systemdesc : $history->seo->seo_duration_other.' '.$history->seo->duration_interval->systemdesc  }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Campaign Brief & Expectations</label>
                    <span>
                        @if(!empty($history->seo->seo_campaign))
                            {!! nl2br($history->seo->seo_campaign) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Keyword Theme Focus</label>
                    <span>
                        @if(!empty($history->seo->keyword_focus))
                            {!! nl2br($history->seo->keyword_focus) !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Keyword Maintenance Project</label>
                    <span>
                        @if(!empty($history->seo->keyword_maintenance))
                            {!! !is_int($history->seo->keyword_maintenance) ? nl2br($history->seo->keyword_maintenance) : 'No' !!}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>SEO Total Fee</label>
                    <span>
                        @if(!empty($history->seo->seo_total_fee))
                            {{ $history->seo->seo_currency.' '.$history->seo->seo_total_fee }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Booking Form</label>
                    <span>
                        @if(!empty($history->seo->seo_hist_data))
                            @php 
                            $fname = explode('|',$history->seo->seo_hist_data_fname);
                            $hash = explode('|',$history->seo->seo_hist_data);
                            @endphp
                            @foreach($fname as $n => $row)
                            <p><a href="{{ route('account.view.pdf',['seo',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                            @endforeach
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>
    
            </div>
        </div>
    </details>