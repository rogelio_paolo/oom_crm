<details>
    <summary>Blog Content</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Total Number of Blog Post</label>
                <span>
                   @if(!empty($history->blog->blog_total_post))
                        {{ $history->blog->blog_total_post }}
                   @else
                        Not Specified
                   @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Number of Blog Post Per Month</label>
                <span>
                    @if(!empty($history->blog->blog_monthly_post))
                        {{ $history->blog->blog_monthly_post }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Content Brief and Writing Style</label>
                <span>
                   @if(!empty($history->blog->blog_content_brief))
                        {!! nl2br($history->blog->blog_content_brief) !!}
                   @else
                        Not Specified
                   @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-box">
                <label>Booking Form</label>
                <span>
                    @if(!empty($history->blog->blog_hist_data))
                         @php 
                         $fname = explode('|',$history->blog->blog_hist_data_fname);
                         $hash = explode('|',$history->blog->blog_hist_data);
                         @endphp
                         @foreach($fname as $n => $row)
                            <p><a href="{{ route('account.view.pdf',['blog',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                         @endforeach
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>