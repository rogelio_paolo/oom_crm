<details>
    <summary>WeChat</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>WeChat OA Setup?</label>
                <span>
                    @if($history->wechat->has_wechat_type == 1)
                        Yes
                        <br/>
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
        @if (!empty($history->wechat->has_wechat_type))
            <div class="col-md-4">
                <div class="form-box">
                    <label>WeChat Type</label>
                    <span>
                        @if(!empty($history->wechat->wechat_type))
                            {{ $history->wechat->wechat_type == 'serviced' ? 'Serviced' : 'Subscription' }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Requires Menu Setup?</label>
                    <span>
                        @if($history->wechat->has_wechat_menu_setup == '1')
                            Yes 
                            <br/>
                        @else
                            No
                        @endif
                    </span>
                </div>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Requires WeChat Advertising?</label>
                <span>
                    @if(!empty($history->wechat->wechat_location))
                        Yes 
                        <br/>
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
        @if(!empty($history->wechat->wechat_location))
            <div class="col-md-4">
                <div class="form-box">
                    <label>WeChat Advertising Type</label>
                    <span>
                        @if(!empty($history->wechat->wechat_advertising_type))
                            @foreach(explode('|',$history->wechat->wechat_advertising_type) as $row)
                                <p>{{ \App\System::where('systemcode',$row)->first()->systemdesc }}</p>
                            @endforeach
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Location</label>
                    <span>
                        @if(!empty($history->wechat->wechat_location))
                            {{ $history->wechat->wechat_location }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        @endif
    </div>

    @if(!empty($history->wechat->wechat_location))
         <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Age Group</label>
                    <span>
                        @if(!empty($history->wechat->wechat_age_from))
                            {{ $history->wechat->wechat_age_from }} to {{ $history->wechat->wechat_age_to }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Marital Status</label>
                    <span>
                        @if(!empty($history->wechat->wechat_marital))
                            {{ $history->wechat->wechat_marital }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Gender</label>
                    <span>
                        @if(!empty($history->wechat->wechat_gender))
                            {{ $history->wechat->wechat_gender == 'M' ? 'Male' : ($history->wechat->wechat_gender == 'F' ? 'Female' : 'Not Specified') }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-box">
                    <label>Handphone Device</label>
                    <span>
                        @if(!empty($history->wechat->wechat_handphone))
                            {{ $history->wechat->wechat_handphone == 'android' ? 'Android' : 'iOS' }}
                        @else
                            Not Specified
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-box">
                    <label>Education</label>
                    <span>
                        @if(!empty($history->wechat->wechat_education))
                            {{ $history->wechat->wechat_education }}
                        @else
                            Not Specified
                        @endif  
                    </span>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="form-group clearfix">
            <label for="wechat_historical_data" class="col-md-4 control-label">Booking Form</label>
        
                <div class="col-md-6">
        
                    @if(!empty($history->wechat->wechat_hist_data))
                        @php 
                        $fname = explode('|',$history->wechat->wechat_hist_data_fname);
                        $hash = explode('|',$history->wechat->wechat_hist_data);
                        @endphp
                        @foreach($fname as $n => $row)
                            <p><a href="{{ route('account.view.pdf',['wechat',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                        @endforeach
                    @else
                        Not Specified
                    @endif
                </div>
            </div>
        </div>
    </div>
    
</details>