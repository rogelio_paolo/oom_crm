<details>
    <summary>Social Media Management</summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Duration</label>
                <span>
                    @if(!empty($history->social->social_duration))
                        {{ $history->social->social_duration != 'CD999' ? $history->social->campaign_duration->systemdesc : $history->social->social_duration_other.' '.$history->social->campaign_interval->systemdesc  }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Number of Blog Post Per Month</label>
                <span>
                    @if(!empty($history->social->social_monthly_post))
                        {{ $history->social->social_monthly_post }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Content Brief and Writing Style</label>
                <span>
                    @if(!empty($history->social->social_content_brief))
                        {!! nl2br($history->social->social_content_brief) !!}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Any FB Media Budget to Boost Post?</label>
                <span>
                    @if(!empty($history->social->social_media_budget))
                        Yes - ${{ $history->social->social_media_budget }}
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-box">
                <label>Booking Form</label>
                <span>
                 @if(!empty($history->social->social_hist_data))
                     @php 
                     $fname = explode('|',$history->social->social_hist_data_fname);
                     $hash = explode('|',$history->social->social_hist_data);
                     @endphp
                     @foreach($fname as $n => $row)
                        <p><a href="{{ route('account.view.pdf',['social',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                     @endforeach
                @else
                    Not Specified
                @endif
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>