<details>

    <summary>
        SEM - 

        @if(!empty($history->sem))
            @if(!empty($history->sem->sem_team))
                @php
                    $members = explode('|',$history->sem->sem_team);
                    $mp = array();

                    foreach($members as $row)
                    {
                        $team = \App\Employee::where('emp_id',$row)->first();
                        $mp[] = mb_strimwidth($team->f_name.' '.$team->l_name, 0,50,''); 
                    }

                    $members = implode(', ',$mp);
                @endphp

                &nbsp; Members: {{ $members }}</p>
            @else
                &nbsp; Members: None</p>
            @endif
        @endif
    </summary>
    <div class="panel panel-default">
        <div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Target Country</label>
                <span>
                    @foreach(explode('|',$history->sem->target_market) as $row)
                        @php $sem_countries = \App\Country::where('code',$row)->first() @endphp
                        {{ $sem_countries->country }}
                    @endforeach
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>List Account as</label>
                <span>
                    @if(!empty($history->sem->list_account))
                        <strong>{{ $history->sem->listing->systemdesc }}</strong>
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Media Budget Payment</label>
                <span>
                    @if(!empty($history->sem->payment_method))
                        {{ $history->sem->mode->systemdesc }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Media Channels</label>
                <span>
                    @if(!empty($history->sem->media_channel))
                        @php 
                            $ch = array();
                            foreach(explode('|',$history->sem->media_channel) as $row)
                            {
                                $channels = \App\System::where('systemcode',$row)->first()->systemdesc;
                                $ch[] = mb_strimwidth($channels, 0, 20, '');
                            }
                            $media_channels = implode(', ', $ch);
                        @endphp
                        {{ $media_channels }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Brief & Expectations</label>
                <span>
                    @if(!empty($history->sem->campaign))
                        {!! nl2br($history->sem->campaign) !!}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Duration</label>
                <span>
                    @if(!empty($history->sem->duration))
                        {{ $history->sem->duration != 'CD999' ? $history->sem->campaign_duration->systemdesc : $history->sem->duration_other.' '.$history->sem->campaign_interval->systemdesc  }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign Start Date</label>
                <span>
                    @if(!empty($history->sem->campaign_datefrom))
                        {{ $history->sem->campaign_datefrom }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Campaign End Date</label>
                <span>
                    @if(!empty($history->sem->campaign_dateto))
                        {{ $history->sem->campaign_dateto }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Ad Scheduling</label>
                <span>
                    @if($history->sem->ad_scheduling == '1')
                        Yes 
                        <br/>
                        {!! nl2br($history->sem->ad_scheduling_remark) !!}
                    @else
                        No
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Account Management Fee</label>
                <span>
                    @if(!empty($history->sem->am_fee))
                        {{ $history->sem->currency.' '.$history->sem->am_fee }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Total Campaign Budget</label>
                <span>
                    @if(!empty($history->sem->budget))
                        {{ $history->sem->currency.' '.$history->sem->budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Monthly Campaign Budget</label>
                <span>
                    @if(!empty($history->sem->google_monthly_budget))
                        {{ $history->sem->currency.' '.$history->sem->google_monthly_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-box">
                <label>Daily Campaign Budget</label>
                <span>
                    @if(!empty($history->sem->google_daily_budget))
                        {{ $history->sem->currency.' '.$history->sem->google_daily_budget }}
                    @else
                        Not Specified
                    @endif
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-box">
                <label>Booking Form</label>
                <span>
                    @if(!empty($history->sem->historical_data))
                        @php 
                        $fname = explode('|',$history->sem->historical_data_fname);
                        $hash = explode('|',$history->sem->historical_data);
                        @endphp
                        @foreach($fname as $n => $row)
                           <p><a href="{{ route('account.view.pdf',['sem',$hash[$n],$row] ) }}" target="_blank"><i class="fa fa-file-{{ strpos($row,'doc') || strpos($row,'docx') ? 'word-o' : 'pdf-o' }} text-primary"></i> {{ $row }}</a></p>
                        @endforeach
                   @else
                       Not Specified
                   @endif
                </span>
            </div>
        </div>
    </div>

        </div>
    </div>
    
</details>