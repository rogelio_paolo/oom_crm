@extends('layouts.master_v2')
@section('title')
CRM - Assign members on {{ !empty($account->contract->company) ? $account->contract->company."'s " : $account->prospect->lead->company . "'s "}} account
@endsection
@section('styles')
<link href="{{ asset('fonts_v1/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css_v1/multi-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
@endsection
@section('main_content')

<div class="back-btn">
    <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
        <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
    </a>
</div>

<div class="container pull-left">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <ol class="breadcrumb bg-none">
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('account.list') }}">Accounts</a></li>
                <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }} - Member Assigning</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

<div class="loader-overlay hidden"></div>

<div class="outer-data-box">
    <div class="row">
        <div class="">
            <form method="post" action="{{ route('account.update.assign') }}">
                {{ csrf_field() }}
                <input type="hidden" name="accountid" value="{{ $account->id }}" />
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Prospect Details</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <div class="form-group clearfix">
                                        <label for="company_name" class="col-md-4 control-label">Contract Value (without GST)</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->contract->contract_value) ?  $account->contract->currency . ' ' . $account->contract->contract_value : 'Not Specified' }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="contract_type" class="col-md-4 control-label">Contract Type</label>
                                        <div class="col-md-6">
                                            <p><strong>{{ !empty($account->contract->contract_type) ? $account->contract->contractType->systemdesc : '' }}</strong></p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="company_name" class="col-md-4 control-label">Company Name</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->contract->company) ? $account->contract->company : (!empty($account->prospect->lead->company) ? $account->prospect->lead->company : 'Not Specified') }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="contract_number" class="col-md-4 control-label">Contract Number</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->contract->contract_number) ? $account->contract->contract_number : 'Not Specified' }}</p>
                                        </div>
                                    </div>
                                   
                                  
                                    <div class="form-group clearfix">
                                        <label for="client_name" class="col-md-4 control-label">Client Name</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->contract->f_name) ? $account->contract->f_name.' '.$account->contract->l_name : (!empty($account->prospect->lead->f_name) ? $account->prospect->lead->f_name.' '.$account->prospect->lead->f_name : 'Not Specified') }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="client_number" class="col-md-4 control-label">Client Number</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->contract->contact_number) ? $account->contract->contact_number : (!empty($account->prospect->lead->contact_number) ? $account->prospect->lead->contact_number : 'Not Specified') }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="client_email" class="col-md-4 control-label">Client Email Address</label>
                                        <div class="col-md-6">
                                            @if (!empty($account->contract->email))
                                                @foreach (explode('|', $account->contract->email) as $email)
                                                    <p>{{ $email }}</p>
                                                @endforeach
                                            @elseif (!empty($account->prospect->lead->email))
                                                @foreach (explode('|', $account->prospect->lead->email) as $email)
                                                    <p>{{ $email }}</p>
                                                @endforeach
                                            @else
                                                <p>Not Specified</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="street_address" class="col-md-4 control-label">Client Address</label>
                                        <div class="col-md-6">
                                            <p>{!! !empty($account->prospect->lead->street_address) ? nl2br($account->prospect->lead->street_address) : 'Not Specified' !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="client_email" class="col-md-4 control-label">Website URL</label>
                                        <div class="col-md-6">
                                            <p>{{ !empty($account->prospect->lead->website) ? $account->prospect->lead->website : 'Not specified' }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="nature" class="col-md-4 control-label">Services </label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="nature" value="{{ $account->nature }}" />
                                            <p>
                                                @foreach($nature as $i => $row)
                                                {{ $row }}
                                                @if(!$loop->last)
                                                ,
                                                @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="contact_page_url" class="col-md-4 control-label">Contact Us Page URL</label>
                                        <div class="col-md-6">
                                            <p>
                                                @if(!empty($account->additional->contact_page_url))
                                                {{ $account->additional->contact_page_url }}
                                                @else
                                                Not Specified
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="ty_page_url" class="col-md-4 control-label">Thank You Page URL</label>
                                        <div class="col-md-6">
                                            <p>
                                                @if(!empty($account->additional->ty_page_url))
                                                {{ $account->additional->ty_page_url }}
                                                @else
                                                Not Specified
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="code_required" class="col-md-4 control-label">GA Required?</label>
                                        <div class="col-md-6">
                                            <p>
                                                {{ $account->additional->ga_required == 1 ? 'Yes' : 'No' }} {{ !empty($account->additional->ga_remark) ? '- '.$account->additional->ga_remark : '' }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="implementations" class="col-md-4 control-label">Implementations Needed from OOm?</label>
                                        <div class="col-md-6">
                                            <p>
                                                {{ $account->additional->implementations == 1 ? 'Yes' : 'No' }} {{ !empty($account->additional->implementations_remark) ? '- '.$account->additional->implementations_remark : '' }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label for="web_login" class="col-md-4 control-label">Website CMS / FTP Login</label>
                                        <div class="col-md-6">
                                            <p>
                                                @if(!empty($account->additional->web_login))
                                                {!! nl2br($account->additional->web_login) !!}
                                                @else
                                                Not Specified
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix" style="margin-bottom: 5%">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if (!empty($account->ppc_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">SEM</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.sem')
                                    @include('account.assign.sem_assigning')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                
                @if (!empty($account->fb_id))                       
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Facebook Advertisement</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.fb')
                                    @include('account.assign.fb_assigning')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                
                @if (!empty($account->seo_id))                             
                <div class="main-data-box">
                    <div class="data-header-box">
                        <span class="header-h">SEO</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.seo')
                                    @include('account.seo_assigning')   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                
                @if (!empty($account->web_id))                             
                <div class="main-data-box">
                    <div class="data-header-box">
                        <span class="header-h">Web Development</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.web')
                                    @if($user->role == 1 || ($teamcode == 'sp' && in_array($user->userInfo->emp_id, $managers)))
                                        <div class="col-md-12">
                                            <div class="panel panel-primary assign-border" >
                                                <div class="panel-heading assign-background">
                                                <h3 class="panel-title assign-padding">Assign Development Team Members</h3>
                                                </div>

                                                @if($user->role == 1 || ($teamcode == 'sp' && in_array($user->userInfo->emp_id, $managers)) )
                                                    <div class="panel-body">
                                                        <label for="dev_team" class="col-md-4 control-label">Web Development Team </label>
                                                        <div class="col-md-6">
                                                            <div class="select-box">
                                                                <input type="hidden" name="accountwebid" value="{{ $account->web_id }}">
                                                                @php 
                                                                    $current_dev_member = explode('|', $account->web->dev_team); 
                                                                @endphp
                                                                <select id="dev_team" name="dev_team[]" class="multiselect-ui form-control" multiple="multiple">
                                                                    @foreach($dev_team as $i => $row)
                                                                        <option value="{{ $row['emp_id'] }}" @if(isset($current_dev_member)) {{ in_array($row['emp_id'],$current_dev_member) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                {{--  @if($user->role == 1 || ($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers)) )
                                                    <div class="panel-body">
                                                        <label for="dev_content_team" class="col-md-4 control-label">Web Development Content Team </label>
                                                        <div class="col-md-6">
                                                            <div class="select-box">
                                                                <input type="hidden" name="accountwebid" value="{{ $account->web_id }}">
                                                                @php 
                                                                    $current_dev_content_member = explode('|', $account->web->dev_content_team); 
                                                                @endphp
                                                                <select id="dev_content_team" name="dev_content_team[]" class="multiselect-ui form-control" multiple="multiple">
                                                                    @foreach($content_team as $i => $row)
                                                                        <option value="{{ $row['emp_id'] }}" @if(isset($current_dev_content_member)) {{ in_array($row['emp_id'],$current_dev_content_member) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif  --}}

                                                <div class="row">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <input id="btn-assign-dev" type="button" class="btn btn-success assign-btn" {{ !empty($account->web->dev_team) ? '' : 'disabled' }} value="Assign">
                                                        <input id="btn-dev-clear" type="button" class="btn btn-warning assign-btn" {{ !empty($account->web->dev_team) ? '' : 'disabled' }} value="Clear">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->baidu_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Baidu</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.baidu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->weibo_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Weibo</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.weibo')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->wechat_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">WeChat</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.wechat')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->blog_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Blog Content</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.blog')

                                    @if($user->role == 1 || ($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers)) )
                                        <div class="col-md-12">
                                            <div class="panel panel-primary assign-border" >
                                                <div class="panel-heading assign-background">
                                                    <h3 class="panel-title assign-padding">Assign Blog Content Team Members</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <label for="blog_content_team" class="col-md-4 control-label">Blog Content Team </label>
                                                    <div class="col-md-6">
                                                        <div class="select-box">
                                                            <input type="hidden" name="accountblogid" value="{{ $account->blog_id }}">
                                                            @php 
                                                                $current_blog_content_member = explode('|', $account->blog->blog_content_team); 
                                                            @endphp
                                                            <select id="blog_content_team" name="blog_content_team[]" class="multiselect-ui form-control" multiple="multiple">
                                                                @foreach($content_team as $i => $row)
                                                                    <option value="{{ $row['emp_id'] }}" @if(isset($current_blog_content_member)) {{ in_array($row['emp_id'],$current_blog_content_member) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                                                                @endforeach
                                                            </select>

                                                            <input id="btn-assign-content-blog" type="button" class="btn btn-success assign-btn" {{ !empty($account->blog->blog_content_team) ? '' : 'disabled' }} value="Assign">
                                                            <input id="btn-blog-content-clear" type="button" class="btn btn-warning assign-btn" {{ !empty($account->blog->blog_content_team) ? '' : 'disabled' }} value="Clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->social_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Social Media Management</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.social')

                                    @if($user->role == 1 || ($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers)) )
                                        <div class="col-md-12">
                                            <div class="panel panel-primary assign-border" >
                                                <div class="panel-heading assign-background">
                                                    <h3 class="panel-title assign-padding">Assign Social Media Management Team Members</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <label for="social_content_team" class="col-md-4 control-label">Social Media Management Content Team </label>
                                                    <div class="col-md-6">
                                                        <div class="select-box">
                                                            <input type="hidden" name="accountsocialid" value="{{ $account->social_id }}">
                                                            @php 
                                                                $current_social_content_member = explode('|', $account->social->social_content_team); 
                                                            @endphp
                                                            <select id="social_content_team" name="social_content_team[]" class="multiselect-ui form-control" multiple="multiple">
                                                                @foreach($content_team as $i => $row)
                                                                    <option value="{{ $row['emp_id'] }}" @if(isset($current_social_content_member)) {{ in_array($row['emp_id'],$current_social_content_member) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                                                                @endforeach
                                                            </select>

                                                            <input id="btn-assign-content-social" type="button" class="btn btn-success assign-btn" {{ !empty($account->social->social_content_team) ? '' : 'disabled' }} value="Assign">
                                                            <input id="btn-social-content-clear" type="button" class="btn btn-warning assign-btn" {{ !empty($account->social->social_content_team) ? '' : 'disabled' }} value="Clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (!empty($account->postpaid_sem_id))                              
                <div class="main-data-box">
                    <div class="data-header-box clearfix">
                        <span class="header-h">Post Paid SEM</span>
                    </div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    @include('account.assign.postpaid_sem')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if( (in_array($teamcode,['acc','busi']) || $user->role == 1) )
                    <div class="main-data-box">
                        <div class="data-header-box">
                            <span class="header-h">Assign Account Management</span>
                        </div>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">

                <div class="form-group clearfix">
                    <label for="acc_team" class="col-md-4">Main Account Manager </label>
                    <div class="col-md-6 col-md-offset-4">
                        <div class="select-box">
                            @php 
                                $current_acc_member = $account->acc_assigned;
                            @endphp

                            <select id="acc_team" name="acc_team" class="selectpicker form-control">
                                <option value="0" {{ !isset($current_acc_member) ? 'selected' : '' }}>None Selected</option>

                                @foreach($acc_team as $i => $row)
                                    <option value="{{ $row['emp_id'] }}" @if(isset($current_acc_member)) {{ $row['emp_id'] == $current_acc_member ? 'selected' : '' }} @endif>{{ $row['f_name'].' '.$row['l_name'] }}</option>
                                @endforeach

                            </select>

                            <div class="clearfix">&nbsp;</div>
                        </div>

                        @if ($errors->has('acc_team'))
                            <span class="help-block">
                                <strong>{{ $errors->first('acc_team') }}</strong>
                            </span>
                        @endif

                    </div>


                    <label for="acc_assistant" class="col-md-4">Assistant Account Manager </label>
                    <div class="col-md-6 col-md-offset-4">
                            <div class="select-box">
                                @php 
                                    $current_acc_assistant = $account->acc_assistant;
                                @endphp
    
                                <select id="acc_assistant" name="acc_assistant" class="selectpicker form-control">
                                    <option value="0" {{ !isset($current_acc_assistant) ? 'selected' : '' }}>None Selected</option>
    
                                    @foreach($acc_team as $i => $row)
                                        <option value="{{ $row['emp_id'] }}" @if(isset($current_acc_assistant)) {{ $row['emp_id'] == $current_acc_assistant ? 'selected' : '' }} @endif>{{ $row['f_name'].' '.$row['l_name'] }}</option>
                                    @endforeach
    
                                </select>
    
                                <div class="clearfix">&nbsp;</div>
                            </div>
    
                            
                            @if ($errors->has('acc_team'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('acc_team') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-md-6 col-md-offset-4">
                            <div class="clearfix assign-btn">
                                <input id="btn-assign-acc" type="button" class="btn btn-success" value="Assign" {{ !empty($current_acc_member) || !empty($current_acc_assistant) ? '' : 'disabled' }}/>
                                <a href="{{ route('account.list') }}" class="btn btn-danger btn-md">Cancel</a>    
                            </div>
                        </div>
                </div>
                               
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
            </form>

          

        </div>
              
       
    </div>

</div>

<div class="clearfix" style="margin-top: 1%">&nbsp;</div>

<div class="back-btn">
    <a href="{{ route('contract.summary', $account->prospect->id) }}" class="btn btn-danger btn-button-rect">
        <span class="fa fa-chevron-circle-left"></span> Back to Contract Summary List
    </a>
</div>

<div class="container pull-left">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <ol class="breadcrumb bg-none">
                <li><a href="{{ route('user.dashboard') }}">Home</a></li>
                <li><a href="{{ route('account.list') }}">Accounts</a></li>
                <li><a href="{{ route('contract.summary', $account->prospect->id) }}">Contract Summary</a></li>
                <li class="active">{{ !empty($account->contract->company) ? $account->contract->company : $account->prospect->lead->company }} - Member Assigning</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>

@section('scripts')
<script type="text/javascript" src="{{ asset('js/account/assign.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.min.js') }}"></script>
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
@endsection
@endsection
 