@if($user->role == 1 || ($teamcode == 'sp' && in_array($user->userInfo->emp_id, $managers)) || ($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers)) )
<div class="col-md-12">
    <div class="panel panel-primary assign-border" >
        <div class="panel-heading assign-background">
          <h3 class="panel-title assign-padding">Assign SEO Team Members</h3>
        </div>
        <input type="hidden" name="accountseoid" value="{{ $account->seo_id }}">

        @if($user->role == 1 || ($teamcode == 'sp' && in_array($user->userInfo->emp_id, $managers)) )
            <div class="panel-body">
                <label for="seo_strategist_assigned" class="col-md-4 control-label">SEO Strategist </label>
                <div class="col-md-6">
                    <div class="select-box">
                        @php 
                            $current_seo_strategist = explode('|', $account->seo->seo_strategist_assigned); 
                            $asst_current_seo_strategist = explode('|', $account->seo->asst_seo_strategist_assigned); 
                            $current_seo_link_builder = explode('|', $account->seo->seo_link_builder_assigned); 
                            $asst_current_seo_link_builder = explode('|', $account->seo->asst_seo_link_builder_assigned); 
                        @endphp
                        <select id="seo_strategist_assigned" name="seo_strategist_assigned" class="selectpicker form-control">
                            <option value="" {{ !isset($current_seo_strategist) ? 'selected' : '' }}>None Selected</option>
                            @foreach($seo_team as $i => $row)
                                <option value="{{ $row['emp_id'] }}" @if(isset($current_seo_strategist)) {{ in_array($row['emp_id'],$current_seo_strategist) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                            @endforeach
                        </select>   
                    </div>
                </div>
                <div class="clearfix" style="margin:1%"></div>
                <label for="asst_seo_strategist_assigned" class="col-md-4 control-label">Assistant SEO Strategist (Optional)</label>
                <div class="col-md-6">
                    <div class="select-box">
                        <select id="asst_seo_strategist_assigned" name="asst_seo_strategist_assigned" class="selectpicker form-control">
                            <option value="" {{ !isset($asst_current_seo_strategist) ? 'selected' : '' }}>None Selected</option>
                            @foreach($seo_team as $i => $row)
                                <option value="{{ $row['emp_id'] }}" @if(isset($asst_current_seo_strategist)) {{ in_array($row['emp_id'],$asst_current_seo_strategist) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                            @endforeach
                        </select>   
                    </div>
                    <hr />
                </div>
                <div class="clearfix" style="margin:1%"></div>
                <label for="seo_link_builder_assigned" class="col-md-4 control-label">SEO Link Builder </label>
                <div class="col-md-6">
                    <div class="select-box">
                        <select id="seo_link_builder_assigned" name="seo_link_builder_assigned" class="selectpicker form-control">
                            <option value="" {{ !isset($current_seo_link_builder) ? 'selected' : '' }}>None Selected</option>
                            @foreach($link_builders as $i => $row)
                                <option value="{{ $row['emp_id'] }}" @if(isset($current_seo_link_builder)) {{ in_array($row['emp_id'],$current_seo_link_builder) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                            @endforeach
                        </select>   
                    </div>
                </div>
                <div class="clearfix" style="margin:1%"></div>
                <label for="asst_seo_link_builder_assigned" class="col-md-4 control-label">Assistant SEO Link Builder (Optional) </label>
                <div class="col-md-6">
                    <div class="select-box">
                        <select id="asst_seo_link_builder_assigned" name="asst_seo_link_builder_assigned" class="selectpicker form-control">
                            <option value="" {{ !isset($asst_current_seo_link_builder) ? 'selected' : '' }}>None Selected</option>
                            @foreach($link_builders as $i => $row)
                                <option value="{{ $row['emp_id'] }}" @if(isset($asst_current_seo_link_builder)) {{ in_array($row['emp_id'],$asst_current_seo_link_builder) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                            @endforeach
                        </select>   
                    </div>
                    <hr />
                </div>
            </div>
        @endif

        @if ($user->role == 1 || ($teamcode == 'con' && in_array($user->userInfo->emp_id, $managers)) )
            <div class="panel-body">
                <label for="seo_content_team" class="col-md-4 control-label">SEO Content Team </label>
                <div class="col-md-6">
                    <div class="select-box">
                        @php 
                            $current_seo_content_member = explode('|', $account->seo->seo_content_team); 
                        @endphp
                        <select id="seo_content_team" name="seo_content_team[]" class="multiselect-ui form-control" multiple="multiple">
                            @foreach($content_team as $i => $row)
                                <option value="{{ $row['emp_id'] }}" @if(isset($current_seo_content_member)) {{ in_array($row['emp_id'], $current_seo_content_member) ? 'selected' : '' }} @endif>{{ $row['f_name'] . ' ' . $row['l_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <input id="btn-assign-seo" type="button" class="btn btn-success assign-btn" {{ !empty($current_seo_strategist) || !empty($current_asst_seo_strategist) || !empty($current_seo_link_builder) || !empty($current_asst_seo_link_builder) || !empty($current_seo_content_member) ? '' : 'disabled' }}  value="Assign members">
                {{-- <input id="btn-seo-clear" type="button" class="btn btn-warning assign-btn" {{ !empty($account->seo->seo_team) ? '' : 'disabled' }} value="Clear"> --}}
            </div>
        </div>
      
    </div>
</div>
@endif