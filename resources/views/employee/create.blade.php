@extends('layouts.master_v2')
@section('title')
Crm - Create Employee
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}" />
@endsection
@section('main_content')
<form class="outer-data-box" method="POST" action="{{ route('employee.create') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="main-data-box">
                <div class="data-header-box clearfix">
                    <span class="header-h"><i class="fa fa-plus" aria-hidden="true"></i> Create Employee</span>
                    <div class="data-search-box">
                        <span>
                            Branch:
                            <div class="select-box">
                                <select id="branch" name="branch">
                                <option value="sg" {{ old('branch') == 'sg' ? 'selected' : ''}}>Singapore</option>
                                <option value="ph" {{ old('branch') == 'ph' ? 'selected' : ''}}>Philippines</option>
                                </select>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="true">
                                User Login Details
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Employee ID</label>
                                            <input id="emp_id" type="text" class="form-control" name="emp_id" value="{{ old('emp_id') }}" readonly>
                                            @if ($errors->has('emp_id'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('emp_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Username *</label>
                                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">
                                            @if ($errors->has('username'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('username') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Company Email Address *</label>
                                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Role</label>
                                            <div class="select-box">
                                                <select name="role">
                                                @foreach($roles as $row)
                                                <option value="{{ $row->id }}" {{ old('role') == $row->id ? 'selected' : '' }}>{{ $row->syscode->systemdesc }}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('role'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('role') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-2" aria-expanded="true" class="collapsed">
                                Personal Particulars
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-2" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Firstname *</label>
                                            <input id="f_name" type="text" class="form-control" name="f_name" value="{{ old('f_name') }}">
                                            @if ($errors->has('f_name'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('f_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Lastname *</label>
                                            <input id="l_name" type="text" class="form-control" name="l_name" value="{{ old('l_name') }}">
                                            @if ($errors->has('l_name'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('l_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Gender</label>
                                            <div class="select-box">
                                                <select name="gender">
                                                <option value="M" {{ old('gender') == 'M' ? 'selected' : ''}}>Male</option>
                                                <option value="F" {{ old('gender') == 'F' ? 'selected' : ''}}>Female</option>
                                                </select>
                                                @if ($errors->has('gender'))
                                                <span class="help-block">
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('gender') }}</span>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Nationality *</label>
                                            <input id="nationality" type="text" class="form-control" name="nationality" value="{{ old('nationality') }}">
                                            @if ($errors->has('nationality'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('nationality') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Date of Birth</label>
                                            <input type="text" class="form-control" id="bday" name="bday" value="{{ old('bday') }}" data-date-format="yyyy-mm-dd" readonly>
                                            @if ($errors->has('bday'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('bday') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Religion *</label>
                                            <input type="text" class="form-control" id="religion" name="religion" value="{{ old('religion') }}">
                                            @if ($errors->has('religion'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('religion') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Highest Educational Attainment *</label>
                                            <input type="text" class="form-control" id="highest_education" name="highest_education" value="{{ old('highest_education') }}">
                                            @if ($errors->has('highest_education'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('highest_education') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Marital Status</label>
                                            <div class="select-box">
                                                <select class="form-control" name="civil_status">
                                                @foreach($civil_statuses as $row)
                                                <option value="{{ $row->systemcode }}" {{ old('civil_status') == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('civil_status'))
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('civil_status') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>No of Children</label>
                                            <input type="number" min="0" value="0" class="form-control" id="children" name="children" value="{{ old('children') }}">
                                            @if ($errors->has('children'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('children') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Mobile Number</label>
                                            <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{ old('mobile_number') }}">
                                            @if ($errors->has('mobile_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('mobile_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Landline Number</label>
                                            <input type="text" class="form-control" id="landline_number" name="landline_number" value="{{ old('landline_number') }}">
                                            @if ($errors->has('landline_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('landline_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Personal Email Address *</label>
                                            <input type="text" class="form-control" id="personal_email" name="personal_email" value="{{ old('personal_email') }}">
                                            @if ($errors->has('personal_email'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('personal_email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="nric">
                                    <div class="col-md-6">
                                       <div class="form-box">
                                            <label>NRIC Number *</label>
                                            <input type="text" maxlength="10" class="form-control" id="nric_id" name="nric_id" value="{{ old('nric_id') }}">
                                            @if ($errors->has('nric_id'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('nric_id') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-box">
                                            <label>Current Address *</label>
                                            <textarea name="address" rows="5" class="form-control">{{ old('address') }}</textarea>
                                            @if ($errors->has('address'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-box">
                                            <label>Medical History (if any)</label>
                                            <textarea name="med_history" rows="5" class="form-control">{{ old('med_history') }}</textarea>
                                            @if ($errors->has('med_history'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('med_history') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ph-benefits">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-3" aria-expanded="true" class="collapsed">
                                Government Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-3" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Tin Number</label>
                                            <input type="text" class="form-control" id="tin_number" name="tin_number" value="{{ old('tin_number') }}">
                                            @if ($errors->has('tin_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('tin_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>SSS Number</label>
                                            <input type="text" class="form-control" id="sss_number" name="sss_number" value="{{ old('sss_number') }}">
                                            @if ($errors->has('sss_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('sss_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>PAGIBIG Number</label>
                                            <input type="text" class="form-control" id="pagibig_number" name="pagibig_number" value="{{ old('pagibig_number') }}">
                                            @if ($errors->has('pagibig_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('pagibig_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>PhilHealth Number</label>
                                            <input type="text" class="form-control" id="philhealth_number" name="philhealth_number" value="{{ old('philhealth_number') }}">
                                            @if ($errors->has('philhealth_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('philhealth_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default sg-bank-info">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-4" aria-expanded="true" class="collapsed">
                                Bank Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-4" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Bank Name *</label>
                                            <input type="text" class="form-control" id="bank_name" name="bank_name" value="{{ old('bank_name') }}">
                                            @if ($errors->has('bank_name'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('bank_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Bank Account Number *</label>
                                            <input type="text" class="form-control" id="bank_acc_no" name="bank_acc_no" value="{{ old('bank_acc_no') }}">
                                            @if ($errors->has('bank_acc_no'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('bank_acc_no') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Payee Name (as of bank account) *</label>
                                            <input type="text" class="form-control" id="payee_name" name="payee_name" value="{{ old('payee_name') }}">
                                            @if ($errors->has('payee_name'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('payee_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Bank Code *</label>
                                            <input type="text" class="form-control" id="bank_code" name="bank_code" value="{{ old('bank_code') }}">
                                            @if ($errors->has('bank_code'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('bank_code') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Branch Code *</label>
                                            <input type="text" class="form-control" id="branch_code" name="branch_code" value="{{ old('branch_code') }}">
                                            @if ($errors->has('branch_code'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('branch_code') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-5" aria-expanded="true" class="collapsed">
                                Emergency Contact Person
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-5" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Name *</label>
                                            <input type="text" class="form-control" id="emergency_contact_name" name="emergency_contact_name" value="{{ old('emergency_contact_name') }}">
                                            @if ($errors->has('emergency_contact_name'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('emergency_contact_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Contact Number *</label>
                                            <input type="text" class="form-control" id="emergency_contact_number" name="emergency_contact_number" value="{{ old('emergency_contact_number') }}">
                                            @if ($errors->has('emergency_contact_number'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('emergency_contact_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Relationship *</label>
                                            <input type="text" class="form-control" id="emergency_contact_rel" name="emergency_contact_rel" value="{{ old('emergency_contact_rel') }}">
                                            @if ($errors->has('emergency_contact_rel'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('emergency_contact_rel') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-6" aria-expanded="true" class="collapsed">
                                HR Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-6" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Department</label>
                                            <div class="select-box">
                                                <select class="form-control" id="dept_code" name="dept_code" data-live-search="true">
                                                @foreach($departments as $row)
                                                    <option value="{{ $row->systemcode }}" {{ old('dept_code') == $row->systemcode ? 'selected' : ''}}>{{ $row->systemdesc }}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('department'))
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('depatment') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Designation</label>
                                            <div class="select-box">
                                                <select class="form-control" id="designation" name="designation" data-live-search="true">
                                                @foreach($designations as $row)
                                                <option value="{{ $row->designation_code }}" {{ old('designation') == $row->designation_code ? 'selected' : ''}}>{{ $row->designation }}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('designation'))
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('designation') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="sales-target-container">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Sales Target (Note: For Sales Team)</label>
                                            <input type="text" class="form-control" id="sales_target" name="sales_target" value="{{ old('sales_target') }}">
                                            @if ($errors->has('sales_target'))
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('sales_target') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Status</label>
                                            <div class="select-box">
                                                <select class="selectpicker form-control" name="status" data-live-search="true">
                                                @foreach($statuses as $row)
                                                    <option value="{{ $row->systemcode }}" {{ old('status') == $row->systemcode ? 'selected' : '' }}>{{ $row->systemdesc }}</option>
                                                @endforeach
                                                </select>
                                                @if ($errors->has('status'))
                                                <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('status') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Joined Date *</label>
                                        <input type="text" class="form-control datepicker" id="date_hired" name="date_hired" data-date-format="yyyy-mm-dd" value="{{ old('date_hired') }}" readonly>
                                            @if ($errors->has('date_hired'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('date_hired') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Confirmation Date</label>
                                            <input type="text" class="form-control datepicker" id="confirmation_date" name="confirmation_date" data-date-format="yyyy-mm-dd" value="{{ old('confirmation_date') }}" readonly>
                                            @if ($errors->has('confirmation_date'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('confirmation_date') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label>Exit Date</label>
                                            <input type="text" class="form-control datepicker" id="exit_date" name="exit_date" data-date-format="yyyy-mm-dd" value="{{ old('exit_date') }}" readonly>
                                            @if ($errors->has('exit_date'))
                                            <span class="error-p"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('exit_date') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-buttons clearfix">
                    <div class="buttons-box">
                        <button type="submit" class="cta-btn add-btn">Create Employee</button>
                        <a href="{{ route('employee.list') }}" class="cta-btn cancel-btn">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@section('scripts')
<script src="{{ asset('js/employee/create.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-multiselect.js') }}" type="text/javascript"></script>
<script>
    $('#bday').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yyyy-mm-dd',
        defaultDate: new Date(),
        yearRange: "-100:+0",
        startView: 2
    });

    $('#date_hired').datepicker();

    $('#confirmation_date').datepicker();

    $('#exit_date').datepicker();
    
</script>

@endsection
@endsection