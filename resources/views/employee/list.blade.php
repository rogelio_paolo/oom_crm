@extends('layouts.master_v2')

@section('title')
  Crm - Employees
@endsection

@section('modals')
@include('includes.usermodal')
@include('includes.employeemodal')
@endsection

@section('main_content')


<div class="outer-data-box">

  <div class="main-data-box">

    <div class="data-header-box clearfix">

      <span class="header-h"><i class="fa fa-users" aria-hidden="true"></i> Employees </span>

      <div class="data-search-box">

        <span>
          Search Employee:
          <div class="search-box">
            <input type="text" class="form-control search-area" name="searchEmployee" id="searchEmployee" placeholder="Search" onkeypress="triggerSearch(event)">
            <button type="button" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </span>

      </div>

    </div>

    <div class="data-buttons-box clearfix">

      <div class="filter-box">

        <!-- <a href="#"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a>

        <a href="#"><i class="fa fa-th" aria-hidden="true"></i> Group by</a>

        <a href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i> Sort by</a> -->

        <a href="" id="refresh-table"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh table</a>

      </div>

      <div class="buttons-box">

        <a href="{{ route('employee.create') }}" class="cta-btn add-btn">ADD NEW <i class="fa fa-plus" aria-hidden="true"></i></a>

        {{-- @foreach(Auth::user()->userAccess as $access)
            @if($access->module != 'MD003')
                @continue
            @else
                @if(strpos($access->access,'C') >= 1)
                    <a href="{{ route('employee.create') }}" class="cta-btn add-btn">ADD NEW <i class="fa fa-plus" aria-hidden="true"></i></a>
                @endif
            @endif
        @endforeach

        @php
             $access = Auth::user()->userAccess->toArray();
             $type = json_decode($access[1]['access']);
        @endphp --}}

      </div>

    </div>

    <div class="table-box">
      
      <i id="loader" class="loader hidden"></i>
        
      <table class="table main-table" id="employee-management">

        <thead>
            <tr>
                <!-- <th>Name <a href="" class="sort-user" data-sortby="name"><i id="sort-user" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th> -->
                <th>ID No. <a href="" class="sort-employee" data-sortby="emp_id"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th>Name <a href="" class="sort-employee" data-sortby="l_name"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th>Designation <a href="" class="sort-employee" data-sortby="designation"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th>Status <a href="" class="sort-employee" data-sortby="status"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th>Role <a href="" class="sort-employee" data-sortby="role"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>
                <th>Hired Date <a href="" class="sort-employee active" data-sortby="date_hired"><i id="sort-employee" class="fa fa-sort-alpha-desc" aria-hidden="true"></i></a></th>

                @if($user->role == 1 || $user->role == 3)
                <th>action</th>
                @endif
            </tr>
        </thead>
        <i id="loader" class="loader fa fa-refresh fa-spin hidden" style=""></i>
        <tbody>
            @foreach($data as $row)
            <tr>
                <!-- <td> {{ $row->firstname.' '.$row->lastname }} </td> -->
                <td> {{ $row->emp_id }} </td>
                <td> {{ $row->l_name.', '.$row->f_name.' '.$row->m_name }} </td>
                <td> {{ $row->emp_designation->designation }} </td>
                <td> {{ $row->emp_status->systemdesc }} </td>
                <td> {{ !empty($row->UserInfo) ? $row->UserInfo->userRole->syscode->systemdesc : '' }} </td>
                <td> {{ !empty($row->date_hired) ? date('d M Y', strtotime($row->date_hired)) : 'unspecified' }} </td>
                <td id="employee-management">
                    <a href="{{ route('employee.edit',$row->emp_id) }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                    <a id="employeeDelete" data-employeeid='{{ $row->emp_id }}' class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    @if($user->role == 1)
                    <a id="userRole" data-employeeid='{{ $row->emp_id }}' class="btn btn-primary"><i class="fa fa-wrench"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>

      </table>

       <div class="row">
          <div class="col-md-1">
              <label for="limit_entry">Items per page: </label>
          </div>
          <div class="col-md-2">
              <div class="select-box">
                  <select id="limit_entry" name="limit_entry" class="w-150">
                      <option value="10" {{ !empty($items) ? $items->per_page == 10 ? 'selected' : '' : '' }}>10</option>
                      <option value="20" {{ !empty($items) ? $items->per_page == 20 ? 'selected' : '' : '' }}>20</option>
                      <option value="30" {{ !empty($items) ? $items->per_page == 30 ? 'selected' : '' : '' }}>30</option>
                      <option value="40" {{ !empty($items) ? $items->per_page == 40 ? 'selected' : '' : '' }}>40</option>
                  </select>
              </div>
          </div>
      </div>

    </div>

    <div class="pagination-box">
      {{ $data->render() }}
    </div>

  </div>

</div>


@section('scripts')
     <script src="{{ asset('js/employee/list.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/employee/role.js') }}" type="text/javascript"></script>
@endsection

@endsection
