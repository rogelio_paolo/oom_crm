@extends('layouts.master_v2')
@section('title')
    Crm - View Employee Details
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('css_v1/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}" />
@endsection
@section('main_content')

<div id="app-employee">
    <employee-component></employee-component>
</div>

<div class="clearfix" style="margin-bottom: 2%">&nbsp;</div>


@section('scripts')
<script src="{{ asset('js/employee/edit.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-multiselect.js') }}" type="text/javascript"></script>

@endsection
@endsection