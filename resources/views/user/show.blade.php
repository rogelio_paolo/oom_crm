@extends('layouts.app')

@section('title')
  Crm - User Profile
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-4">
        <div class="span4">
            <blockquote>
                <p>{{ $name }}</p>
                <small><cite title="Source Title">{{ $adrs }}  <i class="icon-map-marker"></i></cite></small>
            </blockquote>
            <p>
                <i class="fa fa-building"></i> {{ $department }}<br>
                <i class="fa fa-suitcase"></i> {{ $work }}<br>
                <i class="fa fa-envelope"></i> {{ $email }}<br>
                <i class="fa fa-gift"></i> {{ $bday }}<br>
            </p>
        </div>
    </div>
@endsection
