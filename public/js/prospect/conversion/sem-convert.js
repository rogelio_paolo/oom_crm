function isNumberKey(e, length) {
    var charCode = (e.which) ? e.which : e.keyCode,
        other_duration = $('input[id="other_duration"]');

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

$(document).on('click', 'input[id="has_historical_data"]', function () {

    let has_historical_data = $(this).val();

    if (has_historical_data == '1')
        $('div.file-uploading').removeClass('hidden');
    else
        $('div.file-uploading').addClass('hidden');

});

// SEM CAMPAIGN DURATION
$(document).on('click', 'input[id="ad_scheduling"]', function () {
    var ad_scheduling = $(this).val(),
        budget_field = $('input[id="google_daily_budget"]'),
        monthly_field = $('input[id="google_monthly_budget"]').val(),
        duration_other = $('input[id="duration_other"]').val(),
        ad_sched_remark = $('#ad_scheduling_remark');

    if(ad_scheduling === '1'){
        budget_field.val('');
        budget_field.removeAttr('readonly');
        ad_sched_remark.removeClass('hidden');

            if ($('span#sem_daily_budget').length <= 0){
                budget_field.after('<span class="error-p account" id="sem_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
            }
            else{
                return false;
            }
    } else {
        $("#select#duration").multiselect("clearSelection");

        ad_sched_remark.addClass('hidden');
        ad_sched_remark.val('');
        if (monthly_field !== '') {
            daily_result = calculateDailyBudget(duration_other, true);
            number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            budget_field.val(number);
            budget_field.attr('readonly','readonly');
            $('span#sem_daily_budget').remove();
        } else {
            budget_field.val('');
            budget_field.attr('readonly','readonly');
            $('span#sem_daily_budget').remove();
        }          
    }
});

$(document).on('change', 'input[id="total_budget"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});
