function serviceOtherField(services)
{
    if($.inArray('SV999',services) !== -1)
        $('input[id="service_other"]').removeClass('hidden');
    else
        $('input[id="service_other"]').addClass('hidden'),
        $('input[id="services_array"]').val(services);
}

function sourceOtherField(sources)
{
    if(sources === 'SL999')
        $('input[id="source_other"]').removeClass('hidden');
    else
        $('input[id="source_other"]').addClass('hidden');
}

function landingOtherField(landings)
{
    if(landings === 'LP999')
        $('input[id="landing_other"]').removeClass('hidden');
    else
        $('input[id="landing_other"]').addClass('hidden');
}

function isNumberKey(e, length) {
    var charCode = (e.which) ? e.which : e.keyCode,
        other_duration = $('input[id="other_duration"]');

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

// Input total budget change into parse float
$(document).on('change', 'input[id="contract_value"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

$(document).on('change', 'select[id="currency"]', function() {
    var value = $(this).val();
    
    $('span[id="display-currency"]').html(value);
});



(function(){

    $('span[id="display-currency"]').html($('select[id="currency"]').val());

    $('select#services').on({
        'ready': function(e){
            serviceOtherField($(this).val());
        },
        'change': function(e){
            serviceOtherField($(this).val());
        }
    });

    $('select#sources').on({
        'ready': function(e){
            sourceOtherField($(this).val());
        },
        'change': function(e){
            sourceOtherField($(this).val());
        }
    });

    $('select#landings').on({
        'ready': function(e){
            landingOtherField($(this).val());
        },
        'change': function(e){
            landingOtherField($(this).val());
        }
    });

    // $(document).on('change','select#services', function(){
    //     var selected = this.value;
    //
    //     if (selected !== '')
    //         $('input[id="services_null"]').remove();
    //     else
    //         $(this).next().append('<input type="hidden" id="services_null" name="services" />');
    //
    // });

    $(document).on('click', 'a#edit-note', function() {
        var noteID = $(this).data('editnote'),
            note = $('span#lead-note-'+noteID),
            currNote = note.text(),
            thisNote = $('<textarea id="lead-note-'+noteID+'" class="form-control" rows="10" />'),
            editBtn = $('a[data-editnote="'+noteID+'"]'),
            delBtn = $('a[data-delnote="'+noteID+'"]');

        note.parent().css('background','#fff');
        thisNote.html((note.html()).replace(/<br\s*[\/]?>/gi, '\n'));
        editBtn.after('<a id="save-note" data-savenote="'+noteID+'" class="btn btn-success"><i class="fa fa-save"></i> Save</a>');
        editBtn.remove();
        delBtn.after('<a id="cancel-note" data-cancelnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-close"></i> Cancel</a>');
        delBtn.remove();
        note.replaceWith(thisNote);
        thisNote.focus();
    });
    
    $(document).on('click','a#cancel-note', function() {
        var noteID = $(this).data('cancelnote'),
            note = $('textarea#lead-note-'+noteID),
            currNote = note.text(),
            thisNote = $('<span id="lead-note-'+noteID+'"></span>'),
            saveBtn = $('a[data-savenote="'+noteID+'"]'),
            cancelBtn = $('a[data-cancelnote="'+noteID+'"]');

        note.parent().removeAttr('style');
        thisNote.html((note.text()).replace(/(?:\r\n|\r|\n)/g, '<br />'));
        saveBtn.after('<a id="edit-note" data-editnote="'+noteID+'" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>');
        saveBtn.remove();
        cancelBtn.after('<a id="delete-note" data-delnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>');
        cancelBtn.remove();
        note.replaceWith(thisNote);
    });

    $(document).on('click','a#save-note', function() {
        var noteID = $(this).data('savenote'),
            note = $('textarea#lead-note-'+noteID),
            currNote = note.val(),
            thisNote = $('<span id="lead-note-'+noteID+'"></span>');
            saveBtn = $('a[data-savenote="'+noteID+'"]'),
            cancelBtn = $('a[data-cancelnote="'+noteID+'"]'),
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
            data = {_token:CSRF_TOKEN,id:noteID, note:currNote};

            $.ajax({
                type: 'patch',
                url: $('base').attr('href')+'/lead/note/update',
                data: data,
                success:function(data){
                    thisNote.html((data).replace(/(?:\r\n|\r|\n)/g,'<br />'));
                }
            });

            note.parent().removeAttr('style');
            saveBtn.after('<a id="edit-note" data-editnote="'+noteID+'" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>');
            saveBtn.remove();
            cancelBtn.after('<a id="delete-note" data-delnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>');
            cancelBtn.remove();
            note.after(thisNote);
            note.remove();

    });

    $(document).on('click','a#delete-note', function() {
        var noteID = $(this).data('delnote'),
            note = $('span#lead-note-'+noteID);
            $('input[id="delete-leadnote"]').val(noteID);
            $('#deleteNoteModal').modal('show');

    });

    $(document).on('click','a#delete-leadnote', function() {
        var noteID = $('input[id="delete-leadnote"]').val(),
            note = $('span#lead-note-'+noteID),
            prospectID = $('input[id="prospectid"]').val(),
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
            data = {_token: CSRF_TOKEN, note_id: noteID, prospect_id: prospectID};

            $.ajax({
                url: $('base').attr('href')+'/lead/note/delete',
                type: 'patch',
                data: data,
                success:function(data){
                }
            });

            note.parent().parent().next().remove();
            note.parent().parent().remove();
            $('#deleteNoteModal').modal('hide');
    });

    // Create Lead button overlay
    $(document).on('click', '#submit_lead', function() {
        $('div.loader-overlay').removeClass('hidden');
    });
    
    $(document).on('change', 'select#sources', function() {
        var source = this.value;
        
        if (source == 'SL002') 
            $('div.landing-page').addClass('hidden');
        else
            $('div.landing-page').removeClass('hidden');
    });

     // Update lead button with extra effect
    $(document).ready(function(){
        $('button#btn-update-prospect').addClass('disable-btn').attr('disabled',true);

        var fields = ['input, select, textarea#street_address'];

        for(var i = 0; i < fields.length; i++)
        {
           $(document).on('keyup change', fields[i], function() {
                if($(this).val().length !=0){
                    $('button#btn-update-prospect').removeClass('disable-btn').attr('disabled', false);
                }
                else
                {
                    $('button#btn-update-prospect').addClass('disable-btn').attr('disabled', true);        
                }
           });
        }
    });

    

})();
