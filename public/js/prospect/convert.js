function serviceOtherField(services)
{
    if($.inArray('SV999',services) !== -1)
        $('input[id="service_other"]').removeClass('hidden');
    else
        $('input[id="service_other"]').addClass('hidden'),
        $('input[id="services_array"]').val(services);
}

function sourceOtherField(sources)
{
    if(sources === 'SL999')
        $('input[id="source_other"]').removeClass('hidden');
    else
        $('input[id="source_other"]').addClass('hidden');
}

function landingOtherField(landings)
{
    if(landings === 'LP999')
        $('input[id="landing_other"]').removeClass('hidden');
    else
        $('input[id="landing_other"]').addClass('hidden');
}

function isNumberKey(e, length) {
    var charCode = (e.which) ? e.which : e.keyCode,
        other_duration = $('input[id="other_duration"]');

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function create_new_category(input){
    if(input.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/))
        return true; //if created successfully otherwise return false
}

// Input total budget change into parse float
$(document).on('change', 'input[id="contract_value"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

$('select#email').selectize({
    create: function(input) {
        //This function will create the category on the server side
        if(create_new_category(input)){
            return {
                value: input,
                text: input
            }
        }
        return false;
    }
});



(function(){
    $('select#services').on({
        'ready': function(e){
            serviceOtherField($(this).val());
        },
        'change': function(e){
            serviceOtherField($(this).val());
        }
    });

    $('select#sources').on({
        'ready': function(e){
            sourceOtherField($(this).val());
        },
        'change': function(e){
            sourceOtherField($(this).val());
        }
    });

    $('select#landings').on({
        'ready': function(e){
            landingOtherField($(this).val());
        },
        'change': function(e){
            landingOtherField($(this).val());
        }
    });

    $(document).on('click', 'button.btn-success', function () {
        $('div.loader-overlay').removeClass('hidden');
    });

    $(document).on('change', 'select#sources', function() {
        var source = this.value;
        
        if (source == 'SL002') 
            $('div.landing-page').addClass('hidden');
        else
            $('div.landing-page').removeClass('hidden');
    });
})();