function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function pagination(pagination,prospectRow,route){
    var baseUrl = $('base').attr('href'),
        url = pagination.path + '?page=';
        var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#prospect-management > tbody');

        if(pagination.last_page > 1){
            
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else {
            $('ul.pagination').addClass('hidden');
        }
        //table.hide();
        
        table.html(prospectRow).fadeIn('fast');
}

function getPagination(uri) {
    var prospectRow = "",
        base_url = $('base').attr('href');

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('table#prospect-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading();
        },
        success:function(data){
            loaded();
            // modify();
            if (data.result.length > 0){
                $.each(data.result,function(i,j){
                    prospectRow += "<tr>";
                    prospectRow += "<td class='text-center overlay'>"+j.company+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.name+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.services+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.status+"</td>";
                    data.role == 1 ? prospectRow += "<td class='text-center overlay'>"+j.created_by+"</td>" : '';
                    prospectRow += "<td class='text-center overlay'>"+j.created_date+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.ex_client+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.locked+"</td>";
                    prospectRow += "<td class='text-center text-nowrap overlay' id='prospect-management'> ";
                    
                    if(j.status != 'Converted') {
                        if($.inArray(data.role,[3,4]) === -1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ) {
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode, ['acc','busi']) !== -1 ? "<a href='/prospect/view/" + j.id + "' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " + "<a href='/prospect/edit/"+j.id+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " : '' ); 
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode,['acc']) >= 0 || j.created_by == data.user.user_info.f_name+' '+data.user.user_info.l_name ? "<a id='prospectDelete' data-prospectid="+j.id+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a> " : '');
                            prospectRow += (j.opportunity_exist === false ? '<a href="/prospect/convert/'+j.id+'/step-1" class="btn btn-primary btn-sm"><i class="fa fa-sign-out"></i> Convert</a> ' : '');
                            prospectRow +=  '<button data-prospect-id="'+ j.id +'" id="btnAddOpportunity" class="btn btn-sm btn-info" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Add Opportunity</button>';
                        } else {
                            prospectRow += "<a href='/prospect/view/"+j.id+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> ";
                    }
                    } else {
                        prospectRow += 'No actions';
                    }
    
                
                    prospectRow += "</td></tr>";
                });

                pagination(data.pagination,prospectRow);
                $('ul.pagination').removeClass('hidden');
            }
            else{
                prospectRow = "<tr><td colspan='6' class='text-center'>No prospects found</td></tr>";

                $('ul.pagination').addClass('hidden');
            }

            var route = window.location.href;
            
            pagination(data.pagination,prospectRow,route);

        }
    });
}

function clearSorting(){
    $('i#sort-prospect').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created_at')
            $(this).parent().addClass('active');
    });
}

function sortProspect(url){
    var prospectRow = "",
        base_url = $('base').attr('href');

    $.ajax({
        url: url,
        beforeSend:function(){
            $('table#prospect-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading();

        },
        success:function(data){
            loaded();
            // modify();
            if(data.result.length > 0) {
                $.each(data.result, function (i, j){
                    prospectRow += "<tr>";
                    prospectRow += "<td class='text-center overlay'>"+j.company+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.name+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.services+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.status+"</td>";
                    data.role == 1 ? prospectRow += "<td class='text-center overlay'>"+j.created_by+"</td>" : '';
                    prospectRow += "<td class='text-center overlay'>"+j.created_date+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.ex_client+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.locked+"</td>";
                    prospectRow += "<td class='text-center text-nowrap overlay' id='prospect-management'> ";
                    
                    if(j.status != 'Converted') {
                        if($.inArray(data.role,[3,4]) === -1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ) {
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode, ['acc','busi']) !== -1 ? "<a href='/prospect/view/" + j.id + "' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " + "<a href='/prospect/edit/"+j.id+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " : '' ); 
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode,['acc']) >= 0 || j.created_by == data.user.user_info.f_name+' '+data.user.user_info.l_name ? "<a id='prospectDelete' data-prospectid="+j.id+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a> " : '');
                            prospectRow += (j.opportunity_exist === false ? '<a href="/prospect/convert/'+j.id+'/step-1" class="btn btn-primary btn-sm"><i class="fa fa-sign-out"></i> Convert</a> ' : '');
                            prospectRow +=  '<button data-prospect-id="'+ j.id +'" id="btnAddOpportunity" class="btn btn-sm btn-info" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Add Opportunity</button>';
                        } else {
                            prospectRow += "<a href='/prospect/view/"+j.id+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> "; 
                        }
                    } else {
                        prospectRow += 'No actions';
                    }
    
    
                    prospectRow += "</td></tr>";
                });
            } else {
                prospectRow = "<tr><td colspan='7' class='text-center'>No prospects to sort</td></tr>";
            }
            

            var route = '/prospect/sort/' + data.field + '/' + data.order;
            
            pagination(data.pagination,prospectRow,route);
            $('ul.pagination').removeClass('hidden');
        }
    });
}

function searchProspect(){
    var prospect = $('input[id="searchProspect"]').val(),
        key = prospect === '' ? 'null' : prospect,
        base_url = $('base').attr('href');

    if (prospect === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/prospect/search/' + key,
        beforeSend:function(){
            $('table#prospect-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded();
            // modify();
            var prospectRow = "",
                tblRow = $('table#prospect-management > tbody');
            if (data.result.length > 0) {
                $.each(data.result,function(i,j){
                    prospectRow += "<tr>";
                    prospectRow += "<td class='text-center overlay'>"+j.company+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.name+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.services+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.status+"</td>";
                    data.role == 1 ? prospectRow += "<td class='text-center overlay'>"+j.created_by+"</td>" : '';
                    prospectRow += "<td class='text-center overlay'>"+j.created_date+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.ex_client+"</td>";
                    prospectRow += "<td class='text-center overlay'>"+j.locked+"</td>";
                    prospectRow += "<td class='text-center text-nowrap overlay' id='prospect-management'> ";

                    if(j.status != 'Converted') {
                        if($.inArray(data.role,[3,4]) === -1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ) {
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode, ['acc','busi']) !== -1 ? "<a href='/prospect/view/" + j.id + "' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " + "<a href='/prospect/edit/"+j.id+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " : '' ); 
                            prospectRow += (data.role === 1 || $.inArray(data.teamcode,['acc']) >= 0 || j.created_by == data.user.user_info.f_name+' '+data.user.user_info.l_name ? "<a id='prospectDelete' data-prospectid="+j.id+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a> " : '');
                            prospectRow += (j.opportunity_exist === false ? '<a href="/prospect/convert/'+j.id+'/step-1" class="btn btn-primary btn-sm"><i class="fa fa-sign-out"></i> Convert</a> ' : '');
                            prospectRow +=  '<button data-prospect-id="'+ j.id +'" id="btnAddOpportunity" class="btn btn-sm btn-info" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Add Opportunity</button>';
                        } else {
                            prospectRow += "<a href='/prospect/view/"+j.id+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> "; 
                        }
                    } else {
                        prospectRow += 'No actions';
                    }
    

                    prospectRow += "</td></tr>";
                });
                
                pagination(data.pagination,prospectRow);
            }
            else{
                prospectRow = "<tr><td colspan='7' class='text-center'>No results found for "+prospect+"</td></tr>";

                tblRow.html(prospectRow).fadeIn('fast');
                $('ul.pagination').addClass('hidden');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchProspect();
    }
}

function modify() {
    $.ajax({
        beforeSend : function() {
            // The loader overlay will show
            $('i#loader-modal').removeClass('hidden');
        },
        success : function() {
            // If not checked, the column will hide
                $('input:checkbox:not(:checked)').each(function() {
                    var column = "table ." + $(this).attr("name");
                    $(column).hide();
                });
                
                // If checked, the column will show
                $('input:checkbox:checked').each(function() {
                    var column = "table ." + $(this).attr("name");
                    $(column).show();
                });

            // The loader overlay and modal will hide
            $('i#loader-modal').addClass('hidden');
            $('#modifyColumn').modal('hide');
            
            if(num > 6 && num < 8)
                $('#columns').addClass('account-cols-7');
            else
                if(num == 8)
                    $('#columns').addClass('account-cols-8');
                else
                    if($('#columns').hasClass('account-cols-7'))
                        $('#columns').removeClass('account-cols-7');
                    else if($('#columns').hasClass('account-cols-8'))
                        $('#columns').removeClass('account-cols-8');
                        
        },
        error : function(http, status, error) {
              console.log('Some errors: ' + error);
        }
    });

}

(function(){
    var root = $('base').attr('href');
    
    $(document).on('click','a#prospectDelete', function(){
        var prospectid = $(this).data('prospectid');
        
        $('#delete-prospectid').val(prospectid);
        $('#deleteProspectModal').modal('show');
    });
    
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if (url) {
            if (url.indexOf('list') >= 1)
                getPagination(url);
            else if (url.indexOf('prospectsearch') >= 1)
                searchProspect();
            else
                sortProspect(url);
        }

    });
    
    $(document).on('click','a.sort-prospect', function(e){
        e.preventDefault();
        var currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        // old algorithm
        // if (currOrder.indexOf('asc') >= 1)
        //     $('i#sort-account').each(function(){
        //         $(this).removeClass('fa-sort-alpha-asc');
        //         $(this).addClass('fa-sort-alpha-desc');
        //     });
        // else
        //     $('i#sort-account').each(function(){
        //         $(this).removeClass('fa-sort-alpha-desc');
        //         $(this).addClass('fa-sort-alpha-asc');
        //     });

        $(this).addClass('active');
        $(this).parent().siblings().find('a').removeClass('active');

        if (currOrder.indexOf('asc') >= 1)
            $(this).find('i').removeClass('fa-sort-alpha-asc'),
            $(this).find('i').addClass('fa-sort-alpha-desc');
        else
            $(this).find('i').removeClass('fa-sort-alpha-desc'),
            $(this).find('i').addClass('fa-sort-alpha-asc');

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

        var url = root + '/prospect/sort/' + sortBy + '/' + sortOrder;

        sortProspect(url);
    });
    
    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        var url = $('base').attr('href') + '/prospect/refresh';

        clearSorting()

        getPagination(url);
        $('#searchProspect').val('');
    });
    
    $(document).on('click', 'button.search-btn', function() {
        searchProspect();
    });

    $(document).on('change', 'select#limit_entry', function() {
        var baseUrl = $('base').attr('href');

        $.ajax({
            url : baseUrl + '/items/prospects/' +  this.value,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                loaded();
                location.reload();
            }
        })
    });


})();