/* GLOBAL VARIABLES */
const modal = 'div[id="prospect_opportunity_modal_2"] ',
    formElement = 'form[id="form-prospect-opp"] ',
    divTableOpportunity = $('div[id="table-div-opportunity"]'),
    tableOpportunityInfos = $(`${modal} tbody[id="opportunity-infos"]`),
    btnCreate = $('button[id="btnCreateNew"]'),
    formProspectOpp = $(`${modal} form[id="form-prospect-opp"]`),
    prospectID = $(`${formElement} input[id="prospect_id"]`),
    opportunityID = $(`${formElement} input[id="opportunity_id"]`),
    services = $(`${formElement} select[id="services"]`),
    currency = $(`${formElement} select[id="currency"]`),
    contractType = $(`${formElement} select[id="contract_type"]`),
    contractValue = $(`${formElement} input[id="contract_value"]`),
    clientName = $(`${formElement} input[id="client_name"]`),
    mobileNumber = $(`${formElement} input[id="mobile_number"]`),
    officeNumber = $(`${formElement} input[id="office_number"]`),
    designation = $(`${formElement} input[id="designation"]`),
    email = $(`${formElement} input[id="email"]`),
    address = $(`${formElement} textarea[id="address"]`),
    city = $(`${formElement} input[id="city"]`),
    postalCode = $(`${formElement} input[id="postal_code"]`),
    source = $(`${formElement} select[id="source"]`),
    industry = $(`${formElement} select[id="industry"]`),
    landingPage = $(`${formElement} select[id="landing_page"]`),
    websiteURL = $(`${formElement} textarea[id="website_url"]`),
    status = $(`${formElement} select[id="status"]`);

let isTouch = false,
    newAdd = false;

/* END OF GLOBAL VARIABLES */

formProspectOpp.validate({
    rules: {
        email: {
            required: true,
            email: true
        }
    }
});

// Create New Button then show the form input fields
$(document).on('click', `${modal} button[id="btnCreateNew"], ${modal} a[id="createNewOpportunity"]`, (e) => {
    e.preventDefault();

    formProspectOpp.removeClass("hidden");
    origForm = formProspectOpp.serialize();
    btnCreate.attr('disabled', 'disabled');

    $('form :input').on('change input', function() {
        btnCreate.removeAttr('disabled');
        isTouch =  true;
    });

    if (isTouch === true) {
        if (contractType.val() === '' || contractValue.val() === '' || clientName.val() === '' || mobileNumber.val() === '' || email.val() === '' ||
                source.val() === '' || status.val() === '' || services.val() === '' || currency.val() === '') {
            swal({
                type: 'error',
                title: 'Warning...',
                text: 'Please input required fields'
            });

            if (contractType.val() === '')
                $('p[id="error-contract-type"]').removeClass('hidden').text('Contract Type is required');
            else
                $('p[id="error-contract-type"]').addClass('hidden').text('');

            if (contractValue.val() === '')
                $('p[id="error-contract-value"]').removeClass('hidden').text('Contract Value is required');
            else
                $('p[id="error-contract-value"]').addClass('hidden').text('');

            if (clientName.val() === '')
                $('p[id="error-client-name"]').removeClass('hidden').text('Client Name is required');
            else
                $('p[id="error-client-name"]').addClass('hidden').text('');

            if (mobileNumber.val() === '')
                $('p[id="error-mobile-number"]').removeClass('hidden').text('Mobile Number is required');
            else
                $('p[id="error-mobile-number"]').addClass('hidden').text('');

            if (email.val() === '')
                $('p[id="error-email"]').removeClass('hidden').text('Email Address is required');
            else
                $('p[id="error-email"]').add('hidden').text('');

            if (source.val() === '')
                $('p[id="error-source"]').removeClass('hidden').text('Source of Lead is required');
            else
                $('p[id="error-source"]').addClass('hidden').text('');

            if (status.val() === '')
                $('p[id="error-source"]').removeClass('hidden').text('Status is required');
            else
                $('p[id="error-source"]').addClass('hidden').text('');

            if (services.val() === '')
                $('p[id="error-services"]').removeClass('hidden').text('Services is required');
            else
                $('p[id="error-services"]').addClass('hidden').text('');

            if (currency.val() === '')
                $('p[id="error-currency"]').removeClass('hidden').text('Currency is required');
            else
                $('p[id="error-currency"]').addClass('hidden').text('');

            return false;
        }

        var form = {
            opportunityForm : {
                _token: $('meta[name="csrf-token"]').attr('content'),
                prospect_id: prospectID.val(),
                opportunity_id: opportunityID.val(),
                contract_type: contractType.val(),
                currency: currency.val(),
                contract_value: contractValue.val(),
                client_name: clientName.val(),
                mobile_number: mobileNumber.val(),
                office_number: officeNumber.val(),
                designation: designation.val(),
                email: email.val(),
                address: address.val(),
                city: city.val(),
                postal_code: postalCode.val(),
                industry: industry.val(),
                landing_page: landingPage.val(),
                website_url: websiteURL.val(),
                source: source.val(),
                status: status.val(),
                services: services.val()
            }
        };

        $.ajax({
            url: '/opportunity/store',
            type: 'POST',
            data: form,
            beforeSend: function() {
                console.log('Loading...');
            },
            success: function(res) {
                newAdd = true;

                swal({
                    type: 'success',
                    title: 'Success...',
                    text: 'Successfully created the opportunity'
                });

                if (divTableOpportunity.is(":hidden")) {
                    divTableOpportunity.removeClass('hidden');
                    $('p.text-center').addClass('hidden');

                }

                if ($('tbody[id="opportunity-infos"] tr').length > 2 ) {
                    divTableOpportunity.addClass('scrollable');
                }

                appendData(res.opportunity);

                isTouch = false;
                btnCreate.removeAttr('disabled');
                formProspectOpp.addClass("hidden");
                resetTheFormFields();
            }
        });

    }

});


$(document).on('click', `${modal} button[data-dismiss="modal"]`, () => {
    prospectID.val('');
    opportunityID.val('');

    if (newAdd == true)
        location.reload();
});

// Button for hiding the form
$(document).on('click', `${modal} button[id="btnHideForm"], ${modal} button[data-dismiss="modal"]`, (ev) => {
    ev.preventDefault();

    isTouch = false;
    btnCreate.removeAttr('disabled');
    formProspectOpp.addClass("hidden");

    resetTheFormFields();

});

// Input Contract Value change into parse float
$(document).on('change', 'input[id="contract_value"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

$(`${modal} input[id="contract_value"]`).on({
    'change': function (e) {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    },
    'ready': function (e) {
        var currency = $(this).val();
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    }
});

// Change the currency text in group add on
$(`${modal} select[id="currency"]`).on({
    'change': function (e) {
        var currency = $(this).val();
        $('span[id="input-group-currency"]').text(currency);
    },
    'ready': function (e) {
        var currency = $(this).val();
        $('span[id="input-group-currency"]').text(currency);
    }
});

$(document).on('click', `[data-target="#prospect_opportunity_modal_2"]`, function() {
    const id = $(this).data('prospect-id'),
            noOpportunityMsg = $(`${modal} p.text-center`),
            divOpportunity = $(`${modal} div[id="table-div-opportunity"]`);

    $(`${modal} form[id="form-prospect-opp"]`).addClass("hidden");
    noOpportunityMsg.addClass('hidden');
    divOpportunity.addClass('hidden');
    divTableOpportunity.removeClass('scrollable');

    prospectID.val(id);

    $.ajax({
        url: `/opportunity/api/getProspect/${id}`,
        type: 'GET',
        beforeSend: function() {
         console.log('Loading...');
        },
        success: function(res) {
            let row = '';
            const tableOpportunity = $(`${modal} table[id="table-opportunity"]`),
                    tableOpportunityInfos = $(`${modal} table[id="table-opportunity"] tbody[id="opportunity-infos"]`);

            if (res.opportunities.length > 0) {
                divOpportunity.removeClass('hidden');
                $.each(res.opportunities, function(i, j) {
                    $.each(j.infos, function(k, l) {
                        row += '<tr>';
                        row += '<td>'+ l.client_name +'</td>';
                        row += '<td>'+ l.contract_type +'</td>';
                        row += '<td>'+ l.contract_value +'</td>';
                        row += '<td>'+ l.services +'</td>';
                        row += '<td>'+ l.status +'</td>';
                        row += '<td>'+ l.mobile_number +'</td>';
                        row += '<td>'+ l.created_at +'</td>';
                        row += '<td>'+ l.created_by +'</td>';
                        row += '<td class="text-nowrap">';

                        row += (l.status !== 'Converted' ? '<button id="btnDirectConvert" data-opportunityid="'+l.id+'" class="btn btn-primary"><i class="fa fa-sign-out"></i></button> ' +
                                '<button id="btnDeleteOpportunity"  class="btn btn-danger"><i class="fa fa-remove"></i></button>' : '<span>No Actions</span>') +
                                    '</td>';

                        opportunityID.val(l.opportunity_id);
                    })
                });

                tableOpportunityInfos.html(row);

                if ($('tbody[id="opportunity-infos"] tr').length > 2 ) {
                    divTableOpportunity.addClass('scrollable');
                }
            } else {
                noOpportunityMsg.removeClass('hidden');
                tableOpportunityInfos.html('');
            }


        }
    });
});

$(document).on('click', 'button[id="btnDeleteOpportunity"]', function() {
    swal(
        'Delete this opportunity?',
        'Go to Opportunities Module',
        'question'
    )
});

$(document).on('click', 'button[id="btnDirectConvert"]',  function() {
  const opportunityID = $(this).data('opportunityid');

  $.ajax({
      url: '/opportunity/api/getOpportunityInfo/' + opportunityID,
      type: 'get',
      success: function(response) {
          var data = response.opportunity;

          const form = {
                  _token: $('meta[name="csrf-token"]').attr('content'),
                  id: opportunityID,
                  client_name: data.client_name,
                  contract_type: data.contract_type,
                  contract_value: data.contract_value,
                  created_at: data.created_at,
                  created_by: data.created_by,
                  designation: data.designation,
                  email: data.email,
                  industry: data.industry,
                  landing_page: data.landing_page,
                  mobile_number: data.mobile_number,
                  office_number: data.office_number,
                  opportunity_id: data.opportunity_id,
                  other_landing: data.other_landing,
                  other_services: data.other_services,
                  other_source: data.other_source,
                  postal_code: data.postal_code,
                  prospect_id: prospectID.val(),
                  services: data.services,
                  source: data.source,
                  status: data.status,
                  updated_at: data.updated_at,
                  updated_by: data.updated_by,
                  website_url: data.website_url
          };

          swal({
              title: 'Confirmation Message!',
              text: "Are you sure you want to convert this opportunity info?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
          }).then((result) => {
              if (result.value) {
                  $.ajax({
                      url: '/opportunity/store_session2',
                      type: 'POST',
                      data: form,
                      success: function(response) {
                          console.log('Successfully added the session');
                          window.location.href = "/prospect/convert/" + form.prospect_id + "/step-1";
                      }
                  })
              }
          })
      }
  })




});

function resetTheFormFields() {

    services.val('NP001');
    services.multiselect("refresh");

    contractType.val('CT001');
    contractType.selectpicker("refresh");

    currency.val('SGD');
    currency.selectpicker("refresh");

    contractValue.val('');
    clientName.val('');
    mobileNumber.val('');
    officeNumber.val('');
    designation.val('');
    email.val('[]');
    address.val('');
    city.val('');
    postalCode.val('');

    industry.val('ID999');
    industry.selectpicker('refresh');

    landingPage.val('LP001');
    landingPage.selectpicker('refresh');

    websiteURL.val('');

    source.val('SL002');
    source.selectpicker("refresh");

    status.val('LS002');
    status.selectpicker('refresh');

    $('p[id="error-services"]').addClass('hidden').text('');
    $('p[id="error-contract-type"]').addClass('hidden').text('');
    $('p[id="error-currency"]').addClass('hidden').text('');
    $('p[id="error-contract-value"]').addClass('hidden').text('');
    $('p[id="error-client-name"]').addClass('hidden').text('');
    $('p[id="error-mobile-number"]').addClass('hidden').text('');
    $('p[id="error-email"]').addClass('hidden').text('');
    $('p[id="error-source"]').addClass('hidden').text('');
    $('p[id="error-status"]').addClass('hidden').text('');

}

function appendData(opportunity) {
    tableOpportunityInfos.append(`
        <tr>
            <td>${opportunity.client_name}</td>
            <td>${opportunity.contract_type}</td>
            <td>${opportunity.contract_value}</td>
            <td>${opportunity.services}</td>
            <td>${opportunity.status}</td>
            <td>${opportunity.mobile_number}</td>
            <td>${opportunity.created_at}</td>
            <td>${opportunity.created_by}</td>
                <td class="text-nowrap">
                    <button id="btnDirectConvert" class="btn btn-primary" data-opportunityid="${opportunity.id}">
                        <i class="fa fa-sign-out"></i>
                    </button> 
                    <button id="btnDeleteOpportunity" class="btn btn-danger">
                        <i class="fa fa-remove"></i>
                    </button>
                </td>
            </tr>
        `);
}




