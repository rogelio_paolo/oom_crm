function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '&lt;br&gt;' : '&lt;br&gt;';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function decodeHtml(html) {
    var txt = document.getElementById("eventRemark");
    txt.innerHTML = html;
    return txt.value;
}

function ReloadPage() {
    location.reload();
}

(function (){
    /* PREVENT INSPECT ELEMENT AND F12 KEY */
    // document.onkeydown = function(e) {
    //     if(event.keyCode == 123) {
    //         return false;
    //     }
    //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
    //         return false;
    //     }
    //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
    //         return false;
    //     }
    //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
    //         return false;
    //     }
    //     if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
    //         return false;
    //     }
    // }

    /* ACTIVATE MULTIPLE EMAIL FIELD TAGGING */
    $('input[id="email"]').multiple_emails();
    /* END OF ACTIVATE MULTIPLE EMAIL FIELD TAGGING */

    
    $(document).ready(function(){
        var baseUrl = $('base').attr('href');

        $.ajax({
           url: baseUrl + '/checkauth',
            success:function(data){
            }
        });

        var d = new Date();
        d = d.getTime();

        if ($('#reloadValue').val().length === 0)
        {
            $('#reloadValue').val(d);
            $('body').show();
        }
        else
        {
            $('#reloadValue').val('');
            setTimeout("ReloadPage()", 100);
        }
    });

    $(document).on('click', 'span#notification-clear', function(e) {
        e.preventDefault();

        swal({
            title: 'Are you sure you want to clear all notifications?',
            text: "This action cannot be undo",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                swal(
                    'Notification cleared!',
                     '',
                    'success'
                );
                window.location.href = $('base').attr('href') + '/notification/clear';
            }
        });
    });

    $(document).on('click', 'span#reminder-clear', function(e) {
        e.preventDefault();

        swal({
            title: 'Are you sure you want to clear all reminders?',
            text: "This action cannot be undo",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                swal(
                    'Reminders cleared!',
                     '',
                    'success'
                );
                window.location.href = $('base').attr('href') + '/reminder/clear';
            }
        });
    });

    var pendingList = '';

    $.ajax({
        url: $('base').attr('href') + '/appointment/pending',
        type: 'get',
        success: function(data) {
            // console.log(data);
            if(data.appointment.length > 0 && data.login_count_today == 1) {
                setTimeout(showPopupModal, 3000);
            
                $.each(data.appointment, function(i, j) {
                    pendingList += `
                    <tr data-appointment-id="${j.id}" class="text-center">
                        <td>
                            <button class="btn btn-sm btn-danger circle" disabled="disabled">A</button>
                        </td>
                        <td>${j.scheduled_date}</td>
                        <td>${j.company}</td>
                        <td>${j.status}</td>
                        <td class="text-nowrap">
                            <button data-appointment-id="${j.id}" class="btn btn-sm btn-success pending" title="Mark as Completed"><i class="fa fa-check"></i></button>
                            <button data-appointment-id="${j.id}" class="btn btn-sm btn-danger pending" title="Mark as Cancelled"><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                    `
                });
            } else {
                pendingList += '<tr colspan="5">No pending appointments and reminders</tr>';
            }

            $('tbody#tbl-pending').html(pendingList);
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})();

$(document).on('click', 'button.pending', function() {
    var appt_id = $(this).attr('data-appointment-id');
    
    if($(this).hasClass('btn-success')) {
        updatePending(appt_id, 'AP003')
    } else {
        updatePending(appt_id, 'AP004')
    }
});

function updatePending(appt_id, status) {
    $.ajax({
        url: $('base').attr('href') + '/appointment/modal/update/' + appt_id,
        type: 'put',
        data: { id: appt_id, status: status },
        success: function(data) {
            console.log(data);
            $('tr[data-appointment-id="'+data.appointment.id+'"]').fadeOut().remove();

            if ($('tbody#tbl-pending tr.text-center').length == 0)
                $('tbody#tbl-pending').append('<tr class="text-center" colspan="5"><td><h4>No appointments and reminders yet</h4></td></tr>');

            $('tr[data-appointment="'+data.appointment.id+'"]').addClass('bg-warning');
            $('tr[data-appointment="'+data.appointment.id+'"]').find('td.status').text((data.appointment.status == 'AP003' ? 'Completed' : 'Cancelled'));

            setInterval(function() {
                $('tr[data-appointment="'+data.appointment.id+'"]').removeClass('bg-warning', 'slow');
            }, 8000);
        },
        error: function(error) {
            console.log('Error data: ' + error);
        }
    });
}

function showPopupModal() {
    $('div#modalPopup').modal('show');
}