function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function modify() {
    $.ajax({
        beforeSend : function() {
            // The loader overlay will show
            $('i#loader-modal').removeClass('hidden');
        },
        success : function() {
            // If checked, the column will show
            if($('.checkboxes').is(":checked")) {
                $('input.checkboxes:checkbox:checked').each(function() {
                    var column = "table ." + $(this).attr("name");
                    $(column).removeClass('hidden');
                });
            }
           
            // If not checked, the column will hide
            $('input.checkboxes:checkbox:not(:checked)').each(function() {
                var column = "table ." + $(this).attr("name");
                $(column).addClass('hidden');
            });
                
            // The loader overlay and modal will hide
            $('i#loader-modal').addClass('hidden');
            $('#modifyColumn').modal('hide');
            
            // if(num > 6 && num < 8)
            //     $('#columns').addClass('account-cols-7');
            // else
            //     if(num == 8)
            //         $('#columns').addClass('account-cols-8');
            //     else
            //         if($('#columns').hasClass('account-cols-7'))
            //             $('#columns').removeClass('account-cols-7');
            //         else if($('#columns').hasClass('account-cols-8'))
            //             $('#columns').removeClass('account-cols-8');
                        
        },
        error : function(http, status, error) {
              console.log('Some errors: ' + error);
        }
    });

}

function pagination(pagination,lobbyRow,route){

    let baseUrl = $('base').attr('href'),
        url = pagination.path + '?page=';
        // console.log(lobbyRow);
        let fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#lobby-management > tbody');

        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }
                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        table.addClass('hidden');
        table.html(lobbyRow).fadeIn('fast');
        table.removeClass('hidden');
}

function getPagination(uri) {
    var lobbyRow = "",
        tblRow = $('table#lobby-management > tbody');

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('table#lobby-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded();
            modify();
            if(data.lobby.length > 0)
            {
                $.each(data.lobby, function (i, j){
                    lobbyRow += "<tr>";
                    lobbyRow += "<td class='text-center overlay company_name'>"+j.company+"</td>";
                    lobbyRow += "<td class='text-center overlay name'>"+ j.name +"</td>";
                    lobbyRow += "<td class='text-center overlay contact'>"+j.contact+"</td>";
                    lobbyRow += "<td class='text-center overlay email'>"+j.email+"</td>";
                    lobbyRow += "<td class='text-center overlay status'>"+j.status+"</td>";
                    lobbyRow += "<td class='text-center overlay created'>"+j.created+"</td>";
                    lobbyRow += "<td class='text-center overlay' id='lobby-management'> ";
                    lobbyRow += (j.status == 'Open' ? "<input type='checkbox' class='check-open' name='check-open[]' value='"+j.id+"'>" : 'N/A');
                    lobbyRow += "</td></tr>";
                });
    
                lobbyRow += "<tr><td class='company_name overlay'></td><td class='name overlay'></td><td class='contact overlay'></td><td class='email overlay'></td>";
                lobbyRow += "<td class='status overlay'></td><td class='created overlay'></td>";
                lobbyRow += "<td class='text-center overlay' id='lobby-management'>";
                lobbyRow += "<button disabled='true' onclick='confirmClaim(event)' id='claim-btn' type='submit' class='btn btn-success'>";
                lobbyRow += "<i class='fa fa-ticket'> </i> Claim</button>";
                lobbyRow += "</td></tr>";
            } else {
                lobbyRow += "<tr><td colspan='7' align='center'>No open leads/prospect/account found</td></tr>";
            }
            

            let route = window.location.href;
            pagination(data.pagination,lobbyRow,route);
            $('ul.pagination').removeClass('hidden');
            $('a.sort-lobby').removeClass('hidden');
        }
    });
}

function clearSorting(){
    $('i#sort-lobby').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created')
            $(this).parent().addClass('active');
    });
}

function sortLobby(url){
    var lobbyRow = "",
        tblRow = $('table#lobby-management > tbody'),
        base_url = $('base').attr('href');

    $.ajax({
        url: url,
        beforeSend:function(){
            $('table#lobby-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            console.log(data);
            loaded();
            modify();
            if(data.result.length > 0)
            {
                $.each(data.result,function(i,j){
                    lobbyRow += "<tr>";
                    lobbyRow += "<td class='text-center overlay company_name'>"+(j.contract ? '<a href="'+base_url+'/contract/summary/'+j.prospect+'">'+j.company+'</a>' : j.company)+"</td>";
                    lobbyRow += "<td class='text-center overlay name'>"+j.name+"</td>";
                    lobbyRow += "<td class='text-center overlay contact'>"+j.contact+"</td>";
                    lobbyRow += "<td class='text-center overlay email'>"+j.email+"</td>";
                    lobbyRow += "<td class='text-center overlay status'>"+j.status+"</td>";
                    lobbyRow += "<td class='text-center overlay created'>"+j.created+"</td>";
                    lobbyRow += "<td class='text-center overlay' id='lobby-management'> ";
                    lobbyRow += (j.status == 'Open' ? "<input type='checkbox' class='check-open' name='check-open[]' value='"+j.id+"'>" : 'N/A');
                    lobbyRow += "</td></tr>";
                });
    
                lobbyRow += "<tr><td class='company_name overlay'></td><td class='name overlay'></td><td class='contact overlay'></td><td class='email overlay'></td>";
                lobbyRow += "<td class='status overlay'></td><td class='created overlay'></td>";
                lobbyRow += "<td class='text-center overlay' id='lobby-management'>";
                lobbyRow += "<button disabled='true' onclick='confirmClaim(event)' id='claim-btn' type='submit' class='btn btn-success'>";
                lobbyRow += "<i class='fa fa-ticket'> </i> Claim</button>";
                lobbyRow += "</td></tr>";
            } else {
                lobbyRow += "<tr><td colspan='7' align='center'>No open leads/prospect/account to sort</td></tr>";
            }
           
            // let hasPage = url.indexOf('?page=') >= 1 ? url.replace(/([&\?]key=val*$|key=val&|[?&]key=val(?=#))/, '') : '?page=';
            //     route = url.replace($('base').attr('href'),'')+hasPage;
            let route = '/lobby/sort/' + data.field + '/' + data.order;
            pagination(data.pagination,lobbyRow,route);
            $('ul.pagination').removeClass('hidden');
            $('a.sort-lobby').removeClass('hidden');
        }
    });
}

function searchLobby(){
    var lobby = $('input[id="searchLobby"]').val(),
        key = lobby === '' ? 'null' : lobby,
        base_url = $('base').attr('href');

    if (lobby === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/lobby/search/' + key,
        beforeSend:function(){
            $('table#lobby-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded();
            modify();
            var lobbyRow = "",
                tblRow = $('table#lobby-management > tbody');

            if ((data.result).length > 0){
                $.each(data.result,function(i,j){
                    lobbyRow += "<tr>";
                    lobbyRow += "<td class='text-center overlay company_name'>"+(j.contract ? '<a href="'+base_url+'/contract/summary/'+j.prospect+'">'+j.company+'</a>' : j.company)+"</td>";
                    lobbyRow += "<td class='text-center overlay name'>"+j.name+"</td>";
                    lobbyRow += "<td class='text-center overlay contact'>"+j.contact+"</td>";
                    lobbyRow += "<td class='text-center overlay email'>"+j.email+"</td>";
                    lobbyRow += "<td class='text-center overlay status'>"+j.status+"</td>";
                    lobbyRow += "<td class='text-center overlay created'>"+j.created+"</td>";
                    lobbyRow += "<td class='text-center overlay' id='lobby-management'> ";
                    lobbyRow += (j.status == 'Open' ? "<input type='checkbox' class='check-open' name='check-open[]' value='"+j.id+"'>" : 'N/A');
                    lobbyRow += "</td></tr>";
                });

                lobbyRow += "<tr><td class='company_name overlay'></td><td class='name overlay'></td><td class='contact overlay'></td><td class='email overlay'></td>";
                lobbyRow += "<td class='status overlay'></td><td class='created overlay'></td>";
                lobbyRow += "<td class='text-center overlay' id='lobby-management'>";
                lobbyRow += "<button disabled='true' onclick='confirmClaim(event)' id='claim-btn' type='submit' class='btn btn-success'>";
                lobbyRow += "<i class='fa fa-ticket'> </i> Claim</button>";
                lobbyRow += "</td></tr>";

                var route = '/lobby/search/'+lobby+'?page=';
                pagination(data.pagination,lobbyRow,route);
            }
            else{
                lobbyRow = "<tr><td colspan='7' class='text-center'>No results found for "+lobby+"</td></tr>";

                tblRow.html(lobbyRow).fadeIn('fast');
                $('ul.pagination').addClass('hidden');
                $('a.sort-lobby').removeClass('hidden');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchLobby();
    }
}

(function(){
    let root = $('base').attr('href');

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        let link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('list') >= 1)
            getPagination(url);
        else if(url.indexOf('lobbysearch') >= 1)
            searchLobby();
        else
            sortLobby(url);

    });

    $(document).on('click', 'button.search-btn', function(){
        searchLobby();
    });

    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        let url = $('base').attr('href') + '/lobby/list?page=1';

        clearSorting();

        getPagination(url);
        $('#searchLobby').val('');
        $('div#filter-container').addClass('hidden');
    });

    $(document).on('click','a.sort-lobby', function(e){
        e.preventDefault();
        var currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        // old algorithm
        // if (currOrder.indexOf('asc') >= 1)
        //     $('i#sort-lead').each(function(){
        //         $(this).removeClass('fa-sort-alpha-asc');
        //         $(this).addClass('fa-sort-alpha-desc');
        //     });
        // else
        //     $('i#sort-lead').each(function(){
        //         $(this).removeClass('fa-sort-alpha-desc');
        //         $(this).addClass('fa-sort-alpha-asc');
        //     });

        $(this).addClass('active');
        $(this).parent().siblings().find('a').removeClass('active');

        if (currOrder.indexOf('asc') >= 1)
            $(this).find('i').removeClass('fa-sort-alpha-asc'),
            $(this).find('i').addClass('fa-sort-alpha-desc');
        else
            $(this).find('i').removeClass('fa-sort-alpha-desc'),
            $(this).find('i').addClass('fa-sort-alpha-asc');

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

        var url = root + '/lobby/sort/' + sortBy + '/' + sortOrder;

        sortLobby(url);
    });

    // Filter Column Button
   $('#filter-column').click(function(e) {
       e.preventDefault();
       $('div#filter-container').toggleClass("hidden");
   });

   // Filter Column Selection: Level 1
   $(document).on('change', 'select#filter', function(){

        var url = $('base').attr('href'),
            val = this.value;

        if(val == "")
            return false;

        $.ajax({
            url : url + '/lobby/filter/' + val,
            beforeSend : loading(),
            success : function(data) {
                loaded();
                $('#filter-level-2').removeClass('hidden');

                var option = "<option value=''></option>";

                $(data.result).each(function(i, j) {
                    option += '<option value="'+ j + '">' + j + '</option>';
                });
                
                $('select#filter-2').append(option);
                $('button#btn-filter').removeAttr('disabled');
            }
        });

        $('select#filter-2').empty();
        $(this).attr('disabled','true');
        
   });

   // Filter Column Selection: Level 2
   $(document).on('change', 'select#filter-2', function() {

        var url = $('base').attr('href'),
            col = $('select#filter').val(),
            val = this.value,
            base_url = $('base').attr('href');

        if (val == "")
            return false;

        $.ajax({
            url : url + '/lobby/filter/' + col + '/' + val,
            beforeSend : loading(),
            success : function(data) {
                console.log(data.result);
                loaded();
                modify();

                var lobbyRow = "",
                    tblRow = $('table#lobby-management > tbody');

                if(data.result.length > 0)
                {
                    $.each(data.result,function(i,j){
                        lobbyRow += "<tr>";
                        lobbyRow += "<td class='text-center overlay company_name'>"+(j.contract ? '<a href="'+base_url+'/contract/summary/'+j.prospect+'">'+j.company+'</a>' : j.company)+"</td>";
                        lobbyRow += "<td class='text-center overlay name'>"+j.name+"</td>";
                        lobbyRow += "<td class='text-center overlay contact'>"+j.contact+"</td>";
                        lobbyRow += "<td class='text-center overlay email'>"+j.email+"</td>";
                        lobbyRow += "<td class='text-center overlay status'>"+j.status+"</td>";
                        lobbyRow += "<td class='text-center overlay created'>"+j.created+"</td>";
                        lobbyRow += "<td class='text-center overlay' id='lobby-management'> ";
                        lobbyRow += (j.status == 'Open' ? "<input type='checkbox' class='check-open' name='check-open[]' value='"+j.id+"'>" : 'N/A');
                        lobbyRow += "<td/></tr>";
                    });
                        
                    lobbyRow += "<tr><td class='company_name overlay'></td><td class='name overlay'></td><td class='contact overlay'></td><td class='email overlay'></td>";
                    lobbyRow += "<td class='status overlay'></td><td class='created overlay'></td>";
                    lobbyRow += "<td class='text-center overlay' id='lobby-management'>";
                    lobbyRow += "<button disabled='true' onclick='confirmClaim(event)' id='claim-btn' type='submit' class='btn btn-success'>";
                    lobbyRow += "<i class='fa fa-ticket'> </i> Claim</button>";
                    lobbyRow += "</td></tr>";
                    
                   
                } else {
                    lobbyRow += "<tr><td colspan='7' align='center'>No open leads/prospect/account to filter</td></tr>";
                }

                var route = '/lobby/filter/' + col + '/' + val;
                pagination(data.pagination,lobbyRow,route);
                $('a.sort-lobby').addClass('hidden');
               
                    
            }
        });

   });

   // Filter Clear button : Filter Column
   $(document).on('click', 'button#btn-filter', function() {
        $('select#filter').removeAttr('disabled');
        $('div#filter-level-2').addClass('hidden');
        $('select#filter-level-2').empty();
        $(this).attr('disabled','true');
   });

   $(document).on('click','input.check-open',function(){
        $('input#check-all').prop('checked',false);

        $('button#claim-btn').prop('disabled', $('input.check-open:checked').length == 0);

        
   });

   $(document).on('click','input#check-all',function(){
        $('.check-open').prop('checked', $('input#check-all').prop('checked'));

        $('button#claim-btn').prop('disabled', $('input.check-open:checked').length == 0);
   });

   $(document).on('change', 'select#limit_entry', function() {
        var baseUrl = $('base').attr('href');

        $.ajax({
            url : baseUrl + '/items/lobby/' + this.value,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                loaded();
                location.reload();
            }
        })
    });

})();


function toggle(source) {
    var checkboxes = document.getElementsByClassName('check-open');
    for(var i = 0, n = checkboxes.length ; i < n; i++ ) {
        checkboxes[i].checked = source.checked;
    }
}

function confirmClaim(e) {
    e.preventDefault(); // prevent form submit
    var form = e.target.form; // storing the form

    swal({
      title: 'Are you sure?',
      text: "You want to be able to claim this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        form.submit();          // submitting the form when user press yes
      } else {
        
      }
    });
    
};
