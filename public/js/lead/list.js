function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function pagination(pagination,leadRow,route){
   
    let baseUrl = $('base').attr('href'),
       
        url = pagination.path + '?page=';

        let fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#lead-management > tbody');

        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }
                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        table.addClass('hidden');
        table.html(leadRow).fadeIn('fast');
        table.removeClass('hidden');
}

function getPagination(uri) {
    let leadrow = "";

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('table#lead-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded();
            if(data.leads.length > 0)
            {
                $.each(data.leads, function (i, j){
                    leadrow += "<tr>";
                    leadrow += "<td class='text-center'>"+j.name+"</td>";
                    leadrow += "<td class='text-center'>"+(j.cno !== null ? j.cno : 'Not Specified')+"</td>";
                    leadrow += "<td class='text-center'>"+j.comp+"</td>";
                    leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                    leadrow += "<td class='text-center'>"+j.date+"</td>";
                    leadrow += j.isAdmin === 'yes' ? "<td class='text-center'>"+j.owner+"</td>" : "";
                    leadrow += "<td class='text-center'>"+j.locked+"</td>";
                    leadrow += "<td class='text-center text-nowrap' id='lead-management'> ";
    
                    if(j.leadstatus !== 'Converted'){
                        leadrow += "<a href='/lead/view/"+j.leadid+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " +
                            "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " +
                                (data.role === 1 || data.teamcode === 'acc' || j.created_by === data.user.userid ? "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger btn-sm'>" + 
                                    "<i class='fa fa-trash'></i></a> " : '') +
                                        "<a href='/lead/convert/"+j.leadid+"/prospect' class='btn btn-primary btn-sm'><i class='fa fa-sign-out'></i> Convert</a></td>";
                        }
                    else{
                        leadrow += "No actions";
                    }
    
                    leadrow += "</tr>";
                });
            } else {
                leadrow = "<tr><td colspan='9' class='text-center'>No leads available</td></tr>";
            }
            

            let route = window.location.href;
            pagination(data.pagination,leadrow,route);
        }
    });
}

function clearSorting(){
    $('i#sort-lead').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created_at')
            $(this).parent().addClass('active');
    });
}

function sortLead(url){
    let leadrow = "";

    $.ajax({
        url: url,
        beforeSend:function(){
            $('table#lead-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded()
            if(data.result.length > 0)
            {
                $.each(data.result,function(i,j){
                    leadrow += "<tr>";
                    leadrow += "<td class='text-center'>"+j.name+"</td>";
                    leadrow += "<td class='text-center'>"+(j.cno !== null ? j.cno : 'Not Specified')+"</td>";
                    leadrow += "<td class='text-center'>"+j.comp+"</td>";
                    leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                    leadrow += "<td class='text-center'>"+j.date+"</td>";
                    leadrow += j.isAdmin === 'yes' ? "<td class='text-center'>"+j.owner+"</td>" : "";
                    leadrow += "<td class='text-center'>"+j.locked+"</td>";
                    leadrow += "<td class='text-center text-nowrap' id='lead-management'> ";
    
                    if(j.leadstatus !== 'Converted'){
                        leadrow += "<a href='/lead/view/"+j.leadid+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " +
                            "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " +
                                (data.role === 1 || data.teamcode === 'acc' || j.created_by === data.user.userid ? "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger btn-sm'>" + 
                                    "<i class='fa fa-trash'></i></a> " : '') +
                                        "<a href='/lead/convert/"+j.leadid+"/prospect' class='btn btn-primary btn-sm'><i class='fa fa-sign-out'></i> Convert</a></td>";
                        }
                    else{
                        leadrow += "No actions";
                    }
    
                    leadrow += "</tr>";
                });
            } else {
                leadrow = "<tr><td colspan='9' class='text-center'>No leads available to sort</td></tr>";
            }
           

            // let hasPage = url.indexOf('?page=') >= 1 ? url.replace(/([&\?]key=val*$|key=val&|[?&]key=val(?=#))/, '') : '?page=';
            //     route = url.replace($('base').attr('href'),'')+hasPage;
            let route = '/lead/sort/' + data.field + '/' + data.order;
            pagination(data.pagination,leadrow,route);
            $('ul.pagination').removeClass('hidden');
        }
    });
}

function searchLead(){
    let lead = $('input[id="searchLead"]').val(),
        key = lead === '' ? 'null' : lead;

    if (lead === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/lead/search/' + key,
        beforeSend:function(){
            $('table#lead-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded()
            let leadrow = "",
                tblRow = $('table#lead-management > tbody');

            if ((data.result).length > 0){
                $.each(data.result,function(i,j){
                    leadrow += "<tr>";
                    leadrow += "<td class='text-center'>"+j.name+"</td>";
                    leadrow += "<td class='text-center'>"+(j.cno !== null ? j.cno : 'Not Specified')+"</td>";
                    leadrow += "<td class='text-center'>"+j.comp+"</td>";
                    leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                    leadrow += "<td class='text-center'>"+j.date+"</td>";
                    leadrow += j.isAdmin === 'yes' ? "<td class='text-center'>"+j.owner+"</td>" : "";
                    leadrow += "<td class='text-center'>"+j.locked+"</td>";
                    leadrow += "<td class='text-center text-nowrap' id='lead-management'> ";

                    if(j.leadstatus !== 'Converted'){
                        leadrow += "<a href='/lead/view/"+j.leadid+"' class='btn btn-info btn-sm'><i class='fa fa-eye'></i></a> " +
                            "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a> " +
                                (data.role === 1 || data.teamcode === 'acc' || j.created_by === data.user.userid ? "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger btn-sm'>" + 
                                    "<i class='fa fa-trash'></i></a> " : '') +
                                        "<a href='/lead/convert/"+j.leadid+"/prospect' class='btn btn-primary btn-sm'><i class='fa fa-sign-out'></i> Convert</a></td>";
                        }
                    else{
                        leadrow += "No actions";
                    }

                    leadrow += "</tr>";
                });

                let route = '/lead/search/'+lead+'?page=';
                pagination(data.pagination,leadrow,route);
            }
            else{
                leadrow = "<tr><td colspan='7' class='text-center'>No results found for "+lead+"</td></tr>";

                tblRow.html(leadrow).fadeIn('fast');
                $('ul.pagination').addClass('hidden');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchLead();
    }
}

(function(){

    let root = $('base').attr('href');

    $('a#leadDelete').on('click', function(){
        let lead_id = $(this).data('leadid');


    });

    $(document).on('click','a#leadDelete', function(){
        let leadid = $(this).data('leadid');

        $('#delete-leadid').val(leadid);
        $('#deleteLeadModal').modal('show');
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        let link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('list') >= 1)
            getPagination(url);
        else if(url.indexOf('leadsearch') >= 1)
            searchLead();
        else
            sortLead(url);

    });

    $(document).on('click', 'button.search-lead', function(){
        searchLead();
    });

    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        let url = $('base').attr('href') + '/lead/list?page=1';

        clearSorting()

        getPagination(url);
        $('#searchLead').val('');
    });

    $(document).on('click','a.sort-lead', function(e){
        e.preventDefault();
        let currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        // old algorithm
        // if (currOrder.indexOf('asc') >= 1)
        //     $('i#sort-lead').each(function(){
        //         $(this).removeClass('fa-sort-alpha-asc');
        //         $(this).addClass('fa-sort-alpha-desc');
        //     });
        // else
        //     $('i#sort-lead').each(function(){
        //         $(this).removeClass('fa-sort-alpha-desc');
        //         $(this).addClass('fa-sort-alpha-asc');
        //     });

        $(this).addClass('active');
        $(this).parent().siblings().find('a').removeClass('active');

        if (currOrder.indexOf('asc') >= 1)
            $(this).find('i').removeClass('fa-sort-alpha-asc'),
            $(this).find('i').addClass('fa-sort-alpha-desc');
        else
            $(this).find('i').removeClass('fa-sort-alpha-desc'),
            $(this).find('i').addClass('fa-sort-alpha-asc');

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

        let url = root + '/lead/sort/' + sortBy + '/' + sortOrder;

        sortLead(url);
    });

    $(document).on('change', 'select#limit_entry', function() {
        var baseUrl = $('base').attr('href');

        $.ajax({
            url : baseUrl + '/items/leads/' + this.value,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                loaded();
                location.reload();
            }
        })
    });

})();
