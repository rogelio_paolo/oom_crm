function isNumberKey(e, length) {
    var charCode = (e.which) ? e.which : e.keyCode,
        other_duration = $('input[id="other_duration"]');

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}


// $('input[id="email"]').multiple_emails();

$(document).on('change', 'select#nature', function () {

    var nature = $(this).val();

    $('input[id="nature_array"]').val(nature);
});

$(document).on('click', 'input[id="has_historical_data"]', function () {

    var has_historical_data = $(this).val();

    if (has_historical_data == '1')
        $('div.file-uploading').removeClass('hidden');
    else
        $('div.file-uploading').addClass('hidden');

});

$(document).on('click', 'input[id="has_fb_hisdata"]', function () {

    var has_historical_data = $(this).val();

    if (has_historical_data == '1')
        $('div.fb-file-uploading').removeClass('hidden');
    else
        $('div.fb-file-uploading').addClass('hidden');

});

$(document).on('click', 'input[id="has_seo_hisdata"]', function () {

    var has_historical_data = $(this).val();

    if (has_historical_data == '1')
        $('div.seo-file-uploading').removeClass('hidden');
    else
        $('div.seo-file-uploading').addClass('hidden');

});

$(document).on('click', 'input[id="has_web_hisdata"]', function () {

    var has_historical_data = $(this).val();

    if (has_historical_data == '1')
        $('div.web-file-uploading').removeClass('hidden');
    else
        $('div.web-file-uploading').addClass('hidden');

});

//-----------------SEM CAMPAIGN DURATION--------------------//

// Ad scheduling
$(document).on('click', 'input[id="ad_scheduling"]', function () {
    var ad_scheduling = $(this).val(),
        budget_field = $('input[id="google_daily_budget"]'),
        monthly_field = $('input[id="google_monthly_budget"]').val(),
        duration_other = $('input[id="duration_other"]').val(),
        ad_sched_remark = $('#ad_scheduling_remark');

    if(ad_scheduling === '1'){
        budget_field.val('');
        budget_field.removeAttr('readonly');
        ad_sched_remark.removeClass('hidden');

            if ($('span#sem_daily_budget').length <= 0){
                budget_field.after('<span class="error-p account" id="sem_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
            }
            else{
                return false;
            }
    } else {
        $("#select#duration").multiselect("clearSelection");

        ad_sched_remark.addClass('hidden');
        ad_sched_remark.val('');
        if (monthly_field !== '') {
            daily_result = calculateDailyBudget(duration_other, true);
            number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            budget_field.val(number);
            budget_field.attr('readonly','readonly');
            $('span#sem_daily_budget').remove();
        } else {
            budget_field.val('');
            budget_field.attr('readonly','readonly');
            $('span#sem_daily_budget').remove();
        }
               
    }

});

// Media channel
$(document).on('change', 'select#media_channel', function () {
    var selected = $(this).val();
    $('input[id="media_channel_array"]').val(selected);
});
// Input total budget change into parse float
$(document).on('change', 'input[id="budget"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

// Input monthly budget change into parse float and calculated in 30 days
$(document).on('change', 'input[id="google_monthly_budget"]', function () {
    var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
        ad_sched = $('input[id="ad_scheduling"]:checked').val(),
        number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        
    $(this).val(number);

    if (ad_sched == 0)
        $('input[id="google_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
});

// Input daily Budget change into parse float
$(document).on('change', 'input[id="google_daily_budget"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

// AM Fee change into parse float
$(document).on('change','input[id="am_fee"]', function(){
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

// function to calculate SEM daily budget
function calculateSEMDailyBudget(duration, is_other = false) {
    var monthly_budget = $('input[id="google_monthly_budget"]').val(),
        duration_other_picker = $('select#duration_other_picker').val();

    if(is_other == true) {
        if(duration_other_picker === 'CD992')
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
        else if(duration_other_picker === 'CD993')
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        else
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
    } else {
        daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
    }
        
    var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
    return pretty_daily_result;
}

// Input Monthly budget change into parse float
$(document).on('change', 'input[id="monthly_budget"]', function () {
    var daily_budget = $('input[id="google_daily_budget"]'),
        duration_other = $('input[id="duration_other"]'),
        ad_sched = $('input[id="ad_scheduling"]:checked').val();

    if (ad_sched == 0) 
        daily_budget.val(calculateSEMDailyBudget(duration_other, false));
});

// Dropdown selection duration
$(document).on('change', 'select#duration', function () {
    // Setting of variables for selection of duration
    var duration = $('select#duration').val(),
        duration_other = $('input[id="duration_other"]'),
        duration_other_container = $('div#duration-other'),
        daily_budget = $('input[id="google_daily_budget"]'),
        ad_sched = $('input[id="ad_scheduling"]:checked').val();
        
        // If select 'Others' on dropdown selection
        if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateSEMDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateSEMDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

    // Campaign date from and date to
    if ($('input[id="campaign_datefrom"]').val() !== '') {
        var txt_duration = $('select#duration option:selected').text(),
            startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
            enddate = $('input[id="campaign_dateto"]');

        months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
        console.log(months);
        if (txt_duration !== 'Others')
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
            enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    }

});

// Input other duration change into parse float and automatic change of campaign datefrom and dateto
$('input[id="duration_other"]').on('change', function () {
    var other = this.value,
        interval = $('select#duration_other_picker option:selected').val(),
        startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
        daily_budget = $('input[id="google_daily_budget"]'),
        campaign_date_set = $('input[id="campaign_date_set"]:checked').val(),
        ad_sched = $('input[id="ad_scheduling"]:checked').val();

    if (ad_sched == 0) {
        daily_result = calculateSEMDailyBudget(this.value, true);
        var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        daily_budget.val(number);
    }

    if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
    else if (interval === 'CD992')
        startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
    
    if(campaign_date_set === 1)
        if (other !== '')
            $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

// Dropdown selection of days,weeks,months on Duration other
$(document).on('change', 'select#duration_other_picker', function () {
    var interval = this.value,
        duration = $('input[id="duration_other"]').val(),
        startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
        campaign_date_set = $('input[id="campaign_date_set"]:checked').val(),
        ad_sched = $('input[id="ad_scheduling"]:checked').val(),
        enddate = $('input[id="campaign_dateto"]');

        if(ad_sched == 0)
            $('input[id="google_daily_budget"]').val(calculateSEMDailyBudget(duration, true));

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
        
        if(campaign_date_set === 1)
            if (duration !== '')
                $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

            console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});


$(document).on('change', 'input[id="campaign_datefrom"]', function () {

    var camp_durr = $('select#duration option:selected').val(),
        duration = $('select#duration option:selected').text(),
        other = $('input[id="duration_other"]'),
        startdate = new Date(Date.parse($(this).val())),
        interval = $('select#duration_other_picker option:selected').val(),
        enddate = $('input[id="campaign_dateto"]');

    months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));

    if (camp_durr == 'CD999')
        if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

    if (startdate !== '')
        $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

    $(this).datepicker('hide');
});

$('select#currency').on({
    'change': function (e) {
        var currency = $(this).val();
        $('span#campaign-currency').text(currency);
    }
});

$(document).ready(function () {
    var currency = $('select#currency option:selected').val(),
        ad_sched = $('input[id="ad_scheduling"]:checked').val(),
        budget_field = $('input[id="google_daily_budget"]');
    $('span#campaign-currency').text(currency);
   
    if(ad_sched === '1')
        budget_field.after('<span class="error-p account" id="sem_daily_budget">Please recalculate daily budget because of ad scheduling</span>'),
        budget_field.removeAttr('readonly');

});

// End of SEM Campaign Duration

//---------------------FB CAMPAIGN DURATION----------------------//

// Media channels dropdown selection
$(document).on('change', 'select#fb_media_channel', function () {
    var selected = $(this).val();
    $('input[id="fb_media_channel_array"]').val(selected);
});

// Input total budget parse into float
$(document).on('change', 'input[id="total_budget"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

// Input monthly budget change into parse float and calculated in 30 days
$(document).on('change', 'input[id="monthly_budget"]', function () {
    var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
        // ad_sched = $('input[id="ad_scheduling"]:checked').val();
        number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

        $(this).val(number);

    // if (ad_sched == 0)
    $('input[id="daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
});

// function to calculate FB daily budget
function calculateFBDailyBudget(duration, is_other = false) {
    var monthly_budget = $('input[id="monthly_budget"]').val(),
        duration_other_picker = $('select#fb_other_picker').val();

    if(is_other == true) {
        if(duration_other_picker === 'CD992')
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
        else if(duration_other_picker === 'CD993')
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        else
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
    } else {
        daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
    }
        
    var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
    return pretty_daily_result;
}

// Dropdown selection duration
$(document).on('change', 'select#fb_duration', function () {
    var duration = $('select#fb_duration').val(),
        duration_other = $('input[id="fb_duration_other"]'),
        duration_other_container = $('div#fb-duration-other'),
        daily_budget = $('input[id="daily_budget"]');
    
    // If select 'Others' on dropdown selection
    if(duration === 'CD999') {
        duration_other_container.removeClass('hidden');

        // Calculate the budget in manual input of days, weeks and months
        daily_result = calculateFBDailyBudget(duration_other, true);
    } else {
        // Setting of variable daily result and calculates it into 30 days
        daily_result = calculateFBDailyBudget(duration_other, false);
        duration_other_container.addClass('hidden');
        duration_other.val('');
    }

    var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
    daily_budget.val(number);

    // Campaign date from and date to
    if ($('input[id="fb_campaign_dt_from"]').val() !== '') {
        var txt_duration = $('select#fb_duration option:selected').text(),
            startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
            enddate = $('input[id="fb_campaign_dt_to"]');

        months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));

        if (txt_duration !== 'Others')
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
            enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    }
});

// Input other duration change into parse float and automatic change of campaign datefrom and dateto
$('input[id="fb_duration_other"]').on('change', function () {

    var other = this.value,
        daily_budget = $('input[id="daily_budget"]'),
        interval = $('select#fb_other_picker option:selected').val(),
        startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
        campaign_date_set = $('input[id="fb_campaign_date_set"]:checked').val(),
        enddate = $('input[id="fb_campaign_dt_to"]');

    daily_result = calculateFBDailyBudget(this.value, true);
    var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
    daily_budget.val(number);
    
    if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
    else if (interval === 'CD992')
        startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));

    if(campaign_date_set === 1)
        if (other !== '')
            enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

// Dropdown selection of days,weeks,months on Duration other
$(document).on('change', 'select#fb_other_picker', function () {
    var interval = this.value,
        duration = $('input[id="fb_duration_other"]').val(),
        startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
        campaign_date_set = $('input[id="fb_campaign_date_set"]:checked').val(),
        enddate = $('input[id="fb_campaign_dt_to"]');

        $('input[id="daily_budget"]').val(calculateFBDailyBudget(duration, true));

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
        
        if(campaign_date_set === 1)
            if (duration !== '')
                enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

});

$(document).on('change', 'input[id="fb_campaign_dt_from"]', function () {

    var camp_durr = $('select#fb_duration option:selected').val(),
        duration = $('select#fb_duration option:selected').text(),
        other = $('input[id="fb_duration_other"]'),
        interval = $('select#fb_other_picker option:selected').val(),
        startdate = new Date(Date.parse($(this).val())),
        enddate = $('input[id="fb_campaign_dt_to"]');

    months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
    
    if (camp_durr == 'CD999')
        if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

    if (startdate !== '')
        $('input[id="fb_campaign_dt_to"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1)),
        //$('input[id="fb_campaign_dt_to"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

    $(this).datepicker('hide');
});

// End of FB Campaign Duration


//-------------------SEO------------------------//
$(document).on('change', 'select#seo_duration', function () {
    var seo_duration = this.value,
        divOther = $('div#seo-duration-other'),
        other = $('input[id="seo_duration_other"]');

    if (seo_duration === 'CD999')
        divOther.removeClass('hidden');
    else
        divOther.addClass('hidden'),
        other.val('');
});

$(document).on('change', 'select#keyword_themes_select', function () {
    var keyword = this.value,
        other = $('input[id="keyword_themes"]');

    if (keyword === 'NK999')
        other.removeAttr('disabled'),
        other.removeClass('hidden');
    else
        other.val(''),
        other.attr('disabled', 'disabled'),
        other.addClass('hidden');
});

$(document).on('click', 'input[id="has_keyword_maintenance"]', function () {
    var has_keyword_maintenance = this.value,
        keyword_maintenance = $('#keyword_maintenance');

    if (has_keyword_maintenance == 1)
        keyword_maintenance.removeAttr('disabled'),
        keyword_maintenance.removeClass('hidden'),
        keyword_maintenance.val('');
    else
        keyword_maintenance.attr('disabled', 'disabled'),
        keyword_maintenance.addClass('hidden');
});

// End of SEO

//--------------BAIDU----------------------- //

    // Input radio button if has baidu account
    $(document).on('click', 'input[id="has_baidu_account"]', function() {

        var has_baidu_account = $(this).val();

        if (has_baidu_account == 1) {
            $('div.baidu-account').removeClass('hidden'),
            $('input[id="baidu_account"]').focus();
        } else {
            $('div.baidu-account').addClass('hidden');
            $('input[id="baidu_account"]').val('');
        }   
        
    });

    // Input radio button if has campaign date
    $(document).on('click', 'input[id="has_baidu_campaign_dateset"]', function() {

        var has_baidu_campaign_dateset = $(this).val();

        if (has_baidu_campaign_dateset == 1) {
            $('div.baidu-campaign').removeClass('hidden');
        } else {
            $('div.baidu-campaign').addClass('hidden');
            $('input[id="baidu_campaign_datefrom"]').val('');
            $('input[id="baidu_campaign_dateto"]').val('');
        }   
    });

    // Dropdown selection of currency
    $('select#baidu_currency').on({
        'change': function (e) {
            var currency = $(this).val();
            $('span#baidu-campaign-currency').text(currency);
        }
    });

    // Input radio button if has ad scheduling
    $(document).on('click', 'input[id="baidu_ad_scheduling"]', function () {

        var ad_scheduling = $(this).val(),
            budget_field = $('input[id="baidu_daily_budget"]'),
            monthly_field = $('input[id="baidu_monthly_budget"]'),
            duration_other = $('input[id="baidu_duration_other"]').val(),
            ad_sched_remark = $('#baidu_ad_scheduling_remark');

        if(ad_scheduling === '1'){
            budget_field.val(''),
            budget_field.removeAttr('readonly'),
            ad_sched_remark.removeClass('hidden'),
            ad_sched_remark.focus();
                if ($('span#baidu_daily_budget').length <= 0){
                    budget_field.after('<span class="error-p account" id="baidu_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
                }
                else{
                    return false;
                }
        }   
        else{
            ad_sched_remark.addClass('hidden');
            ad_sched_remark.val('');
            if (monthly_field !== '') {
                daily_result = calculateBaiduDailyBudget(duration_other, true);
                number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
                budget_field.val(number);
                budget_field.attr('readonly','readonly');
                $('span#baidu_daily_budget').remove();
            } else {
                    budget_field.val('');
                    budget_field.attr('readonly','readonly');
                    $('span#baidu_daily_budget').remove();
            }
        }

    });

    $(document).on('change', 'input[id="baidu_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).on('change', 'input[id="baidu_daily_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });


    // Input monthly budget change into parse float and calculated in 30 days
    $(document).on('change', 'input[id="baidu_monthly_budget"]', function () {
        var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
            ad_sched = $('input[id="baidu_ad_scheduling"]:checked').val();
            var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

            $(this).val(number);

        if (ad_sched == 0)
            $('input[id="baidu_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

    // function to calculate daily budget
    function calculateBaiduDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="baidu_monthly_budget"]').val(),
            duration_other_picker = $('select#baidu_duration_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }

    // Dropdown selection duration
    $(document).on('change', 'select#baidu_duration', function () {
        var duration = this.value,
            duration_other = $('input[id="baidu_duration_other"]'),
            duration_other_container = $('div#baidu-duration-other'),
            daily_budget = $('input[id="baidu_daily_budget"]'),
            ad_sched = $('input[id="baidu_ad_scheduling"]:checked').val();

       // If select 'Others' on dropdown selection
       if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateBaiduDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateBaiduDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        // Campaign date from and date to
        if ($('input[id="baidu_campaign_datefrom"]').val() !== '') {
            var txt_duration = $('select#baidu_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
                enddate = $('input[id="baidu_campaign_dateto"]');

            months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
    
            if (txt_duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });

    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="baidu_duration_other"]').on('change', function () {
        var other = this.value,
            interval = $('select#baidu_duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
            daily_budget = $('input[id="baidu_daily_budget"]'),
            campaign_date_set = $('input[id="has_baidu_campaign_date_set"]:checked').val(),
            ad_sched = $('input[id="baidu_ad_scheduling"]:checked').val();

        if (ad_sched == 0) {
            daily_result = calculateBaiduDailyBudget(this.value, true);
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
        
        if(campaign_date_set === 1)
            if (other !== '')
                $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });

    // Dropdown selection of days,weeks,months on Duration other
    $(document).on('change', 'select#baidu_duration_other_picker', function () {

    var interval = this.value,
        duration = $('input[id="baidu_duration_other"]').val(),
        startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
        ad_sched = $('input[id="baidu_ad_scheduling"]:checked').val(),
        campaign_date_set = $('input[id="has_baidu_campaign_date_set"]:checked').val(),
        enddate = $('input[id="baidu_campaign_dateto"]');

        if(ad_sched == 0)
            $('input[id="baidu_daily_budget"]').val(calculateBaiduDailyBudget(duration, true));

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
        
        if(campaign_date_set === 1)
            if (duration !== '')
                $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

            // console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

    $(document).on('click', 'input[id="has_baidu_hisdata"]', function() {

        var has_baidu_hisdata = $(this).val();

        if (has_baidu_hisdata == 1)
            $('div.baidu-file-uploading').removeClass('hidden');
        else
            $('div.baidu-file-uploading').addClass('hidden');

    });

    $(document).on('change', 'input[id="baidu_campaign_datefrom"]', function () {

        var camp_durr = $('select#baidu_duration option:selected').val(),
            duration = $('select#baidu_duration option:selected').text(),
            other = $('input[id="baidu_duration_other"]'),
            startdate = new Date(Date.parse($(this).val())),
            interval = $('select#baidu_duration_other_picker option:selected').val(),
            enddate = $('input[id="baidu_campaign_dateto"]');

        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));

        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

        if (startdate !== '')
            $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

        $(this).datepicker('hide');

    });

    $('input[id="baidu_campaign_date_set"]').on({
        'ready' : function(){

            var dateSet = this.value;
            
            if(dateSet === '1')
                $('div.baidu-campaign').removeClass('hidden');
            else
                $('div.baidu-campaign').addClass('hidden');

                console.log(dateSet);
        },
        'click' : function(){
            var dateSet = this.value;

            if(dateSet === '1')
                $('div.baidu-campaign').removeClass('hidden');
            else
                $('div.baidu-campaign').addClass('hidden');

                // Remove the value after setting radio to "No"
                $('input[id="baidu_campaign_datefrom"]').val('');
                $('input[id="baidu_campaign_dateto"]').val('');
                
                console.log(dateSet);
        }
    });
// End of Baidu

//--------------------WEIBO---------------------//
    $(document).on('click', 'input[id="has_weibo_account"]', function() {

        var has_weibo_account = $(this).val();

        if (has_weibo_account == 1) {
            $('div.weibo-account').removeClass('hidden'),
            $('input[id="weibo_account"]').focus();
        } else {
            $('div.weibo-account').addClass('hidden');
            $('input[id="weibo_account"]').val('');
        }   
        
    });


    $(document).on('click', 'input[id="has_weibo_campaign_dateset"]', function() {

        var has_weibo_campaign_dateset = $(this).val();

        if (has_weibo_campaign_dateset == 1) {
            $('div.weibo-campaign').removeClass('hidden');
        } else {
            $('div.weibo-campaign').addClass('hidden');
            $('input[id="weibo_campaign_datefrom"]').val('');
            $('input[id="weibo_campaign_dateto"]').val('');
        }   
        
    });

     $(document).on('click', 'input[id="has_weibo_campaign_dateset"]', function() {

        var has_weibo_campaign_dateset = $(this).val();

        if (has_weibo_campaign_dateset == 1) {
            $('div.weibo-campaign').removeClass('hidden');
        } else {
            $('div.weibo-campaign').addClass('hidden');
            $('input[id="weibo_campaign_datefrom"]').val('');
            $('input[id="weibo_campaign_dateto"]').val('');
        }   
        
    });

    $('select#weibo_currency').on({
        'change': function (e) {
            var currency = $(this).val();
            $('span#weibo-campaign-currency').text(currency);
        }
    });

   // Input radio button if has ad scheduling
    $(document).on('click', 'input[id="weibo_ad_scheduling"]', function () {

        var ad_scheduling = $(this).val(),
            budget_field = $('input[id="weibo_daily_budget"]'),
            monthly_field = $('input[id="weibo_monthly_budget"]'),
            duration_other = $('input[id="weibo_duration_other"]').val(),
            ad_sched_remark = $('#weibo_ad_scheduling_remark');

        if(ad_scheduling === '1'){
            budget_field.val(''),
            budget_field.removeAttr('readonly'),
            ad_sched_remark.removeClass('hidden'),
            ad_sched_remark.focus();
                if ($('span#weibo_daily_budget').length <= 0){
                    budget_field.after('<span class="error-p account" id="weibo_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
                }
                else{
                    return false;
                }
        }   
        else{
            ad_sched_remark.addClass('hidden');
            ad_sched_remark.val('');
            if (monthly_field !== '') {
                daily_result = calculateWeiboDailyBudget(duration_other, true);
                number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
                budget_field.val(number);
                budget_field.attr('readonly','readonly');
                $('span#weibo_daily_budget').remove();
            } else {
                    budget_field.val('');
                    budget_field.attr('readonly','readonly');
                    $('span#weibo_daily_budget').remove();
            }
        }

    });

    $(document).on('change', 'input[id="weibo_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).on('change', 'input[id="weibo_daily_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });


   // Input monthly budget change into parse float and calculated in 30 days
   $(document).on('change', 'input[id="weibo_monthly_budget"]', function () {
        var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
            ad_sched = $('input[id="weibo_ad_scheduling"]:checked').val();
            var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

            $(this).val(number);

        if (ad_sched == 0)
            $('input[id="weibo_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

    // function to calculate daily budget
    function calculateWeiboDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="weibo_monthly_budget"]').val(),
            duration_other_picker = $('select#weibo_duration_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }
    
    // Dropdown selection duration
    $(document).on('change', 'select#weibo_duration', function () {
        var duration = this.value,
            duration_other = $('input[id="weibo_duration_other"]'),
            duration_other_container = $('div#weibo-duration-other'),
            daily_budget = $('input[id="weibo_daily_budget"]'),
            ad_sched = $('input[id="weibo_ad_scheduling"]:checked').val();

       // If select 'Others' on dropdown selection
       if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateWeiboDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateWeiboDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        // Campaign date from and date to
        if ($('input[id="weibo_campaign_datefrom"]').val() !== '') {
            var txt_duration = $('select#weibo_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
                enddate = $('input[id="weibo_campaign_dateto"]');

            months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
            console.log(months);
            if (txt_duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });

    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="weibo_duration_other"]').on('change', function () {
        var other = this.value,
            interval = $('select#weibo_duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
            daily_budget = $('input[id="weibo_daily_budget"]'),
            campaign_date_set = $('input[id="has_weibo_campaign_date_set"]:checked').val(),
            ad_sched = $('input[id="weibo_ad_scheduling"]:checked').val();

        if (ad_sched == 0) {
            daily_result = calculateWeiboDailyBudget(this.value, true);
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
        
        if(campaign_date_set === 1)
            if (other !== '')
                $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });

    $(document).on('change', 'select#weibo_duration_other_picker', function () {

        var interval = this.value,
            duration = $('input[id="weibo_duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
            ad_sched = $('input[id="weibo_ad_scheduling"]:checked').val(),
            campaign_date_set = $('input[id="fb_campaign_date_set"]:checked').val(),
            enddate = $('input[id="weibo_campaign_dateto"]');
    
            if(ad_sched == 0)
                $('input[id="weibo_daily_budget"]').val(calculateWeiboDailyBudget(duration, true));
    
            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if(campaign_date_set === 1)
                if (duration !== '')
                    $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                    //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        
                // console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });
    

    $(document).on('click', 'input[id="has_weibo_hisdata"]', function() {

        var has_weibo_hisdata = $(this).val();

        if (has_weibo_hisdata == 1)
            $('div.weibo-file-uploading').removeClass('hidden');
        else
            $('div.weibo-file-uploading').addClass('hidden');

    });

     $(document).on('change', 'input[id="weibo_campaign_datefrom"]', function () {

        var camp_durr = $('select#weibo_duration option:selected').val(),
            duration = $('select#weibo_duration option:selected').text(),
            other = $('input[id="weibo_duration_other"]'),
            startdate = new Date(Date.parse($(this).val())),
            interval = $('select#weibo_duration_other_picker option:selected').val(),
            enddate = $('input[id="weibo_campaign_dateto"]');

        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));

        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

        if (startdate !== '')
            $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

        $(this).datepicker('hide');

    });

    $('input[id="weibo_campaign_date_set"]').on({
        'ready' : function(){

            var dateSet = this.value;
            
            if(dateSet === '1')
                $('div.weibo-campaign').removeClass('hidden');
            else
                $('div.weibo-campaign').addClass('hidden');

                console.log(dateSet);
        },
        'click' : function(){
            var dateSet = this.value;

            if(dateSet === '1')
                $('div.weibo-campaign').removeClass('hidden');
            else
                $('div.weibo-campaign').addClass('hidden');

                // Remove the value after setting radio to "No"
                $('input[id="weibo_campaign_datefrom"]').val('');
                $('input[id="weibo_campaign_dateto"]').val('');
                
                console.log(dateSet);
        }
    });
// End of Weibo

// WeChat
    $(document).on('click', 'input[id="has_wechat_hisdata"]', function() {

        var has_wechat_hisdata = $(this).val();

        if (has_wechat_hisdata == 1)
            $('div.wechat-file-uploading').removeClass('hidden');
        else
            $('div.wechat-file-uploading').addClass('hidden');

    });

    $(document).on('change', 'select#wechat_age_from', function() {
        var age_from = this.value,
            age_to = $('select#wechat_age_to'),
            age = age_from !== '65' ? parseInt(age_from) + 1 : parseInt(age_from);
        age_to.val(age).change();
    });

    $(document).on('click', 'input[id="has_wechat_type"]', function() {

        var has_wechat_type = $(this).val();

        if (has_wechat_type == 1) {
            $('div.wechat-account').removeClass('hidden');
        } else {
            $('div.wechat-account').addClass('hidden');
            $('input[id="wechat_account"]').val('');
        }   
        
    });

    $(document).on('click', 'input[id="has_wechat_advertising"]', function() {

        var has_wechat_advertising = $(this).val();

        if (has_wechat_advertising == 1) {
            $('div.wechat-advertising').removeClass('hidden');
        } else {
            $('div.wechat-advertising').addClass('hidden');
            // $('input[id="wechat_account"]').val('');
        }   
        
    });
//End of WeChat

// Blog Content
    $(document).on('click', 'input[id="has_blog_hisdata"]', function() {

        var has_historical_data = $(this).val();

        if (has_historical_data == 1) {
            $('div.blog-file-uploading').removeClass('hidden');
        } else {
            $('div.blog-file-uploading').addClass('hidden');
        }

    });
// End of Blog Content

// Social Media Management
    $(document).on('change', 'input[id="social_media_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).on('click', 'input[id="has_social_media_budget"]', function() {

        var has_social_media_budget = $(this).val();

        if (has_social_media_budget == 1) {
            $('div.social-media-budget').removeClass('hidden'),
            $('input[id="social_media_budget"]').focus();
        } else {
            $('div.social-media-budget').addClass('hidden');
            $('input[id="social_media_budget"]').val('');
        }
        
    });

    $(document).on('click', 'input[id="has_social_hisdata"]', function() {

        var has_historical_data = $(this).val();

        if (has_historical_data == 1)
            $('div.social-file-uploading').removeClass('hidden');
        else
            $('div.social-file-uploading').addClass('hidden');

    });

    $(document).on('change', 'select#social_duration', function () {
        var duration = this.value,
            divOther = $('div#social-duration-other'),
            other = $('input[id="social_duration_other"]'),
            result = 0,
            social_monthly_budget = $('input[id="social_monthly_budget"]').val();

        switch(duration) {
            case 'CD001' :
                result = 30;
                break;
            case 'CD002' : 
                result = 90;
                break;
            case 'CD003' :
                result = 180;
                break;
            case 'CD004' :
                result = 360;
                break;
            case 'CD005' : 
                result = 720;
                break;
            default :
                result = 1.0;
        }

        if (duration === 'CD999')
            divOther.removeClass('hidden');
        else
            divOther.addClass('hidden'),
            other.val('');
    });

// End of Social Media Management

// Additional Option: Post Paid SEM Service Package

// Input other duration automatic change of campaign datefrom and dateto
$('input[id="postpaid_duration_other"]').on('change', function () {
    var other = this.value,
        interval = $('select#postpaid_duration_other_picker option:selected').val(),
        startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
        campaign_date_set = $('input[id="postpaid_campaign_date_set"]:checked').val();

    if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
    else if (interval === 'CD992')
        startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
    
    if(campaign_date_set === 1)
        if (other !== '')
            $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

// Dropdown selection of days,weeks,months on Duration other
$(document).on('change', 'select#postpaid_duration_other_picker', function () {
    var interval = this.value,
        duration = $('input[id="postpaid_duration_other"]').val(),
        startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
        campaign_date_set = $('input[id="postpaid_campaign_date_set"]:checked').val(),
        enddate = $('input[id="postpaid_campaign_dateto"]');

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
        
        if(campaign_date_set === 1)
            if (duration !== '')
                $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

            console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

$('input[id="postpaid_campaign_date_set"]').on({
    'ready' : function(){

        var dateSet = this.value;
        
        if(dateSet === '1')
            $('div.postpaid-campaign').removeClass('hidden');
        else
            $('div.postpaid-campaign').addClass('hidden');
    },
    'click' : function(){
        var dateSet = this.value;

        if(dateSet === '1')
            $('div.postpaid-campaign').removeClass('hidden');
        else
            $('div.postpaid-campaign').addClass('hidden');

            // Remove the value after setting radio to "No"
            $('input[id="postpaid_campaign_datefrom"]').val('');
            $('input[id="postpaid_campaign_dateto"]').val('');
    }
});

$(document).on('change', 'input[id="postpaid_campaign_datefrom"]', function () {

    var camp_durr = $('select#postpaid_duration option:selected').val(),
        duration = $('select#postpaid_duration option:selected').text(),
        other = $('input[id="postpaid_duration_other"]'),
        startdate = new Date(Date.parse($(this).val())),
        interval = $('select#postpaid_duration_other_picker option:selected').val(),
        enddate = $('input[id="postpaid_campaign_dateto"]');

    months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));

    if (camp_durr == 'CD999')
        if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

    if (startdate !== '')
        $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

    $(this).datepicker('hide');
});

// Dropdown selection duration
$(document).on('change', 'select#postpaid_duration', function () {
    // Setting of variables for selection of duration
    var duration = $('select#postpaid_duration').val(),
        duration_other = $('input[id="postpaid_duration_other"]'),
        duration_other_container = $('div#postpaid-duration-other');
        
        // If select 'Others' on dropdown selection
        if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');
        } else {
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

    // Campaign date from and date to
    if ($('input[id="postpaid_campaign_datefrom"]').val() !== '') {
        var txt_duration = $('select#postpaid_duration option:selected').text(),
            startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
            enddate = $('input[id="postpaid_campaign_dateto"]');

        months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
        if (txt_duration !== 'Others')
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
            enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    }

});

// AM Fee and Media Budget Input Event
$(document).on('ready change', 'input[id="postpaid_media_budget"]', function() {

    var media_budget = $(this),
        am_fee = $('input[id="postpaid_am_fee"]'),
        am_fee_amount = $('input[id="postpaid_am_fee_amount"]');

    if(media_budget.val().length == '') {
        $('span[id="media_budget_err"]').text('Please input Media Budget');
        $('div[id="input-group-media-budget"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="media_budget_err"]').text('');
        $('div[id="input-group-media-budget"]').css('border', 'none');
    }

    if(am_fee.val().length == '')
    {
        $('span[id="am_fee_err"]').text('Please input AM Fee %');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="am_fee_err"]').text('');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid #bbbbbb');
    }

    var total = (media_budget.val() * 1) * parseFloat((am_fee.val() / 100)).toFixed(2);
    
    am_fee_amount.val(parseFloat(total).toFixed(2));


    // Radio button selection
    var isPaidThroughOOm = $('input[id="is_media_budget_paid"]:checked').val(),
        
        no = am_fee_amount.val(),
        yes = (am_fee_amount.val() * 1)  + (media_budget.val() * 1);

    if(isPaidThroughOOm == 1)
        $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
    else
        $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
});

$(document).on('ready change', 'input[id="postpaid_am_fee"]', function() {

    var media_budget = $('input[id="postpaid_media_budget"]'),
        am_fee = $(this),
        am_fee_amount = $('input[id="postpaid_am_fee_amount"]');

    if(media_budget.val().length == '') {
        $('span[id="media_budget_err"]').text('Please input Media Budget');
        $('div[id="input-group-media-budget"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="media_budget_err"]').text('');
        $('div[id="input-group-media-budget"]').css('border', 'none');
    }

    if(am_fee.val().length == '')
    {
        $('span[id="am_fee_err"]').text('Please input AM Fee %');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="am_fee_err"]').text('');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid #bbbbbb');
    }

    var total = (media_budget.val() * 1) * parseFloat((am_fee.val() / 100)).toFixed(2);
    
    am_fee_amount.val(parseFloat(total).toFixed(2));

     // Radio button selection
     var isPaidThroughOOm = $('input[id="is_media_budget_paid"]:checked').val(),
     no = am_fee_amount.val(),
     yes = (am_fee_amount.val() * 1)  + (media_budget.val() * 1);

    if(isPaidThroughOOm == 1)
        $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
    else
        $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));

});

//Is Media Budget paid through OOm? Radio Button
$('input[id="is_media_budget_paid"]').on({
    'ready' : function() {
        var isPaidThroughOOm = this.value,
            mediaBudget = $('input[id="postpaid_media_budget"]').val(),
            amFeeAmount = $('input[id="postpaid_am_fee_amount"]').val(),
            yes = (mediaBudget * 1) + (amFeeAmount * 1),
            no = amFeeAmount * 1;

            if(isPaidThroughOOm == 1)
                $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
            else
                $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
    },      

    'click' : function() {
        var isPaidThroughOOm = this.value,
            mediaBudget = $('input[id="postpaid_media_budget"]').val(),
            amFeeAmount = $('input[id="postpaid_am_fee_amount"]').val(),
            yes = (mediaBudget * 1) + (amFeeAmount * 1),
            no = amFeeAmount * 1;

        if(isPaidThroughOOm == 1)
            $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
        else
            $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
    }
});
// End of Post Paid SEM

$(document).on('change', 'select#age_from', function() {
    var age_from = this.value,
        age_to = $('select#age_to'),
        age = age_from !== '65' ? parseInt(age_from) + 1 : parseInt(age_from);
    age_to.val(age).change();
});

$(document).on('click', 'button.btn-success', function () {
    $('div.loader-overlay').removeClass('hidden');
});

$(document).on('change', 'select#target_market', function () {
    var selected = $(this).val();

    $('input[id="targetmarket_array"]').val(selected);

});

$(document).on('change', 'select#fb_target_country', function () {
    var selected = $(this).val();

    $('input[id="fb_targetmarket_array"]').val(selected);

});

$(document).on('change', 'select#seo_target_countries', function () {
    var selected = $(this).val();

    $('input[id="seo_targetmarket_array"]').val(selected);

});

$(document).on('change', 'input[id="seo_total_fee"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    $(this).val(number);
});

// First Step of Conversion
$(document).on('change', 'input[id="contract_value"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    
    // if(this.value > 0)
    $(this).val(number);
});

$('select#currency').on({
    'change': function (e) {
        var currency = $(this).val();
        $('span.input-group-addon').text(currency);
    }
});

$(document).ready(function(){

    var sem_target_country = $('select#target_market option:selected').val(),
        sem_target_country_array = $('input[id="targetmarket_array"]'),
        fb_target_country = $('select#fb_target_country option:selected').val(),
        fb_target_country_array = $('input[id="fb_targetmarket_array"]')
        seo_target_country = $('select#seo_target_countries option:selected').val(),
        seo_target_country_array = $('input[id="seo_targetmarket_array"]');

        sem_target_country_array.val(sem_target_country);
        fb_target_country_array.val(fb_target_country);
        seo_target_country_array.val(seo_target_country);

    var ccurrency = $('select#baidu_currency option:selected').val(),
        baidu_ad_sched = $('input[id="baidu_ad_scheduling"]:checked').val(),
        baidu_budget_field = $('input[id="baidu_daily_budget"]');
        $('span#baidu-campaign-currency').text(ccurrency);
        $('span#weibo-campaign-currency').text(ccurrency);

        $('span.input-group-addon').html($('select[id="currency"]').val());
});

$('input[id="campaign_date_set"]').on({
    'ready' : function(){

        var dateSet = this.value;
        
        if(dateSet === '1')
            $('div.sem-campaign').removeClass('hidden');
        else
            $('div.sem-campaign').addClass('hidden');

            console.log(dateSet);
    },
    'click' : function(){
        var dateSet = this.value;

        if(dateSet === '1')
            $('div.sem-campaign').removeClass('hidden');
        else
            $('div.sem-campaign').addClass('hidden');

            // Remove the value after setting radio to "No"
            $('input[id="campaign_datefrom"]').val('');
            $('input[id="campaign_dateto"]').val('');
            
            console.log(dateSet);
    }
});

$('input[id="fb_campaign_date_set"]').on({
    'ready' : function(){

        var dateSet = this.value;
        
        if(dateSet === '1')
            $('div.fb-campaign').removeClass('hidden');
        else
            $('div.fb-campaign').addClass('hidden');

            console.log(dateSet);
    },
    'click' : function(){
        var dateSet = this.value;

        if(dateSet === '1')
            $('div.fb-campaign').removeClass('hidden');
        else
            $('div.fb-campaign').addClass('hidden');

             // Remove the value after setting radio to "No"
             $('input[id="fb_campaign_dt_from"]').val('');
             $('input[id="fb_campaign_dt_to"]').val('');

            console.log(dateSet);
    }
});



