function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function pagination(pagination,noteRow,route){

    var baseUrl = $('base').attr('href'),
        
        url = pagination.path + '?page=';
        var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            notes = $('div.lead-notes');

        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        notes.hide();
        notes.html(noteRow).fadeIn('fast');
}

function getPagination(uri) {
    var noteRow = "";

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('div.lead-notes').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            console.log(data);
            loaded();
            if (data.notes.length > 0){
                $.each(data.notes,function(i,j){
                    noteRow += "<div class='lead-note'>";
                    noteRow += "<span><p>"+j.name+"</p>"+"<p>"+j.date+"</p></span>";
                    noteRow += j.created_user === data.user ? "<span class='pull-right'>" +
                               "<a id='edit-note' data-editnote="+j.id+" class='btn btn-warning'><i class='fa fa-pencil'></i> Edit</a> " +
                               "<a id='delete-note' data-delnote="+j.id+" class='btn btn-danger'><i class='fa fa-trash'></i> Delete</a>" +
                               "</span>" : '';
                    noteRow += "<div class='note-wrapper'>";
                    noteRow += "<span id='lead-note-"+j.id+"'>"+decodeHtml(j.note)+"</span>";
                    noteRow += "</div></div><hr>";
                });

                //pagination(data.pagination,noteRow);
                $('ul.pagination').removeClass('hidden');
            }
            else{
                noteRow = "<div class='col-md-12 text-center leadnotes-loading'>No notes found</div>";
                $('ul.pagination').addClass('hidden');
            }
            
            var route = window.location.href;
            pagination(data.pagination,noteRow,route);
        }
    });
}

(function(){
    var baseUrl = $('base').attr('href');

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        getPagination(url);
    });

    $(document).on('click','button#clearNote', function (e) {
        e.preventDefault();

        $('textarea#addnote').val('');
    });

    $('select#filterLeadNotes').on('change', function(){
        var filter = $(this).val(),
            filterBy = "";
        
        if (filter === 'date')
            $('div#filter-by-date').removeClass('hidden');
        else
            $('div#filter-by-date').addClass('hidden');

        if (filter === 'commentor')
            $('div#filter-by-commentor').removeClass('hidden');
        else
            $('div#filter-by-commentor').addClass('hidden');

        if (filter === 'all')
            $('div#filterButtons').removeClass('hidden');
        else
            $('div#filterButtons').addClass('hidden');

    });

    $('button#FilterNotesByCommentor').on('click', function(e){
        e.preventDefault();

        var commentor = $('select#lead-commentors option:selected').val(),
            lead = $('input[id="leadid"]').val(),
            url = baseUrl + '/lead/note/filter/'+lead+'/commentor/'+commentor+'/null/null';

        getPagination(url);
    });

    $('button#FilterNotesByDate').on('click', function(e){
        e.preventDefault();

        var datefrom = $('input[id="filterDateFrom"]').val(),
            dateto = $('input[id="filterDateTo"]').val(),
            lead = $('input[id="leadid"]').val(),
            url = baseUrl + '/lead/note/filter/'+lead+'/date/null/'+datefrom+'/'+dateto;

        getPagination(url);
    });

    $('button#FilterNotesAll').on('click', function(e){
        e.preventDefault();
        var url = window.location.href;

        getPagination(url);
    });
    
})();