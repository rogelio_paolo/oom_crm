function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function pagination(pagination,auditRow,route){

    var baseUrl = $('base').attr('href'),
        
        url = pagination.path + '?page=';
        var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            logs = $('table.audit-logs > tbody');

        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        logs.hide();
        logs.html(auditRow).fadeIn('fast');
}

function getPagination(uri) {
    var auditRow = "";

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('table.audit-logs > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            loaded()
            if ((data.logs).length > 0){
                $.each(data.logs,function(i,j){
                    auditRow += "<tr>";
                    auditRow += "<td class='text-center'>"+j.ip+"</td>";
                    auditRow += "<td class='text-center'>"+j.module+"</td>";
                    auditRow += "<td class='text-center'>"+j.action+"</td>";
                    auditRow += "<td class='text-center'>"+j.message+"</td>";
                    auditRow += "<td class='text-center'>"+j.employee+"</td>";
                    auditRow += "<td class='text-center'>"+j.date+"</td>";
                    auditRow += "<td class='text-center' id='log-management'> ";
                    auditRow += "<a href='audit/log/show/"+j.id+"' class='btn btn-warning'><i class='fa fa-eye'></i></a>";
                    auditRow += "</td>";
                    auditRow += "</tr>";
                });

                //pagination(data.pagination,noteRow);
                $('ul.pagination').removeClass('hidden');
            }
            else{
                auditRow = "<tr><td colspan='7' class='text-center'>No Logs to show</td></tr>";
                $('ul.pagination').addClass('hidden');
            }
            
            pagination(data.pagination,auditRow,uri);
        }
    });
}

(function(){
    var baseUrl = $('base').attr('href');

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        getPagination(url);
    });

    
})();