$(document).ready(function() {
    $(document).on('click','a#deleteContract', function(){
        var contractid = $(this).data('contractid');
        
        $('#delete-contractid').val(contractid);
        $('#deleteContractModal').modal('show');
    });

    $(document).on('click', 'a[id="schedule-reminder"]', function(e) {
        e.preventDefault();

        var receivers = '',
            $reminderModal = $('#addReminderModal'),
            accountid = $(this).data('accountid'),
            baseUrl = $('base').attr('href'),
            url = baseUrl + '/reminder/get/receivers/' + accountid,
            name = $('input[id="name"]'),
            date = $('input[id="date"]'),
            receiver = $('select[id="receivers"]'),
            remarks = $('textarea[id="remark"]');

        
        $('span[id="span-title"]').text('');
        name.val('');
        name.css('border', '1px solid #bbbbbb');

        $('span[id="span-date"]').text('');
        date.val('');
        date.css('border', '1px solid #bbbbbb');

        $('span[id="span-remarks"]').text('');
        remarks.val();
        remarks.css('border', '1px solid #bbbbbb');

        $('span[id="span-receivers"]').text('');
        receiver.val(0);
            
        $.ajax({
            url : url,
            success: function(data) {
                $.each(data[0], function(i, j) {
                    receivers += '<option value="'+j.email+'">'+j.name+'</option>';
                });

                $reminderModal.find('select#receivers').html(receivers);
                $('select#receivers').selectpicker('refresh');
            }
        });

            $reminderModal.find('input[id="reminder-accountid"]').val(accountid);
    });

    $(document).on('click', 'button[id="reminder-create"]', function() {
        
        var name = $('input[id="name"]'),
            date = $('input[id="date"]'),
            receiver = $('select[id="receivers"]'),
            remarks = $('textarea[id="remark"]');

        if(name.val().length != '' && receiver.val().length != '' && date.val().length != '' && remarks.val().length != '') {
            return true;
        } else {
            if(name.val().length == '') {
                $('span[id="span-title"]').text('Title is required');
                name.css('border', '1px solid red');
            } else {
                $('span[id="span-title"]').text('');
                name.css('border', '1px solid #bbbbbb');
            }

            if(receiver.val().length == '') {
                $('span[id="span-receivers"]').text('Select at least one');
                receiver.css('border', '1px solid red');
            } else {
                $('span[id="span-receivers"]').text('');
                receiver.css('border', '1px solid #bbbbbb');
            }

            if(date.val().length == '') {
                $('span[id="span-date"]').text('Date is required');
                date.css('border', '1px solid red');
            } else {
                $('span[id="span-date"]').text('');
                date.css('border', '1px solid #bbbbbb');
            }

            if(remarks.val().length == '') {
                $('span[id="span-remarks"]').text('Remarks is required');
                remarks.css('border', '1px solid red');
            } else {
                $('span[id="span-remarks"]').text('');
                remarks.css('border', '1px solid #bbbbbb');
            }
        
            return false;
        }
    });

     // Reset modal dialog window when close!
     $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });

});