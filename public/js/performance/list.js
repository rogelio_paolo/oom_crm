
$(document).on('click', 'a#btn-refresh-table', function(e){
    e.preventDefault();
    var url = $('base').attr('href') + '/performance/refresh';
    clearSorting();
    getPagination(url);
    $('#searchSalesAcc').val('');
});

$(document).on('click','a.sort-perf', function(e){
    e.preventDefault();

    var root = $('base').attr('href');
    var currOrder = $(this).find('i').attr('class'),
        sortOrder = "",
        sortBy = $(this).data('sortby');

    $(this).addClass('active');
    $(this).parent().siblings().find('a').removeClass('active');

    if (currOrder.indexOf('asc') >= 1)
        $(this).find('i').removeClass('fa-sort-alpha-asc'),
            $(this).find('i').addClass('fa-sort-alpha-desc');
    else
        $(this).find('i').removeClass('fa-sort-alpha-desc'),
            $(this).find('i').addClass('fa-sort-alpha-asc');

    sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

    var url = root + '/performance/sort/' + sortBy + '/' + sortOrder;

    getPagination(url, true);
});

$('#btn-search-perf').click(function() {
    const search = $('#searchSalesAcc');
    var url = $('base').attr('href') + '/performance/search/' + search.val();
    getPagination(url, false, true);
});

$(document).on('click ready', '#edit-perf', function(e) {
    e.preventDefault();

    const perf_id = $(this).data('perf-id'),
        comment = $('#txt-comment'),
        company = $('span#perf-company'),
        notesList = $('div#sales-notes');

    var itemNotes = '';

    company.text('');
    comment.val('');
    $.ajax({
        url: $('base').attr('href') + '/performance/fetch_remark/' + perf_id,
        beforeSend: function() {
            loading()
        },
        success: function(data) {
            loaded();



            if(data.notes.length > 0) {
                $.each(data.notes, function(i, j) {
                    itemNotes += `<div class="lead-note remark-${j.id}">
                                        <span> 
                                            <p>${j.created_by}</p> 
                                            <p>${j.created_at}</p>
                                        </span>

                                        <div class="note-wrapper">
                                            <span id="lead-note-${j.id}">${j.note}</span>
                                        </div>
                                    </div>`;
                });


            } else {
                itemNotes += `<div class="text-center">No remarks to be shown!</div>`
            }

            notesList.html(itemNotes);
            ($('div.lead-note').length > 3 ? notesList.addClass('scrollable').scrollTop(0) : notesList.removeClass('scrollable'));

            company.text(data.performance.company + "'s Remarks");
            $('input#perf_id').val(data.performance.id);
        }
    });


});

$(document).on('click', '#btn-add-remarks', function(e) {
    e.preventDefault();

    const perf_id = $('input#perf_id'),
        txt_comment = $('#txt-comment'),
        comment_box = $('#comment-box[data-perf-id="'+perf_id.val()+'"]'),
        salesNotes = $('div#sales-notes');

    if(!txt_comment.val()) {
        txt_comment.css('border-color', 'red');
        return false;
    }

    $.ajax({
        url : $('base').attr('href') + '/performance/add_remark',
        type: 'PATCH',
        data: { _token: $('meta[name="csrf-token"]').attr('content'), sales_id: perf_id.val(), remarks: (!txt_comment.val() ? 'TBA' : txt_comment.val()) },
        beforeSend: function() {
            loading()
        },
        success: function(data) {
            loaded();

            ($('div.lead-note').length > 3 ? salesNotes.addClass('scrollable').scrollTop(0) : salesNotes.removeClass('scrollable'));

            txt_comment.css('border-color', '#bbbbbb');
            comment_box.text(truncate(data.note.note));

            swal({
                type: 'success',
                title: 'Success...',
                text: 'The item ' + data.performance.category + ' ' + data.performance.company + ' remarks has been created!',
                timer: 5000
            });
            txt_comment.val('');

            salesNotes.prepend(`<div class="bg-success lead-note remark-${data.note.id}">
                                    <span> 
                                        <p>${data.created_by}</p> 
                                        <p>${data.created_at}</p>
                                    </span>

                                    <div class="note-wrapper">
                                        <span id="lead-note-${data.note.id}">${data.note.note}</span>
                                    </div>
                                </div>`);

            $('div.text-center').remove();

            setInterval(function() {
                $(`div.remark-${data.note.id}`).removeClass('bg-success');
            }, 15000);

            $('#btn-add-remarks').attr('disabled', 'disabled');
            setTimeout(enableBtnAddRemark, 10000);

        },
        error: function(error) {
            console.log('Error: ' + error.responseText)
        }
    });
});

$(document).on('change', 'select#limit_entry', function() {
    var baseUrl = $('base').attr('href');

    $.ajax({
        url : baseUrl + '/items/performance/' + this.value,
        beforeSend: function() {
            loading();
        },
        success: function(data) {
            loaded();
            location.reload();
        }
    })
});

$(document).ready(function() {
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }
        console.log(url);

        if (url.indexOf('list') >= 1) {
            getPagination(url);
        } else if(url.indexOf('search') >= 1) {
            getPagination(url, false, true);
        } else {
            getPagination(url, true);
        }

    });
});

$(document).on('click', 'button#btn-change-status', function() {
    const performanceID = $(this).data('perf-id');

    divContainerHideSeek(performanceID, false); // Hide and Seek buttons
});


$(document).on('click', 'button#btn-cancel-update', function() {
    const performanceID = $(this).data('perf-id');

    divContainerHideSeek(performanceID, true);  // Hide and Seek buttons
    $('button[id*="btn-apply-update"]').attr('disabled', 'disabled');

});

$(document).on('change', 'select[id*="change-status-"]', function() {
    $('button[id*="btn-apply-update"]').removeAttr('disabled')
});

$(document).on('click', 'button#btn-apply-update', function() {
    const performanceID = $(this).data('perf-id');
    const status = $(`select#change-status-${performanceID}`);

    $.ajax({
        url: `${$('base').attr('href')}/performance/update_status/${performanceID}`,
        type: 'PUT',
        data: { _token: $('meta[name="csrf-token"]').attr('content'), status: status.val() },
        beforeSend: function() {
            loading();
        },
        success: function(data) {
            loaded();
            divContainerHideSeek(performanceID, true);  // Hide and Seek buttons
            $(`span#status-${performanceID}`).text(data.performance.get_status.systemdesc);
            swal({
                type: 'success',
                title: 'Success...',
                text: 'The item ' + data.performance.category + ' ' + data.performance.company + ' status has been updated!',
                timer: 5000
            });

            $('tr[data-performance="'+data.performance.id+'"]').addClass('bg-warning');

            setInterval(function() {
                $('tr[data-performance="'+data.performance.id+'"]').removeClass('bg-warning');
            }, 10000);

            $('button[id*="btn-apply-update"]').attr('disabled', 'disabled');
        },
        error: function(error) {
            console.log('Error: ' + error.responseText)
        }
    });

});

$(document).on('click', 'button[id="view-perf"]', function() {

    const perfId = $(this).data('perf-id');

    $.ajax({
        url: `${$('base').attr('href')}/performance/lead/${perfId}`,
        beforeSend: function() {
            loading();
        },
        success: function(data) {
            loaded();

            var result = data.result,
                category = data.category;

            $('span#perf-company').text(`${result.company}'s ${category} Information`);
            $('div#view-status').find('p').text(result.status);
            $('div#view-company').find('p').text(result.company);
            $('div#view-contract-value').find('p').text(result.contract_value);
            $('div#view-client-name').find('p').text(result.client_name);
            $('div#view-office-number').find('p').text(result.office_number);
            $('div#view-contact-number').find('p').text(result.contact_number);
            $('div#view-email').find('p').text(result.email);
            $('div#view-designation').find('p').text(result.designation);
            $('div#view-address').find('p').text(result.street_address);
            $('div#view-zip-code').find('p').text(result.zip_code);
            $('div#view-city').find('p').text(result.city);
            $('div#view-website').find('p').text(br2nl(result.website));
            $('div#view-services').find('p').text(result.services);
            $('div#view-other-services').find('p').text(result.other_services);
            $('div#view-source').find('p').text(result.source);
            $('div#view-source-other').find('p').text(result.source_other);
            $('div#view-landing').find('p').text(result.landing_page);
            $('div#view-landing-other').find('p').text(result.landing_other);
            $('div#view-created-by').find('p').text(result.created_by);
            $('div#view-created-at').find('p').text(result.created_at);
        },
        error: function(error) {
            console.log('Error: ' + error.responseText)
        }
    });
});

$(document).on('change', 'select#filter-salesperson', function() {
    var baseUrl = $('base').attr('href');
    $.ajax({ url : baseUrl + '/filter/performance/' + this.value, beforeSend: function() { loading() }, success: function(data) { loaded(); location.reload(); } })
});

/* Helper functions */
function br2nl(str) {  // Convert <br> to new line.
    return str.replace(/<\s*\/?br>/ig, "\r\n");
}

function enableBtnAddRemark() {
    $('#btn-add-remarks').removeAttr('disabled');
}

function divContainerHideSeek(performanceID, show = true) {
    const divStatus = $(`span#div-change-status-${performanceID}`),
        btnStatus = $(`button[id*="btn-change-status"]`),
        btnStatusPerfID =  $(`button#btn-change-status[data-perf-id="${performanceID}"]`),
        spanStatus = $(`span#status-${performanceID}`),
        selectStatus = $(`select#change-status-${performanceID}`),
        remarks = $('button[id*="edit-perf"]'),
        btnView = $('button[id*="view-perf"]');

    if (show === true) {
        divStatus.addClass('hidden');
        selectStatus.addClass('hidden');
        btnStatusPerfID.removeClass('hidden');
        spanStatus.removeClass('hidden');
        btnStatus.removeAttr('disabled');
        remarks.removeAttr('disabled');
        btnView.removeAttr('disabled');
    } else {
        btnStatusPerfID.addClass('hidden');
        spanStatus.addClass('hidden');
        divStatus.removeClass('hidden');
        selectStatus.removeClass('hidden');
        btnStatus.attr('disabled','disabled');
        remarks.attr('disabled', 'disabled');
        btnView.attr('disabled', 'disabled');
    }
}

function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function pagination(pagination,salesPerfRow){
    var baseUrl = $('base').attr('href'),

        url = pagination.path + '?page=';
    var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
        lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
        table = $('table#performance-list > tbody');

    if(pagination.last_page > 1){
        $('ul.pagination').removeClass('hidden');

        var links = "";
        for (var a = 1; a <= pagination.last_page; a++) {
            var half_total_links = Math.floor(7 / 2),
                from = pagination.current_page - half_total_links,
                to = pagination.current_page + half_total_links;

            if (pagination.current_page < half_total_links) {
                to += half_total_links - pagination.current_page;
            }
            if (pagination.last_page - pagination.current_page < half_total_links) {
                from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
            }

            if (from < a && a < to) {
                var isActive = pagination.current_page === a ? 'active' : '';
                links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
            }
        }
        $('ul.pagination').html(fpage+' '+links+' '+lpage);
        $('ul.pagination').removeClass('hidden');
    }
    else {
        $('ul.pagination').addClass('hidden');
    }

    table.html(salesPerfRow).fadeIn('fast');
}

function getPagination(uri, sort = false, search = false) {
    var salesPerfRow = "",
        baseUrl = $('base').attr('href');

    $.ajax({
        url: uri,
        beforeSend:function(){
            $('table#performance-list > tbody').html('<div class="leadnotes-loading"></div>'),
                loading();
        },
        success:function(data){
            loaded();
            if (data.performance.length > 0){
                $.each(data.performance,function(i,j){
                    i = ((data.pagination.current_page * data.pagination.per_page) - data.pagination.per_page ) + i;
                    salesPerfRow += "<tr data-performance='"+ j.id + "'>";
                    salesPerfRow += "<td class='text-center company'>"+ j.company+ "</td>";
                    salesPerfRow += "<td class='text-center client_name'>"+ j.client_name+ "</td>";
                    salesPerfRow += "<td class='text-center contact_number'>"+ j.contact_number+ "</td>";
                    salesPerfRow += "<td class='text-center contract_value'>"+ j.contract_value+ "</td>";
                    salesPerfRow += "<td class='text-center services'>"+ j.services+ "</td>";
                    // salesPerfRow += "<td class='text-center landing_page'>"+ j.landing_page+ "</td>";
                    salesPerfRow += "<td class='text-center created_at'>"+ j.created_at +"</td>";
                    salesPerfRow += (data.role == 1 ? "<td class='text-center created_by'>"+ j.created_by +"</td>" : '');
                    // salesPerfRow += "<td class='text-center category'>"+ j.category+ "</td>";
                    salesPerfRow += "<td class='text-center status'>"+
                        '<span id="status-'+j.id+'" title="'+(j.status_updated_by && j.status_updated_at ? 'Last Updated: ' + j.status_updated_at + ' Last Updated By: ' + j.status_updated_by : 'No further remarks!')+ '">'
                        + j.status +
                        '</span>' +
                        "<select id='change-status-"+j.id+"' class='form-control w-150 hidden'>";

                    $.each(data.statuses, function(k, l) {
                        salesPerfRow += '<option value="'+ l.systemcode +'" '+ (j.status == l.systemdesc ? 'selected' : '') + '>'+ l.systemdesc +'</option>';
                    });
                    salesPerfRow += "</select></td>";
                    salesPerfRow += "<td class='text-center remarks' id='td-comment-"+j.id+"'><span id='comment-box' data-perf-id='"+j.id+"'>"+ truncate(j.remarks) + (j.remarks_date != 'TBA' ? '<small>('+ j.remarks_date +'</small>)' : '') + "</span></td>";
                    salesPerfRow += '<td class="text-center actions text-nowrap">' +
                        '<button title="View information" data-toggle="modal" data-target="#view_information" id="view-perf" data-perf-id="'+j.id+'" class="btn btn-info"><i class="fa fa-eye"></i></button> ' +
                        '<button title="Change status" id="btn-change-status" data-perf-id="'+j.id+'" class="btn btn-danger"><i class="fa fa-exchange"></i></button> ' +
                        '<span id="div-change-status-'+j.id+'" class="hidden">' +
                        ' <button title="Cancel" id="btn-cancel-update" class="btn btn-danger" data-perf-id="'+j.id+'"><i class="fa fa-times"></i></button>' +
                        ' <button title="Save" id="btn-apply-update" class="btn btn-success" disabled data-perf-id="'+j.id+'"><i class="fa fa-save"></i></button>' +
                        '</span> ' +
                        '<button title="Add remarks" data-toggle="modal" data-target="#view_contract_modal" id="edit-perf" data-perf-id="'+j.id+'" class="btn btn-primary"><i class="fa fa-edit"></i></button></td>';

                });

                pagination(data.pagination,salesPerfRow);
                $('ul.pagination').removeClass('hidden');
            }
            else {
                salesPerfRow = "<tr><td colspan='12' class='text-center'>No records found</td></tr>";

                $('ul.pagination').addClass('hidden');
            }

            if(sort == true) {
                var route = '/performance/sort/' + data.field + '/' + data.order;
                pagination(data.pagination,salesPerfRow,route);

            }
            else if(search = true)
            {
                var route = '/performance/search/' + data.key;
                pagination(data.pagination,salesPerfRow,route);
            }
            else {
                var route = window.location.href;
                pagination(data.pagination,salesPerfRow,route);
            }
        }
    });
}

function clearSorting() {
    $('i#sort-account').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created_at')
            $(this).parent().addClass('active');
    });
}

function triggerSearch(e){
    const search = $('#searchSalesAcc');

    if(e.keyCode === 13) {
        e.preventDefault();

        if(!search.val()) {
            return false;
        }

        var url = $('base').attr('href') + '/performance/search/' + search.val();
        getPagination(url, false, true);
    }
}

function truncate(string){
    if (string.length > 13)
        return string.substring(0,13)+'...';
    else
        return string;
};