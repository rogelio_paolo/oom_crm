$('select[id="export-salesperson"]').on('change', function() {
    const salesPerson = $(this);
    setExportHref(salesPerson);
});

$('a[id="btn-export"]').on('click', function()  {
    $(this).attr('disabled', 'disabled');
    setTimeout(enableBtnExport, 3000);
});

$('input[id="opportunity-date-checked"]').on('change', function() {
    const value = this.checked,
        start = $('input[id="opportunity_start_date"]'),
        end = $('input[id="opportunity_end_date"]'),
        salesPerson =  $('select[id="export-salesperson"]');

    start.val(moment().startOf('month').format('YYYY-MM-DD'));
    end.val(moment().format('YYYY-MM-DD'));


    $('input[id="opportunity_daterange"]').val(moment().startOf('month').format('YYYY/MM/DD') +' - ' + moment().format('YYYY/MM/DD'));

    if (value === true) {
        $('div[id="opportunity-date-container"]').removeClass('hidden')
    } else {
        $('div[id="opportunity-date-container"]').addClass('hidden')

    }

    setExportHref(salesPerson);
});

$('input[id="appointment-date-checked"]').on('change', function() {
    const value = this.checked,
        start = $('input[id="appointment_start_date"]'),
        end = $('input[id="appointment_end_date"]'),
        salesPerson =  $('select[id="export-salesperson"]');

    start.val(moment().isoWeekday(1).format('YYYY-MM-DD'));
    end.val(moment().isoWeekday(7).format('YYYY-MM-DD'));

    $('input[id="appointment_daterange"]').val(moment().isoWeekday(1).format('YYYY/MM/DD') +' - ' + moment().isoWeekday(7).format('YYYY/MM/DD'));

    if (value === true) {
        $('div[id="appointment-date-container"]').removeClass('hidden')
    } else {
        $('div[id="appointment-date-container"]').addClass('hidden')
    }

    setExportHref(salesPerson);
});

$(function() {
    $('input[name="opportunity_daterange"]').daterangepicker({
        opens: 'right',
        minDate: "12/31/2017",
        maxDate: moment().add(1, 'days'),
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
        autoApply: true
    }, function(start, end, label) {
        const salesPerson =  $('select[id="export-salesperson"]');

        $('input[id="opportunity_start_date"]').val(start.format('YYYY-MM-DD'));
        $('input[id="opportunity_end_date"]').val(end.format('YYYY-MM-DD'));

        setExportHref(salesPerson);
    });

    $('input[name="appointment_daterange"]').daterangepicker({
        opens: 'right',
        minDate: "12/31/2017",
        startDate: moment().isoWeekday(1),
        endDate: moment().isoWeekday(7),
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Current Week': [moment().isoWeekday(1), moment().isoWeekday(7)],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
        autoApply: true
    }, function(start, end, label) {
        const salesPerson =  $('select[id="export-salesperson"]');

        $('input[id="appointment_start_date"]').val(start.format('YYYY-MM-DD'));
        $('input[id="appointment_end_date"]').val(end.format('YYYY-MM-DD'));

        setExportHref(salesPerson);
    });
});

$(document).on('click', 'a[id="btn-sales-report-modal"]', function() {
    const opportunityStart = $('input[id="opportunity_start_date"]'),
        opportunityEnd = $('input[id="opportunity_end_date"]'),
        appointmentStart = $('input[id="appointment_start_date"]'),
        appointmentEnd = $('input[id="appointment_end_date"]'),
        salesPerson = $('select[id="export-salesperson"]').find('option:first-child');

    opportunityStart.val(moment().startOf('month').format('YYYY-MM-DD'));
    opportunityEnd.val(moment().format('YYYY-MM-DD'));
    appointmentStart.val(moment().isoWeekday(1).format('YYYY-MM-DD'));
    appointmentEnd.val(moment().isoWeekday(7).format('YYYY-MM-DD'));

    $('input[id="opportunity_daterange"]').val(moment().startOf('month').format('YYYY/MM/DD') +' - ' + moment().format('YYYY/MM/DD'));
    $('input[id="appointment_daterange"]').val(moment().isoWeekday(1).format('YYYY/MM/DD') +' - ' + moment().isoWeekday(7).format('YYYY/MM/DD'));

    setExportHref(salesPerson);

});

// HELPER FUNCTION
function enableBtnExport() {
    $('a[id="btn-export"]').removeAttr('disabled');
}

function setExportHref(salesPerson) {
    const opportunityStart = $('input[id="opportunity_start_date"]'),
        opportunityEnd = $('input[id="opportunity_end_date"]'),
        appointmentStart = $('input[id="appointment_start_date"]'),
        appointmentEnd = $('input[id="appointment_end_date"]'),
        opportunityCheck = $('input[id="opportunity-date-checked"]'),
        appointmentCheck = $('input[id="appointment-date-checked"]');

    $('a[id="btn-export"]').attr('href', `/performance/export/${salesPerson.val() === undefined ? null : salesPerson.val()}/${opportunityStart.val()}/${opportunityEnd.val()}/${appointmentStart.val()}/${appointmentEnd.val()}/${appointmentCheck.is(':checked') ? 1 : 0}`);
}
