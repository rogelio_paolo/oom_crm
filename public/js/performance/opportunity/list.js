

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();  

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }
        console.log(url);

        if (url.indexOf('list') >= 1) {
            getPagination(url);
        } else if(url.indexOf('opportunity/search') >= 1) {
            getPagination(url, false, true);
        } else {
            getPagination(url, true);
        }

    });
});

$(document).on('change', 'select#limit_entry', function() {
    var baseUrl = $('base').attr('href');

    $.ajax({
        url : baseUrl + '/items/opportunity/' + this.value,
        beforeSend: function() {
            loading();
        },
        success: function(data) {
            loaded();
            location.reload();
        }
    })
});


$(document).on('click', 'a#btn-refresh-table', function(e){
    e.preventDefault();
    var url = $('base').attr('href') + '/opportunity/refresh';
    clearSorting();
    getPagination(url);
    $('#searchOpportunity').val('');
});

$(document).on('click','a.sort-opp', function(e){
    e.preventDefault();
    var root = $('base').attr('href');
    var currOrder = $(this).find('i').attr('class'),
        sortOrder = "",
        sortBy = $(this).data('sortby');

    $(this).addClass('active');
    $(this).parent().siblings().find('a').removeClass('active');

    if (currOrder.indexOf('asc') >= 1)
        $(this).find('i').removeClass('fa-sort-alpha-asc'),
        $(this).find('i').addClass('fa-sort-alpha-desc');
    else
        $(this).find('i').removeClass('fa-sort-alpha-desc'),
        $(this).find('i').addClass('fa-sort-alpha-asc');

    sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

    var url = root + '/opportunity/sort/' + sortBy + '/' + sortOrder;
    
    getPagination(url, true);
});

$('#btn-search-opportunity').click(function() {
    const search = $('#searchOpportunity');
    var url = $('base').attr('href') + '/opportunity/search/' + search.val();
    getPagination(url, false, true);
});

$(document).on('click', 'button#btn-change-status', function() {
    const opportunityID = $(this).data('opp-id');
   
   divContainerHideSeek(opportunityID, false); // Hide and Seek buttons
});


$(document).on('click', 'button#btn-cancel-update', function() {
    const opportunityID = $(this).data('opp-id');
   
    divContainerHideSeek(opportunityID, true);  // Hide and Seek buttons
    $('button[id*="btn-apply-update"]').attr('disabled', 'disabled');
});

$(document).on('change', 'select[id*="change-status-"]', function() {
    $('button[id*="btn-apply-update"]').removeAttr('disabled')
});

$(document).on('click', 'button#btn-apply-update', function() {
    const opportunityID = $(this).data('opp-id');

    const status = $(`select#change-status-${opportunityID}`);

    swal({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Update'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `${$('base').attr('href')}/opportunity/update/${opportunityID}`,
                type: 'PUT',
                data: { _token: $('meta[name="csrf-token"]').attr('content'), status: status.val() },
                beforeSend: function() {
                    loading();
                },
                success: function(data) {
                    loaded();
                    divContainerHideSeek(opportunityID, true);  // Hide and Seek buttons
                    $(`span#status-${opportunityID}`).text(data.opportunities.get_status.systemdesc);
                    swal({
                        type: 'success',
                        title: 'Success...',
                        text: 'The opportunity ' + data.opportunities.category + ' ' + data.opportunities.company + ' status has been updated!',
                        timer: 5000
                    });

                    $('tr[data-opp-id="'+data.opportunities.id+'"]').addClass('bg-warning');

                    setInterval(function() {
                        $('tr[data-opp-id="'+data.opportunities.id+'"]').removeClass('bg-warning');
                    }, 10000);
                },
                error: function(error) {
                    console.log('Error: ' + error.responseText)
                }
            });
        }
      })


});

/* HELPER FUNCTIONS */
function divContainerHideSeek(opportunityID, show = true) {
    const divStatus = $(`div#div-change-status-${opportunityID}`),
          btnStatus =  $(`button#btn-change-status[data-opp-id="${opportunityID}"]`),
          spanStatus = $(`span#status-${opportunityID}`),
          selectStatus = $(`select#change-status-${opportunityID}`);

    if(show === true) {
        divStatus.addClass('hidden');
        selectStatus.addClass('hidden');
        btnStatus.removeClass('hidden');
        spanStatus.removeClass('hidden');
    
        $(`button[id*="btn-change-status"]`).removeAttr('disabled');
    } else {
        btnStatus.addClass('hidden');
        spanStatus.addClass('hidden');
        divStatus.removeClass('hidden');
        selectStatus.removeClass('hidden');
    
        $(`button[id*="btn-change-status"]`).attr('disabled','disabled');
        
    }
}

function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function pagination(pagination,opportunityRow){
    var baseUrl = $('base').attr('href'),
        
        url = pagination.path + '?page=';
        var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#opportunity-list > tbody');

        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');

            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to) {
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else {
            $('ul.pagination').addClass('hidden');
        }

        table.html(opportunityRow).fadeIn('fast');
}

function getPagination(uri, sort = false, search = false) {
    var opportunityRow = "",
        baseUrl = $('base').attr('href');

    $.ajax({
        url: uri,
        beforeSend:function(){
            $('table#opportunity-list > tbody').html('<div class="leadnotes-loading"></div>'),
            loading();
            
        },
        success:function(data){
            loaded();
            
            if (data.opportunities.length > 0){
                $.each(data.opportunities,function(i,j){
                    i = ((data.pagination.current_page * data.pagination.per_page) - data.pagination.per_page ) + i;
                    opportunityRow += "<tr data-opp-id='"+ j.id + "'>";
                    opportunityRow += "<td class='text-center company'>"+ j.company+ "</td>";
                    opportunityRow += "<td class='text-center f_name'>"+ j.client_name+ "</td>";
                    opportunityRow += "<td class='text-center contact_number'>"+ j.contact_number+ "</td>";
                    opportunityRow += "<td class='text-center category'>"+ j.category+ "</td>";
                    opportunityRow += "<td class='text-center created_at'>"+ j.created_at+ "</td>";
                    opportunityRow += (data.role == '1' ? "<td class='text-center busi_manager'>"+ j.busi_manager+ "</td>" : '');
                    opportunityRow += "<td class='text-center status'>"+
                                            '<span id="status-"'+j.id+'" data-toggle="tooltip" title="'+(j.status_updated_by && j.status_updated_at ? 'Last Updated: ' + j.status_updated_at + ' Last Updated By: ' + j.status_updated_by : '')+ '">'
                                                + j.status +
                                            '</span>' +
                                            "<select id='change-status-"+j.id+"' class='form-control hidden'>";

                    $.each(data.statuses, function(k, l) {
                        opportunityRow += '<option value="'+ l.systemcode +'" '+ (j.status == l.systemdesc ? 'selected' : '') + '>'+ l.systemdesc +'</option>';
                    });

                    opportunityRow += "</select></td>";
                    opportunityRow += "<td class='text-center actions'>" +
                                            '<button id="btn-change-status" class="btn btn-primary" data-opp-id="'+j.id+'"><i class="fa fa-edit"></i> Change</button>' +
                                            '<div id="div-change-status-'+j.id+'" class="hidden">' +
                                                '<button id="btn-cancel-update" class="btn btn-danger" data-opp-id="'+j.id+'"><i class="fa fa-times"></i> Cancel</button> '+
                                                '<button id="btn-apply-update" class="btn btn-success" disabled data-opp-id="'+j.id+'"><i class="fa fa-save"></i> Save</button>' +
                                            '</div>'
                                    + "</td></tr>";
                });

                pagination(data.pagination,opportunityRow);
                $('ul.pagination').removeClass('hidden');
            }
            else {
                opportunityRow = "<tr><td colspan='7' class='text-center'>No records found</td></tr>";

                $('ul.pagination').addClass('hidden');
            }

            if(sort == true) {
                var route = '/opportunity/sort/' + data.field + '/' + data.order;
                pagination(data.pagination,opportunityRow,route);
            } 
            else if(search = true)
            {
                var route = '/opportunity/search/' + data.key;
                pagination(data.pagination,opportunityRow,route);
                $('ul.pagination').addClass('hidden');
            }
            else {
                var route = window.location.href;
                pagination(data.pagination,opportunityRow,route);
            }
        }
    });
}

function clearSorting() {
    $('i#sort-opp').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created_at')
            $(this).parent().addClass('active');
    });
}

function triggerSearch(e){
    const search = $('#searchOpportunity');

    if(e.keyCode === 13) {
        e.preventDefault();
        
        if(!search.val()) {
            return false;
        }
        
        var url = $('base').attr('href') + '/opportunity/search/' + search.val();
        getPagination(url, false, true);
    }
}