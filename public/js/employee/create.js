
(function(){
    let root = $('base').attr('href'),
        uri = window.location.href;

    $(document).ready(function(){
        let branch = $('select#branch option:selected').val();

        if (branch === 'sg'){
            $('div.sg-bank-info').css('display','block');
            $('div.ph-benefits').css('display','none');
            $('div.ph-name').css('display','none');

            // NRIC Number
            $('div#nric').css('display','block');
        }
        else{
            $('div.ph-benefits').css('display','block');
            $('div.sg-bank-info').css('display','none');
            $('div.ph-name').css('display','block');

             // NRIC Number
            $('div#nric').css('display','none');
        }

    });

    $('select#branch').on('change', function(){
        let branch = this.value;

        if (branch === 'sg'){
            $('div.sg-bank-info').css('display','block');
            $('div.ph-benefits').css('display','none');
            $('div.ph-name').css('display','none');

               // NRIC Number
            $('div#nric').css('display','block');
        }
        else{
            $('div.ph-benefits').css('display','block');
            $('div.sg-bank-info').css('display','none');
            $('div.ph-name').css('display','block');

            // NRIC Number
            $('div#nric').css('display','none');
        }
    });

    $(document).on('click','a#employeeDelete', function(){
        let empid = $(this).data('employeeid');
        $('#delete-employee').val(empid);
        $('#deleteEmployeeModal').modal('show');
    });

    if (uri.indexOf('/employee/edit') < 0){
        $(document).ready(function(){
            let branch = $('select#branch option:selected').val();

            $.ajax({
                url : root + '/employee/new/' + branch,
                success:function(empid){
                    $('input[id="emp_id"]').val(empid);
                }
            })
        });

        $('select#branch').on('change',function(){
            let branch = this.value;

            $.ajax({
                url : root + '/employee/new/' + branch,
                success:function(empid){
                    $('input[id="emp_id"]').val(empid);
                }
            })
        });
    }

    $(document).on('change','select#dept_code',function(){
        let dept = this.value,
            designation = $('select#designation');

        if(dept == 'DP001' || dept == 'DP002' || dept == 'DP007') {
            $('div#sales-target-container').removeClass('hidden');
            // $('input[id="sales_target"]').val('');
        } else {
            $('div#sales-target-container').addClass('hidden');
            $('input[id="sales_target"]').val('');
        }

        $.ajax({
            url : root + '/employee/designations/' + dept,
            success:function(data){
                let options = "";
                $.each(data, function(i,j){
                    options += '<option value="'+j.designation_code+'">'+j.designation+'</option>';
                });
                
                designation.html(options);
            }
        })
    });

    // Input total sales target change into parse float
    $(document).on('change', 'input[id="sales_target"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).ready(function(){
        let dept = $('select#dept_code option:selected').val(),
            designation = $('select#designation');

        if(dept == 'DP001' || dept == 'DP002' || dept == 'DP007') {
            $('div#sales-target-container').removeClass('hidden');
            // $('input[id="sales_target"]').val('');
        } else {
            $('div#sales-target-container').addClass('hidden');
            $('input[id="sales_target"]').val('');
        }

        $.ajax({
            url : root + '/employee/designations/' + dept,
            success:function(data){
                let options = "";
                $.each(data, function(i,j){
                    options += '<option value="'+j.designation_code+'">'+j.designation+'</option>';
                });

                designation.html(options);
            }
        })
    });
    
    $(document).on('click','button.add-btn', function(){
       $(this).after('<div class="loader-overlay"></div>'); 
    });

})();
