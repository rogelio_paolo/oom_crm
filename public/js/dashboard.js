function filterTotalAccounts (dates,data,from,to) {
    $('#TotalAccountsChart').highcharts({
        chart: {
            renderTo: 'container',
            type: 'column',
        },
        colors: ['#00c1ae'],
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: dates
        },
        series: [{
            name: 'Total Accounts',
            data: data
        }]
   });
}

function filterAccountsPerStatus (statuses,data) {
    $('#AccountsPerStatusChart').highcharts({
        chart:{
            type: 'pie', 
            options3d: {
                enabled:true,
                alpha:45,
            }
        },
        plotOptions:{
            pie: {
                innerSize:100,
                depth:45
            }
        },
        colors: ['#5bbc64','#fb5e64'],
        title:{
            text: 'Total Accounts Created'
        },
        subtitle: {
            text:'Per Status'
        },
        series: [{
            name: 'Total Accounts Per Package',
            data: data
        }]
    });
}

function filterAccountsPerPackage (packages,data) {
    $('#AccountsPerPackageChart').highcharts({
        chart:{
            type: 'pie', 
            options3d: {
                enabled:true,
                alpha:45,
            }
        },
        plotOptions:{
            pie: {
                innerSize:100,
                depth:45
            }
        },
        colors: ['#c983be','#3594d8','#977acf','#ff8641'],
        title:{
            text: 'Total Accounts Created'
        },
        subtitle: {
            text:'Per Package'
        },
        series: [{
            name: 'Total Accounts Per Package',
            data: data
        }]
    });
}

function filterTotalLeads (data) {
    var seriesname = data.filterby === 'sales' ? 'Total Leads of '+data.sales : (data.filterby === 'date' ? 'Total Leads' : 'Total Leads' );
    
    $('#TotalLeadsChart').highcharts({
        chart: {
            renderTo: 'container',
            type: 'column',
        },
        colors: ['#00c1ae'],
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: data.leadchart.labels
        },
        series: [{
            name: seriesname,
            data: data.leadchart.datasets[0].values
        }]
   });
}

function filterLeadsPerStatus (statuses,data) {
    $('#LeadsPerStatusChart').highcharts({
        chart:{
            type: 'pie', 
            options3d: {
                enabled:true,
                alpha:45,
            }
        },
        plotOptions:{
            pie: {
                innerSize:100,
                depth:45
            }
        },
        colors: ['#fb5e64','#ff8641','#5bbc64','#3594d8','#977acf','#c983be'],
        title:{
            text: 'Total Leads Created'
        },
        subtitle: {
            text:'Per Status'
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Leads Per Status',
            data: data
        }]
    });
}

(function(){

    var root = $('base').attr('href');

    $('select#filterAccounts').on('change', function(){
        var filter = $(this).val(),
            filterBy = "";
        
        if (filter === 'date')
            $('div#filter-by-date').removeClass('hidden');
        else
            $('div#filter-by-date').addClass('hidden');

        if (filter === 'holder')
            $('div#filter-by-holder').removeClass('hidden');
        else
            $('div#filter-by-holder').addClass('hidden');

        if (filter === 'all')
            $('div#filterButtons').removeClass('hidden');
        else
            $('div#filterButtons').addClass('hidden');

        // filterBy = filter === 'date' ? 

        // $.ajax({
        //     url: root + '/dashboard/filter/accounts/'+
        // });
    });

    $('button#FilterAccountByDate').on('click', function(){
        var datefrom = $('input[id="filterDateFrom"]').val(),
            dateto = $('input[id="filterDateTo"]').val(),
            chart = '<div id="TotalAccountsChart"></div>',
            chartPerStatus = '<div id="AccountsPerStatusChart"></div>',
            chartPerPackage = '<div id="AccountsPerPackageChart"></div>',
            chartsNoData = '<div id="no-account-chart" class="col-md-12 text-center">No graphs to show for Accounts</div>';
            
            $.ajax({
                url: root + '/dashboard/filter/accounts/date/null/'+datefrom+'/'+dateto,
                success:function(data){
                    if (data.count > 0){
                        $('div#chart-total-accounts').html(chart);
                        filterTotalAccounts(data.chart.labels,data.chart.datasets[0].values,data.datefrom,data.dateto);
                        $('div#chart-accounts-perstatus').html(chartPerStatus);
                        filterAccountsPerStatus(data.chartPerStatus.labels,data.chartPerStatus.datasets[0].values);
                        $('div#chart-accounts-perpackage').html(chartPerPackage);
                        filterAccountsPerPackage(data.chartPerPackage.labels,data.chartPerPackage.datasets[0].values);
                        $('div#accounts-charts').show(),
                        $('div#no-account-chart').remove();
                        $('a#ExportAccountByDate').removeAttr('disabled');
                        $('a#ExportAccountByDate').attr('href',root + '/dashboard/filter/accounts/export/excel/date/null/'+datefrom+'/'+dateto);
                    }
                    else{
                        $('div#accounts-charts').hide();
                        if ($('div#no-account-chart').length <= 0)
                            $('div#accounts-charts').after(chartsNoData);

                        $('a#ExportAccountByDate').attr('disabled','disabled');
                    }

                }
            });
    });

    $('button#FilterAccountByHolder').on('click', function(){
        var userid = $('select#account-holders option:selected').val(),
            chart = '<div id="TotalAccountsChart"></div>',
            chartPerStatus = '<div id="AccountsPerStatusChart"></div>',
            chartPerPackage = '<div id="AccountsPerPackageChart"></div>',
            chartsNoData = '<div id="no-account-chart" class="col-md-12 text-center">No graphs to show for Accounts</div>';
            
            $.ajax({
                url: root + '/dashboard/filter/accounts/holder/'+userid+'/null/null',
                success:function(data){
                    if (data.count > 0){
                        $('div#chart-total-accounts').html(chart);
                        filterTotalAccounts(data.chart.labels,data.chart.datasets[0].values,data.datefrom,data.dateto);
                        $('div#chart-accounts-perstatus').html(chartPerStatus);
                        filterAccountsPerStatus(data.chartPerStatus.labels,data.chartPerStatus.datasets[0].values);
                        $('div#chart-accounts-perpackage').html(chartPerPackage);
                        filterAccountsPerPackage(data.chartPerPackage.labels,data.chartPerPackage.datasets[0].values);
                        $('div#accounts-charts').show();
                        $('div#no-account-chart').remove();
                        $('a#ExportAccountByHolder').removeAttr('disabled');
                        $('a#ExportAccountByHolder').attr('href',root + '/dashboard/filter/accounts/export/excel/holder/'+userid+'/null/null');
                    }
                    else{
                        $('div#accounts-charts').hide();
                        if ($('div#no-account-chart').length <= 0)
                            $('div#accounts-charts').after(chartsNoData);

                        $('a#ExportAccountByHolder').attr('disabled','disabled');
                    }

                }
            });
    });

    $('button#FilterAccountAll').on('click', function(){
        var chart = '<div id="TotalAccountsChart"></div>',
            chartPerStatus = '<div id="AccountsPerStatusChart"></div>',
            chartPerPackage = '<div id="AccountsPerPackageChart"></div>',
            chartsNoData = '<div id="no-account-chart" class="col-md-12 text-center">No graphs to show for Accounts</div>';

        $.ajax({
            url: root + '/dashboard',
            success:function(data){
                if (data.count > 0){
                    $('div#chart-total-accounts').html(chart);
                    filterTotalAccounts(data.chart.labels,data.chart.datasets[0].values,data.datefrom,data.dateto);
                    $('div#chart-accounts-perstatus').html(chartPerStatus);
                    filterAccountsPerStatus(data.chartPerStatus.labels,data.chartPerStatus.datasets[0].values);
                    $('div#chart-accounts-perpackage').html(chartPerPackage);
                    filterAccountsPerPackage(data.chartPerPackage.labels,data.chartPerPackage.datasets[0].values);
                    $('div#accounts-charts').show();
                    $('div#no-account-chart').remove();
                }
                else{
                    $('div#accounts-charts').hide();
                    if ($('div#no-account-chart').length <= 0)
                        $('div#accounts-charts').after(chartsNoData);
                }

            }
        });
    });

    $('select#filterLeads').on('change', function(){
        var filter = $(this).val(),
            filterBy = "";
        
        if (filter === 'date')
            $('div#lead-by-date').removeClass('hidden');
        else
            $('div#lead-by-date').addClass('hidden');

        if (filter === 'sales')
            $('div#lead-by-sales').removeClass('hidden');
        else
            $('div#lead-by-sales').addClass('hidden');

        if (filter === 'all')
            $('div#leadButtons').removeClass('hidden');
        else
            $('div#leadButtons').addClass('hidden');

        // filterBy = filter === 'date' ? 

        // $.ajax({
        //     url: root + '/dashboard/filter/accounts/'+
        // });
    });

    $('button#FilterLeadsByDate').on('click', function(){
        var datefrom = $('input[id="leadDateFrom"]').val(),
            dateto = $('input[id="leadDateTo"]').val(),
            chart = '<div id="TotalLeadsChart"></div>',
            chartPerStatus = '<div id="LeadsPerStatusChart"></div>',
            chartPerPackage = '<div id="LeadsPerPackageChart"></div>',
            chartsNoData = '<div id="no-lead-chart" class="col-md-12 text-center">No graphs to show for Leads</div>';
            
            $.ajax({
                url: root + '/dashboard/filter/leads/date/null/'+datefrom+'/'+dateto,
                success:function(data){

                    if (data.count > 0){
                        $('div#chart-total-leads').html(chart);
                        filterTotalLeads(data);
                        $('div#chart-leads-perstatus').html(chartPerStatus);
                        filterLeadsPerStatus(data.leadchartPerStatus.labels,data.leadchartPerStatus.datasets[0].values);
                        $('div#leads-charts').show(),
                        $('div#no-lead-chart').remove();
                        $('a#ExportLeadsByDate').removeAttr('disabled');
                        $('a#ExportLeadsByDate').attr('href',root + '/dashboard/filter/leads/export/excel/date/null/'+datefrom+'/'+dateto);
                    }
                    else{
                        $('div#leads-charts').hide();
                        if ($('div#no-lead-chart').length <= 0)
                            $('div#leads-charts').after(chartsNoData);

                        $('a#ExportLeadsByDate').attr('disabled','disabled');
                    }

                }
            });
    });

    $('button#FilterLeadsBySales').on('click', function(){
        var userid = $('select#lead-sales option:selected').val(),
            chart = '<div id="TotalLeadsChart"></div>',
            chartPerStatus = '<div id="LeadsPerStatusChart"></div>',
            chartPerPackage = '<div id="LeadsPerPackageChart"></div>',
            chartsNoData = '<div id="no-lead-chart" class="col-md-12 text-center">No graphs to show for Leads</div>';
            
            $.ajax({
                url: root + '/dashboard/filter/leads/sales/'+userid+'/null/null',
                success:function(data){
                    if (data.count > 0){
                        $('div#chart-total-leads').html(chart);
                        filterTotalLeads(data);
                        $('div#chart-leads-perstatus').html(chartPerStatus);
                        filterLeadsPerStatus(data.leadchartPerStatus.labels,data.leadchartPerStatus.datasets[0].values);
                        $('div#leads-charts').show();
                        $('div#no-lead-chart').remove();
                        $('a#ExportLeadsBySales').removeAttr('disabled');
                        $('a#ExportLeadsBySales').attr('href',root + '/dashboard/filter/leads/export/excel/sales/'+userid+'/null/null');
                    }
                    else{
                        $('div#leads-charts').hide();
                        if ($('div#no-lead-chart').length <= 0)
                            $('div#leads-charts').after(chartsNoData);

                        $('a#ExportLeadsBySales').attr('disabled','disabled');
                    }

                }
            });
    });

    $('button#FilterLeadAll').on('click', function(){
        var chart = '<div id="TotalLeadsChart"></div>',
            chartPerStatus = '<div id="LeadsPerStatusChart"></div>',
            chartPerPackage = '<div id="LeadsPerPackageChart"></div>',
            chartsNoData = '<div id="no-lead-chart" class="col-md-12 text-center">No graphs to show for Leads</div>';

        $.ajax({
            url: root + '/dashboard',
            success:function(data){
                if (data.count > 0){
                    $('div#chart-total-leads').html(chart);
                    filterTotalLeads(data);
                    $('div#chart-leads-perstatus').html(chartPerStatus);
                    filterLeadsPerStatus(data.leadchartPerStatus.labels,data.leadchartPerStatus.datasets[0].values);
                    $('div#leads-charts').show();
                    $('div#no-lead-chart').remove();
                }
                else{
                    $('div#leads-charts').hide();
                    if ($('div#no-lead-chart').length <= 0)
                        $('div#leads-charts').after(chartsNoData);
                }

            }
        });
    });

})();