function isNumberKey(e,length){
    var charCode = (e.which) ? e.which : e.keyCode,
        other_duration = $('input[id="other_duration"]');

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

(function(){

    $(document).on('change','select#nature',function(){
        var nature = $(this).val();

        if ($.inArray('NP001',nature) === -1){
            $('div.sem-included').addClass('hidden');
            $('input[id="sem_included"]').val('no');
        }
        else{
            $('div.sem-included').removeClass('hidden');
            $('input[id="sem_included"]').val('yes');
        }


        if ($.inArray('NP002',nature) === -1){
            $('div.fb-included').addClass('hidden');
            $('input[id="fb_included"]').val('no');
        }
        else{
            $('div.fb-included').removeClass('hidden');
            $('input[id="fb_included"]').val('yes');
        }

        if ($.inArray('NP003',nature) === -1){
            $('div.seo-included').addClass('hidden');
            $('input[id="seo_included"]').val('no');
        }
        else{
            $('div.seo-included').removeClass('hidden');
            $('input[id="seo_included"]').val('yes');
        }
        
        if ($.inArray('NP004',nature) === -1){
            $('div.web-included').addClass('hidden');
            $('input[id="web_included"]').val('no');
        }
        else{
            $('div.web-included').removeClass('hidden');
            $('input[id="web_included"]').val('yes');
        }

        if ($.inArray('NP005',nature) === -1){
            $('div.baidu-included').addClass('hidden');
            $('input[id="baidu_included"]').val('no');
        }
        else{
            $('div.baidu-included').removeClass('hidden');
            $('input[id="baidu_included"]').val('yes');
        }

        if ($.inArray('NP006',nature) === -1){
            $('div.weibo-included').addClass('hidden');
            $('input[id="weibo_included"]').val('no');
        }
        else{
            $('div.weibo-included').removeClass('hidden');
            $('input[id="weibo_included"]').val('yes');
        }

        if ($.inArray('NP007',nature) === -1){
            $('div.wechat-included').addClass('hidden');
            $('input[id="wechat_included"]').val('no');
        }
        else{
            $('div.wechat-included').removeClass('hidden');
            $('input[id="wechat_included"]').val('yes');
        }

        if ($.inArray('NP008',nature) === -1){
            $('div.blog-included').addClass('hidden');
            $('input[id="blog_included"]').val('no');
        }
        else{
            $('div.blog-included').removeClass('hidden');
            $('input[id="blog_included"]').val('yes');
        }

        if ($.inArray('NP009',nature) === -1){
            $('div.social-included').addClass('hidden');
            $('input[id="social_included"]').val('no');
        }
        else{
            $('div.social-included').removeClass('hidden');
            $('input[id="social_included"]').val('yes');
        }
        
    });

    $(document).on('click', 'button.btn-success', function () {
        $('div.loader-overlay').removeClass('hidden');
    });

//------------------SEM-----------------//

    $(document).on('change', 'select#target_market', function () {

        var selected = $(this).val().join('|');
        
        $('input[id="target_market_array"]').val(selected);

    });

    // Input radio button if has ad scheduling
    $(document).on('click','input[id="ad_scheduling"]', function(){
        var ad_scheduling = $(this).val(),
        budget_field = $('input[id="google_daily_budget"]'),
        duration_other = $('input[id="duration_other"]').val(),
        monthly_field = $('input[id="google_monthly_budget"]'),
        ad_sched_remark = $('#ad_scheduling_remark');

        if(ad_scheduling === '1'){
            budget_field.val(''),
            budget_field.removeAttr('readonly'),
            ad_sched_remark.removeClass('hidden');
                if ($('span#sem_daily_budget').length <= 0){
                    budget_field.after('<span class="error-p account" id="sem_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
                }
                else{
                    return false;
                }
        }   
        else{
            ad_sched_remark.addClass('hidden');
            ad_sched_remark.val('');
            if (monthly_field !== '') {
                daily_result = calculateSEMDailyBudget(duration_other, true);
                number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
                budget_field.val(number);
                budget_field.attr('readonly','readonly');
                $('span#sem_daily_budget').remove();
            } else {
                 budget_field.val('');
                 budget_field.attr('readonly','readonly');
                 $('span#sem_daily_budget').remove();
            }
        }

    });

    // Dropdown selection SEM media channels
    $(document).on('change','select#media_channel', function(){
        var selected = this.value,
            curr_channel = $('input[id="media_channel_array"]');

        if (selected !== '')
            $('input[id="mediachannel_null"]').remove();
        else
            $(this).next().append('<input type="hidden" id="mediachannel_null" name="media_channel" />');
        
        if (selected !== curr_channel.val())
            curr_channel.attr('disabled','disabled')
        else   
            curr_channel.removeAttr('disabled');
    });

    // Input total budget change into parse float (SEM)
    $(document).on('change','input[id="budget"]', function(){
        var budget = this.value == '' ? 0 : this.value;
        var number = accounting.formatMoney(budget.length,'') > 13 ? accounting.formatMoney(budget.slice(0,13),'') : accounting.formatMoney(budget, '');
        $(this).val(number);
    });

    // Input monthly budget change into parse float (SEM)
    $(document).on('change','input[id="google_monthly_budget"]', function(){
        
        var monthly_budget = this.value == '' ? 0 : this.value,
            daily_budget = monthly_budget / 30,
            ad_sched = $('input[id="ad_scheduling"]:checked').val();

            var budget = this.value == '' ? 0 : this.value;
            var number = accounting.formatMoney(budget.length,'') > 13 ? accounting.formatMoney(budget.slice(0,13),'') : accounting.formatMoney(budget, '');
            $(this).val(number);

        if (ad_sched == 0)
            $('input[id="google_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

    // Input daily budget change into parse float (SEM)
      $(document).on('change','input[id="google_daily_budget"]', function(){
        var budget = this.value == '' ? 0 : this.value;
        var number = accounting.formatMoney(budget.length,'') > 13 ? accounting.formatMoney(budget.slice(0,13),'') : accounting.formatMoney(budget, '');
        $(this).val(number);
    });

    // function to calculate daily budget
    function calculateSEMDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="google_monthly_budget"]').val(),
            duration_other_picker = $('select#duration_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }

    // Dropdown selection duration
    $(document).on('change', 'select#duration', function(){
        var duration = this.value,
            duration_other_container = $('div#duration-other'),
            duration_other = $('input[id="duration_other"]'),
            daily_budget = $('input[id="google_daily_budget"]'),
            ad_sched = $('input[id="ad_scheduling"]:checked').val();
    
        // If select 'Others' on dropdown selection
       if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateSEMDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateSEMDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        if ($('input[id="campaign_datefrom"]').val() !== '') {
            var txt_duration = $('select#duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
                enddate = $('input[id="campaign_dateto"]');

            months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
            console.log(months);
            if (txt_duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });

    // Dropdown selection of days,weeks,months on Duration other
    $(document).on('change', 'select#duration_other_picker', function () {

        var interval = this.value,
            duration = $('input[id="duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
            ad_sched = $('input[id="ad_scheduling"]:checked').val(),
            campaign_date_set = $('input[id="campaign_date_set"]:checked').val(),
            enddate = $('input[id="campaign_dateto"]');
    
            if(ad_sched == 0)
                $('input[id="google_daily_budget"]').val(calculateSEMDailyBudget(duration, true));
    
            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if(campaign_date_set === 1)
                if (duration !== '')
                    $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });

    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="duration_other"]').on('change', function () {
        var other = this.value,
            interval = $('select#duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="campaign_datefrom"]').val())),
            daily_budget = $('input[id="google_daily_budget"]'),
            campaign_date_set = $('input[id="campaign_date_set"]:checked').val(),
            ad_sched = $('input[id="ad_scheduling"]:checked').val();

        if (ad_sched == 0) {
            daily_result = calculateSEMDailyBudget(this.value, true);
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
    
        if(campaign_date_set === 1)
            if (other !== '')
                $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });

    // Campaign date from - date to
    $(document).on('change', 'input[id="campaign_datefrom"]', function () {

        var camp_durr = $('select#duration option:selected').val(),
            duration = $('select#duration option:selected').text(),
            other = $('input[id="duration_other"]'),
            startdate = new Date(Date.parse($(this).val())),
            interval = $('select#duration_other_picker option:selected').val(),
            enddate = $('input[id="campaign_dateto"]');
    
        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
    
        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));
    
        if (startdate !== '')
            $('input[id="campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
        $(this).datepicker('hide');
    });

     // AM Fee change into Parse Float
     $(document).on('change','input[id="am_fee"]', function(){
        var am_fee = this.value == '' ? 0 : this.value;
        var number = accounting.formatMoney(am_fee.length,'') > 13 ? accounting.formatMoney(am_fee.slice(0,13),'') : accounting.formatMoney(am_fee, '');
        $(this).val(number);
    });

    $('select#currency').on({
        'change': function (e) {
            var currency = $(this).val();
            $('span#campaign-currency').text(currency);
        }
    });
    
    $(document).ready(function () {
        var currency = $('select#currency option:selected').val();
        $('span#campaign-currency').text(currency);
    });

    $('input[id="campaign_date_set"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.sem-campaign').removeClass('hidden');
            else
                $('div.sem-campaign').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.sem-campaign').removeClass('hidden');
            else
                $('div.sem-campaign').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="campaign_datefrom"]').val('');
            $('input[id="campaign_dateto"]').val('');
        },
    });

// End of SEM

//-------------------FB--------------------//

    $(document).on('change', 'select#fb_target_country', function () {
        var selected = $(this).val().join('|');

        $('input[id="fb_targetmarket_array"]').val(selected);

    });

    // Dropdown selection FB media channels
    $(document).on('change','select#fb_media_channel', function(){
        var selected = this.value,
            curr_channel = $('input[id="fb_media_channel_array"]');

        if (selected !== '')
            $('input[id="fb_mediachannel_null"]').remove();
        else
            $(this).next().append('<input type="hidden" id="fb_mediachannel_null" name="media_channel" />');

        if (selected !== curr_channel.val())
            curr_channel.attr('disabled','disabled')
        else   
            curr_channel.removeAttr('disabled');
    });
    
    
    // Input total budget change into parse float (FB)
    $(document).on('change','input[id="total_budget"]', function(){
        var budget = this.value == '' ? 0 : this.value;
        var number = accounting.formatMoney(budget.length,'') > 13 ? accounting.formatMoney(budget.slice(0,13),'') : accounting.formatMoney(budget, '');
        $(this).val(number);
    });

    // Input monthly budget change into parse float and calculated in 30 days
    $(document).on('change', 'input[id="monthly_budget"]', function () {
        var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
            number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

        $(this).val(number);
        $('input[id="daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

     // function to calculate daily budget
     function calculateFBDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="monthly_budget"]').val(),
            duration_other_picker = $('select#fb_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }
    
    // Dropdown selection duration
    $(document).on('change', 'select#fb_duration', function(){
        var duration = this.value,
            duration_other = $('input[id="fb_duration_other"]'),
            duration_other_container = $('div#fb-duration-other'),
            daily_budget = $('input[id="daily_budget"]');

        // If select 'Others' on dropdown selection
        if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateFBDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateFBDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        daily_budget.val(number);
        
         // Campaign date from and date to
        if ($('input[id="fb_campaign_dt_from"]').val() !== '') {
            var txt_duration = $('select#fb_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
                enddate = $('input[id="fb_campaign_dt_to"]');

            months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));

            if (txt_duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }
    });

    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="fb_duration_other"]').on('change', function () {

        var other = this.value,
            interval = $('select#fb_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
            daily_budget = $('input[id="daily_budget"]'),
            campaign_date_set = $('input[id="fb_campaign_date_set"]:checked').val(),
            enddate = $('input[id="fb_campaign_dt_to"]');

        daily_result = calculateFBDailyBudget(this.value, true);
        var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        daily_budget.val(number);
    
        if (interval === 'CD991') 
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
    
        if(campaign_date_set === 1)
            if (other !== '')
                enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });
    
    // Dropdown selection of days,weeks,months on Duration other
    $(document).on('change', 'select#fb_other_picker', function () {
    
        var interval = this.value,
            duration = $('input[id="fb_duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="fb_campaign_dt_from"]').val())),
            campaign_date_set = $('input[id="fb_campaign_date_set"]:checked').val(),
            enddate = $('input[id="fb_campaign_dt_to"]');

            $('input[id="daily_budget"]').val(calculateFBDailyBudget(duration, true));
    
            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if(campaign_date_set === 1)
                if (duration !== '')
                    enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                    //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
                    // console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });

    // Campaign date from - date to
    $(document).on('change', 'input[id="fb_campaign_dt_from"]', function () {
    
        var camp_durr = $('select#fb_duration option:selected').val(),
            duration = $('select#fb_duration option:selected').text(),
            other = $('input[id="fb_duration_other"]'),
            interval = $('select#fb_other_picker option:selected').val(),
            startdate = new Date(Date.parse($(this).val())),
            enddate = $('input[id="fb_campaign_dt_to"]');
    
        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
        
        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));
    
        if (startdate !== '')
            $('input[id="fb_campaign_dt_to"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1)),
            //$('input[id="fb_campaign_dt_to"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
        $(this).datepicker('hide');
    });

    $(document).on('change', 'select#age_from', function() {
        var age_from = this.value,
            age_to = $('select#age_to'),
            age = age_from !== '65' ? parseInt(age_from) + 1 : parseInt(age_from);
        age_to.val(age).change();
    });

    $('input[id="fb_campaign_date_set"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.fb-campaign').removeClass('hidden');
            else
                $('div.fb-campaign').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.fb-campaign').removeClass('hidden');
            else
                $('div.fb-campaign').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="fb_campaign_dt_from"]').val('');
            $('input[id="fb_campaign_dt_to"]').val('');
        },
    });

// End of FB

//---------------SEO---------------//

    $(document).on('change', 'select#seo_target_countries', function () {
        var selected = $(this).val().join('|');

        $('input[id="seo_targetmarket_array"]').val(selected);

    });

    $(document).on('change', 'select#seo_duration', function () {
        var seo_duration = this.value,
            divOther = $('div#seo-duration-other'),
            other = $('input[id="seo_duration_other"]');
    
        if (seo_duration === 'CD999')
            divOther.removeClass('hidden');
        else
            divOther.addClass('hidden'),
            other.val('');
    });

    $(document).on('click', 'input[id="has_keyword_maintenance"]', function(){
        var has_keyword_maintenance = this.value,
            keyword_maintenance = $('#keyword_maintenance');
        
        keyword_maintenance.val('');

        if (has_keyword_maintenance == 1)
            keyword_maintenance.removeAttr('disabled'),
            keyword_maintenance.removeClass('hidden');
        else
            keyword_maintenance.attr('disabled'),
            keyword_maintenance.addClass('hidden');
    });

    $(document).ready(function(){
        var has_keyword_maintenance = $('input[id="has_keyword_maintenance"]:checked').val(),
            keyword_maintenance = $('#keyword_maintenance'),
            keyword = $('select#keyword_themes_select option:selected').val(),
            other = $('input[id="keyword_themes"]');

        if (has_keyword_maintenance !== '0')
            keyword_maintenance.removeAttr('disabled'),
            keyword_maintenance.removeClass('hidden');
        else
            keyword_maintenance.attr('disabled','disabled'),
            keyword_maintenance.addClass('hidden');

        if (keyword === 'NK999')
            other.removeClass('hidden'),
            other.addClass('in-other');
        else
            other.addClass('hidden'),
            other.removeClass('in-other');
    });

    $(document).on('change', 'select#keyword_themes_select', function(){
        var keyword = this.value,
            other = $('input[id="keyword_themes"]');

        if (keyword === 'NK999')
            other.removeClass('hidden'),
            other.addClass('in-other');
        else
            other.val(''),
            other.addClass('hidden'),
            other.removeClass('in-other');
    });

    $(document).on('change', 'input[id="seo_total_fee"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

// End of SEO

//-------------LEAD NOTE------------------//
    $(document).on('click', 'a#edit-note', function() {
        var noteID = $(this).data('editnote'),
            note = $('span#lead-note-'+noteID),
            currNote = note.text(),
            thisNote = $('<textarea id="lead-note-'+noteID+'" class="form-control" rows="10" />'),
            editBtn = $('a[data-editnote="'+noteID+'"]'),
            delBtn = $('a[data-delnote="'+noteID+'"]');

        note.parent().css('background','#fff');
        thisNote.html((note.html()).replace(/<br\s*[\/]?>/gi, '\n'));
        editBtn.after('<a id="save-note" data-savenote="'+noteID+'" class="btn btn-success"><i class="fa fa-save"></i> Save</a>');
        editBtn.remove();
        delBtn.after('<a id="cancel-note" data-cancelnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-close"></i> Cancel</a>');
        delBtn.remove();
        note.replaceWith(thisNote);
        thisNote.focus();
    });
    
    $(document).on('click','a#cancel-note', function() {
        var noteID = $(this).data('cancelnote'),
            note = $('textarea#lead-note-'+noteID),
            currNote = note.text(),
            thisNote = $('<span id="lead-note-'+noteID+'"></span>'),
            saveBtn = $('a[data-savenote="'+noteID+'"]'),
            cancelBtn = $('a[data-cancelnote="'+noteID+'"]');

        note.parent().removeAttr('style');
        thisNote.html((note.text()).replace(/(?:\r\n|\r|\n)/g, '<br />'));
        saveBtn.after('<a id="edit-note" data-editnote="'+noteID+'" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>');
        saveBtn.remove();
        cancelBtn.after('<a id="delete-note" data-delnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>');
        cancelBtn.remove();
        note.replaceWith(thisNote);
    });

    $(document).on('click','a#save-note', function() {
        var noteID = $(this).data('savenote'),
            note = $('textarea#lead-note-'+noteID),
            currNote = note.val(),
            thisNote = $('<span id="lead-note-'+noteID+'"></span>');
            saveBtn = $('a[data-savenote="'+noteID+'"]'),
            cancelBtn = $('a[data-cancelnote="'+noteID+'"]'),
            accountID = $('input[id="accountid"]').val(),
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
            data = {_token:CSRF_TOKEN,id:noteID, note:currNote, acc_id: accountID};

            $.ajax({
                type: 'patch',
                url: $('base').attr('href')+'/lead/note/update',
                data: data,
                success:function(data){
                    thisNote.html((data).replace(/(?:\r\n|\r|\n)/g,'<br />'));
                },
                error: function(error) {
                    console.log(error.responseText);
                }
            });

            note.parent().removeAttr('style');
            saveBtn.after('<a id="edit-note" data-editnote="'+noteID+'" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>');
            saveBtn.remove();
            cancelBtn.after('<a id="delete-note" data-delnote="'+noteID+'" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>');
            cancelBtn.remove();
            note.after(thisNote);
            note.remove();

    });

    $(document).on('click','a#delete-note', function() {
        var noteID = $(this).data('delnote'),
            note = $('span#lead-note-'+noteID);
            $('input[id="delete-leadnote"]').val(noteID);
            $('#deleteNoteModal').modal('show');

    });

    $(document).on('click','a#delete-leadnote', function() {
        var noteID = $('input[id="delete-leadnote"]').val(),
            note = $('span#lead-note-'+noteID),
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
            accountID = $('input[id="accountid"]').val(),
            data = {_token: CSRF_TOKEN, note_id: noteID, account_id: accountID};
            
            $.ajax({
                url: $('base').attr('href')+'/lead/note/delete',
                type: 'patch',
                data: data,
                success:function(data){
                }
            });

            note.parent().parent().next().remove();
            note.parent().parent().remove();
            $('#deleteNoteModal').modal('hide');
    });
// End of Leadnote

//----------------BAIDU-------------//
    $(document).on('change', 'select#baidu_media_channel', function () {

        var baidu_media_channel = $(this).val();
    
        $('input[id="baidu_media_channel_array"]').val(baidu_media_channel);
    });

    $('input[id="has_baidu_account"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1) {
                $('div.baidu-account').removeClass('hidden');
                $('input[id="baidu_account"]').focus();
            } else {
                $('div.baidu-account').addClass('hidden');
            }
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1) {
                $('div.baidu-account').removeClass('hidden');
                $('input[id="baidu_account"]').focus();
            } else {
                $('div.baidu-account').addClass('hidden');
            }

             // Remove the value after setting radio to "No"
            $('input[id="baidu_account"]').val('');
        },
    });

    $(document).on('change', 'input[id="baidu_campaign_datefrom"]', function () {

        var camp_durr = $('select#baidu_duration option:selected').val(),
            duration = $('select#baidu_duration option:selected').text(),
            other = $('input[id="baidu_duration_other"]'),
            startdate = new Date(Date.parse($(this).val())),
            interval = $('select#baidu_duration_other_picker option:selected').val(),
            enddate = $('input[id="baidu_campaign_dateto"]');
    
        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
    
        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));
    
        if (startdate !== '')
            $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
        $(this).datepicker('hide');
    });
    
    $(document).on('click','input[id="has_baidu_ad_scheduling"]', function(){

        var ad_scheduling = $(this).val(),
            budget_field = $('input[id="baidu_daily_budget"]'),
            monthly_field = $('input[id="baidu_monthly_budget"]'),
            duration_other = $('input[id="baidu_duration_other"]').val(),
            ad_sched_remark = $('#baidu_ad_scheduling_remark');

        if(ad_scheduling === '1'){
            budget_field.val(''),
            budget_field.removeAttr('readonly'),
            ad_sched_remark.removeClass('hidden');
                if ($('span#baidu_daily_budget').length <= 0){
                    budget_field.after('<span class="error-p account" id="baidu_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
                }
                else{
                    return false;
                }

            $('textarea[id="baidu_ad_scheduling_remark"]').focus();
        }   
        else{
            ad_sched_remark.addClass('hidden');
            ad_sched_remark.val('');
            if (monthly_field !== '') {
                daily_result = calculateBaiduDailyBudget(duration_other, true);
                number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
                budget_field.val(number);
                budget_field.attr('readonly','readonly'),
                $('span#baidu_daily_budget').remove();
            } else {
                 budget_field.val(''),
                 budget_field.attr('readonly','readonly'),
                 $('span#baidu_daily_budget').remove();
            }
        }

    });

    

    // function to calculate daily budget
    function calculateBaiduDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="baidu_monthly_budget"]').val(),
            duration_other_picker = $('select#baidu_duration_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }

    $(document).on('change', 'input[id="baidu_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).on('change', 'input[id="baidu_daily_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });


    // Input monthly budget change into parse float and calculated in 30 days
    $(document).on('change', 'input[id="baidu_monthly_budget"]', function () {
        var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
            ad_sched = $('input[id="has_baidu_ad_scheduling"]:checked').val();
            var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

            $(this).val(number);

        if (ad_sched == 0)
            $('input[id="baidu_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

     // Dropdown selection duration
    $(document).on('change', 'select#baidu_duration', function(){
        var duration = this.value,
            duration_other = $('input[id="baidu_duration_other"]'),
            duration_other_container = $('div#baidu-duration-other'),
            daily_budget = $('input[id="baidu_daily_budget"]'),
            ad_sched = $('input[id="has_baidu_ad_scheduling"]:checked').val();

       // If select 'Others' on dropdown selection
       if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateBaiduDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateBaiduDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        // Campaign date from and date to
        if ($('input[id="baidu_campaign_datefrom"]').val() !== '') {
            var txt_duration = $('select#baidu_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
                duration_other = $('input[id="baidu_duration_other"]'),
                enddate = $('input[id="baidu_campaign_dateto"]');

            months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
            console.log(months);
            if (txt_duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });

    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="baidu_duration_other"]').on('change', function () {

        var other = this.value,
            interval = $('select#baidu_duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
            daily_budget = $('input[id="baidu_daily_budget"]'),
            campaign_date_set = $('input[id="has_baidu_campaign_date_set"]:checked').val(),
            ad_sched = $('input[id="has_baidu_ad_scheduling"]:checked').val();

        if (ad_sched == 0) {
            daily_result = calculateBaiduDailyBudget(this.value, true);
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
    }
        
        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
        
        if(campaign_date_set === 1)
            if (other !== '')
                $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });
    
    // Dropdown selection of days,weeks,months on Duration other
    $(document).on('change', 'select#baidu_duration_other_picker', function () {
        var interval = this.value,
            duration = $('input[id="baidu_duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="baidu_campaign_datefrom"]').val())),
            ad_sched = $('input[id="has_baidu_ad_scheduling"]:checked').val(),
            campaign_date_set = $('input[id="has_baidu_campaign_date_set"]:checked').val(),
            enddate = $('input[id="baidu_campaign_dateto"]');

        if(ad_sched == 0)
            $('input[id="baidu_daily_budget"]').val(calculateBaiduDailyBudget(duration, true));
    
            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if(campaign_date_set === 1)
                if (duration !== '')
                    $('input[id="baidu_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                    //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        
                console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });

    $('input[id="has_baidu_campaign_dateset"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.baidu-campaign').removeClass('hidden');
            else
                $('div.baidu-campaign').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.baidu-campaign').removeClass('hidden');
            else
                $('div.baidu-campaign').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="baidu_campaign_datefrom"]').val('');
            $('input[id="baidu_campaign_dateto"]').val('');
        },
    });
// End of Baidu

//----------------WEIBO----------------//
    $('input[id="has_weibo_account"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.weibo-account').removeClass('hidden'),
                $('input[id="weibo_account"]').focus();
            else
                $('div.weibo-account').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.weibo-account').removeClass('hidden'),
                $('input[id="weibo_account"]').focus();
            else
                $('div.weibo-account').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="weibo_account"]').val('');
        },
    });

    $(document).on('change', 'input[id="weibo_campaign_datefrom"]', function () {

        var camp_durr = $('select#weibo_duration option:selected').val(),
            duration = $('select#weibo_duration option:selected').text(),
            other = $('input[id="weibo_duration_other"]'),
            startdate = new Date(Date.parse($(this).val())),
            interval = $('select#weibo_duration_other_picker option:selected').val(),
            enddate = $('input[id="weibo_campaign_dateto"]');
    
        months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
    
        if (camp_durr == 'CD999')
            if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));
    
        if (startdate !== '')
            $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
        $(this).datepicker('hide');
    });
    
    $(document).on('click','input[id="has_weibo_ad_scheduling"]', function(){

        var ad_scheduling = $(this).val(),
            budget_field = $('input[id="weibo_daily_budget"]'),
            monthly_field = $('input[id="weibo_monthly_budget"]'),
            duration_other = $('input[id="weibo_duration_other"]').val(),
            ad_sched_remark = $('#weibo_ad_scheduling_remark');

        if(ad_scheduling === '1'){
            budget_field.val(''),
            budget_field.removeAttr('readonly'),
            ad_sched_remark.removeClass('hidden');
                if ($('span#weibo_daily_budget').length <= 0){
                    budget_field.after('<span class="error-p account" id="weibo_daily_budget">Please recalculate daily budget because of ad scheduling</span>');
                }
                else{
                    return false;
                }
            
            $('textarea[id="weibo_ad_scheduling_remark"]').focus();
        }   
        else{
            ad_sched_remark.addClass('hidden');
            ad_sched_remark.val('');
            if (monthly_field !== '') {
                daily_result = calculateWeiboDailyBudget(duration_other, true);
                number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
                budget_field.val(number);
                budget_field.attr('readonly','readonly');
                $('span#weibo_daily_budget').remove();
            } else {
                 budget_field.val(''),
                 budget_field.attr('readonly','readonly'),
                 $('span#weibo_daily_budget').remove();
            }
        }

    });

    $(document).on('change', 'input[id="weibo_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $(document).on('change', 'input[id="weibo_daily_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });


    // Input monthly budget change into parse float and calculated in 30 days
    $(document).on('change', 'input[id="weibo_monthly_budget"]', function () {
        var daily_budget = parseFloat((this.value).replace(/[^0-9.]/g,  1) / 30).toFixed(2),
            ad_sched = $('input[id="has_weibo_ad_scheduling"]:checked').val();
            var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');

            $(this).val(number);

        if (ad_sched == 0)
            $('input[id="weibo_daily_budget"]').val(accounting.formatMoney(daily_budget.length,'') > 13 ? accounting.formatMoney(daily_budget.slice(0,13),'') : accounting.formatMoney(daily_budget, ''));
    });

    // function to calculate daily budget
    function calculateWeiboDailyBudget(duration, is_other = false) {
        var monthly_budget = $('input[id="weibo_monthly_budget"]').val(),
            duration_other_picker = $('select#weibo_duration_other_picker').val();

        if(is_other == true) {
            if(duration_other_picker === 'CD992')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / (duration * 7)).toFixed(2);
            else if(duration_other_picker === 'CD993')
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
            else
                daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / duration).toFixed(2);
        } else {
            daily_result = parseFloat((monthly_budget).replace(/[^0-9.]/g, '') / 30).toFixed(2);
        }
            
        var pretty_daily_result = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
        return pretty_daily_result;
    }

    // Dropdown selection duration
    $(document).on('change', 'select#weibo_duration', function(){
        var duration = this.value,
            duration_other = $('input[id="weibo_duration_other"]'),
            duration_other_container = $('div#weibo-duration-other'),
            daily_budget = $('input[id="weibo_daily_budget"]'),
            ad_sched = $('input[id="has_weibo_ad_scheduling"]:checked').val();

       // If select 'Others' on dropdown selection
       if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');

            // Calculate the budget in manual input of days, weeks and months
            daily_result = calculateWeiboDailyBudget(duration_other, true);
        } else {
            // Setting of variable daily result and calculates it into 30 days
            daily_result = calculateWeiboDailyBudget(duration_other, false);
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

        // If Ad Scheduling is set to 'No'
        if (ad_sched == 0) {
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }

        // Campaign date from and date to
        if ($('input[id="weibo_campaign_datefrom"]').val() !== '') {
            var duration = $('select#weibo_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
                enddate = $('input[id="weibo_campaign_dateto"]');

            months = duration === 'CD999' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
            console.log(months);
            if (duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });
    
    // Input other duration change into parse float and automatic change of campaign datefrom and dateto
    $('input[id="weibo_duration_other"]').on('change', function () {
        var other = this.value,
            interval = $('select#weibo_duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
            daily_budget = $('input[id="weibo_daily_budget"]'),
            campaign_date_set = $('input[id="has_weibo_campaign_date_set"]:checked').val(),
            ad_sched = $('input[id="has_weibo_ad_scheduling"]:checked').val();

        if (ad_sched == 0) {
            daily_result = calculateWeiboDailyBudget(this.value, true);
            var number = accounting.formatMoney(daily_result.length,'') > 13 ? accounting.formatMoney(daily_result.slice(0,13),'') : accounting.formatMoney(daily_result, '');
            daily_budget.val(number);
        }
        
        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
            
        if(campaign_date_set === 1)
            if (other !== '')
                $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });
    
    // Dropdown selection of days,weeks,months on Duration other
    $(document).on('change', 'select#weibo_duration_other_picker', function () {
    
        var interval = this.value,
            duration = $('input[id="weibo_duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="weibo_campaign_datefrom"]').val())),
            ad_sched = $('input[id="has_baidu_ad_scheduling"]:checked').val(),
            campaign_date_set = $('input[id="has_weibo_campaign_date_set"]:checked').val(),
            enddate = $('input[id="weibo_campaign_dateto"]');
    
            if(ad_sched == 0)
                $('input[id="weibo_daily_budget"]').val(calculateWeiboDailyBudget(duration, true));

            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if(campaign_date_set === 1)
                if (duration !== '')
                    $('input[id="weibo_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                    //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        
                console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });

    $('input[id="has_weibo_campaign_dateset"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.weibo-campaign').removeClass('hidden');
            else
                $('div.weibo-campaign').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.weibo-campaign').removeClass('hidden');
            else
                $('div.weibo-campaign').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="weibo_campaign_datefrom"]').val('');
            $('input[id="weibo_campaign_dateto"]').val('');
        },
    });
// End of Weibo

//--------------WeChat-------------//
    $(document).on('change', 'select#wechat_advertising_type', function () {

        var wechat_advertising_type = $(this).val();

        $('input[id="wechat_advertising_type_array"]').val(wechat_advertising_type);
    });
    
    $('input[id="has_wechat_type"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.wechat-type').removeClass('hidden');
            else
                $('div.wechat-type').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.wechat-type').removeClass('hidden');
            else
                $('div.wechat-type').addClass('hidden');
        },
    });

    $('input[id="has_wechat_advertising"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1)
                $('div.wechat-advertising').removeClass('hidden');
            else
                $('div.wechat-advertising').addClass('hidden');
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1)
                $('div.wechat-advertising').removeClass('hidden');
            else
                $('div.wechat-advertising').addClass('hidden');


             // Remove the value after setting radio to "No"
            $('input[id="wechat_location"]').val('');
            $('input[id="wechat_marital"]').val('');
            $('input[id="wechat_education"]').val('');
        },
    });

    $(document).on('change', 'select#wechat_age_from', function() {
        var age_from = this.value,
            age_to = $('select#wechat_age_to'),
            age = age_from !== '65' ? parseInt(age_from) + 1 : parseInt(age_from);
        age_to.val(age).change();
    });
// End of WeChat

//------------Social Media Management--------------------//
    $(document).on('change', 'input[id="social_media_budget"]', function () {
        var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
        $(this).val(number);
    });

    $('input[id="has_social_media_budget"]').on({
        'ready' : function() {
            var dateSet = this.value;

            if (dateSet == 1) {
                $('div.social-media-budget').removeClass('hidden');
                $('input[id="social_media_budget"]').focus();
            } else {
                $('div.social-media-budget').addClass('hidden');
            }
        },
        'click' : function() {
             var dateSet = this.value;

            if (dateSet == 1) {
                $('div.social-media-budget').removeClass('hidden');
                $('input[id="social_media_budget"]').focus();
            } else {
                $('div.social-media-budget').addClass('hidden');
            }

             // Remove the value after setting radio to "No"
            $('input[id="social_media_budget"]').val('');
        },
    });

    
    $(document).on('change', 'select#social_duration', function(){
        var duration = this.value,
        divOther = $('div#social-duration-other'),
        other = $('input[id="social_duration_other"]');

        if (duration === 'CD999')
            divOther.removeClass('hidden');
        else
            divOther.addClass('hidden'),
            other.val('');


        if ($('input[id="social_campaign_datefrom"]').val() !== '') {
            var duration = $('select#social_duration option:selected').text(),
                startdate = new Date(Date.parse($('input[id="social_campaign_datefrom"]').val())),
                enddate = $('input[id="social_campaign_dateto"]');

            months = duration === 'CD999' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));
            console.log(months);
            if (duration !== 'Others')
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
                $('input[id="social_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        }

    });
    
    $('input[id="social_duration_other"]').on('change', function () {

        var other = this.value,
            interval = $('select#social_duration_other_picker option:selected').val(),
            startdate = new Date(Date.parse($('input[id="social_campaign_datefrom"]').val())),
            enddate = $('input[id="social_campaign_dateto"]');
        
        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
        
        if (other !== '')
            $('input[id="social_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    });
    
    $(document).on('change', 'select#social_duration_other_picker', function () {
    
        var interval = this.value,
            duration = $('input[id="social_duration_other"]').val(),
            startdate = new Date(Date.parse($('input[id="social_campaign_datefrom"]').val())),
            enddate = $('input[id="social_campaign_dateto"]');
    
            if (interval === 'CD991')
                startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
            else if (interval === 'CD992')
                startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
            else
                startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
            
            if (duration !== '')
                $('input[id="social_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
                //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
            console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    
    });
// End of Social Media Management

// Additional Option: Post Paid SEM Service Package

// Input other duration automatic change of campaign datefrom and dateto
$('input[id="postpaid_duration_other"]').on('change', function () {
    var other = this.value,
        interval = $('select#postpaid_duration_other_picker option:selected').val(),
        startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
        campaign_date_set = $('input[id="postpaid_campaign_date_set"]:checked').val();

    if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other));
    else if (interval === 'CD992')
        startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other) * 7));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other));
    
    if(campaign_date_set === 1)
        if (other !== '')
            $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

// Dropdown selection of days,weeks,months on Duration other
$(document).on('change', 'select#postpaid_duration_other_picker', function () {
    var interval = this.value,
        duration = $('input[id="postpaid_duration_other"]').val(),
        startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
        campaign_date_set = $('input[id="postpaid_campaign_date_set"]:checked').val(),
        enddate = $('input[id="postpaid_campaign_dateto"]');

        if (interval === 'CD991')
            startdate.setDate(parseInt(startdate.getDate()) + parseInt(duration));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(duration) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(duration));
        
        if(campaign_date_set === 1)
            if (duration !== '')
                $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

            console.log(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
});

$('input[id="postpaid_campaign_date_set"]').on({
    'ready' : function(){

        var dateSet = this.value;
        
        if(dateSet === '1')
            $('div.postpaid-campaign').removeClass('hidden');
        else
            $('div.postpaid-campaign').addClass('hidden');
    },
    'click' : function(){
        var dateSet = this.value;

        if(dateSet === '1')
            $('div.postpaid-campaign').removeClass('hidden');
        else
            $('div.postpaid-campaign').addClass('hidden');

            // Remove the value after setting radio to "No"
            $('input[id="postpaid_campaign_datefrom"]').val('');
            $('input[id="postpaid_campaign_dateto"]').val('');
    }
});

$(document).on('change', 'input[id="postpaid_campaign_datefrom"]', function () {

    var camp_durr = $('select#postpaid_duration option:selected').val(),
        duration = $('select#postpaid_duration option:selected').text(),
        other = $('input[id="postpaid_duration_other"]'),
        startdate = new Date(Date.parse($(this).val())),
        interval = $('select#postpaid_duration_other_picker option:selected').val(),
        enddate = $('input[id="postpaid_campaign_dateto"]');

    months = duration === 'Others' ? other.val() : (duration === '1 Month' ? duration.replace(' Month', '') : duration.replace(' Months', ''));

    if (camp_durr == 'CD999')
        if (interval === 'CD991')
        startdate.setDate(parseInt(startdate.getDate()) + parseInt(other.val()));
        else if (interval === 'CD992')
            startdate.setDate(parseInt(startdate.getDate()) + (parseInt(other.val()) * 7));
        else
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(other.val()));
    else
        startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months));

    if (startdate !== '')
        $('input[id="postpaid_campaign_dateto"]').datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
        //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));

    $(this).datepicker('hide');
});

// Dropdown selection duration
$(document).on('change', 'select#postpaid_duration', function () {
    // Setting of variables for selection of duration
    var duration = $('select#postpaid_duration').val(),
        duration_other = $('input[id="postpaid_duration_other"]'),
        duration_other_container = $('div#postpaid-duration-other');
        
        // If select 'Others' on dropdown selection
        if(duration === 'CD999') {
            duration_other_container.removeClass('hidden');
        } else {
            duration_other_container.addClass('hidden');
            duration_other.val('');
        }

    // Campaign date from and date to
    if ($('input[id="postpaid_campaign_datefrom"]').val() !== '') {
        var txt_duration = $('select#postpaid_duration option:selected').text(),
            startdate = new Date(Date.parse($('input[id="postpaid_campaign_datefrom"]').val())),
            enddate = $('input[id="postpaid_campaign_dateto"]');

        months = txt_duration === 'CD999' ? duration_other.val() : (txt_duration === '1 Month' ? txt_duration.replace(' Month', '') : txt_duration.replace(' Months', ''));
        if (txt_duration !== 'Others')
            startdate.setMonth(parseInt(startdate.getMonth()) + parseInt(months)),
            enddate.datepicker('setDate', startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
            //$('input[id="campaign_dateto"]').data('datepicker').setStartDate(startdate.getFullYear() + '-' + (startdate.getMonth() + 1) + '-' + (startdate.getDate() - 1));
    }

});

// AM Fee and Media Budget Input Event
$(document).on('ready change', 'input[id="postpaid_media_budget"]', function() {

    var media_budget = $(this),
        am_fee = $('input[id="postpaid_am_fee"]'),
        am_fee_amount = $('input[id="postpaid_am_fee_amount"]');

    if(media_budget.val().length == '') {
        $('span[id="media_budget_err"]').text('Please input Media Budget');
        $('div[id="input-group-media-budget"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="media_budget_err"]').text('');
        $('div[id="input-group-media-budget"]').css('border', 'none');
    }

    if(am_fee.val().length == '')
    {
        $('span[id="am_fee_err"]').text('Please input AM Fee %');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="am_fee_err"]').text('');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid #bbbbbb');
    }

    var total = (media_budget.val() * 1) * parseFloat((am_fee.val() / 100)).toFixed(2);
    
    am_fee_amount.val(parseFloat(total).toFixed(2));


    // Radio button selection
    var isPaidThroughOOm = $('input[id="is_media_budget_paid"]:checked').val(),
        
        no = am_fee_amount.val(),
        yes = (am_fee_amount.val() * 1)  + (media_budget.val() * 1);

    if(isPaidThroughOOm == 1)
        $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
    else
        $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
});

$(document).on('ready change', 'input[id="postpaid_am_fee"]', function() {

    var media_budget = $('input[id="postpaid_media_budget"]'),
        am_fee = $(this),
        am_fee_amount = $('input[id="postpaid_am_fee_amount"]');

    if(media_budget.val().length == '') {
        $('span[id="media_budget_err"]').text('Please input Media Budget');
        $('div[id="input-group-media-budget"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="media_budget_err"]').text('');
        $('div[id="input-group-media-budget"]').css('border', 'none');
    }

    if(am_fee.val().length == '')
    {
        $('span[id="am_fee_err"]').text('Please input AM Fee %');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid red');
        return false;
    } else {
        $('span[id="am_fee_err"]').text('');
        $('input[id="postpaid_am_fee"]').css('border', '1px solid #bbbbbb');
    }

    var total = (media_budget.val() * 1) * parseFloat((am_fee.val() / 100)).toFixed(2);
    
    am_fee_amount.val(parseFloat(total).toFixed(2));

     // Radio button selection
     var isPaidThroughOOm = $('input[id="is_media_budget_paid"]:checked').val(),
     no = am_fee_amount.val(),
     yes = (am_fee_amount.val() * 1)  + (media_budget.val() * 1);

    if(isPaidThroughOOm == 1)
        $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
    else
        $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));

});

//Is Media Budget paid through OOm? Radio Button
$('input[id="is_media_budget_paid"]').on({
    'ready' : function() {
        var isPaidThroughOOm = this.value,
            mediaBudget = $('input[id="postpaid_media_budget"]').val(),
            amFeeAmount = $('input[id="postpaid_am_fee_amount"]').val(),
            yes = (mediaBudget * 1) + (amFeeAmount * 1),
            no = amFeeAmount * 1;

            if(isPaidThroughOOm == 1)
                $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
            else
                $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
    },      

    'click' : function() {
        var isPaidThroughOOm = this.value,
            mediaBudget = $('input[id="postpaid_media_budget"]').val(),
            amFeeAmount = $('input[id="postpaid_am_fee_amount"]').val(),
            yes = (mediaBudget * 1) + (amFeeAmount * 1),
            no = amFeeAmount * 1;

        if(isPaidThroughOOm == 1)
            $('input[id="postpaid_total"]').val(accounting.formatMoney(yes.length,'') > 13 ? accounting.formatMoney(yes.slice(0,13) * 1,'') : accounting.formatMoney(yes * 1, ''));
        else
            $('input[id="postpaid_total"]').val(accounting.formatMoney(no.length,'') > 13 ? accounting.formatMoney(no.slice(0,13),'') : accounting.formatMoney(no, ''));
    }
});
// End of Post Paid SEM

// Contract Value
$(document).on('change', 'input[id="contract_value"]', function () {
    var number = accounting.formatMoney($(this).val().length,'') > 13 ? accounting.formatMoney($(this).val().slice(0,13),'') : accounting.formatMoney($(this).val(), '');
    
    // if(this.value > 0)
    $(this).val(number);
});

$(document).on('click', 'span#basic-unlock', function() {
    var unlock = $('i#unlock-contract').data('unlockcontract');

    if(unlock == '1')
        $('i#unlock-contract').removeClass('fa-lock').addClass('fa-unlock'),
        $('i#unlock-contract').data('unlockcontract', 0),
        $('input[id="contract_number"]').attr('readonly',false);
    else
        $('i#unlock-contract').removeClass('fa-unlock').addClass('fa-lock'),
        $('i#unlock-contract').data('unlockcontract', 1),
        $('input[id="contract_number"]').attr('readonly',true);
});

$('[id^=detail-').hide();
$('.toggle').click(function() {
    $input = $(this);
    $target = $('#'+$input.attr('data-toggle'));
    $target.slideToggle();
});


})();
