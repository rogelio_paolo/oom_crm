$('.buttons-box').on('click', 'button#btn-bulk-assign', function() {
    const holder = $('select#acc_assigned'),
          account_select = $("select#account_select"),
          table = $('table#tbl-list'),
          tbody = $('tbody#acc-list');

    holder.val();
    account_select.val('');
    account_select.selectpicker("refresh");
    table.addClass('hidden');
    tbody.empty();
    

});

$('.modal-footer').on('click', 'button#btnApplyAMHolder', function() {
    const holder = $('select#acc_assigned'),
          base = $('base').attr('href'),
          account_select = $('select#account_select'),
          table = $('table#tbl-list'),
          error = $('div#error-acc-select');
    
    var accList = '';

    $.ajax({
        url: `${base}/account/multiple-assign-holder`,
        type: 'POST',
        data: { _token: $('meta[name="csrf-token"]').attr('content'), acc_assigned: holder.val(), account_ids: account_select.val() },
        beforeSend: function() {
            loading();
            if (account_select.val().length == 0) {
                loaded();
                !table.hasClass('hidden') ? $('table#tbl-list').addClass('hidden') : '';
                account_select.css('border-color', 'red');
                error.removeClass('hidden');
                return false;
            }
        },
        success: function(data) {
            loaded();
            account_select.val('');
            account_select.selectpicker("refresh");
            error.addClass('hidden');
            table.removeClass('hidden');
            $.each(data.result, function(i, j) {
                accList += `<tr class="bg-warning">
                                <td>${j.company}</td>
                                <td>${j.contract_number}</td>
                                <td>${j.acc_assigned}</td>
                                <td>${j.busi_manager}</td>
                                <td>${j.updated_at}</td>
                            </tr>`;
            });
            
            $('tbody#acc-list').html(accList).fadeIn('fast');
           
        },
        error: function(error) {
            console.log(`Error: ${error.responseText}`);
        }
    });

});

// $('select#acc_assigned').on('change', function() {
//     const acc_assigned = $(this);
//           account_ids = $('select#account_ids');

// });
