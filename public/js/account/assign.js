function loading() {
    $('div.loader-overlay').removeClass('hidden');
}

function loaded() {
    $('div.loader-overlay').addClass('hidden');
}

function listEmployee(selected, assignee) {
    $.each($(selected + " option:selected"), function(){            
        assignee.push($(this).val());
    });
}

function assign(assignPackage, assignPackageId, assignSelected, assignOtherSelected = false, assignSelected2 = false, assignOtherSelected2 = false, others = false) {
    var assignEmployee = [];
    var assignOtherEmployee = [];
    var assignEmployee2 = [];
    var assignOtherEmployee2 = [];
    var assignOthers = [];
    var accountId = $('input[name="accountid"]').val();

    // Main Members
    listEmployee(assignSelected, assignEmployee);
    // Other assignee
    listEmployee(assignOtherSelected, assignOtherEmployee);
    // Other assignee
    listEmployee(assignSelected2, assignEmployee2);
    // Other assignee
    listEmployee(assignOtherSelected2, assignOtherEmployee2);
    // Other assignee
    listEmployee(others, assignOthers);

    var urlEmployee = (assignEmployee.join('|') == '' ? 0 : assignEmployee.join('|'));
    var urlOtherEmployee = (assignOtherEmployee.join('|') == '' ? 0 : assignOtherEmployee.join('|'));
    var urlEmployee2 = (assignEmployee2.join('|') == '' ? 0 : assignEmployee2.join('|'));
    var urlOtherEmployee2 = (assignOtherEmployee2.join('|') == '' ? 0 : assignOtherEmployee2.join('|'));
    var urlOthers = (assignOthers.join('|') == '' ? 0 : assignOthers.join('|'));
    
    var url = $('base').attr('href') + '/account/assign/' + accountId + '/' + assignPackage  + '/' + $(assignPackageId).val() + '/' + urlEmployee + 
                '/' + urlOtherEmployee + '/' + urlEmployee2 + '/' + urlOtherEmployee2 + '/' + urlOthers;

    console.log(url);

    $.ajax({
        url : url,
        beforeSend : function() {
            loading();
        },
        success: function(data) {
            loaded();
            console.log(data);
        }
    });
}

function alertAssign(package,id, members, other_members = false, members2 = false, other_members2 = false, others = false) {
    // var names = $.map($(members + " option:selected"), function (el, i) {
    //     return $(el).text()
    // });

    // var names_other = $.map($(other_members + " option:selected"), function (el, i) {
    //     return $(el).text()
    // });

    // var names2 = $.map($(members2 + " option:selected"), function (el, i) {
    //     return $(el).text()
    // });

    // var names2_other = $.map($(other_members2 + " option:selected"), function (el, i) {
    //     return $(el).text()
    // });

    // var others = $.map($(others + " option:selected"), function (el, i) {
    //     return $(el).text()
    // });

    // names.push(names_other);
    // names_other.push(names);
    // names2.push(names_other);
    // names2_other.push(names2);
    // others.sort().push(names2_other);

    swal({
        title: 'Are you sure?',
        text: "You are assigning members in " + package.toUpperCase() + ' service!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
    }).then((result) => {
        if (result.value) {
            assign(package, id, members, other_members, members2, other_members2, others);
            swal(
                'Assigned!',
                'Successfully assigned members in ' + package.toUpperCase() + ' service!',
                'success'
            )
        }
    });
}

function selectOption(main, asst, link = false, asst_link = false) {
    $(main).on("change", function() {
        let $boxval = $(this).val();
      
        if($boxval !== '')
            $(asst + " > option").each(function(ind) {
                let el = $(asst + "> option").eq(ind);
                if (el.val() === $boxval) el.attr("disabled", "disabled");
                else el.removeAttr("disabled");
            });
      });
      
      $(asst).on("change", function() {
        let $boxval = $(this).val();
      
        if($boxval !== '')
            $(main + " > option").each(function(ind) {
                let el = $(main + " > option").eq(ind);
                if (el.val() === $boxval) el.attr("disabled", "disabled");
                else el.removeAttr("disabled");
            });
      });

      $(main).on("change", function() {
        let $boxval = $(this).val();
      
        if($boxval !== '')
            $(asst + " > option").each(function(ind) {
                let el = $(asst + "> option").eq(ind);
                if (el.val() === $boxval) el.attr("disabled", "disabled");
                else el.removeAttr("disabled");
            });
      });
      
      if(link !== false && asst_link !== false) {
        $(link).on("change", function() {
            let $boxval = $(this).val();
          
            if($boxval !== '')
                $(asst_link + " > option").each(function(ind) {
                    let el = $(asst_link + "> option").eq(ind);
                    if (el.val() === $boxval) el.attr("disabled", "disabled");
                    else el.removeAttr("disabled");
                });
          });

        $(asst_link).on("change", function() {
            let $boxval = $(this).val();
        
            if($boxval !== '')
                $(link + " > option").each(function(ind) {
                    let el = $(link + " > option").eq(ind);
                    if (el.val() === $boxval) el.attr("disabled", "disabled");
                    else el.removeAttr("disabled");
                });
        });
    }
      
}

/* SELECT OPTION NOT DUPLICATING OPTION VALUES */
$(document).ready(function() {
    selectOption('select#sem_strategist_assigned', 'select#asst_sem_strategist_assigned'); // SEM Strategist
    selectOption('select#fb_strategist_assigned', 'select#asst_fb_strategist_assigned'); // FB Strategist
    selectOption('select#seo_strategist_assigned', 'select#asst_seo_strategist_assigned', 'select#seo_link_builder_assigned', 'select#asst_seo_link_builder_assigned'); // SEM Strategist and SEO Link Builder
    selectOption('select#acc_team', 'select#acc_assistant'); // AM
});

/* END OF CODE */


$(document).on('change', 'select#sem_strategist_assigned, select#asst_sem_strategist_assigned', function() {
    const sem_strategist = $('select#sem_strategist_assigned');

     if (!sem_strategist.val())
        $('input[id="btn-assign-sem"]').attr('disabled', 'disabled');
     else
        $('input[id="btn-assign-sem"]').removeAttr('disabled');
});

$(document).on('change', 'select#fb_strategist_assigned, select#asst_fb_strategist_assigned', function() {
    if (!$('select#fb_strategist_assigned').val()) {
       $('input[id="btn-assign-fb"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-fb"]').removeAttr('disabled');
    }
});

// SEO Service Package
$(document).on('change', 'select#seo_strategist_assigned, select#asst_seo_strategist_assigned', function() {
    if (!$('select#seo_strategist_assigned').val()) {
       $('input[id="btn-assign-seo"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-seo"]').removeAttr('disabled');
    }
});

$(document).on('change', 'select#seo_link_builder_assigned, select#asst_seo_link_builder_assigned', function() {
    if (!$('select#seo_link_builder_assigned').val()) {
       $('input[id="btn-assign-seo"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-seo"]').removeAttr('disabled');
    }
});

$(document).on('change', 'select#acc_team, select#acc_assistant', function() {
    if ($('select#acc_team').val() == '0') {
       $('input[id="btn-assign-acc"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-acc"]').removeAttr('disabled');
    }
});
// End of SEO Service Package

// WebDev Service Package
$(document).on('change', 'select#dev_team, select#dev_content_team', function() {
    if ($('select#dev_team option:selected').length == 0 && $('select#dev_content_team option:selected').length == 0) {
       $('input[id="btn-assign-dev"]').attr('disabled', 'disabled');
       $('input[id="btn-dev-clear"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-dev"]').removeAttr('disabled');
       $('input[id="btn-dev-clear"]').removeAttr('disabled');
    }
});
// End of WebDev Service Package

// Blog Content Service Package
$(document).on('change', 'select#blog_content_team', function() {
    if ($('select#blog_content_team option:selected').length == 0) {
       $('input[id="btn-assign-content-blog"]').attr('disabled', 'disabled');
       $('input[id="btn-blog-content-clear"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-content-blog"]').removeAttr('disabled');
       $('input[id="btn-blog-content-clear"]').removeAttr('disabled');
    }
});
// End of Blog Content Service Package

// Social Media Management Content Service Package
$(document).on('change', 'select#social_content_team', function() {
    if ($('select#social_content_team option:selected').length == 0) {
       $('input[id="btn-assign-content-social"]').attr('disabled', 'disabled');
       $('input[id="btn-social-content-clear"]').attr('disabled', 'disabled');
    } else {
       $('input[id="btn-assign-content-social"]').removeAttr('disabled');
       $('input[id="btn-social-content-clear"]').removeAttr('disabled');
    }
});
// End of Social Media Management Content Service Package

// WebDev Service Package: Clear button
$(document).on('click', 'input[id="btn-dev-clear"]', function() {
    $('select#dev_team option:selected').each(function() {
        $(this).prop('selected', false);
    });

    $('select#dev_content_team option:selected').each(function() {
        $(this).prop('selected', false);
    });

    $('select#dev_team').multiselect('refresh');
    $('select#dev_content_team').multiselect('refresh');

    $('input[id="btn-assign-dev"]').attr('disabled', 'disabled');
    $(this).attr('disabled', 'disabled');
});
// End of WebDev Service Package: Clear button

// Blog Content Service Package: Clear button
$(document).on('click', 'input[id="btn-blog-content-clear"]', function() {
    $('select#blog_content_team option:selected').each(function() {
        $(this).prop('selected', false);
    });

    $('select#blog_content_team').multiselect('refresh');

    $('input[id="btn-assign-content-blog"]').attr('disabled', 'disabled');
    $(this).attr('disabled', 'disabled');
});
// End Blog Content Service Package: Clear button

// Social Media Management Content Service Package: Clear button
$(document).on('click', 'input[id="btn-social-content-clear"]', function() {
    $('select#social_content_team option:selected').each(function() {
        $(this).prop('selected', false);
    });

    $('select#social_content_team').multiselect('refresh');

    $('input[id="btn-assign-content-social"]').attr('disabled', 'disabled');
    $(this).attr('disabled', 'disabled');
});
// End of Social Media Management Content Service Package: Clear button

$(document).on('click', 'input[id="btn-assign-sem"]', function(){
   alertAssign('sem', 'input[name="accountsemid"]', 'select#sem_strategist_assigned', 'select#asst_sem_strategist_assigned');
});

$(document).on('click', 'input[id="btn-assign-fb"]', function(){
    alertAssign('fb', 'input[name="accountfbid"]', 'select#fb_strategist_assigned', 'select#asst_fb_strategist_assigned');
});

// Assign SEO Content
$(document).on('click', 'input[id="btn-assign-seo"]', function(){
    alertAssign('seo', 'input[name="accountseoid"]', 'select#seo_strategist_assigned', 'select#asst_seo_strategist_assigned', 'select#seo_link_builder_assigned', 'select#asst_seo_link_builder_assigned', 'select#seo_content_team');
});
// End of Assign SEO Content

// Assign WebDev Content
$(document).on('click', 'input[id="btn-assign-dev"]', function(){
    alertAssign('web', 'input[name="accountwebid"]', 'select#dev_team', 'select#dev_content_team');
});
// End of WebDev Content

// Assign Blog Content
$(document).on('click', 'input[id="btn-assign-content-blog"]', function(){
    alertAssign('blog', 'input[name="accountblogid"]', 'select#blog_content_team');
});
// End of Blog Content

// Assign Social Media Management
$(document).on('click', 'input[id="btn-assign-content-social"]', function(){
    alertAssign('social', 'input[name="accountsocialid"]', 'select#social_content_team' );
});
// End of Social Media Management

// Assign Account Management Assign
$(document).on('click', 'input[id="btn-assign-acc"]', function(){
    alertAssign('acc', 'input[name="accountid"]', 'select#acc_team', 'select#acc_assistant');
});
// End of Account Management Assign