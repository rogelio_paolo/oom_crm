function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
}

function loaded(){
    var loader = $('#loader');
    loader.addClass('hidden');
}

function pagination(pagination,accountRow,route){
    var baseUrl = $('base').attr('href'),
        
        url = pagination.path + '?page=';
        console.log(url);
        var fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#account-management > tbody');

        if(pagination.last_page > 1){
            
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to) {
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else {
            $('ul.pagination').addClass('hidden');
        }
        
        // table.html(accountRow).fadeOut();
        table.html(accountRow).fadeIn('fast');
}

function getPagination(uri) {
    var accountRow = "",
        baseUrl = $('base').attr('href');

    $.ajax({
        url:uri,
        beforeSend:function(){
            $('table#account-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading();
        },
        success:function(data){
            loaded();
            modify();
            if (data.result.length > 0){
                $.each(data.result,function(i,j){
                    i = ((data.pagination.current_page * data.pagination.per_page) - data.pagination.per_page ) + i;
                    accountRow += "<tr>";
                    accountRow += "<td class='text-center company_name'>"+ j.company+ "</td>";
                    accountRow += "<td class='text-center contract_number " + (data.column ? (data.column.contract_number ? '' : 'hidden') : '') +  "'>" + (j.contracts > 1 ? '<span id="ellipsis" class="ellipsis ellipsis-'+j.prospect+'" data-prospect-id="'+j.prospect +'">'+j.contract_number+'</span> <i id="btn-arrow-'+j.prospect+ '" class="fa fa-ellipsis-v"></i>' : j.contract_number ) + "</td>";
                    accountRow += "<td class='text-center day_90 " + (data.column ? (data.column.day_90 ? '' : 'hidden') : '') +  "'>" +j.day_90+"</td>";
                    accountRow += "<td class='text-center day_120 " + (data.column ? (data.column.day_120 ? '' : 'hidden') : '') +  "'>" +j.day_120+"</td>";
                    accountRow += "<td class='text-center account_holder " + (data.column ? (data.column.account_holder ? '' : 'hidden') : '') + "'>"+j.holder+"</td>";
                    accountRow += "<td class='text-center sp_assigned " + (data.column ? (data.column.sp_assigned ? '' : 'hidden') : '') + "'>"+j.sp_assigned+"</td>";
                    accountRow += "<td class='text-center busi_manager " + (data.column ? (data.column.busi_manager ? '' : 'hidden') : '') + "'>" + j.sales + "</td>";
                    accountRow += "<td class='text-center nature " + (data.column ? (data.column.nature ? '' : 'hidden') : '') + "'>"+j.package+"</td>";
                    accountRow += "<td class='text-center am_fee " + (data.column ? (data.column.am_fee ? '' : 'hidden') : 'hidden') + "'>"+j.am_fee+"</td>";
                    accountRow += "<td class='text-center asst_account_holder " + (data.column ? (data.column.asst_account_holder ? '' : 'hidden') : '') + "'>"+j.asst_holder+"</td>";
                    accountRow += "<td class='text-center campaign_start " + (data.column ? (data.column.campaign_start ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_start+"</td>";
                    accountRow += "<td class='text-center campaign_end " + (data.column ? (data.column.campaign_end ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_end+"</td>";
                    accountRow += "<td class='text-center budget " + (data.column ? (data.column.budget ? '' : 'hidden') : 'hidden') + "'>"+j.budget+"</td>";
                    accountRow += "<td class='text-center date_created'>"+j.date+"</td>";
                    accountRow += "<td class='text-center text-nowrap' id='account-management'> ";

                    if (!$.inArray(data.role,[3,4]) !== -1 || $.inArray(data.teamcode,['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1) {
                        accountRow += (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<span class="dropup">' +
                            '<button class="btn btn-success dropdown-toggle margin-top default-dom" type="button" id="btnAddDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '<i class="fa fa-plus"></i> Add</button> ' +
                            '<ul class="dropdown-menu" aria-labelledby="btnAddDropdown">'+
                            '<li><a id="prospect-convert" data-prospectid="'+ j.prospect +'" data-toggle="modal" data-target="#contract-modal"><i class="fa fa-plus-circle"></i> Contract</a></li>' +
                            '<li><a id="prospect-opportunity" data-prospect-id="'+ j.prospect +'" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Opportunity</a></li>' +
                            '</ul> ' : '');

                        accountRow += (j.contracts > 1 ? ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' ) : '' +
                                '<a data-toggle="tooltip" title="Click to launch more buttons" href="#" id="btn-group-single-acc" data-prospect-id="'+j.prospect+'" class="btn btn-info"><i class="glyphicon glyphicon-option-vertical"></i></a>' +
                                ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                    '<div id="btn-group-acc-' + j.prospect + '" class="btn-group-vertical hidden" role="group">' +
                                    '<a data-toggle="tooltip" title="View account details" href="/account/view/'+j.id+'" class="btn btn-info"><i class="fa fa-eye"></i></a> ' : '') +
                                (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<a data-toggle="tooltip" title="Edit account details" href="/account/edit/'+j.id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '' ) +
                                ($.inArray(data.user.emp_id, data.managers) !== -1 || data.role == 1 ? '<a data-toggle="tooltip" title="Assign members on this account" href="/account/edit/assign/'+j.id+'"' +
                                    'class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i></a> ' : '') +
                                (data.role == 1 ? '<a data-toggle="tooltip" title="Delete account" id="accountDelete" data-accountid="'+j.id+'" class="btn btn-danger">' +
                                    '<i class="fa fa-trash"></i></a> ' : '')
                        );


                    }
                    else {
                      accountRow += (j.contracts > 1 ? '' + 
                      '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' +
                            "<a data-toggle='tooltip' 'title='View account details' href='/account/view/"+j.id+"' class='btn btn-info'><i class='fa fa-eye'></i></a> ");
                    }

                    accountRow += "</td></tr>";
                });

                pagination(data.pagination,accountRow);
                $('ul.pagination').removeClass('hidden');
            }
            else{
                accountRow = "<tr><td colspan='6' class='text-center'>No accounts found</td></tr>";

                $('ul.pagination').addClass('hidden');
            }

            var route = window.location.href;
            
            pagination(data.pagination,accountRow,route);

        }
    });
}

function clearSorting(){
    $('i#sort-account').each(function(){
        $(this).parent().removeClass('active');
        $(this).addClass('fa fa-sort-alpha-desc');

        if ($(this).parent().data('sortby') == 'created_at')
            $(this).parent().addClass('active');
    });
}

function sortAccount(url){
    var accountRow = "",
        baseUrl = $('base').attr('href');

    $.ajax({
        url: url,
        beforeSend:function(){
            $('table#account-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading();

        },
        success:function(data){
            console.log(data);
            loaded();
            modify();
            if(data.result.length > 0) {
                 $.each(data.result, function (i, j){
                    i = ((data.pagination.current_page * data.pagination.per_page) - data.pagination.per_page ) + i;
                    accountRow += "<tr>";
                    accountRow += "<td class='text-center company_name'>"+ j.company+ "</td>";
                    accountRow += "<td class='text-center contract_number " + (data.column ? (data.column.contract_number ? '' : 'hidden') : '') +  "'>" + (j.contracts > 1 ? '<span id="ellipsis" class="ellipsis ellipsis-'+j.prospect+'" data-prospect-id="'+j.prospect +'">'+j.contract_number+'</span> <i id="btn-arrow-'+j.prospect+ '" class="fa fa-ellipsis-v"></i>' : j.contract_number ) + "</td>";
                    accountRow += "<td class='text-center day_90 " + (data.column ? (data.column.day_90 ? '' : 'hidden') : '') +  "'>" +j.day_90+"</td>";
                    accountRow += "<td class='text-center day_120 " + (data.column ? (data.column.day_120 ? '' : 'hidden') : '') +  "'>" +j.day_120+"</td>";
                    accountRow += "<td class='text-center account_holder " + (data.column ? (data.column.account_holder ? '' : 'hidden') : '') + "'>"+j.holder+"</td>";
                    accountRow += "<td class='text-center sp_assigned " + (data.column ? (data.column.sp_assigned ? '' : 'hidden') : '') + "'>"+j.sp_assigned+"</td>";
                    accountRow += "<td class='text-center busi_manager " + (data.column ? (data.column.busi_manager ? '' : 'hidden') : '') + "'>" + j.sales + "</td>";
                    accountRow += "<td class='text-center nature " + (data.column ? (data.column.nature ? '' : 'hidden') : '') + "'>"+j.package+"</td>";
                    accountRow += "<td class='text-center am_fee " + (data.column ? (data.column.am_fee ? '' : 'hidden') : 'hidden') + "'>"+j.am_fee+"</td>";
                    accountRow += "<td class='text-center asst_account_holder " + (data.column ? (data.column.asst_account_holder ? '' : 'hidden') : '') + "'>"+j.asst_holder+"</td>";
                    accountRow += "<td class='text-center campaign_start " + (data.column ? (data.column.campaign_start ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_start+"</td>";
                    accountRow += "<td class='text-center campaign_end " + (data.column ? (data.column.campaign_end ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_end+"</td>";
                    accountRow += "<td class='text-center budget " + (data.column ? (data.column.budget ? '' : 'hidden') : 'hidden') + "'>"+j.budget+"</td>";
                    accountRow += "<td class='text-center date_created'>"+j.date+"</td>";
                    accountRow += "<td class='text-center text-nowrap' id='account-management'> ";

                     if (!$.inArray(data.role,[3,4]) !== -1 || $.inArray(data.teamcode,['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1) {
                         accountRow += (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<span class="dropup">' +
                             '<button class="btn btn-success dropdown-toggle margin-top default-dom" type="button" id="btnAddDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                             '<i class="fa fa-plus"></i> Add</button> ' +
                             '<ul class="dropdown-menu" aria-labelledby="btnAddDropdown">'+
                             '<li><a id="prospect-convert" data-prospectid="'+ j.prospect +'" data-toggle="modal" data-target="#contract-modal"><i class="fa fa-plus-circle"></i> Contract</a></li>' +
                             '<li><a id="prospect-opportunity" data-prospect-id="'+ j.prospect +'" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Opportunity</a></li>' +
                             '</ul> ' : '');

                         accountRow += (j.contracts > 1 ? ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                 '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' ) : '' +
                                 '<a data-toggle="tooltip" title="Click to launch more buttons" href="#" id="btn-group-single-acc" data-prospect-id="'+j.prospect+'" class="btn btn-info"><i class="glyphicon glyphicon-option-vertical"></i></a>' +
                                 ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                     '<div id="btn-group-acc-' + j.prospect + '" class="btn-group-vertical hidden" role="group">' +
                                     '<a data-toggle="tooltip" title="View account details" href="/account/view/'+j.id+'" class="btn btn-info"><i class="fa fa-eye"></i></a> ' : '') +
                                 (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<a data-toggle="tooltip" title="Edit account details" href="/account/edit/'+j.id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '' ) +
                                 ($.inArray(data.user.emp_id, data.managers) !== -1 || data.role == 1 ? '<a data-toggle="tooltip" title="Assign members on this account" href="/account/edit/assign/'+j.id+'"' +
                                     'class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i></a> ' : '') +
                                 (data.role == 1 ? '<a data-toggle="tooltip" title="Delete account" id="accountDelete" data-accountid="'+j.id+'" class="btn btn-danger">' +
                                     '<i class="fa fa-trash"></i></a> ' : '')
                         );


                     }
                     else {
                         accountRow += (j.contracts > 1 ? '' +
                             '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' +
                             "<a data-toggle='tooltip' 'title='View account details' href='/account/view/"+j.id+"' class='btn btn-info'><i class='fa fa-eye'></i></a> ");
                     }

                     accountRow += "</td></tr>";
                 });
             } else {
                accountRow = "<tr><td colspan='6' class='text-center'>No accounts found to sort</td></tr>";

                $('ul.pagination').addClass('hidden');
             }
           

            var route = '/account/sort/' + data.field + '/' + data.order;
            
            pagination(data.pagination,accountRow,route);
            $('ul.pagination').removeClass('hidden');
        }
    });
}

function searchAccount(){
    var account = $('input[id="searchAccount"]').val(),
        key = account === '' ? 'null' : account,
        baseUrl = $('base').attr('href');

    if (account === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/account/search/' + key,
        beforeSend:function(){
            $('table#account-management > tbody').html('<div class="leadnotes-loading"></div>'),
            loading()
        },
        success:function(data){
            console.log(data.role == 1 || $.inArray(data.teamcode, ["busi","acc"]) !== -1);
            loaded();
            modify();
            var accountRow = "",
                tblRow = $('table#account-management > tbody');
            if (data.result.length > 0){
                $.each(data.result,function(i,j){
                    i = ((data.pagination.current_page * data.pagination.per_page) - data.pagination.per_page ) + i;
                    accountRow += "<tr>";
                    accountRow += "<td class='text-center company_name'>"+ j.company+ "</td>";
                    accountRow += "<td class='text-center contract_number " + (data.column ? (data.column.contract_number ? '' : 'hidden') : '') +  "'>" + (j.contracts > 1 ? '<span id="ellipsis" class="ellipsis ellipsis-'+j.prospect+'" data-prospect-id="'+j.prospect +'">'+j.contract_number+'</span> <i id="btn-arrow-'+j.prospect+ '" class="fa fa-ellipsis-v"></i>' : j.contract_number ) + "</td>";
                    accountRow += "<td class='text-center day_90 " + (data.column ? (data.column.day_90 ? '' : 'hidden') : '') +  "'>" +j.day_90+"</td>";
                    accountRow += "<td class='text-center day_120 " + (data.column ? (data.column.day_120 ? '' : 'hidden') : '') +  "'>" +j.day_120+"</td>";
                    accountRow += "<td class='text-center account_holder " + (data.column ? (data.column.account_holder ? '' : 'hidden') : '') + "'>"+j.holder+"</td>";
                    accountRow += "<td class='text-center sp_assigned " + (data.column ? (data.column.sp_assigned ? '' : 'hidden') : '') + "'>"+j.sp_assigned+"</td>";
                    accountRow += "<td class='text-center busi_manager " + (data.column ? (data.column.busi_manager ? '' : 'hidden') : '') + "'>" + j.sales + "</td>";
                    accountRow += "<td class='text-center nature " + (data.column ? (data.column.nature ? '' : 'hidden') : '') + "'>"+j.package+"</td>";
                    accountRow += "<td class='text-center am_fee " + (data.column ? (data.column.am_fee ? '' : 'hidden') : 'hidden') + "'>"+j.am_fee+"</td>";
                    accountRow += "<td class='text-center asst_account_holder " + (data.column ? (data.column.asst_account_holder ? '' : 'hidden') : '') + "'>"+j.asst_holder+"</td>";
                    accountRow += "<td class='text-center campaign_start " + (data.column ? (data.column.campaign_start ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_start+"</td>";
                    accountRow += "<td class='text-center campaign_end " + (data.column ? (data.column.campaign_end ? '' : 'hidden') : 'hidden') + "'>"+j.campaign_end+"</td>";
                    accountRow += "<td class='text-center budget " + (data.column ? (data.column.budget ? '' : 'hidden') : 'hidden') + "'>"+j.budget+"</td>";
                    accountRow += "<td class='text-center date_created'>"+j.date+"</td>";
                    accountRow += "<td class='text-center text-nowrap' id='account-management'> ";

                    if (!$.inArray(data.role,[3,4]) !== -1 || $.inArray(data.teamcode,['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1) {
                        accountRow += (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<span class="dropup">' +
                            '<button class="btn btn-success dropdown-toggle margin-top default-dom" type="button" id="btnAddDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '<i class="fa fa-plus"></i> Add</button> ' +
                            '<ul class="dropdown-menu" aria-labelledby="btnAddDropdown">'+
                            '<li><a id="prospect-convert" data-prospectid="'+ j.prospect +'" data-toggle="modal" data-target="#contract-modal"><i class="fa fa-plus-circle"></i> Contract</a></li>' +
                            '<li><a id="prospect-opportunity" data-prospect-id="'+ j.prospect +'" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus-circle"></i> Opportunity</a></li>' +
                            '</ul> ' : '');

                        accountRow += (j.contracts > 1 ? ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' ) : '' +
                                '<a data-toggle="tooltip" title="Click to launch more buttons" href="#" id="btn-group-single-acc" data-prospect-id="'+j.prospect+'" class="btn btn-info"><i class="glyphicon glyphicon-option-vertical"></i></a>' +
                                ($.inArray(data.teamcode, ['dev','acc','busi','sp','sem','seo','fb','con','hr']) !== -1 || data.role == 1 ? '' +
                                    '<div id="btn-group-acc-' + j.prospect + '" class="btn-group-vertical hidden" role="group">' +
                                    '<a data-toggle="tooltip" title="View account details" href="/account/view/'+j.id+'" class="btn btn-info"><i class="fa fa-eye"></i></a> ' : '') +
                                (data.role == 1 || $.inArray(data.teamcode, ['busi','acc']) !== -1 ? '<a data-toggle="tooltip" title="Edit account details" href="/account/edit/'+j.id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '' ) +
                                ($.inArray(data.user.emp_id, data.managers) !== -1 || data.role == 1 ? '<a data-toggle="tooltip" title="Assign members on this account" href="/account/edit/assign/'+j.id+'"' +
                                    'class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i></a> ' : '') +
                                (data.role == 1 ? '<a data-toggle="tooltip" title="Delete account" id="accountDelete" data-accountid="'+j.id+'" class="btn btn-danger">' +
                                    '<i class="fa fa-trash"></i></a> ' : '')
                        );

                    }
                    else {
                        accountRow += (j.contracts > 1 ? '' +
                            '<a data-toggle="tooltip" title="View contract summary" href="/contract/summary/'+j.prospect+'" class="btn btn-primary"><i class="glyphicon glyphicon-menu-hamburger"></i></a> ' : '' +
                            "<a data-toggle='tooltip' 'title='View account details' href='/account/view/"+j.id+"' class='btn btn-info'><i class='fa fa-eye'></i></a> ");
                    }

                    accountRow += "</td></tr>";
                });
                
                pagination(data.pagination,accountRow);
            }
            else{
                accountRow = "<tr><td colspan='6' class='text-center'>No results found for "+account+"</td></tr>";

                tblRow.html(accountRow).fadeIn('fast');
                $('ul.pagination').addClass('hidden');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchAccount();
    }
}

function modify() {
    const url = $('base').attr('href') + '/account/modifycolumn';

    var checkboxes = {
        '_token' : $('meta[name="csrf-token"]').attr('content'),
        'contract_number' : +$('input[name="contract_number"]').is(':checked'),
        'day_90' : +$('input[name="day_90"]').is(':checked'),
        'day_120' : +$('input[name="day_120"]').is(':checked'),
        'nature' : +$('input[name="nature"]').is(':checked'),
        'account_holder' : +$('input[name="account_holder"]').is(':checked'),
        'sp_assigned' : +$('input[name="sp_assigned"]').is(':checked'),
        'busi_manager' : +$('input[name="busi_manager"]').is(':checked'),
        'am_fee' : +$('input[name="am_fee"]').is(':checked'),
        'asst_account_holder' : +$('input[name="asst_account_holder"]').is(':checked'),
        'campaign_start' : +$('input[name="campaign_start"]').is(':checked'),
        'campaign_end' : +$('input[name="campaign_end"]').is(':checked'),
        'budget' : +$('input[name="budget"]').is(':checked')
    };

    $.ajax({
        url: url,
        type: 'patch',
        data: checkboxes,
        beforeSend: function() {
            // The loader overlay will show
            $('i#loader-modal').removeClass('hidden');
        },
        success: function(data) {
            // If not checked, the column will hide
            $('input:checkbox:not(:checked)').each(function() {
                var column = "table ." + $(this).attr("name");
                $(column).addClass('hidden');
            });
                
            // If checked, the column will show
            $('input:checkbox:checked').each(function() {
                var column = "table ." + $(this).attr("name");
                $(column).removeClass('hidden');
            });

            // The loader overlay and modal will hide
            $('i#loader-modal').addClass('hidden');
            $('#modifyColumn').modal('hide');
        },
        error: function(error) {
            console.log('Error: ' + error.responseText);
        }
    });
}

function showAddReminder(id){
    $('tr#row-'+id).find('td.overlay').css('opacity','0.2');
    $('tr#row-'+id).find('td.set-reminder').removeClass('hidden');
}

function dismissAddReminder(id){
    $('tr#row-'+id).find('td.overlay').css('opacity','1');
    $('tr#row-'+id).find('td.set-reminder').addClass('hidden');
}

(function(){
    var root = $('base').attr('href');
    
    $(document).on('click','a#accountDelete', function(){
        var accountid = $(this).data('accountid');
        
        $('#delete-accountid').val(accountid);
        $('#deleteAccountModal').modal('show');
    });
    
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        var link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('list') >= 1)
            getPagination(url);
        else if(url.indexOf('accountsearch') >= 1)
            searchAccount();
        else
            sortAccount(url);

    });
    
    $(document).on('click','a.sort-account', function(e){
        e.preventDefault();
        var currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        // old algorithm
        // if (currOrder.indexOf('asc') >= 1)
        //     $('i#sort-account').each(function(){
        //         $(this).removeClass('fa-sort-alpha-asc');
        //         $(this).addClass('fa-sort-alpha-desc');
        //     });
        // else
        //     $('i#sort-account').each(function(){
        //         $(this).removeClass('fa-sort-alpha-desc');
        //         $(this).addClass('fa-sort-alpha-asc');
        //     });

        $(this).addClass('active');
        $(this).parent().siblings().find('a').removeClass('active');

        if (currOrder.indexOf('asc') >= 1)
            $(this).find('i').removeClass('fa-sort-alpha-asc'),
            $(this).find('i').addClass('fa-sort-alpha-desc');
        else
            $(this).find('i').removeClass('fa-sort-alpha-desc'),
            $(this).find('i').addClass('fa-sort-alpha-asc');

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'asc' : 'desc';

        var url = root + '/account/sort/' + sortBy + '/' + sortOrder;

        sortAccount(url);
    });
    
    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        var url = $('base').attr('href') + '/account/refresh';

        clearSorting()

        getPagination(url);
        $('#searchAccount').val('');
    });
    
    $(document).ready(function() {


        var overlay = '<td class="overlay"><a href="" class="btn btn-default"> <i class="fa fa-clock"></i> Set a reminder </a></td>';
    
        $('table#account-management tbody').find('tr').each(function (){
            
            $(this).hover(
                function() {
                    $(this).find('td.overlay').css('opacity','0.2');
                    $(this).find('td.set-reminder').removeClass('hidden');
                },
                function() {
                    $(this).find('td.overlay').css('opacity','1');
                    $(this).find('td.set-reminder').addClass('hidden');
                }
            );
            // $(this).find('a#schedule-reminder').on('click', function(){
            //     var accountid = $(this).data('accountid'),
            //         reminder = $('#addReminderModal');
    
            //         $.ajax({
            //             url: root + '/reminder/get/receivers/' + accountid,
            //             success:function(data){
            //                 console.log(data);
            //                 $.each(data[0], function(i,j) {
            //                     receivers += '<option value="'+j.email+'">';
            //                     receivers += j.name;
            //                     receivers += '</option>';
            //                 });
    
            //                 // $.each(data[1], function(i,j) {
            //                 //     remindertime += '<option value="'+j.systemcode+'">';
            //                 //     remindertime += j.systemdesc;
            //                 //     remindertime += '</option>';
    
            //                 //     $(document).on('change', 'select#remind_time', function() {
            //                 //         if ($(this).val() == 'RT999')
            //                 //             reminder.find('tr#reminder_other').removeClass('hidden');
            //                 //         else
            //                 //             reminder.find('tr#reminder_other').addClass('hidden');
            //                 //     });
                                    
            //                 // });
    
            //                 // $.each(data[2], function(i, j) {
            //                 //     remindertimeother += '<option value="'+j.systemcode+'">';
            //                 //     remindertimeother += j.systemdesc;
            //                 //     remindertimeother += '</option>';
                                
            //                 // });
                            
            //                 reminder.find('select#receivers').html(receivers);
            //                 // reminder.find('select#remind_time').html(remindertime);
            //                 // reminder.find('select#remind_time_other').html(remindertimeother);
            //                 $('select#receivers').selectpicker('refresh');
            //                 // $('select#remind_time').selectpicker('refresh');
            //                 // $('select#remind_time_other').selectpicker('refresh');
    
                            
            //             }
            //         });
    
            //         reminder.find('input[id="reminder-accountid"]').val(accountid);
            //         reminder.modal('show');
                    
            // })
            
        });

        $(document).on('click', 'a[id="schedule-reminder"]', function(e) {
            e.preventDefault();

            var receivers = '',
                $reminderModal = $('#addReminderModal'),
                accountid = $(this).data('accountid'),
                baseUrl = $('base').attr('href'),
                url = baseUrl + '/reminder/get/receivers/' + accountid,
                name = $('input[id="name"]'),
                date = $('input[id="date"]'),
                receiver = $('select[id="receivers"]'),
                remarks = $('textarea[id="remark"]');

           
            $('span[id="span-title"]').text('');
            name.val('');
            name.css('border', '1px solid #bbbbbb');

            $('span[id="span-date"]').text('');
            date.val('');
            date.css('border', '1px solid #bbbbbb');

            $('span[id="span-remarks"]').text('');
            remarks.val();
            remarks.css('border', '1px solid #bbbbbb');

            $('span[id="span-receivers"]').text('');
            receiver.val(0);
            
            $.ajax({
                url : url,
                success: function(data) {
                    $.each(data[0], function(i, j) {
                        receivers += '<option value="'+j.email+'">'+j.name+'</option>';
                    });

                    $reminderModal.find('select#receivers').html(receivers);
                    $('select#receivers').selectpicker('refresh');
                }
            });

            $reminderModal.find('input[id="reminder-accountid"]').val(accountid);
        });

        // Reset modal dialog window when close!
          $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
    });

    $(document).on('click', 'button[id="reminder-create"]', function() {
        
        var name = $('input[id="name"]'),
            date = $('input[id="date"]'),
            receiver = $('select[id="receivers"]'),
            remarks = $('textarea[id="remark"]');


        if(name.val().length != '' && receiver.val().length != '' && date.val().length != '' && remarks.val().length != '') {
            return true;
        } else {
            if(name.val().length == '') {
                $('span[id="span-title"]').text('Title is required');
                name.css('border', '1px solid red');
            } else {
                $('span[id="span-title"]').text('');
                name.css('border', '1px solid #bbbbbb');
            }

            if(receiver.val().length == '') {
                $('span[id="span-receivers"]').text('Select at least one');
                receiver.css('border', '1px solid red');
            } else {
                $('span[id="span-receivers"]').text('');
                receiver.css('border', '1px solid #bbbbbb');
            }

            if(date.val().length == '') {
                $('span[id="span-date"]').text('Date is required');
                date.css('border', '1px solid red');
            } else {
                $('span[id="span-date"]').text('');
                date.css('border', '1px solid #bbbbbb');
            }

            if(remarks.val().length == '') {
                $('span[id="span-remarks"]').text('Remarks is required');
                remarks.css('border', '1px solid red');
            } else {
                $('span[id="span-remarks"]').text('');
                remarks.css('border', '1px solid #bbbbbb');
            }
        
            
            return false;
        }
    });

   
    
    $(document).on('click', 'button.search-btn', function() {
        searchAccount();
    });

    $(document).on('change', 'select#limit_entry', function() {
        var baseUrl = $('base').attr('href');

        $.ajax({
            url : baseUrl + '/items/accounts/' + this.value,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                loaded();
                location.reload();
            }
        })
    });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();  

        var base_url = $('base').attr('href'); // Get the URL link of browser.

        // Create DOM element labels and tables element for active and past contracts
        $('#contract').html('<table id="contracts" class="table table-hover text-center"></table>'); // Create DOM element for Contracts table.

        // "Add Contract" event button when clicked!
        $(document).on('click', 'a#prospect-convert', function(e) {

            e.preventDefault(); // Remove the default state of anchor tag element.

            var table_contracts = $('table#contracts'), // Initialize variable of Contracts table.
                contracts = ''; // Initialize empty variable contract for concatenating.

            $('a#add-contract').attr('href', base_url + '/prospect/convert/' + $(this).data('prospectid') + '/step-1'); // Change the anchor href attribute into conversion page module.

            // AJAX setup
            $.ajax({
                url: base_url + '/contract/get-contract/' + $(this).data('prospectid'), // Get URL from Laravel Route
                success: function(data) { // When fetching of data is successful, fetch all the data then loop each.

                    if(data.contracts.length > 0) // If has data contracts
                    {
                        // active_contracts concatenating all the values as follows through below.
                        contracts += '<thead><tr><th>Contract Number</th><th>Contract Type</th><th>Contract Value</th><th>Name</th><th>Contact</th>'; 
                        contracts += '<th>Email Address</th><th>Date Created</th><th>Contract Status</th></tr></thead>';

                        $.each(data.contracts, function(i, j) {
                            contracts += '<tbody><tr class="'+ (j.is_active == 'Active' ? 'success' : '') +'">';
                            contracts += '<td>'+(j.account_id ? '<a title="Click to view '+ j.company+' account" href="'+base_url+'/account/view/'+j.account_id+'">'+j.contract_number+'</a>' : j.contract_number)+'</td>';
                            contracts += '<td>'+j.contract_type+'</td>'
                            contracts += '<td>'+j.contract_value+'</td>';
                            contracts += '<td>'+j.full_name+'</td>';
                            contracts += '<td>'+j.contact_number+'</td>';
                            contracts += '<td>'+j.email+'</td></button>';
                            contracts += '<td>'+j.created_at+'</td></button>';
                            contracts += '<td>'+j.is_active+'</td></button>';
                            contracts += '</tr></tbody>';
                        });

                    }
                    else { // If both neither data of active and past contracts
                        contracts = '<p class="text-center">No Active or Past Contracts</p>';
                    }
                    
                    
                    table_contracts.html(contracts).fadeIn('fast'); // Append concatenating variables to table_contract element.
                }
            });

            // Reset modal dialog window when close!
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });

        });

    });

    $(document).on('click', 'span#ellipsis', function() {
        var prospect = $(this).data('prospect-id'),
            contract = $('span.ellipsis-' + prospect);
        
        $('i#btn-arrow-' + prospect).toggleClass('fa-ellipsis-v');
        contract.toggleClass('ellipsis');
    });

    $(document).on('click', 'a#btn-group-single-acc', function(e) {
        e.preventDefault();
        
        var prospect = $(this).data('prospect-id'),
            groupBtn = $('div#btn-group-acc-' + prospect),
            singleGroupBtn = $('a#btn-group-single-acc[data-prospect-id="'+ prospect +'"]'),
            default_dom = $('span.dropup button.default-dom'),
            role = $('input[id="user_role"]').val(),
            team = $('input[id="team_code"]').val();
            
            singleGroupBtn.addClass('hidden');
            groupBtn.append((role == 1 || $.inArray(team,['busi','acc']) !== -1 ? '<span class="dropup dom-prospect">' +
                '<button class="btn btn-success dropdown-toggle margin-top" type="button" id="btnAddDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i> Add</button> ' +
                 '<ul class="dropdown-menu" aria-labelledby="btnAddDropdown"><li><a id="prospect-convert" data-prospectid="'+ prospect +'" data-toggle="modal" data-target="#contract-modal"><i class="fa fa-plus-circle"></i> Contract</a></li>' +
                '<li><a id="prospect-opportunity" data-prospect-id="'+ prospect +'" data-toggle="modal" data-target="#prospect_opportunity_modal_2"><i class="fa fa-plus circle"></i> Opportunity</a></li>' +
                '</ul></span>' : '' ));

            default_dom.addClass('hidden');
            groupBtn.removeClass('hidden');
            $('[data-toggle="tooltip"]').tooltip();
            //
            // $(document).on('mouseleave', groupBtn, function() {
            //     singleGroupBtn.removeClass('hidden');
            //     default_dom.removeClass('hidden');
            //     $('span.dropup button.dom-prospect').remove();
            //     $(this).addClass('hidden');
            // });

            $(groupBtn).on('mouseleave', function() {
                singleGroupBtn.removeClass('hidden');
                default_dom.removeClass('hidden');
                $('span.dropup.dom-prospect').remove();
                $(groupBtn).addClass('hidden');
            });
    });

})();