
$(document).ready(function() {
    const filterBy = $('select[id="filter-by"]'),
          filterItem = $('select[id="filter-item"]');

    fetchItems(filterBy, filterItem);
});

/* FILTER BUTTON */
$(document).on('click', 'a[id="filter-account"]', function(e) {

    e.preventDefault();
    $('div#filter-container').toggleClass("hidden");

});

/* FILTER BY Button */
$(document).on('change', 'select[id="filter-by"]', function() {

    const filterBy = $(this),
          filterItem = $('select[id="filter-item"]');

    fetchItems(filterBy, filterItem);

    $('button[id="filter-submit"]').attr('disabled', 'disabled');

    if(filterBy.val() == 'all')
        setFilterItem(filterBy);

});

/* FILTER ITEM Button */
$(document).on('change', 'select[id="filter-item"]', function() {

    const btnSubmit = $('button[id="filter-submit"]'),
          filterItem = $(this).val();

    if (filterItem != '') {
        btnSubmit.removeAttr('disabled');
    } else {
        btnSubmit.attr('disabled', 'disabled');
    }

});

/* FILTER SUBMIT Button */
$(document).on('click', 'button[id="filter-submit"]', function() {
    const filterBy =$('select[id="filter-by"]'),
          filterItem = $('select[id="filter-item"]');

    if(!filterItem.val())
        return false      
        
    setFilterItem(filterBy, filterItem);
});

/* FILTER CANCEL Button */
$(document).on('click', 'button[id="filter-cancel"]', function() {
    $('div[id="filter-container"]').addClass("hidden"); // Hide FILTER Container
    $('select[id="filter-item"]').val(''); // Reset the FILTER Item selection
    $('button[id="filter-submit"]').attr('disabled', 'disabled'); // Disable the FILTER Submit Button
});

function setFilterItem(filterBy, filterItem = false) {
    $.ajax({
        url: `/account/fetch/update/account/${filterBy.val()}/${filterItem ? filterItem.val() : 'null'}`,
        success: function(data) {
            location.reload();
        }
        
    });

}

/* Fetch data */
function fetchItems(filterBy, filterItem) {

    var initOption = '';

    if(filterBy.val() == 'all') {
        $('div[id="filter-item-container"]').addClass('hidden');
    }
    else  {
        $('div[id="filter-item-container"]').removeClass('hidden');

        $.ajax({
            url: '/account/fetch/' + filterBy.val() + '/' + (filterItem.val() ? filterItem.val() : 'null'),
            success: function(data) {
               
                var label = filterBy.val() == 'busi_manager' ? 'Business Manager' : (filterBy.val() == 'account_holder' ? 'Account Holder' : (filterBy.val() == 'contract_number' ? 'Contract Number' : '') ); 

                initOption += '<option value="">'+ label +'</option>';
    
                $.each(data.name, function(i, j) {
                    initOption += '<option value="'+ (filterBy.val() == 'busi_manager' ? j.userid : (filterBy.val() == 'account_holder' ? j.emp_id : (filterBy.val() == 'contract_number' ? j.id : ''))) +'"' + (filterBy.val() == 'busi_manager' ? (data.filter.person_filter == j.userid ? 'selected' : '') : (filterBy.val() == 'account_holder' ? (data.filter.person_filter == j.emp_id ? 'selected' : '') : (data.filter.person_filter == j.id ? 'selected' : ''))) +'>'+ (filterBy.val() == 'busi_manager' || filterBy.val() == 'account_holder' ? j.f_name + ' ' + j.l_name : j.contract_number) +'</option>';
                });

                
               filterItem.html(initOption);

            }
        });
    }
}