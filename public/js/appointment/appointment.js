function loading() {
    $('div.loader-overlay').removeClass('hidden');
}

function loaded() {
    $('div.loader-overlay').addClass('hidden');
}

function create_new_category(input){ 
    if(input.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/))
        return true; //if created successfully otherwise return false
}   

function refreshPageTable() {
    $('#appointment-data').paginate({
        perPage: 5
    });
}

function create_new_category(input){
    if(input.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/))
        return true; //if created successfully otherwise return false
}

$('select#guest').selectize({
    create: function(input) {
        //This function will create the category on the server side
        if(create_new_category(input)){
            return {
                value: input,
                text: input
            }
        }
        return false;
    }
});

// $(function() {});

$(document).on('keypress', 'div.bs-searchbox input[type="text"]', function() {
    const text = $(this);

    if (text.val().length <= 40)
        $('select#company').selectpicker({
            noneResultsText: `No matches found on "${text.val()}" <br/>
                                <button data-toggle="modal" 
                                        data-target="#quick_add_leads" 
                                        class="btn btn-success"
                                        data-search-result="${text.val()}">
                                    <i class="fa fa-plus"></i> Add Company "${text.val()}"
                                </button>`
        });
    else
        return false;

});


$('button#btnShowAddAppointmentModal').on('click', function() {
    $("select#company").val('');
    $("select#company").selectpicker("refresh");
    $("select#guest").selectize()[0].selectize.clear();
    // $('input[id="title"]').val('');
    $('textarea[id="description"]').val('');
    $('input[name="datetimes"]').val('');
    $('div#error-appointment-message').addClass('hidden');

  
    $('input[name="datetimes"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(1, 'hour'),
        autoApply: true,
        locale: {
            format: 'MM/DD/Y hh:mm A',
            firstDay: 1
        },
        minDate: moment()
    });
});

$('button#btnShowEditAppointmentModal').on('click', function() {
    var appointment_id = $(this).attr('data-appointment'),
        company = $('input[id="update_company"]'),
        // title = $('input[id="update_title"]'),
        client = $('input[id="update_client"]'),
        description = $('textarea[id="update_description"]'),
        status = $('select#update_status'),
        statusOption = $('select#update_status option');

    company.val('');
    // title.val('');
    description.val('');
    client.val('');
    status.selectpicker('refresh');
    statusOption.removeAttr('selected');
    $('div#error-update-appointment-message').addClass('hidden');
   
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: $('base').attr('href') + '/appointment/edit/' + appointment_id,
        success: function(data) {
                company.val(data.appointment.company);
                // title.val(data.appointment.title);
                description.val(data.appointment.description); 
                client.val(data.appointment.client_name);
                
                status.ready(function () {
                    $.each(statusOption, function(i, j) {
                        if(data.appointment.status == j.value) {
                            status.find('option[value="'+data.appointment.status+'"]').attr('selected', 'selected');
                            $.each(data.status, function(k, l) {
                                if(i == k) {
                                    $('button[data-id="update_status"]').attr('title', l.systemdesc);
                                    $('button[data-id="update_status"] span.filter-option.pull-left').text(l.systemdesc);
                                }
                            });
                        }   
                    });
                });

                $('input[name="update_datetimes"]').daterangepicker({
                        timePicker: true,
                        startDate: new Date(data.appointment.date_start),
                        endDate: new Date(data.appointment.date_end),
                        locale: {
                            format: 'MM/DD/Y hh:mm A',
                            firstDay: 1
                        },
                        minDate: moment()
                });

                $('button#btnUpdateAppointment').attr('data-appointment', data.appointment.id);
        },
        error: function(data) {
            console.log('Error: ' + data);
        }
    });
});

$('button#btnUpdateAppointment').on('click', function() {
    var appointment_id = $(this).attr('data-appointment'),
        company = $('input[id="update_company"]'),
        client = $('input[id="update_client"]'),
        description = $('textarea[id="update_description"]'),
        status = $('select#update_status option:selected'),
        color = $('input[name="update_color"]:checked'),
        dates = $('input[id="update_datetimes"]'),
        tableRowAppointment = $('tr[data-appointment="'+appointment_id+'"]');

    if(dates.val() == '') {
        $('div#error-update-appointment-message').removeClass('hidden');
        return false;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var form = {
        _token: CSRF_TOKEN,
        id: appointment_id,
        description: description.val(),
        client_name: client.val(),
        status: status.val(),
        color: color.val(),
        date_start: dates.data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss'),
        date_end: dates.data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss')
    };

    $.ajax({
        url: $('base').attr('href') + '/appointment/update/' + appointment_id,
        data: form,
        dataType: 'json',
        method: 'put',
        beforeSend: function() {
            loading()
        },
        success: function(data) {
            // console.log(data);
            loaded();
            var markup = '<tr class="bg-warning" data-appointment="'+data.appointment.id+'"><td class="text-center dates text-nowrap">'+ data.appointment.scheduled_date +'</td>' +
                            '<td class="text-center company">'+ data.appointment.company +'</td>' +
                            '<td class="text-center client">'+ data.appointment.client + '</td>' +
                            '<td class="text-center description">' + data.appointment.description + '</td>' +
                            '<td class="text-center services">' + data.appointment.services + '</td>' +
                            '<td class="text-center guest">' + data.appointment.guest + '</td>' +
                            '<td class="text-center created">' + data.appointment.created_by + '</td>' +
                            '<td class="text-center status">' + data.appointment.status + '</td>' +
                            '<td class="text-center action text-nowrap">' +
                                // '<button class="btn btn-primary" id="btnShowAppointmentModal" data-appointment="'+ data.appointment.id +'">' +
                                //     '<i class="fa fa-eye"></i>' +
                                // '</button> ' +
                                (data.appointment.status == 'Pending' || data.appointment.status == 'Ongoing' ? '<button data-toggle="modal" data-target="#update_appointment_model" class="btn btn-info" id="btnShowEditAppointmentModel" ' +
                                    'data-appointment="'+ data.appointment.id +'"><i class="fa fa-edit"></i>' +
                                        '</button> ' : 'No action') +
                            '</td></tr>';

            tableRowAppointment.replaceWith(markup);
            
            setInterval(function() {
                $('tr[data-appointment="'+data.appointment.id+'"]').removeClass('bg-warning', 'slow');
            }, 8000);
            

            $('div#update_appointment_model').modal('hide');
            swal({
                type: 'success',
                title: 'Success...',
                text: 'The appointment ' + data.appointment.company + ' has been updated'
              });
        },
        error: function (data) {
            console.log('Error:', data.responseText);
        }
    });

});

$('button#btnCreateAppointment').on('click', function() {
    
    var company = $('select#company option:selected'),
        guest = $('select#guest'),
        description = $('textarea[id="description"]'),
        dates = $('input[id="datetimes"]'),
        color = $('input[name="color"]:checked');

    if(company.val() == '' || dates.val() == '') 
    {
        $('div#error-appointment-message').removeClass('hidden');
        return false;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var form = {
        _token: CSRF_TOKEN,
        lead_id: company.val(),
        guest: guest.val(),
        description: description.val(),
        date_start: dates.data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss'),
        date_end: dates.data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss'),
        color: color.val()
    };

    $.ajax({
        url: $('base').attr('href') + '/appointment/create',
        data: form,
        dataType: 'json',
        type: 'get',
        beforeSend: function() {
            loading()
        }, 
        success: function(data) {
            loaded();
            $('div#add_appointment_model').modal('hide');
            swal({
                type: 'success',
                title: 'Success...',
                text: 'The appointment ' + data.appointment.company + ' has been added'
            }).then((result) => {
                if (result.value) {
                    window.location.href = '/appointment/list';
                }
            });
        },
        error: function (data) {
            console.log('Error:', data.responseText);
        }
    });
});

$(document).on('click', 'button[data-target="#quick_add_leads"]', function() {
    $('input#lead_company').val($(this).data('search-result'));
});

$('button#btnCreateLead').on('click', function() {
    var company = $('input#lead_company'),
        name = $('input#lead_client_name'),
        contact = $('input#lead_contact'),
        email = $('input#lead_email'),
        designation = $('input#lead_designation'),
        office_number = $('input#lead_office_number'),
        street_address = $('textarea#lead_address'),
        zip_code = $('input#lead_postal'),
        city = $('input#lead_city'),
        website = $('input#lead_website'),
        services = $('select#lead_services'),
        other_services = $('input#lead_other_services'),
        source = $('select#lead_source'),
        landing = $('select#lead_landing'),
        status = $('select#lead_status');

    if (company.val() == '' || email.val().match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/) === null) {
        $('div#error-create-lead-message').removeClass('hidden');
        return false
    }

    $('div#error-create-lead-message').addClass('hidden');

    var client_name = name.val().split(' '),
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'),
        services_arr = services.val().join('|');

    var form = { 
        _token: CSRF_TOKEN, company: company.val(), f_name: (client_name[0] ? client_name[0] : null), l_name: (client_name[1] ? client_name[1] : null), contact_number: contact.val(), email: email.val(),
        designation: designation.val(), office_number: office_number.val(), street_address: street_address.val(), zip_code: zip_code.val(), city: city.val(), website: website.val(),
        services: services_arr, other_services: other_services.val(), source: source.val(), landing_page: landing.val(), status: status.val()
    };

    $.ajax({
        url: $('base').attr('href') + '/appointment/quick/create/lead',
        type: 'post',
        dataType: 'json',
        data: form,
        beforeSend: function() {
            loading()
        },
        success: function(data) {
            loaded();
            $('div#quick_add_leads').modal('hide');

            $('ul[role="listbox"] li:last-child').next().html(`
                <li data-original-index="${$(this).data('original-index') + 1}" class="">
                    <a tabindex="0" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                        <span class="text">${data.lead.company}</span><span class="glyphicon glyphicon-ok check-mark"></span>
                    </a>
                </li>
            `);

            swal({
                type: 'success',
                title: 'Success...',
                text: 'The lead ' + data.lead.company + ' has been created'
            });

            // Append the newly created company
            $('select#company').append(`
                <option value="${data.lead.id}">${data.lead.company}</option>
            `);

            $('#msg-quick-add').removeClass('hidden').html(`<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> <i>${data.lead.company}</i> has been added`);

            // Refresh the Company dropdown selection
            $("select#company").val('');
            $("select#company").selectpicker("refresh");
            

            company.val('');
            name.val('');
            contact.val('');
            email.val('');
            designation.val('');
            office_number.val('');
            street_address.val('');
            zip_code.val('');
            city.val('');
            website.val('');
            services.val('');
            services.multiselect('refresh');
            other_services.val('');
            source.val('');
            source.val('');
            source.selectpicker('refresh');
            landing.val('');
            landing.selectpicker('refresh');
            status.val('');
            status.selectpicker('refresh');
        },
        error: function(error) {
            console.log('Error data: ' + error)
        }
    });
});
