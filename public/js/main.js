$(document).ready( function() {

// -------------- Placeholder Jquery

$('input, textarea').placeholder();

// -------------- Reject IE Jquery

$.reject({

  reject : {
      all: false,
      msie: 9
  },
  display: ['firefox','chrome','safari'],
  imagePath: 'assets/images/'

});

// -------------- New Jquery

$('#accMulti').multiselect({
  enableClickableOptGroups: true
});

});
