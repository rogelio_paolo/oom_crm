
(function(){
    let root = $('base').attr('href'),
        uri = window.location.href;

    $(document).ready(function(){
        let branch = $('select#branch option:selected').val();

        if (branch === 'sg'){
            $('div.sg-bank-info').css('display','block');
            $('div.ph-benefits').css('display','none');
            $('div.ph-name').css('display','none');
        }
        else{
            $('div.ph-benefits').css('display','block');
            $('div.sg-bank-info').css('display','none');
            $('div.ph-name').css('display','block');
        }

    });

    $('select#branch').on('change', function(){
        let branch = this.value;

        if (branch === 'sg'){
            $('div.sg-bank-info').css('display','block');
            $('div.ph-benefits').css('display','none');
            $('div.ph-name').css('display','none');
        }
        else{
            $('div.ph-benefits').css('display','block');
            $('div.sg-bank-info').css('display','none');
            $('div.ph-name').css('display','block');
        }
    });

    $(document).on('click','a#employeeDelete', function(){
        let empid = $(this).data('employeeid');
        $('#delete-employee').val(empid);
        $('#deleteEmployeeModal').modal('show');
    });

    if (uri.indexOf('/employee/edit') < 0){
        $(document).ready(function(){
            let branch = $('select#branch option:selected').val();

            $.ajax({
                url : root + '/employee/new/' + branch,
                success:function(empid){
                    $('input[id="emp_id"]').val(empid);
                }
            })
        });

        $('select#branch').on('change',function(){
            let branch = this.value;

            $.ajax({
                url : root + '/employee/new/' + branch,
                success:function(empid){
                    $('input[id="emp_id"]').val(empid);
                }
            })
        });
    }

})();
