
(function(){
    let root = $('base').attr('href');

    $(document).on('click','a#roleAdd',function(e){
        e.preventDefault();
        $('#addRoleModal').modal('show');
    });

    $(document).on('click','a#roleEdit',function(){
        let roleid = $(this).data('roleid');

        $.ajax({
            url: root + '/role/get/'+roleid,
            success:function(data){
                $('input[id="roleid"]').val(data.roleid);
                $('input[id="role"]').val(data.role);

                let activeflag = data.isActive === 1 ? 'checked' : '',
                    isActive = '<input type="checkbox" name="role-active" value="Y" '+activeflag+'>is Active?';
                $('#isActive').html(isActive);
            }
        })

        $('#editRoleModal').modal('show');
    });

    $(document).on('click','a#addManager',function(e){
        e.preventDefault();
        $('#addManagerModal').modal('show');
    });

    $(document).on('click','a#deleteManager',function(e){
        var manager = $(this).data('manager'),
            delManager = $('input[id="delete-manager"]');
        
        e.preventDefault();
        delManager.val(manager);
        $('#deleteManagerModal').modal('show');
    });

})();
