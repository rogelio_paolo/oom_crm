
function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
    if(!loader.hasClass('hidden')) {
        window.setTimeout(function () {
            if (!loader.hasClass('hidden')) {
                loader.fadeTo(500, 0).slideUp(500, function () {
                    $(this).addClass('hidden');
                    $(this).removeAttr('style');
                });
            }
        }, 500);
    }
}

function pagination(pagination,userRow,route){

    let baseUrl = $('base').attr('href'),
        // queryExist = $('#paginationQuery').val(),
        // route = queryExist !== '' ? queryUrl+'?page=' : (queryUrl === '/home' ? baseUrl+queryUrl+'?page=' : queryUrl+'?page=');
        url = baseUrl + route;

        let fpage = pagination.current_page === 1 ? "<li class='disabled'><a href='"+route+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#user-management > tbody');
        loading();
        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        table.hide('fast');
        table.html(userRow).fadeIn('fast');

}

function getPagination(url) {
    $.ajax({
        url:url,
        cache:false,
        type:'GET',
        dataType:'JSON',
        beforeSend: function() {
            loading();
        },
        success:function(data){

            let userRow = "",
                baseUrl = $('base').attr('href');


            $.each(data.users, function (i, j){
                userRow += "<tr>";
                userRow += "<td class='text-center'>"+j.username+"</td>";
                userRow += "<td class='text-center'>"+j.role+"</td>";
                userRow += "<td class='text-center'>"+j.date+"</td>";
                userRow += "<td class='text-center' id='user-management'> " +
                "<a href='/user/profile/"+j.userid+"' class='btn btn-default'><i class='fa fa-search'></i></a> " +
                "<a id='userEdit' data-userid="+j.userid+" class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                "<a id='userDelete' data-userid="+j.userid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                "<a id='userRole' data-userid="+j.userid+" class='btn btn-primary'><i class='fa fa-wrench'></i></a></td>";
                userRow += "</tr>";
            });

            let route = '/dashboard?page='
            pagination(data.pagination,userRow,route);
        }
    });
}

function sortUser(url){
    let userRow = "";

    $.ajax({
        url: url,
        success:function(data){
            $.each(data.result,function(i,j){
                userRow += "<tr>";
                userRow += "<td class='text-center'>"+j.username+"</td>";
                userRow += "<td class='text-center'>"+j.role+"</td>";
                userRow += "<td class='text-center'>"+j.date+"</td>";
                userRow += "<td class='text-center' id='user-management'> " +
                "<a id='userEdit' data-userid="+j.userid+" class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                "<a id='userDelete' data-userid="+j.userid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                "<a id='userRole' data-userid="+j.userid+" class='btn btn-primary'><i class='fa fa-wrench'></i></a></td>";
                userRow += "</tr>";
            });

            let hasPage = url.indexOf('?page=') >= 1 ? url.replace(/([&\?]key=val*$|key=val&|[?&]key=val(?=#))/, '') : '?page=';
                route = url.replace($('base').attr('href'),'')+hasPage;
            pagination(data.pagination,userRow,route);
            $('ul.pagination').show('fast');
        }
    });
}

function searchUser(){
    let user = $('input[id="searchUser"]').val(),
        key = user === '' ? 'null' : user;

    if (user === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/admin/usersearch/' + key,
        success:function(data){
            let userRow = "",
                tblRow = $('table#user-management > tbody');

            if ((data.result).length > 0){
                $.each(data.result,function(i,j){
                    userRow += "<tr>";
                    userRow += "<td class='text-center'>"+j.username+"</td>";
                    userRow += "<td class='text-center'>"+j.role+"</td>";
                    userRow += "<td class='text-center'>"+j.date+"</td>";
                    userRow += "<td class='text-center' id='user-management'> " +
                    "<a id='userEdit' data-userid="+j.userid+" class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                    "<a id='userDelete' data-userid="+j.userid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                    "<a id='userRole' data-userid="+j.userid+" class='btn btn-primary'><i class='fa fa-wrench'></i></a></td>";
                    userRow += "</tr>";
                });
                let route = '/admin/usersearch/'+user+'?page=';
                pagination(data.pagination,userRow,route);
                $('ul.pagination').show('fast');
            }
            else{
            userRow = "<tr><td colspan='5' class='text-center'>No results found for "+user+"</td></tr>";

            tblRow.html(userRow).fadeIn('fast');
            $('ul.pagination').hide('fast');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchUser();
    }
}

(function(){
    let root = $('base').attr('href');

    $(document).on('click', 'a#userAdd', function(){
        $('#addUserModal').modal('show');
    });

    $(document).on('click', 'div.modal-footer > button.btn-danger', function(){
        $('#firstname').val('');
        $('#lastname').val('');
        $('#username').val('');
        $('#email').val('');
    });

    let table = $('table#user-management > tbody');

    $(document).on('click','a#userEdit', function(){
        let user = $(this).data('userid'),
            editUser  = $('#userid'),
            editFname = $('#firstname'),
            editLname = $('#lastname'),
            editUname = $('#username'),
            editEmail = $('#email');

        $.ajax({
            url: root + '/admin/edit/' + user,
            success:function(data){
                editUser.val(data.userid);
                editFname.val(data.firstname);
                editLname.val(data.lastname);
                editUname.val(data.username);
                editEmail.val(data.email);

            }
        })

        $('#editUserModal').modal('show');
    });

    $(document).on('click','a#userDelete', function(){
        let user = $(this).data('userid');
        $('#delete-userid').val(user);
        $('#deleteUserModal').modal('show');
    });

    $(document).on('click','a#userRole', function(){
        let empid = $(this).data('empid'),
            roles = "";

        $('#emp_id').val(empid);

        $.ajax({
            url: root + '/admin/role/get/' + empid,
            success:function(data){
                $.each(data.roles, function(i,j){
                    roles += "<option value='"+j.systemcode+"' "+(data.role === j.systemcode ? 'selected' : '')+">"+j.systemdesc+"</option>";
                });

                $('#userRole').html(roles);

                if((data.access).length > 0)
                    $.each(data.access, function(a,b){
                        let access = "";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='C' "+((b.access).indexOf('C') >=0 ? 'checked' : '')+">Add</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='R' "+((b.access).indexOf('R') >=0 ? 'checked' : '')+">View</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='U' "+((b.access).indexOf('U') >=0 ? 'checked' : '')+">Edit</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='D' "+((b.access).indexOf('D') >=0 ? 'checked' : '')+">Delete</label>";

                        $('#'+b.module_name+'-module').html(access);
                    });
            }
        })

        $('#userRoleModal').modal('show');

    });

    $('#userRoleModal').on('shown.bs.modal', function(){
        let role = $('select#userRole option:selected').val();

        if (role === 'RL004'){
            $('div.employees-module').css('display','none');
            $('div.accounts-module').css('display','block');
        }
        else if (role === 'RL002'){
            $('div.employees-module').css('display','block');
            $('div.accounts-module').css('display','block');
        }
        else if (role === 'RL003'){
            $('div.accounts-module').css('display','none');
            $('div.employees-module').css('display','block');
        }
        else{
            $('div.employees-module').css('display','block');
            $('div.accounts-module').css('display','block');
        }
    });

    $('select#userRole').on('change', function(){
          let role = this.value;

          if (role === 'RL004'){
              $('div.employees-module').css('display','none');
              $('div#employees-module').html('');
              $('div.accounts-module').css('display','block');
          }
          else if (role === 'RL003'){
              $('div.accounts-module').css('display','none');
              $('div#accounts-module').html('');
              $('div.employees-module').css('display','block');
          }
          else{
              $('div.employees-module').css('display','block');
              $('div.accounts-module').css('display','block');
          }
    });

    $(document).on('click', 'button.search-user', function(){
        searchUser();
    });

    $(document).on('click','a.sort-user', function(e){
        e.preventDefault();
        let currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        if (currOrder.indexOf('desc') >= 1)
            $('i#sort-user').each(function(){
                $(this).removeClass('fa-sort-alpha-desc');
                $(this).addClass('fa-sort-alpha-asc');
            });
        else
            $('i#sort-user').each(function(){
                $(this).removeClass('fa-sort-alpha-asc');
                $(this).addClass('fa-sort-alpha-desc');
            });

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'desc' : 'asc';

        let url = root + '/admin/order/' + sortBy + '/' + sortOrder;

        sortUser(url);
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        let link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('dashboard') >= 1)
            getPagination(url);
        else if(url.indexOf('usersearch') >= 1)
            searchUser();
        else
            sortUser(url);
    });

    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        let url = $('base').attr('href') + '/dashboard?page=1';
        getPagination(url);
        $('#searchUser').val('');
    });

    $(document).on('click', 'a#user-role', function(e){
        e.preventDefault();
    });

})();
