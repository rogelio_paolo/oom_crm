
(function(){

    $(document).on('change','select#nature',function(){
        let nature = $(this).val();

        if ($.inArray('NP001',nature) === -1){
            $('div.sem-included').addClass('hidden');
            $('input[id="sem_included"]').val('no');
        }
        else{
            $('div.sem-included').removeClass('hidden');
            $('input[id="sem_included"]').val('yes');
        }


        if ($.inArray('NP002',nature) === -1){
            $('div.fb-included').addClass('hidden');
            $('input[id="fb_included"]').val('no');
        }
        else{
            $('div.fb-included').removeClass('hidden');
            $('input[id="fb_included"]').val('yes');
        }

        if ($.inArray('NP003',nature) === -1){
            $('div.seo-included').addClass('hidden');
            $('input[id="seo_included"]').val('no');
        }
        else{
            $('div.seo-included').removeClass('hidden');
            $('input[id="seo_included"]').val('yes');
        }

    });


})();
