let root = $('base').attr('href');

function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
    if(!loader.hasClass('hidden')) {
        window.setTimeout(function () {
            if (!loader.hasClass('hidden')) {
                loader.fadeTo(500, 0).slideUp(500, function () {
                    $(this).addClass('hidden');
                    $(this).removeAttr('style');
                });
            }
        }, 500);
    }
}

function pagination(pagination,empRow){

    let baseUrl = $('base').attr('href'),
        // queryExist = $('#paginationQuery').val(),
        // route = queryExist !== '' ? queryUrl+'?page=' : (queryUrl === '/home' ? baseUrl+queryUrl+'?page=' : queryUrl+'?page=');
        url = pagination.path + '?page=';

        let fpage = pagination.current_page === 1 ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#employee-management > tbody');

        loading();
        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        table.hide('fast');
        table.html(empRow).fadeIn('fast');

}

function getPagination(url) {
    let empRow = "",
        root = $('base').attr('href');

    $.ajax({
        url:url,
        success:function(data){

            $.each(data.employees, function (i, j){
                empRow += "<tr>";
                empRow += "<td class='text-center'>"+j.emp_id+"</td>";
                empRow += "<td class='text-center'>"+j.name+"</td>";
                empRow += "<td class='text-center'>"+j.designation+"</td>";
                empRow += "<td class='text-center'>"+j.status+"</td>";
                empRow += "<td class='text-center'>"+j.role+"</td>";
                empRow += "<td class='text-center'>"+j.date+"</td>";
                empRow += "<td class='text-center' id='user-management'> ";

                $.each(data.access, function(k, v){
                    if (v.module !== 'MD003'){
                        $.noop();
                    }
                    else{
                        empRow += (v.access).indexOf('U') >= 0 ? '<a href="'+root+'/employee/edit/'+j.emp_id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '';
                        empRow += (v.access).indexOf('D') >= 0 ? '<a href id="employeeDelete" data-employeeid="'+j.emp_id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a> ' : '';
                        empRow += data.role === 1 ? '<a id="userRole" data-employeeid="'+j.emp_id+'" class="btn btn-primary"><i class="fa fa-wrench"></i></a>' : '';
                    }
                });

                empRow += "</tr>";
            });

            pagination(data.pagination,empRow);
        }
    });
}

function sortEmployee(url){
    let empRow = "";

    $.ajax({
        url: url,
        success:function(data){
            $.each(data.employees,function(i,j){
                empRow += "<tr>";
                empRow += "<td class='text-center'>"+j.emp_id+"</td>";
                empRow += "<td class='text-center'>"+j.name+"</td>";
                empRow += "<td class='text-center'>"+j.designation+"</td>";
                empRow += "<td class='text-center'>"+j.status+"</td>";
                empRow += "<td class='text-center'>"+j.role+"</td>";
                empRow += "<td class='text-center'>"+j.date+"</td>";
                empRow += "<td class='text-center' id='user-management'> ";
                $.each(data.access, function(k, v){
                    if (v.module !== 'MD003'){
                        $.noop();
                    }
                    else{
                        empRow += (v.access).indexOf('U') >= 0 ? '<a href="'+root+'/employee/edit/'+j.emp_id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '';
                        empRow += (v.access).indexOf('D') >= 0 ? '<a href id="employeeDelete" data-employeeid="'+j.emp_id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a> ' : '';
                        empRow += data.role === 1 ? '<a id="userRole" data-employeeid="'+j.emp_id+'" class="btn btn-primary"><i class="fa fa-wrench"></i></a>' : '';
                    }
                });
                empRow += "</tr>";
            });

            pagination(data.pagination,empRow);
            $('ul.pagination').show('fast');
        }
    });
}

function searchEmployee(){
    let employee = $('input[id="searchEmployee"]').val(),
        key = employee === '' ? 'null' : employee;

    if (employee === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/employee/search/' + key,
        success:function(data){
            let empRow = "",
                tblRow = $('table#employee-management > tbody');

            if ((data.employees).length > 0){
                $.each(data.employees,function(i,j){
                    empRow += "<tr>";
                    empRow += "<td class='text-center'>"+j.emp_id+"</td>";
                    empRow += "<td class='text-center'>"+j.name+"</td>";
                    empRow += "<td class='text-center'>"+j.designation+"</td>";
                    empRow += "<td class='text-center'>"+j.status+"</td>";
                    empRow += "<td class='text-center'>"+j.role+"</td>";
                    empRow += "<td class='text-center'>"+j.date+"</td>";
                    empRow += "<td class='text-center' id='user-management'> ";
                    $.each(data.access, function(k, v){
                        if (v.module !== 'MD003'){
                            $.noop();
                        }
                        else{
                            empRow += (v.access).indexOf('U') ? '<a href="'+root+'/employee/edit/'+j.emp_id+'" class="btn btn-warning"><i class="fa fa-pencil"></i></a> ' : '';
                            empRow += (v.access).indexOf('D') ? '<a href id="employeeDelete" data-employeeid="'+j.emp_id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a> ' : '';
                            empRow += data.role === 1 ? '<a id="userRole" data-employeeid="'+j.emp_id+'" class="btn btn-primary"><i class="fa fa-wrench"></i></a>' : '';
                        }
                    });
                    empRow += "</tr>";
                });

                pagination(data.pagination,empRow);
                $('ul.pagination').show('fast');
            }
            else{
            empRow = "<tr><td colspan='5' class='text-center'>No results found for "+employee+"</td></tr>";

            tblRow.html(empRow).fadeIn('fast');
            $('ul.pagination').hide('fast');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchEmployee();
    }
}

(function(){
    let root = $('base').attr('href');

    $(document).ready(function(){
        let branch = $('select#branch option:selected').val();

        if (branch === 'sg')
            $('div.ph-benefits').css('display','none');
        else
            $('div.ph-benefits').css('display','block');
    });

    $('select#branch').on('change', function(){
        let branch = this.value;

        if (branch === 'sg')
            $('div.ph-benefits').css('display','none');
        else
            $('div.ph-benefits').css('display','block');
    });

    $(document).on('click','a#employeeDelete', function(e){
        e.preventDefault();

        let empid = $(this).data('employeeid');
        $('#delete-employee').val(empid);
        $('#deleteEmployeeModal').modal('show');
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        let link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('list') >= 1)
            getPagination(url);
        else if(url.indexOf('usersearch') >= 1)
            searchEmployee();
        else
            sortEmployee(url);
    });

    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        let url = $('base').attr('href') + '/employee/list';
        getPagination(url);
        $('#searchEmployee').val('');
    });

    $(document).on('click', 'a#user-role', function(e){
        e.preventDefault();
    });

    $(document).on('click', 'button.search-employee', function(){
        searchEmployee();
    });

    $(document).on('click','a.sort-employee', function(e){
        e.preventDefault();
        let currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        if (currOrder.indexOf('desc') >= 1)
            $('i#sort-employee').each(function(){
                $(this).removeClass('fa-sort-alpha-desc');
                $(this).addClass('fa-sort-alpha-asc');
            });
        else
            $('i#sort-employee').each(function(){
                $(this).removeClass('fa-sort-alpha-asc');
                $(this).addClass('fa-sort-alpha-desc');
            });

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'desc' : 'asc';

        let url = root + '/employee/sort/' + sortBy + '/' + sortOrder;

        sortEmployee(url);
    });

    $(document).on('click','a#userRole', function(){
        let empid = $(this).data('employeeid'),
            roles = "";

        $('#emp_id').val(empid);

        $.ajax({
            url: root + '/admin/role/get/' + empid,
            success:function(data){
                $.each(data.roles, function(i,j){
                    roles += "<option value='"+j.systemcode+"' "+(data.role === j.systemcode ? 'selected' : '')+">"+j.systemdesc+"</option>";
                });

                $('#userRole').html(roles);

                if((data.access).length > 0)
                    $.each(data.access, function(a,b){
                        let access = "";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='C' "+((b.access).indexOf('C') >=0 ? 'checked' : '')+">Add</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='R' "+((b.access).indexOf('R') >=0 ? 'checked' : '')+">View</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='U' "+((b.access).indexOf('U') >=0 ? 'checked' : '')+">Edit</label>";
                        access += "<label class='checkbox-inline'>" +
                                  "<input type='checkbox' name='"+b.module_name+"-module[]' value='D' "+((b.access).indexOf('D') >=0 ? 'checked' : '')+">Delete</label>";

                        $('#'+b.module_name+'-module').html(access);
                    });
            }
        })

        $('#userRoleModal').modal('show');

    });

    $('#userRoleModal').on('shown.bs.modal', function(){
        let role = $('select#userRole option:selected').val();

        if (role === 'RL004'){
            $('div.employees-module').addClass('hidden');
            $('div.accounts-module').removeClass('hidden');
        }
        else if (role === 'RL002'){
            $('div.employees-module').removeClass('hidden');
            $('div.accounts-module').removeClass('hidden');
        }
        else if (role === 'RL003'){
            $('div.accounts-module').addClass('hidden');
            $('div.employees-module').removeClass('hidden');
        }
        else{
            $('div.employees-module').removeClass('hidden');
            $('div.accounts-module').removeClass('hidden');
        }
    });

    $('select#userRole').on('change', function(){
          let role = this.value;

          if (role === 'RL004'){
              $('div.employees-module').addClass('hidden');
              $('div.accounts-module').removeClass('hidden');
          }
          else if (role === 'RL003'){
              $('div.accounts-module').addClass('hidden');
              $('div.employees-module').removeClass('hidden');
          }
          else{
              $('div.employees-module').removeClass('hidden');
              $('div.accounts-module').removeClass('hidden');
          }
    });


})();
