function loading(){
    var loader = $('#loader');
    loader.removeClass('hidden');
    if(!loader.hasClass('hidden')) {
        window.setTimeout(function () {
            if (!loader.hasClass('hidden')) {
                loader.fadeTo(500, 0).slideUp(500, function () {
                    $(this).addClass('hidden');
                    $(this).removeAttr('style');
                });
            }
        }, 500);
    }
}

function pagination(pagination,leadRow,route){

    let baseUrl = $('base').attr('href'),
        // queryExist = $('#paginationQuery').val(),
        // route = queryExist !== '' ? queryUrl+'?page=' : (queryUrl === '/home' ? baseUrl+queryUrl+'?page=' : queryUrl+'?page=');
        url = pagination.path + '?page=';
        console.log(leadRow);
        let fpage = (pagination.current_page === 1) ? "<li class='disabled'><a href='"+url+"1'>&laquo;</a></li>" : "<li><a href='"+url+"1'>&laquo;</a></li>",
            lpage = pagination.current_page === pagination.last_page ? "<li class='disabled'><a href='"+url+pagination.last_page+"'>&raquo;</a></li>" : "<li><a href='"+url+pagination.last_page+"'>&raquo;</a></li>",
            table = $('table#lead-management > tbody');

        loading();
        if(pagination.last_page > 1){
            $('ul.pagination').removeClass('hidden');
            //$('ul.pagination > div > p').html(totalEntries);
            var links = "";
            for (var a = 1; a <= pagination.last_page; a++) {
                var half_total_links = Math.floor(7 / 2),
                    from = pagination.current_page - half_total_links,
                    to = pagination.current_page + half_total_links;

                if (pagination.current_page < half_total_links) {
                    to += half_total_links - pagination.current_page;
                }
                if (pagination.last_page - pagination.current_page < half_total_links) {
                    from -= half_total_links - (pagination.last_page - pagination.current_page) - 1;
                }

                if (from < a && a < to){
                    var isActive = pagination.current_page === a ? 'active' : '';
                    links += '<li class='+isActive+'><a href='+url+a+'>'+a+'</a></li> ';
                }
            }
            $('ul.pagination').html(fpage+' '+links+' '+lpage);
            $('ul.pagination').removeClass('hidden');
        }
        else{
            $('ul.pagination').addClass('hidden');
        }
        table.hide();
        table.html(leadRow).fadeIn('fast');
}

function getPagination(uri) {
    let leadrow = "";

    $.ajax({
        url:uri,
        success:function(data){
            $.each(data.leads, function (i, j){
                leadrow += "<tr>";
                leadrow += "<td class='text-center'>"+j.name+"</td>";
                leadrow += "<td class='text-center'>"+j.email+"</td>";
                leadrow += "<td class='text-center'>"+j.cno+"</td>";
                leadrow += "<td class='text-center'>"+j.comp+"</td>";
                leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                leadrow += "<td class='text-center'>"+j.date+"</td>";
                leadrow += "<td class='text-center' id='lead-management'> " +
                "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                "<a id='leadConvert' data-leadid="+j.leadid+" class='btn btn-primary'><i class='fa fa-exchange'></i></a></td>";
                leadrow += "</tr>";
            });

            let route = window.location.href;
            pagination(data.pagination,leadrow,route);
        }
    });
}

function sortLead(url){
    let leadrow = "";

    $.ajax({
        url: url,
        success:function(data){
            $.each(data.result,function(i,j){
                leadrow += "<tr>";
                leadrow += "<td class='text-center'>"+j.name+"</td>";
                leadrow += "<td class='text-center'>"+j.email+"</td>";
                leadrow += "<td class='text-center'>"+j.cno+"</td>";
                leadrow += "<td class='text-center'>"+j.comp+"</td>";
                leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                leadrow += "<td class='text-center'>"+j.date+"</td>";
                leadrow += "<td class='text-center' id='lead-management'> " +
                "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                "<a id='leadConvert' data-leadid="+j.leadid+" class='btn btn-primary'><i class='fa fa-exchange'></i></a></td>";
                leadrow += "</tr>";
            });

            // let hasPage = url.indexOf('?page=') >= 1 ? url.replace(/([&\?]key=val*$|key=val&|[?&]key=val(?=#))/, '') : '?page=';
            //     route = url.replace($('base').attr('href'),'')+hasPage;
            let route = '/lead/sort/' + data.field + '/' + data.order;
            pagination(data.pagination,leadrow,route);
            $('ul.pagination').show('fast');
        }
    });
}

function searchLead(){
    let lead = $('input[id="searchLead"]').val(),
        key = lead === '' ? 'null' : lead;

    if (lead === '')
        return false;

    $.ajax({
        url: $('base').attr('href') + '/lead/search/' + key,
        success:function(data){
            let leadrow = "",
                tblRow = $('table#lead-management > tbody');

            if ((data.result).length > 0){
                $.each(data.result,function(i,j){
                    leadrow += "<tr>";
                    leadrow += "<td class='text-center'>"+j.name+"</td>";
                    leadrow += "<td class='text-center'>"+j.email+"</td>";
                    leadrow += "<td class='text-center'>"+j.cno+"</td>";
                    leadrow += "<td class='text-center'>"+j.comp+"</td>";
                    leadrow += "<td class='text-center'>"+j.leadstatus+"</td>";
                    leadrow += "<td class='text-center'>"+j.date+"</td>";
                    leadrow += "<td class='text-center' id='lead-management'> " +
                    "<a href='/lead/edit/"+j.leadid+"' class='btn btn-warning'><i class='fa fa-pencil'></i></a> " +
                    "<a id='leadDelete' data-leadid="+j.leadid+" class='btn btn-danger'><i class='fa fa-trash'></i></a> " +
                    "<a id='leadConvert' data-leadid="+j.leadid+" class='btn btn-primary'><i class='fa fa-exchange'></i></a></td>";
                    leadrow += "</tr>";
                });

                let route = '/lead/search/'+lead+'?page=';
                pagination(data.pagination,leadrow,route);
                $('ul.pagination').show('fast');
            }
            else{
            leadrow = "<tr><td colspan='7' class='text-center'>No results found for "+lead+"</td></tr>";

            tblRow.html(leadrow).fadeIn('fast');
            $('ul.pagination').hide('fast');
            }
        }
    });
}

function triggerSearch(e){
    if(e.keyCode === 13){
        e.preventDefault();
        searchLead();
    }
}

(function(){

    let root = $('base').attr('href');

    $('a#leadDelete').on('click', function(){
        let lead_id = $(this).data('leadid');


    });

    $(document).on('click','a#leadDelete', function(){
        let leadid = $(this).data('leadid');

        $('#delete-leadid').val(leadid);
        $('#deleteLeadModal').modal('show');
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();

        let link = $(this).parent(),
            siblings = link.siblings(),
            url = $(this).attr('href');

        if(siblings.hasClass('active')){
            siblings.removeClass('active');
            link.addClass('active');
        }

        if  (url.indexOf('list') >= 1)
            getPagination(url);
        else if(url.indexOf('leadsearch') >= 1)
            searchLead();
        else
            sortLead(url);

    });

    $(document).on('click', 'button.search-lead', function(){
        searchLead();
    });

    $(document).on('click', 'a#refresh-table', function(e){
        e.preventDefault();
        let url = $('base').attr('href') + '/lead/list?page=1';
        getPagination(url);
        $('#searchLead').val('');
    });

    $(document).on('click','a.sort-lead', function(e){
        e.preventDefault();
        let currOrder = $(this).find('i').attr('class'),
            sortOrder = "",
            sortBy = $(this).data('sortby');

        if (currOrder.indexOf('desc') >= 1)
            $('i#sort-lead').each(function(){
                $(this).removeClass('fa-sort-alpha-desc');
                $(this).addClass('fa-sort-alpha-asc');
            });
        else
            $('i#sort-lead').each(function(){
                $(this).removeClass('fa-sort-alpha-asc');
                $(this).addClass('fa-sort-alpha-desc');
            });

        sortOrder = currOrder.indexOf('desc') >= 1 ? 'desc' : 'asc';

        let url = root + '/lead/sort/' + sortBy + '/' + sortOrder;

        sortLead(url);
    });

})();
