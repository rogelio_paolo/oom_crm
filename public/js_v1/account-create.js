
// Activate Next Step

$(document).on('change','select#nature', function(){

    let nature = $(this).val();

    if ($.inArray('NP001',nature) !== -1){
        $('li.step-2').removeClass('hidden');
        $('button.next-step').attr('id','activate-step-2');
        $('button.prev-step-sem').attr('id','activate-step-2');

        if ($.inArray('NP002',nature) === -1 && $.inArray('NP003',nature) === -1 && $.inArray('NP004',nature) === -1){
            $('.step-2-last').removeClass('hidden');
            $('button.next-step-fb').addClass('hidden');
        }
        else{
            $('.step-2-last').addClass('hidden');
            $('button.next-step-fb').removeClass('hidden');
        }

        $('input[id="sem-included"]').val('yes');
    }
    else{
        $('button.prev-step-sem').attr('id','activate-step-1');
        $('li.step-2').addClass('hidden');
        $('input[id="sem-included"]').val('no');
    }

    if ($.inArray('NP002',nature) !== -1){
        $('li.step-3').removeClass('hidden');

        if ($.inArray('NP001',nature) === -1){
          $('button.next-step').attr('id','activate-step-3');
        }
        else{
          $('button.next-step-fb').attr('id','activate-step-3');
        }

        if ($.inArray('NP003',nature) === -1 && $.inArray('NP004',nature) === -1){
            $('.step-3-last').removeClass('hidden');
            $('button.next-step-seo').addClass('hidden');
        }
        else{
            $('.step-3-last').addClass('hidden');
            $('button.next-step-seo').removeClass('hidden');
        }

        $('input[id="fb-included"]').val('yes');
    }
    else{
        $('button.next-step-fb').attr('id','activate-step-4');
        $('button.prev-step-fb').attr('id','activate-step-2');
        $('li.step-3').addClass('hidden');
        $('input[id="fb-included"]').val('no');
    }

    if ($.inArray('NP003',nature) !== -1){
        $('li.step-4').removeClass('hidden');

        if ($.inArray('NP001',nature) === -1 && $.inArray('NP002',nature) === -1){
          $('button.next-step').attr('id','activate-step-4');
          $('button.prev-step-fb').attr('id','activate-step-1');
        }

        if ($.inArray('NP002',nature) !== -1){
          $('button.prev-step-fb').attr('id','activate-step-3');
        }

        if ($.inArray('NP004',nature) === -1){
          $('button.next-step-web').addClass('hidden');
          $('.step-4-last').removeClass('hidden');
        }
        else{
          $('button.next-step-web').removeClass('hidden');
          $('.step-4-last').addClass('hidden');
        }

        $('input[id="seo-included"]').val('yes');
    }
    else{
        if($.inArray('NP002') === -1){
            $('button.next-step-fb').attr('id','activate-step-5');
            $('button.prev-step-seo').attr('id','activate-step-2');
        }
        else{
            $('button.next-step-seo').attr('id','activate-step-5');
            $('button.prev-step-seo').attr('id','activate-step-3');
        }

        $('li.step-4').addClass('hidden');
        $('input[id="seo-included"]').val('no');
    }

    if ($.inArray('NP004',nature) !== -1){
        $('li.step-5').removeClass('hidden');

        if ($.inArray('NP001',nature) === -1 && $.inArray('NP002',nature) === -1 && $.inArray('NP003',nature) === -1 ){
          $('button.next-step').attr('id','activate-step-5');
          $('button.prev-step-seo').attr('id','activate-step-1');
        }

        if ($.inArray('NP003',nature) !== -1){
          $('button.prev-step-seo').attr('id','activate-step-4');
        }

        $('input[id="web-included"]').val('yes');
    }
    else{
        $('li.step-5').addClass('hidden');
        $('input[id="web-included"]').val('no');
    }


    if (nature.length > 0){
        $('button.next-step').removeAttr('disabled');
    }
    else{
        $('button.next-step').attr('disabled','disabled');
    }


});

$(document).ready(function() {

    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    $('ul.setup-panel li.active a').trigger('click');

    // DEMO ONLY //

    $(document).on('click', 'button#activate-step-1', function(e) {
        $('ul.setup-panel li:eq(0)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-1"]').trigger('click');

    })

    $(document).on('click', 'button#activate-step-2', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');

    })

    $(document).on('click', 'button#activate-step-3', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');

    })

    $(document).on('click', 'button#activate-step-4', function(e) {
        $('ul.setup-panel li:eq(3)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-4"]').trigger('click');

    })

    $(document).on('click', 'button#activate-step-5', function(e) {
        $('ul.setup-panel li:eq(4)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-5"]').trigger('click');

    })
});
