<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Testing Routes
Route::get('/test', function(){
    return view('design.test1');
});

Route::get('/employee/view', [
    'uses' => 'EmployeeManagementController@view',
    'as'   => 'employee.view'
]);

Route::get('/test2', function(){
    return view('design.test2');
});
Route::get('/test3', function(){
    return view('design.test3');
});

Route::get('/testdash', [
    'uses' => 'UserDashboardController@testDash'
]);

Route::get('/security/errors', [
    'uses' => 'ErrorManagementController@index',
    'as'   => 'error.index'
]);

Route::get('/request-error/{name}', [
    'uses' => 'ErrorPageController@render',
    'as'   => 'error.render'
]);

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::get('/',[
  'uses' => 'UserLoginController@index',
  'as'   => 'user.login'
]);

Route::post('/',[
  'uses' => 'UserLoginController@login',
  'as'   => 'user.login'
]);

Route::get('/firstlogin/{userid}',[
  'uses' => 'UserLoginController@getInitialLogin',
  'as'   => 'user.firstlogin'
]);

Route::post('/firstlogin/{username}',[
  'uses' => 'UserLoginController@postInitialLogin',
  'as'   => 'user.firstlogin'
]);

Route::get('/checkauth', [
    'uses' => 'UserLoginController@checkAuth',
    'as' => 'user.checkauth'
]);

Route::get('/reset-password',[
    'uses' => 'UserLoginController@getResetPassword',
    'as'   => 'user.password.reset'
]);

Route::post('/reset-password',[
    'uses' => 'UserLoginController@postResetPassword',
    'as'   => 'user.password.reset'
]);

Route::get('/reset-password/{token}/{userid}',[
    'uses' => 'UserLoginController@getResetPasswordChange',
    'as'   => 'user.password.reset.change.show'
]);

Route::post('/reset-password/changed',[
    'uses' => 'UserLoginController@postResetPasswordChange',
    'as'   => 'user.password.reset.change'
]);

Route::group(['middleware' => 'prevent-back-history'],function(){

    Auth::routes();

    Route::group(['prefix' => 'dashboard'], function(){
        Route::post('/logout',[
        'uses' => 'UserDashboardController@logout',
        'as'   => 'user.logout'
        ]);

        Route::get('/change-password',[
        'uses' => 'UserDashboardController@getChangePassword',
        'as'   => 'user.changepassword'
        ]);

        Route::post('/change-password',[
        'uses' => 'UserDashboardController@postChangePassword',
        'as'   => 'user.changepassword'
        ]);

        Route::get('/role', [
        'uses' => 'UserRoleManagementController@userRole'
        ]);

        Route::get('/profile/{userid}',[
        'uses' => 'UserManagementController@show',
        'as'   => 'user.show'
        ]);

        Route::get('/filter/accounts/{by}/{userid}/{from}/{to}',[
            'uses' => 'UserDashboardController@filterAccountTotal'
        ]);

        Route::get('/filter/accounts/export/excel/{by}/{userid}/{from}/{to}', [
            'uses' => 'UserDashboardController@excelExportAccounts',
            'as'   => 'filter.accounts.export'
        ]);

        Route::get('/filter/leads/{by}/{userid}/{from}/{to}',[
            'uses' => 'UserDashboardController@filterLeadTotal'
        ]);

        Route::get('/filter/leads/export/excel/{by}/{userid}/{from}/{to}', [
            'uses' => 'UserDashboardController@excelExportLeads',
            'as'   => 'filter.leads.export'
        ]);

    });

    Route::get('/dashboard',[
    'uses' => 'UserDashboardController@index',
    'as'   => 'user.dashboard'
    ]);

    // admin routes

    Route::group(['prefix' => 'admin'], function(){
        Route::get('/usersearch/{user}',[
        'uses' => 'UserManagementController@search',
        'as'   => 'user.management.search'
        ]);

        Route::get('/order/{field}/{order}',[
        'uses' => 'UserManagementController@sort',
        'as'   => 'user.management.sort'
        ]);

        Route::post('/create', [
        'uses' => 'UserManagementController@create',
        'as'   => 'user.management.create'
        ]);

        Route::get('/edit/{userid}', [
        'uses' => 'UserManagementController@edit',
        'as'   => 'user.management.edit'
        ]);

        Route::post('/update', [
        'uses' => 'UserManagementController@update',
        'as'   => 'user.management.update'
        ]);

        Route::post('/delete', [
        'uses' => 'UserManagementController@delete',
        'as'   => 'user.management.delete'
        ]);

        Route::post('/role/update', [
        'uses' => 'UserRoleManagementController@update',
        'as'   => 'user.role.update'
        ]);

        Route::get('/role/get/{empid}', [
        'uses' => 'UserRoleManagementController@getRole'
        ]);
    });

    Route::group(['prefix' => 'role'], function(){

        Route::post('/create', [
            'uses' => 'RolesController@create',
            'as'   => 'role.create'
        ]);

        Route::get('/get/{roleid}', [
            'uses' => 'RolesController@edit',
            'as'   => 'role.edit'
        ]);

        Route::post('/update', [
            'uses' => 'RolesController@update',
            'as'   => 'role.update'
        ]);

        Route::post('/add/manager', [
            'uses' => 'RolesController@addManager',
            'as'   => 'role.add.manager'
        ]);

        Route::post('/delete/manager', [
            'uses' => 'RolesController@deleteManager',
            'as'   => 'role.delete.manager'
        ]);

    });

    Route::group(['prefix' => 'audit', 'middleware' => ['auth','admin'] ], function(){
        
        Route::get('/list',[
            'uses' => 'AuditLogsController@list',
            'as'   => 'audit.list'
        ]);

        Route::get('/log/show/{id}',[
            'uses' => 'AuditLogsController@show',
            'as'   => 'audit.show'
        ]);
    });

    // employee routes

    Route::group(['prefix' => 'employee', 'middleware' => ['auth','employees'] ], function(){

        Route::get('/list',[
            'uses' => 'EmployeeManagementController@list',
            'as'   => 'employee.list'
        ]);

        Route::get('/add', [
            'uses' => 'EmployeeManagementController@create',
            'as'   => 'employee.create'
        ]);

        Route::post('/add', [
            'uses' => 'EmployeeManagementController@save',
            'as'   => 'employee.create'
        ]);

        Route::get('/edit/{emp_id}', [
            'uses' => 'EmployeeManagementController@edit',
            'as'   => 'employee.edit'
        ]); 

        Route::post('/update', [
            'uses' => 'EmployeeManagementController@update',
            'as'   => 'employee.update'
        ]);

        Route::post('/delete', [
            'uses' => 'EmployeeManagementController@delete',
            'as'   => 'employee.delete'
        ]);

        Route::get('/search/{employee}',[
        'uses' => 'EmployeeManagementController@search',
        'as'   => 'employee.search'
        ]);

        Route::get('/sort/{field}/{order}',[
        'uses' => 'EmployeeManagementController@sort',
        'as'   => 'employee.sort'
        ]);

        Route::get('/new/{branch}', [
            'uses' => 'EmployeeManagementController@newEmployee'
        ]);

        Route::get('/get/countries', [
            'uses' => 'EmployeeManagementController@getCountries'
        ]);

        Route::get('/get/region/{country}', [
            'uses' => 'EmployeeManagementController@getRegion'
        ]);

        Route::get('/get/state/{region}', [
            'uses' => 'EmployeeManagementController@getState'
        ]);

        Route::get('/get/city/{region}/{state}', [
            'uses' => 'EmployeeManagementController@getCity'
        ]);

        Route::get('/designations/{dept}', [
            'uses' => 'EmployeeManagementController@designations'
        ]);

    });

    Route::group(['prefix' => 'roles', 'middleware' => ['auth', 'roles'] ], function(){

        Route::get('/list',[
            'uses' => 'UserRoleManagementController@list',
            'as'   => 'roles.list'
        ]);

    });

    Route::group(['prefix' => 'lobby', 'middleware' => ['auth', 'lobby'] ], function() {
        Route::get('/list',[
            'uses'  => 'LobbyManagementController@list',
            'as'    => 'lobby.list'
        ]);

        Route::get('/search/{lobby}',[
            'uses' => 'LobbyManagementController@search',
            'as'   => 'lobby.search'
        ]);

        Route::get('/refresh',[
            'uses' => 'LobbyManagementController@refresh',
            'as'   => 'lobby.refresh'
        ]);

        Route::get('/sort/{sortby}/{sortorder}', [
            'uses' => 'LobbyManagementController@sort',
            'as'   => 'lobby.sort'
        ]);

        Route::get('/filter/{column}', [
            'uses' => 'LobbyManagementController@filter',
            'as'   => 'lobby.filter'
        ]);

        Route::get('/filter/{column}/{value}', [
            'uses' => 'LobbyManagementController@filter',
            'as'   => 'lobby.filter'
        ]);

        Route::post('/update/open', [
            'uses'  => 'LobbyManagementController@updateLeadProspectAccountToOpen',
            'as'    => 'lobby.open'
        ]);

        Route::get('/upload', [
            'uses'  => 'LobbyManagementController@getUpload',
            'as'    => 'lobby.upload'
        ]);

        Route::post('/upload', [
            'uses'  => 'LobbyManagementController@storeUpload',
            'as'    => 'lobby.upload'
        ]);

    });

    Route::group(['prefix' => 'lead'], function() {
        Route::post('/note/add',[
            'uses' => 'LeadManagementController@createNote',
            'as'   => 'lead.note.create'
        ]);

        Route::patch('/note/update',[
            'uses' => 'LeadManagementController@updateNote',
            'as'   => 'lead.note.update'
        ]);

        Route::patch('/note/delete',[
            'uses' => 'LeadManagementController@deleteNote',
            'as'   => 'lead.note.delete'
        ]);

        Route::get('/note/filter/{id}/{by}/{userid}/{from}/{to}',[
            'uses' => 'LeadManagementController@filterNotes',
        ]);
    });

    Route::group(['prefix' => 'lead', 'middleware' => ['auth', 'leads'] ], function(){

        Route::get('/list',[
            'uses' => 'LeadManagementController@list',
            'as'   => 'lead.list'
        ]);

        Route::get('/create', [
            'uses' => 'LeadManagementController@create',
            'as'   => 'lead.create'
        ]);

        Route::post('/create', [
            'uses' => 'LeadManagementController@save',
            'as'   => 'lead.create'
        ]);

        Route::get('/view/{id}', [
            'uses' => 'LeadManagementController@view',
            'as'   => 'lead.view'
        ]);

        Route::get('/edit/{id}', [
            'uses' => 'LeadManagementController@edit',
            'as'   => 'lead.edit'
        ]);

        Route::post('/update', [
            'uses' => 'LeadManagementController@update',
            'as'   => 'lead.update'
        ]);

        Route::post('/delete', [
            'uses' => 'LeadManagementController@delete',
            'as'   => 'lead.delete'
        ]);

        Route::get('/sort/{sortby}/{sortorder}', [
            'uses' => 'LeadManagementController@sort',
            'as'   => 'lead.sort'
        ]);

        Route::get('/search/{lead}',[
        'uses' => 'LeadManagementController@search',
        'as'   => 'lead.search'
        ]);

       
        Route::get('/convert/{lead}/prospect',[
        'uses' => 'LeadManagementController@getProspectStep1',
        'as'   => 'lead.convert.prospect'
        ]);

        Route::post('/convert/{lead}/prospect',[
            'uses' => 'LeadManagementController@convertProspectStep1',
            'as'   => 'lead.convert.prospect'
        ]);
    });

    Route::group(['prefix' => 'account', 'middleware' => ['auth', 'accounts'] ], function(){

        Route::patch('/modifycolumn', [
            'uses' => 'AccountManagementController@modifyAccountsColumn',
            'as'   => 'accounts.modify.column'
        ]);

        Route::post('/multiple-assign-holder', [
            'uses' => 'AccountManagementController@multipleHolderAssign',
            'as'   => 'accounts.holderAssign'
        ]);

        Route::get('/list',[
            'uses' => 'AccountManagementController@list',
            'as'   => 'account.list'
        ]);

        Route::post('/create',[
            'uses' => 'AccountManagementController@create',
            'as'   => 'account.create'
        ]);

        Route::get('/edit/{account}',[
            'uses' => 'AccountManagementController@edit',
            'as'   => 'account.edit'
        ]);

        Route::post('/update',[
            'uses' => 'AccountManagementController@update',
            'as'   => 'account.update'
        ]);

        Route::get('/edit/assign/{account}',[
            'uses' => 'AccountManagementController@assign',
            'as'   => 'account.edit.assign'
        ]);

        Route::post('/update/assign',[
            'uses' => 'AccountManagementController@assignUpdate',
            'as'   => 'account.update.assign'
        ]);
        

        Route::get('/assign/{id}/{package}/{packageid}/{member}', [
            'uses' => 'AccountManagementController@assignMembers',
            'as'   => 'account.update.member'
        ]);

        Route::get('/assign/{id}/{package}/{packageid}/{member}/{assistant}', [
            'uses' => 'AccountManagementController@assignMembers',
            'as'   => 'account.update.member'
        ]);

        Route::get('/assign/{id}/{package}/{packageid}/{member}/{assistant}/{member2}', [
            'uses' => 'AccountManagementController@assignMembers',
            'as'   => 'account.update.member'
        ]);

        Route::get('/assign/{id}/{package}/{packageid}/{member}/{assistant}/{member2}/{assistant2}', [
            'uses' => 'AccountManagementController@assignMembers',
            'as'   => 'account.update.member'
        ]);

        Route::get('/assign/{id}/{package}/{packageid}/{member}/{assistant}/{member2}/{assistant2}/{others}', [
            'uses' => 'AccountManagementController@assignMembers',
            'as'   => 'account.update.member'
        ]);
        
        Route::get('/view/{id}',[
            'uses' => 'AccountManagementController@viewAccount',
            'as'   => 'account.view'
        ]);

        Route::get('/view/{id}/{nature}',[
            'uses' => 'AccountManagementController@viewAccount',
            'as'   => 'account.view.nature'
        ]);

        Route::post('/delete',[
            'uses' => 'AccountManagementController@delete',
            'as'   => 'account.delete'
        ]);
        
        // Route::get('/historical-data/delete/{accountid}/{package}/{doc}',[
        //     'uses' => 'AccountManagementController@deleteHistoricalData',
        //     'as'   => 'account.historicaldata.delete'
        // ]);

        // Route::get('/services-blog-historical-data/delete/{accountid}/{package}/{doc}',[
        //     'uses' => 'AccountManagementController@deleteServicesBlogHistoricalData',
        //     'as'   => 'blog.historicaldata.delete'
        // ]);

        // Route::get('/services-social-historical-data/delete/{accountid}/{package}/{doc}',[
        //     'uses' => 'AccountManagementController@deleteServicesSocialHistoricalData',
        //     'as'   => 'social.historicaldata.delete'
        // ]);

        // Route::get('/services-baidu-historical-data/delete/{accountid}/{package}/{doc}',[
        //     'uses' => 'AccountManagementController@deleteServicesBaiduWeiboHistoricalData',
        //     'as'   => 'baiduweibo.historicaldata.delete'
        // ]);
        
        Route::get('/sort/{sortby}/{sortorder}', [
            'uses' => 'AccountManagementController@sort',
            'as'   => 'account.sort'
        ]);

        Route::get('/search/{account}',[
            'uses' => 'AccountManagementController@search',
            'as'   => 'account.search'
        ]);
        
        Route::get('/refresh',[
            'uses' => 'AccountManagementController@refresh',
            'as'   => 'account.refresh'
        ]);

        Route::get('/fetch/{by}/{item}', [
            'uses' => 'AccountManagementController@fetchItems',
            'as'   => 'account.fetch.item'
        ]);

        Route::get('/fetch/update/{table}/{by}/{item}', [
            'uses' => 'AccountManagementController@setFilterItem',
            'as'   => 'accounts.set.filter.item'
        ]);
    });

    Route::group(['prefix' => 'notification'], function(){
        
        Route::get('/list',[
            'uses' => 'NotificationController@list',
            'as'   => 'notification.list'
        ]);

        Route::post('/all',[
            'uses' => 'NotificationController@fetchAll',
            'as'   => 'notification.all'
        ]);

        Route::post('/fetch',[
            'uses' => 'NotificationController@fetch',
            'as'   => 'notification.fetch'
        ]);

        Route::post('/fetch/reminders',[
            'uses' => 'NotificationController@fetchReminders',
            'as'   => 'notification.fetch.reminders'
        ]);

        Route::post('/read',[
            'uses' => 'NotificationController@read',
            'as'   => 'notification.read'
        ]);

        Route::get('/clear', [
            'uses' => 'NotificationController@clear',
            'as'   => 'notification.clear'
        ]);
    });

    Route::group(['prefix' => 'reminder'], function(){
        Route::post('/create',[
            'uses' => 'AccountReminderController@create',
            'as'   => 'reminder.create'
        ]);

        Route::get('/get/receivers/{id}',[
            'uses' => 'AccountReminderController@getReceivers',
            'as'   => 'reminder.receivers'
        ]);

        Route::get('/list',[
            'uses' => 'AccountReminderController@list',
            'as'   => 'reminder.list'
        ]);

        Route::get('/show/{id}',[
            'uses' => 'AccountReminderController@show',
            'as'   => 'reminder.show'
        ]);

        Route::get('/clear', [
            'uses' => 'NotificationController@reminderClear',
            'as'   => 'notification.clear'
        ]);
    });

    Route::group(['prefix' => 'contract'], function() {
        Route::post('/create', [
            'uses' => 'AccountContractController@create',
            'as'   => 'contract.create'
        ]);

        Route::get('/get-contract/{id}',[
            'uses' => 'AccountContractController@getContracts',
            'as'   => 'contract.all'
        ]);

        Route::get('/summary/{prospectid}',[
            'uses' => 'AccountContractController@getSummary',
            'as'   => 'contract.summary'
        ]);

        Route::post('/delete',[
            'uses' => 'AccountContractController@deleteContract',
            'as'   => 'contract.delete'
        ]);
    });

    Route::group(['prefix'  => 'crawl'], function() {

        Route::get('/get', [
            'uses'  => 'WebCrawlController@get',
            'as'    => 'crawl.get'
        ]);

        Route::get('/links', [
            'uses'  => 'WebCrawlController@links',
            'as'    => 'crawl.links'
        ]);

    });

    Route::group(['prefix' => 'prospect', 'middleware' => ['auth', 'prospects']], function() {
        Route::get('/receivers',[
            'uses' => 'ProspectManagementController@receivers',
            'as'   => 'prospect.receivers'
        ]);

        Route::get('/list', [
            'uses' => 'ProspectManagementController@list',
            'as'   => 'prospect.list'
        ]);

        Route::get('/view/{id}', [
            'uses' => 'ProspectManagementController@view',
            'as'   => 'prospect.view'
        ]);

        Route::get('/edit/{prospect}',[
            'uses'  => 'ProspectManagementController@edit',
            'as'    => 'prospect.edit'
        ]);

        Route::post('/update', [
            'uses'  => 'ProspectManagementController@update',
            'as'    => 'prospect.update'
        ]);

        Route::post('/delete', [
            'uses'  => 'ProspectManagementController@delete',
            'as'    => 'prospect.delete'
        ]);

        Route::get('/search/{prospect}', [
            'uses'  => 'ProspectManagementController@search',
            'as'    => 'prospect.search'
        ]);

        Route::get('/sort/{sortby}/{sortorder}', [
            'uses' => 'ProspectManagementController@sort',
            'as'   => 'prospect.sort'
        ]);

        Route::get('/refresh', [
            'uses'  => 'ProspectManagementController@refresh',
            'as'    => 'prospect.refresh'
        ]);

        Route::get('/convert/{prospect}/step-1',[
            'uses' => 'ProspectManagementController@getStep1',
            'as'   => 'prospect.convert.step1'
        ]);

        Route::post('/convert/{prospect}/step-1',[
            'uses' => 'ProspectManagementController@convertStep1',
            'as'   => 'prospect.convert.step1'
        ]);

        Route::get('/convert/{prospect}/step-2',[
            'uses' => 'ProspectManagementController@getStep2',
            'as'   => 'prospect.convert.step2'
        ]);

        Route::post('/convert/{prospect}/step-2',[
            'uses' => 'ProspectManagementController@convertStep2',
            'as'   => 'prospect.convert.step2'
        ]);

        Route::get('/convert/{prospect}/step-3',[
            'uses' => 'ProspectManagementController@getStep3',
            'as'   => 'prospect.convert.step3'
        ]);

        Route::post('/convert/{prospect}/step-3',[
            'uses' => 'ProspectManagementController@convertStep3',
            'as'   => 'prospect.convert.step3'
        ]);

        Route::get('/convert/{prospect}/step-4',[
            'uses' => 'ProspectManagementController@getStep4',
            'as'   => 'prospect.convert.step4'
        ]);

        Route::post('/convert/{prospect}/step-4',[
            'uses' => 'ProspectManagementController@convertStep4',
            'as'   => 'prospect.convert.step4'
        ]);

        Route::get('/convert/{prospect}/step-5',[
            'uses' => 'ProspectManagementController@getStep5',
            'as'   => 'prospect.convert.step5'
        ]);

        Route::post('/convert/{prospect}/step-5',[
            'uses' => 'ProspectManagementController@convertStep5',
            'as'   => 'prospect.convert.step5'
        ]);
        // New Nature (Baidu, Weibo, WeChat)
        Route::get('/convert/{prospect}/step-6',[
            'uses' => 'ProspectManagementController@getStep6',
            'as'   => 'prospect.convert.step6'
        ]);

        Route::post('/convert/{prospect}/step-6',[
            'uses' => 'ProspectManagementController@convertStep6',
            'as'   => 'prospect.convert.step6'
        ]);

        Route::get('/convert/{prospect}/step-7',[
            'uses' => 'ProspectManagementController@getStep7',
            'as'   => 'prospect.convert.step7'
        ]);

        Route::post('/convert/{prospect}/step-7',[
            'uses' => 'ProspectManagementController@convertStep7',
            'as'   => 'prospect.convert.step7'
        ]);

        Route::get('/convert/{prospect}/step-8',[
            'uses' => 'ProspectManagementController@getStep8',
            'as'   => 'prospect.convert.step8'
        ]);

        Route::post('/convert/{prospect}/step-8',[
            'uses' => 'ProspectManagementController@convertStep8',
            'as'   => 'prospect.convert.step8'
        ]);

        Route::get('/convert/{prospect}/step-9',[
            'uses' => 'ProspectManagementController@getStep9',
            'as'   => 'prospect.convert.step9'
        ]);

        Route::post('/convert/{prospect}/step-9',[
            'uses' => 'ProspectManagementController@convertStep9',
            'as'   => 'prospect.convert.step9'
        ]);

        Route::get('/convert/{prospect}/step-10',[
            'uses' => 'ProspectManagementController@getStep10',
            'as'   => 'prospect.convert.step10'
        ]);

        Route::post('/convert/{prospect}/step-10',[
            'uses' => 'ProspectManagementController@convertStep10',
            'as'   => 'prospect.convert.step10'
        ]);

        Route::get('/convert/{prospect}/step-11',[
            'uses' => 'ProspectManagementController@getStep11',
            'as'   => 'prospect.convert.step11'
        ]);

        Route::post('/convert/{prospect}/step-11',[
            'uses' => 'ProspectManagementController@convertStep11',
            'as'   => 'prospect.convert.step11'
        ]);

        

    });

    Route::get('/items/{table}/{perpage}',[
        'uses'  => 'AccountManagementController@paginationPerPage',
        'as'    =>  'table.perpage'
    ]);

    Route::get('/filter/{table}/{person}',[
        'uses'  => 'SalesPerformanceController@setFilterSelection',
        'as'    =>  'filter.selection'
    ]);

    Route::middleware(['auth', 'appointments'])->prefix('appointment')->group(function() {
        Route::get('/list', 'AppointmentManagementController@list')->name('appointment.list');
        Route::get('/edit/{id}', 'AppointmentManagementController@edit');
        Route::get('/create', 'AppointmentManagementController@store');
        Route::put('/update/{id}', 'AppointmentManagementController@update');
        Route::post('/quick/create/lead', 'AppointmentManagementController@quickAddLead');

        // Update the appointment through modal popup
        Route::put('/modal/update/{id}', 'AppointmentManagementController@updatePending');
        Route::get('/pending', 'AppointmentManagementController@pending');
    });

//    Route::group(['prefix' => 'performance', 'middleware' => ['auth', 'performance']], function() {
//        Route::get('/list', ['uses' => 'SalesPerformanceController@list', 'as' => 'performance.list']);
//        Route::get('/refresh', ['uses' => 'SalesPerformanceController@refresh', 'as' => 'performance.refresh']);
//        Route::get('/sort/{sortBy}/{sortOrder}', ['uses' => 'SalesPerformanceController@sort', 'as' => 'performance.sort']);
//        Route::get('/search/{perf}', ['uses' => 'SalesPerformanceController@search', 'as' => 'performance.search']);
//        Route::get('/fetch/{value}', 'SalesPerformanceController@fetchDataOnSalesReport');
//        Route::get('/fetch_remark/{perf}', 'SalesPerformanceController@fetchNotes');
//        Route::put('/update_status/{perf}', 'SalesPerformanceController@updateStatus');
//        Route::patch('/add_remark', 'SalesPerformanceController@addNote');
//        Route::get('/opportunity/{id}', 'SalesPerformanceController@getOpportunityInformation');
//    });

    Route::group(['prefix' => 'performance', 'middleware' => ['auth', 'performance']], function() {
        Route::get('/list', ['uses' => 'SalesPerformanceController@list', 'as' => 'performance.list']);
        Route::get('/refresh', ['uses' => 'SalesPerformanceController@refresh', 'as' => 'performance.refresh']);
        Route::get('/sort/{sortBy}/{sortOrder}', ['uses' => 'SalesPerformanceController@sort', 'as' => 'performance.sort']);
        Route::get('/search/{perf}', ['uses' => 'SalesPerformanceController@search', 'as' => 'performance.search']);
        Route::get('/fetch/{value}', 'SalesPerformanceController@fetchDataOnSalesReport');
        Route::get('/fetch_remark/{perf}', 'SalesPerformanceController@fetchNotes');
        Route::put('/update_status/{perf}', 'SalesPerformanceController@updateStatus');
        Route::patch('/add_remark', 'SalesPerformanceController@addNote');
        Route::get('/lead/{id}', 'SalesPerformanceController@getLeadInformation');
    });

    Route::middleware(['auth', 'opportunity'])->prefix('opportunity')->group(function() {
        // For Backend
        Route::get('/list', 'OpportunityManagementController@list')->name('opportunity.list');
        Route::get('/list/{sort}/{order}', 'OpportunityManagementController@list')->name('opportunity.sort');
        Route::get('/refresh', 'OpportunityManagementController@refresh')->name('opportunity.refresh');
        Route::get('/search/{string}', 'OpportunityManagementController@search')->name('opportunity.search');
       
        Route::post('/store', 'OpportunityManagementController@store')->name('opportunity.store');
        Route::post('/store_session', 'OpportunityManagementController@storeSession')->name('opportunity.storeSession');
        Route::post('/store_session2', 'OpportunityManagementController@directSessionConvert')->name('opportunity.storeSession2');

        Route::post('/delete', 'OpportunityManagementController@destroy')->name('opportunity.delete');

        // API request
        Route::get('/api/options', 'OpportunityManagementController@getOptionsData');
        Route::get('/api/search/{company}', 'OpportunityManagementController@searchString');
        Route::get('/api/getProspect/{id}', 'OpportunityManagementController@getProspect');
        Route::get('/api/getOpportunityInfo/{id}', 'OpportunityManagementController@getOpportunityInfo');
        // Route::get('/refresh', 'OpportunityManagementController@refresh')->name('opportunity.refresh');
        // Route::get('/sort/{sortBy}/{sortOrder}', 'OpportunityManagementController@sort')->name('opportunity.sort');
        // Route::get('/search/{opp}', 'OpportunityManagementController@search')->name('opportunity.search');
        // Route::get('/edit/{opp}', 'OpportunityManagementController@edit')->name('opportunity.edit');
        // Route::put('/update/{id}', 'OpportunityManagementController@update')->name('opportunity.update');
    });

});

// Route::group(['middleware' => 'prevent-logout'], function() {

//     Route::get('/account/view/pdf/{package}/{file}/{fname}', [ 'as' => 'account.view.pdf', function($package, $file, $fname) {
//         $path = storage_path($package . DIRECTORY_SEPARATOR . 'historicaldata' . DIRECTORY_SEPARATOR) . $file;
//         $headers = strpos($file,'pdf') ? array('Content-Type: application/pdf') : array('Content-Type: application/word');
//         return \Response::download($path, $fname, $headers);
        
//         // $filetype = strpos($file,'pdf') ? 'application/pdf' : 'application/word';
//         // return \Response::make(file_get_contents($path), 200, [
//         //             'Content-Type' => $filetype,
//         //             'Content-Disposition' => 'inline; filename="'.$fname.'"'
//         // ]);
//     }]);
    
// });

Route::get('/lobby/download/file', ['uses'  => 'LobbyManagementController@downloadLead','as'    => 'lobby.download']);
Route::get('/performance/export/{sales}/{opportunityStart}/{opportunityEnd}/{appointmentStart}/{appointmentEnd}/{appointmentChecked}', ['uses' => 'SalesPerformanceController@getSalesReport', 'as' => 'performance.export']);

