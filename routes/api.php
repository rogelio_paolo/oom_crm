<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Lead;
use App\Account;
use App\User;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['guest']], function () {
    Route::post('/crm/user/create', 'UserManagementController@create');
});

Route::get('/account/view/pdf/{package}/{file}/{fname}', [ 'as' => 'account.view.pdf', function($package, $file, $fname) {
    $path = storage_path($package . DIRECTORY_SEPARATOR . 'historicaldata' . DIRECTORY_SEPARATOR) . $file;
    
    $filetype = strpos($file,'pdf') ? 'application/pdf' : 'application/word';
    return \Response::make(file_get_contents($path), 200, [
                'Content-Type' => $filetype,
                'Content-Disposition' => 'inline; filename="'.$fname.'"'
    ]);
}]);

Route::get('/historical-data/delete/{accountid}/{package}/{doc}',[ 'as'   => 'account.historicaldata.delete', function($accountid, $package, $doc) {
    $account = Account::find($accountid);

    $data = $package == 'sem' ? 'historical_data' : $package.'_hist_data';
    $data_fname = $package == 'sem' ? 'historical_data_fname' : $package.'_hist_data_fname';
    $hashed = explode('|',$account->$package->$data);
    $original = explode('|',$account->$package->$data_fname);
    
    foreach($hashed as $n => $row)
    {
        if($row == $doc)
        {
            $del_file = $original[$n];
            \File::delete(storage_path($package.'/historicaldata/'.$doc));
            unset($hashed[$n]);
            unset($original[$n]);
        }
    }
    
    $account->$package->$data = !empty($hashed) ? implode('|',$hashed) : null;
    $account->$package->$data_fname = implode('|',$original);
    $account->$package->update();

    $user = User::where('userid', $account->updated_by)->first();

    app(__('global.Audit'))->logAuditTrail("MD003","AT006","Removed historical data of Account ".$account->prospect->lead->company,null,null,request()->ip(),$user->userid,$user->role);

    return redirect()->back();
}]);

