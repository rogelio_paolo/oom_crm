** Getting the repository in local/hosting server **

	0) git init
	1) git add remote remotename git@bitbucket.org:rogelio_paolo/oom_crm.git
	2) ssh-keygen -t rsa
	3) eval $(ssh-agent)
	4) ssh-add ~/path/to/key/yourkeyname
	5) if master branch:
		- git fetch && git checkout master
		else
		- git fetch -all
		- git reset --hard remotename/branchname
		- git pull remotename branchname
		
*******************************************************************************